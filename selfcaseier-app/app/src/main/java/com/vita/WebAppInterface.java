package com.vita;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.tencent.bugly.beta.Beta;
import com.vita.util.Helper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class WebAppInterface {
    private final static Logger logger = LoggerFactory.getLogger(WebAppInterface.class);

    private Context mContext;
    private WebView webView;
    private SecondScreen secondScreen;

    public Context getmContext() {
        return mContext;
    }

    public WebView getWebView() {
        return webView;
    }

    WebAppInterface(Context c, SecondScreen secondScreen, WebView wv) {
        mContext = c;
        webView = wv;
        this.secondScreen = secondScreen;
    }

    @JavascriptInterface
    public void secondRedirect(final String url) {
        if (secondScreen != null)
            secondScreen.setWebViewUrl(url);
    }

    @JavascriptInterface
    public void calculator() {
        MainActivity activity = (MainActivity) mContext;
        PackageInfo pak = getAllApps("Calculator", "calculator"); //大小写
        if (pak != null) {
            Intent intent = new Intent();
            intent = mContext.getPackageManager().getLaunchIntentForPackage(pak.packageName);
            activity.startActivity(intent);
        } else {
            Toast.makeText(mContext, "未找到计算器", Toast.LENGTH_SHORT).show();
        }
    }


    public PackageInfo getAllApps(String app_flag_1, String app_flag_2) {
        PackageManager pManager = mContext.getPackageManager();
        // 获取手机内所有应用
        List<PackageInfo> packlist = pManager.getInstalledPackages(0);
        for (int i = 0; i < packlist.size(); i++) {
            PackageInfo pak = (PackageInfo) packlist.get(i);
            if (pak.packageName.contains(app_flag_1) || pak.packageName.contains(app_flag_2)) {
                return pak;
            }
        }
        return null;
    }

    @JavascriptInterface
    public void exitApp() {
        MainActivity activity = (MainActivity) mContext;
        activity.finish();
        System.exit(0);
    }

    @JavascriptInterface
    public void updateApp() {
        Beta.checkUpgrade();
    }

    @JavascriptInterface
    public void minApp() {
        MainActivity activity = (MainActivity) mContext;
        activity.moveTaskToBack(true);
    }

    @JavascriptInterface
    public void setRid(String rid, String data, String headers) {
        MainActivity.postDatas.put(rid, data);
        MainActivity.postHeaders.put(rid, headers);
    }

    @JavascriptInterface
    public String coreExecAsync(String uuid, boolean isIntervalLive) {
        Map<String, Object> result = new HashMap<>();

        if (!isIntervalLive) {
            Helper.removeStore(mContext, uuid);
            result.put("code", "200");
            result.put("msg", "");
            result.put("data", new Object[]{""});
            return Helper.objectToJson(result);
        }

        String invokeResult = Helper.getStore(mContext, uuid, "=== waiting for return ===");
        if (invokeResult.equals("=== waiting for return ===")) {
            result.put("code", "0");
            result.put("msg", "=== waiting for return ===");
            result.put("data", uuid);
            return Helper.objectToJson(result);
        }

        Helper.removeStore(mContext, uuid);
        result.put("code", "200");
        result.put("msg", "");
        if (JSON.isValidArray(invokeResult) || JSON.isValidObject(invokeResult)) {
            result.put("data", new Object[]{JSON.parse(invokeResult)});
        } else {
            result.put("data", new Object[]{invokeResult});
        }
        return Helper.objectToJson(result);
    }

    @JavascriptInterface
    public String coreExec(String className, String methodName) {
        return coreExec(className, methodName, "");
    }

    @JavascriptInterface
    public String coreExec(String className, String methodName, String params) {
        Map<String, Object> result = new HashMap<>();
        try {
            JSONArray arr = (JSONArray) JSONArray.parse(params.equals("") ? "[]" : params);
            arr.add(0, this);
            Object obj = Class.forName("com.vita.controller." + upperCase(className)).newInstance();
            Method[] methods = obj.getClass().getDeclaredMethods();
            for (Method method : methods) {
                if (!Modifier.isPublic(method.getModifiers())) {
                    continue;
                }

                method.setAccessible(true);
                if (method.getName().equalsIgnoreCase(methodName)) {
                    Object invokeResult;
                    if (method.getReturnType() == void.class) {
                        String uuid = UUID.randomUUID().toString();
                        arr.add(1, uuid);
                        castType(method.getParameterTypes(), arr, 2);
                        method.invoke(obj, arr.toArray(new Object[]{}));
                        invokeResult = Helper.getStore(mContext, uuid, "=== waiting for return ===");
                        if (invokeResult.equals("=== waiting for return ===")) {
                            result.put("code", "0");
                            result.put("msg", "=== waiting for return ===");
                            result.put("data", uuid);
                            return Helper.objectToJson(result);
                        }
                    } else {
                        castType(method.getParameterTypes(), arr, 1);
                        invokeResult = method.invoke(obj, arr.toArray(new Object[]{}));
                    }
                    result.put("code", "200");
                    result.put("msg", "");
                    if (invokeResult != null && invokeResult.getClass().isArray()) {
                        result.put("data", invokeResult);
                    } else {
                        result.put("data", new Object[]{invokeResult});
                    }
                    return Helper.objectToJson(result);
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            result.put("code", "201");
            result.put("msg", e.getMessage());
            return Helper.objectToJson(result);
        }

        result.put("code", "202");
        result.put("msg", "method not found");
        return Helper.objectToJson(result);
    }

    void castType(Class<?>[] pt, JSONArray arr, int startIndex) {
        for (int i = startIndex; i < pt.length; i++) {
            if (arr.size() < i) {
                break;
            }
            if (pt[i] == String.class) {
                arr.set(i, Helper.toStr(arr.size() > i ? arr.get(i) : ""));
            }
            if (pt[i] == int.class || pt[i] == Integer.class) {
                arr.set(i, Helper.toInt(arr.size() > i ? arr.get(i) : 0));
            }
            if (pt[i] == float.class) {
                arr.set(i, Helper.toFloat(arr.size() > i ? arr.get(i) : 0));
            }
            if (pt[i] == double.class) {
                arr.set(i, (double) Helper.toFloat(arr.size() > i ? arr.get(i) : 0));
            }
            if (pt[i] == boolean.class || pt[i] == Boolean.class) {
                String tmpStr = Helper.toStr(arr.size() > i ? arr.get(i) : false);
                arr.set(i, tmpStr.equals("1") || tmpStr.equalsIgnoreCase("true") ? true : false);
            }
        }
    }

    String upperCase(String str) {
        String[] tmp = str.split("/");
        String outStr = "";
        for (int i = 0; i < tmp.length; i++) {
            if (i > 0) {
                outStr += ".";
            }
            if (i < tmp.length - 1) {
                outStr += tmp[i];
            } else {
                char[] ch = tmp[i].toCharArray();
                if (ch[0] >= 'a' && ch[0] <= 'z') {
                    ch[0] = (char) (ch[0] - 32);
                }
                outStr += new String(ch);
            }
        }

        return outStr;
    }

}
