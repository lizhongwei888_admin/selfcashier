package com.vita;

import android.app.Presentation;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.Display;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.vita.util.OkHttpExt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.util.Map;

public class SecondScreen extends Presentation {
    public SecondScreen(Context context, Display display) {
        super(context, display);

        this.context = context;
    }

    private final static Logger logger = LoggerFactory.getLogger(SecondScreen.class);
    private WebView webView;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        webView = (WebView) findViewById(R.id.webView);
        WebSettings webSettings = webView.getSettings();
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setAppCacheEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setAllowFileAccess(true);
        webSettings.setAllowFileAccessFromFileURLs(true);
        webSettings.setAllowUniversalAccessFromFileURLs(true);
        webView.setBackgroundColor(Color.TRANSPARENT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            webView.setWebContentsDebuggingEnabled(true);
        }
        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                return super.onJsAlert(view, url, message, result);
            }
        });
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, final WebResourceRequest request) {
                String url = request.getUrl().toString();
                String rid = request.getRequestHeaders().get("ajax_guid");
                if (rid != null) {
                    try {
                        MainActivity mainActivity = (MainActivity) context;
                        url = mainActivity.injectUrl(url);
                        String postData = mainActivity.getRequestBody(rid);
                        Map<String, String> headers = mainActivity.getRequestHeaders(rid);
                        byte[] result;
                        if (request.getMethod().equals("GET")) {
                            result = OkHttpExt.execGetReBody(url, "", headers).bytes();
                        } else {
                            result = OkHttpExt.execPost(url, postData, headers, "application/json").getBytes();
                        }
                        return new WebResourceResponse(headers.get("Content-Type"), "utf-8", new ByteArrayInputStream(result));
                    } catch (Exception e) {
                        logger.error(e.getMessage(), e);
                    }
                }
                return super.shouldInterceptRequest(view, request);
            }
        });

        webView.addJavascriptInterface(new WebAppInterface(getContext(), this, webView), "android");
    }

    public void setWebViewUrl(String url) {
        if (!"".equals(url)) {
            url = "#" + url;
        }
        final String finalUrl = url;
        webView.post(new Runnable() {
            @Override
            public void run() {
                if (MainActivity.isApkInDebug(getContext())) {
                    webView.loadUrl("http://" + BuildConfig.App_HtmlHostIP + ":8080/index.html" + finalUrl);
                } else {
                    webView.loadUrl("file:///android_asset/index.html" + finalUrl);
                }
            }
        });
    }
}
