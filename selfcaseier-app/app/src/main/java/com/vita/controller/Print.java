package com.vita.controller;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Environment;
import android.util.Log;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.newland.pospp.openapi.manager.INewlandCashDrawerManager;
import com.newland.pospp.openapi.manager.INewlandManagerCallback;
import com.newland.pospp.openapi.manager.INewlandOpenDrawerCallback;
import com.newland.pospp.openapi.manager.INewlandPrintCallback;
import com.newland.pospp.openapi.manager.INewlandPrinterManager;
import com.newland.pospp.openapi.manager.ServiceManagerType;
import com.newland.pospp.openapi.manager.ServiceManagersHelper;
import com.newland.pospp.openapi.model.CapabilityProvider;
import com.newland.pospp.openapi.model.NewlandError;
import com.newland.pospp.openapi.model.PrinterScript;
import com.newland.pospp.openapi.model.printer.Align;
import com.newland.pospp.openapi.model.printer.Barcode;
import com.newland.pospp.openapi.model.printer.FontSize;
import com.newland.pospp.openapi.model.printer.PaperSize;
import com.newland.pospp.openapi.model.printer.PrinterConfig;
import com.newland.pospp.openapi.model.printer.PrinterIdentify;
import com.newland.pospp.openapi.model.printer.Qrcode;
import com.newland.pospp.openapi.model.printer.Text;
import com.newland.pospp.openapi.model.printer.Texts;
import com.printsdk.cmd.PrintCmd;
import com.printsdk.usbsdk.UsbDriver;
import com.vita.MainActivity;
import com.vita.R;
import com.vita.WebAppInterface;
import com.vita.util.CommUtil;
import com.vita.util.Helper;
import com.vita.util.PrintTemplate;
import com.vita.util.PrintTplParse;
import com.vita.util.QrCodeUtil;
import com.vita.util.T;
import com.vita.util.Utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Print {

    UsbDriver mUsbDriver = MainActivity.mUsbDriver;
    String iline = "4";

    public void print(final WebAppInterface appInterface, final String uuid,final JSONObject json) {
        MainActivity.printConnStatus();
        printTikcet(json);

        Helper.setStore(appInterface.getmContext(), uuid, "");
    }

    public void printCoupon(final WebAppInterface appInterface, final String uuid,final JSONArray json) {
        MainActivity.printConnStatus();
        printCoupon(json);

        Helper.setStore(appInterface.getmContext(), uuid, "");
    }


    public void printTpl(final WebAppInterface appInterface, final String uuid, final int count, final JSONObject json,
            final String tplname) {
        Map<String, Object> appInfo = Helper.getConfigAppInfo(appInterface.getmContext());
        final int paper = Helper.toInt(appInfo.get("print_paper"), 58);
        final ServiceManagersHelper mHelper = new ServiceManagersHelper(appInterface.getmContext());
        INewlandManagerCallback.INewlandPrinterCallback callback = new INewlandManagerCallback.INewlandPrinterCallback() {
            @Override
            public void onSuccess(INewlandPrinterManager manager, CapabilityProvider provider) {
                if (null == manager) {
                    mHelper.disconnectService(ServiceManagerType.PRINTER);
                    Helper.setStore(appInterface.getmContext(), uuid, "未找到支持的打印服务");
                    return;
                }

                String tplStr = getFromAssets(appInterface.getmContext(), tplname + ".txt");
                ArrayList<PrinterScript> printerScriptList = getPrintScriptListTpl(appInterface, count, json, paper,
                        tplStr);
                // 为null则调默认打印机进行打印
                PrinterIdentify identify = null;
                manager.print(identify, printerScriptList, new INewlandPrintCallback() {
                    @Override
                    public void onSuccess() {
                        mHelper.disconnectService(ServiceManagerType.PRINTER);
                        Helper.setStore(appInterface.getmContext(), uuid, "打印完成");
                    }

                    @Override
                    public void onError(final NewlandError error) {
                        mHelper.disconnectService(ServiceManagerType.PRINTER);
                        Helper.setStore(appInterface.getmContext(), uuid, "打印失败" + ": " + error.getReason());
                    }
                });
            }

            @Override
            public void onError(NewlandError error) {
                mHelper.disconnectService(ServiceManagerType.TRANSACTION);

                Log.d("openapi-printTpl", error.getCode() + ": " + error.getReason());
            }
        };
        mHelper.getPrinterManager(callback);
    }

    public void openCashBox(final WebAppInterface appInterface, final String uuid) {
        byte[] bSend = PrintCmd.SetOpenCashBoxCmd();
        mUsbDriver.write(bSend, bSend.length);
        Helper.setStore(appInterface.getmContext(), uuid, "");
    }

    String getFromAssets(Context context, String fileName) {
        try {
            InputStreamReader inputReader = new InputStreamReader(context.getResources().getAssets().open(fileName));
            BufferedReader bufReader = new BufferedReader(inputReader);
            String line = "";
            StringBuilder result = new StringBuilder();
            while ((line = bufReader.readLine()) != null)
                result.append(line + "\r\n");
            return result.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    private ArrayList<PrinterScript> getPrintScriptList(int count, JSONArray json, int paper) {
        ArrayList<PrinterScript> scriptArrayList = new ArrayList<>();
        PrinterConfig printerConfig = new PrinterConfig();
        if (paper == 58)
            printerConfig.setPaperSize(PaperSize.SIZE_2INCH);
        else if (paper == 80)
            printerConfig.setPaperSize(PaperSize.SIZE_3INCH);
        List<PrintTemplate> list = Helper.jsonToBeanList(PrintTemplate.class, json.toString());
        for (int i = 0; i < count; i++) {
            PrinterScript printerScript = new PrinterScript();
            printerScript.setDefaultConfig(printerConfig);
            int lastY = -1;
            Text text = new Text();
            String tmpStr = "";
            for (PrintTemplate item : list) {
                if (lastY != -1 && item.getY() != lastY) {
                    printerScript.addText(text.setTxt(tmpStr));
                    tmpStr = "";
                    text = new Text();
                }

                if (item.getType() != null && item.getType().equals("qrcode")) {
                    Qrcode qrcode = new Qrcode();
                    qrcode.setAlign(item.getAlign());
                    qrcode.setWidth(item.getWidth());
                    qrcode.setOffset(item.getX());
                    qrcode.setQrcode(item.getText());
                    printerScript.addQrcode(qrcode);
                } else if (item.getType() != null && item.getType().equals("barcode")) {
                    Barcode barcode = new Barcode();
                    barcode.setAlign(item.getAlign());
//                    barcode.setWidth(item.getWidth());
//                    barcode.setHeight(item.getHeight());
                    barcode.setHeight(60);
                    barcode.setWidth(3);
                    barcode.setOffset(item.getX());
                    barcode.setBarcode(item.getText());
                    printerScript.addBarcode(barcode);
                } else {
                    String content = item.getText();
                    if (item.getWidth() > 0) {
                        if (item.getAlign() == Align.RIGHT) {
                            content = PrintTplParse.padLeftWhileDouble(content, item.getWidth(), ' ');
                        } else {
                            content = PrintTplParse.padRightWhileDouble(content, item.getWidth(), ' ');
                        }
                    }

                    text.setChFontSize(FontSize.getFontSize(Helper.toStr(item.getFontsize())));
                    text.setAlign(item.getAlign());
                    if (item.getAlign() == Align.LEFT)
                        text.setOffset(item.getX());
                    tmpStr += content;
                }

                lastY = item.getY();
            }

            printerScript.addFeedLine(5);
            printerScript.addCutPaper(0);

            scriptArrayList.add(printerScript);
        }
        return scriptArrayList;
    }

    private ArrayList<PrinterScript> getPrintScriptListTpl(WebAppInterface appInterface, int count, JSONObject json,
            int paper, String tplStr) {
        ArrayList<PrinterScript> scriptArrayList = new ArrayList<>();
        PrinterConfig printerConfig = new PrinterConfig();
        if (paper == 58)
            printerConfig.setPaperSize(PaperSize.SIZE_2INCH);
        else if (paper == 80)
            printerConfig.setPaperSize(PaperSize.SIZE_3INCH);
        for (int i = 0; i < count; i++) {
            PrinterScript printerScript = new PrinterScript();
            printerScript.setDefaultConfig(printerConfig);
            String printTxt = PrintTplParse.parse(tplStr, json);
            Pattern re = Pattern.compile("\\[(barcode|qrcode|walkpaper|cutpaper|cashbox)(:([^\\[\\]]+?)|)\\]",
                    Pattern.CASE_INSENSITIVE);
            Matcher match = re.matcher(printTxt);
            int startIndex = 0;
            while (match.find()) {
                Texts texts = new Texts();
                texts.setTxt(printTxt.substring(startIndex, match.end()));
                printerScript.addTexts(texts);
                startIndex = match.end() + 1;

                String tag = match.group(1).toLowerCase();
                if ("barcode".equals(tag)) {
                    Barcode barcode = new Barcode();
                    barcode.setHeight(60);
                    barcode.setWidth(3);
                    barcode.setBarcode(match.group(3));
                    printerScript.addBarcode(barcode);
                } else if ("qrcode".equals(tag)) {
                    Qrcode qrcode = new Qrcode();
                    qrcode.setWidth(200);
                    qrcode.setAlign(Align.CENTER);
                    qrcode.setQrcode(match.group(3));
                    printerScript.addQrcode(qrcode);
                } else if ("walkpaper".equals(tag)) {
                    printerScript.addFeedLine(Helper.toInt(match.group(3)));
                } else if ("cutpaper".equals(tag)) {
                    printerScript.addCutPaper(0);
                } else if ("cashbox".equals(tag)) {
                    openCashBox(appInterface, null);
                }
            }

            if (startIndex < printTxt.length()) {
                Texts texts = new Texts();
                texts.setTxt(printTxt.substring(startIndex));
                printerScript.addTexts(texts);
            }

            scriptArrayList.add(printerScript);
        }
        return scriptArrayList;
    }


    private void printTikcet(JSONObject json) {

        mUsbDriver.write(PrintCmd.SetSizetext(1, 1), 1);
        mUsbDriver.write(PrintCmd.SetLinespace(20));
        mUsbDriver.write(PrintCmd.SetAlignment(1));
        mUsbDriver.write(PrintCmd.PrintString(json.getString("shopName"), 0));

        mUsbDriver.write(PrintCmd.SetSizetext(0, 0), 1);
        mUsbDriver.write(PrintCmd.SetAlignment(0));
        mUsbDriver.write(PrintCmd.PrintString("交易号:"+json.getString("orderNo")+"   销售", 0));
        mUsbDriver.write(PrintCmd.PrintString("交易时间:"+json.getString("payTime"), 0));
        mUsbDriver.write(PrintCmd.PrintFeedline(1));
        mUsbDriver.write(PrintCmd.PrintString("-------------------------------------", 0));
        //条形码
        mUsbDriver.write(PrintCmd.PrintFeedline(1));
        mUsbDriver.write(PrintCmd.Print1Dbar(2, 90, 0, 0, 9, json.getString("orderNo") ));

        mUsbDriver.write(PrintCmd.PrintString("-------------------------------------", 0));
        mUsbDriver.write(PrintCmd.PrintString((CommUtil.fillRightString("序号" + "", 5, ' ')
                + CommUtil.fillRightString("编码" + "", 16, ' ')
                + CommUtil.fillRightString("数量" + "", 12, ' ')
                + CommUtil.fillRightString("金额" + "", 5, ' ')), 0));

        JSONArray goodsList = json.getJSONArray("goodsList");
        for (int i = 0; i <goodsList.size() ; i++) {
            JSONObject obj = goodsList.getJSONObject(i);
            mUsbDriver.write(PrintCmd.PrintFeedline(1));
            mUsbDriver.write(PrintCmd.PrintString((CommUtil.fillRightString(""+(i+1), 5, ' ')
                    + CommUtil.fillRightString(obj.getString("wareCode") , 16, ' ')
                    + CommUtil.fillRightString(obj.getString("wareCount") , 12, ' ')
                    + CommUtil.fillRightString(obj.getString("warePrice") , 5, ' ')), 0));
            mUsbDriver.write(PrintCmd.PrintString((CommUtil.fillRightString(obj.getString("name"), 40, ' ')), 0));
            mUsbDriver.write(PrintCmd.PrintFeedline(1));
        }

        mUsbDriver.write(PrintCmd.PrintString("-------------------------------------", 0));

        mUsbDriver.write(PrintCmd.PrintString((CommUtil.fillRightString("实收：" + "", 16, ' ')
                + CommUtil.fillRightString("" + "", 12, ' ')
                + CommUtil.fillRightString(json.getString("sfMoney") + "", 5, ' ')), 0));

        mUsbDriver.write(PrintCmd.PrintString((CommUtil.fillRightString("优惠总计：" + "", 16, ' ')
                + CommUtil.fillRightString("" + "", 12, ' ')
                + CommUtil.fillRightString(json.getString("yhMoney") + "", 5, ' ')), 0));

        mUsbDriver.write(PrintCmd.PrintString((CommUtil.fillRightString("商品总数：" + "", 16, ' ')
                + CommUtil.fillRightString("" + "", 12, ' ')
                + CommUtil.fillRightString(json.getInteger("goodsNum") + "", 5, ' ')), 0));
        mUsbDriver.write(PrintCmd.PrintString("-------------------------------------", 0));
        mUsbDriver.write(PrintCmd.SetAlignment(1));
        mUsbDriver.write(PrintCmd.PrintString("电子发票开具限消费日30天内", 0));
        mUsbDriver.write(PrintCmd.PrintString("此为您获取电子发票唯一凭证", 0));
        mUsbDriver.write(PrintCmd.PrintString("打开微信扫一扫", 0));
        mUsbDriver.write(PrintCmd.PrintString("扫描下方二维码", 0));
        mUsbDriver.write(PrintCmd.PrintString("根据提示开具电子发惠", 0));
        mUsbDriver.write(PrintCmd.PrintFeedline(1));
        mUsbDriver.write(PrintCmd.SetAlignment(1));
        //		二维码
//        String qrcode1 = "1";
//        Bitmap inputBmp1  = QrCodeUtil.getQRcode(qrcode1, 200, 200);
//        if (inputBmp1 == null)
//            return;
//        Bitmap bm = QrCodeUtil.getSinglePic(inputBmp1);
//        int[] data1 = QrCodeUtil.getPixelsByBitmap(bm);
//        mUsbDriver.write(PrintCmd.PrintDiskImagefile(data1, bm.getWidth(), bm.getHeight()));

        mUsbDriver.write(PrintCmd.PrintQrcode("111111", 20, 8,1));

        mUsbDriver.write(PrintCmd.SetAlignment(1));
        mUsbDriver.write(PrintCmd.PrintFeedline(1));
        mUsbDriver.write(PrintCmd.PrintString("2.关注微信公众号'i中百'", 0));
        mUsbDriver.write(PrintCmd.PrintString("点击右下角会员中心-电子发票", 0));
        mUsbDriver.write(PrintCmd.PrintString("查看开票状态", 0));
        mUsbDriver.write(PrintCmd.PrintString("-------------------------------------", 0));

        mUsbDriver.write(PrintCmd.PrintString("多谢惠顾请再光临", 0));
        mUsbDriver.write(PrintCmd.PrintString("如有质量问题此为退货唯一凭证", 0));
        mUsbDriver.write(PrintCmd.PrintString("中百好邦加盟热线:027-85385806", 0));
        
        mUsbDriver.write(PrintCmd.PrintString("中百巢团服务执线:96518", 0));

        mUsbDriver.write(PrintCmd.PrintFeedline(1));
        mUsbDriver.write(PrintCmd.PrintFeedline(1));

        setFeedCut(0, Integer.valueOf(iline));


    }


    private void printCoupon(JSONArray json) {


        for (int i = 0; i <json.size() ; i++) {
            JSONObject obj = json.getJSONObject(i);
            mUsbDriver.write(PrintCmd.SetSizetext(1, 1), 1);
            mUsbDriver.write(PrintCmd.SetAlignment(1));
            mUsbDriver.write(PrintCmd.PrintString(obj.getString("qname"), 0));
            mUsbDriver.write(PrintCmd.PrintFeedline(1));
            mUsbDriver.write(PrintCmd.SetSizetext(0, 0), 1);
            mUsbDriver.write(PrintCmd.PrintQrcode(obj.getString("qbarcode"), 23, 8,1));
            mUsbDriver.write(PrintCmd.PrintString(obj.getString("qbarcode"), 0));
            mUsbDriver.write(PrintCmd.SetAlignment(0));
            mUsbDriver.write(PrintCmd.PrintFeedline(1));
            mUsbDriver.write(PrintCmd.SetSizetext(1, 1), 1);
            mUsbDriver.write(PrintCmd.PrintString("有效期", 0));
            mUsbDriver.write(PrintCmd.PrintFeedline(1));
            mUsbDriver.write(PrintCmd.SetSizetext(0, 0), 1);
            mUsbDriver.write(PrintCmd.PrintString(obj.getString("sdate")+"~"+obj.getString("edate"), 0));
            mUsbDriver.write(PrintCmd.PrintString(obj.getString("qdesc"), 0));
        }
        mUsbDriver.write(PrintCmd.PrintFeedline(1));
        setFeedCut(0, Integer.valueOf(iline));


    }

    // 指定---走纸换行数量、切纸类型
    private void setFeedCut(int iMode, int num) {
//		if(iMode == 0){
//			T.showShort(MainActivity.this, "全切："  + cutter);
//		}else{
//			T.showShort(MainActivity.this, "半切："  + cutter);
//		}
        mUsbDriver.write(PrintCmd.PrintFeedline(num));   // 走纸换行
        mUsbDriver.write(PrintCmd.PrintCutpaper(iMode)); // 切纸类型
        mUsbDriver.write(PrintCmd.SetClean());           // 清除缓存,初始化
    }


}
