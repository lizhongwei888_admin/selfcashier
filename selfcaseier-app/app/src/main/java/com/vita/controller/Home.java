package com.vita.controller;

import android.content.Context;
import android.telephony.TelephonyManager;

import com.alibaba.fastjson.JSONObject;
import com.vita.WebAppInterface;
import com.vita.util.Helper;
import com.vita.util.OkHttpExt;
import com.vita.util.SpeechUtils;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class Home {
    public String getToken(WebAppInterface appInterface) {
        Map<String, Object> appInfo = Helper.getConfigAppInfo(appInterface.getmContext());
        String apiHost = Helper.trimEnd(appInfo.get("apiHost").toString(), '/');
        String appId = appInfo.get("appId").toString();
        String appSecret = appInfo.get("appSecret").toString();
        String res = OkHttpExt.execPost(apiHost + "/auth/getToken", String.format("appid=%s&appsecret=%s", appId, appSecret));
        if (res.equals("")) {
            return "";
        }

        Map<String, Object> resMap = Helper.jsonToMap(res);
        if (resMap.get("code").toString().equals("200")) {
            return resMap.get("token").toString();
        }

        return "";
    }

    public Map<String, Object> getAppInfo(WebAppInterface appInterface) {
        Map<String, Object> appInfo = Helper.getConfigAppInfo(appInterface.getmContext());
     /*   try {
            appInfo.put("id", getDeviceId(appInterface));
            String deviceId = appInfo.get("id").toString();
            String[] orginStrs = decrypt(appInfo.get("code").toString(), "vita2020to9999..").split("===");
            if (orginStrs.length == 2 && orginStrs[0].equals(deviceId)) {
                Date expiryDate = Helper.toDate(orginStrs[1], "yyyy-MM-dd");
                if (expiryDate.getTime() >= new Date().getTime()) {
                    if (expiryDate.getTime() - new Date().getTime() < 10 * 24 * 60 * 60 * 1000) {
                        appInfo.put("checkStatus", 2);
                    } else {
                        appInfo.put("checkStatus", 1);
                    }
                }
            } else {
                appInfo.put("checkStatus", 0);
            }
        } catch (Exception e) {
            appInfo.put("checkStatus", 0);
        }*/
        appInfo.put("checkStatus", 1);
        return appInfo;
    }

    public int saveConfig(WebAppInterface appInterface, JSONObject appInfo) {
        try {
            String deviceId = getDeviceId(appInterface);
            Helper.setConfigAppInfo(appInterface.getmContext(), appInfo);
            return 1;
        /*    String[] orginStrs = decrypt(appInfo.get("code").toString(), "vita2020to9999..").split("===");
            if (orginStrs.length == 2 && orginStrs[0].equals(deviceId)) {
                Date expiryDate = Helper.toDate(orginStrs[1], "yyyy-MM-dd");
                if (expiryDate.getTime() >= new Date().getTime()) {
                    Helper.setConfigAppInfo(appInterface.getmContext(), appInfo);
                    if (expiryDate.getTime() - new Date().getTime() < 10 * 24 * 60 * 60 * 1000) {
                        return 2;
                    }
                    return 1;
                }
            }*/
        } catch (Exception e) {
        }

        return 0;
    }

    public Map<String, Object> getConfig(WebAppInterface appInterface) {
        Map<String, Object> appInfo = Helper.getConfigAppInfo(appInterface.getmContext());
        appInfo.put("id", getDeviceId(appInterface));
        return appInfo;
    }

    public int saveHost(WebAppInterface appInterface, String host) {
        Helper.setConfigAppInfo(appInterface.getmContext(), "apiHost", host);
        return 1;
    }

    public boolean verifyPassword(WebAppInterface appInterface, String password) {
        return password.equals(Helper.getConfigAppInfoStringValue(appInterface.getmContext(), "pw"));
    }

    public int saveLoginInfo(WebAppInterface appInterface, String account, String password, String merchant_id, String terminal_id, String poskey, String usbAddress) {
        Helper.setStore(appInterface.getmContext(), "login_account", account);
        Helper.setStore(appInterface.getmContext(), "login_password", password);
        Helper.setStore(appInterface.getmContext(), "merchant_id", merchant_id);
        Helper.setStore(appInterface.getmContext(), "terminal_id", terminal_id);
        Helper.setStore(appInterface.getmContext(), "poskey", poskey);
        if (!usbAddress.trim().equals(""))
            Helper.setStore(appInterface.getmContext(), "usbAddress", usbAddress);
        return 1;
    }

    public Map<String, String> getLoginInfo(WebAppInterface appInterface) {
        Map<String, String> map = new HashMap<>();
        map.put("account", Helper.getStore(appInterface.getmContext(), "login_account", ""));
        map.put("password", Helper.getStore(appInterface.getmContext(), "login_password", ""));
        map.put("merchant_id", Helper.getStore(appInterface.getmContext(), "merchant_id", ""));
        map.put("terminal_id", Helper.getStore(appInterface.getmContext(), "terminal_id", ""));
        map.put("poskey", Helper.getStore(appInterface.getmContext(), "poskey", ""));
        map.put("usbAddress", Helper.getStore(appInterface.getmContext(), "usbAddress", ""));
        return map;
    }

    public String getDeviceId(WebAppInterface appInterface) {
        try {
            TelephonyManager mTelephonyMgr = (TelephonyManager) appInterface.getmContext().getSystemService(Context.TELEPHONY_SERVICE);
            String macSerial = mTelephonyMgr.getDeviceId();
            if (macSerial != null && !macSerial.equals("")) {
                return macSerial;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return getMacString(appInterface);
    }

    String getMacString(WebAppInterface appInterface) {
        String str = "";
        try {
            Process pp = Runtime.getRuntime().exec("cat /sys/class/net/wlan0/address");
            InputStreamReader ir = new InputStreamReader(pp.getInputStream());
            LineNumberReader input = new LineNumberReader(ir);

            for (; null != str; ) {
                str = input.readLine();
                if (str != null) {
                    str = str.trim();
                    break;
                }
            }
        } catch (IOException ex) {
        }
        if (str == null) {
            String devideId = Helper.getStore(appInterface.getmContext(), "deviceId", "");
            if (devideId.equals("")) {
                SecureRandom rd = new SecureRandom();
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < 3; i++) {
                    sb.append(rd.nextInt(10));
                }
                sb.append(new Date().getTime());
                devideId = sb.toString();
                Helper.setStore(appInterface.getmContext(), "deviceId", devideId);
                return sb.toString();
            } else {
                return devideId;
            }
        }

        String[] tmp = str.split(":");
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < tmp.length; i++) {
            sb.append(Integer.parseInt(tmp[i], 16));
        }
        return sb.toString();
    }

    String decrypt(String content, String key) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(autoFillKey(key), "AES"));
        byte[] bytes = hexToBytes(content);
        bytes = cipher.doFinal(bytes);
        return new String(bytes, StandardCharsets.UTF_8);
    }

    byte[] autoFillKey(String key) throws Exception {
        byte[] keyArray = (key + "0000000000000000").getBytes(StandardCharsets.UTF_8);
        byte[] result = new byte[16];
        System.arraycopy(keyArray, 0, result, 0, 16);
        return result;
    }

    byte[] hexToBytes(String hexStr) {
        byte[] result = new byte[hexStr.length() / 2];
        for (int i = 0; i < hexStr.length() / 2; i++) {
            int high = Integer.parseInt(hexStr.substring(i * 2, i * 2 + 1), 16);
            int low = Integer.parseInt(hexStr.substring(i * 2 + 1, i * 2 + 2), 16);
            result[i] = (byte) (high * 16 + low);
        }
        return result;
    }

    public String speakText(WebAppInterface appInterface,String text) {
        SpeechUtils.getInstance().speakText(text);
        return "result";
    }
}
