package com.vita.controller;

import android.util.Log;

import com.newland.pospp.openapi.manager.INewlandCashDrawerManager;
import com.newland.pospp.openapi.manager.INewlandManagerCallback;
import com.newland.pospp.openapi.manager.INewlandOpenDrawerCallback;
import com.newland.pospp.openapi.manager.INewlandScannerManager;
import com.newland.pospp.openapi.manager.ServiceManagerType;
import com.newland.pospp.openapi.manager.ServiceManagersHelper;
import com.newland.pospp.openapi.model.CapabilityProvider;
import com.newland.pospp.openapi.model.NewlandError;
import com.newland.pospp.openapi.model.ScannerContext;
import com.newland.pospp.openapi.model.ScannerIdentify;
import com.newland.pospp.openapi.services.INewlandScanListener;
import com.newland.pospp.openapi.services.IScanner;
import com.vita.WebAppInterface;
import com.vita.util.Helper;

public class Scan {
    public void code(final WebAppInterface appInterface, final String uuid,final int cameraId) {
        final ServiceManagersHelper mHelper = new ServiceManagersHelper(appInterface.getmContext());
        INewlandManagerCallback.INewlandScannerCallback callback = new
                INewlandManagerCallback.INewlandScannerCallback() {
                    @Override
                    public void onSuccess(INewlandScannerManager manager, CapabilityProvider provider) {
                        if (null == manager) {
                            mHelper.disconnectService(ServiceManagerType.CASH_DRAWER);
                            Helper.setStore(appInterface.getmContext(), uuid, "未找到支持的扫码服务");
                            return;
                        }

                        IScanner scanner = manager.getScanner(cameraId == 0 ? ScannerIdentify.POS_FORWARD : ScannerIdentify.POS_BACKWARD);
                        ScannerContext scannerContext = new ScannerContext();
                        scannerContext.setHasView(true);//是否显示扫码提示页面(或者预览页面)
                        scannerContext.setCameraLight(true);//是否开启照明灯
                        scannerContext.setMultiple(false);//是否连续扫码
                        scannerContext.setFocusLight(true);//是否开启对焦灯
                        scanner.scan(scannerContext, new INewlandScanListener.Stub() {
                            @Override
                            public void onSuccess(String content) {
                                mHelper.disconnectService(ServiceManagerType.CASH_DRAWER);
                                Helper.setStore(appInterface.getmContext(), uuid, content);
                            }

                            @Override
                            public void onError(NewlandError error) {
                                mHelper.disconnectService(ServiceManagerType.CASH_DRAWER);
                                Helper.setStore(appInterface.getmContext(), uuid, "-1");
                            }
                        });
                    }

                    @Override
                    public void onError(NewlandError error) {
                        mHelper.disconnectService(ServiceManagerType.CASH_DRAWER);

                        Log.d("openapi-scan", error.getCode() + ": " + error.getReason());
                    }
                };
        mHelper.getScannerManager(callback);
    }
}
