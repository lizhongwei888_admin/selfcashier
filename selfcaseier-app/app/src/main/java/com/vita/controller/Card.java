package com.vita.controller;

import android.util.Log;

import com.newland.pospp.openapi.manager.INewlandBlockDataCallback;
import com.newland.pospp.openapi.manager.INewlandM1CardReaderManager;
import com.newland.pospp.openapi.manager.INewlandMagCardReaderManager;
import com.newland.pospp.openapi.manager.INewlandManagerCallback;
import com.newland.pospp.openapi.manager.INewlandTrackDataCallback;
import com.newland.pospp.openapi.manager.ServiceManagerType;
import com.newland.pospp.openapi.manager.ServiceManagersHelper;
import com.newland.pospp.openapi.model.CapabilityProvider;
import com.newland.pospp.openapi.model.NewlandError;
import com.vita.WebAppInterface;
import com.vita.util.ByteUtil;
import com.vita.util.Helper;
import com.vita.device.DeviceHelper;
import com.vita.device.MagCardReaderImpl;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class Card {
    public void readMiCard(final WebAppInterface appInterface, final String uuid, final String key) {
        final ServiceManagersHelper mHelper = new ServiceManagersHelper(appInterface.getmContext());
        INewlandManagerCallback.INewlandM1CardReaderCallback callback = new
                INewlandManagerCallback.INewlandM1CardReaderCallback() {
                    @Override
                    public void onSuccess(INewlandM1CardReaderManager manager, CapabilityProvider provider) {
                        if (null == manager) {
                            mHelper.disconnectService(ServiceManagerType.CARD_READER);
                            Helper.setStore(appInterface.getmContext(), uuid, "未找到支持的读卡服务");
                            return;
                        }

                        manager.readDataBlock(8, ByteUtil.hexString2Bytes(key), new INewlandBlockDataCallback() {
                            @Override
                            public void onSuccess(byte[] data) {
                                mHelper.disconnectService(ServiceManagerType.CARD_READER);

                                Map<String, Object> result = new HashMap<>();
                                String code = "";
                                try {
                                    code = new String(data, "utf8").trim();
                                    result.put("err", "");
                                } catch (UnsupportedEncodingException e) {
                                    result.put("err", e.getMessage());
                                }
                                result.put("code", code);
                                Helper.setStore(appInterface.getmContext(), uuid, Helper.objectToJson(result));
                            }

                            @Override
                            public void onError(final NewlandError error) {
                                mHelper.disconnectService(ServiceManagerType.CARD_READER);

                                Map<String, Object> result = new HashMap<>();
                                result.put("err", "读卡失败" + ": " + error.getReason());
                                result.put("code", "");
                                Helper.setStore(appInterface.getmContext(), uuid, Helper.objectToJson(result));
                            }
                        });
                    }

                    @Override
                    public void onError(NewlandError error) {
                        mHelper.disconnectService(ServiceManagerType.CASH_DRAWER);

                        Log.d("openapi-m1card", error.getCode() + ": " + error.getReason());
                    }
                };
        mHelper.getM1CardReaderManager(callback);
    }

    public void readMagCard(final WebAppInterface appInterface, final String uuid) {
        final ServiceManagersHelper mHelper = new ServiceManagersHelper(appInterface.getmContext());
        INewlandManagerCallback.INewlandMagCardReaderCallback callback = new
                INewlandManagerCallback.INewlandMagCardReaderCallback() {
                    @Override
                    public void onSuccess(INewlandMagCardReaderManager manager, CapabilityProvider provider) {
                        if (null == manager) {
                            mHelper.disconnectService(ServiceManagerType.CARD_READER);
                            Helper.setStore(appInterface.getmContext(), uuid, "未找到支持的读卡服务");
                            return;
                        }

                        manager.readTrackData(30000L, new INewlandTrackDataCallback() {
                            @Override
                            public void onSuccess(byte[] bytes, byte[] bytes1, byte[] bytes2) {
                                mHelper.disconnectService(ServiceManagerType.CARD_READER);

                                Map<String, Object> result = new HashMap<>();
                                String code = "";
                                try {
                                    code = new String(bytes1, "utf8").trim();
                                    result.put("err", "");
                                } catch (UnsupportedEncodingException e) {
                                    result.put("err", e.getMessage());
                                }
                                result.put("code", code);
                                Helper.setStore(appInterface.getmContext(), uuid, Helper.objectToJson(result));
                            }

                            @Override
                            public void onError(final NewlandError error) {
                                mHelper.disconnectService(ServiceManagerType.CARD_READER);

                                Map<String, Object> result = new HashMap<>();
                                result.put("err", "读卡失败" + ": " + error.getReason());
                                result.put("code", "");
                                Helper.setStore(appInterface.getmContext(), uuid, Helper.objectToJson(result));
                            }
                        });
                    }

                    @Override
                    public void onError(NewlandError error) {
                        mHelper.disconnectService(ServiceManagerType.CASH_DRAWER);

                        Log.d("openapi-magcard", error.getCode() + ": " + error.getReason());
                    }
                };
        mHelper.getMagCardReaderManager(callback);
    }

    public int stopMagRead(final WebAppInterface appInterface) {
        DeviceHelper.login(appInterface);
        MagCardReaderImpl magCardReader = DeviceHelper.getMagCardReader();
        magCardReader.stopSearch();
        DeviceHelper.logout();
        return 1;
    }
}
