package com.vita.controller.zb;

import android.util.Log;

import androidx.core.util.Consumer;

import com.alibaba.fastjson.JSONObject;
import com.newland.pospp.openapi.manager.INewlandManagerCallback;
import com.newland.pospp.openapi.manager.INewlandTransactionManager;
import com.newland.pospp.openapi.manager.ServiceManagerType;
import com.newland.pospp.openapi.manager.ServiceManagersHelper;
import com.newland.pospp.openapi.model.CapabilityProvider;
import com.newland.pospp.openapi.model.NewlandError;
import com.newland.pospp.openapi.model.Payment;
import com.newland.pospp.openapi.model.PaymentStatus;
import com.newland.pospp.openapi.model.PaymentType;
import com.vita.WebAppInterface;
import com.vita.util.Helper;
import com.vita.util.TradeInfo;

public class Bank {
    public void pay(final WebAppInterface appInterface, final String uuid, final String money, final String extBillNo) {
        payExec(appInterface, uuid, money, extBillNo, PaymentType.CUPS);
    }

    public void codepay(final WebAppInterface appInterface, final String uuid, final String money, final String extBillNo) {
        payExec(appInterface, uuid, money, extBillNo, PaymentType.QRCODE);
    }

    void payExec(final WebAppInterface appInterface, final String uuid, final String money, final String extBillNo, final PaymentType paymentType) {
        final ServiceManagersHelper mHelper = new ServiceManagersHelper(appInterface.getmContext());
        final TradeInfo info = new TradeInfo();
        transaction(appInterface, uuid, mHelper, info, new Consumer<INewlandTransactionManager>() {
            @Override
            public void accept(INewlandTransactionManager manager) {
                Payment payment = new Payment();
                payment.setReferenceId(extBillNo);//参考号 , 不可重复
                payment.setAmount(Helper.toInt(money));
                payment.setPaymentType(paymentType);
                //PaymentType.CUPS，银行卡交易
                //PaymentType.PREORDER_ALIPAY，支付宝预订单交易
                //PaymentType.PREORDER_WECHAT，微信预订单交易
                //PaymentType.PREORDER_UNIONPAY，云闪付预订单
                //PaymentType.QRCODE_ALIPAY，支付宝扫码交易
                //PaymentType.QRCODE_WECHAT，微信扫码交易
                //PaymentType.QRCODE_UNIONPAY，闪付二维码支付
                //PaymentType.QRCODE，聚合扫码
                //payment.setAuthzOnly(true); 设置为true时为预授权交易 , 仅支持银行卡交易
                manager.startPayment(payment, new com.newland.pospp.openapi.manager.INewlandTransactionCallback() {
                    @Override
                    public void onSuccess(Payment payment) {
                        mHelper.disconnectService(ServiceManagerType.TRANSACTION);

                        PaymentStatus status = payment.getStatus();
                        if (status == PaymentStatus.COLLECTED) {
                            info.setCode("00");
                            info.setResult(Helper.objectToJson(payment));
                            info.setMsg("交易成功！");
                        } else {
                            info.setCode("-1");
                            info.setResult(Helper.objectToJson(payment));
                            info.setMsg(status.toString());
                        }
                        Helper.setStore(appInterface.getmContext(), uuid, Helper.objectToJson(info));
                    }

                    @Override
                    public void onError(NewlandError error) {
                        mHelper.disconnectService(ServiceManagerType.TRANSACTION);

                        info.setCode("-1");
                        info.setMsg("交易失败：" + error.getReason());
                        Helper.setStore(appInterface.getmContext(), uuid, Helper.objectToJson(info));
                    }
                });
            }
        });
    }

    public void cancel(final WebAppInterface appInterface, final String uuid, final JSONObject tradeInfo) {
        final ServiceManagersHelper mHelper = new ServiceManagersHelper(appInterface.getmContext());
        final TradeInfo info = new TradeInfo();
        final Payment payment = Helper.jsonToBean(Payment.class, tradeInfo.toJSONString());
        transaction(appInterface, uuid, mHelper, info, new Consumer<INewlandTransactionManager>() {
            @Override
            public void accept(INewlandTransactionManager manager) {
                manager.voidPayment(payment.getTransaction().getVoucherNo(), new com.newland.pospp.openapi.manager.INewlandTransactionCallback() {
                    @Override
                    public void onSuccess(Payment payment) {
                        mHelper.disconnectService(ServiceManagerType.TRANSACTION);

                        PaymentStatus status = payment.getStatus();
                        if (status == PaymentStatus.VOIDED) {
                            info.setCode("00");
                            info.setResult(Helper.objectToJson(payment));
                            info.setMsg("撤销成功！");
                        } else {
                            info.setCode("-1");
                            info.setResult(Helper.objectToJson(payment));
                            info.setMsg(status.toString());
                        }
                        Helper.setStore(appInterface.getmContext(), uuid, Helper.objectToJson(info));
                    }

                    @Override
                    public void onError(NewlandError error) {
                        mHelper.disconnectService(ServiceManagerType.TRANSACTION);

                        info.setCode("-1");
                        info.setMsg("撤销失败：" + error.getReason());
                        Helper.setStore(appInterface.getmContext(), uuid, Helper.objectToJson(info));
                    }
                });
            }
        });
    }

    public void refund(final WebAppInterface appInterface, final String uuid, final JSONObject tradeInfo, final String money) {
        refundExex(appInterface, uuid, tradeInfo, money);
    }

    public void coderefund(final WebAppInterface appInterface, final String uuid, final JSONObject tradeInfo, final String money) {
        refundExex(appInterface, uuid, tradeInfo, money);
    }

    void refundExex(final WebAppInterface appInterface, final String uuid, final JSONObject tradeInfo, final String money) {
        final ServiceManagersHelper mHelper = new ServiceManagersHelper(appInterface.getmContext());
        final TradeInfo info = new TradeInfo();
        final Payment payment = Helper.jsonToBean(Payment.class, tradeInfo.toJSONString());
        transaction(appInterface, uuid, mHelper, info, new Consumer<INewlandTransactionManager>() {
            @Override
            public void accept(INewlandTransactionManager manager) {
                manager.refundPayment(payment.getPaymentType(), payment.getTransaction().getRefNo(), payment.getTransaction().getCreatedAt(), Helper.toInt(money), new com.newland.pospp.openapi.manager.INewlandTransactionCallback() {
                    @Override
                    public void onSuccess(Payment payment) {
                        mHelper.disconnectService(ServiceManagerType.TRANSACTION);

                        PaymentStatus status = payment.getStatus();
                        if (status == PaymentStatus.REFUNDED) {
                            info.setCode("00");
                            info.setResult(Helper.objectToJson(payment));
                            info.setMsg("退款成功！");
                        } else {
                            info.setCode("-1");
                            info.setResult(Helper.objectToJson(payment));
                            info.setMsg(status.toString());
                        }
                        Helper.setStore(appInterface.getmContext(), uuid, Helper.objectToJson(info));
                    }

                    @Override
                    public void onError(NewlandError error) {
                        mHelper.disconnectService(ServiceManagerType.TRANSACTION);

                        info.setCode("-1");
                        info.setMsg("退款失败：" + error.getReason());
                        Helper.setStore(appInterface.getmContext(), uuid, Helper.objectToJson(info));
                    }
                });
            }
        });
    }

    public void query(final WebAppInterface appInterface, final String uuid, final JSONObject tradeInfo) {
        final ServiceManagersHelper mHelper = new ServiceManagersHelper(appInterface.getmContext());
        final TradeInfo info = new TradeInfo();
        final Payment payment = Helper.jsonToBean(Payment.class, tradeInfo.toJSONString());
        transaction(appInterface, uuid, mHelper, info, new Consumer<INewlandTransactionManager>() {
            @Override
            public void accept(INewlandTransactionManager manager) {
                manager.queryPayment(payment.getPaymentType(), payment.getTransaction().getVoucherNo(), new com.newland.pospp.openapi.manager.INewlandTransactionCallback() {
                    @Override
                    public void onSuccess(Payment payment) {
                        mHelper.disconnectService(ServiceManagerType.TRANSACTION);

                        PaymentStatus status = payment.getStatus();
                        if (status == PaymentStatus.QUERIED) {
                            info.setCode("00");
                            info.setResult(Helper.objectToJson(payment));
                            info.setMsg("查询成功！");
                        } else {
                            info.setCode("-1");
                            info.setResult(Helper.objectToJson(payment));
                            info.setMsg(status.toString());
                        }
                        Helper.setStore(appInterface.getmContext(), uuid, Helper.objectToJson(info));
                    }

                    @Override
                    public void onError(NewlandError error) {
                        mHelper.disconnectService(ServiceManagerType.TRANSACTION);

                        info.setCode("-1");
                        info.setMsg("查询失败：" + error.getReason());
                        Helper.setStore(appInterface.getmContext(), uuid, Helper.objectToJson(info));
                    }
                });
            }
        });
    }

    public void transaction(final WebAppInterface appInterface, final String uuid, final ServiceManagersHelper mHelper, final TradeInfo info, final Consumer<INewlandTransactionManager> iNewlandTransactionManagerCallback) {
        INewlandManagerCallback.INewlandTransactionCallback callback = new
                INewlandManagerCallback.INewlandTransactionCallback() {
                    @Override
                    public void onSuccess(INewlandTransactionManager manager, CapabilityProvider provider) {
                        if (null == manager) {
                            mHelper.disconnectService(ServiceManagerType.TRANSACTION);

                            info.setCode("-1");
                            info.setMsg("交易组件异常，请重试！");
                            Helper.setStore(appInterface.getmContext(), uuid, Helper.objectToJson(info));
                            return;
                        }

                        iNewlandTransactionManagerCallback.accept(manager);
                    }

                    @Override
                    public void onError(NewlandError error) {
                        mHelper.disconnectService(ServiceManagerType.TRANSACTION);

                        Log.d("openapi-bank", error.getCode() + ": " + error.getReason());
                    }
                };
        mHelper.getTransactionManager(callback);
    }
}
