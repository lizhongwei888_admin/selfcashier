package com.vita;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.display.DisplayManager;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.speech.tts.TextToSpeech;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.util.Consumer;

import com.printsdk.usbsdk.UsbDriver;
import com.tencent.bugly.beta.Beta;
import com.vita.controller.Print;
import com.vita.util.Helper;
import com.vita.util.NetworkUtil;
import com.vita.util.OkHttpExt;
import com.vita.util.ToastUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

public class MainActivity extends Activity {

  private final static Logger logger = LoggerFactory.getLogger(MainActivity.class);

  public static TextToSpeech textToSpeech; // TTS对象
  public static UsbDriver mUsbDriver;
  public static UsbManager mUsbManager;
  private static final int FILE_SELECT_CODE = 0;
  static UsbDevice mUsbDev1;        //打印机1
  static UsbDevice mUsbDev2;        //打印机2
  static UsbDevice mUsbDev;
  private final static int PID11 = 8211;
  private final static int PID13 = 8213;
  private final static int PID15 = 8215;
  private final static int VENDORID = 1305;
  private int densityValue = 85; // 默认：80,浓度范围：70-200


  private static String[] permissions = new String[] { Manifest.permission.READ_PHONE_STATE,
      Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE };

  private static final String ACTION_USB_PERMISSION = "com.usb.sample.USB_PERMISSION";

  private WebView webView;
  private long exitTime = 0;
  public static Map<String, String> postDatas = new HashMap<>();
  public static Map<String, String> postHeaders = new HashMap<>();
  public static String token;

  public static Consumer<Intent> ActivityResultCallback;
  public static final int REQUEST_CODE = 0x777;


  @AfterPermissionGranted(REQUEST_CODE)
  private void checkPermission() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      if (!Settings.canDrawOverlays(getApplicationContext())) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage("星河自助POS需要开启浮窗权限，请前往设置");
        dialog.setPositiveButton("确定", new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
            intent.setData(Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, REQUEST_CODE);
          }
        });
        dialog.show();
        return;
      }
    }
    if (EasyPermissions.hasPermissions(this, permissions)) {
      startShow();

      long updateTime = 0;
      try {
        updateTime = Long.parseLong(Helper.getConfigAppInfoStringValue(MainActivity.this, "updateTime"));
      } catch (Exception e) {
      }
      if (new Date().getTime() - updateTime > 300000) {
        Helper.setConfigAppInfo(MainActivity.this, "updateTime", new Date().getTime());
        Beta.checkUpgrade();
      }
    } else {
      EasyPermissions.requestPermissions(this, "欢迎使用星河自助POS，我们需要以下权限", REQUEST_CODE, permissions);
    }
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    StringBuilder sb = new StringBuilder();
    boolean hasDeniedPermission = false;
    for (int i = 0; i < grantResults.length; i++) {
      // 获取Denied,有否认的权限就添加
      if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
        sb.append(permissions[i]);
        sb.append("\n");
        hasDeniedPermission = true;
      }
    }

    if (hasDeniedPermission) {
      new AppSettingsDialog.Builder(this).setTitle("权限申请").setRationale("应用程序运行缺少必要的权限，请前往设置页面打开")
          .setPositiveButton("去设置").setNegativeButton("取消").setRequestCode(REQUEST_CODE).build().show();
    }
    EasyPermissions.onRequestPermissionsResult(REQUEST_CODE, permissions, grantResults, this);
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    requestWindowFeature(Window.FEATURE_NO_TITLE);
    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    if(!BuildConfig.App_Keyboard) {
      getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
      getWindow().addFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
    }

    getUsbDriverService();
    printConnStatus();


    textToSpeech = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
      @Override
      public void onInit(int i) {
        if (i == TextToSpeech.SUCCESS) {
          textToSpeech.setLanguage(Locale.CHINA);
          textToSpeech.setPitch(1.0f);// 设置音调，值越大声音越尖（女生），值越小则变成男声,1.0是常规
          textToSpeech.setSpeechRate(1.0f);

        }
        else {
          ToastUtil.show("播报引擎加载失败");
        }
      }
    });


    //屏蔽状态栏下拉
    //android.intent.action.STATUSBAR_DISABLE： 屏蔽
    //android.intent.action.STATUSBAR_ENABLE： 恢复
    Intent intent1 = new Intent(true ?
            "android.intent.action.STATUSBAR_DISABLE":
            "android.intent.action.STATUSBAR_ENABLE");
    sendBroadcast(intent1);

    //屏蔽任务管理器界面（或称最近应用界面，android中长按menu或home键出现的列出运行过的应用程序的那个界面）
    //android.intent.action.RECENT_APP_DISABLE： 屏蔽
    //android.intent.action.RECENT_APP_ENABLE： 恢复
    Intent intent2 = new Intent(true ?
            "android.intent.action.RECENT_APP_DISABLE":
            "android.intent.action.RECENT_APP_ENABLE");
    sendBroadcast(intent2);

    //屏蔽home键
    //home键屏蔽在机器重启后会取消屏蔽。
    //android.intent.action. HOME_KEY_DISABLE： 屏蔽
    //android.intent.action. HOME_KEY_ENABLE： 恢复
    Intent intent3 = new Intent(true ?
            "android.intent.action.HOME_KEY_DISABLE" :
            "android.intent.action.HOME_KEY_ENABLE");
    sendBroadcast(intent3);


    //屏蔽下方虚拟键（P950\P990）
    //虚拟键屏蔽在机器重启后会取消屏蔽。
    //android.intent.action.NAVIGATION_DISABLE： 屏蔽
    //android.intent.action.NAVIGATION_ENABLE： 恢复
    Intent intent4 = new Intent(true ?
            "android.intent.action.NAVIGATION_DISABLE" :
            "android.intent.action.NAVIGATION_ENABLE");
    //sendBroadcast(intent4);

    //屏蔽电源键（电源键屏蔽后，屏幕会常亮，无法休眠）
    //电源键屏蔽在机器重启后会取消屏蔽。
    //android.intent.action.POWER_KEY_DISABLE： 屏蔽
    //android.intent.action.POWER_KEY_ENABLE： 恢复
    Intent intent5 = new Intent(true ?
            "android.intent.action.POWER_KEY_DISABLE" :
            "android.intent.action.POWER_KEY_ENABLE");
    //sendBroadcast(intent5);

    //电源屏蔽广播可以添加extra项ONLY_SHORT_PRESS，配置是否仅仅屏蔽短按power行为（长按power仍然会显示关机菜单）
    Intent intent = new Intent("android.intent.action.POWER_KEY_DISABLE");
    intent.putExtra("ONLY_SHORT_PRESS", true);//是否仅屏蔽短按power行为，默认为false
    //sendBroadcast(intent);


    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      checkPermission();
    } else {
      startShow();
    }
  }

  void startShow() {
    ToastUtil.init(getApplication());

    setContentView(R.layout.activity_main);

    DisplayManager displayManager = (DisplayManager) getSystemService(Context.DISPLAY_SERVICE);
    Display[] displays = displayManager.getDisplays();
    SecondScreen secondScreen = null;
    if (displays.length > 1) {
      Display display = displays[1];
      secondScreen = new SecondScreen(MainActivity.this, display);
      secondScreen.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
      secondScreen.show();
    }

    // 模拟存储数据
    Map<String, Object> appInfo = Helper.getConfigAppInfo(MainActivity.this);
    if (!appInfo.containsKey("apiHost") || appInfo.get("apiHost") == null
        || appInfo.get("apiHost").toString().equals("")) {

      appInfo.put("apiHost", BuildConfig.App_ApiHost);
      appInfo.put("appId", BuildConfig.App_AppId);
      appInfo.put("appSecret", BuildConfig.App_AppSecret);
      appInfo.put("orgID", BuildConfig.App_OrgID);
      appInfo.put("projectId", BuildConfig.App_ProjectId);
      appInfo.put("posnum", BuildConfig.App_Posnum);
      appInfo.put("printNum", BuildConfig.App_PrintNum);
      appInfo.put("syjId", BuildConfig.App_SyjId);
      appInfo.put("pw", BuildConfig.App_PW);
      appInfo.put("USBPrint", BuildConfig.App_USBPrint);
      appInfo.put("Keyboard", BuildConfig.App_Keyboard ? "1":"0");
      appInfo.put("ScanModel", BuildConfig.App_ScanModel);
      appInfo.put("print_paper", "58");
      // flynn设备授权码
      appInfo.put("code", BuildConfig.App_Code);
    }
    Helper.setStore(this, "appInfo", Helper.objectToJson(appInfo));

    webView = (WebView) findViewById(R.id.webView);
    WebSettings webSettings = webView.getSettings();
    webSettings.setLoadWithOverviewMode(true);
    webSettings.setUseWideViewPort(true);
    webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
    webSettings.setJavaScriptEnabled(true);
    webSettings.setAppCacheEnabled(true);
    webSettings.setDomStorageEnabled(true);
    webSettings.setAllowFileAccess(true);
    webSettings.setAllowFileAccessFromFileURLs(true);
    webSettings.setAllowUniversalAccessFromFileURLs(true);
    webView.setBackgroundColor(Color.TRANSPARENT);
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
      webView.setWebContentsDebuggingEnabled(true);
    }
    webView.setWebChromeClient(new WebChromeClient() {
      @Override
      public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
        return super.onJsAlert(view, url, message, result);
      }
    });
    webView.setWebViewClient(new WebViewClient() {
      @Override
      public boolean shouldOverrideUrlLoading(WebView view, String url) {
        view.loadUrl(url);
        return true;
      }

      @Override
      public WebResourceResponse shouldInterceptRequest(WebView view, final WebResourceRequest request) {
        String url = request.getUrl().toString();
        String rid = request.getRequestHeaders().get("ajax_guid");
        if (rid != null) {
          try {
            url = injectUrl(url);
            String postData = getRequestBody(rid);
            Map<String, String> headers = getRequestHeaders(rid);
            byte[] result;
            if (request.getMethod().equals("GET")) {
              result = OkHttpExt.execGetReBody(url, "", headers).bytes();
            } else {
              result = OkHttpExt.execPost(url, postData, headers, "application/json").getBytes();
            }
            return new WebResourceResponse(headers.get("Content-Type"), "utf-8", new ByteArrayInputStream(result));
          } catch (Exception e) {
            logger.error(e.getMessage(), e);
          }
        }
        return super.shouldInterceptRequest(view, request);
      }
    });

    webView.addJavascriptInterface(new WebAppInterface(this, secondScreen, webView), "android");
    if (webView.getUrl() == null) {
      if (isApkInDebug(this)) {
        webView.loadUrl("http://" + BuildConfig.App_HtmlHostIP + ":8080/index.html");
      } else {
        webView.loadUrl("file:///android_asset/index.html");
      }
    }
  }

  public String injectUrl(String url) {
    Map<String, Object> appInfo = Helper.jsonToMap(Helper.getStore(MainActivity.this, "appInfo", "{}"));
    String apiHost = Helper.trimEnd(appInfo.get("apiHost").toString(), '/');
    Pattern pattern = Pattern.compile("(.+)(/api/.+)", Pattern.CASE_INSENSITIVE);
    Matcher m = pattern.matcher(url);
    if (!url.startsWith(apiHost) && m.find()) {
      url = url.replace(m.group(), apiHost + m.group(2));
    }
    return url.replace("/api","");
  }

  public String getRequestBody(String rid) {
    if (postDatas.containsKey(rid)) {
      String data = postDatas.get(rid);
      postDatas.remove(rid);
      return data;
    } else {
      return "";
    }
  }

  public Map<String, String> getRequestHeaders(String rid) {
    if (postHeaders.containsKey(rid)) {
      String headers = postHeaders.get(rid);
      postHeaders.remove(rid);
      return Helper.jsonToMap(headers);
    } else {
      return new HashMap<>();
    }
  }

  @Override
  public boolean onKeyDown(int keyCode, KeyEvent event) {
    if (keyCode == KeyEvent.KEYCODE_BACK) {
      if (webView.getUrl().endsWith("/login") || webView.getUrl().endsWith("%2Flogin")
          || webView.getUrl().endsWith("/counter") || webView.getUrl().endsWith("%2Fcounter")
          || webView.getUrl().contains("login?redirect=")) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("确认退出？");
        dialog.setIcon(android.R.drawable.ic_dialog_info);
        dialog.setPositiveButton("确定", new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            System.exit(0);
          }
        });

        dialog.setNegativeButton("取消", new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            // 点击“返回”后的操作,这里不设置没有任何操作
          }
        });

        dialog.show();
      } else {
        webView.goBack();
      }
    }
    return super.onKeyDown(keyCode, event);
  }

  public static boolean isApkInDebug(Context context) {
    try {
      ApplicationInfo info = context.getApplicationInfo();
      return (info.flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0;
    } catch (Exception e) {
      return false;
    }
  }

  @Override
  protected void onResume() {
    super.onResume();
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();

    if(textToSpeech != null) {

      textToSpeech.stop();
      textToSpeech.shutdown();
    }
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
    if (requestCode == REQUEST_CODE) {
      checkPermission();
      return;
    }

    if (ActivityResultCallback != null) {
      ActivityResultCallback.accept(data);
    }
  }

  /*
   *  BroadcastReceiver when insert/remove the device USB plug into/from a USB port
   *  创建一个广播接收器接收USB插拔信息：当插入USB插头插到一个USB端口，或从一个USB端口，移除装置的USB插头
   */
  BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
    public void onReceive(Context context, Intent intent) {
      String action = intent.getAction();
      if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {
        UsbDevice device = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
        if ((device.getProductId() == 8211 && device.getVendorId() == 1305)
                || (device.getProductId() == 8213 && device.getVendorId() == 1305)) {
          mUsbDriver.closeUsbDevice(device);
        }
      } else if (ACTION_USB_PERMISSION.equals(action)) synchronized (this) {
        UsbDevice device = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
        if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
          if ((device.getProductId() == 8211 && device.getVendorId() == 1305)
                  || (device.getProductId() == 8213 && device.getVendorId() == 1305)) {
            //赋权限以后的操作
          }
        } else {

        }
      }

    }
  };

  // Get UsbDriver(UsbManager) service
  private void getUsbDriverService() {
    mUsbManager = (UsbManager) this.getSystemService(Context.USB_SERVICE);
    mUsbDriver = new UsbDriver(mUsbManager, this);
    PendingIntent permissionIntent1 = PendingIntent.getBroadcast(this, 0,
            new Intent(ACTION_USB_PERMISSION), 0);
    mUsbDriver.setPermissionIntent(permissionIntent1);
  }


  public static boolean printConnStatus() {
    boolean blnRtn = false;
    try {
      if (!mUsbDriver.isConnected()) {
        // USB线已经连接
        for (UsbDevice device : mUsbManager.getDeviceList().values()) {
          if ((device.getProductId() == PID11 && device.getVendorId() == VENDORID)
                  || (device.getProductId() == PID13 && device.getVendorId() == VENDORID)
                  || (device.getProductId() == PID15 && device.getVendorId() == VENDORID)) {

            blnRtn = mUsbDriver.usbAttached(device);
            if (blnRtn == false) {
              break;
            }
            blnRtn = mUsbDriver.openUsbDevice(device);

            // 打开设备
            if (blnRtn) {
              if (device.getProductId() == PID11) {
                mUsbDev1 = device;
                mUsbDev = mUsbDev1;
              } else {
                mUsbDev2 = device;
                mUsbDev = mUsbDev2;
              }
              break;
            } else {
              break;
            }
          }
        }
      } else {
        blnRtn = true;
      }
    } catch (Exception e) {
    }
    return blnRtn;
  }


  /**
   * * 启动时判断用户网络是否可用，如不可用，则弹窗让用户设置网络，
   * * 如网络可用，则检查是否有更新
   * */
  @Override
  protected void onStart() {
    if (!NetworkUtil.isNetworkAvailable(this)) {
      //网络不可用时弹出设置窗口
      showSetNetworkUI(this);
    } else {
      //如果网络可用就检查更新，然后跳到主界面。
    }
    super.onStart();
  }

  //当网络不可用时需要弹窗，写一个弹窗的方法。

  /** 弹窗、打开设置网络界面*/
  public void showSetNetworkUI(final Context context) {
    // 提示对话框
    AlertDialog.Builder builder = new AlertDialog.Builder(context);
    builder.setTitle("网络设置提示").setMessage("网络连接不可用,是否进行设置?")
            .setPositiveButton("设置", new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                Intent intent = null;
                // 判断手机系统的版本 即API大于10 就是3.0或以上版本
                if (android.os.Build.VERSION.SDK_INT > 10) {
                  intent = new Intent( android.provider.Settings.ACTION_WIFI_SETTINGS);
                } else {
                  intent = new Intent();
                  ComponentName component = new ComponentName( "com.android.settings",
                          "com.android.settings.WirelessSettings");
                  intent.setComponent(component);
                  intent.setAction("android.intent.action.VIEW");
                } context.startActivity(intent);
              } }) .setNegativeButton("重新连接", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        showSetNetworkUI(context);
      } }).show();
  }

}
