package com.vita.util;

public class TradeInfo {
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    //返回码，00：成功，数字非00：失败
    private String code;

    //错误信息
    private String msg;

    //交易结果
    private String result;

}
