package com.vita.util;

import com.newland.pospp.openapi.model.printer.Align;

public class PrintTemplate {
    private int x = 0;

    private int y = 0;

    private int width = 0;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    private int height = 0;

    private String text = "";

    private String align = "left";

    private int fontsize = 1;

    private String type;

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getFontsize() {
        return fontsize;
    }

    public void setFontsize(int fontsize) {
        this.fontsize = fontsize;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setAlign(String align) {
        this.align = align;
    }

    public Align getAlign() {
        switch (align) {
            case "left":
                return Align.LEFT;
            case "right":
                return Align.RIGHT;
            case "middle":
                return Align.CENTER;
        }
        return Align.LEFT;
    }
}
