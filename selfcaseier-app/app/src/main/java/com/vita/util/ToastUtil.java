package com.vita.util;

import android.app.Application;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import java.lang.ref.WeakReference;


public class ToastUtil {
    private static Toast toast = null;
    private static Handler handler = new Handler(Looper.getMainLooper());
    private static WeakReference<Application> app = null;

    public static void init(Application app){
        ToastUtil.app = new WeakReference<Application>(app);
    }

    public static void show(final String text){
        if(app == null || app.get() == null){
            Log.e("sys", ToastUtil.class.getSimpleName() + "未初始化");
            return;
        }
        if(handler == null) {
            handler = new Handler(Looper.getMainLooper());
        }
        handler.post(new Runnable() {
            @Override
            public void run() {
                if(toast == null) {
                    toast = Toast.makeText(app.get(), text, Toast.LENGTH_SHORT);
                } else {
                    toast.setText(text);
                    toast.setDuration(Toast.LENGTH_SHORT);
                }
                toast.show();
            }
        });
    }
}
