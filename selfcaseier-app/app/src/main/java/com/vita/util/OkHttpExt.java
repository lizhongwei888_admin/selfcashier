package com.vita.util;

import okhttp3.*;
import okio.BufferedSink;

import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class OkHttpExt {
    public static Long connectTimeout = 30L;
    public static Long readTimeout = 30L;
    public static Long writeTimeout = 30L;
    public static int maxRequests = 2000;
    public static int maxRequestsPerHost = 300;

    public static String execGet(String url) {
        return execGet(url, "", null);
    }

    public static ResponseBody execGetReBody(String url) throws IOException {
        return execGetReBody(url, "", null);
    }

    public static String execGet(String url, String queryString) {
        return execGet(url, queryString, null);
    }

    public static ResponseBody execGetReBody(String url, String queryString) throws IOException {
        return execGetReBody(url, queryString, null);
    }

    public static String execGet(String url, Map<String, String> params) {
        return execGet(url, serialize(params), null);
    }

    public static ResponseBody execGetReBody(String url, Map<String, String> params) throws IOException {
        return execGetReBody(url, serialize(params), null);
    }

    public static String execGet(String url, Map<String, String> params, Map<String, String> headersParams) {
        return execGet(url, serialize(params), headersParams);
    }

    public static ResponseBody execGetReBody(String url, Map<String, String> params, Map<String, String> headersParams)
            throws IOException {
        return execGetReBody(url, serialize(params), headersParams);
    }

    public static String execGet(String url, String queryString, Map<String, String> headersParams) {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(connectTimeout, TimeUnit.SECONDS)
                .readTimeout(readTimeout, TimeUnit.SECONDS)
                .writeTimeout(writeTimeout, TimeUnit.SECONDS).build();
        okHttpClient.dispatcher().setMaxRequests(maxRequests);
        okHttpClient.dispatcher().setMaxRequestsPerHost(maxRequestsPerHost);
        Request.Builder builder = new Request.Builder().url(url + queryString);
        Headers headers = getHeaders(headersParams);
        if (headers != null)
            builder.headers(headers);
        Request request = builder.build();
        return execute(okHttpClient, request);
    }

    public static ResponseBody execGetReBody(String url, String queryString, Map<String, String> headersParams)
            throws IOException {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(connectTimeout, TimeUnit.SECONDS)
                .readTimeout(readTimeout, TimeUnit.SECONDS)
                .writeTimeout(writeTimeout, TimeUnit.SECONDS).build();
        okHttpClient.dispatcher().setMaxRequests(maxRequests);
        okHttpClient.dispatcher().setMaxRequestsPerHost(maxRequestsPerHost);
        Request.Builder builder = new Request.Builder().url(url + queryString);
        Headers headers = getHeaders(headersParams);
        if (headers != null)
            builder.headers(headers);
        Request request = builder.build();
        return executeReBody(okHttpClient, request);
    }

    public static String execPost(String url) {
        return execPost(url, "", null, "application/x-www-form-urlencoded");
    }

    public static String execPost(String url, String params) {
        return execPost(url, params, null, "application/x-www-form-urlencoded");
    }

    public static String execPost(String url, String params, Map<String, String> headersParams) {
        return execPost(url, params, headersParams, "application/x-www-form-urlencoded");
    }

    public static String execPost(String url, String params, String mediaType) {
        return execPost(url, params, null, mediaType);
    }

    public static String execPostMultipart(String url, Map<String, Object> params) {
        return execPostMultipart(url, params, null);
    }

    public static String execPost(String url, Map<String, String> params) {
        return execPost(url, params, null);
    }

    public static String execPostMultipart(String url, Map<String, Object> params, Map<String, String> headersParams) {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(connectTimeout, TimeUnit.SECONDS)
                .readTimeout(readTimeout, TimeUnit.SECONDS)
                .writeTimeout(writeTimeout, TimeUnit.SECONDS).build();
        okHttpClient.dispatcher().setMaxRequests(maxRequests);
        okHttpClient.dispatcher().setMaxRequestsPerHost(maxRequestsPerHost);
        MultipartBody formBody = getMultipartBody(params);
        Request.Builder builder = new Request.Builder().url(url).post(formBody);
        Headers headers = getHeaders(headersParams);
        if (headers != null)
            builder.headers(headers);
        Request request = builder.build();
        return execute(okHttpClient, request);
    }

    public static String execPost(String url, Map<String, String> params, Map<String, String> headersParams) {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(connectTimeout, TimeUnit.SECONDS)
                .readTimeout(readTimeout, TimeUnit.SECONDS)
                .writeTimeout(writeTimeout, TimeUnit.SECONDS).build();
        okHttpClient.dispatcher().setMaxRequests(maxRequests);
        okHttpClient.dispatcher().setMaxRequestsPerHost(maxRequestsPerHost);
        FormBody formBody = getFormBody(params);
        Request.Builder builder = new Request.Builder().url(url).post(formBody);
        Headers headers = getHeaders(headersParams);
        if (headers != null)
            builder.headers(headers);
        Request request = builder.build();
        return execute(okHttpClient, request);
    }

    public static String execPost(String url, String params, Map<String, String> headersParams, String mediaType) {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(connectTimeout, TimeUnit.SECONDS)
                .readTimeout(readTimeout, TimeUnit.SECONDS)
                .writeTimeout(writeTimeout, TimeUnit.SECONDS).build();
        okHttpClient.dispatcher().setMaxRequests(maxRequests);
        okHttpClient.dispatcher().setMaxRequestsPerHost(maxRequestsPerHost);
        RequestBody requestBody = RequestBody.create(params, MediaType.parse(mediaType));
        Request.Builder builder = new Request.Builder().url(url).post(requestBody);
        Headers headers = getHeaders(headersParams);
        if (headers != null)
            builder.headers(headers);
        Request request = builder.build();
        return execute(okHttpClient, request);
    }

    public static String execute(OkHttpClient okHttpClient, Request request) {
        String result = "";
        try {
            result = executeReBody(okHttpClient, request).string();
        } catch (IOException e) {
        }

        return result;
    }

    public static ResponseBody executeReBody(OkHttpClient okHttpClient, Request request) throws IOException {
        Call call = okHttpClient.newCall(request);
        Response response = call.execute();
        return response.body();
    }

    public static String execStream(String url, final byte[] bytes, final Map<String, String> headersParams) {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(connectTimeout, TimeUnit.SECONDS)
                .readTimeout(readTimeout, TimeUnit.SECONDS)
                .writeTimeout(writeTimeout, TimeUnit.SECONDS).build();
        okHttpClient.dispatcher().setMaxRequests(maxRequests);
        okHttpClient.dispatcher().setMaxRequestsPerHost(maxRequestsPerHost);
        RequestBody requestBody = new RequestBody() {
            @Override
            public MediaType contentType() {
                return MediaType.parse(headersParams.get("Content-Type"));
            }

            @Override
            public void writeTo(BufferedSink sink) throws IOException {
                DataOutputStream out = new DataOutputStream(sink.outputStream());
                out.write(bytes);
            }
        };
        Request.Builder builder = new Request.Builder().url(url).post(requestBody);
        Headers headers = getHeaders(headersParams);
        if (headers != null)
            builder.headers(headers);
        builder.build();
        Request request = builder.build();
        return execute(okHttpClient, request);
    }

    public static Headers getHeaders(Map<String, String> headersParams) {
        if (headersParams != null) {
            Headers.Builder headersBuilder = new Headers.Builder();
            Iterator<?> iterator = headersParams.keySet().iterator();
            String key = "";
            while (iterator.hasNext()) {
                key = iterator.next().toString();
                headersBuilder.add(key, Helper.toStr(headersParams.get(key)));
            }
            return headersBuilder.build();
        }

        return null;
    }

    public static FormBody getFormBody(Map<String, String> params) {
        FormBody.Builder formBodyBuilder = new FormBody.Builder();
        if (params != null) {
            Iterator<String> iterator = params.keySet().iterator();
            String key = "";
            while (iterator.hasNext()) {
                key = iterator.next().toString();
                formBodyBuilder.add(key, Helper.toStr(params.get(key)));
            }
        }

        return formBodyBuilder.build();
    }

    public static MultipartBody getMultipartBody(Map<String, Object> params) {
        MultipartBody.Builder formBodyBuilder = new MultipartBody.Builder();
        formBodyBuilder.setType(MultipartBody.FORM);
        if (params != null) {
            Iterator<String> iterator = params.keySet().iterator();
            String key = "";
            while (iterator.hasNext()) {
                key = iterator.next().toString();
                if (params.get(key) instanceof File) {
                    File file = (File) params.get(key);
                    String fileName = file.getName();
                    formBodyBuilder.addPart(
                            Headers.of("Content-Disposition",
                                    "form-data; name=\"file\"; filename=\"" + fileName + "\""),
                            RequestBody.create(file, MediaType.parse(Helper.guessContentType(fileName))));
                } else {
                    formBodyBuilder.addFormDataPart(key, Helper.toStr(params.get(key)));
                }
            }
        }

        return formBodyBuilder.build();
    }

    public static String serialize(Map<String, String> params) {
        StringBuilder result = new StringBuilder();
        if (params != null) {
            Iterator<String> iterator = params.keySet().iterator();
            String key = "";
            while (iterator.hasNext()) {
                key = iterator.next().toString();
                if (result.length() == 0)
                    result.append("?");
                else
                    result.append("&");
                try {
                    result.append(URLEncoder.encode(key, "utf-8") + "="
                            + URLEncoder.encode(Helper.toStr(params.get(key)), "utf-8"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return result.toString();
    }

    public static Map<String, String> serializeMap(String params) {
        if (params != null && !params.equals("")) {
            Map<String, String> map = new HashMap<String, String>();
            String[] tmp = params.split("&");
            for (int i = 0; i < tmp.length; i++) {
                String[] t = tmp[i].split("=");
                if (t.length > 1)
                    map.put(t[0], tmp[i].substring(t[0].length() + 1, tmp[i].length()));
            }

            return map;
        }

        return null;
    }
}
