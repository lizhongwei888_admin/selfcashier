package com.vita.util;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PrintTplParse {
    public static String parse(String tplStr, JSONObject data) {
        Pattern re = Pattern.compile("([\\s]+|)\\<(.+?)\\>([^\\<]+?)\\</\\>([\\s]+|)");
        Pattern subRe = Pattern.compile("\\{(.+?)\\}");
        Matcher match = re.matcher(tplStr);
        int lastFindIndex = 0;
        while (match.find()) {
            String tableName = match.group(2);
            String tableStr = match.group(3);
            if (!data.containsKey(tableName)) {
                tplStr = tplStr.replace(match.group(), "");
                continue;
            }
            List<JSONObject> subData = JSONArray.parseArray(data.getString(tableName), JSONObject.class);
            String originHeadStr;
            String originTableStr = tableStr;
            Matcher m = Pattern.compile("([^\\|]+?[^\\\\])\\|([\\s]+)").matcher(tableStr);
            if (m.find()) {
                originHeadStr = m.group(1) + m.group(2);
                originTableStr = tableStr.substring(m.group().length());
            } else {
                originHeadStr = "\r\n";
                originTableStr = Helper.trimStart(originTableStr, ' ');
            }

            if (match.end() == lastFindIndex) {
                originHeadStr = Helper.trimStart(originHeadStr, ' ');
            }
            String newTableStr = "";
            for (JSONObject subItem : subData) {
                String tmpTableStr = originTableStr;
                Matcher subMatch = subRe.matcher(tmpTableStr);
                while (subMatch.find()) {
                    String fieldName = subMatch.group(1);
                    tmpTableStr = tmpTableStr.replace(subMatch.group(), getFieldValue(fieldName, subItem));
                }

                newTableStr += tmpTableStr;
            }
            if ("".equals(newTableStr))
                tplStr = tplStr.replace(match.group(), "");
            else
                tplStr = tplStr.replace(match.group(), originHeadStr + newTableStr);

            lastFindIndex = match.end();
        }

        Matcher thirdMatch = subRe.matcher(tplStr);
        while (thirdMatch.find()) {
            String fieldName = thirdMatch.group(1);
            tplStr = tplStr.replace(thirdMatch.group(), getFieldValue(fieldName, data));
        }

        return tplStr;
    }

    static String getFieldValue(String fieldName, JSONObject data) {
        String[] fieldNames = fieldName.split(":");
        if (fieldNames.length == 1) {
            return Helper.toStr(Helper.getFieldValue(data, fieldNames[0]));
        }

        if (fieldNames.length == 2) {
            if (!data.containsKey(fieldNames[0])) {
                return "";
            }

            String value = Helper.toStr(Helper.getFieldValue(data, fieldNames[0]));
            if ("barcode".equals(fieldNames[1]) || "qrcode".equals(fieldNames[1]))
                return "[" + fieldNames[1] + ":" + value + "]";

            if (fieldNames[1].length() == 0) {
                return value;
            }

            String align = fieldNames[1].substring(0, 1).toLowerCase();
            String lenStr = fieldNames[1];
            if ( "l".equals(align) || "r".equals(align)) {
                lenStr = lenStr.substring(1);
            } else {
                align = "l";
            }
            int len = Helper.toInt(lenStr);
            if ("r".equals(align))
                return padLeftWhileDouble(value, len, ' ');
            else
                return padRightWhileDouble(value, len, ' ');
        }

        return "";
    }

    public static String padLeftWhileDouble(String input, int length, char paddingChar) {
        int singleLength = getSingleLength(input);
        return StringUtil.append(true, input, paddingChar, length - singleLength + input.length());
    }

    static int getSingleLength(String input) {
        if (input == null) {
            return 0;
        }
        return input.replaceAll("[^\\x00-\\xff]", "aa").length();
    }

    public static String padRightWhileDouble(String input, int length, char paddingChar) {
        int singleLength = getSingleLength(input);
        return StringUtil.append(false, input, paddingChar, length - singleLength + input.length());
    }
}
