package com.vita.util;

import com.vita.WebAppInterface;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class LogHelper {
    private final static Logger logger = LoggerFactory.getLogger(LogHelper.class);

    public enum InfoLevel {
        INFO, WARN, ERROR
    }

    public static void sendLog(WebAppInterface appInterface, String infoType, InfoLevel infoLvl, String params, String result) {
        try {
            Map<String, Object> appInfo = Helper.jsonToMap(Helper.getStore(appInterface.getmContext(), "appInfo", "{}"));
            String apiHost = Helper.trimEnd(appInfo.get("apiHost").toString(), '/');
            final Map<String, String> data = new HashMap<>();
            data.put("infoType", infoType);
            data.put("infoLvl", infoLvl.name());
            data.put("params", params);
            data.put("result", result);

            OkHttpClient okHttpClient = new OkHttpClient();
            FormBody.Builder formBodyBuilder = new FormBody.Builder();
            Iterator<String> iterator = data.keySet().iterator();
            String key = "";
            while (iterator.hasNext()) {
                key = iterator.next();
                formBodyBuilder.add(key, data.get(key));
            }
            FormBody formBody = formBodyBuilder.build();
            Request.Builder builder = new Request.Builder().url(apiHost + "/api/auth/sendLog").post(formBody);
            Request request = builder.build();
            Call call = okHttpClient.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    logger.error("POS日志上传请求：" + Helper.objectToJson(data) + "，返回：" + e.getMessage(), e);
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    logger.info("POS日志上传请求：{}，返回：{}", Helper.objectToJson(data), response.body().string());
                }
            });
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
}
