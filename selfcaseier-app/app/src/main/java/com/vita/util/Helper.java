package com.vita.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.alibaba.fastjson.parser.ParserConfig;
import com.alibaba.fastjson.parser.deserializer.JavaBeanDeserializer;
import com.alibaba.fastjson.serializer.SerializerFeature;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Helper {

    public static String toStr(Object str) {
        return toStr(str, "");
    }

    public static String toStr(Object str, String defaultValue) {
        if (str == null) {
            return defaultValue;
        }

        String result;
        if (str instanceof Map || isJavaBean(str.getClass())) {
            result = objectToJson(str);
        } else if (str instanceof List) {
            result = listToJson((List<?>) str);
        } else if (str instanceof byte[]) {
            result = new String((byte[]) str);
        } else if (str instanceof Bundle) {
            Bundle bundle = (Bundle) str;
            Set<String> keys = bundle.keySet();
            JSONObject obj = new JSONObject();
            for (String key : keys) {
                obj.put(key, bundle.get(key));
            }
            result = obj.toJSONString();
        } else {
            result = String.valueOf(str);
        }
        return result;
    }

    public static int toInt(Object str) {
        return toInt(str, 0);
    }

    public static int toInt(Object str, int defaultValue) {
        String tmp = String.valueOf(str);
        if (tmp.equals("null"))
            return defaultValue;
        else {
            try {
                return Integer.parseInt(tmp);
            } catch (Exception e) {
                return defaultValue;
            }
        }
    }

    public static float toFloat(Object str) {
        return toFloat(str, 0, -1);
    }

    public static float toFloat(Object str, float defaultValue) {
        return toFloat(str, defaultValue, -1);
    }

    public static float toFloat(Object str, int pointPos) {
        return toFloat(str, 0, pointPos);
    }

    public static float toFloat(Object str, float defaultValue, int pointPos) {
        String tmp = String.valueOf(str);
        float Result = defaultValue;
        if (tmp.equals("null"))
            Result = defaultValue;
        else {
            try {
                Result = Float.parseFloat(tmp);
            } catch (Exception e) {
                Result = defaultValue;
            }
        }

        if (pointPos != -1) {
            return new BigDecimal(Result).setScale(pointPos, BigDecimal.ROUND_HALF_UP).floatValue();
        } else {
            return Result;
        }
    }

    public static Date toDate(Object str) {
        return toDate(str, null, "yyyy-MM-dd HH:mm:ss");
    }

    public static Date toDate(Object str, Date defaultValue) {
        return toDate(str, defaultValue, "yyyy-MM-dd HH:mm:ss");
    }

    public static Date toDate(Object str, String formatStr) {
        return toDate(str, null, formatStr);
    }

    public static Date toDate(Object str, Date defaultValue, String formatStr) {
        SimpleDateFormat formatter = new SimpleDateFormat(formatStr);
        String tmp = String.valueOf(str);
        if (tmp.equals("null")) {
            try {
                return formatter.parse(formatter.format(defaultValue));
            } catch (ParseException e) {
                return null;
            }
        } else {
            try {
                return formatter.parse(tmp);
            } catch (Exception e) {
                return defaultValue;
            }
        }
    }

    public static String getDate() {
        return getDate("yyyy-MM-dd HH:mm:ss");
    }

    public static String getDate(String formatStr) {
        Calendar cal = Calendar.getInstance();
        return dateFormat(cal.getTime(), formatStr);
    }

    public static String dateFormat(Date date, String formatStr) {
        SimpleDateFormat formatter = new SimpleDateFormat(formatStr);
        return formatter.format(date);
    }

    public static JSONObject BundleToJSONObject(Bundle bundle) {
        Set<String> keys = bundle.keySet();
        JSONObject obj = new JSONObject();
        for (String key : keys) {
            obj.put(key, bundle.get(key));
        }
        return obj;
    }

    public static <T> String objectToJson(T bean) {
        JSON.DEFFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
        return JSON.toJSONString(bean, SerializerFeature.WriteDateUseDateFormat, SerializerFeature.WriteMapNullValue,
                SerializerFeature.WriteNullStringAsEmpty, SerializerFeature.WriteNullListAsEmpty);
    }

    public static <T> T jsonToBean(Class<T> clazz, String json) {
        return (T) JSON.parseObject(json, clazz);
    }

    public static <T, Q> Map<T, Q> jsonToMap(String json) {
        return JSON.parseObject(json, new TypeReference<Map<T, Q>>() {
        });
    }

    public static <T, Q> LinkedHashMap<T, Q> jsonToLinkedHashMap(String json) {
        return JSON.parseObject(json, new TypeReference<LinkedHashMap<T, Q>>() {
        });
    }

    public static <T> String listToJson(List<T> list) {
        JSON.DEFFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
        return JSON.toJSONString(list, SerializerFeature.WriteDateUseDateFormat);
    }

    public static <T, Q> List<Map<T, Q>> jsonToMapList(String json) {
        return JSON.parseObject(json, new TypeReference<List<Map<T, Q>>>() {
        });
    }

    public static <T, Q> List<LinkedHashMap<T, Q>> jsonToLinkedHashMapList(String json) {
        return JSON.parseObject(json, new TypeReference<List<LinkedHashMap<T, Q>>>() {
        });
    }

    public static <T> List<T> jsonToBeanList(Class<T> clazz, String json) {
        return JSON.parseArray(json, clazz);
    }

    public static List<JSONObject> treeJSONArrayToList(JSONArray jsonArray, String childKey) {
        List<JSONObject> list = new ArrayList<>();
        for (Object obj : jsonArray) {
            JSONObject item = (JSONObject) obj;
            if (item.containsKey(childKey)) {
                list.addAll(treeJSONArrayToList(item.getJSONArray(childKey), childKey));
            } else {
                list.add(item);
            }
        }

        return list;
    }

    public static boolean isJavaBean(Type type) {
        if (null == type)
            return false;

        return ParserConfig.global.getDeserializer(type) instanceof JavaBeanDeserializer;
    }

    public static String formatString(String content, Map<String, String> map) {
        Set<Entry<String, String>> sets = map.entrySet();
        for (Entry<String, String> entry : sets) {
            String regex = "(#|\\$|)\\{" + entry.getKey() + "\\}";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(content);
            content = matcher.replaceAll(entry.getValue());
        }
        return content;
    }

    public static String trim(String str, char... trimChars) {
        if (str == null || str.equals("")) {
            return str;
        }
        if (trimChars == null || trimChars.length == 0) {
            return str;
        }

        for (int i = 0; i < trimChars.length; i++) {
            String strTrim = toStr(trimChars[i]).toLowerCase();
            if (str.toLowerCase().startsWith(strTrim)) {
                str = str.substring(1, str.length());
            }

            if (str.toLowerCase().endsWith(strTrim)) {
                str = str.substring(0, str.length() - 1);
            }
        }

        return str;
    }

    public static String trimStart(String str, char... trimChars) {
        if (str == null || str.equals("")) {
            return str;
        }
        if (trimChars == null || trimChars.length == 0) {
            return str;
        }

        for (int i = 0; i < trimChars.length; i++) {
            String strTrim = toStr(trimChars[i]).toLowerCase();
            if (str.toLowerCase().startsWith(strTrim)) {
                str = str.substring(1, str.length());
            }
        }

        return str;
    }

    public static String trimEnd(String str, char... trimChars) {
        if (str == null || str.equals("")) {
            return str;
        }
        if (trimChars == null || trimChars.length == 0) {
            return str;
        }

        for (int i = 0; i < trimChars.length; i++) {
            String strTrim = toStr(trimChars[i]).toLowerCase();
            if (str.toLowerCase().endsWith(strTrim)) {
                str = str.substring(0, str.length() - 1);
            }
        }

        return str;

    }


    public static String guessContentType(String fileName) {
        FileNameMap fileNameMap = URLConnection.getFileNameMap();
        String contentTypeFor = fileNameMap.getContentTypeFor(fileName);
        if (contentTypeFor == null) {
            contentTypeFor = "application/octet-stream";
        }
        return contentTypeFor;
    }

    public static String getValueFromXML(String key, String xml) {
        Pattern pattern = Pattern.compile("<" + key + ">(\\<\\!\\[CDATA\\[|)(.+?)(\\]\\]\\>|)</" + key + ">");
        Matcher matcher = pattern.matcher(xml);
        if (matcher.find())
            return matcher.group(2);
        else
            return "";
    }

    public static void saveConfigToFile(String fileName, String config) {
        try {
            File file = new File(android.os.Environment.getExternalStorageDirectory().getAbsolutePath() + "/vitapos/" + fileName + ".ini");
            file.getParentFile().mkdirs();
            file.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(file);
            outputStream.write(config.getBytes());
        } catch (Exception e) {
        }
    }

    public static String getConfigFromFile(String fileName) {
        try {
            File file = new File(android.os.Environment.getExternalStorageDirectory().getAbsolutePath() + "/vitapos/" + fileName + ".ini");
            FileInputStream inputStream = new FileInputStream(file);
            byte[] buf = new byte[1024]; //数据中转站 临时缓冲区
            int length = 0;
            StringBuilder sb = new StringBuilder();
            while ((length = inputStream.read(buf)) != -1) {
                sb.append(new String(buf, 0, length));
            }
            return sb.toString();
        } catch (Exception e) {
            return "";
        }
    }

    public static void setStore(Context mContext, String key, String value) {
        SharedPreferences sp = mContext.getSharedPreferences("data", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(key, value);
        editor.commit();
        saveConfigToFile("data", getStoreAll(mContext));
    }

    public static String getStoreAll(Context mContext) {
        SharedPreferences sp = mContext.getSharedPreferences("data", Context.MODE_PRIVATE);
        return Helper.objectToJson(sp.getAll());
    }

    public static String getStore(Context mContext, String key, String defaultValue) {
        SharedPreferences sp = mContext.getSharedPreferences("data", Context.MODE_PRIVATE);
        return sp.getString(key, defaultValue);
    }

    public static Map<String, Object> getConfigAppInfo(Context mContext) {
        return Helper.jsonToMap(Helper.getStore(mContext, "appInfo", "{}"));
    }

    public static String getConfigAppInfoStringValue(Context mContext, String key) {
        return Helper.toStr(getConfigAppInfo(mContext).get(key));
    }

    public static int getConfigAppInfoIntValue(Context mContext, String key) {
        return Helper.toInt(getConfigAppInfo(mContext).get(key));
    }

    public static void setConfigAppInfo(Context mContext, Map<String, Object> appInfo) {
        Map<String, Object> info = getConfigAppInfo(mContext);
        for (Entry<String, Object> entry : appInfo.entrySet()) {
            info.put(entry.getKey(), entry.getValue());
        }
        Helper.setStore(mContext, "appInfo", Helper.objectToJson(info));
    }

    public static void setConfigAppInfo(Context mContext, String key, Object value) {
        Map<String, Object> appInfo = getConfigAppInfo(mContext);
        appInfo.put(key, value);
        Helper.setStore(mContext, "appInfo", Helper.objectToJson(appInfo));
    }

    public static void removeStore(Context mContext, String key) {
        SharedPreferences sp = mContext.getSharedPreferences("data", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.remove(key);
        editor.commit();
    }

    public static Object getFieldValue(JSONObject data, String fieldName) {
        if (data == null)
            return null;

        String[] fieldNames = fieldName.split("_|\\.");
        if (fieldNames.length == 1 && data.containsKey(fieldNames[0])) {
            return data.get(fieldName);
        }

        if (fieldNames.length > 1) {
            Pattern re = Pattern.compile("(.*[^0-9])([0-9]+)", Pattern.CASE_INSENSITIVE);
            Matcher match = re.matcher(fieldNames[0]);
            if (match.find()) {
                JSONArray arr = data.getJSONArray(match.group(1));
                return getFieldValue(arr, toInt(match.group(2)), fieldName.substring(fieldNames[0].length() + 1));
            } else {
                JSONObject subObj = data.getJSONObject(fieldNames[0]);
                return getFieldValue(subObj, fieldName.substring(fieldNames[0].length() + 1));
            }
        }

        return null;
    }

    public static Object getFieldValue(JSONArray arr, int index, String fieldName) {
        if (arr == null)
            return null;

        if (fieldName == "index")
            return index + 1;

        JSONObject subObj = arr.getJSONObject(index);
        return getFieldValue(subObj, fieldName);
    }

}