package com.vita.util;

import android.content.Context;
import android.speech.tts.TextToSpeech;

import com.vita.MainActivity;

import java.util.Locale;
import java.util.UUID;

public class SpeechUtils {


    private static final String TAG = "SpeechUtils";
    private static SpeechUtils singleton;

    private TextToSpeech textToSpeech = MainActivity.textToSpeech; // TTS对象

    public static SpeechUtils getInstance() {
        if (singleton == null) {
            synchronized (SpeechUtils.class) {
                if (singleton == null) {
                    singleton = new SpeechUtils();
                }
            }
        }
        return singleton;
    }



    public void speakText(String text) {
        if (textToSpeech != null) {
            textToSpeech.speak(text,
                    TextToSpeech.QUEUE_FLUSH, null);
        }

    }


    public void speakText(String text, int queueMode) {
        if (textToSpeech != null) {
            textToSpeech.speak(text, queueMode, null, UUID.randomUUID().toString());
        }

    }


    public void close() {
        if (textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
            textToSpeech=null;
            singleton=null;
        }
    }

    public void stop() {
        if (textToSpeech != null) {
            textToSpeech.stop();

        }
    }


}
