package com.vita.data;

import com.landicorp.android.eptapi.card.MifareDriver;
import com.landicorp.android.eptapi.device.RFCardReader;

public interface Constants {
    /**
     * 设备类型
     */
    interface DeviceType {
        /** 内置 */
        String DEVICE_INNER = "inner";
        /** 外置 */
        String DEVICE_EXTERNAL = "external";
    }

    interface RFCard {
        String DEVICE_INNER = "USERCARD";
        String DEVICE_EXTERNAL = "EXTRFCARD";

        /** 卡片激活驱动名称 */
        String DRIVER_NAME_PRO = "PRO";
        String DRIVER_NAME_S50 = "S50";
        String DRIVER_NAME_S70 = "S70";
        String DRIVER_NAME_CPU = "CPU";

        /** 认证密钥类型 */
        int KEY_TYPE_A =  MifareDriver.KEY_A;
        int KEY_TYPE_B =  MifareDriver.KEY_B;

        /** 强制设置卡片类型 */
        int CARD_TYPE_S50 = RFCardReader.OnSearchListener.S50_CARD;
        int CARD_TYPE_S70 = RFCardReader.OnSearchListener.S70_CARD;
        int CARD_TYPE_PRO = RFCardReader.OnSearchListener.PRO_CARD;
    }

    interface Scanner {
        int CAMERA_FRONT = 0x00;
        int CAMERA_BACK = 0x01;
    }
}
