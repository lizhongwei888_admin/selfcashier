package com.vita.device;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import com.landicorp.android.eptapi.DeviceService;
import com.landicorp.android.eptapi.exception.ReloginException;
import com.landicorp.android.eptapi.exception.RequestException;
import com.landicorp.android.eptapi.exception.ServiceOccupiedException;
import com.landicorp.android.eptapi.exception.UnsupportMultiProcess;

public class BaseDevice {
    public Handler uiHandler = new Handler(Looper.getMainLooper());
    protected Context context;
    private Toast mToast;

    protected void onDeviceServiceCrash() {
        if (context == null) {
            return;
        }
    }

    public void displayInfo(String text) {
        if (context == null) {
            return;
        }

        if (mToast == null) {
            mToast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
            mToast.show();
        } else {
            mToast.setText(text);
            mToast.show();
        }
    }
}
