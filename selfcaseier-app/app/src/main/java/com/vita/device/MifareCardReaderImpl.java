package com.vita.device;

import android.content.Context;

import com.landicorp.android.eptapi.card.MifareDriver;
import com.landicorp.android.eptapi.card.RFDriver;
import com.landicorp.android.eptapi.device.Beeper;
import com.landicorp.android.eptapi.device.RFCardReader;
import com.landicorp.android.eptapi.device.RFCardReader.OnSearchListener;
import com.landicorp.android.eptapi.exception.RequestException;
import com.landicorp.android.eptapi.utils.BytesBuffer;
import com.vita.controller.Print;
import com.vita.data.BaseError;
import com.vita.data.Constants;
import com.vita.util.ByteUtil;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 针对终端上已具备非接模块可直接使用，否则需外接设备进行调用。使用外接设备时，
 * RFCardReader实例化时需使用getOtherInstance()，传入设备名EXTRFCARD。
 * 且寻卡监听回调需使用OnSearchListenerEx。
 */
public class MifareCardReaderImpl extends BaseDevice {

    private RFCardReader reader;
    private MifareDriver driver;
    private boolean forceSetCardType = false;
    private int cardType = Constants.RFCard.CARD_TYPE_S50;
    private int blockNo;
    private int keyType;
    private byte[] key;
    private MifareCardReaderCallback callback;
    private String driverName;

    public interface MifareCardReaderCallback {
        public void exec(String err, String data);
    }

    private RFCardReader.OnSearchListener searchListener = new RFCardReader.OnSearchListener() {
        @Override
        public void onCrash() {
            Beeper.startBeep(1000);
            onDeviceServiceCrash();
            callback.exec("search error, error = crash", "");
        }

        @Override
        public void onFail(int error) {
            Beeper.startBeep(100);
            Beeper.startBeep(100);
            callback.exec("search error, error = " + error, "");
        }

        @Override
        public void onCardPass(int cardType) {
            if (forceSetCardType) {
                setCardType();
            } else {
                setDriverName(cardType);
            }
            runOnUI(new Runnable() {
                @Override
                public void run() {
                    Beeper.startBeep(100);
                    activeCard();
                }
            });
        }
    };

    public MifareCardReaderImpl(Context context, String deviceType) {
        this.context = context;
        if (deviceType.equals(Constants.DeviceType.DEVICE_INNER)) {
            reader = RFCardReader.getInstance();
        } else {
            reader = RFCardReader.getOtherInstance(Constants.RFCard.DEVICE_EXTERNAL);
        }
    }

    public void cardHalt() {
        stopSearch();
        halt();
    }

    /**
     * 强制设置卡片类型
     * 对于某些比较特殊的mifareone卡（比如：MifareOne UltraLight卡片），寻卡时会识别到pro卡类型，此时需要强制设置卡片类型
     */
    private void setCardType() {
        setDriverName(cardType);
        reader.setParameter(0x0D, cardType);
    }

    private void stopSearch() {
        try {
            reader.stopSearch();
        } catch (RequestException e) {
            e.printStackTrace();
        }
    }

    private void activeCard() {
        try {
            if (driverName == null) {
                return;
            }
            if (!Constants.RFCard.DRIVER_NAME_S50.equals(driverName) && !Constants.RFCard.DRIVER_NAME_S70.equals(driverName)) {
                Beeper.startBeep(100);
                Beeper.startBeep(100);
                callback.exec("this card is not s50 or s70, not support", "");
                return;
            }
            reader.activate(driverName, new RFCardReader.OnActiveListener() {
                @Override
                public void onCardActivate(RFDriver rfDriver) {
                    driver = (MifareDriver) rfDriver;
                    try {
                        int i = 0;
                        List<Byte> list = new ArrayList<>();
                        while (true) {
                            if ((i + 1) % 4 == 0) {
                                i++;
                                continue;
                            }

                            if (driver.authBlock(blockNo + i, keyType, key) != BaseError.SUCCESS) {
                                cardHalt();
                                Beeper.startBeep(100);
                                Beeper.startBeep(100);
                                callback.exec("authBlock error", "");
                            }

                            BytesBuffer data = new BytesBuffer();
                            int result = driver.readBlock(blockNo + i, data);
                            if (result == BaseError.SUCCESS) {
                                byte[] bs = data.getData();
                                for (byte b : bs) {
                                    list.add(b);
                                }
                                if (bs[bs.length - 1] == 0) {
                                    break;
                                }
                            } else {
                                cardHalt();
                                Beeper.startBeep(100);
                                Beeper.startBeep(100);
                                callback.exec("read error", "");
                            }

                            i++;
                        }

                        Beeper.startBeep(100);
                        callback.exec("", ByteUtil.bytes2HexString(listTobyte(list)));
                    } catch (
                            RequestException e) {
                        cardHalt();
                        Beeper.startBeep(100);
                        Beeper.startBeep(100);
                        callback.exec(e.getMessage(), "");
                    }
                }

                @Override
                public void onActivateError(int error) {
                    Beeper.startBeep(100);
                    Beeper.startBeep(100);
                    callback.exec("activate error, error = " + error, "");
                }

                @Override
                public void onUnsupport(String s) {
                    Beeper.startBeep(100);
                    Beeper.startBeep(100);
                    callback.exec("unsupport driverName = " + s, "");
                }

                @Override
                public void onCrash() {
                    Beeper.startBeep(1000);
                    onDeviceServiceCrash();
                    callback.exec("search error, error = crash", "");
                }
            });
        } catch (
                RequestException e) {
            e.printStackTrace();
        }

    }

    private static byte[] listTobyte(List<Byte> list) {
        if (list == null || list.size() < 0)
            return null;
        byte[] bytes = new byte[list.size()];
        int i = 0;
        Iterator<Byte> iterator = list.iterator();
        while (iterator.hasNext()) {
            bytes[i] = iterator.next();
            i++;
        }
        return bytes;
    }

    public void getCardContent(boolean forceSetCardType, int cardType, int blockNo, int keyType, byte[] key, MifareCardReaderCallback callback) {
        this.forceSetCardType = forceSetCardType;
        this.cardType = cardType;
        this.blockNo = blockNo;
        this.keyType = keyType;
        this.key = key;
        this.callback = callback;
        reader.setParameter(0x0E, 0);
        try {
            reader.searchCard(searchListener);
        } catch (RequestException e) {
            e.printStackTrace();
        }
    }

    private void halt() {
        if (driver != null) {
            try {
                driver.halt();
                driverName = null;
            } catch (RequestException e) {
                e.printStackTrace();
            }
        }
    }

    private void setDriverName(int cardType) {
        switch (cardType) {
            case OnSearchListener.S50_CARD:
            case OnSearchListener.S50_PRO_CARD:
                driverName = Constants.RFCard.DRIVER_NAME_S50;
                break;
            case OnSearchListener.S70_CARD:
            case OnSearchListener.S70_PRO_CARD:
                driverName = Constants.RFCard.DRIVER_NAME_S70;
                break;
            case OnSearchListener.CPU_CARD:
                driverName = Constants.RFCard.DRIVER_NAME_CPU;
                break;
            case OnSearchListener.PRO_CARD:
                driverName = Constants.RFCard.DRIVER_NAME_PRO;
                break;
            default:
                driverName = Constants.RFCard.DRIVER_NAME_PRO;
                break;
        }
    }

    private void runOnUI(Runnable runnable) {
        uiHandler.post(runnable);
    }
}
