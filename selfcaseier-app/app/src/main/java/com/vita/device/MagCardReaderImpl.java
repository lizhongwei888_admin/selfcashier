package com.vita.device;

import android.text.TextUtils;

import com.landicorp.android.eptapi.device.Beeper;
import com.landicorp.android.eptapi.device.MagCardReader;
import com.landicorp.android.eptapi.exception.RequestException;
import com.vita.device.BaseDevice;
import com.vita.device.MifareCardReaderImpl;
import com.vita.util.Helper;

import java.util.HashMap;
import java.util.Map;

/**
 * This sample show that how to read magnetic card
 */
public class MagCardReaderImpl extends BaseDevice {
    private MagCardReaderImpl.MagCardReaderCallback callback;

    public interface MagCardReaderCallback {
        public void exec(String err, String data);
    }

    MagCardReader.OnSearchListener listener = new MagCardReader.OnSearchListener() {
        @Override
        public void onCrash() {
            Beeper.startBeep(1000);
            onDeviceServiceCrash();
            callback.exec("search error, error = crash", "");
        }

        @Override
        public void onFail(int code) {
            Beeper.startBeep(100);
            Beeper.startBeep(100);
            String errorMessage = getErrorDescription(code);
            callback.exec(errorMessage, "");
        }

        @Override
        public void onCardStriped(boolean[] hasTrack, String[] track) {
            if (track.length > 2) {
                Beeper.startBeep(100);
                callback.exec("", track[1]);
            } else {
                Beeper.startBeep(100);
                Beeper.startBeep(100);
                callback.exec("cardno incorrect", "");
            }
        }

        /**
         * Get msg of the error code for display
         * @param code
         * @return
         */
        public String getErrorDescription(int code) {
            switch (code) {
                case ERROR_NODATA:
                    return "no data";
                case ERROR_NEEDSTART:
                    return "need restart search";//This error never happened
                case ERROR_INVALID:
                    return "has invalid track";//
            }
            return "unknown error - " + code;
        }
    };

    public MagCardReaderImpl() {
    }

    public void getCardContent(MagCardReaderImpl.MagCardReaderCallback callback) {
        this.callback = callback;
        searchCard();
    }

    /**
     * Search card and show all track info
     */
    public void searchCard() {
        // start search card
        try {
            int enable = MagCardReader.TRK1 | MagCardReader.TRK2 | MagCardReader.TRK3;
            MagCardReader.getInstance().enableTrack(enable);
            MagCardReader.getInstance().setLRCCheckEnabled(true);
            MagCardReader.getInstance().searchCard(listener);
        } catch (RequestException e) {
            // the device service has a fatal exception
            e.printStackTrace();
            Beeper.startBeep(1000);
            onDeviceServiceCrash();
            callback.exec(e.getMessage(), "");
        }
    }

    /**
     * Stop search if card searching is started
     */
    public void stopSearch() {
        try {
            MagCardReader.getInstance().stopSearch();
        } catch (RequestException e) {
            e.printStackTrace();
            onDeviceServiceCrash();
        }
    }
}
