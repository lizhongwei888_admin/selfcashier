package com.vita.device;

import com.landicorp.android.eptapi.device.Printer;

public class PrintTemplate {
    private int y = 0;

    private int width = 0;

    private String text = "";

    private String align = "left";

    private int fontsize = 1;

    private String type;

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getFontsize() {
        return fontsize;
    }

    public void setFontsize(int fontsize) {
        this.fontsize = fontsize;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setAlign(String align) {
        this.align = align;
    }

    public Printer.Alignment getAlign() {
        switch (align) {
            case "left":
                return Printer.Alignment.LEFT;
            case "right":
                return Printer.Alignment.RIGHT;
            case "middle":
                return Printer.Alignment.CENTER;
        }
        return Printer.Alignment.LEFT;
    }
}
