package com.vita.device;

import com.landicorp.android.eptapi.DeviceService;
import com.landicorp.android.eptapi.exception.ReloginException;
import com.landicorp.android.eptapi.exception.RequestException;
import com.landicorp.android.eptapi.exception.ServiceOccupiedException;
import com.landicorp.android.eptapi.exception.UnsupportMultiProcess;
import com.vita.WebAppInterface;
import com.vita.data.Constants;
import com.vita.util.LogHelper;

public class DeviceHelper {
    public static boolean login(WebAppInterface appInterface) {
        try {
            DeviceService.login(appInterface.getmContext());
        } catch (ServiceOccupiedException e) {
            LogHelper.sendLog(appInterface, "DeviceHelper", LogHelper.InfoLevel.INFO, "login", e.getMessage());
            return false;
        } catch (ReloginException e) {
            LogHelper.sendLog(appInterface, "DeviceHelper", LogHelper.InfoLevel.INFO, "login", e.getMessage());
            return false;
        } catch (UnsupportMultiProcess e) {
            LogHelper.sendLog(appInterface, "DeviceHelper", LogHelper.InfoLevel.INFO, "login", e.getMessage());
            return false;
        } catch (RequestException e) {
            LogHelper.sendLog(appInterface, "DeviceHelper", LogHelper.InfoLevel.INFO, "login", e.getMessage());
            return false;
        }

        return true;
    }

    public static void logout() {
        DeviceService.logout();
    }

    public static MifareCardReaderImpl getMiCardReader(WebAppInterface appInterface) {
        return new MifareCardReaderImpl(appInterface.getmContext(), Constants.DeviceType.DEVICE_INNER);
    }

    public static MagCardReaderImpl getMagCardReader() {
        return new MagCardReaderImpl();
    }

    public static PrinterImpl getPrinter(WebAppInterface appInterface) {
        return new PrinterImpl(appInterface.getmContext());
    }

//    public static CameraScannerPresenterImpl getCameraScanner(WebAppInterface appInterface) {
//        return new CameraScannerPresenterImpl(appInterface.getmContext());
//    }
}
