package com.vita.device;

import android.content.Context;
import android.text.TextUtils;

import com.landicorp.android.eptapi.device.Printer;
import com.landicorp.android.eptapi.device.Printer.Format;
import com.landicorp.android.eptapi.exception.RequestException;
import com.landicorp.android.eptapi.utils.QrCode;
import com.vita.data.BaseError;
import com.vita.util.StringUtil;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.landicorp.android.eptapi.utils.QrCode.ECLEVEL_Q;

/**
 * 针对无打印机终端需使用外接打印机，如蓝牙打印机等。该示例针对内置打印机。
 */

public class PrinterImpl extends BaseDevice {
    private com.landicorp.android.eptapi.device.Printer.Progress progress;
    private List<com.landicorp.android.eptapi.device.Printer.Step> stepList;
    private PrintCallback callback;

    public interface PrintCallback {
        public void exec(String err, String data);
    }

    public PrinterImpl(Context context) {
        this.context = context;
    }

    private int getPrinterStatus() {
        try {
            int status = Printer.getInstance().getStatus();
            return status;
        } catch (RequestException e) {
            e.printStackTrace();
        }
        return BaseError.FAIL;
    }

    private void init() {
        stepList = new ArrayList<com.landicorp.android.eptapi.device.Printer.Step>();
    }

    private boolean feedLine(final int line) {
        if (stepList == null) {
            //displayInfo("打印失败");
            return false;
        }
        stepList.add(new com.landicorp.android.eptapi.device.Printer.Step() {
            @Override
            public void doPrint(com.landicorp.android.eptapi.device.Printer printer) throws Exception {
                printer.feedLine(line);
            }
        });
        return true;
    }

    private boolean cutPaper() {
        if (stepList == null) {
            //displayInfo("打印失败");
            return false;
        }

        if (!Printer.getInstance().isSupportCutter()) {
            return true;
        }

        stepList.add(new com.landicorp.android.eptapi.device.Printer.Step() {
            @Override
            public void doPrint(com.landicorp.android.eptapi.device.Printer printer) throws Exception {
                printer.autoCutPaper();
            }
        });
        return true;
    }

    private void startPrint() {
        if (stepList == null) {
            //displayInfo("打印失败");
            callback.exec("printer has not inited!", "");
            return;
        }
        progress = new com.landicorp.android.eptapi.device.Printer.Progress() {
            @Override
            public void doPrint(com.landicorp.android.eptapi.device.Printer printer) throws Exception {
            }

            @Override
            public void onFinish(int error) {
                stepList.clear();
                if (error == com.landicorp.android.eptapi.device.Printer.ERROR_NONE) {
                    //displayInfo("打印成功");
                    callback.exec("", "");
                } else {
                    String errorDes = getDescribe(error);
                    //displayInfo("打印失败");
                    callback.exec(errorDes, "");
                }
            }

            @Override
            public void onCrash() {
                stepList.clear();
                onDeviceServiceCrash();
                callback.exec("打印失败", "");
            }
        };
        for (com.landicorp.android.eptapi.device.Printer.Step step : stepList) {
            progress.addStep(step);
        }
        try {
            progress.start();
        } catch (RequestException e) {
            e.printStackTrace();
            //displayInfo("打印失败");
            callback.exec("request exception has ocurred", "");
        }
    }

    private int getPrinterWidth() {
        int width = Printer.getInstance().getValidWidth();
        if (width <= 0) {
            return 384;
        }
        return width;
    }

    private String getDescribe(int error) {
        switch (error) {
            case com.landicorp.android.eptapi.device.Printer.ERROR_BMBLACK:
                return "ERROR_BMBLACK";
            case com.landicorp.android.eptapi.device.Printer.ERROR_BUFOVERFLOW:
                return "ERROR_BUFOVERFLOW";
            case com.landicorp.android.eptapi.device.Printer.ERROR_BUSY:
                return "ERROR_BUSY";
            case com.landicorp.android.eptapi.device.Printer.ERROR_COMMERR:
                return "ERROR_COMMERR";
            case com.landicorp.android.eptapi.device.Printer.ERROR_CUTPOSITIONERR:
                return "ERROR_CUTPOSITIONERR";
            case com.landicorp.android.eptapi.device.Printer.ERROR_HARDERR:
                return "ERROR_HARDERR";
            case com.landicorp.android.eptapi.device.Printer.ERROR_LIFTHEAD:
                return "ERROR_LIFTHEAD";
            case com.landicorp.android.eptapi.device.Printer.ERROR_LOWTEMP:
                return "ERROR_LOWTEMP";
            case com.landicorp.android.eptapi.device.Printer.ERROR_LOWVOL:
                return "ERROR_LOWVOL";
            case com.landicorp.android.eptapi.device.Printer.ERROR_MOTORERR:
                return "ERROR_MOTORERR";
            case com.landicorp.android.eptapi.device.Printer.ERROR_NOBM:
                return "ERROR_NOBM";
            case com.landicorp.android.eptapi.device.Printer.ERROR_NONE:
                return "ERROR_NONE";
            case com.landicorp.android.eptapi.device.Printer.ERROR_OVERHEAT:
                return "ERROR_OVERHEAT";
            case com.landicorp.android.eptapi.device.Printer.ERROR_PAPERENDED:
                return "ERROR_PAPERENDED";
            case com.landicorp.android.eptapi.device.Printer.ERROR_PAPERENDING:
                return "ERROR_PAPERENDING";
            case com.landicorp.android.eptapi.device.Printer.ERROR_PAPERJAM:
                return "ERROR_PAPERJAM";
            case com.landicorp.android.eptapi.device.Printer.ERROR_PENOFOUND:
                return "ERROR_PENOFOUND";
            case com.landicorp.android.eptapi.device.Printer.ERROR_WORKON:
                return "ERROR_WORKON";
            case Printer.ERROR_CUTCLEAN:
                return "ERROR_CUTCLEAN";
            case Printer.ERROR_CUTERROR:
                return "ERROR_CUTERROR";
            case Printer.ERROR_CUTFAULT:
                return "ERROR_CUTFAULT";
            case Printer.ERROR_OPENCOVER:
                return "ERROR_OPENCOVER";
            default:
                return "UNKNOWN ERROR";
        }
    }

    public boolean printTemplate(final int count, final List<PrintTemplate> list, PrintCallback callback) {
        this.callback = callback;
        int ret = getPrinterStatus();
        if (ret != Printer.ERROR_NONE) {
            //displayInfo("打印失败");
            callback.exec(getDescribe(ret), "");
            return false;
        }
        init();

        if (stepList == null) {
            displayInfo("printer has not inited");
            callback.exec("printer has not inited", "");
            return false;
        }
        stepList.add(new com.landicorp.android.eptapi.device.Printer.Step() {
            @Override
            public void doPrint(com.landicorp.android.eptapi.device.Printer printer) throws Exception {
                printer.setAutoTrunc(false);
                Format format = new Format();
                for (int i = 0; i < count; i++) {
                    int lastY = 0;
                    for (PrintTemplate item : list) {
                        if (lastY > 0 && item.getY() != lastY) {
                            printer.printText("\n");
                        }

                        format.setAscScale(Format.AscScale.values()[item.getFontsize() > 10 ? 9 : item.getFontsize() - 1]);
                        format.setAscSize(Format.ASC_DOT24x12);
                        format.setHzScale(Format.HzScale.values()[item.getFontsize() > 10 ? 9 : item.getFontsize() - 1]);
                        format.setHzSize(Format.HZ_DOT24x24);
                        if(item.getY() != lastY) {
                            format.setYSpace((item.getY() - lastY) * 10);
                        }
                        printer.setFormat(format);

                        if (item.getType() != null && item.getType().equals("qrcode")) {
                            printer.printQrCode(item.getAlign(), new QrCode(item.getText(), ECLEVEL_Q), item.getWidth());
                        } else if (item.getType() != null && item.getType().equals("barcode")) {
                            printer.printBarCode(item.getAlign(), item.getText());
                        } else {
                            String content = item.getText();
                            if (item.getWidth() > 0) {
                                if (item.getAlign() == Printer.Alignment.RIGHT) {
                                    content = padLeftWhileDouble(content, item.getWidth(), ' ');
                                } else {
                                    content = padRightWhileDouble(content, item.getWidth(), ' ');
                                }
                            }
                            printer.setFormat(format);
                            printer.printText(item.getAlign(), content);
                        }

                        lastY = item.getY();
                    }

                    printer.feedLine(5);
                    printer.cutPaper();
                }
            }
        });
        startPrint();
        return true;
    }

    String padLeftWhileDouble(String input, int length, char paddingChar) {
        int singleLength = getSingleLength(input);
        return StringUtil.append(true, input, paddingChar, length - singleLength + input.length());
    }

    int getSingleLength(String input) {
        if (input == null) {
            return 0;
        }
        return input.replaceAll("[^\\x00-\\xff]", "aa").length();
    }

    String padRightWhileDouble(String input, int length, char paddingChar) {
        int singleLength = getSingleLength(input);
        return StringUtil.append(false, input, paddingChar, length - singleLength + input.length());
    }

    public void printMultiColumnReceipt(String title, List<LinkedHashMap<String, String>> list, PrintCallback callback) {
        this.callback = callback;
        int ret = getPrinterStatus();
        if (ret != Printer.ERROR_NONE) {
            displayInfo("打印失败");
            callback.exec(getDescribe(ret), "");
            return;
        }
        init();

        if (!addMultiColumnReceipt(title, createReceiptData(list), new int[]{140, 244})) {
            displayInfo("add receipt data fail");
            callback.exec("add receipt data fail", "");
            return;
        }

        if (!feedLine(5)) {
            displayInfo("feed line fail");
            callback.exec("feed line fail", "");
            return;
        }
        if (!cutPaper()) {
            displayInfo("cut page fail");
            callback.exec("cut page fail", "");
            return;
        }
        startPrint();
    }

    private List<PrinterImpl.ReceiptTextFormat> createReceiptData(List<LinkedHashMap<String, String>> list) {
        List<PrinterImpl.ReceiptTextFormat> receiptTextFormats = new ArrayList<>();
        for (Map<String, String> map : list) {
            for (Map.Entry<String, String> item : map.entrySet()) {
                List<String> normalNameLine = new ArrayList<>();
                normalNameLine.add(item.getKey());
                normalNameLine.add(item.getValue() == null ? "" : item.getValue().toString());
                receiptTextFormats.add(new PrinterImpl.ReceiptTextFormat(normalNameLine, PrinterImpl.ReceiptTextFormat.FONT_NORMAL, PrinterImpl.ReceiptTextFormat.FONT_NORMAL));
            }
        }

        return receiptTextFormats;
    }

    /**
     * 打印多列格式小票示例
     *
     * @param content      小票内容，包括文本内容和字体大小
     * @param columnWidths 列宽度，按照列顺序传入所有列的宽度
     * @return
     */
    private boolean addMultiColumnReceipt(final String title, final List<ReceiptTextFormat> content, final int[] columnWidths) {
        if (stepList == null) {
            displayInfo("printer has not inited!");
            return false;
        }
        stepList.add(new com.landicorp.android.eptapi.device.Printer.Step() {
            @Override
            public void doPrint(com.landicorp.android.eptapi.device.Printer printer) throws Exception {
                if (!checkReceiptDataValidity(content, columnWidths)) {
                    return;
                }
                printer.setAutoTrunc(false);
                Format format = new Format();
                if (!title.equals("")) {
                    format.setAscScale(Format.ASC_SC2x2);
                    format.setAscSize(Format.ASC_DOT24x12);
                    format.setHzScale(Format.HZ_SC2x2);
                    format.setHzSize(Format.HZ_DOT24x24);
                    printer.setFormat(format);
                    printer.printText(Printer.Alignment.CENTER, title + "\n");
                }
                format.setAscScale(Format.ASC_SC1x1);
                format.setAscSize(Format.ASC_DOT16x8);
                format.setHzScale(Format.HZ_SC1x1);
                format.setHzSize(Format.HZ_DOT16x16);
                printer.setFormat(format);
                printer.printText(Printer.Alignment.CENTER, "\n");
                printReceipt(content, columnWidths, printer);
            }
        });
        return true;
    }

    private void printReceipt(List<ReceiptTextFormat> content, int[] columnWidths, Printer printer) throws Exception {
        for (ReceiptTextFormat line : content) {
            List<String> texts = line.getTexts();
            if (!texts.get(1).equals("") && !texts.get(1).startsWith("Printer.Alignment.")) {
                printOneLineOfReceipt(line.getTexts(), line.getAscCharFormat(), line.getChineseCharFormat(), columnWidths, printer);
            } else {
                printer.printText((texts.get(1).equals("Printer.Alignment.LEFT") || texts.get(1).equals("")) ? Printer.Alignment.LEFT : texts.get(1).equals("Printer.Alignment.CENTER") ? Printer.Alignment.CENTER : Printer.Alignment.RIGHT, texts.get(0) + "\n");
            }
        }
    }

    /**
     * 打印小票上的一行文本
     *
     * @param texts             单行所有列的文本
     * @param ascCharFormat     西文字体格式
     * @param chineseCharFormat 中文字体格式
     * @param columnWidths      列宽集合
     * @param printer           打印机对象
     * @throws Exception
     */
    private void printOneLineOfReceipt(List<String> texts, int ascCharFormat, int chineseCharFormat, int[] columnWidths, Printer printer) throws Exception {

        StringBuilder textBuilder = new StringBuilder();
        for (int i = 0; i < texts.size(); i++) {
            String text = texts.get(i);
            int textWidth = calcTextWidth(texts.get(i), ascCharFormat, chineseCharFormat);

            if (isOverflowColumnWidth(textWidth, columnWidths[i])) {
                String splitTexts = getSplitTexts(text, i, columnWidths, ascCharFormat, chineseCharFormat);
                textBuilder.append(splitTexts);
            } else {
                textBuilder.append(text);
                int nextTextOffset = calcNextTextOffset(textWidth, columnWidths[i]);
                String space = getSpaceText(nextTextOffset, ascCharFormat);
                textBuilder.append(space);
            }
        }

        printer.setFormat(getReceiptTextFormat(ascCharFormat, chineseCharFormat));
        printer.printText(textBuilder.toString() + "\n");
    }

    /**
     * 判断列文本是否超过当前列宽度
     *
     * @param textWidth
     * @param columnWidth
     * @return
     */
    private boolean isOverflowColumnWidth(int textWidth, int columnWidth) {
        return columnWidth < textWidth;
    }

    /**
     * 获取补充偏移的空格字符
     *
     * @return
     */
    private String getSpaceText(int offset, int ascCharFormat) {
        int per = getAscCharWidth(ascCharFormat);
        int num = offset / per;
        return StringUtil.append(true, "", ' ', num);
    }

    /**
     * 计算下一列文本的偏移位置
     *
     * @param curTextWidth
     * @param columnWidths
     * @return
     */
    private int calcNextTextOffset(int curTextWidth, int columnWidths) {
        return columnWidths - curTextWidth;
    }

    private String getSplitTexts(String text, int index, int[] columnWidths, int ascCharFormat, int chineseCharFormat) {
        StringBuilder builder = new StringBuilder();

        int ascCharWidth = getAscCharWidth(ascCharFormat);
        int chineseCharWidth = getChineseCharWidth(chineseCharFormat);

        int leftWholeWidth = 0;
        for (int i = 0; i < index; i++) {
            leftWholeWidth += columnWidths[i];
        }
        int rightWholeWidth = 0;
        for (int i = index; i < columnWidths.length; i++) {
            rightWholeWidth += columnWidths[i];
        }

        String leftSpace = getSpaceText(leftWholeWidth, ascCharFormat);
        int count = 0;
        int leftWidth = rightWholeWidth;
        String lastLineText = "";
        char[] chars = text.toCharArray();
        for (; count < chars.length; count++) {
            char c = chars[count];
            int cWidth = StringUtil.isChinese(c) || StringUtil.isChinesePunctuation(c) ? chineseCharWidth : ascCharWidth;
            if (cWidth > leftWidth) {
                builder.append("\n").append(leftSpace);
                lastLineText = "";
                leftWidth = rightWholeWidth;
            }

            builder.append(c);
            lastLineText += c;
            leftWidth -= cWidth;
        }

        int nextTextOffset = calcNextTextOffset(calcTextWidth(lastLineText, ascCharFormat, chineseCharFormat), columnWidths[index]);
        String space = getSpaceText(nextTextOffset, ascCharFormat);
        builder.append(space);

        return builder.toString();
    }

    /**
     * 校验小票数据有效性
     *
     * @param content
     * @param columnWidths
     * @return
     */
    private boolean checkReceiptDataValidity(List<ReceiptTextFormat> content, int[] columnWidths) {
        if (null == content || content.isEmpty()) {
            displayInfo("receipt hasn't any data");
            return false;
        }
        if (null == columnWidths || columnWidths.length == 0) {
            displayInfo("columnWidths is invalid");
            return false;
        }

        // 判断所有列的宽度总和是否超过有效打印宽度
        final int MAX_PAPER_WIDTH = getPrinterWidth();
        int dataWidth = 0;
        for (int width : columnWidths) {
            if (width <= 0) {
                displayInfo("columnWidths is invalid");
                return false;
            }
            dataWidth += width;
        }
        if (dataWidth > MAX_PAPER_WIDTH) {
            displayInfo("data width is more than paper width");
            return false;
        }

        // 判断每行文本数量和列数量是否一致
        for (ReceiptTextFormat format : content) {
            List<String> texts = format.getTexts();
            if (null == texts || texts.size() != columnWidths.length) {
                displayInfo("line text num is not match with column num");
                return false;
            }
        }

        return true;
    }

    /**
     * 计算文本打印宽度
     *
     * @param text
     * @param ascCharFormat
     * @param chineseCharFormat
     * @return
     */
    private int calcTextWidth(String text, int ascCharFormat, int chineseCharFormat) {
        if (TextUtils.isEmpty(text)) {
            return 0;
        }

        int width = 0;
        int ascCharWidth = getAscCharWidth(ascCharFormat);
        int chineseCharWidth = getChineseCharWidth(chineseCharFormat);

        char[] chars = text.toCharArray();
        for (char c : chars) {
            if (StringUtil.isChinese(c) || StringUtil.isChinesePunctuation(c)) {
                width += chineseCharWidth;
            } else {
                width += ascCharWidth;
            }
        }
        return width;
    }

    private int getAscCharWidth(int ascCharFormat) {
        switch (ascCharFormat) {
            case ReceiptTextFormat.FONT_SMALL:
                return 8;
            case ReceiptTextFormat.FONT_NORMAL:
                return 12;
            case ReceiptTextFormat.FONT_LARGE:
                return 16;
            default:
                return 12;
        }
    }

    private int getChineseCharWidth(int chineseCharFormat) {
        switch (chineseCharFormat) {
            case ReceiptTextFormat.FONT_SMALL:
                return 16;
            case ReceiptTextFormat.FONT_NORMAL:
                return 24;
            case ReceiptTextFormat.FONT_LARGE:
                return 32;
            default:
                return 24;
        }
    }

    private Format getReceiptTextFormat(int ascCharFormat, int chineseCharFormat) {
        Format format = new Format();
        switch (ascCharFormat) {
            case ReceiptTextFormat.FONT_SMALL:
                format.setAscSize(Format.AscSize.DOT16x8);
                format.setAscScale(Format.AscScale.SC1x1);
                break;
            case ReceiptTextFormat.FONT_NORMAL:
                format.setAscSize(Format.AscSize.DOT24x12);
                format.setAscScale(Format.AscScale.SC1x1);
                break;
            case ReceiptTextFormat.FONT_LARGE:
                format.setAscSize(Format.AscSize.DOT16x8);
                format.setAscScale(Format.AscScale.SC2x2);
                break;
            default:
                format.setAscSize(Format.AscSize.DOT24x12);
                format.setAscScale(Format.AscScale.SC1x1);
                break;
        }
        switch (chineseCharFormat) {
            case ReceiptTextFormat.FONT_SMALL:
                format.setHzSize(Format.HzSize.DOT16x16);
                format.setHzScale(Format.HzScale.SC1x1);
                break;
            case ReceiptTextFormat.FONT_NORMAL:
                format.setHzSize(Format.HzSize.DOT24x24);
                format.setHzScale(Format.HzScale.SC1x1);
                break;
            case ReceiptTextFormat.FONT_LARGE:
                format.setHzSize(Format.HzSize.DOT16x16);
                format.setHzScale(Format.HzScale.SC2x2);
                break;
            default:
                format.setHzSize(Format.HzSize.DOT24x24);
                format.setHzScale(Format.HzScale.SC1x1);
                break;
        }
        return format;
    }

    /**
     * 小票文本格式
     */
    public static class ReceiptTextFormat {
        /**
         * 字体大小
         */
        public static final int FONT_SMALL = 0;
        public static final int FONT_NORMAL = 1;
        public static final int FONT_LARGE = 2;

        /**
         * 单行文本内容，按照列顺序组装
         */
        private List<String> texts;
        /**
         * 西文单个字符宽度
         */
        private int ascCharFormat;
        /**
         * 中文单个字符宽度
         */
        private int chineseCharFormat;

        public ReceiptTextFormat(List<String> texts, int ascCharFormat, int chineseCharFormat) {
            this.texts = texts;
            this.ascCharFormat = ascCharFormat;
            this.chineseCharFormat = chineseCharFormat;
        }

        public List<String> getTexts() {
            return texts;
        }

        public void setTexts(List<String> texts) {
            this.texts = texts;
        }

        public int getAscCharFormat() {
            return ascCharFormat;
        }

        public void setAscCharFormat(int ascCharFormat) {
            this.ascCharFormat = ascCharFormat;
        }

        public int getChineseCharFormat() {
            return chineseCharFormat;
        }

        public void setChineseCharFormat(int chineseCharFormat) {
            this.chineseCharFormat = chineseCharFormat;
        }
    }
}
