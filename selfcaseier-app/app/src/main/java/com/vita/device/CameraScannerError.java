package com.vita.device;

public interface CameraScannerError {
    /** 成功 */
    int SUCCESS = 0x00;
    /** 其他异常 */
    int FAIL = 0xFF;

    int INIT_DECODER_FAIL = 0x01;
    int HAS_CREATED = 0x02;
    int OPEN_CAMERA_FAIL = 0x03;
    int LICENSE_FAIL = 0x04;
    int NOT_FOUND_DECODRE = 0x05;
}
