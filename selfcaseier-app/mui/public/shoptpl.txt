               {title}
        新时尚    新文化    新生活
                 {reprint}

单据类型：{orderType}
交 易 日：{orderDate}
小 票 号：{orderNo}
收 银 员：{userName}  
商铺名称：{shopName}  
总 小 计：{money}  
序 商品名称      数量    单价      成交价
------------------------------------------
<goodsList>
{index:2}{goodsCode:8}{goodsCount:r8}{goodsPrice:r12}{realPrice:r10}
{goodsName}
</>
------------------------------------------
支付方式       实付       找零       优惠
<payList>
{payName:10}{money:r9}{zlMoney:r11}{yhMoney:r11}
</>
[walkpaper:3]