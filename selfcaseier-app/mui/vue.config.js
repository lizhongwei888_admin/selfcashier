
// 使用等比适配插件
module.exports = {
  publicPath: './',
  outputDir: '../app/src/main/assets',
  pages: {
    index: {
      entry: 'src/main.js',
      template: 'public/index.html',
      filename: 'index.html',
      title: '星河自助POS'
    }
  },
  devServer: {
    open: false,
    port: 8080
  },
  css: {
    sourceMap: true,
    loaderOptions: {
      postcss: {
        plugins: [
          require('postcss-px2rem')({
            remUnit: 108
          })
        ]
      }
    }
  },
  productionSourceMap: false,
  configureWebpack: {
    devtool: 'source-map',
    performance: {
      hints: 'warning',
      maxEntrypointSize: 1024000,
      maxAssetSize: 1024000,
      assetFilter: function (assetFilename) {
        return assetFilename.endsWith('.js')
      }
    }
  }, 
}