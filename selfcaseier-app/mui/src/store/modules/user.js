import { login, logout } from '@/api/login'

const user = {
  namespaced: true,
  state: {
    userInfo: JSON.parse(sessionStorage.getItem(`userInfo`) || '{}')
  },

  mutations: {
    SET_USERINFO: (state, userInfo) => {
      sessionStorage.setItem(`userInfo`, JSON.stringify(userInfo))
      state.userInfo = userInfo
    }
  },

  actions: {
    // 登录
    Login ({ commit }, userInfo) {
      return new Promise((resolve, reject) => {
        login(userInfo)
          .then(response => {
            commit('SET_USERINFO', response.data[0])
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },
    // 退出
    LogOut ({ commit }) {
      return new Promise((resolve, reject) => {
        logout()
          .then(() => {
            commit('SET_USERINFO', {})
            resolve()
          })
          .catch(error => {
            reject(error)
          })
      })
    }
  }
}

export default user
