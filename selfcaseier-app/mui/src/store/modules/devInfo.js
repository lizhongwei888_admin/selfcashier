const devInfo = {
  namespaced: true,
  state: {
    deviceInfo: JSON.parse(sessionStorage.getItem(`deviceInfo`) || '{}')
  },

  mutations: {
    SET_DEVICEINFO: (state, deviceInfo) => {
      sessionStorage.setItem(`deviceInfo`, JSON.stringify(deviceInfo))
      state.deviceInfo = deviceInfo
    }
  }
}

export default devInfo
