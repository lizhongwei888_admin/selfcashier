const scanCode = {
    namespaced: true,
    state: {
        scanString: ''
    },
    mutations: {
        SET_SCANSTRING: (state, scanString) => {
            state.scanString = new Date().getTime() + '_' + scanString
        }
    }
}

export default scanCode