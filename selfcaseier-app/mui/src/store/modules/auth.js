const auth = {
  namespaced: true,
  state: {
    token: sessionStorage.getItem(`token`) || ''
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      sessionStorage.setItem(`token`, token)
      state.token = token
    }
  },

  actions: {
    // 登录
    SetToken({ commit }, token) {
      commit('SET_TOKEN', token)
    }
  }
}

export default auth
