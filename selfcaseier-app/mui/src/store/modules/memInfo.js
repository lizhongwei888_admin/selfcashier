const memInfo = {
  namespaced: true,
  state: {
    isLogin : sessionStorage.getItem(`isLogin`) || false,
    memberInfo: JSON.parse(sessionStorage.getItem(`memberInfo`) || '{}')
  },

  mutations: {
    SET_ISLOGIN:(state, isLogin) => {
      sessionStorage.setItem(`isLogin`, isLogin)
      state.isLogin = isLogin
    },
    SET_MEMBERINFO: (state, memberInfo) => {
      sessionStorage.setItem(`memberInfo`, JSON.stringify(memberInfo))
      state.memberInfo = memberInfo
    }
  }
}

export default memInfo

