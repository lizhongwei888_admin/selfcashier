const system = {
  namespaced: true,
  state: {
    systemInfo: JSON.parse(sessionStorage.getItem(`systemInfo`) || '{}'),
  },
  mutations: {
    SET_SYSTEMINFO: (state, systemInfo) => {
      sessionStorage.setItem(`systemInfo`, JSON.stringify(systemInfo))
      state.systemInfo = systemInfo
    },
  },
}

export default system
