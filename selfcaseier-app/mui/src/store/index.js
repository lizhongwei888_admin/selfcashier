import Vue from 'vue'
import Vuex from 'vuex'
import auth from './modules/auth'
import devInfo from './modules/devInfo'
import memInfo from './modules/memInfo'
import scanCode from './modules/scanCode'
import system from './modules/system'
import user from './modules/user'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    user,
    auth,
    memInfo,
    devInfo,
    scanCode,
    system
  },
  getters: {
    userInfo: state => state.user.userInfo,
    deviceInfo: state => state.devInfo.deviceInfo,
    memberInfo:state => state.memInfo.memberInfo,
    token: state => state.auth.token,
    systemInfo: state => state.system.systemInfo,
    scanString: state => state.scanCode.scanString,
    isLogin: state => state.memInfo.isLogin
  }
})

export default store
