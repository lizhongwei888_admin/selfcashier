import store from '@/store'
import { execMethod } from '@/utils/helper'
import NProgress from 'nprogress'; // progress bar
import 'nprogress/nprogress.css'; // progress bar style
import Vue from 'vue'
import router from './router'

NProgress.configure({ showSpinner: false }) // NProgress Configuration

// 是否有权限
const hasPermission = userPermission => {
  let userPermissionList = Array.isArray(userPermission)
    ? userPermission
    : [userPermission]
  let permissionList = store.getters.userInfo.permission
  return userPermissionList.some(
    e =>
      permissionList.length > 0 && permissionList.split(/,/g).includes(e)
  )
}

// 指令
Vue.directive('permission', {
  inserted: (el, binding) => {
    if (hasPermission(binding.expression)) {
      el.parentNode.removeChild(el)
    }
  }
})

const whiteList = ['/set','/inform','/index','/cashier','/pay','/payLoad','/payType','/payResult','/login','/menu','/printList','/cashInfo']
const handler = (to, from, next) => {
  if (Object.keys(store.getters.userInfo).length > 0) {
    if (to.path === '/set') {
      next({ path: '/' })
      NProgress.done()
    } else {
      next()
    }
  } else {
    if (whiteList.indexOf(to.path) !== -1) {
      next()
    } else {
      next(`/set?redirect=${to.path}`) // 否则全部重定向到登录页
      NProgress.done()
    }
  }
}

router.beforeEach((to, from, next) => {
  NProgress.start()
  if (whiteList.indexOf(to.path) !== -1) {
    next()
  } else {
    if (store.getters.token == '') {
      execMethod('home', 'getToken')
        .then(token => {
          if (token != '') {
            store.dispatch('SetToken', token)
            handler(to, from, next)
          } else {
            next(`/set?redirect=${to.path}`) // 否则全部重定向到登录页
            NProgress.done()
          }
        })
        .catch(() => {
          next(`/set?redirect=${to.path}`) // 否则全部重定向到登录页
          NProgress.done()
        })
    } else {
      handler(to, from, next)
    }
  }
})

router.afterEach(() => {
  NProgress.done()
})
