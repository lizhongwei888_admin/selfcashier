import Vue from 'vue'
import App from './App.vue'
import './plugins/element.js'
import ElementUI from 'element-ui'
import router from './router'
import store from './store'
import './utils/scanCode'
import './assets/styles/page.scss' // global css
import './assets/font/iconfont.css' // global css
import './assets/font/iconfont.js' // global css

import './permission' // permission control
import * as filters from './filters' // global filters
import echarts from 'echarts'
// import './utils/rem'
import VueScroller from 'vue-scroller'
import 'lib-flexible/flexible.js'
Vue.use(VueScroller)


Vue.prototype.$echarts = echarts
Vue.config.productionTip = false

ElementUI.Dialog.props.closeOnClickModal.default = false

Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')
