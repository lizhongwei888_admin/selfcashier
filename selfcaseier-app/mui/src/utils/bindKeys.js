let codeString = ''
let vueObj
let lastTime

export const setCodeString = (value) => {
  codeString = value
}

export const init = obj => {
  vueObj = obj
  window.addEventListener('load', bindLoadHandler)
  document.addEventListener('keydown', bindKeyDownHandler)
}

export const destroy = () => {
  vueObj = null
  window.removeEventListener('load', bindLoadHandler)
  document.removeEventListener('keydown', bindKeyDownHandler)
}

const bindLoadHandler = () => {
  //定时器每隔200ms 清空codeString
  setInterval(function () {
    let now = new Date().getTime()
    if (now - lastTime > 2000) {
      codeString = ''
    }
  }, 200)
}

const bindKeyDownHandler = (e) => {
  let nextTime = new Date().getTime()
  let code = e.key
  if (code == 'Enter') {
    let head = codeString.length >= 16 && codeString.length <= 24 ? parseInt(codeString.substr(0, 2)) : 0
    if ((vueObj.paytypeTabs == 'wx' &&
      head >= 10 &&
      head <= 15 &&
      codeString.length == 18) || (vueObj.paytypeTabs == 'zfb' && head >= 25 && head <= 30 &&
        codeString.length >= 16 && codeString.length <= 24)
    ) {
      if (vueObj.authCode.length == 18) {
        vueObj.$notify.warning({
          title: '警告',
          message: '正在支付中，不要重复扫码'
        })
      } else {
        vueObj.authCode = codeString
        codeString = ''
        vueObj.payOrder()
      }
      return
    } else if (vueObj.paytypeTabs == 'jh' && vueObj.payVisible && codeString) {
      vueObj.authCode = codeString
      codeString = ''
      vueObj.jhPay()
      return
    } else if (vueObj.paytypeTabs == 'fb' && vueObj.payVisible && codeString) {
      vueObj.authCode = codeString
      codeString = ''
      vueObj.payOrder()
      return
    } else if (vueObj.paytypeTabs == 'czk' && codeString) {
      vueObj.showCard(codeString)
      codeString = ''
      return
    } else if (vueObj.currentDiv == 'discount' && codeString) {
      vueObj.couponNo = codeString;
      vueObj.searchCoupon();
      codeString = ''
      return
    } else if (vueObj.refundVisible && codeString && vueObj.scanType == "order") {
      vueObj.orderNo = codeString.replace(/Shift/g, '');
      vueObj.searchOrder();
      codeString = ''
      return
    } else if (vueObj.paytypeTabs == 'goods' && codeString) {
      vueObj.getGoodsDetail(codeString)
      codeString = ''
      return
    }

    codeString = ''

  } else if (code == 'F4') {
    //挂单 F4
    vueObj.deity()
  } else if (code == 'F5') {
    //F5 取单
    vueObj.orderPend()
  } else if (code == 'F6') {
    //F6 选择会员
    vueObj.selectMer()
  } else if (code == 'F7') {
    //F8 优惠折扣
    vueObj.discount()
  } else if (code == 'F8') {
    //F8 手选商品
    vueObj.selectGoods()
  } else if (code == 'F9') {
    //F9 手输条码
    vueObj.moLing()
  } else if (code == 'F10') {
    //F10 直接付款
    vueObj.directPay()
  } else if (code == 'F11') {
    //付款 Enter
    vueObj.pay()
  } else if (code == 'F12') {
    //锁屏
    vueObj.btnLock()
  }
  // else {
  //   if (codeString == '') {
  //     codeString += code
  //   } else if (nextTime - lastTime <= 30) {
  //     codeString += code
  //   }
  // }
  lastTime = nextTime
}
