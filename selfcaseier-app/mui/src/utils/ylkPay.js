import { formatDate } from '@/utils/index'
import { insertPayType, queryPay } from '@/api/counter'
import { execMethod } from '@/utils/helper'
import { v4 as uuidv4 } from 'uuid'
import { showMsgDialog } from '@/utils/helper'

//银联卡支付
export const ylkPay = (vueObj) => {
  const ssMoney = vueObj.ssMoney
  if (ssMoney == 0) {
    showMsgDialog("请输入收款金额", "error")
    vueObj.closeLoad();
    vueObj.disabled = true;
    return;
  }
  if (parseFloat(ssMoney) > parseFloat(vueObj.ysMoney)) {

    showMsgDialog("应收需大于等于实收", "error")
    vueObj.closeLoad();
    vueObj.disabled = true;
    return
  }

  const that = vueObj;
  vueObj.showLoad("正在支付中");
  vueObj.loadingText = "等待支付中";
  vueObj.loadingCard = true;

  let orderNo = uuidv4().replace(/-/g, '');
  //更新订单状态
  execMethod('zb/bank', 'pay', (parseFloat(parseFloat(((ssMoney + '').replace(/,/g, ''))).toFixed(2)) * 100).toFixed(0), orderNo).then((payRes) => {
    if (payRes.code == '00') {
      vueObj.showLoad("正在上传支付结果");
      var payTypeList = [{ MC004: that.MC004, TU011: that.payType, TU010: that.TU010, TU002: 1, TU005: that.MC005, TU006: ssMoney, TU007: JSON.stringify(payRes), TU008: formatDate(new Date(), "yyyy-MM-dd HH:mm:ss"), TU009: '' }];
      insertPay(ssMoney, vueObj, payTypeList);
    } else {
      showMsgDialog(payRes.msg, "error")
      vueObj.disabled = true;
      vueObj.closeLoad();
      return
    }
  })
}
//聚合支付
export const jhPay = (vueObj, ssMoney, ysMoney, orderCode) => {
  if (ssMoney == 0) {
    showMsgDialog("请输入收款金额", "error")
    vueObj.closeLoad();
    return;
  }
  if (parseFloat(ssMoney) > parseFloat(ysMoney)) {
    showMsgDialog("应收需大于等于实收", "error")
    vueObj.closeLoad();
    return
  }
  const orderNo = orderCode + ((Math.random() * 9 + 1) * 10000).toFixed(0);
  //更新订单状态
  execMethod(
    'zb/bank',
    'codepay',
    (parseFloat(parseFloat(((ssMoney + '').replace(/,/g, ''))).toFixed(2)) * 100).toFixed(0),
    orderNo
  ).then((payRes) => {
    console.log(payRes)
    vueObj.queryPayRes(payRes, orderNo, ssMoney);
  })
}

export const insertPay = (ssMoney, vueObj, payTypeList) => {
  insertPayType({
    orgID: vueObj.$store.getters.userInfo.orgID,
    orderId: vueObj.orderId,
    userId: vueObj.$store.getters.userInfo.MB001,
    payTypeList: JSON.stringify(payTypeList)
  }).then(res => {
    if (res.result == "false") {
      showMsgDialog(res.msg, "error")
      return;
    }
    var have1 = false;
    vueObj.closePayCard();
    vueObj.payList.forEach((item) => {
      if (item.TU011 == vueObj.payType) {
        have1 = true
        item.TU006 = parseFloat(ssMoney) + parseFloat(item.TU006)
      }
    })
    if (!have1) {
      vueObj.payList.push({ TU005: vueObj.MC005, TU006: ssMoney, MC004: vueObj.MC004, TU011: vueObj.payType });
    }
    vueObj.ysMoney =  parseFloat((parseFloat(vueObj.ysMoney ) - parseFloat(ssMoney)).toFixed(2))
    vueObj.ssMoney = 0;
    vueObj.zlMoney = 0;
    vueObj.zfMoney = parseFloat((parseFloat(vueObj.zfMoney) + parseFloat(ssMoney)).toFixed(2));

    if (vueObj.ysMoney <= 0) {
      //更新订单状态
      vueObj.updateOrder();
    } else {
      vueObj.disabled = true;
      vueObj.ssMoney = vueObj.ysMoney;
      vueObj.closeLoad();
    }
  }).catch(e => {
    console.log(e);
    vueObj.closeLoad();
    vueObj.$confirm('网络异常，请重试?', '提示', {
      confirmButtonText: '重试',
      cancelButtonText: '取消'
    }).then(action => {
      if (action == 'confirm') {
        //确认 
        insertPay(ssMoney, vueObj, payTypeList)
      }
    }).catch(error => {
      if (error == 'cancel') {
        //取消
        vueObj.$confirm('建议不要取消，请记录当前支付信息，及时联系管理员！', '提示', {
          confirmButtonText: '确定',
          cancelButtonText: '取消'
        }).then(action => {
          if (action == 'confirm') {
            //确认 
            vueObj.disabled = true;
            vueObj.PayVisible = false;
            vueObj.payCardVisible = false;
            vueObj.clearData();
          }
        }).catch(error => {
          if (error == 'cancel') {
            //取消
            insertPay(ssMoney, vueObj, payTypeList)
          }
        });
      }
    });
  })
}

//条码查询支付结果
export const queryPayResult = (vueObj) => {
  if (vueObj.orderId == 0) {
    showMsgDialog("正在支付的订单才可以补录", "error")
    return
  }
  if (!vueObj.authCode || vueObj.authCode == -1) {
    showMsgDialog("请先扫码", "error")
    vueObj.authCode = '';
    return
  }


  vueObj.showLoad("正在查询支付结果");
  vueObj.disabled = true;

  queryPay({
    storeCode: vueObj.$store.getters.userInfo.mdCode, //商铺编码
    posId: vueObj.$store.getters.userInfo.MA004, // POS机编码
    cashier: vueObj.$store.getters.userInfo.MB005, // 收银员
    orderNo: vueObj.authCode, //付款编码
    orderType: ""
  }).then((payRes) => {
    if (payRes.result == "true") {
      vueObj.authCode = ''
      // payRes = JSON.parse(payRes);
      console.log(payRes);
      vueObj.showLoad("正在上传支付结果");
      let payInfo = payRes.data;
      let ssMoney = parseFloat(payInfo.data.factAmt) / 100;
      let payType = 12;
      if (payInfo.data.funds[0].name.indexOf("微信") >= 0) {
        //微信聚合
        payType = 13;
      } else if (payInfo.data.funds[0].name.indexOf("支付宝") >= 0) {
        //支付宝聚合
        payType = 12;
      } else if (payInfo.data.funds[0].name.indexOf("钱包") >= 0) {
        //钱包
        payType = 17;
      }
      let MC004 = "";
      let TU011 = "";
      let TU010 = "";
      let MC005 = "";

      vueObj.typeList.forEach((item) => {
        if (item.MC005 == payType) {
          TU011 = item.MC001
          TU010 = item.MC006;
          MC005 = item.MC005;
          MC004 = item.MC004;
        } else if (item.MC005 == 12 || item.MC005 == 13) {
          if (payType == 12 || payType == 13) {
            TU011 = item.MC001
            TU010 = item.MC006;
            MC005 = item.MC005;
            MC004 = item.MC004;
          }
        }

      })
      if (MC005 == "" || TU011 == "") {
        showMsgDialog("请先查询相关的支付方式是否存在", "error")
        vueObj.disabled = true;
        vueObj.closeLoad();
        return
      }


      vueObj.payType = TU011
      vueObj.TU010 = TU010;
      vueObj.MC005 = MC005;
      vueObj.MC004 = MC004;
      vueObj.paytypeTabs = vueObj.formatPayType(MC005)

      let payTypeList = [{ MC004: MC004, TU011: TU011, TU010: TU010, TU002: 1, TU005: MC005, TU006: ssMoney, TU007: JSON.stringify(payRes.data), TU008: formatDate(new Date(), "yyyy-MM-dd HH:mm:ss"), TU009: '' }];

      insertPay(ssMoney, vueObj, payTypeList);
      vueObj.disabled = true;
      vueObj.closeLoad();


    } else {
      showMsgDialog(payRes.msg, "error")
      vueObj.disabled = true;
      vueObj.closeLoad();
      return
    }
  })
}
