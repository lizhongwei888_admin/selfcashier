import { execMethod } from '@/utils/helper';

export const scan = () => {
  execMethod('scan', 'code', 1).then(codeString => {
    codeString = codeString.toString();
    console.log(codeString)
  }).catch(e => {
    console.log(e)
  });

}
