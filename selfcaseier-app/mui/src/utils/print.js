import { execMethod, showMsgDialog } from '@/utils/helper'
import { formatMoney } from '@/utils/index'
import { getOrderDetail, getOrderById, getOrderPayType, getProjectInfo, withdrawalOrderList } from '@/api/order'

export const print = async (orgID, projectId, orderId, isOne, giveCouponData, printNum) => {

  if (printNum == 0) {
    return;
  }
  let orderDetail = await getOrderDetail({
    orgID: orgID,
    projectId: projectId,
    orderId: orderId
  })
  let goodList = orderDetail.data
  let res = await getOrderById({
    orgID: orgID,
    projectId: projectId,
    orderId: orderId
  })

  let printInfo = await getProjectInfo({
    projectId: projectId,
    MA003: res.data[0].TQ042
  })

  let payRes = await getOrderPayType({
    TU002: 1,
    orderId: orderId
  })

  if (printInfo.data.TC030 == "" || printInfo.data.TC031 == "") {
    showMsgDialog("请联系管理员设置打印信息", "error")
    return;
  }

  let payTypeData = payRes.data
  let money = res.data[0].TQ017
  let TQ004 = res.data[0].TQ004
  let TQ029 = res.data[0].TQ029
  let TQ016 = res.data[0].TQ016
  let yh = res.data[0].yh
  let TQ013 = res.data[0].TQ013
  let TQ041 = res.data[0].TQ041
  let TQ040 = res.data[0].TQ040
  let MB005 = res.data[0].MB005
  let posno = res.data[0].posno

  money = TQ016 - yh;

  var text = '';
  if (!isOne)
    text = '重印\n';

  let y = 0
  const demoJson = [];

  let shopName = "";
  if (printInfo.shopName != "") {
    shopName = printInfo.shopName + '\n';
  }

  demoJson.push(
    {
      x: 2,
      y: 1,
      text: printInfo.data.TC030 + '\n' + shopName + text,
      align: 'middle',
      fontsize: 0
    },
    {
      x: 2,
      y: 2,
      text:
        '单据类型：销售单' +
        '\n交 易 日：' +
        TQ029 +
        '\n单    号：' +
        TQ004.substr(15) +
        '\n收银机号：' +
        posno +
        '\n收 银 员：' +
        MB005,
      align: 'left',
      fontsize: 0
    },
    {
      x: 2,
      y: 3,
      text: '--------------------------------',
      align: 'left',
      fontsize: 0
    }
  )
  y = 4;
  demoJson.push(
    {
      width: 10,
      y: y,
      text: '产品',
      fontsize: 0
    },
    {
      width: 8,
      y: y,
      text: '数量',
      fontsize: 0
    },
    {
      width: 8,
      y: y,
      text: '单价',
      fontsize: 0
    })

  var goodsCount = 0
  goodList.forEach(item => {
    goodsCount += parseFloat(item.TR007)
    y++
    demoJson.push(
      {
        width: 10,
        y: y,
        text: item.TL003,
        fontsize: 0
      },
      {
        width: 8,
        y: y,
        text: item.TR007,
        fontsize: 0
      },
      {
        width: 8,
        y: y,
        text: parseFloat(item.TR006).toFixed(2),
        fontsize: 0
      }
    )
    y++
    demoJson.push(
      {
        width: 12,
        y: y,
        text: item.TR005,
        fontsize: 0
      },
      {
        width: 14,
        y: y + 1,
        text: '成交金额',
        fontsize: 0
      },
      {
        width: 10,
        y: y + 1,
        text: '折扣金额',
        fontsize: 0
      },
      {
        width: 14,
        y: y + 2,
        text: parseFloat(item.TR016).toFixed(2),
        fontsize: 0
      },
      {
        width: 10,
        y: y + 2,
        text: parseFloat(item.TR021).toFixed(2),
        fontsize: 0
      }
    )

  })
  demoJson.push(
    {
      x: 2,
      y: y + 1,
      text: '--------------------------------',
      align: 'left',
      fontsize: 0
    },
    {
      x: 2,
      y: y + 2,
      text: '件数：',
      fontsize: 0
    },
    {
      x: 10,
      y: y + 2,
      text: goodsCount,
      fontsize: 0
    },
    {
      x: 2,
      y: y + 3,
      text: '应收：',
      fontsize: 0
    },
    {
      x: 10,
      y: y + 3,
      text: formatMoney(TQ016, 2),
      fontsize: 0
    },
    {
      x: 2,
      y: y + 4,
      text: '会员折扣：',
      fontsize: 0
    },
    {
      x: 10,
      y: y + 4,
      text: formatMoney(TQ041, 2),
      fontsize: 0
    },
    {
      x: 2,
      y: y + 5,
      text: '优惠券：',
      fontsize: 0
    },
    {
      x: 10,
      y: y + 5,
      text: formatMoney(TQ013, 2),
      fontsize: 0
    },
    {
      x: 2,
      y: y + 6,
      text: '促销优惠：',
      fontsize: 0
    },
    {
      x: 10,
      y: y + 6,
      text: formatMoney(TQ040, 2),
      fontsize: 0
    },
    {
      x: 2,
      y: y + 7,
      text: '实收：',
      fontsize: 0
    },
    {
      x: 10,
      y: y + 7,
      text: formatMoney(money, 2),
      fontsize: 0
    },
    {
      x: 2,
      y: y + 8,
      text: '--------------------------------',
      align: 'left',
      fontsize: 0
    },
    {
      width: 10,
      y: y + 9,
      text: '支付方式',
      fontsize: 0
    },
    {
      width: 8,
      y: y + 9,
      text: '实付',
      fontsize: 0
    },
    {
      width: 6,
      y: y + 9,
      text: '优惠',
      fontsize: 0
    },
    {
      width: 6,
      y: y + 9,
      text: '找零',
      fontsize: 0
    }

  )
  y = y + 9
  payTypeData.forEach(item => {
    y++
    demoJson.push({
      x: 2,
      y: y,
      text: item.MC004,
      fontsize: 0
    })
    y++
    demoJson.push(
      {
        width: 10,
        y: y,
        text: '',
        fontsize: 0
      },
      {
        width: 8,
        y: y,
        text: (item.TU005 == 1 ? (parseFloat(item.TU019) + parseFloat(item.TU006)).toFixed(2) : (parseFloat(item.TU006) - parseFloat(item.TU014 || 0)).toFixed(2)),
        fontsize: 0
      },
      {
        width: 6,
        y: y,
        text: parseFloat(item.TU014 || 0).toFixed(2),
        fontsize: 0
      },
      {
        width: 6,
        y: y,
        text: parseFloat(item.TU019).toFixed(2),
        fontsize: 0
      }
    )
  })


  demoJson.push(
    {
      x: 2,
      y: y + 1,
      text: '--------------------------------',
      align: 'left',
      fontsize: 0
    },
    {
      x: 2,
      y: y + 2,
      align: 'left',
      text: printInfo.data.TC031 + '\n',
      fontsize: 0
    },
    {
      x: 2,
      y: y + 3,
      text: TQ004,
      align: 'middle',
      type: 'barcode',
      width: 2,
      height: 64
    }
  )


  execMethod('print', 'print', printNum, demoJson).then(printRes => {
    if (printRes == "打印完成") {
      printCoupon(projectId, giveCouponData)
      showMsgDialog("打印成功", "success")
    } else if (printRes == "打印错误：240") {

      this.$confirm('打印机缺纸，请装纸后重试?', '提示', {
        confirmButtonText: '重试',
        cancelButtonText: '取消',
      })
        .then(() => {
          //确认
          print(orgID, projectId, orderId, isOne, giveCouponData, printNum);
          return;
        })
        .catch(() => {
          return;
        })
      return


    } else {
      showMsgDialog(printRes, "error")
    }
  });
}

export const printRefund = async (orgID, projectId, orderId, isone, printNum) => {
  let order = await getOrderById({
    orgID: orgID,
    projectId: projectId,
    orderId: orderId
  })
  let orderDetail = await getOrderDetail({
    orgID: orgID,
    projectId: projectId,
    orderId: orderId
  })

  let printInfo = await getProjectInfo({
    projectId: projectId,
    MA003: order.data[0].TQ042
  })
  if (printInfo.data.TC030 == "" || printInfo.data.TC031 == "") {
    showMsgDialog("请联系管理员设置打印信息", "error")
    return;
  }
  let goodList = orderDetail.data
  let money = order.data[0].TQ017
  let TQ004 = order.data[0].TQ004
  let TQ029 = order.data[0].TQ029
  let TQ016 = order.data[0].TQ016
  let MB005 = order.data[0].MB005
  let posno = order.data[0].posno
  let orderType = order.data[0].orderType == "1" ? "正常退款" : (order.data[0].orderType == "3" ? "部分退款" : "压卡退款")

  let payRes = await getOrderPayType({
    TU002: 2,
    orderId: orderId * -1

  })

  let payTypeData = payRes.data

  let shopName = "";
  if (printInfo.shopName != "") {
    shopName = printInfo.shopName + '\n';
  }
  if (isone)
    shopName = shopName + '重印\n'

  const demoJson = [
    {
      x: 2,
      y: 1,
      text: printInfo.data.TC030 + '\n' + shopName,
      align: 'middle',
      fontsize: 0
    },
    {
      x: 2,
      y: 2,
      text:
        '单据类型：' +
        orderType +
        '\n交 易 日：' +
        TQ029 +
        '\n单    号：' +
        TQ004 +
        '\n收银机号：' +
        posno +
        '\n收 银 员：' +
        MB005,
      align: 'left',
      fontsize: 0
    }, {
      x: 2,
      y: 3,
      text: '--------------------------------',
      align: 'left',
      fontsize: 0
    }
  ]
  let y = 4
  demoJson.push(
    {
      width: 10,
      y: y,
      text: '产品',
      fontsize: 0
    },
    {
      width: 8,
      y: y,
      text: '单价',
      fontsize: 0
    },
    {
      width: 5,
      y: y,
      text: '数量',
      fontsize: 0
    },
    {
      width: 8,
      y: y,
      text: '金额',
      fontsize: 0
    })

  var goodsCount = 0
  goodList.forEach(item => {
    goodsCount += parseFloat(item.TR007)
    y++

    demoJson.push({
      y: y,
      text: item.TR005,
      fontsize: 0
    })
    y++
    demoJson.push(
      {
        width: 10,
        y: y,
        text: '',
        fontsize: 0
      },
      {
        width: 8,
        y: y,
        text: parseFloat(item.TR015).toFixed(2),
        fontsize: 0
      },
      {
        width: 5,
        y: y,
        text: item.TR007,
        fontsize: 0
      },
      {
        width: 8,
        y: y,
        text: parseFloat(item.TR016).toFixed(2),
        fontsize: 0
      }
    )
  })

  demoJson.push(
    {
      x: 2,
      y: y + 1,
      text: '--------------------------------',
      align: 'left',
      fontsize: 0
    },
    {
      x: 2,
      y: y + 2,
      text: '件数：',
      fontsize: 0
    },
    {
      x: 10,
      y: y + 2,
      text: goodsCount,
      fontsize: 0
    },
    {
      x: 2,
      y: y + 3,
      text: '应退：',
      fontsize: 0
    },
    {
      x: 10,
      y: y + 3,
      text: formatMoney(TQ016, 2),
      fontsize: 0
    },
    {
      x: 2,
      y: y + 4,
      text: '实退：',
      fontsize: 0
    },
    {
      x: 10,
      y: y + 4,
      text: formatMoney(money, 2),
      fontsize: 0
    },
    {
      x: 2,
      y: y + 5,
      text: '--------------------------------',
      align: 'left',
      fontsize: 0
    },
    {
      width: 10,
      y: y + 6,
      text: '支付方式',
      fontsize: 0
    },
    {
      width: 12,
      y: y + 6,
      text: '实付',
      fontsize: 0
    },
    {
      width: 8,
      y: y + 6,
      text: '实退',
      fontsize: 0
    }
  )
  y = y + 7
  payTypeData.forEach(item => {
    y++
    demoJson.push({
      x: 2,
      y: y,
      text: item.MC004,
      fontsize: 0
    })

    y++
    demoJson.push(
      {
        width: 10,
        y: y,
        text: '',
        fontsize: 0
      },
      {
        width: 12,
        y: y,
        text: (parseFloat(item.TU006) - parseFloat(item.TU014 || 0)).toFixed(2),
        fontsize: 0
      },
      {
        width: 8,
        y: y,
        text: (parseFloat(item.TU006) - parseFloat(item.TU014 || 0)).toFixed(2),
        fontsize: 0
      }
    )
  })

  demoJson.push(
    {
      x: 2,
      y: y + 1,
      text: '--------------------------------',
      align: 'left',
      fontsize: 0
    },
    {
      x: 2,
      y: y + 2,
      align: 'left',
      text: printInfo.data.TC031 + '\n',
      fontsize: 0
    },
    {
      x: 2,
      y: y + 3,
      text: TQ004,
      align: 'middle',
      type: 'barcode',
      width: 2,
      height: 64
    }
  )
  execMethod('print', 'print', printNum, demoJson).then(printRes => {
    if (printRes == "打印完成") {
      showMsgDialog("打印成功", "success")
    } else if (printRes == "打印错误：240") {

      this.$confirm('打印机缺纸，请装纸后重试?', '提示', {
        confirmButtonText: '重试',
        cancelButtonText: '取消',
      })
        .then(() => {
          //确认
          printRefund(orgID, projectId, orderId, printNum);
          return;
        })
        .catch(() => {
          return;
        })
      return


    } else {
      showMsgDialog(printRes, "error")
    }
  });
}

export const printDayliSale = async (jsonData, projectId) => {

  let printInfo = await getProjectInfo({
    projectId: projectId
  })
  if (printInfo.data.TC030 == "" || printInfo.data.TC031 == "") {
    showMsgDialog("请联系管理员设置打印信息", "error")
    return;
  }
  const demoJson = [{
    y: 1,
    text: printInfo.data.TC030 + '\n',
    align: 'middle',
    fontsize: 0
  }]
  let width = 12
  var y = 2;
  for (let i = 0; i < jsonData.length; i++) {
    const item = jsonData[i]
    demoJson.push({
      y: y + 1.5,
      width: width,
      text: '收银机号：',
      align: 'left',
      fontsize: 0
    })
    demoJson.push({
      y: y + 1.5,
      text: item.syjName,
      align: 'left',
      fontsize: 0
    })

    demoJson.push({
      y: y + 2.5,
      width: width,
      text: '收银员号：',
      align: 'left',
      fontsize: 0
    })
    demoJson.push({
      y: y + 2.5,
      text: item.syName,
      align: 'left',
      fontsize: 0
    })

    demoJson.push({
      y: y + 3.5,
      width: width,
      text: '销售日期：',
      align: 'left',
      fontsize: 0
    })
    demoJson.push({
      y: y + 3.5,
      text: item.saleDate,
      align: 'left',
      fontsize: 0
    })

    demoJson.push({
      y: y + 4.5,
      width: width,
      text: '打印时间：',
      align: 'left',
      fontsize: 0
    })
    demoJson.push({
      y: y + 4.5,
      text: item.dyTime,
      align: 'left',
      fontsize: 0
    }, {
      y: y + 4.5,
      text: '\n--------------------------------',
      align: 'left',
      fontsize: 0
    })

    let j = 0
    for (j; j < item.payList.length; j++) {
      const subItem = item.payList[j]
      demoJson.push({
        y: y + 5.5 + j,
        width: width,
        text: subItem.payName,
        align: 'left',
        fontsize: 0
      })
      demoJson.push({
        y: y + 5.5 + j,
        text: formatMoney(subItem.sfMoney, 2),
        align: 'left',
        fontsize: 0
      })
    }

    demoJson.push({
      y: y + 4.5 + j,
      text: '\n--------------------------------',
      align: 'left',
      fontsize: 0
    })

    demoJson.push({
      y: y + 5.5 + j,
      width: width,
      text: '应收金额：',
      align: 'left',
      fontsize: 0
    })
    demoJson.push({
      y: y + 5.5 + j,
      text: formatMoney(item.yfMoney, 2),
      align: 'left',
      fontsize: 0
    })

    demoJson.push({
      y: y + 6.5 + j,
      width: width,
      text: '实收金额：',
      align: 'left',
      fontsize: 0
    })
    demoJson.push({
      y: y + 6.5 + j,
      text: formatMoney(item.sfMoney, 2),
      align: 'left',
      fontsize: 0
    })

    demoJson.push({
      y: y + 7.5 + j,
      width: width,
      text: '损溢金额：',
      align: 'left',
      fontsize: 0
    })
    demoJson.push({
      y: y + 7.5 + j,
      text: formatMoney(item.syMoney, 2),
      align: 'left',
      fontsize: 0
    })

    demoJson.push({
      y: y + 8.5 + j,
      width: width,
      text: '总优惠金额：',
      align: 'left',
      fontsize: 0
    })
    demoJson.push({
      y: y + 8.5 + j,
      text: formatMoney(item.yhMoney, 2),
      align: 'left',
      fontsize: 0
    })
    demoJson.push({
      y: y + 9.5 + j,
      width: width,
      text: "【优惠折扣】",
      align: 'left',
      fontsize: 0
    })
    demoJson.push({
      y: y + 10.5 + j,
      width: width,
      text: '  优惠券：',
      align: 'left',
      fontsize: 0
    })
    demoJson.push({
      y: y + 10.5 + j,
      text: formatMoney(item.couponMoney, 2),
      align: 'left',
      fontsize: 0
    })

    demoJson.push({
      y: y + 11.5 + j,
      width: width,
      text: '  商品折扣：',
      align: 'left',
      fontsize: 0
    })
    demoJson.push({
      y: y + 11.5 + j,
      text: formatMoney(item.zkMoney, 2),
      align: 'left',
      fontsize: 0
    })

    demoJson.push({
      y: y + 12.5 + j,
      width: width,
      text: '  促销优惠：',
      align: 'left',
      fontsize: 0
    })
    demoJson.push({
      y: y + 12.5 + j,
      text: formatMoney(item.cxMoney, 2),
      align: 'left',
      fontsize: 0
    })

    demoJson.push({
      y: y + 13.5 + j,
      width: width,
      text: '  会员折扣：',
      align: 'left',
      fontsize: 0
    })
    demoJson.push({
      y: y + 13.5 + j,
      text: formatMoney(item.memMoney, 2),
      align: 'left',
      fontsize: 0
    })
    let n = 0
    if (item.coupon.length > 0) {
      demoJson.push({
        y: y + 14.5 + j,
        width: width,
        text: "【优惠券详情】",
        align: 'left',
        fontsize: 0
      })
      for (n; n < item.coupon.length; n++) {
        const couponItem = item.coupon[n]
        demoJson.push({
          y: y + 15.5 + j + n,
          width: width,
          text: "  " + couponItem.name + "(" + couponItem.num + "张)：",
          align: 'left',
          fontsize: 0
        })
        demoJson.push({
          y: y + 15.5 + j + n,
          text: formatMoney(couponItem.money, 2),
          align: 'left',
          fontsize: 0
        })
      }
    }

    demoJson.push({
      y: y + 15.5 + j + n,
      text: '--------------------------------',
      align: 'left',
      fontsize: 0
    })

    demoJson.push({
      y: y + 16.5 + j + n,
      width: width,
      text: '销售笔数：',
      align: 'left',
      fontsize: 0
    })

    demoJson.push({
      y: y + 16.5 + j + n,
      text: item.bsNum,
      align: 'left',
      fontsize: 0
    })
    demoJson.push({
      y: y + 17.5 + j + n,
      width: width,
      text: '销售金额：',
      align: 'left',
      fontsize: 0
    })
    demoJson.push({
      y: y + 17.5 + j + n,
      text: formatMoney(item.sfMoney, 2),
      align: 'left',
      fontsize: 0
    })

    demoJson.push({
      y: y + 18.5 + j + n,
      width: width,
      text: '退货笔数：',
      align: 'left',
      fontsize: 0
    })
    demoJson.push({
      y: y + 18.5 + j + n,
      text: item.tkNum,
      align: 'left',
      fontsize: 0
    })
    demoJson.push({
      y: y + 19.5 + j + n,
      width: width,
      text: '退货金额：',
      align: 'left',
      fontsize: 0
    })
    demoJson.push({
      y: y + 19.5 + j + n,
      text: formatMoney(item.tkMoney, 2),
      align: 'left',
      fontsize: 0
    })
    demoJson.push({
      y: y + 20.5 + j + n,
      text: '\n--------------------------------',
      align: 'left',
      fontsize: 0
    })
    if (jsonData.length > 1) {
      demoJson.push({
        y: y + 22.5 + j + n,
        width: width,
        text: " ",
        fontsize: 0
      })
      y = y + 22.5 + j + n;
    }
  }
  execMethod('print', 'print', 1, demoJson).then(printRes => {
    if (printRes == "打印完成") {
      showMsgDialog("打印成功", "success")
    } else if (printRes == "打印错误：240") {
      this.$confirm('打印机缺纸，请装纸后重试?', '提示', {
        confirmButtonText: '重试',
        cancelButtonText: '取消',
      })
        .then(() => {
          //确认
          printDayliSale(jsonData, projectId);
          return;
        })
        .catch(() => {
          return;
        })

    } else {
      showMsgDialog(printRes, "error")
    }
  });
}

export const printCoupon = async (projectId, giveCouponData) => {


  let printInfo = await getProjectInfo({
    projectId: projectId
  })

  const couponJson = [];
  let j = 0;
  if (giveCouponData && giveCouponData.length > 0) {
    giveCouponData.forEach(item => {
      j = j + 2;
      couponJson.push({
        x: 2,
        y: j++,
        text: '--------------------------------',
        align: 'left',
        fontsize: 0
      }, {
        x: 2,
        y: j++,
        text: '--------------------------------',
        align: 'left',
        fontsize: 0
      }, {
        x: 2,
        y: j++,
        text: printInfo.data.TC030 + '\n',
        align: 'middle',
        fontsize: 0
      }, {
        x: 2,
        y: j++,
        text: "券号：" + item.couponNo,
        align: 'left'
      }, {
        x: 2,
        y: j++,
        text: '发券日期：' +
          item.giftDate,
        align: 'left',
        fontsize: 0
      }, {
        x: 2,
        y: j++,
        text: '--------------------------------',
        align: 'left',
        fontsize: 0
      }, {
        x: 2,
        y: j++,
        text: item.couponTitle + "：" + item.couponMoney,
        align: 'left'
      }, {
        x: 2,
        y: j++,
        text: '--------------------------------',
        align: 'left',
        fontsize: 0
      }, {
        x: 2,
        y: j++,
        text: item.couponDes + "\n",
        align: 'left'
      }, {
        x: 2,
        y: j++,
        text: "使用开始日期：" + item.couponStart.split(" ")[0],
        align: 'left'
      }, {
        x: 2,
        y: j++,
        text: "使用结束日期：" + item.couponEnd.split(" ")[0],
        align: 'left'
      }, {
        x: 2,
        y: j++,
        text: item.couponNo,
        align: 'middle',
        width: 200,
        height: 200,
        type: 'qrcode'
      })
    })
    execMethod('print', 'print', 1, couponJson).then(printRes => {
      if (printRes == "打印完成") {
        showMsgDialog("打印成功", "success")
      } else if (printRes == "打印错误：240") {
        this.$confirm('打印机缺纸，请装纸后重试?', '提示', {
          confirmButtonText: '重试',
          cancelButtonText: '取消',
        })
          .then(() => {
            //确认
            printCoupon(projectId, giveCouponData);
            return;
          })
          .catch(() => {
            return;
          })


      } else {
        showMsgDialog(printRes, "error")
      }
    });
  }

}


export const printVoucher = async (orderNo, projectId) => {


  let printInfo = await getProjectInfo({
    projectId: projectId
  })

  let orderInfo = await withdrawalOrderList({
    orderNo: orderNo
  })


  const couponJson = [];
  let j = 2;
  couponJson.push({
    x: 2,
    y: j++,
    text: printInfo.data.TC030 + '\n',
    align: 'middle',
    fontsize: 0
  }, {
    x: 2,
    y: j++,
    text: '--------------------------------',
    align: 'left',
    fontsize: 0
  }, {
    x: 2,
    y: j++,
    text: "单据类型：挂单票",
    align: 'left',
    fontsize: 0.5
  }, {
    x: 2,
    y: j++,
    text: "挂 单 号：" + orderInfo.data[0].MD004.substr(10),
    align: 'left',
    fontsize: 0.5
  }, {
    x: 2,
    y: j++,
    text: "交 易 日：" + orderInfo.data[0].create_time,
    align: 'left',
    fontsize: 0.5
  }, {
    x: 2,
    y: j++,
    text: "收银机号：" + orderInfo.data[0].posno,
    align: 'left',
    fontsize: 0.5
  }, {
    x: 2,
    y: j++,
    text: "收 银 员：" + orderInfo.data[0].posname,
    align: 'left',
    fontsize: 0.5
  }, {
    x: 2,
    y: j++,
    text: "交易金额：" + formatMoney(orderInfo.data[0].MD017, 2),
    align: 'left',
    fontsize: 0.5
  }, {
    x: 2,
    y: j++,
    text: '--------------------------------',
    align: 'left',
    fontsize: 0
  }, {
    x: 2,
    y: j + 13,
    text: "请在三小时内到总收银台缴款",
    align: 'left',
    fontsize: 0.5
  }, {
    x: 2,
    y: j++,
    text: '--------------------------------',
    align: 'left',
    fontsize: 0
  }, {
    x: 2,
    y: j++,
    text: orderInfo.data[0].MD004,
    align: 'middle',
    type: 'barcode',
    width: 2,
    height: 64
  })
  couponJson.push({
    y: j + 14,
    text: '\n--------------------------------',
    align: 'left',
    fontsize: 0
  })
  execMethod('print', 'print', 1, couponJson).then(printRes => {
    if (printRes == "打印完成") {
      showMsgDialog("打印成功", "success")
    } else if (printRes == "打印错误：240") {
      this.$confirm('打印机缺纸，请装纸后重试?', '提示', {
        confirmButtonText: '重试',
        cancelButtonText: '取消',
      })
        .then(() => {
          //确认
          printVoucher(orderNo, projectId);
          return;
        })
        .catch(() => {
          return;
        })
    } else {
      showMsgDialog(printRes, "error")
    }
  });


}