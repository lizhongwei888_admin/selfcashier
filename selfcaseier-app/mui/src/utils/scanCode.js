import store from '@/store'

let str = ''
let timeArr = [0, 0]

const scanCode = () => {
    window.addEventListener('keydown', bindKeyDownHandler)
}

const bindKeyDownHandler = (event) => {
    if (new Date().getTime() - timeArr[1] > 40) {
        str = ''
        timeArr = [0, 0]
    }

    if (str.length % 2 != 0) {
        timeArr[0] = new Date().getTime();
    } else {
        timeArr[1] = new Date().getTime();
    }


    if (event.keyCode !== 13) {
        if (event.keyCode >= 32 && event.keyCode <= 126) {
            let k = String.fromCharCode(event.keyCode)
            str += k
        }
    } else {
        if (str) {
            store.commit('scanCode/SET_SCANSTRING', str)
        }
        str = ''
        timeArr = [0, 0]
    }
}

export default scanCode()