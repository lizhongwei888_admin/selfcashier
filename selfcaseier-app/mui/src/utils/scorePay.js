
import { formatDate } from '@/utils/index'
import { insertPayType } from '@/api/counter'
import { showMsgDialog } from '@/utils/helper'

export const useIntegralPay = (vueObj) => {
  if (!vueObj.isSaveCard) {
    return;
  }
  if (vueObj.scoreMoney < vueObj.scoreDis) {
    showMsgDialog('抵扣积分至少大于：' + vueObj.scoreDis, "error")
    vueObj.closeLoad();
    vueObj.disabled = true;
    return;
  }
  if (vueObj.payMoney == 0) {
    showMsgDialog("请输入需要抵扣的积分", "error")
    vueObj.closeLoad();
    vueObj.disabled = true;
    return;
  }
  if (vueObj.scoreMoney > vueObj.MB005) {
    showMsgDialog("使用积分不能大于会员积分", "error")
    vueObj.closeLoad();
    vueObj.disabled = true;
    return;
  }
  if (vueObj.payMoney > vueObj.ysMoney) {
    showMsgDialog("积分抵扣金额不能大于应收金额", "error")
    vueObj.closeLoad();
    vueObj.disabled = true;
    return;
  }
  vueObj.isSaveCard = false;
  vueObj.showLoad("正在支付中");
  let payTypeList = [{ TU021: vueObj.scoreMoney, MC004: vueObj.MC004, TU011: vueObj.payType, TU010: vueObj.TU010, TU002: 1, TU005: 8, TU006: vueObj.payMoney, TU007: "", TU008: formatDate(new Date(), "yyyy-MM-dd HH:mm:ss"), TU009: "" }];
  //插入支付明细
  scoreInsertPayType(vueObj, payTypeList);
}
export const scoreInsertPayType = (vueObj, payTypeList) => {
  insertPayType({
    orgID: vueObj.$store.getters.userInfo.orgID,
    orderId: vueObj.orderId,
    userId: vueObj.$store.getters.userInfo.MB001,
    payTypeList: JSON.stringify(payTypeList)
  }).then(async res => {
    if (res.result == "false") {
      showMsgDialog(res.msg, "error")
      return;
    }
    vueObj.scoreTotal += vueObj.payMoney;
    vueObj.payList.push({ MC004: vueObj.MC004, TU011: vueObj.payType, TU005: vueObj.MC005, TU006: vueObj.payMoney })
    if ((vueObj.ysMoney - vueObj.payMoney) <= 0)
      vueObj.ysMoney = 0
    else
      vueObj.ysMoney = vueObj.ysMoney - vueObj.payMoney;
    vueObj.payMoney = 0;
    vueObj.zlMoney = 0;
    vueObj.closeUseIntegral();
    if (vueObj.ysMoney <= 0) {
      vueObj.updateOrder()
    } else {
      vueObj.payMoney = vueObj.ysMoney;
      vueObj.closeLoad();
    }
  }).catch(e => {
    console.log(e);
    vueObj.closeLoad();
    vueObj.$confirm('网络异常，请重试?', '提示', {
      confirmButtonText: '重试',
      cancelButtonText: '取消'
    }).then(() => {
      scoreInsertPayType(vueObj, payTypeList)
    }).catch(() => {
      //取消
      vueObj.$confirm('建议不要取消，请记录当前支付信息，及时联系管理员！', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消'
      }).then(() => {
        //确认 
        vueObj.disabled = true;
        vueObj.PayVisible = false;
        vueObj.payCardVisible = false;
        vueObj.clearData();
      }).catch(() => {
        scoreInsertPayType(vueObj, payTypeList)
      });
    });
  });
}
