import { showMsgDialog } from '@/utils/helper'

import { formatDate } from '@/utils/index'
import { insertPay } from '@/utils/scanPay'

//现金支付
export const xjPay = (vueObj, payMoney, ysMoney, paytypeTabs, MC004, payType, TU010, MC005, TU002) => {
  if (payMoney == 0 && ysMoney != 0) {
    showMsgDialog("请输入收款金额", "error")
    // Message({
    //   message: '请输入收款金额',
    //   type: 'error',
    //   duration: 2 * 1000,
    // })
    vueObj.closeLoad();
    vueObj.disabled = true;
    return;
  }
  if (!payMoney) {
    showMsgDialog("请重新输入收款金额", "error")
    // Message({
    //   message: '请重新输入收款金额',
    //   type: 'error',
    //   duration: 2 * 1000,
    // })
    vueObj.closeLoad();
    vueObj.disabled = true;
    return;
  }
  if (vueObj.paytypeTabs != "xj") {
    if (parseFloat(payMoney) > parseFloat(ysMoney)) {
      showMsgDialog("应收需大于等于实收", "error")
      // Message({
      //     message: '应收需大于等于实收',
      //     type: 'error',
      //     duration: 2 * 1000,
      //   })
      vueObj.closeLoad();
      vueObj.disabled = true;
      return
    }
  }

  if (parseFloat(payMoney) > parseFloat(ysMoney) && paytypeTabs == "sg") {
    showMsgDialog("应收需大于等于实收", "error")
    // Message({
    //   message: '应收需大于等于实收',
    //   type: 'error',
    //   duration: 2 * 1000,
    // })
    vueObj.disabled = true;
    vueObj.closeLoad();
    return;
  }
  vueObj.showLoad("正在支付中");
  var payTypeList = [{ TU002: TU002, TU020: payMoney, TU019: (payMoney - ysMoney) > 0 ? payMoney - ysMoney : 0, MC004: MC004, TU011: payType, TU010: TU010, TU005: MC005, TU006: (payMoney - ysMoney) > 0 ? ysMoney : payMoney, TU007: "", TU008: formatDate(new Date(), "yyyy-MM-dd HH:mm:ss"), TU009: '' }];
  //插入支付明细
  insertPay(payMoney, vueObj, payTypeList);
}



