import store from '@/store'
import { showMsgDialog } from '@/utils/helper'
import axios from 'axios'
import { v4 as uuidv4 } from 'uuid'


const service = axios.create({
  baseURL: '/api',
  timeout: 60000
})

service.interceptors.request.use(
  config => {
    config.headers['token'] = store.getters.token

    if (config.method == 'post') {
      for (const key in config.data) {
        if (config.data[key] == null) {
          delete config.data[key]
        }
      }
      // config.data = {
      //   ...config.data
      // }
      let rid = uuidv4()
      config.headers['ajax_guid'] = rid
      window.android.setRid(
        rid,
        JSON.stringify(config.data),
        JSON.stringify(config.headers)
      )
    } else if (config.method == 'get') {
      for (const key in config.params) {
        if (config.params[key] == null) {
          delete config.params[key]
        }
      }
      // config.params = {
      //   ...config.params
      // }
    }
    return config
  },
  error => {
    console.log(error)
    Promise.reject(error)
  }
)

service.interceptors.response.use(
  response => {
    const res = response.data;
    return res
    // if (res.code === 200 || res.code == "0000" || res.retCode == '00') {
    //   return res
    // }
    // if (res.errno === 501) {
    //   MessageBox.alert('系统未登录，请重新登录', '错误', {
    //     confirmButtonText: '确定',
    //     type: 'error'
    //   }).then(() => {
    //     store.dispatch('LogOut').then(() => {
    //       location.reload()
    //     })
    //   })
    //   return Promise.reject('error')
    // } else if (res.errno === 502) {
    //   MessageBox.alert('系统内部错误，请联系管理员维护', '错误', {
    //     confirmButtonText: '确定',
    //     type: 'error'
    //   })
    //   return Promise.reject('error')
    // } else if (res.errno === 503) {
    //   MessageBox.alert('请求业务目前未支持', '警告', {
    //     confirmButtonText: '确定',
    //     type: 'error'
    //   })
    //   return Promise.reject('error')
    // } else if (res.errno === 504) {
    //   MessageBox.alert('更新数据已经失效，请刷新页面重新操作', '警告', {
    //     confirmButtonText: '确定',
    //     type: 'error'
    //   })
    //   return Promise.reject('error')
    // } else if (res.errno === 505) {
    //   MessageBox.alert('更新失败，请再尝试一次', '警告', {
    //     confirmButtonText: '确定',
    //     type: 'error'
    //   })
    //   return Promise.reject('error')
    // } else if (res.errno === 506) {
    //   MessageBox.alert('没有操作权限，请联系管理员授权', '错误', {
    //     confirmButtonText: '确定',
    //     type: 'error'
    //   })
    //   return Promise.reject('error')
    // } else if (res.errno !== 0) {
    //   return Promise.reject(response)
    // } else {
    //   return response
    // }
  },
  error => {
    console.log('err' + error)
    showMsgDialog("登录连接超时（后台不能连接，请联系系统管理员）", "error")
    return Promise.reject(error)
  }
)

export default service
