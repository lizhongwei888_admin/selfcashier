import { execMethod, showMsgDialog } from '@/utils/helper'
import { formatMoney } from '@/utils/index'
import { getOrderDetail, getOrderById, getProjectInfo, withdrawalOrderList, getOrderPayType, getOrderPayCoupon } from '@/api/order'

export const print = async (orgID, projectId, orderId, isOne, giveCouponData, printNum) => {

  let orderDetail = await getOrderDetail({
    orgID: orgID,
    projectId: projectId,
    orderId: orderId
  })
  let goodList = orderDetail.data
  let res = await getOrderById({
    orgID: orgID,
    projectId: projectId,
    orderId: orderId
  })

  let printInfo = await getProjectInfo({
    projectId: projectId
  })

  let payRes = await getOrderPayType({
    TU002: 1,
    orderId: orderId
  })

  let couponRes = await getOrderPayCoupon({
    orderId: orderId
  })
  let couponData = couponRes.data;

  if (printInfo.data.TC030 == "" || printInfo.data.TC031 == "") {
    showMsgDialog("请联系管理员设置打印信息", "error")
    return;
  }

  let payTypeData = payRes.data
  let money = res.data[0].TQ054
  let TQ004 = res.data[0].TQ004
  let TQ029 = res.data[0].TQ029
  let TQ016 = res.data[0].TQ016
  let yh = 0
  let TQ013 = res.data[0].TQ013
  let TQ041 = res.data[0].TQ041
  let TQ040 = res.data[0].TQ040
  let MB005 = res.data[0].MB005
  let posno = res.data[0].posno
  let memCardNo = res.data[0].memCardNo
  let integral = res.data[0].integral
  let isOpenBox = false;

  let printJson = {};
  let goodsCount = 0;
  let goodsList = [], payList = [], cardList = [], shopList = [], couponList = [];
  goodList.forEach((item, index) => {
    if (shopList.indexOf(item.TR019) < 0)
      shopList.push(item.TR019);
    goodsCount += parseFloat(item.TR007)
    let goodsInfo = {};
    goodsInfo.index = index + 1;
    goodsInfo.goodsCode = item.TR004;
    goodsInfo.goodsCode = item.TR004;
    goodsInfo.goodsCount = item.TR007;
    goodsInfo.goodsName = item.TR005;
    goodsInfo.shopName = item.TJ003 || item.TR020;
    goodsInfo.goodsPrice = parseFloat(item.TR006).toFixed(2);
    goodsInfo.realPrice = parseFloat(item.TR015).toFixed(2);
    goodsInfo.payMoney = (parseFloat(item.TR015) * parseFloat(item.TR007)).toFixed(2);
    goodsInfo.zkMoney = parseFloat(item.TR021).toFixed(2);
    goodsList.push(goodsInfo);
  })
  if (couponData.length > 0) {
    couponData.forEach((item) => {
      let couponInfo = {};
      couponInfo.couponName = item.TA005;
      couponInfo.couponCode = item.TF015;
      if (item.TA006 == 1)
        couponInfo.money = item.TA007 + '元'
      if (item.TA006 == 2)
        couponInfo.money = item.TA007 + '折'

      couponList.push(couponInfo);
    })
  }
  printJson.couponList = couponList;

  payTypeData.forEach(item => {
    let payInfo = {};
    payInfo.payName = item.MC004;
    if (item.TU005 == 3 && item.TU007) {
      let bankNo = "";
      if (item.MC010 == 'boc')
        bankNo = JSON.parse(item.TU007).result.substr(0, 21).trim();
      else
        bankNo = JSON.parse(JSON.parse(item.TU007).result)[0].sourceId;
      payInfo.payName = item.MC004 + '(' + bankNo.substr(-4) + ')';
    }
    if (item.TU005 == 1) {
      isOpenBox = true;
      payInfo.money = (parseFloat(item.TU019) + parseFloat(item.TU006)).toFixed(2);
    } else
      payInfo.money = (parseFloat(item.TU006) - parseFloat(item.TU014 || 0)).toFixed(2);

    payInfo.yhMoney = parseFloat(item.TU014 || 0).toFixed(2);
    payInfo.zlMoney = parseFloat(item.TU019 || 0).toFixed(2);


    yh += parseFloat(item.TU014 || 0);

    payList.push(payInfo);

    let cardInfo = {};
    if (item.TU009) {
      cardInfo.cardNo = item.TU009;
      cardInfo.amount = item.TU012;
      cardList.push(cardInfo);
    }
  })
  money = money - yh;

  var text = '';
  if (!isOne) {
    text = '重印';
    isOpenBox = false;
  }

  printJson.title = printInfo.data.TC030;
  printJson.count = parseFloat(goodsCount).toFixed(2);
  printJson.createTime = TQ029;
  printJson.cashNo = posno;
  printJson.userName = MB005;
  printJson.orderType = "零售销售";
  printJson.orderNo = TQ004;
  printJson.memCardNo = memCardNo;
  printJson.integral = integral;
  printJson.ysMoney = formatMoney(TQ016, 2);
  printJson.ssMoney = formatMoney(money, 2);
  printJson.yhMoney = formatMoney(TQ040, 2);
  printJson.couponMoney = formatMoney(TQ013, 2);
  printJson.memMoney = formatMoney(TQ041, 2);
  printJson.ticketInfo = printInfo.data.TC031;
  printJson.reprint = text;
  printJson.goodsList = goodsList;
  printJson.payList = payList;
  printJson.cardList = cardList;
  let paper = JSON.parse(localStorage.getItem("appInfo")).print_paper;

  execMethod('print', 'printTpl', printNum, printJson, "template" + paper).then(() => {

    if (isOpenBox) {
      execMethod('print', 'openCashBox');
    }

    if (giveCouponData.length > 0) {
      printCoupon(projectId, giveCouponData);
    }
  }).catch(() => {
    print(orgID, projectId, orderId, isOne, giveCouponData, printNum)
  });
}

export const printRefund = async (orgID, projectId, orderId, isOne, printNum) => {
  let order = await getOrderById({
    orgID: orgID,
    projectId: projectId,
    orderId: orderId
  })
  let orderDetail = await getOrderDetail({
    orgID: orgID,
    projectId: projectId,
    orderId: orderId
  })

  let payRes = await getOrderPayType({
    TU002: 2,
    orderId: orderId * -1

  })



  let printInfo = await getProjectInfo({
    projectId: projectId
  })
  if (printInfo.data.TC030 == "" || printInfo.data.TC031 == "") {
    showMsgDialog("请联系管理员设置打印信息", "error")
    return;
  }
  let goodList = orderDetail.data
  let payTypeData = payRes.data
  let money = order.data[0].TQ017
  let TQ004 = order.data[0].TQ004
  let TQ029 = order.data[0].TQ029
  let TQ016 = order.data[0].TQ016
  let MB005 = order.data[0].MB005
  let posno = order.data[0].posno
  let TQ013 = order.data[0].TQ013
  let TQ041 = order.data[0].TQ041
  let TQ040 = order.data[0].TQ040
  let memCardNo = order.data[0].memCardNo
  let integral = order.data[0].integral

  let couponRes = await getOrderPayCoupon({
    orderId: order.data[0].orderId
  })
  let couponData = couponRes.data;


  let printJson = {};
  let goodsCount = 0;
  let goodsList = [], payList = [], cardList = [], couponList = [];
  goodList.forEach((item, index) => {
    goodsCount += parseFloat(item.TR007)
    let goodsInfo = {};
    goodsInfo.index = index + 1;
    goodsInfo.goodsCode = item.TR004;
    goodsInfo.goodsCode = item.TR004;
    goodsInfo.goodsCount = item.TR007;
    goodsInfo.goodsName = item.TR005;
    goodsInfo.shopName = item.TJ003;
    goodsInfo.goodsPrice = parseFloat(item.TR006).toFixed(2);
    goodsInfo.realPrice = parseFloat(item.TR015).toFixed(2);
    goodsInfo.payMoney = (parseFloat(item.TR015) * parseFloat(item.TR007)).toFixed(2);
    goodsInfo.zkMoney = parseFloat(item.TR021).toFixed(2);
    goodsList.push(goodsInfo);
  })

  if (couponData.length > 0) {
    couponData.forEach((item) => {
      let couponInfo = {};
      couponInfo.couponName = item.TA005;
      couponInfo.couponCode = item.TF015;
      if (item.TA006 == 1)
        couponInfo.money = item.TA007 + '元'
      if (item.TA006 == 2)
        couponInfo.money = item.TA007 + '折'

      couponList.push(couponInfo);
    })
  }
  printJson.couponList = couponList;

  payTypeData.forEach(item => {
    let payInfo = {};
    payInfo.payName = item.MC004;
    if (item.TU005 == 3 && item.TU007) {
      let bankNo = "";
      if (item.MC010 == 'boc')
        bankNo = JSON.parse(item.TU007).result.substr(0, 21).trim();
      else
        bankNo = JSON.parse(JSON.parse(item.TU007).result)[0].sourceId;
      payInfo.payName = item.MC004 + '(' + bankNo.substr(-4) + ')';
    }
    if (item.TU005 == 1) {
      payInfo.money = (parseFloat(item.TU019) + parseFloat(item.TU006)).toFixed(2);
    }
    else
      payInfo.money = (parseFloat(item.TU006) - parseFloat(item.TU014 || 0)).toFixed(2);

    payInfo.yhMoney = parseFloat(item.TU014 || 0).toFixed(2);
    payInfo.zlMoney = parseFloat(item.TU019 || 0).toFixed(2);


    payList.push(payInfo);

    let cardInfo = {};
    if (item.TU009) {
      cardInfo.cardNo = item.TU009;
      cardInfo.amount = item.TU012;
      cardList.push(cardInfo);
    }
  })

  var text = '';
  if (!isOne) {
    text = '重印';
  }

  printJson.title = printInfo.data.TC030;
  printJson.count = parseFloat(goodsCount).toFixed(2);
  printJson.createTime = TQ029;
  printJson.cashNo = posno;
  printJson.userName = MB005;
  printJson.orderType = "退款单";
  printJson.orderNo = TQ004;
  printJson.memCardNo = memCardNo;
  printJson.integral = integral;
  printJson.ysMoney = formatMoney(money, 2);
  printJson.ssMoney = formatMoney(TQ016, 2);
  printJson.yhMoney = formatMoney(TQ040, 2);
  printJson.couponMoney = formatMoney(TQ013, 2);
  printJson.memMoney = formatMoney(TQ041, 2);
  printJson.ticketInfo = printInfo.data.TC031;
  printJson.reprint = text;
  printJson.goodsList = goodsList;
  printJson.payList = payList;
  printJson.cardList = cardList;
  let paper = JSON.parse(localStorage.getItem("appInfo")).print_paper;
  execMethod('print', 'printTpl', printNum, printJson, "refundtpl" + paper).then(() => {
    showMsgDialog("打印成功", "success")
    return
  }).catch(() => {
    printRefund(orgID, projectId, orderId, isOne, printNum)
  });
}

export const printDayliSale = async (jsonData, projectId) => {
  let printInfo = await getProjectInfo({
    projectId: projectId
  })
  if (printInfo.data.TC030 == '' || printInfo.data.TC031 == '') {
    showMsgDialog("请联系管理员设置打印信息", "error")

    return
  }

  for (let i = 0; i < jsonData.length; i++) {
    let printJson = {};
    const item = jsonData[i]
    printJson.orderType = "日结单";
    printJson.syjName = item.syjName;
    printJson.syName = item.syName;
    printJson.saleDate = item.saleDate;
    printJson.dyTime = item.dyTime;
    let j = 0
    let payList = [];
    for (j; j < item.payList.length; j++) {
      const subItem = item.payList[j]
      let payInfo = {};
      payInfo.payName = subItem.payName;
      payInfo.money = formatMoney(subItem.sfMoney, 2);
      payList.push(payInfo);
    }
    printJson.payList = payList;


    printJson.yfMoney = formatMoney(item.yfMoney, 2);
    printJson.sfMoney = formatMoney(item.sfMoney, 2);
    printJson.syMoney = formatMoney(item.syMoney, 2);
    printJson.yhMoney = formatMoney(item.yhMoney, 2);
    printJson.couponMoney = formatMoney(item.couponMoney, 2);
    printJson.zkMoney = formatMoney(item.zkMoney, 2);
    printJson.cxMoney = formatMoney(item.cxMoney, 2);
    printJson.memMoney = formatMoney(item.memMoney, 2);
    printJson.bsNum = item.bsNum;
    printJson.sfMoney = formatMoney(item.sfMoney, 2);
    printJson.tkNum = item.tkNum;
    printJson.tkMoney = formatMoney(item.tkMoney, 2);
    printJson.title = printInfo.data.TC030;
    printJson.ticketInfo = printInfo.data.TC031;
    let paper = JSON.parse(localStorage.getItem("appInfo")).print_paper;
    execMethod('print', 'printTpl', 1, printJson, 'daysale' + paper)
  }

}

export const printCoupon = async (projectId, giveCouponData) => {

  let printInfo = await getProjectInfo({
    projectId: projectId
  })

  if (giveCouponData && giveCouponData.length > 0) {
    giveCouponData.forEach(item => {
      item.title = printInfo.data.TC030;
      item.couponStart = item.couponStart.split(" ")[0]
      item.couponEnd = item.couponEnd.split(" ")[0]
      let paper = JSON.parse(localStorage.getItem("appInfo")).print_paper;
      execMethod('print', 'printTpl', 1, item, "coupontpl" + paper)
    })
  }

}


export const printVoucher = async (orderNo, projectId) => {


  let printInfo = await getProjectInfo({
    projectId: projectId
  })

  let orderInfo = await withdrawalOrderList({
    orderNo: orderNo
  })


  const couponJson = [];
  let j = 0.03;
  couponJson.push({
    x: 2,
    y: (j += 0.03).toFixed(2),
    text: printInfo.data.TC030 + '\n',
    align: 'center',
    fontsize: 1
  }, {
    x: 2,
    y: (j += 0.03).toFixed(2),
    text: '------------------------------------------',
    align: 'left',
    fontsize: 1
  }, {
    x: 2,
    y: (j += 0.03).toFixed(2),
    text: "单据类型：挂单票",
    align: 'left',
    fontsize: 1
  }, {
    x: 2,
    y: (j += 0.03).toFixed(2),
    text: "挂 单 号：" + orderInfo.data[0].MD004.substr(10),
    align: 'left',
    fontsize: 1
  }, {
    x: 2,
    y: (j += 0.03).toFixed(2),
    text: "交 易 日：" + orderInfo.data[0].create_time,
    align: 'left',
    fontsize: 1
  }, {
    x: 2,
    y: (j += 0.03).toFixed(2),
    text: "收银机号：" + orderInfo.data[0].posno,
    align: 'left',
    fontsize: 1
  }, {
    x: 2,
    y: (j += 0.03).toFixed(2),
    text: "收 银 员：" + orderInfo.data[0].posname,
    align: 'left',
    fontsize: 1
  }, {
    x: 2,
    y: (j += 0.03).toFixed(2),
    text: "交易金额：" + formatMoney(orderInfo.data[0].MD017, 2),
    align: 'left',
    fontsize: 1
  }, {
    x: 2,
    y: (j += 0.03).toFixed(2),
    text: '------------------------------------------',
    align: 'left',
    fontsize: 1
  }, {
    x: 2,
    y: (j += 0.03).toFixed(2),
    text: orderInfo.data[0].MD004,
    align: 'center',
    type: 'qrcode',
    height: 800
  }, {
    x: 2,
    y: (j += 0.03).toFixed(2),
    text: '------------------------------------------',
    align: 'left',
    fontsize: 1
  }, {
    x: 2,
    y: (j += 0.03).toFixed(2),
    text: "请在三小时内到总收银台缴款",
    align: 'left',
    fontsize: 1
  })

  execMethod('print', 'printTpl', 1, encodeURIComponent(JSON.stringify(couponJson)))
}



export const printShopTicket = async (goodsList, printInfo, shopList, orderNo, orderDate, MB005, payTypeData, typeName) => {


  shopList.forEach(shop => {
    let payList = [], goodlis = [];
    let printJson = {};
    printJson.title = printInfo.data.TC030;
    printJson.typeName = typeName;
    printJson.orderNo = orderNo.substr(8)
    printJson.orderDate = orderDate;
    printJson.userName = MB005;
    printJson.money = 0;
    goodsList.forEach((item, index) => {
      if (shop == item.TR019) {
        printJson.shopName = item.TJ003 || item.TR020;
        printJson.money += parseFloat(item.TR016);
        let goodsInfo = {};
        goodsInfo.index = index + 1;
        goodsInfo.goodsCode = item.TR004;
        goodsInfo.goodsCode = item.TR004;
        goodsInfo.goodsCount = item.TR007;
        goodsInfo.goodsName = item.TR005;
        goodsInfo.goodsPrice = parseFloat(item.TR006).toFixed(2);
        goodsInfo.realPrice = parseFloat(item.TR015).toFixed(2);
        goodsInfo.zkMoney = parseFloat(item.TR021).toFixed(2);
        goodlis.push(goodsInfo);
      }
    })
    printJson.goodsList = goodlis;
    printJson.money = parseFloat(printJson.money).toFixed(2);


    payTypeData.forEach(item => {
      let payInfo = {};
      payInfo.payName = item.MC004;

      if (item.TU005 == 1) {
        payInfo.money = (parseFloat(item.TU019) + parseFloat(item.TU006)).toFixed(2);
      }
      else
        payInfo.money = (parseFloat(item.TU006) - parseFloat(item.TU014 || 0)).toFixed(2);

      payInfo.yhMoney = parseFloat(item.TU014 || 0).toFixed(2);
      payInfo.zlMoney = parseFloat(item.TU019 || 0).toFixed(2);

      payList.push(payInfo);
    })
    printJson.payList = payList;

    execMethod('print', 'printTpl', 1, encodeURIComponent(JSON.stringify(printJson)), "shoptpl");

  })
}

