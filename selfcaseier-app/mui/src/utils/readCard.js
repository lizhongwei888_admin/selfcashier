import { execMethod } from '@/utils/helper'

export const read = (vueObj) => {
  execMethod('card', 'readMagCard').then(res => {
    vueObj.showCard(res.code);
  })
}
