import { Message } from 'element-ui'

export const execMethod = (filename, func, ...params) => {
  let isIntervalLive = false
  return new Promise((resolve, reject) => {
    let res = JSON.parse(
      window.android.coreExec(filename, func, JSON.stringify(params))
    )
    if (res.code == '0') {
      isIntervalLive = true
      let i = setInterval(() => {
        let resAsync = JSON.parse(
          window.android.coreExecAsync(res.data, isIntervalLive)
        )
        console.log(JSON.stringify(resAsync))
        if (resAsync.code != '0') {
          isIntervalLive = false
          clearInterval(i)
          resolve(...resAsync.data)
        }
      }, 500)
    } else if (res.code == '200') {
      resolve(...res.data)
    } else {
      reject(res.msg)
    }
  })
}

export const dateFormatter = (date) => {
  const month = (date.getMonth() + 1).toString().padStart(2, '0')
  const strDate = date.getDate().toString().padStart(2, '0')
  return `${date.getFullYear()}-${month}-${strDate}`
}

export function formatDate (times, pattern) {
  var d = new Date(times).format("yyyy-MM-dd HH:mm:ss");
  if (pattern) {
    d = new Date(times).format(pattern);
  }
  return d.toLocaleString();
}

export const formatMoney = (
  number,
  decimals = 0,
  decPoint = '.',
  thousandsSep = ','
) => {
  number = (number + '').replace(/[^0-9+-Ee.]/g, '')
  const n = !isFinite(+number) ? 0 : +number
  const prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
  const sep = typeof thousandsSep === 'undefined' ? ',' : thousandsSep
  const dec = typeof decPoint === 'undefined' ? '.' : decPoint
  let s = ''
  const toFixedFix = function (n, prec) {
    const k = Math.pow(10, prec)
    return '' + Math.ceil((n * k).toFixed(2)) / k
  }
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.')
  const re = /(-?\d+)(\d{3})/
  while (re.test(s[0])) {
    s[0] = s[0].replace(re, '$1' + sep + '$2')
  }
  if ((s[1] || '').length < prec) {
    s[1] = s[1] || ''
    s[1] += new Array(prec - s[1].length + 1).join('0')
  }
  return s.join(dec)
}

export const showMsgDialog = (msgText, type) => {
  Message({
    dangerouslyUseHTMLString: true,
    message:
      '<div class="modalMsg"><div class="modalMsgbox"><div class="tit">提示</div><div>' +
      msgText +
      '</div></div></div>',
    type: type,
    duration: 0,
    showClose: true,
  })
}

export const NowTime = () => {
  var myDate = new Date()
  var y = myDate.getFullYear()
  var M = myDate.getMonth() + 1 //获取当前月份(0-11,0代表1月)
  var d = myDate.getDate() //获取当前日(1-31)
  var h = myDate.getHours() //获取当前小时数(0-23)
  var m = myDate.getMinutes() //获取当前分钟数(0-59)
  var s = myDate.getSeconds() //获取当前秒数(0-59)

  //检查是否小于10
  M = M < 10 ? '0' + M : M
  d = d < 10 ? '0' + d : d
  h = h < 10 ? '0' + h : h
  m = m < 10 ? '0' + m : m
  s = s < 10 ? '0' + s : s

  return y + '-' + M + '-' + d + ' ' + h + ':' + m + ':' + s
}

export const getRandom = (num) => {
  let random = Math.floor((Math.random()+Math.floor(Math.random()*9+1))*Math.pow(10,num-1));
  return random;
}
