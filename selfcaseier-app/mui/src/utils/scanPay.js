import { formatDate } from '@/utils/index'
import { insertPayType, queryPay, payOrder, refundPay, delPay } from '@/api/counter'
import { v4 as uuidv4 } from 'uuid'
import { showMsgDialog } from '@/utils/helper'

// 扫码支付
export const scanPay = (vueObj) => {
  vueObj.showLoad("等待支付中。。。");
  vueObj.disabled = true;
  const payMoney = vueObj.payMoney
  if (vueObj.orderId == 0) {
    showMsgDialog("请选确认订单信息", "error")
    vueObj.closeLoad();
    return;
  }
  if (vueObj.authCode=='' || vueObj.authCode==-1) {
    showMsgDialog("请先扫码", "error")
    vueObj.closeLoad();
    vueObj.authCode='';
    return;
  }
 
  let orderNo = (vueObj.orderCode + new Date().getTime()).substring(2)
  let cardPassword = (vueObj.paytypeTabs == 'czk' && vueObj.cardPassword != '') ? '#' + vueObj.cardPassword : '';
  payOrder({
    storeCode: vueObj.$store.getters.userInfo.mdCode, //商铺编码
    posId: vueObj.$store.getters.userInfo.MA004, // POS机编码
    cashier: vueObj.$store.getters.userInfo.MB005, // 收银员
    authCode: vueObj.authCode + cardPassword, //付款编码
    orderNo: orderNo,
    money: (parseFloat(parseFloat(((payMoney + '').replace(/,/g, ''))).toFixed(2)) * 100).toFixed(0),
    orderType: vueObj.paytypeTabs == 'czk' ? 'card' : (vueObj.paytypeTabs == 'qb'?'wallet':'')

  }).then((orderMsg) => {

    //如果是储值卡，直接查询一次，在调用
    if (orderMsg.msg == "请输入密码") {
      //输入密码的框打开
      vueObj.setCardPassworVisible = true;
      return "1"
    } else if (orderMsg.result == "false") {
      //密码错误，就对应提示
      // if (this.loading)
      //   this.loading.close();
      // this.disabled = true;
      vueObj.closeLoad();
      showMsgDialog(orderMsg.msg, "error")
      vueObj.authCode='';
      return "1"
    } else {
      queryPayResult(payMoney, vueObj, orderNo, "", "");
    }

  })
}

export const queryPayResult = (payMoney, vueObj, orderNo, refundNo, info, type) => {
  queryPay({
    storeCode: vueObj.$store.getters.userInfo.mdCode, //商铺编码
    posId: vueObj.$store.getters.userInfo.MA004, // POS机编码
    cashier: vueObj.$store.getters.userInfo.MB005, // 收银员
    refundNo: refundNo, //退款单号
    orderNo: orderNo, //付款编码
    orderType: vueObj.paytypeTabs == 'czk' ? 'card' : (vueObj.paytypeTabs == 'qb'?'wallet':'')
  }).then((payRes) => {
    if (payRes.result == "true") {
      if (!refundNo) {
        var payTypeList = [{ MC004: vueObj.MC004, TU011: vueObj.payType, TU010: vueObj.TU010, TU002: 1, TU005: vueObj.MC005, TU006: payMoney, TU007: JSON.stringify(payRes.data), TU008: formatDate(new Date(), "yyyy-MM-dd HH:mm:ss"), TU009: '',TU012:(vueObj.MC005==2 || vueObj.MC005==17)?payRes.data.cardSyMoney:0 }];
        console.log(payRes.data.cardSyMoney);
        console.log(payTypeList);
        //插入支付明细
        insertPay(payMoney, vueObj, payTypeList);
        vueObj.authCode = ''
      } else {
        if (type != "refund") {
          showMsgDialog("退款成功", "success")
          vueObj.closeLoad();
          delPay(info);
          vueObj.ysMoney = parseFloat((parseFloat(vueObj.ysMoney) + parseFloat(info.TU006)).toFixed(2));
          vueObj.payList.splice(info.index, 1);
          vueObj.zfMoney = parseFloat((parseFloat(vueObj.zfMoney) - parseFloat(info.TU006)).toFixed(2));
        }
        else {
          vueObj.closeLoad();
          return "1"
        }
      }
    } else {
      if (payRes.msg != "等待支付") {
        vueObj.closeLoad();
        showMsgDialog(payRes.msg, "error")
        vueObj.disabled = true;
        vueObj.authCode = '';
      } else {
        queryPayResult(payMoney, vueObj, orderNo, refundNo, info, type);
      }
    }
  }).catch(e => {
    console.log(e);
    vueObj.closeLoad();
    vueObj.$confirm('网络异常，请重试?', '提示', {
      confirmButtonText: '重试',
      cancelButtonText: '取消'
    }).then(action => {
      if (action == 'confirm') {
        //确认 
        queryPayResult(payMoney, vueObj, orderNo, refundNo, info, type);
      }
    }).catch(error => {
      if (error == 'cancel') {
        //取消
        vueObj.$confirm('建议不要取消，请记录当前支付信息，及时联系管理员！', '提示', {
          confirmButtonText: '确定',
          cancelButtonText: '取消'
        }).then(action => {
          if (action == 'confirm') {
            //确认 
            vueObj.closePayCard();
          }
        }).catch(error => {
          if (error == 'cancel') {
            //取消
            queryPayResult(payMoney, vueObj, orderNo, refundNo, info, type);
          }
        });
      }
    });
  })
}

// 退款
export const refundMoney = (vueObj, money, orderNo, info, type) => {
  if (type != "refund") {
    vueObj.showLoad("正在退款中");
    vueObj.disabled = true;
  }
  const payMoney = money;
  let refundNo = uuidv4().replace(/-/g, '');
  refundPay({
    storeCode: vueObj.$store.getters.userInfo.mdCode, //商铺编码
    posId: vueObj.$store.getters.userInfo.MA004, // POS机编码
    cashier: vueObj.$store.getters.userInfo.MB005, // 收银员
    orderNo: orderNo,
    refundNo: refundNo,
    money: (parseFloat(parseFloat(((payMoney + '').replace(/,/g, ''))).toFixed(2)) * 100).toFixed(0)
  }).then(() => {
    queryPayResult(payMoney, vueObj, orderNo, refundNo, info, type);
  })
}


export const insertPay = (payMoney, vueObj, payTypeList) => {
  insertPayType({
    orgID: vueObj.$store.getters.userInfo.orgID,
    orderId: vueObj.orderId,
    userId: vueObj.$store.getters.userInfo.MB001,
    payTypeList: JSON.stringify(payTypeList)
  }).then((res) => {
    vueObj.closeLoad();
    if (res.result == "false") {
      showMsgDialog(res.msg, "error")
      return;
    }
    let TU001 = res.data;
    vueObj.payList.push({ TU005: vueObj.MC005, TU007: payTypeList[0].TU007, TU001: TU001, TU006: payMoney, MC004: vueObj.MC004, TU011: vueObj.payType, paytypeTabs: vueObj.paytypeTabs, });
    vueObj.ysMoney = parseFloat((parseFloat(vueObj.ysMoney) - parseFloat(payMoney)).toFixed(2));
    vueObj.zlMoney = 0;
    vueObj.closePayCard();
    showMsgDialog("支付成功", "success")
    vueObj.zfMoney = parseFloat((parseFloat(vueObj.zfMoney) + parseFloat(payMoney)).toFixed(2));
    vueObj.payMoney = 0;
    if (vueObj.ysMoney <= 0) {
      vueObj.updateOrder();
    }
  }).catch(e => {
    console.log(e);
    vueObj.$confirm('网络异常，请重试?', '提示', {

      confirmButtonText: '重试',
      cancelButtonText: '取消'
    }).then(action => {
      if (action == 'confirm') {
        //确认 
        insertPay(payMoney, vueObj, payTypeList)
      }
    }).catch(error => {
      if (error == 'cancel') {
        //取消
        vueObj.$confirm('建议不要取消，请记录当前支付信息，及时联系管理员！', '提示', {
          confirmButtonText: '确定',
          cancelButtonText: '取消'
        }).then(action => {
          if (action == 'confirm') {
            //确认 
            vueObj.closePayCard();
          }
        }).catch(error => {
          if (error == 'cancel') {
            //取消
            insertPay(payMoney, vueObj, payTypeList)
          }
        });
      }
    });
  })
}


