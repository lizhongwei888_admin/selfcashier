import request from '@/utils/request'

// 首页统计分析
export function getStaticList (query) {
  return request({
    url: '/auth/getStaticList',
    method: 'post',
    data: query
  })
}

//支付方式汇总报表
export function cashierGroup (query) {
  return request({
    url: '/auth/cashierGroup',
    method: 'post',
    data: query
  })
}