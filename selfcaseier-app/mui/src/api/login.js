import request from '@/utils/request'

// 登录方法
export function login (userInfo) {
  return request({
    url: '/auth/login',
    method: 'post',
    data: userInfo
  })
}

// 退出方法
export function logout () {
  return request({
    url: '/auth/logout',
    method: 'post'
  })
}

// 获取组织
export function getOrgList () {
  return request({
    url: '/data/get/admta',
    method: 'post'
  })
}

// 获取项目
export function getProList (proInfo) {
  return request({
    url: '/data/get/cmstc',
    method: 'post',
    data: proInfo
  })
}


//获取收银机
export function getCashList (posInfo) {
  return request({
    url: '/data/get/posma',
    method: 'post',
    data: posInfo
  })
}

//验证授权码
export function getVerifyCode (posInfo) {
  return request({
    url: '/auth/getVerifyCode',
    method: 'post',
    data: posInfo
  })
}


//更新密码 updatePassword
export function updatePassword (passInfo) {
  return request({
    url: '/auth/updatePassword',
    method: 'post',
    data: passInfo
  })
}


//锁屏登录 loginByPass
export function loginByPass (passInfo) {
  return request({
    url: '/auth/loginByPass',
    method: 'post',
    data: passInfo
  })
}