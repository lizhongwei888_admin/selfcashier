import request from '@/utils/request'

// 会员查询
export function getMemberInfo (query) {
  return request({
    url: '/userbase/interFace/memberInfo',
    method: 'post',
    data: query
  })
}
