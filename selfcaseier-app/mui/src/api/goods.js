import request from '@/utils/request'

// 获取商品信息
export function getGoodsInfo (query) {
  return request({
    url: '/userbase/interFace/queryWares',
    method: 'post',
    data: query
  })
}

//获取购物袋信息
export function findByMerchantId (query) {
  return request({
    url: '/userbase/dwSyBags/findByMerchantId',
    method: 'post',
    data: query
  })
}


