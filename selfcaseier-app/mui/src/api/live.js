import request from '@/utils/request'

// 获取直播列表
export function getLiveList(query) {
    return request({
        url: '/live/list',
        method: 'post',
        data: query
    })
}
//获取单个直播
export function getLive(query) {
    return request({
        url: '/live/getLive',
        method: 'post',
        data: query
    })
}
//插入信息
export function insert(query) {
    return request({
        url: '/live/insert',
        method: 'post',
        data: query
    })
}

export function update(query) {
    return request({
        url: '/live/update',
        method: 'post',
        data: query
    })
}

//删除直播相关信息
export function deleteLive(query) {
    return request({
        url: '/live/delete',
        method: 'post',
        data: query
    })
}

//查看直播海报
export function getLiveCover(query) {
    return request({
        url: '/live/getLiveCover',
        method: 'post',
        data: query
    })
}

export function getLiveQrCode(query) {
    return request({
        url: '/live/getLiveQrCode',
        method: 'post',
        data: query
    })
}


//插入直播商品
export function insertGoodsGroup(query) {
    return request({
        url: '/live/insertGoodsGroup',
        method: 'post',
        data: query
    })
}

//查看直播商品列表
export function getLiveGoodsList(query) {
    return request({
        url: '/live/getGoodsList',
        method: 'post',
        data: query
    })
}

//查看直播商品列表
export function getLiveChannelList(query) {
    return request({
        url: '/live/getLiveChannelList',
        method: 'post',
        data: query
    })
}

 






