import request from '@/utils/request'

// 存订单
export function insertOrder(query) {
  return request({
    url: '/userbase/dwOrder/insertData',
    method: 'post',
    data: query,
  })
}

export function insertPayData(query) {
  return request({
    url: '/userbase/dwOrder/insertPayData',
    method: 'post',
    data: query,
  })
}

// 整单促销
export function calPrice(query) {
  return request({
    url: '/userbase/interFace/calPrice',
    method: 'post',
    data: query,
  })
}

// 查询订单列表
export function findTop10(query) {
  return request({
    url: '/userbase/dwOrder/findTop10',
    method: 'post',
    data: query,
  })
}

// 支付
export function payOrder(query) {
  return request({
    url: '/userbase/interFace/pay',
    method: 'post',
    data: query,
  })
}

// 支付查询
export function payCheck(query) {
  return request({
    url: '/userbase/interFace/payCheck',
    method: 'post',
    data: query,
  })
}

export function completeOrder(query) {
  return request({
    url: '/userbase/dwOrder/completeOrder',
    method: 'post',
    data: query,
  })
}

export function findLastOrder(query) {
  return request({
    url: '/userbase/dwOrder/lastOrder',
    method: 'post',
    data: query,
  })
}

// 小票上传
export function ticketsUpload(query) {
  return request({
    url: '/userbase/interFace/ticketsUpload',
    method: 'post',
    data: query,
  })
}
