import request from '@/utils/request'

// 获取设备是否绑定了收银机
export function findByDeviceCode (query) {
  return request({
    url: '/userbase/dwSyCash/findByDeviceCode',
    method: 'post',
    data: query
  })
}

// 获取设备是否绑定了收银机
export function bandDevice (query) {
  return request({
    url: '/userbase/dwSyCash/insertData',
    method: 'post',
    data: query
  })
}