import request from '@/utils/request'

// 获取直播列表
export function getUserList (query) {
  return request({
    url: '/user/list',
    method: 'post',
    data: query
  })
}