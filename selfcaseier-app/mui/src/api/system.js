import request from '@/utils/request'

// 获取商户
export function findAllMerchant (query) {
  return request({
    url: '/userbase/dwMerchant/findAll',
    method: 'post',
    data: query
  })
}
// 获取门店
export function findMerInfo (query) {
  return request({
    url: '/userbase/dwMerchant/getInfoByMerchantCode',
    method: 'post',
    data: query
  })
}

// 获取门店
export function findShopInfo (query) {
  return request({
    url: '/userbase/dwShop/findOne',
    method: 'post',
    data: query
  })
}

// 获取门店
export function findAllShop (query) {
  return request({
    url: '/userbase/dwShop/findPageList',
    method: 'post',
    data: query
  })
}