import Vue from 'vue'
import Router from 'vue-router'

// src/router/index.js
Vue.use(Router)
 
const VueRouterPush = Router.prototype.push
Router.prototype.push = function push (to) {
  return VueRouterPush.call(this, to).catch(err => err)
}


export default new Router({
  mode:'hash',
  routes: [
    {
      path: '/',
      redirect: '/set'
    },
    { path: '/set', name: 'set', component: resolve => require(['@/views/account/set'], resolve) },
    { path: '/login', name: 'login', component: resolve => require(['@/views/manage/login'], resolve) },
    { path: '/menu', name: 'menu', component: resolve => require(['@/views/manage/menu'], resolve) },
    { path: '/printList', name: 'printList', component: resolve => require(['@/views/manage/printList'], resolve) },
    { path: '/cashInfo', name: 'cashInfo', component: resolve => require(['@/views/manage/cashInfo'], resolve) },
    { path: '/inform', name: 'inform', component: resolve => require(['@/views/account/inform'], resolve) },
    { path: '/cashier', name: 'cashier', component: resolve => require(['@/views/cashier/cashier'], resolve) },
    { path: '/index', name: 'index', component: resolve => require(['@/views/cashier/index'], resolve) },
    { path: '/pay', name: 'cashier', component: resolve => require(['@/views/pay/pay'], resolve) },
    { path: '/payLoad', name: 'cashier', component: resolve => require(['@/views/pay/payLoad'], resolve) },
    { path: '/payType', name: 'cashier', component: resolve => require(['@/views/pay/payType'], resolve) },
    { path: '/payResult', name: 'cashier', component: resolve => require(['@/views/pay/payResult'], resolve) },
  ]
})