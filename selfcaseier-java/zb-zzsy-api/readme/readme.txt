本项目：基础服务

基础服务=系统数据+业务基础数据+对外接口功能+小程序功能
系统数据=用户+部门+菜单+模块+权限
业务数据=城市+行政区+作业点+街道+车辆+站点+泊位

项目全程：上海城投智慧物流中转站管理系统
物流中转站管理

1 散装车信息预感知模块主要流程
2 进站车辆综合调度和管控模块 
3 中转站车辆派位模块
4 中转站泊位管理模块
5 中转站派位场是综合AI识别模块  aiidentifyassigncar
6 中转站派位作业综合信息整合模块
7 中转站内集卡调度模块 

物流中转站管理项目-基础服务
端口号：8990
docker容器中的名字:userbase
创建数据库:materialflow_transferstation_manage
字符集：utf8 -- UTF-8 Unicode
排序规则：utf8_unicode_ci


访问权限接口地址：
http://localhost:8989/userbase/modulerolerelative/XXX/XXX
例如访问城市权限列表
http://localhost:8989/userbase/modulerolerelative/city/list

访问基础数据接口地址：
http://localhost:8989/userbase/base/XXX/XXX
例如访问城市列表
http://localhost:8989/userbase/base/city/list

 



开发接口规范约定

1、命名
	a java类名,尽量英文、拼音全名，没有全名需要备注说明，
	b 不同类型的类按业务名+类型名来命名，比如 CityController,CityService,CityMapper....
	c 避免命名同名类 
		比如:微服务A,TestService,微服务B,TestService,这样命名，在A引用B项目时候，启动A项目时候会引起同名异常

2、时间格式
	默认格式:yyyy-MM-dd HH:mm:ss，其他约定自定义格式
	
3、字段默认值
	null代表没有值，""代表空字符串为有值

4、工具类
	不包含业务逻辑的公用代码写在工具类中

5、 通用业务逻辑
	业务逻辑代码尽量写在service包中，controller包中尽量不写业务逻辑代码，方便同一个项目、不同项目调用

6、表设计
	每个表默认定义字段，id,createTime,modifyTime,createUser,modifyUser，便于系统性操作、数据跟踪
