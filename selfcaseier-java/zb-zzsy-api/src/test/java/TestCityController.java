
import java.util.HashMap;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lujie.common.util.JsonHelper;
import com.zzsy.api.entity.City;

@Controller
@RequestMapping(value = "/platform/city")
public class TestCityController {
	
	public static void main(String[] args){
		//detail();
		//findAll();
		list();
		//detail();
		//remove();
		//save();
		//threadrequest();
	}
	public static void  detail(){
		String serviceName="/platform/micro/base/city/detail";
		HashMap<String,String> reqHeaderMap=TestApi.getReqHeaderApplicationFormMap();
		String reqParams="cityCode=HG";
		//reqParams="id=2993&GSPlng=11&GSPlat=22&BDlng=33&BDlat=44&oldOperationid=0&isdeleted=0"; 
		
		TestApi.sendRequest(reqHeaderMap,serviceName,reqParams);
	}
	public static void  remove(){
		String serviceName="/platform/micro/base/city/remove";
		HashMap<String,String> reqHeaderMap=TestApi.getReqHeaderApplicationFormMap();
		String reqParams="cityCode=BJ";
		//reqParams="id=2993&GSPlng=11&GSPlat=22&BDlng=33&BDlat=44&oldOperationid=0&isdeleted=0"; 
		
		TestApi.sendRequest(reqHeaderMap,serviceName,reqParams);
	}
	
	
	public static void  threadrequest(){
		Runnable r = new Runnable() {
            @Override
            public void run() {
            	System.out.println("ThreadID:"+Thread.currentThread().getId());
            	list();
            }
        };
        for(int i=0;i<1;i++){
        	new Thread(r).start();  
        }
	}
	public static void  save(){
		String serviceName="/platform/micro/base/city/save";
		
		HashMap<String,String> reqHeaderMap=TestApi.getReqJsonHeaderMap();
		City city=new City();
		city.setAreacode("HG6");
		city.setCityname("黄冈6");
		city.setInfo("黄冈33446");
		city.setIsvalid((short)1);
		
		String reqParams=JsonHelper.toJson(city);
		 
		TestApi.sendRequest(reqHeaderMap,serviceName,reqParams);
	} 
	
	public static void  list(){
		String serviceName="/mmftransferstation/test/city/list";
		HashMap<String,String> reqHeaderMap=TestApi.getReqJsonHeaderMap();
		City city=new City(); 
		city.setPageNum(1);
		city.setPageSize(10);
		//city.setCityname("上海");
		String reqParams=JsonHelper.toJson(city);
		 
		TestApi.sendRequest(reqHeaderMap,serviceName,reqParams);
	}
	public static void  findAll(){
		String serviceName="/platform/micro/base/city/findAll";
		HashMap<String,String> reqHeaderMap=TestApi.getReqJsonHeaderMap();
		City city=new City(); 
		city.setPageNum(1);
		city.setPageSize(10);
		//city.setCityname("上海");
		String reqParams=JsonHelper.toJson(city);
		 
		TestApi.sendRequest(reqHeaderMap,serviceName,reqParams);
	}
	public static void  export(){
		String serviceName="http://localhost:7000/platform/micro/base/city/export?token=dd7bc9c0a2c74e2680eb570f7bf73866:ba51fefd2fd14de0bbff509d2dd5d757";
		HashMap<String,String> reqHeaderMap=TestApi.getReqJsonHeaderMap();
		City city=new City(); 
		city.setPageNum(1);
		city.setPageSize(10);
		//city.setCityname("上海");
		String reqParams=JsonHelper.toJson(city);
		 
		TestApi.sendRequest(reqHeaderMap,serviceName,reqParams);
	}
	
	  
	
}
