

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lujie.common.constant.ConfigProperties;
import com.lujie.common.util.JsonHelper;
import com.zzsy.api.entity.User;

import net.sf.json.JSONObject;

@Controller
@RequestMapping(value = "/platform/admin")
public class TestAdminController {
	private static final Logger log = LoggerFactory.getLogger(TestAdminController.class);
	 
	
	public static void  login(){
		String serviceName="/mmftransferstation/admin/login";
		HashMap<String,String> reqHeaderMap=getReqJsonHeaderMap();
		User user=new User();
		//user.setUsername("testUser6");
		user.setUsername("zhangzhimin");
		//user.setPassword("123456789");
		//user.setUsername("testUser6");//testUser3
		user.setPassword("12345678");//123455
		user.setCode("2991");
		user.setUserKey("abc1234");  
		
		//http://localhost:7000/platform/verifycode/image?userKey=abc1234
		
		String reqParams=JsonHelper.toJson(user);
		System.out.println(reqParams);
		String result=TestApi.sendRequest(reqHeaderMap,serviceName,reqParams);
		
		
		
		JSONObject jsonObject=JSONObject.fromObject(result);
		JSONObject dataJsonObject=jsonObject.getJSONObject("data");
		System.out.println(dataJsonObject.getString("token"));
		
	}
	
	public static void  wechatlogin(){
		String serviceName="/mmftransferstation/admin/login";
		HashMap<String,String> reqHeaderMap=getWechatReqJsonHeaderMap();
		User user=new User();
		//user.setUsername("testUser6");
		user.setUsername("zhangzhimin");
		//user.setPassword("123456789");
		//user.setUsername("testUser6");//testUser3
		user.setPassword("12345678");//123455
		user.setCode("2991");
		user.setUserKey("abc1234");  
		
		//http://localhost:7000/platform/verifycode/image?userKey=abc1234
		
		String reqParams=JsonHelper.toJson(user);
		System.out.println(reqParams);
		String result=TestApi.sendRequest(reqHeaderMap,serviceName,reqParams);
		
		
		
		JSONObject jsonObject=JSONObject.fromObject(result);
		JSONObject dataJsonObject=jsonObject.getJSONObject("data");
		System.out.println(dataJsonObject.getString("token"));
		
	}
	
	public static void  wechatcitylist(){
		String serviceName="/mmftransferstation/base/city/list";
		HashMap<String,String> reqHeaderMap=getWechatReqJsonHeaderMap();
		User user=new User();
		//user.setUsername("testUser6");
		user.setUsername("zhangzhimin");
		//user.setPassword("123456789");
		//user.setUsername("testUser6");//testUser3
		user.setPassword("12345678");//123455
		user.setCode("2991");
		user.setUserKey("abc1234");  
		
		//http://localhost:7000/platform/verifycode/image?userKey=abc1234
		
		String reqParams=JsonHelper.toJson(user);
		System.out.println(reqParams);
		String result=TestApi.sendRequest(reqHeaderMap,serviceName,reqParams);
		
		
		
		JSONObject jsonObject=JSONObject.fromObject(result);
		JSONObject dataJsonObject=jsonObject.getJSONObject("data");
		System.out.println(dataJsonObject.getString("token"));
		
	}
	
	public static HashMap<String,String> getReqJsonHeaderMap(){
		
		HashMap<String,String> reqHeaderMap=new HashMap<>();
		long timestamp=System.currentTimeMillis();
		ConfigProperties.AppKey="fd0f42cb243441f6dc9529634085a9bc";
		ConfigProperties.AppSecret="cddf42eb24f431f6dc9529634085a9bc";
		
		String sign=ConfigProperties.getSign(ConfigProperties.AppKey, ConfigProperties.AppSecret, timestamp);
		//String token="1000:de5f46f9e6930259c62cd4c5538d9173";
		  
		
		reqHeaderMap.put("Content-Type","application/json;charset=utf-8");
		//reqHeaderMap.put("appKey", ConfigProperties.AppKey);
		//reqHeaderMap.put("sign", sign);
		//reqHeaderMap.put("timestamp", timestamp+"");
		//reqHeaderMap.put("token", token);
		return reqHeaderMap;
	}
	
	public static HashMap<String,String> getWechatReqJsonHeaderMap(){
		
		HashMap<String,String> reqHeaderMap=new HashMap<>();
		long timestamp=System.currentTimeMillis();
		ConfigProperties.AppKey="ed0c42cc343441f6dc9529634085a9bc";
		ConfigProperties.AppSecret="cddf42bb2ff431f6dc9529634085a9bc";
		
		String sign=ConfigProperties.getSign(ConfigProperties.AppKey, ConfigProperties.AppSecret, timestamp);
		String token="fd947cdd6eb94387a08606ccedd1d19a:ec55fb4819394049bb9e9dbfc600be21";
		 System.out.println("sign:"+sign);
		
		reqHeaderMap.put("Content-Type","application/json;charset=utf-8");
		reqHeaderMap.put("appKey", ConfigProperties.AppKey);
		reqHeaderMap.put("sign", sign);
		reqHeaderMap.put("timestamp", timestamp+"");
		reqHeaderMap.put("token", token);
		return reqHeaderMap;
	}

	public static void  logout(){
		String serviceName="/platform/admin/logout";
		HashMap<String,String> reqHeaderMap=TestApi.getReqHeaderApplicationFormMap();
		String reqParams="";
		TestApi.sendRequest(reqHeaderMap,serviceName,reqParams);
	}
	public static void  info(){
		String serviceName="/platform/admin/info";
		HashMap<String,String> reqHeaderMap=TestApi.getReqHeaderApplicationFormMap();
		String reqParams="";
		TestApi.sendRequest(reqHeaderMap,serviceName,reqParams);
	}
	public static void  sendSmsCode(){
		String serviceName="/platform/admin/sendSmsCode";
		HashMap<String,String> reqHeaderMap=TestApi.getReqHeaderApplicationFormMap();
		String reqParams="mobile=13027186502";
		TestApi.sendRequest(reqHeaderMap,serviceName,reqParams);
	}
	public static void  checkmobile(){
		String serviceName="/platform/admin/checkmobile";
		HashMap<String,String> reqHeaderMap=TestApi.getReqHeaderApplicationFormMap();
		String reqParams="mobile=13027186502";
		TestApi.sendRequest(reqHeaderMap,serviceName,reqParams);
	}
	public static void  changepwdfromforget(){
		String serviceName="/platform/admin/changepwdfromforget";
		HashMap<String,String> reqHeaderMap=TestApi.getReqHeaderApplicationFormMap();
		String reqParams="mobile=13027186502&fromcode=683458&newpassword=00000000";
		TestApi.sendRequest(reqHeaderMap,serviceName,reqParams);
	}
	
	
	public static void main(String[] args){
		
		System.out.println(JsonHelper.toJson(args));
		
		//wechatcitylist();
		//wechatlogin();
		//login();
		//info();
		//logout();
		//checkmobile();
		//sendSmsCode(); 
		//changepwdfromforget();
	} 
	
}
