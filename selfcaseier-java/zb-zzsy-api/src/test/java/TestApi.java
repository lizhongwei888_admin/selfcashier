

import java.util.HashMap;

import com.lujie.common.constant.ConfigProperties;
import com.lujie.common.constant.HttpContentType;
import com.lujie.common.util.HttpRequest; 
public class TestApi     {
	
	public static String MuckE3UFormUrl="http://localhost:8989";
	//public static String MuckE3UFormUrl="http://119.3.129.95:7000";
	
	
	//public static String MuckE3UFormUrl="http://121.41.94.104:7000";
	//public static String token="fb745354c74b4103a823e4dab60c6a9d:ac7d53f4c74b4103a823e4dab60c6a9d";
	//public static String token="b8b59ff19fdb409a8a18246bff7923f1:4e98c86020f145f5b937e9a6868bd4dd";
	public static String token="fd947cdd6eb94387a08606ccedd1d19a:ec55fb4819394049bb9e9dbfc600be21";
	
	
	public static HashMap<String,String> getReqJsonHeaderMap(){
		
		HashMap<String,String> reqHeaderMap=new HashMap<>();
		long timestamp=System.currentTimeMillis();
		ConfigProperties.AppKey="fd0f42cb243441f6dc9529634085a9bc";
		ConfigProperties.AppSecret="cddf42eb24f431f6dc9529634085a9bc";
		
		String sign=ConfigProperties.getSign(ConfigProperties.AppKey, ConfigProperties.AppSecret, timestamp);
		//String token="1000:de5f46f9e6930259c62cd4c5538d9173";
		  
		
		reqHeaderMap.put("Content-Type","application/json;charset=utf-8");
		//reqHeaderMap.put("appKey", ConfigProperties.AppKey);
		//reqHeaderMap.put("sign", sign);
		//reqHeaderMap.put("timestamp", timestamp+"");
		reqHeaderMap.put("token", token);
		return reqHeaderMap;
	}
	public static HashMap<String,String> getReqHeaderApplicationFormMap(){
		
		HashMap<String,String> reqHeaderMap=new HashMap<>();
		long timestamp=System.currentTimeMillis();
		ConfigProperties.AppKey="fd0f42cb243441f6dc9529634085a9bc";
		ConfigProperties.AppSecret="cddf42eb24f431f6dc9529634085a9bc";
		
		String sign=ConfigProperties.getSign(ConfigProperties.AppKey, ConfigProperties.AppSecret, timestamp);
		//String token="1000:de5f46f9e6930259c62cd4c5538d9173";
			
		
		reqHeaderMap.put("Content-Type",HttpContentType.ApplicationForm+";charset=utf-8");
		reqHeaderMap.put("appKey", ConfigProperties.AppKey);
		reqHeaderMap.put("sign", sign);
		reqHeaderMap.put("timestamp", timestamp+"");
		reqHeaderMap.put("token", token);
		return reqHeaderMap;
	}
	public static HashMap<String,String> getReqHeaderGetMap(){
		
		HashMap<String,String> reqHeaderMap=new HashMap<>();
		
		reqHeaderMap.put("Content-Type",HttpContentType.ApplicationForm+";charset=utf-8");
		reqHeaderMap.put("token", token);
		return reqHeaderMap;
	}
	
	public static HashMap<String,String> getInterfaceUserReqHeaderMap(){
		
		HashMap<String,String> reqHeaderMap=new HashMap<>();
		long timestamp=System.currentTimeMillis();
		ConfigProperties.AppKey="testAccount";
		ConfigProperties.AppSecret="cddf32eb24f431f6dc9529634085a9bc";
		
		String sign=ConfigProperties.getSign(ConfigProperties.AppKey, ConfigProperties.AppSecret, timestamp);
		
		reqHeaderMap.put("Content-Type",HttpContentType.ApplicationForm+";charset=utf-8");
		reqHeaderMap.put("account", ConfigProperties.AppKey);
		reqHeaderMap.put("sign", sign);
		reqHeaderMap.put("timestamp", timestamp+"");
		return reqHeaderMap;
	}
	
	public static void main(String[] args){
		
	}
	
	public static String sendRequest(HashMap<String,String> reqHeaderMap,String serviceName,String params){
		String result=null;
		System.out.println("请求地址:"+MuckE3UFormUrl+serviceName);
		System.out.println("请求头参数:"+reqHeaderMap);
		System.out.println("请求体参数"+params);
		try {
			result = HttpRequest.sendPost(reqHeaderMap,
					MuckE3UFormUrl+serviceName, 
					params);
			System.out.println(result);
			
		} catch (Exception e) { 
			e.printStackTrace();
		}
		return result;
	} 
	public static String sendGetRequest(HashMap<String,String> reqHeaderMap,String serviceName,String params){
		String result=null;
		try {
			result = HttpRequest.sendGet(reqHeaderMap,
					MuckE3UFormUrl+serviceName, 
					params);
			System.out.println(result);
			
		} catch (Exception e) { 
			e.printStackTrace();
		}
		return result;
	}
	
}
