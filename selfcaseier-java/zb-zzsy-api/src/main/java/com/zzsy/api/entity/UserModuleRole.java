package com.zzsy.api.entity;

import java.io.Serializable;

import com.lujie.common.entity.BasicEntity;

public class UserModuleRole extends BasicEntity implements Serializable{
	private static final long serialVersionUID = -385429157197792921L;

	private Integer id;
	private String userId;
	private Integer moduleRoleId;
	
	private String username;
	private String moduleRoleName;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Integer getModuleRoleId() {
		return moduleRoleId;
	}
	public void setModuleRoleId(Integer moduleRoleId) {
		this.moduleRoleId = moduleRoleId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getModuleRoleName() {
		return moduleRoleName;
	}
	public void setModuleRoleName(String moduleRoleName) {
		this.moduleRoleName = moduleRoleName;
	}
	
	
}