package com.zzsy.api.constant;

/**
 * 集卡调度-指令描述,指令名
 */
public enum InstructionTypeEnum  {

	INTERNAL_TRUCK_DISPATCH( "801" , "内集卡调度") ,
	EXTERNAL_TRUCK_DISPATCH( "804" , "外集卡调度") ;
		
	private String code;

	private String desc;

	InstructionTypeEnum(String code , String desc) {
		this.code = code;
		this.desc = desc;
	}

	public String getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}
	
	public static String getDesc(String code) {
		
		InstructionTypeEnum[] logTypes = values();
		for(InstructionTypeEnum logType : logTypes) {
			if(code.equals(logType.getCode())) {
				return logType.getDesc();
			}
		}
		return "";
	}

}
