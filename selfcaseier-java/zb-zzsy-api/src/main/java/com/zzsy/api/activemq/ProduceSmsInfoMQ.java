package com.zzsy.api.activemq;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Service;

import com.lujie.common.constant.ResultMsg;
import com.lujie.common.constant.SmsAlertType;
import com.lujie.common.constant.SysType;
import com.lujie.common.util.JsonHelper;


@Service
public class ProduceSmsInfoMQ {

	@Autowired
	private JmsTemplate jmsTemplate;

	/**
	 * 这里是根据MQ配置文件定义的queue来注入的，也就是这里将会把不同的内容推送到不同的queue中
	 */
	@Autowired
	@Qualifier("SmsQueue")//使用spring容器中名字为userInfoQueue的bean
	private Destination smsQueue;
	
	public void sendSms(Object info) {
		System.out.print("发送消息："+info);
		jmsTemplate.send(smsQueue, new MessageCreator() {
			@Override
			public Message createMessage(Session session) throws JMSException {
				return session.createObjectMessage(info.toString());
			}
		});
	}
	public void sendSms(String mobile,String content){
		
		ArrayList list=new ArrayList();
		Map<String,Object> map=new HashMap<String,Object>();
		map.put("bussinessId", SysType.PlatForm.name());
		map.put("mobile", mobile);
		map.put("content", content);
		map.put("smscode", "abccode"); 
		map.put("param", "param"); 
		map.put("alertType", SmsAlertType.Notify.name()); 
		map.put("msgId", UUID.randomUUID().toString().replace("-", "")); 
		
		list.add(map);
		
		
		sendSms(
				JsonHelper.toJson(new ResultMsg("",
				"",list))
		);
		
	}
	
	public void sendSms(String mobile,String content,String templateCode,String templateParam){
		
		ArrayList list=new ArrayList();
		Map<String,Object> map=new HashMap<String,Object>();
		map.put("bussinessId", SysType.PlatForm.name());
		map.put("mobile", mobile);
		map.put("content", content);
		map.put("smscode", templateCode); 
		map.put("param", templateParam); 
		map.put("alertType", SmsAlertType.VerifyCode.name()); 
		map.put("msgId", UUID.randomUUID().toString().replace("-", "")); 
		
		list.add(map);
		
		
		sendSms(
				JsonHelper.toJson(new ResultMsg("",
				"",list))
		);
		
	}
}