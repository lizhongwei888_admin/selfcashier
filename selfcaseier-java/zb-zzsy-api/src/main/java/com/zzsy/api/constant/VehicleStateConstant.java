package com.zzsy.api.constant;

public enum VehicleStateConstant {
	ComeIn(1, "进站"),
	Assignment(2, "已派位"),
	Ready(3, "就位"),
	Away(4, "离场")
	;
	
	private Integer code;

	
	private String desc;

	VehicleStateConstant(Integer code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public Integer getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}
	
}