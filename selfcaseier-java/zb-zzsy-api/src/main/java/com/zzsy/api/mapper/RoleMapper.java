package com.zzsy.api.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.lujie.common.dao.BasicMapper;
import com.zzsy.api.entity.Role;
import com.zzsy.api.entity.RoleMenu;
@Repository
public interface RoleMapper extends BasicMapper{
	int deleteByPrimaryKey(String id);

    int insertSelective(Role record);

    Role selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Role record);
    
    List<Role> findRoleFromMenu(Map<String , String> map);
    
    List<Role> findRoleListByMenu(@Param("p")RoleMenu roleMenu);
    
    Integer countRoleListByMenu(@Param("p")RoleMenu roleMenu);
    
    List<Role> findAllRole(Map<String , String> map);

}