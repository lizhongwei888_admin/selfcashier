package com.zzsy.api.constant;

public enum SseMsgType {
	Notice(0, "notice"),
	BOX(1, "box"),
	DIRECTIVE(2, "directive"),
	LOGIN(3, "login")
	;
	
	private Integer code;

	
	private String desc;

	SseMsgType(Integer code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public Integer getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}
	
	public static String getDesc(Integer code) {
		
		SseMsgType[] logTypes = values();
		for(SseMsgType logType : logTypes) {
			if(code.equals(logType.getCode())) {
				return logType.getDesc();
			}
		}
		return "";
	}
}
