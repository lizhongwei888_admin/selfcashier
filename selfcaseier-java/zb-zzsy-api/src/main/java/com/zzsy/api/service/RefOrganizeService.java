package com.zzsy.api.service;

import com.lujie.common.service.BaseService;
import com.zzsy.api.entity.RefOrganize;
import com.zzsy.api.mapper.RefOrganizeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class RefOrganizeService extends BaseService {

    @Autowired
    private RefOrganizeMapper refOrganizeMapper;

    /**
     * 添加或修改
     *
     * @param refOrganize
     * @return
     */
    public int addOrUp(RefOrganize refOrganize) {
        if (refOrganize.getCode() == null) {
            return refOrganizeMapper.add(refOrganize);
        } else {
            refOrganize.setUpdate_time(new Date());
            int up = refOrganizeMapper.up(refOrganize);
            return up;
        }

    }

    public void delete(String codes) {
        Set<Long> collect = Arrays.stream(codes.split(",")).map(m -> Long.parseLong(m)).collect(Collectors.toSet());
        refOrganizeMapper.deleteByIdSet(collect);
    }

    public List<RefOrganize> findPageList(RefOrganize refOrganize) {
        return refOrganizeMapper.findPageList(refOrganize);
    }

    public Long countTotal(RefOrganize refOrganize) {
        return refOrganizeMapper.countTotal(refOrganize);
    }


}
