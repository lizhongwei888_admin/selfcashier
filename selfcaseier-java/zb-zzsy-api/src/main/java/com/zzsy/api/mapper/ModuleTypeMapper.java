package com.zzsy.api.mapper;

import org.springframework.stereotype.Repository;

import com.lujie.common.dao.BasicMapper;

@Repository
public interface ModuleTypeMapper extends BasicMapper{
}