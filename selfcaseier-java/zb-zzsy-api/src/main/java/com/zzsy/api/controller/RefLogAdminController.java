package com.zzsy.api.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.util.JsonHelper;
import com.zzsy.api.entity.RefLogAdmin;
import com.zzsy.api.entity.RefLogInterface;
import com.zzsy.api.entity.RefUser;
import com.zzsy.api.excel.RefLogExcel;
import com.zzsy.api.excel.RefLogInterfaceExcel;
import com.zzsy.api.service.RefLogAdminService;
import com.zzsy.api.util.RedisRefUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

/**
 * 系统-后台日志控制器
 */
@Controller
@RequestMapping(value = "/refLogAdmin")
public class RefLogAdminController {

    private static final Logger log = LoggerFactory.getLogger(RefLogAdminController.class);

    @Autowired
    private RefLogAdminService refLogAdminService;
    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 后端分页查询集合
     *
     * @param object
     * @return
     */
    @RequestMapping(value = "/listRefLogAdmin", method = RequestMethod.POST)
    @ResponseBody
    public String listRefLogAdmin(@RequestBody JSONObject object) {
        RefLogAdmin refLogAdmin = JSONObject.parseObject(JSON.toJSONString(object.getJSONObject("params")), RefLogAdmin.class);
        if (object.getInteger("offset") != null && object.getInteger("limit") != null) {
            refLogAdmin.setPageNum(object.getInteger("offset"));
            refLogAdmin.setPageSize(object.getInteger("limit"));
        }
        JSONObject sort = object.getJSONObject("sort");
        if (sort != null) {
            String prop = sort.getString("prop");
            String order = sort.getString("order");
            refLogAdmin.setProp(prop);
            refLogAdmin.setOrder(order);
        }
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success, refLogAdminService.findPageList(refLogAdmin), refLogAdminService.countTotal(refLogAdmin)));
    }

    @PostMapping(value = "/listRefLogAdminExport")
    @ResponseBody
    public String listRefLogAdminExport(@RequestBody JSONObject object, HttpServletRequest request, HttpServletResponse response) {
        RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
        RefLogAdmin refLogAdmin = JSONObject.parseObject(JSON.toJSONString(object.getJSONObject("params")), RefLogAdmin.class);
        JSONObject sort = object.getJSONObject("sort");
        if (sort != null) {
            String prop = sort.getString("prop");
            String order = sort.getString("order");
            refLogAdmin.setProp(prop);
            refLogAdmin.setOrder(order);
        }
        // 查询列表
        List<RefLogAdmin> result = refLogAdminService.findList(refLogAdmin);
        // 类型转换
        List<RefLogExcel> list = JSONArray.parseArray(JSON.toJSONString(result), RefLogExcel.class);
        try {
            // 设置响应头信息
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.setCharacterEncoding("utf-8");
            String fileName = URLEncoder.encode("后台日志信息表", "UTF-8").replaceAll("\\+", "%20");
            response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + "_" + System.currentTimeMillis() + ".xlsx");
            // 使用EasyExcel进行数据导出
            EasyExcel.write(response.getOutputStream(), RefLogExcel.class).sheet("导出数据").doWrite(list);

            this.refLogAdminService.create(new RefLogAdmin(loginRefUser.getCode()
                    , "后台日志管理界面", "导出", "listRefLogAdminExport", "/refLogAdmin/listRefLogAdminExport"
                    , JSON.toJSONString(result), "ok", loginRefUser.getOrgID()));
            return JsonHelper.toJson(new ResultMsg(ResultCode.Success));
        } catch (IOException e) {
            log.error("listRefLogAdminExport异常:", e);
            return JsonHelper.toJson(new ResultMsg("300", "导出异常"));
        }
    }

}
