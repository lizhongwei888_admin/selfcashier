package com.zzsy.api.controller;

import com.alibaba.fastjson.JSONObject;
import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.util.JsonHelper;
import com.zzsy.api.entity.*;
import com.zzsy.api.service.DwShopService;
import com.zzsy.api.service.RefLogAdminService;
import com.zzsy.api.util.RedisRefUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping(value = "/dwShop")
public class DwShopController {

	@Autowired
	private DwShopService dwShopService;

	@Autowired
	private RefLogAdminService refLogAdminService;

	@Autowired
	private RedisTemplate redisTemplate;

	@RequestMapping(value={"/findPageList"})
	@ResponseBody
	public String findPageList(HttpServletRequest request, @RequestBody DwShop dwShop){
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success,dwShopService.findPageList(dwShop)));
	}

	/**
	 * 后端分页查询集合
	 *
	 * @param dwShop
	 * @return
	 */
	@RequestMapping(value = "/listShop", method = RequestMethod.POST)
	@ResponseBody
	public String listRefApi(@RequestBody DwShop dwShop) {

		return JsonHelper.toJson(new ResultMsg(ResultCode.Success, dwShopService.findPageList(dwShop), dwShopService.countTotal(dwShop)));
	}

	@RequestMapping(value={"/findOne"})
	@ResponseBody
	public String findOne(HttpServletRequest request, @RequestBody DwShop dwShop){
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success,dwShopService.findOne(dwShop)));
	}

	@RequestMapping(value={"/addShop"})
	@ResponseBody
	public String addShop(HttpServletRequest request, @RequestBody DwShop dwShop){
		RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
		dwShop.setCreateby(loginRefUser.getCode());
		dwShop.setCreate_time(new Date());
		dwShop.setOrgID(loginRefUser.getOrgID());
		if(dwShopService.findOneByShopCode(dwShop) !=null){
			return JsonHelper.toJson(new ResultMsg("302","门店编码重复"));
		}
		dwShopService.addOrUp(dwShop);
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success));
	}

	/**
	 * 修改
	 *
	 * @param dwShop
	 * @return
	 */
	@RequestMapping(value = "/updateShop", method = RequestMethod.POST)
	@ResponseBody
	public String updateMerchant(HttpServletRequest request,@RequestBody DwShop dwShop) {


		Object old = dwShopService.findOneByCode(dwShop);

		RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
		dwShop.setUpdateby(loginRefUser.getCode());
		dwShop.setUpdate_time(new Date());

		DwShop data = dwShopService.findOneByShopCode(dwShop);

		if( data !=null && !data.getCode().equals(dwShop.getCode())){
			return JsonHelper.toJson(new ResultMsg("302","门店编码重复"));
		}
		dwShopService.addOrUp(dwShop);

		this.refLogAdminService.create(new RefLogAdmin(loginRefUser.getCode()
				, "门店管理", "修改门店", "updateShop", "/dwShop/updateShop"
				,"原门店信息："+JsonHelper.toJson(old)+"新门店信息："+ JsonHelper.toJson(dwShop), "ok", loginRefUser.getOrgID()));
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success));
	}

	/**
	 * 删除
	 *
	 * @param object
	 * @return
	 */
	@RequestMapping(value = "/delShop", method = RequestMethod.POST)
	@ResponseBody
	public String delMerchant(HttpServletRequest request, @RequestBody JSONObject object) {

		String codes = object.getString("codes");
		if (StringUtils.isEmpty(codes)) {
			return JsonHelper.toJson(new ResultMsg("300", "code不能为空"));
		}
		RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);

		if (dwShopService.getListByShopIdSet(codes).size()>0) {
			return JsonHelper.toJson(new ResultMsg("300", "门店已绑定了收银机，无发删除"));
		}

		if (dwShopService.delete(codes) <= 0) {
			return JsonHelper.toJson(new ResultMsg("300", "删除失败"));
		}

		this.refLogAdminService.create(new RefLogAdmin(loginRefUser.getCode()
				, "门店管理", "删除门店", "delShop", "/dwShop/delShop"
				, codes, "ok", loginRefUser.getOrgID()));
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success));
	}

	@RequestMapping(value = "/updateState", method = RequestMethod.POST)
	@ResponseBody
	public String updateState(@RequestBody DwShop dwShop, HttpServletRequest request) {
		String code = dwShop.getCode();
		if (StringUtils.isEmpty(code)) {
			return JsonHelper.toJson(new ResultMsg("300", "code不能为空"));
		}
		RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
		dwShop = (DwShop) dwShopService.findOne(dwShop);
		if ("false".equals(dwShop.getState())) {
			dwShop.setState("true");
		} else {
			dwShop.setState("false");
		}
        dwShop.setUpdateby(loginRefUser.getCode());
        if (dwShopService.addOrUp(dwShop) <= 0) {
            return JsonHelper.toJson(new ResultMsg("300", "修改状态失败"));
        }
        this.refLogAdminService.create(new RefLogAdmin(loginRefUser.getCode()
                , "门店管理页面", "修改状态", "state", "/dwShop/updateState"
                , dwShop, "ok", loginRefUser.getOrgID()));
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success));
    }


    /**
     * 导入门店
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/importShop", method = RequestMethod.POST)
    @ResponseBody
    public String importShop(HttpServletRequest request) {
        return dwShopService.importShop(request);
    }


}
