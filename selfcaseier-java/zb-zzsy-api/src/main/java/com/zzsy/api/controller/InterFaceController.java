package com.zzsy.api.controller;

import com.zzsy.api.service.InterFaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
@RequestMapping(value = "/interFace")
public class InterFaceController {

    @Autowired
    private InterFaceService interFaceService;

    @RequestMapping(path={"/memberInfo"})
    @ResponseBody
    public String memberInfo(@RequestBody Map<String,Object> map){
       return interFaceService.memberInfo(map);
    }

    @RequestMapping(path={"/queryWares"})
    @ResponseBody
    public String queryWares(@RequestBody Map<String,Object> map){
        return interFaceService.queryWares(map);
    }
    @RequestMapping(path={"/calPrice"})
    @ResponseBody
    public String calPrice(@RequestBody Map<String,Object> map){
        return interFaceService.calPrice(map);
    }

    @RequestMapping(path={"/submitOrder"})
    @ResponseBody
    public String submitOrder(@RequestBody Map<String,Object> map){
        return interFaceService.submitOrder(map);
    }

    @RequestMapping(path={"/pay"})
    @ResponseBody
    public String pay(@RequestBody Map<String,Object> map){
        return interFaceService.pay(map);
    }

    @RequestMapping(path={"/payCheck"})
    @ResponseBody
    public String payCheck(@RequestBody Map<String,Object> map){
        return interFaceService.payCheck(map);
    }

    @RequestMapping(path={"/ticketsUpload"})
    @ResponseBody
    public String ticketsUpload(@RequestBody Map<String,Object> map){
        return interFaceService.ticketsUpload(map);
    }


}
