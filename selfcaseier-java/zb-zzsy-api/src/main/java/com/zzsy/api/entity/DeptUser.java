package com.zzsy.api.entity;

import java.io.Serializable;

import com.lujie.common.entity.BasicEntity;

public class DeptUser extends BasicEntity  implements Serializable{
	private static final long serialVersionUID = 8685506029022563926L;

	private Integer id;

    private String uid;

    private String did;
    
    private String text;
    
    private String userName;
    
    private String deptName;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid == null ? null : uid.trim();
    }

    public String getDid() {
        return did;
    }

    public void setDid(String did) {
        this.did = did == null ? null : did.trim();
    }

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
    
}