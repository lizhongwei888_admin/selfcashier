package com.zzsy.api.constant;

import java.util.Arrays;
import java.util.List;

public class KeysManager {
	
	public static final String CHANGED_GARBAGE_TYPE_PARKNO = "changed_garbage_type_parkno";
	public static final String INTERNAL_VEHICLE_DISPATCH_INSTRUCT_BY_PARK = "internal_vehicle_dispatch_instruct_parkno";
	public static final String USER_BASE="User_Base_";
	public static final String User_Auth="User_Base_";
	public static final String CodeTbale="CodeTbale_";
	public static final String User_Auth_Role_Module=User_Auth+"User_Base_Role_Module_";
	public static final String User_Auth_Path_Module = User_Auth + "Path_Module";
	public static final String Admin = "admin";
	public static final String AdminRoleId = "a594ce962faa404babe4c8ce2ccaf117";//超级管理员角色

	public static final String ModuleId = "ModuleId_";
	public static final String ModuleType = "ModuleType_";
	public static final String ModuleRoleId = "ModuleRoleId_";
	public static final String User = "User_";
	public static final String MenuFindAll = "MenuFindAll_";
	public static final String Menu = "Menu_";


	//新增key关键字
	public static final String Ref_User_Auth = "Ref_User_Base_";

	public static Boolean isAdmin(String userName) {
		if (userName != null && userName.equalsIgnoreCase(Admin)) {
			return true;
		}
		return false;
	}

	public static final String DRY_GARBAGE_TYPE = "干垃圾";
	
	public static final String   CHU_YU_GARBAGE_TYPE = "厨余垃圾";
	
	public static final String   CAN_CHU_GARBAGE_TYPE = "餐厨垃圾";
	
	public static final List<String>  BOX_GARBAGE_TYPE_LIST = Arrays.asList(DRY_GARBAGE_TYPE , CHU_YU_GARBAGE_TYPE , CAN_CHU_GARBAGE_TYPE); 

}
