package com.zzsy.api.mapper;

import com.lujie.common.dao.BasicMapper;
import com.zzsy.api.entity.DwSyCash;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Repository
public interface DwSyCashMapper extends BasicMapper{

    Map<String,Object> findByDeviceCode(@Param("p") DwSyCash d);

    List<DwSyCash> findList(@Param("p") DwSyCash d);


    void insertData(@Param("p") DwSyCash d);

    void auditData(@Param("p") DwSyCash d);

    List<DwSyCash> getListByIdSet(@Param("p") Set<String> codeStr);

    Map<String,Object> getSyCashNo(@Param("p") DwSyCash d);

}