package com.zzsy.api.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import com.zzsy.api.constant.KeysManager;
import com.zzsy.api.constant.LogType;
import com.zzsy.api.constant.UserType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import com.lujie.common.constant.HttpContentType;
import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.service.BasicService;
import com.lujie.common.util.MyHttpServletRequestWrapper;
import com.lujie.common.util.StringUtil;
import com.lujie.common.util.StringUtils;
import com.zzsy.api.entity.Log;
import com.zzsy.api.entity.ModuleRole;
import com.zzsy.api.entity.ModuleRoleRelative;
import com.zzsy.api.entity.ModuleType;
import com.zzsy.api.entity.User;
import com.zzsy.api.entity.UserModuleRole;
import com.zzsy.api.mapper.ModuleRoleRelativeMapper;
import com.zzsy.api.mapper.ModuleTypeMapper;
import com.zzsy.api.util.RedisUtil;

import net.sf.json.JSONObject;

@Service
public class ModuleRoleRelativeService extends BasicService{
	private final static Logger log = LoggerFactory.getLogger(ModuleRoleRelativeService.class);
	
	@Autowired
	private LogService logservice;
	@Autowired
	private UserService userService;
	
	@Autowired 
	private RedisTemplate redisTemplate;
	
	@Autowired
	private ModuleTypeMapper moduleTypeMapper;
	
	@Autowired
	private ModuleRoleRelativeMapper moduleRoleRelativeMapper;
	@Resource(name="moduleRoleRelativeMapper")
	public void initBaseDao(ModuleRoleRelativeMapper moduleRoleRelativeMapper){
		super.setBaseMapper(moduleRoleRelativeMapper);
	} 
	public List<ModuleRoleRelative> findModules(ModuleRoleRelative moduleRoleRelative){
		return moduleRoleRelativeMapper.findModules(moduleRoleRelative);
	}
	
	private HashMap<String,ModuleType> getPathModuleMap(){
		//RedisUtil.deleteMiddleLike(redisTemplate,KeysManager.User_Auth_Path_Module);
		//RedisUtil.deleteMiddleLike(redisTemplate,KeysManager.User_Auth+KeysManager.ModuleType);
		
		HashMap<String,ModuleType> pathModuleMap=(HashMap<String,ModuleType>)redisTemplate.opsForValue().get(KeysManager.User_Auth_Path_Module);
		if(pathModuleMap==null){
			List<ModuleType> moduleTypeList=moduleTypeMapper.findList(null);
			if(moduleTypeList!=null){
				pathModuleMap=new HashMap<>();
				for(ModuleType moduleType:moduleTypeList){
					pathModuleMap.put(moduleType.getPath(), moduleType);
					String moduleTypeKey=KeysManager.User_Auth+KeysManager.ModuleType+moduleType.getId();
					redisTemplate.opsForValue().set(moduleTypeKey, moduleType);
				}
			} 
			if(pathModuleMap!=null && pathModuleMap.size()>0)
			redisTemplate.opsForValue().set(KeysManager.User_Auth_Path_Module, pathModuleMap);
		}
		return pathModuleMap;
	}
	private String getRequestDateId(HttpServletRequest request,ModuleType pathModuleType) throws Exception{
		if(HttpContentType.isJsonContentTypeRequest(request)){
			MyHttpServletRequestWrapper  requestWrapper =new MyHttpServletRequestWrapper(request);
			
			String requestBody=HttpContentType.getRequestBodyStr(requestWrapper.getBody());
			
			if(StringUtil.isEmpty(requestBody)) return null;
			
			JSONObject jsonObject=JSONObject.fromObject(requestBody);
			String dataId=StringUtil.getStr(jsonObject.get(pathModuleType.getDataIdName()));
			return dataId;
		}
		if(HttpContentType.isFormContentTypeRequest(request)){
			String dataId=StringUtil.getStr(request.getParameter(pathModuleType.getDataIdName()));
			return dataId;
		}
		return null;
	}
	
	
	
	public ResultMsg getUserAuth(HttpServletRequest request,User user) throws Exception{
		String urlPath = request.getServletPath();
		
		//测试时候使用
		//RedisUtil.deleteMiddleLike(redisTemplate, KeysManager.User_Auth+"*");
		
		String[] path=urlPath.split("/"); // /city/list
		if(path.length<=1) {//没有配置模块的接口，不需要验证权限
			return new ResultMsg(ResultCode.Success);
		}
		HashMap<String,ModuleType> pathModuleMap=getPathModuleMap();
		if(pathModuleMap==null){//没有配置模块的接口，不需要验证权限
			return new ResultMsg(ResultCode.Success);
		}
		//path下标，1微服务标识，2微服务名、3模块名、4操作名
		String modulePath="/"+path[0];//模块名
		String moduleOperate=path[1];//操作名
		ModuleType pathModuleType=pathModuleMap.get(modulePath);
		if(pathModuleType==null){//没有配置模块的接口，不需要验证权限
			return new ResultMsg(ResultCode.Success);
		}
		if(user.getUsername().equalsIgnoreCase(KeysManager.Admin)){
			return new ResultMsg(ResultCode.Success);
		}
		
		
		if(!pathModuleType.needInterceptMethod(moduleOperate)){
			return new ResultMsg(ResultCode.Success);
		}
		
		if(pathModuleType.getFindAllName()!=null && pathModuleType.getFindAllName().equals(moduleOperate) 
				|| pathModuleType.getListName()!=null && pathModuleType.getListName().equals(moduleOperate)){
			//setUserModuleData(user,pathModuleType);
			return new ResultMsg(ResultCode.Success);
		}
		
		
		List<UserModuleRole> userModuleRoles=user.getUserModuleRoles(); 
		if(userModuleRoles!=null){
			String dataId=StringUtil.getNotNullStr(getRequestDateId(request,pathModuleType));
			
			for(UserModuleRole userModuleRole:userModuleRoles){
				Map<String,Boolean> operateAuthMap=getOperateAuthMap(userModuleRole.getModuleRoleId(),pathModuleType);
				if(operateAuthMap!=null && operateAuthMap.size()>0){//有配置模块的接口，并且设置了允许操作，则通过权限验证
					Boolean moduleAllow=operateAuthMap.get(moduleOperate+"_/");//模块级操作权限
					if(moduleAllow!=null && moduleAllow){
						return new ResultMsg(ResultCode.Success);
					}
					
					Boolean moduleDataAllow=operateAuthMap.get(moduleOperate+"_"+dataId);//模块数据级操作权限
					if(moduleDataAllow!=null && moduleDataAllow){
						return new ResultMsg(ResultCode.Success);
					}
				}
			}
		}
		
		return new ResultMsg("301","没有操作权限！");
	}
	private Map<String,Boolean> getOperateAuthMap(Integer moduleRoleId,ModuleType pathModuleType){
		//RedisUtil.deleteMiddleLike(redisTemplate, KeysManager.User_Auth_Role_Module);
		String roleModuleKey=moduleRoleId+"_"+pathModuleType.getId();
		Map<String,Boolean> operateAuthMap=(Map<String,Boolean>)redisTemplate.opsForValue().get(KeysManager.User_Auth_Role_Module+roleModuleKey);
		
		if(operateAuthMap==null || operateAuthMap.size()==0){
			ModuleRoleRelative tempModuleRoleRelative=new ModuleRoleRelative();
			tempModuleRoleRelative.setModuleRoleId(moduleRoleId);
			tempModuleRoleRelative.setModuleId(pathModuleType.getId());
			List<ModuleRoleRelative> moduleRoleRelativeList=moduleRoleRelativeMapper.findList(tempModuleRoleRelative);
			if(moduleRoleRelativeList!=null && moduleRoleRelativeList.size()>0){
				operateAuthMap=new HashMap<>();
				for(ModuleRoleRelative moduleRoleRelative:moduleRoleRelativeList){
					String dataId=moduleRoleRelative.getDataId();
					
					Boolean add=moduleRoleRelative.getAllowAdd();
					operateAuthMap.put(pathModuleType.getAddName()+"_"+dataId, moduleRoleRelative.getAllowAdd());//新增下级
					operateAuthMap.put(pathModuleType.getDetailName()+"_"+dataId, moduleRoleRelative.getAllowDetail());
					operateAuthMap.put(pathModuleType.getUpdateName()+"_"+dataId, moduleRoleRelative.getAllowUpdate());
					operateAuthMap.put(pathModuleType.getDeleteName()+"_"+dataId, moduleRoleRelative.getAllowDelete());
				}
			}
					
			if(operateAuthMap!=null && operateAuthMap.size()>0)
			redisTemplate.opsForValue().set(KeysManager.User_Auth_Role_Module+roleModuleKey, operateAuthMap);
		}
		return operateAuthMap;
	}
	public ResultMsg roleAddModule(ModuleRoleRelative moduleRoleRelative,HttpServletRequest request){
		if(moduleRoleRelative==null){
			return new ResultMsg("310","没有提交请求数据");
		}
		if(moduleRoleRelative.getModuleId()==null){
			return new ResultMsg("320","模块id不能为空");
		}
		if(moduleRoleRelative.getModuleRoleId()==null){
			return new ResultMsg("330","模块角色id不能为空");
		}
		List<ModuleRoleRelative> modules=moduleRoleRelativeMapper.findModules(moduleRoleRelative);
		if(modules!=null && modules.size()>0){
			return new ResultMsg("340","该角色已经添加了这个模块:"+modules.get(0).getModuleName());
		}
		User loginUser = RedisUtil.getUser(redisTemplate, request);
		moduleRoleRelative.setCreateUserId(loginUser.getId());
		moduleRoleRelative.setDataId("/");
		
		moduleRoleRelativeMapper.save(moduleRoleRelative);
		
		//this.logservice.create(loginUser, "添加模块角色关系");
		this.logservice.create(new Log(loginUser.getId(), "添加模块角色关系", LogType.UserType.getDesc(), UserType.Role.getDesc(), 0, 0) );
		
		return  new ResultMsg(ResultCode.Success);
	}
	//roleDeleteModuleData
	public ResultMsg roleDeleteModuleData(ModuleRole moduleRole,HttpServletRequest request){
		if(moduleRole==null){
			return new ResultMsg("310","没有提交请求数据");
		}
		List<ModuleRoleRelative> moduleRoleRelatives=moduleRole.getModules();
		
		if(moduleRoleRelatives==null || moduleRoleRelatives.size()==0){
			return new ResultMsg("320","模块数据不能为空");
		}
		List<ModuleRoleRelative> oldModuleRoleRelativeList=new ArrayList<>();
		ModuleRoleRelative dbModuleRoleRelative=null;
		
		User loginUser = RedisUtil.getUser(redisTemplate, request);
		for(ModuleRoleRelative moduleRoleRelative:moduleRoleRelatives){
			if(moduleRoleRelative.getId()==null){
				return new ResultMsg("330","模块关系id不能为空");
			}
			dbModuleRoleRelative=(ModuleRoleRelative)moduleRoleRelativeMapper.findOne(moduleRoleRelative);
			if(dbModuleRoleRelative!=null){
				oldModuleRoleRelativeList.add(dbModuleRoleRelative);
			}
		}
		deleteExistModuleRoleRelative(loginUser,oldModuleRoleRelativeList);
		
		reloadModuleRoleRelativeData(dbModuleRoleRelative.getModuleId(),dbModuleRoleRelative.getModuleRoleId());
		userService.exitLoginUserExceptSelf(loginUser);
		
		//this.logservice.create(loginUser, "删除模块角色数据关系");
		this.logservice.create(new Log(loginUser.getId(), "删除模块角色数据关系", LogType.UserType.getDesc(), UserType.Role.getDesc(), 2, 0) );
		
		return  new ResultMsg(ResultCode.Success);
	}
	
	public void reloadAllModuleRoleRelativeData(){
		
		List<ModuleRoleRelative> modules=moduleRoleRelativeMapper.findModules(new ModuleRoleRelative());
		if(modules!=null && modules.size()>0){
			for(ModuleRoleRelative moduleRoleRelative:modules){
				reloadModuleRoleRelativeData(moduleRoleRelative.getModuleId(),moduleRoleRelative.getModuleRoleId());
			}
		}
	}
	
	public void reloadModuleRoleRelativeData(Integer moduleId,Integer moduleRoleId){
		String moduleRoleIdKey=KeysManager.User_Auth+KeysManager.ModuleId+moduleId+KeysManager.ModuleRoleId+moduleRoleId;
		RedisUtil.deleteMiddleLike(redisTemplate, moduleRoleIdKey);
		
		Map<String, ModuleRoleRelative> moduleRoleRelativeMap=new HashMap<>();
		ModuleRoleRelative moduleRoleRelative=new ModuleRoleRelative();
		moduleRoleRelative.setModuleId(moduleId);
		moduleRoleRelative.setModuleRoleId(moduleRoleId); 
		List<ModuleRoleRelative> list=moduleRoleRelativeMapper.findList(moduleRoleRelative);
		if(list!=null && list.size()>0){
			moduleRoleRelativeMap = getDistinctModuleRoleRelativeMap(list);
		}
		
		redisTemplate.opsForValue().set(moduleRoleIdKey, moduleRoleRelativeMap);
	}
	public ResultMsg roleAddModuleData(ModuleRole moduleRole,HttpServletRequest request){
		if(moduleRole==null){
			return new ResultMsg("310","没有提交请求数据");
		}
		List<ModuleRoleRelative> moduleRoleRelatives=moduleRole.getModules();
		
		if(moduleRoleRelatives==null || moduleRoleRelatives.size()==0){
			return new ResultMsg("320","模块数据不能为空");
		}
		
		ModuleRoleRelative module=moduleRoleRelatives.get(0);
		//Boolean allowFindAll=module.getAllowFindAll();
		Boolean allowUpdate=module.getAllowUpdate();
		Boolean allowDelete=module.getAllowDelete();
		List<ModuleRoleRelative> oldModuleRoleRelativeList=new ArrayList<>();
		
		User loginUser = RedisUtil.getUser(redisTemplate, request);
		for(ModuleRoleRelative moduleRoleRelative:moduleRoleRelatives){
			if(moduleRoleRelative.getModuleId()==null){
				return new ResultMsg("330","模块id不能为空");
			}
			if(moduleRoleRelative.getModuleRoleId()==null){
				return new ResultMsg("340","模块角色id不能为空");
			}
			if(StringUtil.isEmpty(moduleRoleRelative.getDataId())){
				return new ResultMsg("340","模块数据id不能为空");
			}
			getOldModuleRoleRelative(oldModuleRoleRelativeList,moduleRoleRelative);
			
			moduleRoleRelative.setAllowDetail(true);
			moduleRoleRelative.setCreateUserId(loginUser.getId());
		}
		deleteExistModuleRoleRelative(loginUser,oldModuleRoleRelativeList);
		
		
		//将有关系id的记录，提取出来
		//删除不是这个模块、模块角色、不是这些关系id的数据记录
		//添加没有关系id的数据记录
		//List<ModuleRoleRelative> modules=moduleRoleRelativeMapper.findModules(moduleRoleRelative);
		
		
		moduleRoleRelativeMapper.batchSave(moduleRoleRelatives);
		
		reloadModuleRoleRelativeData(module.getModuleId(),module.getModuleRoleId());
		userService.exitLoginUserExceptSelf(loginUser);
		
		
		//this.logservice.create(loginUser, "添加模块角色数据关系");
		this.logservice.create(new Log(loginUser.getId(), "添加模块角色数据关系", LogType.UserType.getDesc(), UserType.Role.getDesc(), 0, 0) );
		
		return  new ResultMsg(ResultCode.Success);
	}
	
	private void deleteExistModuleRoleRelative(User loginUser,List<ModuleRoleRelative> oldModuleRoleRelativeList){
		if(oldModuleRoleRelativeList.isEmpty()) return;
		
		for(ModuleRoleRelative moduleRoleRelative:oldModuleRoleRelativeList){
			moduleRoleRelative.setIsValid(0);
			moduleRoleRelative.setModifyUserId(loginUser.getId());
			moduleRoleRelativeMapper.update(moduleRoleRelative);
		}
	}
	private void getOldModuleRoleRelative(List<ModuleRoleRelative> oldModuleRoleRelativeList,ModuleRoleRelative moduleRoleRelative){
		ModuleRoleRelative mrr=new ModuleRoleRelative();
		mrr.setModuleId(moduleRoleRelative.getModuleId());
		mrr.setModuleRoleId(moduleRoleRelative.getModuleRoleId());
		mrr.setDataId(moduleRoleRelative.getDataId());
		ModuleRoleRelative dbModuleRoleRelative=(ModuleRoleRelative)moduleRoleRelativeMapper.findOne(mrr);
		if(dbModuleRoleRelative!=null){
			oldModuleRoleRelativeList.add(dbModuleRoleRelative);
		}
		
	}
	public ResultMsg roleDeleteModule(ModuleRoleRelative moduleRoleRelative,HttpServletRequest request){
		if(moduleRoleRelative==null){
			return new ResultMsg("310","没有提交请求数据");
		}
		if(moduleRoleRelative.getId()==null){
			return new ResultMsg("320","模块角色关系id不能为空");
		}
		ModuleRoleRelative dbModuleRoleRelative=(ModuleRoleRelative)moduleRoleRelativeMapper.findOne(moduleRoleRelative);
		if(dbModuleRoleRelative==null){
			return new ResultMsg("330","没有要删除的记录");
		}
		
		
		User loginUser = RedisUtil.getUser(redisTemplate, request);
		moduleRoleRelative.setModifyUserId(loginUser.getId());
		
		moduleRoleRelative.setIsValid(0);
		moduleRoleRelative.setModuleId(dbModuleRoleRelative.getModuleId());
		moduleRoleRelative.setModuleRoleId(dbModuleRoleRelative.getModuleRoleId());
		moduleRoleRelativeMapper.updateByModuleRole(moduleRoleRelative);
		
		String moduleRoleIdKey=KeysManager.User_Auth+KeysManager.ModuleId+dbModuleRoleRelative.getModuleId()+KeysManager.ModuleRoleId+dbModuleRoleRelative.getModuleRoleId();
		RedisUtil.deleteMiddleLike(redisTemplate, moduleRoleIdKey);
		
		userService.exitLoginUserExceptSelf(loginUser);
		
		//this.logservice.create(loginUser, "删除模块角色关系");
		this.logservice.create(new Log(loginUser.getId(), "删除模块角色关系", LogType.UserType.getDesc(), UserType.Role.getDesc(), 2, 0) );
		
		return  new ResultMsg(ResultCode.Success);
	} 
	
	
	/**
	 * 获取用户可以访问的id,
	 * 结果中，null代表允许访问所有数据，空字符串代表不能访问所有数据，其他代表可以访问的数据
	 * @param redisTemplate
	 * @param user
	 * @param moduleRoleRelativeMap
	 * @return
	 */
	public static String GetUserModuleDataIds(RedisTemplate redisTemplate,User user,Map<String, ModuleRoleRelative> moduleRoleRelativeMap){
		if(user.getUsername().equalsIgnoreCase(KeysManager.Admin) 
				|| (user.getRoleIds()!=null && user.getRoleIds().equalsIgnoreCase(KeysManager.AdminRoleId))){
			return null;//允许访问所有
		}
		
		if(moduleRoleRelativeMap==null) return null; //允许访问所有
		if(moduleRoleRelativeMap.isEmpty()) return ""; //不允许访问所有
		
		StringBuilder sb=new StringBuilder();
		Set<String> dataIdSet=moduleRoleRelativeMap.keySet();
		for(String dataId:dataIdSet){
			ModuleRoleRelative moduleRoleRelative=moduleRoleRelativeMap.get(dataId);
			
			if(moduleRoleRelative.getDataId().equals("/") ){
				if( moduleRoleRelative.getAllowFindAll() !=null && moduleRoleRelative.getAllowFindAll()){
					return null;//模块拥有获取所有权限	
				}
				continue;
			} 
			sb.append(dataId+",");
		}
		if(sb.length()==0) return null;
		
		sb.delete(sb.length()-1, sb.length());
		String dataIds=sb.toString();
		
		Integer moduleId=moduleRoleRelativeMap.values().iterator().next().getModuleId();
		ModuleType moduleType=GetModuleType(redisTemplate,moduleId);
		
		if(moduleType.getDataIdType()==2){//字符串
			return StringUtils.wordsToSqlWords(dataIds);
		}
		return dataIds;
	}
	
	public static String GetUserModuleDataIds(HttpServletRequest request,RedisTemplate redisTemplate,Integer moduleTypeCode){
		User user=RedisUtil.getUserAuthUser(redisTemplate, request);
		Map moduleRoleRelativeMap=GetUserModuleData(redisTemplate, user, moduleTypeCode);
		String dataIds=GetUserModuleDataIds(redisTemplate,user, moduleRoleRelativeMap);
		return dataIds;
	}
	
	public static ModuleType GetModuleType(RedisTemplate redisTemplate,Integer moduleTypeId){
		
		String moduleTypeKey=KeysManager.User_Auth+KeysManager.ModuleType+moduleTypeId;
		ModuleType moduleType=(ModuleType)redisTemplate.opsForValue().get(moduleTypeKey);
		
		return moduleType; 
	}
	
	public static Map<String, ModuleRoleRelative> GetUserModuleData(RedisTemplate redisTemplate,User user,Integer moduleId){
		if(user.getUsername().equalsIgnoreCase(KeysManager.Admin) || (user.getRoleIds()!=null && user.getRoleIds().equalsIgnoreCase(KeysManager.AdminRoleId))){
			return null;//允许访问所有
		}
		log.info("username:"+user.getUsername()+",moduleId:"+moduleId);
		String moduleTypeKey=KeysManager.User_Auth+KeysManager.ModuleType+moduleId;
		Object moduleType=redisTemplate.opsForValue().get(moduleTypeKey);
		log.info("moduleType:"+moduleType);
		
		if(moduleType==null) {
			return null;//没有配置的模块，不需要权限
		}
	
		
		Map<String, ModuleRoleRelative>  userModuleRoleRelativeMap=new HashMap<String, ModuleRoleRelative>();
		
		if(user.getUserModuleRoles()!=null && user.getUserModuleRoles().size()>0){
			log.info("user.getUserModuleRoles():"+user.getUserModuleRoles());
			
			for(UserModuleRole userModuleRole:user.getUserModuleRoles()){
				String moduleRoleIdKey=KeysManager.User_Auth+KeysManager.ModuleId+moduleId+KeysManager.ModuleRoleId+userModuleRole.getModuleRoleId();
				Map<String, ModuleRoleRelative>  moduleRoleRelativeMap=(Map<String, ModuleRoleRelative>)redisTemplate.opsForValue().get(moduleRoleIdKey);
				if(moduleRoleRelativeMap!=null && moduleRoleRelativeMap.size()>0){
					userModuleRoleRelativeMap.putAll(moduleRoleRelativeMap);
				}
			}
		}
		log.info("userModuleRoleRelativeMap:"+userModuleRoleRelativeMap);
		
		return userModuleRoleRelativeMap;
	}
	/**
	 * 设置用户的模块的所有数据到缓存，查询的数据id和各种操作属性
	 */
	public void setUserModuleData(User user,ModuleType moduleType){
		if(user.getUsername().equalsIgnoreCase(KeysManager.Admin)
				|| (user.getRoleIds()!=null && user.getRoleIds().equalsIgnoreCase(KeysManager.AdminRoleId))){
			return ;//允许访问所有
		}
		
		
		if(!StringUtil.isEmpty(user.getModuleRoleIds())){
			for(UserModuleRole userModuleRole:user.getUserModuleRoles()){
				String moduleRoleIdKey=KeysManager.User_Auth+KeysManager.ModuleId+moduleType.getId()+KeysManager.ModuleRoleId+userModuleRole.getModuleRoleId();
				Map<String, ModuleRoleRelative> moduleRoleRelativeMap=(Map<String, ModuleRoleRelative>)redisTemplate.opsForValue().get(moduleRoleIdKey);
				if(moduleRoleRelativeMap!=null){
					continue;
				}
				
				moduleRoleRelativeMap=new HashMap<>();
				ModuleRoleRelative moduleRoleRelative=new ModuleRoleRelative();
				moduleRoleRelative.setModuleId(moduleType.getId());
				moduleRoleRelative.setModuleRoleId(userModuleRole.getModuleRoleId()); 
				List<ModuleRoleRelative> list=moduleRoleRelativeMapper.findList(moduleRoleRelative);
				if(list!=null && list.size()>0){
					moduleRoleRelativeMap = getDistinctModuleRoleRelativeMap(list);
				}
				
				redisTemplate.opsForValue().set(moduleRoleIdKey, moduleRoleRelativeMap);
			}
		} 
		
	}
	private Map<String,ModuleRoleRelative> getDistinctModuleRoleRelativeMap(List<ModuleRoleRelative> list){
		Map<String,ModuleRoleRelative> map=new HashMap<>();
		
		if(list==null || list.isEmpty()) return map;
		
		for(ModuleRoleRelative moduleRoleRelative:list){
			if(moduleRoleRelative.getDataId()==null) {
				continue;
			}
			if(map.containsKey(moduleRoleRelative.getDataId())) continue;
			
			map.put(moduleRoleRelative.getDataId(), moduleRoleRelative);
		}
		return map;
	}
	
}
