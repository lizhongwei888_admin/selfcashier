package com.zzsy.api.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

@ColumnWidth(16)
@HeadRowHeight(20)
@ContentRowHeight(18)
public class RefLogExcel implements Serializable {
    private static final long serialVersionUID = -301062307193761449L;

    @ColumnWidth(20)
    @ExcelProperty(value = "唯一标识")
    private String code;
    @ColumnWidth(20)
    @ExcelProperty(value = "页面名称")
    private String name;
    @ColumnWidth(20)
    @ExcelProperty(value = "业务类型")
    private String type;
    @ColumnWidth(20)
    @ExcelProperty(value = "操作内容")
    private String content;
    @ColumnWidth(20)
    @ExcelProperty(value = "请求方式")
    private String payType;
    @ColumnWidth(20)
    @ExcelProperty(value = "方法名称")
    private String method;
    @ColumnWidth(20)
    @ExcelProperty(value = "请求url地址")
    private String url;
    @ColumnWidth(20)
    @ExcelProperty(value = "主机IP地址")
    private String ip;
    @ColumnWidth(20)
    @ExcelProperty(value = "操作地点")
    private String site;
    @ColumnWidth(20)
    @ExcelProperty(value = "请求参数")
    private String requestParm;
    @ColumnWidth(20)
    @ExcelProperty(value = "返回结果")
    private String returnParm;
    @ColumnWidth(20)
    @ExcelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    protected Date create_time;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getRequestParm() {
        return requestParm;
    }

    public void setRequestParm(String requestParm) {
        this.requestParm = requestParm;
    }

    public String getReturnParm() {
        return returnParm;
    }

    public void setReturnParm(String returnParm) {
        this.returnParm = returnParm;
    }

    public Date getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
