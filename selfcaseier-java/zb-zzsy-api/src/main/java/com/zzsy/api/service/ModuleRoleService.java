package com.zzsy.api.service;

import java.util.Date;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import com.zzsy.api.constant.KeysManager;
import com.zzsy.api.constant.LogType;
import com.zzsy.api.constant.UserType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import com.github.pagehelper.util.StringUtil;
import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.service.BasicService;
import com.zzsy.api.entity.Log;
import com.zzsy.api.entity.ModuleRole;
import com.zzsy.api.entity.User;
import com.zzsy.api.mapper.ModuleRoleMapper;
import com.zzsy.api.util.RedisUtil;

@Service
public class ModuleRoleService extends BasicService{
	
	@Autowired
	private LogService logservice;
	
	@Autowired 
	private RedisTemplate redisTemplate;
	
	@Autowired
	private ModuleRoleMapper moduleRoleMapper;
	@Resource(name="moduleRoleMapper")
	public void initBaseDao(ModuleRoleMapper moduleRoleMapper){
		super.setBaseMapper(moduleRoleMapper);
	} 
	
	public ResultMsg save(ModuleRole moduleRole,HttpServletRequest request){
		if(StringUtil.isEmpty(moduleRole.getName())){
			return new ResultMsg("310","模块角色名不能为空");
		}
		
		ModuleRole dbModuleRole = (ModuleRole)moduleRoleMapper.findOne(moduleRole);
		if(dbModuleRole!=null){
			return new ResultMsg("310","已经存在了模块角色名:"+moduleRole.getName());
		}
		User loginUser = RedisUtil.getUser(redisTemplate, request);
		
		moduleRole.setCreateUserId(loginUser.getId());
		moduleRoleMapper.save(moduleRole);
		
		//this.logservice.create(loginUser, "添加模块角色【"+moduleRole.getName()+"】");
		this.logservice.create(new Log(loginUser.getId(), "添加模块角色【"+moduleRole.getName()+"】", LogType.UserType.getDesc(), UserType.Role.getDesc(), 0, 0) );

		
		return  new ResultMsg(ResultCode.Success);
	}
	public ResultMsg update(ModuleRole moduleRole,HttpServletRequest request){
		if(moduleRole.getId()==null){
			return new ResultMsg("310","模块角色id不能为空");
		}
		if(StringUtil.isEmpty(moduleRole.getName())){
			return new ResultMsg("310","模块角色名不能为空");
		}
		User loginUser = RedisUtil.getUser(redisTemplate, request);
		
		moduleRole.setModifyTime(new Date());
		moduleRole.setModifyUserId(loginUser.getId());
		moduleRoleMapper.update(moduleRole);
		
		//this.logservice.create(loginUser, "修改模块角色名【"+moduleRole.getName()+"】");
		this.logservice.create(new Log(loginUser.getId(), "修改模块角色名【"+moduleRole.getName()+"】", LogType.UserType.getDesc(), UserType.Role.getDesc(), 1, 0) );

		
		return  new ResultMsg(ResultCode.Success);
	}
	public ResultMsg delete(ModuleRole moduleRole,HttpServletRequest request){
		if(moduleRole.getId()==null){
			return new ResultMsg("310","模块角色id不能为空");
		}
		User loginUser = RedisUtil.getUser(redisTemplate, request);
		moduleRole.setIsValid(0);
		moduleRole.setModifyTime(new Date());
		moduleRole.setModifyUserId(loginUser.getId());
		moduleRoleMapper.update(moduleRole);
		
		String moduleRoleIdsUsernameKey= KeysManager.ModuleRoleId+moduleRole.getId();
		RedisUtil.deleteMiddleLike(redisTemplate, moduleRoleIdsUsernameKey);
		
		//this.logservice.create(loginUser, "删除模块角色");
		this.logservice.create(new Log(loginUser.getId(), "删除模块角色", LogType.UserType.getDesc(), UserType.Role.getDesc(), 2, 0) );

		
		return  new ResultMsg(ResultCode.Success);
	}
	
}
