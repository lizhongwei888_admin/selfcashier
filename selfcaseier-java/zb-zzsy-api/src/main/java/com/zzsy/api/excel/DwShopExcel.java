package com.zzsy.api.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.zzsy.api.listene.ShopTypeConverter;

import java.io.Serializable;

public class DwShopExcel implements Serializable {
    private static final long serialVersionUID = -301062307193761449L;

    @ExcelProperty(index = 0)
    private String merchantId;
    @ExcelProperty(index = 1)
    private String shopCode;
    @ExcelProperty(index = 2)
    private String shopName;
    @ExcelProperty(index = 3)
    private String shopLogo;
    @ExcelProperty(index = 4)
    private String phone;
    @ExcelProperty(index = 5)
    private String onlineHours;
    @ExcelProperty(index = 6)
    private String onlineArea;
    @ExcelProperty(index = 7)
    private String onlineMph;
    @ExcelProperty(index = 8)
    private String shopUrls;
    @ExcelProperty(index = 9)
    private String shopDesc;
    @ExcelProperty(index = 10, converter = ShopTypeConverter.class)

    private Integer shopType;
    @ExcelProperty(index = 11)
    private String remark;
    @ExcelProperty(index = 12)
    private String state;

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopLogo() {
        return shopLogo;
    }

    public void setShopLogo(String shopLogo) {
        this.shopLogo = shopLogo;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getOnlineHours() {
        return onlineHours;
    }

    public void setOnlineHours(String onlineHours) {
        this.onlineHours = onlineHours;
    }

    public String getOnlineArea() {
        return onlineArea;
    }

    public void setOnlineArea(String onlineArea) {
        this.onlineArea = onlineArea;
    }

    public String getOnlineMph() {
        return onlineMph;
    }

    public void setOnlineMph(String onlineMph) {
        this.onlineMph = onlineMph;
    }

    public String getShopUrls() {
        return shopUrls;
    }

    public void setShopUrls(String shopUrls) {
        this.shopUrls = shopUrls;
    }

    public String getShopDesc() {
        return shopDesc;
    }

    public void setShopDesc(String shopDesc) {
        this.shopDesc = shopDesc;
    }

    public Integer getShopType() {
        return shopType;
    }

    public void setShopType(Integer shopType) {
        this.shopType = shopType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
