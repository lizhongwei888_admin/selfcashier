package com.zzsy.api.entity;

import com.lujie.common.entity.BasicEntity;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 系统配置-系统接口定义(RefApi)实体类
 *
 * @author makejava
 * @since 2024-02-04 20:18:53
 */
public class RefApi extends BasicEntity implements Serializable {
    private static final long serialVersionUID = -38993542839775202L;
    /**
     * 唯一标识
     */
    private String code;
    /**
     * 接口唯一标识
     */
    private String apicode;
    /**
     * 接口名称
     */
    private String apiname;
    /**
     * 描述
     */
    private String remark;

    /****非数据库字段*********/
    //是否需要子集 0是需要 1是list集合 2 是map集合
    private Integer needSubset = 0;

    private List<RefApiItem> listSubset;

    private Map<String, String> mapSubset;

    /****结束*********/


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getApicode() {
        return apicode;
    }

    public void setApicode(String apicode) {
        this.apicode = apicode;
    }

    public String getApiname() {
        return apiname;
    }

    public void setApiname(String apiname) {
        this.apiname = apiname;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getNeedSubset() {
        return needSubset;
    }

    public void setNeedSubset(Integer needSubset) {
        this.needSubset = needSubset;
    }

    public List<RefApiItem> getListSubset() {
        return listSubset;
    }

    public void setListSubset(List<RefApiItem> listSubset) {
        this.listSubset = listSubset;
    }

    public Map<String, String> getMapSubset() {
        return mapSubset;
    }

    public void setMapSubset(Map<String, String> mapSubset) {
        this.mapSubset = mapSubset;
    }
}

