package com.zzsy.api.service;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import com.zzsy.api.constant.KeysManager;
import com.zzsy.api.constant.LogType;
import com.zzsy.api.constant.LoginType;
import com.zzsy.api.constant.UserType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import com.lujie.common.constant.ConfigProperties;
import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.service.BasicService;
import com.lujie.common.util.MD5Util;
import com.lujie.common.util.StringUtil;
import com.zzsy.api.entity.City;
import com.zzsy.api.entity.Log;
import com.zzsy.api.entity.Menu;
import com.zzsy.api.entity.User;
import com.zzsy.api.entity.UserModuleRole;
import com.zzsy.api.mapper.CityMapper;
import com.zzsy.api.mapper.MenuMapper;
import com.zzsy.api.mapper.UserMapper;
import com.zzsy.api.util.RedisUtil;

@Service
public class UserService extends BasicService {
	public static final Logger log = LoggerFactory.getLogger(UserService.class);

	@Autowired
	private RoleService roleService;
	@Autowired
	private MenuMapper menuMapper;
	@Autowired
	private UserModuleRoleService userModuleRoleService;

	@Autowired
	private MenuService menuService;

	@Autowired
	private LogService logservice;

	@Autowired
	private RedisTemplate redisTemplate;
	
	@Autowired
	SseService sseService;

	@Autowired
	private UserMapper userMapper;

	@Resource(name = "userMapper")
	public void initBaseDao(UserMapper userMapper) {
		super.setBaseMapper(userMapper);
	}

	public List<User> findListByDept(User user) {
		return userMapper.findListByDept(user);
	}

	public Integer findListCountByDept(User user) {
		return userMapper.findListCountByDept(user);
	}

	public int deleteUser(User user) {
		User u = new User();
		u.setId(user.getId());
		u.setIsValid(0);
		userMapper.updateByPrimaryKeySelective(u);
		return 0;
	}

	public int updatePwd(User record) {
		userMapper.updateByPrimaryKeySelective(record);
		return 0;
	}

	public int updatePwdByMobile(User record) {
		userMapper.updatePwdByMobile(record);
		return 0;
	}

	public int add(User user) {
		user.setId(UUID.randomUUID().toString().replace("-", ""));
		userMapper.insertSelective(user);
		return 0;
	}

	public int update(User user) {
		userMapper.updateByPrimaryKeySelective(user);
		return 0;
	}

	public User findByUserName(String userName) {
		User tempUewUser = new User();
		tempUewUser.setUsername(userName);
		User user = (User) userMapper.findOne(tempUewUser);
		return user;
	}

	public User findByName(String name) {
		User tempUewUser = new User();
		tempUewUser.setRealname(name);
		User user = (User) userMapper.findOne(tempUewUser);
		return user;
	}

	@Autowired
	private CityMapper cityMapper;

	public List findCityList() {
		City testCity = new City();
		testCity.setCityid("7a805649916e4317824234cc1a3492d6");
		;
		testCity.setModuleId(1);
		testCity.setModuleRoleId(2193);
		List city = cityMapper.findPageList(testCity);
		return city;
	}

	public User findByToken(String token) {
		User tempUewUser = new User();
		tempUewUser.setToken(token);
		User user = (User) userMapper.findOne(tempUewUser);
		return user;
	}

	public User findByMobile(String mobile) {
		User tempUewUser = new User();
		tempUewUser.setMobilephone(mobile);
		User user = (User) userMapper.findOne(tempUewUser);
		return user;
	}

	public ResultMsg login(User user) {

		Integer loginType = 0;
		if (!StringUtil.isEmpty(user.getUsername())) {
			loginType = 1;
		}
		if (!StringUtil.isEmpty(user.getMobilephone())) {
			loginType = 2;
		}
		if (loginType == 0) {
			return new ResultMsg("301", "用户名或手机号不能为空");
		}

		if (StringUtil.isEmpty(user.getPassword())) {
			return new ResultMsg("302", "密码不能为空");
		}

		if (StringUtil.isEmpty(user.getCode())) {
			return new ResultMsg("303", "验证码不能为空");
		}
		if (StringUtil.isEmpty(user.getUserKey())) {
			return new ResultMsg("3011", "验证码key不能为空");
		}

		Object verifyCodeObj = RedisUtil.getVerifyCode(redisTemplate, user.getUserKey());
		if (verifyCodeObj == null) {
			return new ResultMsg("304", "验证码不存在");
		}
		if (!verifyCodeObj.toString().equals(user.getCode())) {
			return new ResultMsg("305", "验证码错误");
		}

		User dbUser = null;
		if (loginType.equals(1)) {
			dbUser = findByUserName(user.getUsername());
		} else {
			dbUser = findByMobile(user.getMobilephone());
		}
		if (dbUser == null) {
			return new ResultMsg("306", "系统不存在这个用户");
		}
		String encryptPassword = MD5Util.encrypt(user.getPassword());
		if (!dbUser.getPassword().equals(encryptPassword)) {
			return new ResultMsg("308", "密码有误");
		}

		if (!dbUser.getIsactive().equals(1)) {
			return new ResultMsg("309", "未激活");
		}
		if (!dbUser.getLoginType().equals(LoginType.AccountLoginUser.getCode())) {
			return new ResultMsg("310", "账号类型有误，不允许登录");
		}

		Integer sessionMinuteTime = 7 * 24 * 60;
		if (!KeysManager.Admin.equalsIgnoreCase(user.getUsername())) {
			String roles = this.roleService.findRoleByUid(user.getId());
			dbUser.setRoleIds(roles);
			dbUser.setRoleMenuBackendUrls(getUserMenuBackendUrls(dbUser));

			List<UserModuleRole> userModuleRoles = userModuleRoleService.findByUserId(dbUser.getId());
			dbUser.setUserModuleRoles(userModuleRoles);
			dbUser.setModuleRoleIds(userModuleRoleService.getModuleRoleIds(userModuleRoles));

			if (loginType != null && loginType.equals(2)) {
				List<Menu> sl = new ArrayList<Menu>();
				Menu menu = new Menu();
				menu.setId(null);
				menu.setPid(null);
				menuService.allChildMenu(sl, menu, dbUser.getId());
				dbUser.setMenuList(sl);

			}
		} else {
			dbUser.setModuleRoleIds(KeysManager.Admin.toLowerCase());
		}

		dbUser.setPassword(null);// 密码保密，不能传给前台
		
		// 同个账号登录多台设备，最后一次登录生效，前面的失效
		if (!dbUser.getUsername().contains("admin")) {
			// 断开sse连接
			try {
				sseService.removeUid(dbUser.getId());
			} catch (Exception e) {
				// TODO: handle exception
			}

			// 删除这个userId下所有的TOKEN
			RedisUtil.removeTokenByUserId(redisTemplate, dbUser.getId());
		}

		String token = RedisUtil.genarateTokenAndSaveUser(redisTemplate, dbUser, sessionMinuteTime);
		dbUser.setLogintime(new Date());
		userMapper.updateByPrimaryKeySelective(dbUser);

		// logservice.create(user,user.getUsername()+"登陆系统");
		this.logservice.create(new Log(dbUser.getId(), user.getUsername() + "登陆系统", LogType.UserType.getDesc(),
				UserType.Account.getDesc(), 0, 0));

		return new ResultMsg(ResultCode.Success, dbUser);

	}

	private String getUserMenuBackendUrls(User user) {
		StringBuilder sb = new StringBuilder();
		List<Menu> menuList = menuMapper.findByUser(user);
		if (menuList != null && menuList.size() > 0) {
			for (Menu menu : menuList) {
				if (menu.getBackendUrls() != null) {
					sb.append(menu.getBackendUrls());
				}
			}
			return sb.toString();
		}
		return null;

	}

	public ResultMsg logout(HttpServletRequest request) {
		User user = RedisUtil.getUser(redisTemplate, request);

		String userId = user.getId();
		User dbUser = findByUserId(userId);
		dbUser.setOuttime(new Date());
		userMapper.updateByPrimaryKeySelective(dbUser);

		RedisUtil.removeToken(redisTemplate, request);
		// logservice.create(user, user.getUsername()+"退出系统");
		this.logservice.create(new Log(user.getId(), user.getUsername() + "退出系统", LogType.UserType.getDesc(),
				UserType.Account.getDesc(), 0, 0));

		return new ResultMsg(ResultCode.Success);

	}

	public User findByUserId(String userId) {
		User tempUewUser = new User();
		tempUewUser.setId(userId);
		User user = (User) userMapper.findOne(tempUewUser);
		return user;
	}

	public ResultMsg checkNoPicLoginUse(HttpServletRequest request) {

		String token = RedisUtil.getToken(request);

		if (StringUtil.isEmpty(token)) {
			return new ResultMsg("0", "未登录");
		}

		String userId = RedisUtil.getUserId(request);
		if (StringUtil.isEmpty(userId)) {
			return new ResultMsg("330", "userId不能为空");
		}
		User user = findByUserId(userId);
		if (user == null) {
			return new ResultMsg("340", "不不存在这个用户");
		}
		if (user.getIsactive() == null || !user.getIsactive().equals(1)) {
			return new ResultMsg("350", "用户未激活");
		}
		if (user.getIsValid() == null || !user.getIsValid().equals(1)) {
			return new ResultMsg("360", "无效的用户");
		}
		if (user.getLoginType() == null || !user.getLoginType().equals(LoginType.InterfaceUser.getCode())) {
			return new ResultMsg("0", "未登录,不是接口类型的用户");
		}
		if (StringUtil.isEmpty(user.getToken())) {
			return new ResultMsg("380", "未分配token给这个用户");
		}
		if (!user.getToken().equals(token)) {
			return new ResultMsg("390", "系统给这个用户分配的token和请求token不一致");
		}

		List<UserModuleRole> userModuleRoles = userModuleRoleService.findByUserId(user.getId());
		user.setUserModuleRoles(userModuleRoles);
		user.setModuleRoleIds(userModuleRoleService.getModuleRoleIds(userModuleRoles));

		user.setPassword(null);// 密码保密，不能传给前台
		RedisUtil.savePermanentUser(redisTemplate, user);

		return new ResultMsg(ResultCode.Success, user);

	}

	public void exitLoginUserExceptSelf(User loginUser) {
		RedisUtil.deleteMiddleLike(redisTemplate,
				KeysManager.User_Auth + InterfaceAccountService.InterfaceAccountModuleRoleKey);

		List<User> allUser = RedisUtil.getAllUser(redisTemplate);
		if (allUser == null || allUser.size() == 0)
			return;

		for (User user : allUser) {
			if (!loginUser.getId().equals(user.getId())) {
				RedisUtil.exitLoginUser(redisTemplate, user.getId());
			}
		}
	}

	public ResultMsg checkWeChatSmallProgram(HttpServletRequest request) {
		String userAppKey = request.getHeader("appKey");
		String userSign = request.getHeader("sign");
		String timestamp = request.getHeader("timestamp");

		String wechatNo = request.getHeader("wechatNo");
		String wechatId = request.getHeader("wechatId");
		String mobile = request.getHeader("mobile");
		String wechatNick = request.getHeader("wechatNick");

		String token = request.getHeader("token");// wechatNo+wechatId+mobile

		if (userAppKey == null || userAppKey.isEmpty()) {
			return new ResultMsg("301", "appKey不能为空！");
		} else if (userSign == null || userSign.isEmpty()) {
			return new ResultMsg("330", "sign不能为空！");
		} else if (timestamp == null || timestamp.isEmpty()) {
			return new ResultMsg("340", "timestamp不能为空！");
		}
		if (ConfigProperties.AppSecret != null && ConfigProperties.AppSecret.length() > 0) {
			String sign = ConfigProperties.getSign(ConfigProperties.AppKey, ConfigProperties.AppSecret,
					Long.parseLong(timestamp));
			if (!sign.equals(userSign)) {
				return new ResultMsg("350", "sign有误");
			}
		}

		User user = new User();

		user.setWechatNo(wechatNo);
		user.setWechatId(wechatId);
		user.setMobilephone(mobile);
		user.setWechatNick(wechatNick);
		user.setToken(token);

		if (!StringUtil.isEmpty(user.getWechatNick())) {
			try {
				String decodeWechatNick = URLDecoder.decode(user.getWechatNick(), "utf-8");
				// byte[] decodeBytes =
				// java.util.Base64.getDecoder().decode(user.getWechatNick());
				// String decodeWechatNick=new String(decodeBytes);
				user.setWechatNick(decodeWechatNick);
			} catch (Exception ex) {
				log.info("解码昵称失败:" + ex.getMessage());
				return new ResultMsg("364", "解码昵称失败");
			}

		}
		return new ResultMsg(ResultCode.Success, user);
	}

}
