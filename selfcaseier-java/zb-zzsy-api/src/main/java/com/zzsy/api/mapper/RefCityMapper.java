package com.zzsy.api.mapper;

import com.lujie.common.dao.BasicMapper;
import com.zzsy.api.entity.RefCity;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RefCityMapper extends BasicMapper {

    public List<RefCity> findAllRefCity();

}
