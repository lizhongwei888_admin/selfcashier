package com.zzsy.api.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.util.JsonHelper;
import com.lujie.common.util.StringUtil;
import com.zzsy.api.constant.LogType;
import com.zzsy.api.constant.UserType;
import com.zzsy.api.entity.Log;
import com.zzsy.api.entity.Menu;
import com.zzsy.api.entity.User;
import com.zzsy.api.entity.UserRole;
import com.zzsy.api.service.LogService;
import com.zzsy.api.service.MenuService;
import com.zzsy.api.util.RedisUtil;

/**
 * 菜单控制器
 * @author hupei
 *
 */
@Controller
@RequestMapping(value = "/menu")
public class MenuController   {
	 
	@Autowired
	private MenuService menuService;
	@Autowired
	private LogService logservice;
	@Autowired 
	private RedisTemplate redisTemplate;
	/**
	 * 菜单首页
	 * @return
	 */
	@RequestMapping(value={"/findAll"})
	@ResponseBody
	public String findAll(@RequestBody Menu menu){
		List<Menu> menuList =new ArrayList<Menu>();
		menuService.allMenu(menuList, menu);
		
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success,menuList));
	}
	/**
	 * 根据用户id，获取用户菜单
	 * @param userRole
	 * @return
	 */
	@RequestMapping(value="/getMenuByUser")
	@ResponseBody
	public String getmenu(@RequestBody UserRole userRole){
		List<Menu> menuList=new ArrayList<Menu>();
		menuService.allChildMenu(menuList, new Menu(), userRole.getUid());
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success,menuList));
	}
	@RequestMapping(value="/getMenuByRole")
	@ResponseBody
	public String getMenuByRole(@RequestBody UserRole userRole){
		List<Menu> menuList = new ArrayList<Menu>();
		Map<String, Object> map=new HashMap<>();
		map.put("roleid", userRole.getRid());
		menuService.menufromrole(menuList, map, new Menu());
		menuService.setParentNodeUnChecked(menuList);
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success,menuList));
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ResponseBody
	public String add(@RequestBody Menu menu , HttpServletRequest request){
		if(StringUtil.isEmpty(menu.getText())){
			return JsonHelper.toJson(new ResultMsg("310","菜单名不能为空"));
		}
		this.menuService.saveOrUpdate(menu);
		
		User user = RedisUtil.getUser(redisTemplate, request);
		this.logservice.create(new Log(user.getId(), "新增菜单【"+menu.getText()+"】", LogType.UserType.getDesc(), UserType.Menu.getDesc(), 0, 0) );
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success));
	}
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public String update(@RequestBody Menu menu , HttpServletRequest request){
		if(StringUtil.isEmpty(menu.getId())){
			return JsonHelper.toJson(new ResultMsg("310","菜单id不能为空"));
		}
		
		this.menuService.saveOrUpdate(menu);
		
		User user = RedisUtil.getUser(redisTemplate, request);
		//this.logservice.create(user, "修改菜单【"+menu.getText()+"】");
		this.logservice.create(new Log(user.getId(), "修改菜单【"+menu.getText()+"】", LogType.UserType.getDesc(), UserType.Menu.getDesc(), 1, 0) );

		return JsonHelper.toJson(new ResultMsg(ResultCode.Success));
	}
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public String delete(@RequestBody Menu menu , HttpServletRequest request){
		Menu dbmenu = this.menuService.findOneMenu(menu.getId());
		this.menuService.delMenu(menu.getId());
		User user = RedisUtil.getUserByToken(redisTemplate, request);
		//this.logservice.create(user, "删除菜单【"+dbmenu.getText()+"】");
		this.logservice.create(new Log(user.getId(), "删除菜单【"+dbmenu.getText()+"】", LogType.UserType.getDesc(), UserType.Menu.getDesc(), 2, 0) );

		return JsonHelper.toJson(new ResultMsg(ResultCode.Success));
	}
}
