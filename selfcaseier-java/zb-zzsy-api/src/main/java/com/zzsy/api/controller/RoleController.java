package com.zzsy.api.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.util.StringUtil;
import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.util.JsonHelper;
import com.zzsy.api.constant.LogType;
import com.zzsy.api.constant.UserType;
import com.zzsy.api.entity.Log;
import com.zzsy.api.entity.Role;
import com.zzsy.api.entity.RoleMenu;
import com.zzsy.api.entity.User;
import com.zzsy.api.service.LogService;
import com.zzsy.api.service.RoleService;
import com.zzsy.api.util.RedisUtil;

/**
 * 角色控制器
 * @author hupei
 *
 */
@Controller
@RequestMapping(value = "/role")
public class RoleController  {
	
	@Autowired
	private RoleService roleService;
	@Autowired
	private LogService logservice;
	@Autowired 
	private RedisTemplate redisTemplate;
	
	@RequestMapping(value={"/findAll"})
	@ResponseBody
	public String findAll(@RequestBody Role role){
		List<Role> roleList=roleService.findList(role);
		Long totalCount=roleList==null? 0:Long.parseLong(roleList.size()+"");
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success,roleList,totalCount));
	}
	
	/**
	 * 角色首页
	 * @return
	 */
	@RequestMapping(value={"/list"})
	@ResponseBody
	public String list(@RequestBody Role role){
		List<Role> roleList=roleService.findPageList(role);
		Long totalCount=roleService.countTotal(role);
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success,roleList,totalCount));
	}
	
	/**
	 * 批量保存角色
	 */
	@RequestMapping(value = "/saveOrUpdate", method = RequestMethod.POST)
	@ResponseBody
	public String add(@RequestBody Role role,HttpServletRequest request){
		User user = RedisUtil.getUser(redisTemplate, request);
		if(StringUtil.isEmpty(role.getText())){
			return JsonHelper.toJson(new ResultMsg("310","角色名不能为空"));
		}
		if ( role.getId()!=null){
			//this.logservice.create(user, "修改角色【"+role.getText()+"】");
			this.logservice.create(new Log(user.getId(), "修改角色【"+role.getText()+"】", LogType.UserType.getDesc(), UserType.Role.getDesc(), 1, 0) );

		}else{
			//this.logservice.create(user, "新增角色【"+role.getText()+"】");
			this.logservice.create(new Log(user.getId(), "新增角色【"+role.getText()+"】", LogType.UserType.getDesc(), UserType.Role.getDesc(), 0, 0) );

		}
		this.roleService.saveOrUpdate(role);
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success));
	}
	
	/**
	 * 删除角色
	 * @param roleid
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public String remove(@RequestBody Role role,HttpServletRequest request){
		Role dbrole = this.roleService.findOneRole(role.getId());
		this.roleService.delRole(role.getId());
		User user = RedisUtil.getUser(redisTemplate, request);
		//logservice.create(user, "删除角色【"+dbrole.getText()+"】");
		this.logservice.create(new Log(user.getId(), "删除角色【"+dbrole.getText()+"】", LogType.UserType.getDesc(), UserType.Role.getDesc(), 2, 0) );

		return JsonHelper.toJson(new ResultMsg(ResultCode.Success));
	}
	
	/**
	 * 通过菜单获取拥有菜单的所有角色
	 * @param menuid
	 * @param tabid
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/getRoleByMenu")
	@ResponseBody
	public String getRoleByMenu(@RequestBody RoleMenu roleMenu){
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success,
				roleService.findRoleListByMenu(roleMenu),
				Long.parseLong(roleService.countRoleListByMenu(roleMenu).toString())));
	} 
}
