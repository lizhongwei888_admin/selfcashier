package com.zzsy.api.constant;

/**
 * 内集卡调度-指令描述
 */
public enum LoadBoxEnum  {

	NONE( 0 , "无") ,
	EMPTY (1 , "空"),
	FULL(2 , "满") ;
	
	private Integer code;

	private String desc;

	LoadBoxEnum(Integer code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public Integer getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}
	
	public static String getDesc(Integer code) {
		
		LoadBoxEnum[] logTypes = values();
		for(LoadBoxEnum logType : logTypes) {
			if(code.equals(logType.getCode())) {
				return logType.getDesc();
			}
		}
		return "";
	}

}
