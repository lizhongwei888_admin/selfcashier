package com.zzsy.api.service;

import com.lujie.common.service.BasicService;
import com.zzsy.api.entity.DwMerchant;
import com.zzsy.api.entity.DwShop;
import com.zzsy.api.mapper.DwMerchantMapper;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class DwMerchantService extends BasicService{
	
	@Autowired
	private RedisTemplate redisTemplate;

	@Autowired
	private DwMerchantMapper dwMerchantMapper;
	@Resource(name="dwMerchantMapper")
	public void initBaseDao(DwMerchantMapper dwMerchantMapper){
		super.setBaseMapper(dwMerchantMapper);
	}

	public DwMerchant getInfoByMerchantCode(String merchantCode){
		DwMerchant dwm = dwMerchantMapper.getInfoByMerchantCode(merchantCode);
		return dwm;
	}

	public List<DwMerchant> findAll(DwMerchant dwMerchant){
		List<DwMerchant> dwm = dwMerchantMapper.findAllByParam(dwMerchant);
		return dwm;
	}


	public Long deleteByCode(String codes) {
		Set<String> collect = Arrays.stream(codes.split(",")).collect(Collectors.toSet());
		return dwMerchantMapper.deleteByIdSet(collect);
	}

	/**
	 * 添加或修改
	 *
	 * @param dwMerchant
	 * @return
	 */
	public int addOrUp(DwMerchant dwMerchant) {
		if (StringUtils.isEmpty(dwMerchant.getCode())) {
			//插入唯一code
			dwMerchant.setCode(UUID.randomUUID().toString().replace("-", ""));
			return dwMerchantMapper.add(dwMerchant);
		} else {
			dwMerchant.setUpdate_time(new Date());
			int up = dwMerchantMapper.up(dwMerchant);
			return up;
		}

	}


}
