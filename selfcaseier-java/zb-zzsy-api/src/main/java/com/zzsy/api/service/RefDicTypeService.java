package com.zzsy.api.service;

import com.lujie.common.service.BaseService;
import com.zzsy.api.entity.RefDicData;
import com.zzsy.api.entity.RefDicType;
import com.zzsy.api.mapper.RefDicDataMapper;
import com.zzsy.api.mapper.RefDicTypeMapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class RefDicTypeService extends BaseService {

    @Autowired
    private RefDicTypeMapper refDicTypeMapper;

    @Autowired
    private RefDicDataMapper refDicDataMapper;

    /**
     * 添加或修改
     *
     * @param refDicType
     * @return
     */
    public int addOrUp(RefDicType refDicType) {
        if (StringUtils.isEmpty(refDicType.getCode())) {
            //插入唯一code
            refDicType.setCode(UUID.randomUUID().toString().replace("-", ""));
            return refDicTypeMapper.add(refDicType);
        } else {
            refDicType.setUpdate_time(new Date());
            int up = refDicTypeMapper.up(refDicType);
            return up;
        }

    }

    public void delete(String codes) {
        Set<String> collect = Arrays.stream(codes.split(",")).collect(Collectors.toSet());
        refDicTypeMapper.deleteByIdSet(collect);
    }

    public List<RefDicType> findPageList(RefDicType refDicType) {
        return refDicTypeMapper.findPageList(refDicType);
    }

    public Long countTotal(RefDicType refDicType) {
        return refDicTypeMapper.countTotal(refDicType);
    }

    //获得一条记录
    public RefDicType findOneRefDicType(RefDicType refDicType) {
        RefDicType oneRefDicType = refDicTypeMapper.findOneRefDicType(refDicType);
        if (oneRefDicType == null) {
            return null;
        }
        if (refDicType.getNeedSubset().equals(0)) {
            return oneRefDicType;
        }
        RefDicData refDicData = new RefDicData();
        refDicData.setDicTypeId(oneRefDicType.getCode());
        List<RefDicData> listByTypeId = refDicDataMapper.findListByTypeId(refDicData);
        if (CollectionUtils.isNotEmpty(listByTypeId)) {
            if (refDicType.getNeedSubset().equals(1)) {
                oneRefDicType.setListSubset(listByTypeId);
            }
            if (refDicType.getNeedSubset().equals(2)) {
                Map<String, String> collect = listByTypeId.stream().collect(Collectors.toMap(RefDicData::getDicDateKey, RefDicData::getDicDateName));
                oneRefDicType.setMapSubset(collect);
            }
        }
        return oneRefDicType;
    }


}
