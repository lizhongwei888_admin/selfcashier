package com.zzsy.api.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.lujie.common.entity.BaseEntity;

public class Dept extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 7109194604543057049L;

	private String id;
	private String deptId;

    private String text;
    
    private String departmentAddress;

    private Integer seq;

    private String pid;

    private String leader;
    
    private String phoneNum;
    
    private String shortname;

    private Short isvalid;

    private String info;

    private Date lasttime;

    private String s_lasttime;// 同步字段lasttime

	private Date inserttime;

	private String s_inserttime;// 同步字段inserttime

    private String reservearg1;

    private String reservearg2;

    private String reservearg3;
    
    private List<Dept> sdepts;
    
    private boolean checked;
    
    private boolean open;
    
    private Short version;
    
    private Integer haveChild; 
    
    private List<Dept> depts;
    
    private Integer moduleRoleId;
    
    private Integer moduleId;

    public Short getVersion() {
        return version;
    }

    public void setVersion(Short version) {
        this.version = version;
    }

    public String getDepartmentAddress() {
        return departmentAddress;
    }

    public void setDepartmentAddress(String departmentAddress) {
        this.departmentAddress = departmentAddress;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public Integer getHaveChild() {
		return haveChild;
	}

	public void setHaveChild(Integer haveChild) {
		this.haveChild = haveChild;
	}

	public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text == null ? null : text.trim();
    }

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid == null ? null : pid.trim();
    }

    public String getLeader() {
        return leader;
    }

    public void setLeader(String leader) {
        this.leader = leader == null ? null : leader.trim();
    }

    public String getShortname() {
        return shortname;
    }

    public void setShortname(String shortname) {
        this.shortname = shortname == null ? null : shortname.trim();
    }

    public Short getIsvalid() {
        return isvalid;
    }

    public void setIsvalid(Short isvalid) {
        this.isvalid = isvalid;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info == null ? null : info.trim();
    }

    public Date getLasttime() {
        return lasttime;
    }

    public void setLasttime(Date lasttime) {
        this.lasttime = lasttime;
    }

    public Date getInserttime() {
        return inserttime;
    }

    public void setInserttime(Date inserttime) {
        this.inserttime = inserttime;
    }

    public String getReservearg1() {
        return reservearg1;
    }

    public void setReservearg1(String reservearg1) {
        this.reservearg1 = reservearg1 == null ? null : reservearg1.trim();
    }

    public String getReservearg2() {
        return reservearg2;
    }

    public void setReservearg2(String reservearg2) {
        this.reservearg2 = reservearg2 == null ? null : reservearg2.trim();
    }

    public String getReservearg3() {
        return reservearg3;
    }

    public void setReservearg3(String reservearg3) {
        this.reservearg3 = reservearg3 == null ? null : reservearg3.trim();
    }
    
	public List<Dept> getSdepts() {
		return sdepts;
	}

	public void setSdepts(List<Dept> sdepts) {
		this.sdepts = sdepts;
	}
	
	public String getS_lasttime() {
		return s_lasttime;
	}

	public void setS_lasttime(String s_lasttime) {
		this.s_lasttime = s_lasttime;
	}

	public String getS_inserttime() {
		return s_inserttime;
	}

	public void setS_inserttime(String s_inserttime) {
		this.s_inserttime = s_inserttime;
	}

	public List<Dept> getDepts() {
		return depts;
	}

	public void setDepts(List<Dept> depts) {
		this.depts = depts;
	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}

	public Integer getModuleRoleId() {
		return moduleRoleId;
	}

	public void setModuleRoleId(Integer moduleRoleId) {
		this.moduleRoleId = moduleRoleId;
	}

	public Integer getModuleId() {
		return moduleId;
	}

	public void setModuleId(Integer moduleId) {
		this.moduleId = moduleId;
	}

	public String getDeptId() {
		return deptId;
	}

	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}
	
	
}