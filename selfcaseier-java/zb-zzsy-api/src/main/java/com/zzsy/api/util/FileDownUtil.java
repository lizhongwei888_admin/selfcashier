package com.zzsy.api.util;

import com.zzsy.api.service.RefUserService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

public class FileDownUtil {
    private static final Logger log = LoggerFactory.getLogger(FileDownUtil.class);

    /**
     * 本地测试下载
     *
     * @param downUrl  文件路径
     * @param filename 输出后的文件名称
     * @param response
     */
    public static Boolean downLoadFile(String downUrl, String filename, HttpServletResponse response) {
        Boolean flag = true;
        try {
            filename = URLEncoder.encode(filename, "UTF-8");
            response.setHeader("Content-Disposition", "attachment;filename=" + filename);
            response.setContentType("application/octet-stream");
            File downFile = new File(downUrl);
            FileInputStream fileIn = new FileInputStream(downFile);
            ServletOutputStream outputStream = response.getOutputStream();

            byte[] b = new byte[1024];
            int actionLen;
            while ((actionLen = fileIn.read(b, 0, b.length)) != -1) {
                outputStream.write(b, 0, actionLen);
            }
            fileIn.close();
            outputStream.close();
        } catch (IOException e) {
            flag = false;
            log.error("downLoadFile异常", e);
        }
        return flag;
    }

    /**
     * 下载网络上的文件
     *
     * @param downUrl  文件路径
     * @param filename 输出后的文件名称
     * @param response
     */
    public static Boolean downUrlFile(String downUrl, String filename, HttpServletResponse response) {
        Boolean flag = true;
        try {
            filename = URLEncoder.encode(filename, "UTF-8");
            response.setHeader("Content-Disposition", "attachment;filename=" + filename);
            //response.setContentType("application/octet-stream");
            URL realUrl = new URL(downUrl);

            URLConnection connection = realUrl.openConnection();
            connection.setRequestProperty("accept", "*/*");
            connection.setRequestProperty("connection", "Keep-Alive");
            connection.setRequestProperty("user-agent",
                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");

            connection.connect();
            InputStream in = connection.getInputStream();

            ServletOutputStream outputStream = response.getOutputStream();

            byte[] b = new byte[1024];
            int actionLen;
            while ((actionLen = in.read(b, 0, b.length)) != -1) {
                outputStream.write(b, 0, actionLen);
            }
            in.close();
            outputStream.close();
        } catch (IOException e) {
            flag = false;
            log.error("downUrlFile异常", e);
        }
        return flag;
    }


    public static String getContentType(String fileType) {
        String contentType = "";
        if ("jpg,jpeg,png,bmp,gif,webp".contains(fileType)) {
            contentType = "image/" + fileType;
        }
        if ("mp3".contains(fileType)) {
            contentType = "audio/mpeg";
        }
        if ("mp4".contains(fileType)) {
            contentType = "video/" + fileType;
        }
        if ("wav".contains(fileType)) {
            contentType = "audio/" + fileType;
        }
        if ("csv".contains(fileType)) {
            contentType = "text/" + fileType;
        }
        if ("txt".contains(fileType)) {
            contentType = "text/plain";
        }
        if ("pdf,zip,json".contains(fileType)) {
            contentType = "application/" + fileType;
        }
        if ("doc".contains(fileType)) {
            contentType = "application/msword";
        }
        if ("docx".contains(fileType)) {
            contentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
        }
        if ("xls".contains(fileType)) {
            contentType = "application/vnd.ms-excel";
        }
        if ("xlsx".contains(fileType)) {
            contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        }
        if (StringUtils.isEmpty(contentType)) {
            //默认二进制数据流
            contentType = " application/octet-stream";
        }
        return contentType;
    }


}
