package com.zzsy.api.service;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

@Service
public class SseService {

	@Autowired
	private RedisTemplate redisTemplate;

	private static final Logger log = LoggerFactory.getLogger(SseService.class);

	private static Map<String, SseEmitter> sseEmitterMap = new ConcurrentHashMap<>();

	/**
	 * 连接sse
	 * 
	 * @param uuid
	 * @return
	 */
	public SseEmitter connect(String uuid) {

		sseEmitterMap.remove(uuid);

		SseEmitter sseEmitter = new SseEmitter(0L);

		// 连接成功需要返回数据，否则会出现待处理状态
		try {
			sseEmitter.send(SseEmitter.event().comment("welcome"));
		} catch (IOException e) {
			e.printStackTrace();
		}

		// 连接断开
		sseEmitter.onCompletion(() -> {
			log.info("SSE客户端断开链接："+uuid);
			sseEmitterMap.remove(uuid);
		});

		// 连接超时
		sseEmitter.onTimeout(() -> {
			log.info("SSE客户端链接超时："+uuid);
			sseEmitterMap.remove(uuid);
		});

		// 连接报错
		sseEmitter.onError((throwable) -> {
			log.info("SSE客户端链接失败："+uuid);
			sseEmitterMap.remove(uuid);
		});

		// 结束之后的回调触发
		sseEmitter.onCompletion(() -> System.out.println("sss推送结束！！！"));

		sseEmitterMap.put(uuid, sseEmitter);

		return sseEmitter;
	}

	/**
	 * 移除连接
	 * 
	 * @param uuid
	 */
	public void removeUid(String uuid) {
		sseEmitterMap.remove(uuid);
	}

	/**
	 * 发送消息
	 * 
	 * @param message
	 */
	public void sendMessage(String userKey, int msgKey) {

		sseEmitterMap.forEach((uuid, sseEmitter) -> {

		});
	}

}
