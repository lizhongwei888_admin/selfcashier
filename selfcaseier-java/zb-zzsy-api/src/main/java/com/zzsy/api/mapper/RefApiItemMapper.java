package com.zzsy.api.mapper;

import com.lujie.common.dao.BasicMapper;
import com.zzsy.api.entity.RefApiItem;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RefApiItemMapper extends BasicMapper {
    List<RefApiItem> findAll(@Param("p") RefApiItem refApiItem);

    List<RefApiItem> findListByApiId(@Param("p") RefApiItem refApiItem);

}
