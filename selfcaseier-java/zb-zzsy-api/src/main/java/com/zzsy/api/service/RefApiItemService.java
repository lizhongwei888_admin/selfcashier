package com.zzsy.api.service;

import com.lujie.common.service.BaseService;
import com.zzsy.api.entity.RefApiItem;
import com.zzsy.api.mapper.RefApiItemMapper;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class RefApiItemService extends BaseService {

    @Autowired
    private RefApiItemMapper refApiItemMapper;
    @Autowired
    private RedisTemplate redisTemplate;


    public List<RefApiItem> findAll(RefApiItem refApiItem) {
        return refApiItemMapper.findAll(refApiItem);
    }


    /**
     * 添加或修改
     *
     * @param refApiItem
     * @return
     */
    public int addOrUp(RefApiItem refApiItem) {
        if (StringUtils.isEmpty(refApiItem.getCode())) {
            //插入唯一code
            refApiItem.setCode(UUID.randomUUID().toString().replace("-", ""));
            return refApiItemMapper.add(refApiItem);
        } else {
            refApiItem.setUpdate_time(new Date());
            int up = refApiItemMapper.up(refApiItem);
            return up;
        }

    }

    public void delete(String codes) {
        Set<String> collect = Arrays.stream(codes.split(",")).collect(Collectors.toSet());
        refApiItemMapper.deleteByIdSet(collect);
    }

    public List<RefApiItem> findPageList(RefApiItem refApiItem) {
        return refApiItemMapper.findPageList(refApiItem);
    }

    public Long countTotal(RefApiItem refApiItem) {
        return refApiItemMapper.countTotal(refApiItem);
    }

}
