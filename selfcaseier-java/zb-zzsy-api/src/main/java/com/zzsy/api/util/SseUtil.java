package com.zzsy.api.util;

import java.util.HashMap;
import java.util.Map;

import com.zzsy.api.config.UserAuthConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.pagehelper.util.StringUtil;
import com.lujie.common.constant.HttpContentType;
import com.lujie.common.util.HttpRequest;
import com.lujie.common.util.JsonHelper;

public class SseUtil {
	private final static Logger log = LoggerFactory.getLogger(SseUtil.class);
	
	public final static String ParkRealtimes="ParkRealtimes";
	public final static String AlarmPassStationInfo="AlarmPassStationInfo";
	public final static String PassStationInfo="PassStationInfo";
	
	public static void send(final String jsonStr){
		
		Runnable runnable = new Runnable() {
            @Override
            public void run() {
            	 try {
            		String sseSendUrl= UserAuthConfig.ConfigMap.getStr("host");
        			if(StringUtil.isEmpty(sseSendUrl)){
        				log.info("没有配置項目地址host");
        				return;
        			}
        			sseSendUrl=sseSendUrl+":8990/userbase/sse/send";

        			log.info("开始向sse推送数据:sseSendUrl="+sseSendUrl+",jsonStr="+jsonStr);
        			
            		HashMap<String,String> reqHeaderMap=new HashMap<>();
            		reqHeaderMap.put("Content-Type",HttpContentType.ApplicationJSON);
            		reqHeaderMap.put("token", UserAuthConfig.Systoken);
            		
            		String result=HttpRequest.sendPost(reqHeaderMap, sseSendUrl, jsonStr);
        			
            		log.info("向sse推送数据結果:,"+result);
        		} catch (Exception e) {
        			log.info("向sse推送数据失败:"+e.getMessage());
        		}
        	}
        };
        new Thread(runnable).start();  
		
		
		
	}
	
	public static void send(Map dataMap){
		String jsonStr=JsonHelper.toJson(dataMap);
		send(jsonStr);
	}
}
