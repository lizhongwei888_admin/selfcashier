package com.zzsy.api.entity;

import java.io.Serializable;
import java.util.List;

import com.lujie.common.entity.BasicEntity;

public class Menu extends BasicEntity implements Serializable{
	private static final long serialVersionUID = -301062307193761449L;

	private boolean checked;
	
	private boolean open;
	
	private String id;

    private String pid;

    private String text;

    private Integer seq;

    private String icon;

    private String url;
    
    private Integer haveChild; 
    
    private List<Menu> smenus;
    
    private String backendUrls;
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid == null ? null : pid.trim();
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text == null ? null : text.trim();
    }

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon == null ? null : icon.trim();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url == null ? null : url.trim();
    }

	public Integer getHaveChild() {
		return haveChild;
	}

	public void setHaveChild(Integer haveChild) {
		this.haveChild = haveChild;
	}

	public List<Menu> getSmenus() {
		return smenus;
	}

	public void setSmenus(List<Menu> smenus) {
		this.smenus = smenus;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}

	public String getBackendUrls() {
		return backendUrls;
	}

	public void setBackendUrls(String backendUrls) {
		this.backendUrls = backendUrls;
	} 
	
	
}