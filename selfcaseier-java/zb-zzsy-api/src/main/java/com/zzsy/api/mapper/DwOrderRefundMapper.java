package com.zzsy.api.mapper;

import com.lujie.common.dao.BasicMapper;
import com.zzsy.api.entity.DwOrder;
import com.zzsy.api.entity.DwOrderRefund;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface DwOrderRefundMapper extends BasicMapper{
    void insertData(@Param("p") DwOrderRefund d);

    void updateState(String code,String orderState);

}