package com.zzsy.api.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;

import com.zzsy.api.constant.KeysManager;
import com.zzsy.api.entity.User;
import com.zzsy.api.service.InterfaceAccountService;
import org.springframework.data.redis.core.RedisTemplate;

public class RedisUtil {
	private static final String UnLoginUserKey= KeysManager.User_Auth+"UnLoginUserKey_";
	public static final String LoginUserToken=KeysManager.User_Auth+"Login_User_Token_";
	private static final String LoginUserInfo=KeysManager.User_Auth+"LoginUserInfo_";
	private static final Integer LoginUserSessionTime=7*24*60;
	
	private static final String SmsCode=KeysManager.User_Auth+"SmsCode_";
	public static final String VerifyCodeKey=KeysManager.User_Auth+"VerifyCodeKey_"; 
	
	
	
	public static boolean isLogin(RedisTemplate redisTemplate,HttpServletRequest request) { 
		String token=getToken(request);
        Object tokenObj = redisTemplate.opsForValue().get(LoginUserToken+token);
        if(tokenObj==null){
        	return false;
        }
        return true;
	} 
	public static boolean isLogin(RedisTemplate redisTemplate,String token){
		if(token==null||token.equals("")){
        	return false;
        }
        Object tokenObj = redisTemplate.opsForValue().get(LoginUserToken+token);
        if(tokenObj==null){
        	return false;
        }
        return true;
	}
	public static void updateLoginStatus(RedisTemplate redisTemplate,HttpServletRequest request) { 
		String token=getToken(request);
		String tokenKey=LoginUserToken+token;
		Object userObj=redisTemplate.opsForValue().get(tokenKey);
        redisTemplate.opsForValue().set(tokenKey, userObj);
        redisTemplate.expire(tokenKey,LoginUserSessionTime,TimeUnit.MINUTES); 
    }
	
	public static void main(String[] args){
		
	}
	
	public static String genarateToken(RedisTemplate redisTemplate,String userid){
		String token=userid+":"+UUID.randomUUID().toString().replace("-", "");//token生成策略
		String tokenKey=LoginUserToken+token;
        redisTemplate.opsForValue().set(tokenKey, token);
        redisTemplate.expire(tokenKey,LoginUserSessionTime,TimeUnit.MINUTES); 
    	return token;
	}
	public static String genarateTokenAndSaveUser(RedisTemplate redisTemplate, User user){
		String userid=user.getId();
		String token=userid+":"+UUID.randomUUID().toString().replace("-", "");//token生成策略
		user.setToken(token);
		String tokenKey=LoginUserToken+token;
        redisTemplate.opsForValue().set(tokenKey, user);
        redisTemplate.expire(tokenKey,LoginUserSessionTime,TimeUnit.MINUTES); 
    	return token;
	}
	public static String genarateTokenAndSaveUser(RedisTemplate redisTemplate,User user,Integer loginUserSessionTime){
		String userid=user.getId();
		String token=userid+":"+UUID.randomUUID().toString().replace("-", "");//token生成策略
		user.setToken(token);
		String tokenKey=LoginUserToken+token;
        redisTemplate.opsForValue().set(tokenKey, user);
        redisTemplate.expire(tokenKey,loginUserSessionTime,TimeUnit.MINUTES); 
    	return token;
	}
	
	/**
	 * 生成永久的token
	 * @param redisTemplate
	 * @param userid
	 * @return
	 */
	public static String genaratePermanentToken(RedisTemplate redisTemplate,String userid){
		String token=userid+":"+UUID.randomUUID().toString().replace("-", "");//token生成策略
		String tokenKey=LoginUserToken+token;
        redisTemplate.opsForValue().set(tokenKey, token);
        return token;
	}
	public static String genaratePermanentTokenAndSaveUser(RedisTemplate redisTemplate,User user){
		String userid=user.getId();
		String token=userid+":"+UUID.randomUUID().toString().replace("-", "");//token生成策略
		user.setToken(token);
		String tokenKey=LoginUserToken+token;
        redisTemplate.opsForValue().set(tokenKey, user);
        return token;
	}
	public static String savePermanentToken(RedisTemplate redisTemplate,String userid,String token){
		String tokenKey=LoginUserToken+token;
        redisTemplate.opsForValue().set(tokenKey, token);
        return token;
	}
	public static void removeToken(RedisTemplate redisTemplate,HttpServletRequest request) { 
		String token=getToken(request);
		String tokenKey=LoginUserToken+token; 
        redisTemplate.delete(tokenKey); 
	}
	public static void removeTokenByUserId(RedisTemplate redisTemplate,String userId){
		String token=userId+":";
		String tokenKey=LoginUserToken+token;
        RedisUtil.deleteStartLike(redisTemplate, tokenKey);
		
	}
	public static String setToken(RedisTemplate redisTemplate,String token){
		String tokenKey=LoginUserToken+token;
        redisTemplate.opsForValue().set(tokenKey, token);
        return token;
	}
	public static void saveUser(RedisTemplate redisTemplate,User user){
		if(user==null) return;
		
		String userinfoKey=LoginUserInfo+user.getId();
        redisTemplate.opsForValue().set(userinfoKey, user); 
        redisTemplate.expire(userinfoKey,LoginUserSessionTime,TimeUnit.MINUTES); 
    	
	}
	
	public static void saveUserByToken(RedisTemplate redisTemplate,User user){
		String tokenKey=LoginUserToken+user.getToken();
        redisTemplate.opsForValue().set(tokenKey, user);
        redisTemplate.expire(tokenKey,LoginUserSessionTime,TimeUnit.MINUTES); 
    	
	}
	
	
	public static void savePermanentUser(RedisTemplate redisTemplate,User user){
		if(user==null || user.getToken()==null) return;
		String tokenKey=LoginUserToken+user.getToken();
		redisTemplate.opsForValue().set(tokenKey, user);  
	}
	 
	
	public static void removeUser(RedisTemplate redisTemplate,String userid){
		String userinfoKey = LoginUserInfo + userid;
		redisTemplate.delete(userinfoKey);
		removeTokenByUserId(redisTemplate, userid);
	}

	/**
	 * 根据userid获取用户信息
	 *
	 * @param redisTemplate
	 * @param request
	 * @return
	 */
	public static User getUser(RedisTemplate redisTemplate, HttpServletRequest request) {
		String token = getToken(request);
		String tokenKey = LoginUserToken + token;
		return (User) redisTemplate.opsForValue().get(tokenKey);
	}
	
	public static User getUser(RedisTemplate redisTemplate,String userid){
		
		String userinfoKey=LoginUserInfo+userid;
        return (User)redisTemplate.opsForValue().get(userinfoKey);  
	}
	public static User getUserByToken(RedisTemplate redisTemplate,String token) {//throws UnloginException
		//if(token==null || token.length()==0){
			//throw new UnloginException("未登录");
		//}
		return getUser(redisTemplate,getUserid(token));
	} 
	public static User getUserByToken(RedisTemplate redisTemplate,HttpServletRequest request) { 
		String token=getToken(request);
		String tokenKey=LoginUserToken+token;
		//Object obj=redisTemplate.opsForValue().get(tokenKey);
		return (User)redisTemplate.opsForValue().get(tokenKey);  
	}
	/**
	 * 获取用户登录后的token
	 * @param redisTemplate
	 * @param request
	 * @return
	 */
	public static String getToken(HttpServletRequest request) { 
		String token=request.getHeader("token");
		if(token==null||token.isEmpty()){
			token=request.getParameter("token");
		}
		return token;
	}
	public static User getUserAuthUser(RedisTemplate redisTemplate,HttpServletRequest request) { 
		String userinfoKey=LoginUserToken+getUserId(request);
        
		Set keyset=redisTemplate.keys(userinfoKey+"*");
		Object user=null;
		if(keyset!=null && keyset.size()>0){
			Object oneUserinfoKey=keyset.iterator().next();
			user=redisTemplate.opsForValue().get(oneUserinfoKey);
		}
		return (User)user;  
	}
	
	public static List<User> getAllUser(RedisTemplate redisTemplate) { 
		String userinfoKey=LoginUserToken;
        
		Set keyset=redisTemplate.keys(userinfoKey+"*");
		List<User> allUserList=new ArrayList<>();
		if(keyset!=null && keyset.size()>0){
			for(Object obj:keyset){
				if(obj instanceof User){
					allUserList.add((User)obj);
				}
			}
		}
		return allUserList;  
	}
	
	public static String getUserId(HttpServletRequest request) { 
		return getUserid(getToken(request));
	}
	
	
	
	public static String getUserid(String token){
		return token.split(":")[0];
	}
	
	public static void saveSmsCode(RedisTemplate redisTemplate,String mobile,String smscode){
		String SmsCodeKey=SmsCode+mobile;
		redisTemplate.opsForValue().set(SmsCodeKey,smscode);  
		redisTemplate.expire(SmsCodeKey,5,TimeUnit.MINUTES); 
    	
	}
	public static String getSmsCode(RedisTemplate redisTemplate,String mobile){
		String SmsCodeKey=SmsCode+mobile; 
		Object smsCode=redisTemplate.opsForValue().get(SmsCodeKey);  
		if(smsCode!=null){
			return smsCode.toString();
		}
		return null;
	}
	public static void saveVerifyCode(RedisTemplate redisTemplate,String userKey,String verifyCode){
		String verifyCodeKey=VerifyCodeKey+userKey;
		redisTemplate.opsForValue().set(verifyCodeKey,verifyCode);  
		redisTemplate.expire(verifyCodeKey,5,TimeUnit.MINUTES); 
    }
	public static String getVerifyCode(RedisTemplate redisTemplate,String userKey){
		String verifyCodeKey=VerifyCodeKey+userKey;
		Object obj=redisTemplate.opsForValue().get(verifyCodeKey);  
		if(obj!=null) {
			return obj.toString();
		}
		return null;
    }
	
	public static void delete(RedisTemplate redisTemplate,String key){
		redisTemplate.delete(key);  
	}
	public static void deleteStartLike(RedisTemplate redisTemplate,String key){
		Set keyset=redisTemplate.keys(key+"*");
		if(keyset!=null && keyset.size()>0){
			redisTemplate.delete(keyset); 	
		}
	}
	public static void deleteEndLike(RedisTemplate redisTemplate,String key){
		Set keyset=redisTemplate.keys("*"+key);
		if(keyset!=null && keyset.size()>0){
			redisTemplate.delete(keyset); 	
		}
	}
	public static void deleteMiddleLike(RedisTemplate redisTemplate,String key){
		Set keyset=redisTemplate.keys("*"+key+"*");
		if(keyset!=null && keyset.size()>0){
			redisTemplate.delete(keyset); 	
		} 
	}
	public static void exitLoginUser(RedisTemplate redisTemplate,String userId){
		RedisUtil.deleteMiddleLike(redisTemplate, KeysManager.User_Auth+ InterfaceAccountService.InterfaceAccountModuleRoleKey);
		
		
		String tokenKey=LoginUserToken+userId; 
		deleteMiddleLike(redisTemplate,tokenKey);
	}
	public static void exitLoginUser(RedisTemplate redisTemplate){
		String tokenKey=LoginUserToken; 
		deleteMiddleLike(redisTemplate,tokenKey);
	}
	public static void deleteAll(RedisTemplate redisTemplate){
		deleteMiddleLike(redisTemplate,KeysManager.User_Auth);
	}
	
	public static void deleteByPattern(RedisTemplate redisTemplate,String pattern){
		Set keyset=redisTemplate.keys(pattern);
		if(keyset!=null && keyset.size()>0){
			redisTemplate.delete(keyset); 	
		} 
	}
	
	public static void appendValueToQueue(RedisTemplate redisTemplate,String queueName,String value){
		redisTemplate.opsForList().rightPush(queueName, value);  
	}
	/**
	 * 从队列中取出一条记录，并将该记录从队列中删除
	 * @param redisTemplate
	 * @param queueName
	 * @return
	 */
	public static Object getAndRemoveValueFromQueue(RedisTemplate redisTemplate,String queueName){
		return redisTemplate.opsForList().leftPop(queueName, 1, TimeUnit.SECONDS);//.leftPop(queueName);
	}
	
}
