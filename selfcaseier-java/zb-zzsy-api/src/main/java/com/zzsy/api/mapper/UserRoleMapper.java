package com.zzsy.api.mapper;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.lujie.common.dao.BasicMapper;
import com.zzsy.api.entity.UserRole;
@Repository
public interface UserRoleMapper extends BasicMapper{
	int deleteByPrimaryKey(Integer id);

    int insertSelective(UserRole record);

    UserRole selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UserRole record);
    
    List<UserRole> selectByRidUid(Map<String, String> map);
    
    String findRoleByUid(String uid);

    int delRoleUserByUid(String uid);

}