package com.zzsy.api.service;

import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.service.BasicService;
import com.lujie.common.util.DateUtil;
import com.zzsy.api.entity.DwOrderPaywaydetail;
import com.zzsy.api.mapper.DwOrderPaywaydetailMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

@Service
public class DwOrderPaywaydetailService extends BasicService {
	public static final Logger log = LoggerFactory.getLogger(DwOrderPaywaydetailService.class);

	@Autowired
	private RedisTemplate redisTemplate;
	@Autowired
	private DwOrderPaywaydetailMapper dwOrderPaywaydetailMapper;

	@Resource(name = "dwOrderPaywaydetailMapper")
	public void initBaseDao(DwOrderPaywaydetailMapper dwOrderPaywaydetailMapper) {
		super.setBaseMapper(dwOrderPaywaydetailMapper);
	}

	public ResultMsg insertData(HttpServletRequest request, DwOrderPaywaydetail dwOrderPaywaydetail){
		String code = UUID.randomUUID().toString();
		dwOrderPaywaydetail.setCode(code);
		dwOrderPaywaydetail.setPayTime(DateUtil.getNowTime());
		dwOrderPaywaydetail.setCreate_time(DateUtil.getNowTime());
		dwOrderPaywaydetailMapper.insertData(dwOrderPaywaydetail);

		return new ResultMsg(ResultCode.Success);
	}



}
