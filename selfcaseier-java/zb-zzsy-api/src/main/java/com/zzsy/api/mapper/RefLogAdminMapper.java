package com.zzsy.api.mapper;

import com.lujie.common.dao.BasicMapper;
import com.zzsy.api.entity.RefLogAdmin;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface RefLogAdminMapper extends BasicMapper {

    int insertData(@Param("p") RefLogAdmin refLogAdmin);

}
