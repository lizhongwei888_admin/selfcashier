package com.zzsy.api.service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import com.zzsy.api.constant.LogType;
import com.zzsy.api.constant.UserType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.service.BasicService;
import com.zzsy.api.entity.Log;
import com.zzsy.api.entity.Role;
import com.zzsy.api.entity.User;
import com.zzsy.api.entity.UserRole;
import com.zzsy.api.mapper.UserRoleMapper;
import com.zzsy.api.util.RedisUtil;

@Service
public class UserRoleService extends BasicService {
	@Autowired
	private UserRoleMapper userRoleMapper;
	@Autowired
	private LogService logService;
	@Autowired 
	private RedisTemplate redisTemplate;
	@Resource(name="userRoleMapper")
	public void initBaseDao(UserRoleMapper userRoleMapper){
		super.setBaseMapper(userRoleMapper);
	}
	
	public ResultMsg batchSaveUserRole(Role role , HttpServletRequest request){
		if(role==null || role.getUserRoleList()==null || role.getUserRoleList().size()==0){
			return  new ResultMsg("310","请求参数不能为空");
		}
		for(UserRole userRole:role.getUserRoleList()){
			if(userRole.getRid()==null || userRole.getUid()==null){
				return  new ResultMsg("320","请求参数不能为空");
			}
		}
		
		
		for(UserRole userRole:role.getUserRoleList()){
			UserRole dbUserRole=(UserRole)userRoleMapper.findOne(userRole);
			if(dbUserRole!=null){
				return  new ResultMsg("330","已经添加了用户角色:"+dbUserRole.getRoleName());
			}
		}
		
		for(UserRole userRole:role.getUserRoleList()){
			userRoleMapper.insertSelective(userRole);
		}
		User loginuser = RedisUtil.getUser(redisTemplate, request);
		//this.logService.create(loginuser, "添加用户角色");
		this.logService.create(new Log(loginuser.getId(), "添加用户角色", LogType.UserType.getDesc(), UserType.Role.getDesc(), 0, 0) );

		
		return new ResultMsg(ResultCode.Success);
		
	}
	public ResultMsg batchDeleteUserRole(Role role , HttpServletRequest request){
		if(role==null || role.getUserRoleList()==null || role.getUserRoleList().size()==0){
			return  new ResultMsg("310","请求参数不能为空");
		}
		for(UserRole userRole:role.getUserRoleList()){
			if(userRole.getId()==null){
				return  new ResultMsg("320","请求参数不能为空");
			}
		}
		for(UserRole userRole:role.getUserRoleList()){
			userRoleMapper.deleteByPrimaryKey(userRole.getId());
		}
		User loginuser = RedisUtil.getUser(redisTemplate, request);
		//this.logService.create(loginuser, "删除用户角色");
		this.logService.create(new Log(loginuser.getId(), "添加用户角色", LogType.UserType.getDesc(), UserType.Role.getDesc(), 0, 2) );

		return new ResultMsg(ResultCode.Success);
		
	}
 
}
