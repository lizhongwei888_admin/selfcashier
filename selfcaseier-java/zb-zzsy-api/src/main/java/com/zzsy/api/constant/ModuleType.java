package com.zzsy.api.constant;

/**
 * 用户类型
 * @author sl
 *
 */
public enum ModuleType  {

	City(1, "城市"),
	District(2, "行政区"),
	Street(3, "街道"),
	Community(4, "社区"),
	Siteinfo(5, "站点"),
	VehicleFleetCompany(6, "公司"),
	VehicleFleet(7, "车队"),
	Vehicle(8, "车辆"),
	Device(9, "设备"),
	DeviceMaintenance(13,"设备维护"),
	Kaoqin(10,"考勤"),
	KaoqinDept(11,"部门"),
	KaoqinPerson(12,"人员"),
	DeviceTrash(13,"设备微服务垃圾桶"),
	WorkLog(14,"工作日志"),
	ProjectGroup(15,"工作日志")
	
	
	
	;
	
	private Integer code;

	
	private String desc;

	ModuleType(Integer code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public Integer getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}
	 

}
