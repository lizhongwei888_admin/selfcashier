package com.zzsy.api.service;

import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.service.BasicService;
import com.lujie.common.util.DateUtil;
import com.lujie.common.util.JsonHelper;
import com.lujie.common.util.StringUtil;
import com.zzsy.api.entity.DwOrder;
import com.zzsy.api.entity.DwOrderPaywaydetail;
import com.zzsy.api.entity.DwOrderShopdetail;
import com.zzsy.api.entityVo.DwOrderVo;
import com.zzsy.api.mapper.DwOrderMapper;
import com.zzsy.api.mapper.DwOrderPaywaydetailMapper;
import com.zzsy.api.mapper.DwOrderShopdetailMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Service
public class DwOrderService extends BasicService {
	public static final Logger log = LoggerFactory.getLogger(DwOrderService.class);

	@Autowired
	private RedisTemplate redisTemplate;

	@Autowired
	private DwOrderMapper dwOrderMapper;

	@Autowired
	private DwOrderPaywaydetailMapper dwOrderPaywaydetailMapper;

	@Autowired
    DwOrderShopdetailMapper dwOrderShopdetailMapper;

	@Autowired
	InterFaceService interFaceService;

	@Resource(name = "dwOrderMapper")
	public void initBaseDao(DwOrderMapper dwOrderMapper) {
		super.setBaseMapper(dwOrderMapper);
	}

	public ResultMsg insertData(HttpServletRequest request, Map<String,Object> mapOrder){
		String orderCode = UUID.randomUUID().toString();
		DwOrder dwOrder = JsonHelper.toObject(JsonHelper.toJson(mapOrder.get("orderInfo")),DwOrder.class) ;
		dwOrder.setCode(orderCode);
		dwOrder.setCreate_time(DateUtil.getNowTime());
		dwOrderMapper.insertData(dwOrder);
		List<Map<String,Object>>list=(List<Map<String, Object>>) mapOrder.get("orderDetail");
		for (int i = 0; i < list.size(); i++) {
			DwOrderShopdetail dwOrderShopdetail = JsonHelper.toObject(JsonHelper.toJson(list.get(i)),DwOrderShopdetail.class) ;
			dwOrderShopdetail.setOrderId(orderCode);
			dwOrderShopdetail.setCode(UUID.randomUUID().toString());
			dwOrderShopdetail.setOrgID(dwOrder.getOrgID());
			dwOrderShopdetail.setCreate_time(DateUtil.getNowTime());
			dwOrderShopdetailMapper.insertData(dwOrderShopdetail);
		}
		return new ResultMsg(ResultCode.Success,orderCode);
	}

	public ResultMsg completeOrder(HttpServletRequest request, Map<String,Object> mapOrder){
		String saleId = "";
		String requestJsonErp = "";
		String returnJsonErp = "";
		try {
			//完成订单将订单插入redis
			DwOrder data = (DwOrder)dwOrderMapper.findOne(mapOrder);
			List<DwOrderShopdetail> dwOrderShopdetailList = dwOrderShopdetailMapper.findListByOderId(data.getCode());
			List<DwOrderPaywaydetail> dwOrderPaywaydetails = dwOrderPaywaydetailMapper.findListByOderId(data.getCode(),"1");
			DwOrderVo vo = new DwOrderVo();
			vo.setDwOrder(data);
			vo.setDwOrderShopdetails(dwOrderShopdetailList);
			vo.setDwOrderPaywaydetail(dwOrderPaywaydetails);

			Map<String,Object> map = new HashMap<>();
			map.put("phoneNumber",vo.getDwOrder().getUserPhone());
			map.put("storeCode",vo.getDwOrder().getShopCode());
			map.put("deviceCode",vo.getDwOrder().getDeviceCode());
			map.put("orgID",vo.getDwOrder().getOrgID());
			map.put("transDate", DateUtil.formatDate(vo.getDwOrder().getCreate_time(), "yyyyMMddHHmmssSSS"));
			Map<String,Object> dataMap = new HashMap<>();
			dataMap.put("saleDate",DateUtil.formatDate(vo.getDwOrder().getCreate_time(),"yyyyMMddHHmmssSSS")+"001");
			dataMap.put("orderType",1);
			String payCode = dwOrderPaywaydetails.get(0).getPayWayId();
			if(payCode.equals("3302"))
				dataMap.put("payType",2);
			if(payCode.equals("3301"))
				dataMap.put("payType",1);
			String payId = "1|"+payCode+"|"+dwOrderPaywaydetails.get(0).getPayActualMoney()+"|"+data.getOrderNo()+"|"+data.getOrderNo();
			dataMap.put("payId",payId);
			dataMap.put("orderId",data.getOrderNo());
			Map<String,Object> billMap = new HashMap<>();

			billMap.put("wareTotalOrigPrice",data.getGoodTMoney());
			billMap.put("orderPrice",data.getActualTMoney());
			billMap.put("discountAmount",data.getPromotTMoney());
			dataMap.put("bill",billMap);
			dataMap.put("wares",new ArrayList<>());

			map.put("data",dataMap);
			requestJsonErp = JsonHelper.toJson(map);
			String res = interFaceService.submitOrder(map);
			returnJsonErp = res;
			Map<String,Object> resMap= JsonHelper.toObject(res,Map.class);
			if(StringUtil.getStr(resMap.get("code")).equals("0000")){
				Map<String,Object> obj= (Map<String,Object>)resMap.get("data");
				saleId = StringUtil.getStr(obj.get("saleId"));
			}

		} catch (Exception ex){

		}

		String orderCode = mapOrder.get("code").toString();
		mapOrder.put("requestJsonErp",requestJsonErp);
		mapOrder.put("returnJsonErp",returnJsonErp);
		mapOrder.put("saleId",saleId);

		dwOrderMapper.completeOrder(orderCode,requestJsonErp,returnJsonErp,saleId);

		return new ResultMsg(ResultCode.Success,saleId);
	}

		public ResultMsg uploadOrder( DwOrder mapOrder){
			dwOrderMapper.uploadOrder(mapOrder);
			return new ResultMsg(ResultCode.Success,"");
		}

		public ResultMsg refundOrderState( Integer orderState,String orderCode){
			dwOrderMapper.refundOrderState(orderState,orderCode);
			return new ResultMsg(ResultCode.Success,"");
		}


		public ResultMsg findTop10( DwOrder mapOrder){
			List<DwOrder> dataList = dwOrderMapper.findTop10(mapOrder);

			List<DwOrderVo> list = new ArrayList<>();
			for (DwOrder dwOrder:dataList) {
				List<DwOrderShopdetail> dwOrderShopdetailList = dwOrderShopdetailMapper.findListByOderId(dwOrder.getCode());
				DwOrderVo vo = new DwOrderVo();
				vo.setDwOrder(dwOrder);
				vo.setDwOrderShopdetails(dwOrderShopdetailList);
				list.add(vo);
			}

			return new ResultMsg(ResultCode.Success,list);
		}

		public ResultMsg findLastOrder( DwOrder mapOrder){
			DwOrder data = dwOrderMapper.findLastOrder(mapOrder);
			if(data == null){
				return new ResultMsg(ResultCode.Error,"没有找到最后一笔订单");
			}
			List<DwOrderShopdetail> dwOrderShopdetailList = dwOrderShopdetailMapper.findListByOderId(data.getCode());
			DwOrderVo vo = new DwOrderVo();
			vo.setDwOrder(data);
			vo.setDwOrderShopdetails(dwOrderShopdetailList);


			return new ResultMsg(ResultCode.Success,vo);
		}






}
