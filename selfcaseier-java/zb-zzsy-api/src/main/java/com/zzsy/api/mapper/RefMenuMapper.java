package com.zzsy.api.mapper;

import com.lujie.common.dao.BasicMapper;
import com.zzsy.api.entity.RefMenu;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface RefMenuMapper extends BasicMapper {

    List<RefMenu> getListByCodes(@Param("p") Set<String> codeSet);

    List<RefMenu> findListByParams(@Param("p") RefMenu refMenu);


}
