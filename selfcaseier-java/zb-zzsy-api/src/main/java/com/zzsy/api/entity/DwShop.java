package com.zzsy.api.entity;

import com.lujie.common.entity.BasicEntity;

public class DwShop extends BasicEntity {
    private String code;
    private String  merchantId;
    private String  shopCode;
    private String  shopName;
    private String shopLogo;
    private String phone;
    private String onlineArea;
    private String onlineMph;



    private String onlineHours;
    private String shopUrls;
    private String shopDesc;
    private Integer shopType;
    private String remark;
    private String state;

    public String getOnlineHours() {
        return onlineHours;
    }

    public void setOnlineHours(String onlineHours) {
        this.onlineHours = onlineHours;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopLogo() {
        return shopLogo;
    }

    public void setShopLogo(String shopLogo) {
        this.shopLogo = shopLogo;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getOnlineArea() {
        return onlineArea;
    }

    public void setOnlineArea(String onlineArea) {
        this.onlineArea = onlineArea;
    }

    public String getOnlineMph() {
        return onlineMph;
    }

    public void setOnlineMph(String onlineMph) {
        this.onlineMph = onlineMph;
    }

    public String getShopUrls() {
        return shopUrls;
    }

    public void setShopUrls(String shopUrls) {
        this.shopUrls = shopUrls;
    }

    public String getShopDesc() {
        return shopDesc;
    }

    public void setShopDesc(String shopDesc) {
        this.shopDesc = shopDesc;
    }

    public Integer getShopType() {
        return shopType;
    }

    public void setShopType(Integer shopType) {
        this.shopType = shopType;
    }

    @Override
    public String getRemark() {
        return remark;
    }

    @Override
    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
