package com.zzsy.api.mapper;

import com.lujie.common.dao.BasicMapper;
import com.zzsy.api.entity.DwOrderRefunddetail;
import com.zzsy.api.entity.DwOrderShopdetail;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DwOrderRefundDetailMapper extends BasicMapper{
    void insertData(@Param("p") DwOrderRefunddetail d);



}