package com.zzsy.api.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.lujie.common.entity.BasicEntity;

public class DwMerchant extends BasicEntity {
    private String code;
    private String  merchantCode;
    private String  merchantName;
    private String  merchantDesc;


    private String taxpayNumber;
    private String merchantPayCode;
    private String taxpayer;
    private String cardID;
    private String phone;
    private String address;
    private String meony;
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private String openDate;
    private String files;
    private String remark;
    private String state;

    protected Boolean stateBool;

    public Boolean getStateBool() {
        if ("false".equals(this.state)) {
            this.stateBool = Boolean.FALSE;
        } else {
            this.stateBool = Boolean.TRUE;
        }
        return this.stateBool;
    }

    public void setStateBool(Boolean stateBool) {
        if ("false".equals(this.state)) {
            this.stateBool = Boolean.FALSE;
        } else {
            this.stateBool = Boolean.TRUE;
        }
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMerchantCode() {
        return merchantCode;
    }

    public void setMerchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getMerchantDesc() {
        return merchantDesc;
    }

    public void setMerchantDesc(String merchantDesc) {
        this.merchantDesc = merchantDesc;
    }

    public String getTaxpayNumber() {
        return taxpayNumber;
    }

    public void setTaxpayNumber(String taxpayNumber) {
        this.taxpayNumber = taxpayNumber;
    }

    public String getMerchantPayCode() {
        return merchantPayCode;
    }

    public void setMerchantPayCode(String merchantPayCode) {
        this.merchantPayCode = merchantPayCode;
    }

    public String getTaxpayer() {
        return taxpayer;
    }

    public void setTaxpayer(String taxpayer) {
        this.taxpayer = taxpayer;
    }

    public String getCardID() {
        return cardID;
    }

    public void setCardID(String cardID) {
        this.cardID = cardID;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMeony() {
        return meony;
    }

    public void setMeony(String meony) {
        this.meony = meony;
    }

    public String getOpenDate() {
        return openDate;
    }

    public void setOpenDate(String openDate) {
        this.openDate = openDate;
    }

    public String getFiles() {
        return files;
    }

    public void setFiles(String files) {
        this.files = files;
    }

    @Override
    public String getRemark() {
        return remark;
    }

    @Override
    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

}
