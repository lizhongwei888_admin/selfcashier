package com.zzsy.api.service;

import com.alibaba.excel.EasyExcel;
import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.service.BasicService;
import com.lujie.common.util.JsonHelper;
import com.lujie.common.util.StringUtil;
import com.zzsy.api.entity.*;
import com.zzsy.api.excel.DwShopExcel;
import com.zzsy.api.listene.DwShopExcelListene;
import com.zzsy.api.mapper.DwMerchantMapper;
import com.zzsy.api.mapper.DwShopMapper;
import com.zzsy.api.mapper.DwSyCashMapper;
import com.zzsy.api.util.RedisRefUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class DwShopService extends BasicService {
	public static final Logger log = LoggerFactory.getLogger(DwShopService.class);

	@Autowired
	private RedisTemplate redisTemplate;
	@Autowired
	private DwShopMapper dwShopMapper;

	@Autowired
	private DwMerchantMapper dwMerchantMapper;


	@Autowired
	private DwSyCashMapper dwSyCashMapper;


	@Resource(name = "dwShopMapper")
	public void initBaseDao(DwShopMapper dwShopMapper) {
		super.setBaseMapper(dwShopMapper);
	}

	public List<Map<String, Object>> findPageList(DwShop dwShop) {
		List<Map<String, Object>> data = dwShopMapper.findPageList(dwShop);
		return data;
	}
	public List<DwShop> findOneByMerchantId(String merchantId){
		List<DwShop> data = dwShopMapper.findOneByMerchantId(merchantId);
		return data;
	}


	public List<DwSyCash> getListByShopIdSet(String codes) {
		Set<String> collect = Arrays.stream(codes.split(",")).collect(Collectors.toSet());
		return dwSyCashMapper.getListByIdSet(collect);
	}


	public Long delete(String codes) {
		Set<String> collect = Arrays.stream(codes.split(",")).collect(Collectors.toSet());
		return dwShopMapper.deleteByIdSet(collect);
	}

	public  List<DwShop> getListByCodes(String codes) {
		Set<String> collect = Arrays.stream(codes.split(",")).collect(Collectors.toSet());
		return dwShopMapper.getListByCodes(collect);
	}

	public  DwShop findOneByCode(DwShop dwShop) {
		return dwShopMapper.findOneByCode(dwShop);
	}

	public  DwShop findOneByShopCode(DwShop dwShop) {
		return dwShopMapper.findOneByShopCode(dwShop);
	}


	public ResultMsg insertData(HttpServletRequest request, DwShop dwShop){
		if(StringUtil.isEmpty(dwShop.getMerchantId())){
			return new ResultMsg("302","所属商户不能为空");
		}
		if(StringUtil.isEmpty(dwShop.getShopCode())){
			return new ResultMsg("301","门店编码不能为空");
		}
		if(StringUtil.isEmpty(dwShop.getShopName())){
			return new ResultMsg("302","门店名称不能为空");
		}
		dwShopMapper.add(dwShop);
		return new ResultMsg(ResultCode.Success);
	}

	/**
	 * 添加或修改
	 *
	 * @param dwShop
	 * @return
	 */
	public int addOrUp(DwShop dwShop) {
		if (StringUtils.isEmpty(dwShop.getCode())) {
			//插入唯一code
			dwShop.setCode(UUID.randomUUID().toString().replace("-", ""));
			return dwShopMapper.add(dwShop);
		} else {
			dwShop.setUpdate_time(new Date());
			int up = dwShopMapper.up(dwShop);
			return up;
		}

	}


	public String importShop(HttpServletRequest request) {

		RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
		//获得上传excel文件
		Map<String, MultipartFile> files = ((MultipartHttpServletRequest) request).getFileMap();
		if (files.isEmpty()) {
			return JsonHelper.toJson(new ResultMsg("300", "上传为空"));
		}
		Iterator iterator = files.entrySet().iterator();
		if (iterator.hasNext()) {
			Map.Entry<String, MultipartFile> obj = (Map.Entry<String, MultipartFile>) iterator.next();
			MultipartFile file = obj.getValue();
			if (file == null) {
				return JsonHelper.toJson(new ResultMsg("300", "上传为空:" + obj.getKey()));
			}
			//开始操作excel文件
			try {
				//获得导入数据
				List<DwShopExcel> dwShopExcelList = EasyExcel.read(file.getInputStream()).head(DwShopExcel.class).sheet().doReadSync();
				if (CollectionUtils.isEmpty(dwShopExcelList)) {
					return JsonHelper.toJson(new ResultMsg("300", "导入数据为空"));
				}
				Set<String> merchantCodeSet = new HashSet<String>();
				for (int i = 0; i < dwShopExcelList.size(); i++) {
					DwShopExcel dwShopExcel = dwShopExcelList.get(i);
					if (StringUtils.isEmpty(dwShopExcel.getMerchantId())) {
						return JsonHelper.toJson(new ResultMsg("300", "行" + (i + 2) + "商户编号为空"));
					}
					merchantCodeSet.add(dwShopExcel.getMerchantId());
				}
				//获得商户集合
				List<DwMerchant> allByParam = dwMerchantMapper.getListByDwMerchantCodes(merchantCodeSet);
				if (CollectionUtils.isEmpty(allByParam)) {
					return JsonHelper.toJson(new ResultMsg("300", "无商户记录"));
				}
				Map<String, DwMerchant> dwMerchantcollect = allByParam.stream()
						.collect(Collectors.toMap(DwMerchant::getMerchantCode, dwMerchant -> dwMerchant));
				List<DwShop> dwShopList = new ArrayList<DwShop>();
				DwShop dwShop = null;
				for (DwShopExcel dwShopExcel : dwShopExcelList) {
					if (!dwMerchantcollect.containsKey(dwShopExcel.getMerchantId())) {
						continue;
					}
					dwShop = new DwShop();
					BeanUtils.copyProperties(dwShopExcel, dwShop);
					dwShop.setMerchantId(dwMerchantcollect.get(dwShopExcel.getMerchantId()).getCode());
					//插入唯一code
					dwShop.setCode(UUID.randomUUID().toString().replace("-", ""));
					dwShop.setCreateby(loginRefUser.getCode());
					dwShop.setCreate_time(new Date());
					dwShop.setUpdateby(loginRefUser.getCode());
					dwShop.setUpdate_time(new Date());
					dwShop.setOrgID(loginRefUser.getOrgID());

					dwShopList.add(dwShop);
				}
				//进行批量插入
				if (dwShopMapper.addBatch(dwShopList) <= 0) {
					return JsonHelper.toJson(new ResultMsg("300", "导入失败"));
				}
				return JsonHelper.toJson(new ResultMsg(ResultCode.Success));

			} catch (Exception e) {
				log.error("importShop异常:", e);
				return JsonHelper.toJson(new ResultMsg("300", "发生异常,导入失败"));
			}
		}
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success));
	}
}
