package com.zzsy.api.service;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import com.zzsy.api.entity.Log;
import com.zzsy.api.mapper.LogMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lujie.common.service.BaseService;
import com.lujie.common.util.StringUtil;

@Service
public class LogService  extends BaseService {
	
	@Autowired
	private LogMapper logMapper;
	@Resource(name="logMapper")
	public void initBaseDao(LogMapper logmapper){
		super.setBaseMapper(logmapper);
	} 
	/**
	 * 生成操作日志
	 */
	
	public void create(Log log) {
		try {
			InetAddress ip4 = Inet4Address.getLocalHost();
			log.setId(UUID.randomUUID().toString().replaceAll("-", ""));
			log.setIpAddress(ip4.getHostAddress());
			log.setCreateTime(new Date());
			this.logMapper.insertSelective(log);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public List<Object[]> getExportColumnList(List<Log> oList){
		List<Object[]> dataList=new ArrayList<>();
		String operType = "";
		String operStatus = "";
		Map<Integer, String> map = new HashMap<Integer, String>();
		map.put(0, "新增");
		map.put(1, "修改");
		map.put(2, "删除");
		map.put(3, "查询");
		map.put(4, "导入");
		map.put(5, "导出");
		Map<Integer, String> operStatusMap = new HashMap<Integer, String>();
		operStatusMap.put(0, "成功");
		operStatusMap.put(1, "失败");
		for(int i=1;i<=oList.size();i++){
			Log o=oList.get(i-1);
			ArrayList list=new ArrayList();
			if (null != o.getOperType()) {
				operType = map.get(o.getOperType());
			}
			if (null != o.getOperStatus()) {
				operStatus = operStatusMap.get(o.getOperStatus());
			}
			list.add(i); 
			list.add(StringUtil.getNotNullStr(o.getType()));
			list.add(StringUtil.getNotNullStr(o.getSubType()));
			list.add(StringUtil.getNotNullStr(o.getCreateUserName()));
			list.add(StringUtil.getNotNullStr(o.getMark()));
			list.add(StringUtil.getNotNullStr(operType));
			list.add(StringUtil.getNotNullStr(operStatus));
			list.add(StringUtil.getNotNullStr(o.getCreateTimeStr()));
			list.add(StringUtil.getNotNullStr(o.getIpAddress()));
			list.add(StringUtil.getNotNullStr(o.getRemark()));
			dataList.add(list.toArray());
		}
		return dataList;
	}

}
