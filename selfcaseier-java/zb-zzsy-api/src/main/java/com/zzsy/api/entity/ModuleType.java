package com.zzsy.api.entity;

import java.io.Serializable;

public class ModuleType    implements Serializable{
	private static final long serialVersionUID = -385429157197792921L;

	private Integer id;
	private String moduleCode;
	private String name;
	private String path;
	private String dataIdName;
	private Integer dataIdType;//1整型2字符串
	private String findAllName;
	private String listName;
	private String detailName;
	private String addName;
	private String updateName;
	private String deleteName;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getDataIdName() {
		return dataIdName;
	}
	public void setDataIdName(String dataIdName) {
		this.dataIdName = dataIdName;
	}
	public String getAddName() {
		return addName;
	}
	public void setAddName(String addName) {
		this.addName = addName;
	}
	public String getUpdateName() {
		return updateName;
	}
	public void setUpdateName(String updateName) {
		this.updateName = updateName;
	}
	public String getDeleteName() {
		return deleteName;
	}
	public void setDeleteName(String deleteName) {
		this.deleteName = deleteName;
	}
	public String getListName() {
		return listName;
	}
	public void setListName(String listName) {
		this.listName = listName;
	}
	public String getFindAllName() {
		return findAllName;
	}
	public void setFindAllName(String findAllName) {
		this.findAllName = findAllName;
	}
	
	public Boolean needInterceptMethod(String operateName){
		if(findAllName!=null && findAllName.equalsIgnoreCase(operateName)){
			return true;
		}
		if(listName!=null && listName.equalsIgnoreCase(operateName)){
			return true;
		}
		if(addName!=null && addName.equalsIgnoreCase(operateName)){
			return true;
		}
		if(updateName!=null && updateName.equalsIgnoreCase(operateName)){
			return true;
		}
		if(deleteName!=null && deleteName.equalsIgnoreCase(operateName)){
			return true;
		} 
		return false;
	}
	public String getDetailName() {
		return detailName;
	}
	public void setDetailName(String detailName) {
		this.detailName = detailName;
	}
	public String getModuleCode() {
		return moduleCode;
	}
	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}
	public Integer getDataIdType() {
		return dataIdType;
	}
	public void setDataIdType(Integer dataIdType) {
		this.dataIdType = dataIdType;
	}
	
    
}