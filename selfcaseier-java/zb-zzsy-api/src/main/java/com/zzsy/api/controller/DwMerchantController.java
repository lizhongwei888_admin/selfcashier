package com.zzsy.api.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.util.JsonHelper;
import com.zzsy.api.entity.*;
import com.zzsy.api.service.DwMerchantService;
import com.zzsy.api.service.DwShopService;
import com.zzsy.api.service.RefLogAdminService;
import com.zzsy.api.util.RedisRefUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping(value = "/dwMerchant")
public class DwMerchantController {

	@Autowired
	private DwMerchantService dwMerchantService;

	@Autowired
	private DwShopService dwShopService;

	@Autowired
	private RefLogAdminService refLogAdminService;

	@Autowired
	private RedisTemplate redisTemplate;

	@RequestMapping(value={"/getInfoByMerchantCode"})
	@ResponseBody
	public String list(@RequestBody DwMerchant dwMerchant){
		DwMerchant dw =dwMerchantService.getInfoByMerchantCode(dwMerchant.getMerchantCode());
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success,dw));
	}

	@RequestMapping(value={"/findAll"})
	@ResponseBody
	public String findAll(@RequestBody DwMerchant dwMerchant){
		List<DwMerchant> dw =dwMerchantService.findAll(dwMerchant);
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success,dw));
	}

	/**
	 * 后端分页查询集合
	 *
	 * @param dwMerchant
	 * @return
	 */
	@RequestMapping(value = "/listMerchant", method = RequestMethod.POST)
	@ResponseBody
	public String listRefApi(@RequestBody DwMerchant dwMerchant) {

		return JsonHelper.toJson(new ResultMsg(ResultCode.Success, dwMerchantService.findPageList(dwMerchant), dwMerchantService.countTotal(dwMerchant)));
	}

	/**
	 * 新增
	 *
	 * @param dwMerchant
	 * @return
	 */
	@RequestMapping(value = "/addMerchant", method = RequestMethod.POST)
	@ResponseBody
	public String addMerchant(HttpServletRequest request, @RequestBody DwMerchant dwMerchant) {

		RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
		dwMerchant.setCreateby(loginRefUser.getCode());
		dwMerchant.setCreate_time(new Date());
		dwMerchant.setOrgID(loginRefUser.getOrgID());

		DwMerchant dw =dwMerchantService.getInfoByMerchantCode(dwMerchant.getMerchantCode());
		if(dw!=null){
			return JsonHelper.toJson(new ResultMsg("302","商户编码重复"));
		}
		dwMerchantService.addOrUp(dwMerchant);
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success));
	}

	/**
	 * 修改
	 *
	 * @param dwMerchant
	 * @return
	 */
	@RequestMapping(value = "/updateMerchant", method = RequestMethod.POST)
	@ResponseBody
	public String updateMerchant(HttpServletRequest request,@RequestBody DwMerchant dwMerchant) {
		RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
		dwMerchant.setUpdateby(loginRefUser.getCode());
		dwMerchant.setUpdate_time(new Date());


		DwMerchant dw =dwMerchantService.getInfoByMerchantCode(dwMerchant.getMerchantCode());
		if(dw!=null && !dw.getCode().equals(dwMerchant.getCode())){
			return JsonHelper.toJson(new ResultMsg("302","商户编码重复"));
		}

		dwMerchantService.addOrUp(dwMerchant);
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success));
	}

	/**
	 * 删除
	 *
	 * @param object
	 * @return
	 */
	@RequestMapping(value = "/delMerchant", method = RequestMethod.POST)
	@ResponseBody
	public String delMerchant(HttpServletRequest request, @RequestBody JSONObject object) {

		String codes = object.getString("codes");
		if (StringUtils.isEmpty(codes)) {
			return JsonHelper.toJson(new ResultMsg("300", "code不能为空"));
		}
		RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
		//判断角色是否绑定了用户
		List<DwShop> shops = dwShopService.getListByCodes(codes);
		if (shops.size()>0) {
			return JsonHelper.toJson(new ResultMsg("300", "商户已经被绑定的不可以删除"));
		}
		if (dwMerchantService.deleteByCode(codes) <= 0) {
			return JsonHelper.toJson(new ResultMsg("300", "删除失败"));
		}

		this.refLogAdminService.create(new RefLogAdmin(loginRefUser.getCode()
				, "商户管理", "删除商户", "delMerchant", "/dwMerchant/delMerchant"
				, codes, "ok", loginRefUser.getOrgID()));
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success));
	}



	@RequestMapping(value = "/updateState", method = RequestMethod.POST)
	@ResponseBody
	public String updateState(@RequestBody DwMerchant dwMerchant, HttpServletRequest request) {
		String code = dwMerchant.getCode();
		if (StringUtils.isEmpty(code)) {
			return JsonHelper.toJson(new ResultMsg("300", "code不能为空"));
		}
		RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
		dwMerchant = (DwMerchant) dwMerchantService.findOne(dwMerchant);
		if ("false".equals(dwMerchant.getState())) {
			dwMerchant.setState("true");
		} else {
			dwMerchant.setState("false");
		}
		dwMerchant.setUpdateby(loginRefUser.getCode());
		if (dwMerchantService.addOrUp(dwMerchant) <= 0) {
			return JsonHelper.toJson(new ResultMsg("300", "修改状态失败"));
		}
		this.refLogAdminService.create(new RefLogAdmin(loginRefUser.getCode()
				, "商户管理页面", "修改状态", "state", "/dwMerchant/updateState"
				, dwMerchant, "ok", loginRefUser.getOrgID()));
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success));
	}


}
