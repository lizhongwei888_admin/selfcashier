package com.zzsy.api.mapper;

import com.lujie.common.dao.BasicMapper;
import com.zzsy.api.entity.DwShop;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Repository
public interface DwShopMapper extends BasicMapper{

    List<Map<String,Object>> findPageList(@Param("p") DwShop d);
    void insertData(@Param("p") DwShop d);

    DwShop findOneByCode(@Param("p") DwShop d);

    DwShop findOneByShopCode(@Param("p") DwShop d);


    List<DwShop> findOneByMerchantId(String merchantId);

    List<DwShop> getListByCodes(@Param("p") Set<String> codes);

}