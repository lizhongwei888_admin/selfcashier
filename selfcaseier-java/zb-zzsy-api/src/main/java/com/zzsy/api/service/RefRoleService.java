package com.zzsy.api.service;

import com.alibaba.fastjson.JSONArray;
import com.lujie.common.service.BaseService;
import com.zzsy.api.entity.RefMenu;
import com.zzsy.api.entity.RefRole;
import com.zzsy.api.entity.RefUser;
import com.zzsy.api.entity.TreeZH;
import com.zzsy.api.mapper.RefRoleMapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class RefRoleService extends BaseService {

    @Autowired
    private RefRoleMapper refRoleMapper;
    @Autowired
    private RefMenuService refMenuService;


    /**
     * 添加或修改
     *
     * @param refRole
     * @return
     */
    public int addOrUp(RefRole refRole) {
        if (StringUtils.isEmpty(refRole.getCode())) {
            //插入唯一code
            refRole.setCode(UUID.randomUUID().toString().replace("-", ""));
            return refRoleMapper.add(refRole);
        } else {
            refRole.setUpdate_time(new Date());
            int up = refRoleMapper.up(refRole);
            return up;
        }

    }

    public Long delete(String codes) {
        Set<String> collect = Arrays.stream(codes.split(",")).collect(Collectors.toSet());
        return refRoleMapper.deleteByIdSet(collect);
    }

    public List<RefRole> findPageList(RefRole refRole) {
        return refRoleMapper.findPageList(refRole);
    }

    public Long countTotal(RefRole refRole) {
        return refRoleMapper.countTotal(refRole);
    }

    //通过角色code获得角色详情
    public RefRole getOneByCode(String code) {
        RefRole refRole = new RefRole();
        refRole.setCode(code);
        RefRole oneRefRole = (RefRole) refRoleMapper.findOneByParams(refRole);
        if (oneRefRole == null) {
            return null;
        }
        //判断是否是超级用户对应的roleCode(eab674bac31411ee864b00163e36bf77)
        if ("eab674bac31411ee864b00163e36bf77".equals(oneRefRole.getCode())) {
            RefMenu refMenuParm = new RefMenu();
            refMenuParm.setState("true");
            List<RefMenu> listByParams = refMenuService.findListByParams(refMenuParm);

            if (CollectionUtils.isNotEmpty(listByParams)) {
                List<TreeZH> list = refMenuService.menuToTreeZhList(listByParams);
                if (CollectionUtils.isNotEmpty(list)) {
                    oneRefRole.setTreeMenus(list);
                }
            }
            return oneRefRole;
        }
        //oneRefRole.getMenus() 逗号分隔的menuCode
        if (StringUtils.isEmpty(oneRefRole.getMenus())) {
            return oneRefRole;
        }
        //List<RefMenu> refMenus = JSONArray.parseArray(oneRefRole.getMenus(), RefMenu.class);
        List<RefMenu> refMenus = refMenuService.getListByCodes(oneRefRole.getMenus());
        if (CollectionUtils.isNotEmpty(refMenus)) {
            List<TreeZH> list = refMenuService.menuToTreeZhList(refMenus);
            if (CollectionUtils.isNotEmpty(list)) {
                oneRefRole.setTreeMenus(list);
            }
        }
        return oneRefRole;
    }

    public RefRole getOneByParms(RefRole refRole) {
        return (RefRole) refRoleMapper.findOneByParams(refRole);
    }
}
