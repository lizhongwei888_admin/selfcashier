package com.zzsy.api.constant;

/**
 * 用户类型
 * @author sl
 *
 */
public enum LoginType  {

	AccountLoginUser(1, "账号登录类型的用户"),
	InterfaceUser(2, "接口调用类型的用户"),
	//NoPicCheckLoginUser(3, "账号密码免图片验证码登录类型的用户"),
	;
	
	private Integer code;

	
	private String desc;

	LoginType(Integer code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public Integer getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}
	 

}
