package com.zzsy.api.mapper;

import com.lujie.common.dao.BasicMapper;
import com.zzsy.api.entity.DwSyBags;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DwSyBagsMapper extends BasicMapper{

   List<DwSyBags> findByMerchantId(@Param("p") DwSyBags d);

    void insertData(@Param("p") DwSyBags d);


}