package com.zzsy.api;

import org.mybatis.spring.boot.autoconfigure.MybatisAutoConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;

@SpringBootApplication
//配置需要扫描的类包，模糊的类包使用包*,例如：com.lujie.mmftransferstation*
@ComponentScan(basePackages= {"com.zzsy.api*"})
@EnableAutoConfiguration(exclude = {
		DataSourceAutoConfiguration.class,
		DataSourceTransactionManagerAutoConfiguration.class, 
		MybatisAutoConfiguration.class})
public class UserBaseApplication {
    
	private final static Logger log = LoggerFactory.getLogger(UserBaseApplication.class);
	
	public static void main(String[] args) {
        ConfigurableApplicationContext context=SpringApplication.run(UserBaseApplication.class, args);
        
        Environment environment=context.getEnvironment();
    	log.info("userbase项目端口号:"+environment.getProperty("server.port"));
    	log.info("userbase项目地址rootUrl:"+environment.getProperty("server.servlet.context-path"));
    	
    }
}