package com.zzsy.api.service;

import com.lujie.common.service.BaseService;
import com.zzsy.api.entity.RefLogAdmin;
import com.zzsy.api.entity.RefLogInterface;
import com.zzsy.api.entity.RefMenu;
import com.zzsy.api.mapper.RefLogAdminMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.util.List;
import java.util.UUID;

@Service
public class RefLogAdminService extends BaseService {

    private static final Logger log = LoggerFactory.getLogger(RefLogAdminService.class);

    @Autowired
    private RefLogAdminMapper refLogAdminMapper;


    //插入后端系统日志
    public void create(RefLogAdmin refLogAdmin) {
        try {
            refLogAdmin.setCode(UUID.randomUUID().toString().replaceAll("-", ""));
            InetAddress ip4 = Inet4Address.getLocalHost();
            refLogAdmin.setIp(ip4.getHostAddress());
            this.refLogAdminMapper.insertData(refLogAdmin);
        } catch (Exception e) {
            log.error("RefLogAdminService-create异常", e);
        }
    }

    public List<RefLogAdmin> findPageList(RefLogAdmin refLogAdmin) {
        return refLogAdminMapper.findPageList(refLogAdmin);
    }

    public Long countTotal(RefLogAdmin refLogAdmin) {
        return refLogAdminMapper.countTotal(refLogAdmin);
    }


    public List<RefLogAdmin> findList(RefLogAdmin refLogAdmin) {
        return refLogAdminMapper.findList(refLogAdmin);
    }

}
