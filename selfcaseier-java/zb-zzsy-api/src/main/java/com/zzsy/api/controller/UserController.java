package com.zzsy.api.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.util.JsonHelper;
import com.lujie.common.util.MD5Util;
import com.lujie.common.util.StringUtil;
import com.zzsy.api.constant.KeysManager;
import com.zzsy.api.constant.LogType;
import com.zzsy.api.constant.LoginType;
import com.zzsy.api.constant.UserType;
import com.zzsy.api.entity.Log;
import com.zzsy.api.entity.User;
import com.zzsy.api.service.LogService;
import com.zzsy.api.service.UserService;
import com.zzsy.api.util.RedisUtil;


/**
 * @author zzy
 */
@RestController
@RequestMapping("/user")
public class UserController {
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

	@Autowired 
	private RedisTemplate redisTemplate;
	
	@Autowired
	private UserService userService;
	@Autowired
	private LogService logservice;
	
	
	
	/**
	 * 保存用户
	 * @param request
	 * @param user
	 * @return
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ResponseBody
	public String add(HttpServletRequest request ,  @RequestBody User user) {
		User loginuser = RedisUtil.getUser(redisTemplate, request);

		if(StringUtil.isEmpty(user.getUsername())){
			return JsonHelper.toJson(new ResultMsg("310","用户名不能为空！"));
		}
		if(user.getUsername().length()<5 || user.getUsername().length()>32){
			return JsonHelper.toJson(new ResultMsg("310","用户名长度范围5-32字符"));
		}
		if(StringUtil.isEmpty(user.getRealname())){
			return JsonHelper.toJson(new ResultMsg("320","姓名不能为空！"));
		}
		if (StringUtil.isEmpty(user.getPassword())){
			return JsonHelper.toJson(new ResultMsg("330","密码不能为空！"));
		}
		if (user.getPassword().length()<8 || user.getPassword().length()>16){
			return JsonHelper.toJson(new ResultMsg("340","密码长度8-16范围内！"));
		}
		user.setPassword(MD5Util.encrypt(user.getPassword()));
		
		User u1 = userService.findByUserName(user.getUsername());
		if (u1!=null){
			return JsonHelper.toJson(new ResultMsg("350","账号已存在！"));
		}
		
		user.setLasttime(new Date());
		user.setVersion((short)1);
		user.setIsValid(1);
		user.setCreateUserId(loginuser.getId());
		userService.add(user);
		
		if(user.getLoginType()!=null && user.getLoginType().equals(LoginType.InterfaceUser.getCode())){
			String token=RedisUtil.genaratePermanentTokenAndSaveUser(redisTemplate, user);
			User tokenUser=new User();
			tokenUser.setToken(token);
			tokenUser.setId(user.getId());
			userService.update(tokenUser);
		}
		
		
		//this.logservice.create(loginuser, "新增用户【"+user.getRealname()+"】");
		this.logservice.create(new Log(loginuser.getId(), "新增用户【"+user.getRealname()+"】", LogType.UserType.getDesc(), UserType.Account.getDesc(), 0, 0) );

		return JsonHelper.toJson(new ResultMsg(ResultCode.Success,user));
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public String update(HttpServletRequest request ,  @RequestBody User user) {
		User loginuser = RedisUtil.getUser(redisTemplate, request);

		if(StringUtil.isEmpty(user.getId())){
			return JsonHelper.toJson(new ResultMsg("310","用户id不能为空！"));
		}
		User dbUser = userService.findByUserId(user.getId());
		
		if(!StringUtil.isEmpty(user.getUsername())){
			if(user.getUsername().length()<5 || user.getUsername().length()>32){
				return JsonHelper.toJson(new ResultMsg("311","用户名长度5-32！"));
			}
			if(!dbUser.getUsername().equals(user.getUsername())){
				User dbUser2 = userService.findByUserName(user.getUsername());
				if(dbUser2!=null){
					return JsonHelper.toJson(new ResultMsg("312","已经存在这个用户名"));
				}
			}
		}
		if (!StringUtil.isEmpty(user.getPassword())){
			if (user.getPassword().length()<8 || user.getPassword().length()>16){
				return JsonHelper.toJson(new ResultMsg("340","密码长度8-16范围内！"));
			}
			user.setPassword(MD5Util.encrypt(user.getPassword()));
		}
		
		User u1 = userService.findByUserId(user.getId());
		if (u1==null){
			return JsonHelper.toJson(new ResultMsg("350","不存在这个账号！"));
		}
		
		
		user.setLasttime(new Date());
		user.setVersion((short)(u1.getVersion()+1));
		userService.update(user);
		
		if(user.getLoginType()!=null && user.getLoginType().equals(LoginType.InterfaceUser.getCode())){
			user = userService.findByUserId(user.getId());
			RedisUtil.savePermanentUser(redisTemplate, user);
		}
		
		//this.logservice.create(loginuser, "修改用户【"+user.getRealname()+"】");
		this.logservice.create(new Log(loginuser.getId(), "修改用户【"+user.getRealname()+"】", LogType.UserType.getDesc(), UserType.Account.getDesc(), 1, 0) );

		return JsonHelper.toJson(new ResultMsg(ResultCode.Success));
	}
	@RequestMapping(value = "/detail", method = RequestMethod.POST)
	@ResponseBody
	public String detail(HttpServletRequest request ,  @RequestBody User user) {
		User loginuser = RedisUtil.getUser(redisTemplate, request);

		if(StringUtil.isEmpty(user.getId())){
			return JsonHelper.toJson(new ResultMsg("310","用户id不能为空！"));
		}
		
		User dbUser = userService.findByUserId(user.getId());
		if (dbUser==null){
			return JsonHelper.toJson(new ResultMsg("320","不存在这个账号！"));
		}
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success,dbUser));
	}
	
	/**
	 * 删除用户
	 * @param response
	 * @param id
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public String delete(@RequestBody User waitDeleteUser , HttpServletRequest request) {
		User temp =(User) userService.findOne(waitDeleteUser);
		if(temp==null){
			return JsonHelper.toJson(new ResultMsg("310","系统不存在这个用户"));
		}
		if (null != temp.getUsername() && temp.getUsername().equalsIgnoreCase(KeysManager.Admin)){
			return JsonHelper.toJson(new ResultMsg("320","不能删除管理员用户！"));
		}
		User loginuser = RedisUtil.getUser(redisTemplate, request);
		if(loginuser.getId().equals(waitDeleteUser.getId())){
			return JsonHelper.toJson(new ResultMsg("330","不能删除自己！"));
		}
		
		this.userService.deleteUser(waitDeleteUser);
		//this.logservice.create(loginuser, "删除用户【"+temp.getRealname()+"】");
		this.logservice.create(new Log(loginuser.getId(), "删除用户【"+temp.getRealname()+"】", LogType.UserType.getDesc(), UserType.Account.getDesc(), 2, 0) );

		RedisUtil.exitLoginUser(redisTemplate, temp.getId());
		
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success));
	}
	
	/**
	 * 登录的管理员修改某个用户的密码
	 * @return
	 * @throws UnloginException 
	 */
	@RequestMapping(value = "/changeUserPwd", method = RequestMethod.POST)
	@ResponseBody
	public String changpwd(@RequestBody User waitUpdateUser , HttpServletRequest request , HttpServletResponse response)  {
		User  loginuser= RedisUtil.getUser(redisTemplate, request);

		if(StringUtil.isEmpty(waitUpdateUser.getId())){
			return JsonHelper.toJson(new ResultMsg("310","用户id不能为空"));
		}
		if(StringUtil.isEmpty(waitUpdateUser.getPassword())){
			return JsonHelper.toJson(new ResultMsg("320","密码不能为空"));
		}
		if(waitUpdateUser.getPassword().length()<8 || waitUpdateUser.getPassword().length()>16){
			return JsonHelper.toJson(new ResultMsg("302","密码长度范围8-16"));
		}
		waitUpdateUser.setPassword(MD5Util.encrypt(waitUpdateUser.getPassword()));
		this.userService.updatePwd(waitUpdateUser);
		
		User dbUser = (User)this.userService.findOne(waitUpdateUser);
		//logservice.create(loginuser, "修改"+dbUser.getRealname()+"的密码");
		this.logservice.create(new Log(loginuser.getId(), "修改"+dbUser.getRealname()+"的密码", LogType.UserType.getDesc(), UserType.Account.getDesc(), 1, 0) );

		RedisUtil.exitLoginUser(redisTemplate, dbUser.getId());
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success));
		
	}
	
	
	/**
	 * 
	 * @param deptid
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/findAll")
	@ResponseBody
	public String findList(HttpServletRequest request,@RequestBody User user){
		User loginUser = RedisUtil.getUser(redisTemplate, request);
		if(!KeysManager.isAdmin(loginUser.getUsername())){
			user.setDataId(loginUser.getId());
		}	
		List<User> userList=userService.findList(user);
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success,userList));	
	}
	@RequestMapping(value = "/list")
	@ResponseBody
	public String list(HttpServletRequest request,@RequestBody User user){
		User loginUser = RedisUtil.getUser(redisTemplate, request);
		if(!KeysManager.isAdmin(loginUser.getUsername())){
			user.setDataId(loginUser.getId());
		}
		List<User> userList=userService.findPageList(user);
		Long totalCount=userService.countTotal(user);
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success,userList,totalCount));	
	}
	/*
	 * 获取用户部门，根据用户id
	 * */
	@RequestMapping(value = "/getUserListByDeptId")
	@ResponseBody
	public String getUserListByDeptId(@RequestBody User user){
		if(StringUtil.isEmpty(user.getDeptId())){
			return JsonHelper.toJson(new ResultMsg("310","部门id不能为空"));	
		}
		List<User> userList=userService.findListByDept(user);
		Integer totalCount=userService.findListCountByDept(user);
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success,userList,Long.parseLong(totalCount.toString())));	
	}
	
	
}
