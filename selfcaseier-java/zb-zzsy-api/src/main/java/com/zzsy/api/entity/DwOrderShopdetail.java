package com.zzsy.api.entity;

import com.lujie.common.entity.BasicEntity;

import java.io.Serializable;

/**
 * 订单管理-订单详情(DwOrderShopdetail)实体类
 *
 * @author makejava
 * @since 2024-02-05 18:26:12
 */
public class DwOrderShopdetail extends BasicEntity implements Serializable {
    private static final long serialVersionUID = -37627820674199525L;
    /**
     * 唯一标识
     */
    private String code;
    /**
     * 所属订单表ID
     */
    private String orderId;
    /**
     * 商品ID
     */
    private String goodId;
    /**
     * 商品编号
     */
    private String goodCode;
    /**
     * 商品名称
     */
    private String goodName;
    /**
     * 商品规格
     */
    private String goodSpecs;
    /**
     * 商品原单价金额
     */
    private Double goodMoney;
    /**
     * 商品促销价
     */
    private Double promotMoney;
    /**
     * 商品数量
     */
    private Double goodNum;
    /**
     * 实付商品金额=（商品原单价金额-商品促销价）*商品数量
     */
    private Double actualMoney;
    /**
     * 商品状态：1正常；2全部退款；3部分退款
     */
    private Integer goodState;



    /**
     *  商品图片
     */
    private String goodImage;

    /**
     * 退款数量
     */
    private Double refundNum;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getGoodId() {
        return goodId;
    }

    public void setGoodId(String goodId) {
        this.goodId = goodId;
    }

    public String getGoodCode() {
        return goodCode;
    }

    public void setGoodCode(String goodCode) {
        this.goodCode = goodCode;
    }

    public String getGoodName() {
        return goodName;
    }

    public void setGoodName(String goodName) {
        this.goodName = goodName;
    }

    public String getGoodSpecs() {
        return goodSpecs;
    }

    public void setGoodSpecs(String goodSpecs) {
        this.goodSpecs = goodSpecs;
    }

    public Double getGoodMoney() {
        return goodMoney;
    }

    public void setGoodMoney(Double goodMoney) {
        this.goodMoney = goodMoney;
    }

    public Double getPromotMoney() {
        return promotMoney;
    }

    public void setPromotMoney(Double promotMoney) {
        this.promotMoney = promotMoney;
    }

    public Double getGoodNum() {
        return goodNum;
    }

    public void setGoodNum(Double goodNum) {
        this.goodNum = goodNum;
    }

    public Double getActualMoney() {
        return actualMoney;
    }

    public void setActualMoney(Double actualMoney) {
        this.actualMoney = actualMoney;
    }

    public Integer getGoodState() {
        return goodState;
    }

    public void setGoodState(Integer goodState) {
        this.goodState = goodState;
    }

    public Double getRefundNum() {
        return refundNum;
    }

    public void setRefundNum(Double refundNum) {
        this.refundNum = refundNum;
    }

    public String getGoodImage() {
        return goodImage;
    }

    public void setGoodImage(String goodImage) {
        this.goodImage = goodImage;
    }
}

