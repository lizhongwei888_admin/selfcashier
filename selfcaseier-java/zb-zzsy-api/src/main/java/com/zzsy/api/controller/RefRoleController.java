package com.zzsy.api.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.util.JsonHelper;
import com.zzsy.api.entity.RefLogAdmin;
import com.zzsy.api.entity.RefOrganize;
import com.zzsy.api.entity.RefRole;
import com.zzsy.api.entity.RefUser;
import com.zzsy.api.service.RefLogAdminService;
import com.zzsy.api.service.RefRoleService;
import com.zzsy.api.service.RefUserService;
import com.zzsy.api.util.RedisRefUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * 系统-角色控制器
 */
@Controller
@RequestMapping(value = "/refRole")
public class RefRoleController {

    @Autowired
    private RefRoleService refRoleService;
    @Autowired
    private RefLogAdminService refLogAdminService;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private RefUserService refUserService;

    @RequestMapping(value = "/addRefRole", method = RequestMethod.POST)
    @ResponseBody
    public String addRefRole(HttpServletRequest request, @RequestBody RefRole refRole) {
        RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
        refRole.setCreateby(loginRefUser.getCode());
        refRole.setCreate_time(new Date());
        refRole.setUpdateby(loginRefUser.getCode());
        refRole.setUpdate_time(new Date());
        if (refRoleService.addOrUp(refRole) <= 0) {
            return JsonHelper.toJson(new ResultMsg("300", "添加失败"));
        }

        this.refLogAdminService.create(new RefLogAdmin(loginRefUser.getCode()
                , "角色页面", "添加角色", "addRefRole", "/refRole/addRefRole"
                , refRole, "ok", loginRefUser.getOrgID()));

        return JsonHelper.toJson(new ResultMsg(ResultCode.Success, refRole));

    }

    @RequestMapping(value = "/upRefRole", method = RequestMethod.POST)
    @ResponseBody
    public String upRefRole(HttpServletRequest request, @RequestBody RefRole refRole) {
        RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
        refRole.setUpdateby(loginRefUser.getCode());
        refRole.setUpdate_time(new Date());
        if (refRoleService.addOrUp(refRole) <= 0) {
            return JsonHelper.toJson(new ResultMsg("300", "修改失败"));
        }
        this.refLogAdminService.create(new RefLogAdmin(loginRefUser.getCode()
                , "角色页面", "修改角色信息", "upRefRole", "/refRole/upRefRole"
                , refRole, "ok", loginRefUser.getOrgID()));
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success, refRole));

    }

    /**
     * 删除
     *
     * @param object
     * @return
     */
    @RequestMapping(value = "/delRefRole", method = RequestMethod.POST)
    @ResponseBody
    public String delRefRole(HttpServletRequest request, @RequestBody JSONObject object) {
        String codes = object.getString("codes");
        if (StringUtils.isEmpty(codes)) {
            return JsonHelper.toJson(new ResultMsg("300", "code不能为空"));
        }
        RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
        //判断角色是否绑定了用户
        RefUser oneByRoleCodes = refUserService.getOneByRoleCodes(codes);
        if (oneByRoleCodes != null) {
            return JsonHelper.toJson(new ResultMsg("300", "用户已经绑定的不可以删除"));
        }
        if (refRoleService.delete(codes) <= 0) {
            return JsonHelper.toJson(new ResultMsg("300", "删除失败"));
        }
        this.refLogAdminService.create(new RefLogAdmin(loginRefUser.getCode()
                , "角色页面", "删除角色", "delRefRole", "/refRole/delRefRole"
                , codes, "ok", loginRefUser.getOrgID()));
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success));
    }

    /**
     * 后端分页查询集合
     *
     * @param object
     * @returnrefRole
     */
    @RequestMapping(value = "/listRefRole", method = RequestMethod.POST)
    @ResponseBody
    public String listRefRole(@RequestBody JSONObject object) {
        RefRole refRole = JSONObject.parseObject(JSON.toJSONString(object.getJSONObject("params")), RefRole.class);
        if (object.getInteger("offset") != null && object.getInteger("limit") != null) {
            refRole.setPageNum(object.getInteger("offset"));
            refRole.setPageSize(object.getInteger("limit"));
        }
        JSONObject sort = object.getJSONObject("sort");
        if (sort != null) {
            String prop = sort.getString("prop");
            String order = sort.getString("order");
            refRole.setProp(prop);
            refRole.setOrder(order);
        }
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success, refRoleService.findPageList(refRole), refRoleService.countTotal(refRole)));
    }

    @RequestMapping(value = "/state", method = RequestMethod.POST)
    @ResponseBody
    public String state(@RequestBody JSONObject object, HttpServletRequest request) {
        String code = object.getString("code");
        if (StringUtils.isEmpty(code)) {
            return JsonHelper.toJson(new ResultMsg("300", "code不能为空"));
        }
        RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
        RefRole parmRefRole = new RefRole();
        parmRefRole.setCode(code);
        RefRole oneByParms = refRoleService.getOneByParms(parmRefRole);
        if ("false".equals(oneByParms.getState())) {
            parmRefRole.setState("true");
        } else {
            parmRefRole.setState("false");
        }
        parmRefRole.setUpdateby(loginRefUser.getCode());
        if (refRoleService.addOrUp(parmRefRole) <= 0) {
            return JsonHelper.toJson(new ResultMsg("300", "修改状态失败"));
        }
        this.refLogAdminService.create(new RefLogAdmin(loginRefUser.getCode()
                , "角色管理页面", "修改状态", "state", "/refRole/state"
                , parmRefRole, "ok", loginRefUser.getOrgID()));
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success));
    }

}
