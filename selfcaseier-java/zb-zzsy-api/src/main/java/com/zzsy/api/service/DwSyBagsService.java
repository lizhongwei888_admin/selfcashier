package com.zzsy.api.service;

import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.service.BasicService;
import com.lujie.common.util.StringUtil;
import com.zzsy.api.entity.DwSyBags;
import com.zzsy.api.mapper.DwSyBagsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@Service
public class DwSyBagsService extends BasicService {
	public static final Logger log = LoggerFactory.getLogger(DwSyBagsService.class);

	@Autowired
	private RedisTemplate redisTemplate;
	@Autowired
	private DwSyBagsMapper dwSyBagsMapper;
	@Resource(name = "dwSyBagsMapper")
	public void initBaseDao(DwSyBagsMapper dwSyBagsMapper) {
		super.setBaseMapper(dwSyBagsMapper);
	}

	public List<DwSyBags> findPageList(DwSyBags dwSyBags){
		List<DwSyBags> data = dwSyBagsMapper.findPageList(dwSyBags);
		return data;
	}

	public List<DwSyBags> findByMerchantId(DwSyBags dwSyBags){
		List<DwSyBags> data = (List<DwSyBags>)redisTemplate.opsForValue().get("dw_bags_"+dwSyBags.getMerchantID());

		if(data==null) {
			data = dwSyBagsMapper.findByMerchantId(dwSyBags);
			redisTemplate.opsForValue().set("dw_bags_"+dwSyBags.getMerchantID(), data);
		}

		return data;
	}

	public ResultMsg insertData(HttpServletRequest request, DwSyBags dwSyBags){
		if(StringUtil.isEmpty(dwSyBags.getBagsName())){
			return new ResultMsg("302","购物袋名称不能为空");
		}
		if(StringUtil.isEmpty(dwSyBags.getBagsCode())){
			return new ResultMsg("301","购物袋编码不能为空");
		}
		if(StringUtil.isEmpty(dwSyBags.getMerchantID())){
			return new ResultMsg("301","所属商户不能为空");
		}
		dwSyBagsMapper.insertData(dwSyBags);


		redisTemplate.opsForValue().set("dw_bags_"+dwSyBags.getMerchantID(), dwSyBagsMapper.findByMerchantId(dwSyBags));

		return new ResultMsg(ResultCode.Success);
	}



}
