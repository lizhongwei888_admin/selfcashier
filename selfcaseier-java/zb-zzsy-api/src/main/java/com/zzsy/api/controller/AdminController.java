package com.zzsy.api.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.util.StringUtil;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.util.JsonHelper;
import com.lujie.common.util.MD5Util;
import com.zzsy.api.constant.KeysManager;
import com.zzsy.api.constant.LogType;
import com.zzsy.api.constant.UserType;
import com.zzsy.api.entity.Log;
import com.zzsy.api.entity.Menu;
import com.zzsy.api.entity.User;
import com.zzsy.api.service.LogService;
import com.zzsy.api.service.MenuService;
import com.zzsy.api.service.UserService;
import com.zzsy.api.util.RedisUtil;

@Controller
@RequestMapping(value = "/admin")
public class AdminController {
	private static final Logger log = LoggerFactory.getLogger(AdminController.class);
	
	@Autowired
	private UserService userService;
	@Autowired
	private MenuService menuService;
	@Autowired
	private LogService logservice;
	
	@Autowired 
	private RedisTemplate redisTemplate; 
	
	/**
	 * 登陆
	 * @param request
	 * @param user
	 * @return
	 */
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	@ResponseBody
	public String login(HttpServletRequest request ,@RequestBody User user){
		return JsonHelper.toJson(userService.login(user)); 
	} 
	
	/**
	 * 注销
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/logout")
	@ResponseBody
	public String logout(HttpServletRequest request){
		return JsonHelper.toJson(userService.logout(request));
	} 
	
	
	
	
	
	
	/**
	 * 检查验证码是否正确
	 * @param code
	 * @return
	 */
	@RequestMapping(value = "/checkmobile", method = RequestMethod.POST)
	@ResponseBody 
	public String checkmobile( String mobile, HttpServletRequest request) {
		User user=userService.findByMobile(mobile);
		if(user==null){
			return JsonHelper.toJson(new ResultMsg("301","系统不存在这个手机号"));
		}
		return JsonHelper.toJson(new ResultMsg("200","",1));
		
	}
	/**
	 * 忘记密码修改密码
	 * @param fromcode
	 * @param mobile
	 * @param newpassword
	 * @param request
	 * @param response
	 * @return
	 * @throws UnloginException
	 */
	@RequestMapping(value = "/changepwdfromforget", method = RequestMethod.POST)
	@ResponseBody
	public String changepwdfromforget(String fromcode ,String mobile , String newpassword , HttpServletRequest request , HttpServletResponse response)  {
		
		boolean same = false;
		String localcode = RedisUtil.getSmsCode(redisTemplate, mobile);
		if(StringUtil.isEmpty(mobile)) {
			return JsonHelper.toJson(new ResultMsg("300", "手机号不能为空"));
		}
		if(StringUtil.isEmpty(fromcode)) {
			return JsonHelper.toJson(new ResultMsg("402","验证码不能为空"));
		}
		if(StringUtil.isEmpty(localcode)) {
			return JsonHelper.toJson(new ResultMsg("403","验证码失效"));
		}
		if(!localcode.equals(fromcode)) {
			return JsonHelper.toJson(new ResultMsg("404","验证码错误"));
		}
		if(StringUtil.isEmpty(newpassword)) {
			return JsonHelper.toJson(new ResultMsg("405","密码不能为空"));
		}
		if(newpassword.length()<6||newpassword.length()>16) {
			return JsonHelper.toJson(new ResultMsg("406","密码长度为6-16"));
		}
		
		User user = new User();
		user.setPassword(MD5Util.encrypt(newpassword));
		user.setMobilephone(mobile);
		
		this.userService.updatePwdByMobile(user);
		user=userService.findByMobile(mobile); 
		//this.logservice.create(user, user.getRealname()+"修改自己的密码");
		this.logservice.create(new Log(user.getId(), "修改自己的密码", LogType.UserType.getDesc(), UserType.Account.getDesc(), 1, 0) );
		RedisUtil.exitLoginUser(redisTemplate, user.getId());
		
		return JsonHelper.toJson(new ResultMsg("200","修改成功！","CloseCurrent=true"));
		
	} 
	
	/**
	 * 后台首页
	 * @param request
	 * @param map
	 * @return
	 * @throws UnloginException 
	 */
	@RequestMapping(value = "/info")
	@ResponseBody
	public String info(HttpServletRequest request , Map<String, Object> map)  {
		User user = RedisUtil.getUserByToken(redisTemplate, request);
		String userMenuKey=KeysManager.User_Auth+KeysManager.Menu+KeysManager.User+user.getId();
		List<Menu> sl=new ArrayList<Menu>();
		Menu menu = new Menu();
		menu.setId(null);
		menu.setPid(null);
		menuService.allChildMenu(sl, menu, user.getId());
		/*
		sl = (List<Menu>)redisTemplate.opsForValue().get(userMenuKey);
		if(sl==null){
			sl=new ArrayList<Menu>();
			Menu menu = new Menu();
			menu.setId(null);
			menu.setPid(null);
			menuService.allChildMenu(sl, menu, user.getId());
			
			if(sl!=null && sl.size()>0){
				redisTemplate.opsForValue().set(userMenuKey, sl);
			} 
		}*/
		
		map.put("pmenus", sl);
		map.put("user", user);
		String result=JsonHelper.toJson(sl);
		//log.info(result);
		return JsonHelper.toJson(new ResultMsg("200","",map));
	}
	
}
