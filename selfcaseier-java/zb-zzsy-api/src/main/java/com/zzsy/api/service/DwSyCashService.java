package com.zzsy.api.service;

import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.service.BasicService;
import com.lujie.common.util.JsonHelper;
import com.lujie.common.util.StringUtil;
import com.zzsy.api.entity.*;
import com.zzsy.api.mapper.DwSyCashMapper;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class DwSyCashService extends BasicService {
	public static final Logger log = LoggerFactory.getLogger(DwSyCashService.class);

	@Autowired
	private RedisTemplate redisTemplate;
	@Autowired
	private DwSyCashMapper dwSyCashMapper;

	@Autowired
	private RefLogAppService refLogAppService;

	@Resource(name = "dwSyCashMapper")
	public void initBaseDao(DwSyCashMapper dwSyCashMapper) {
		super.setBaseMapper(dwSyCashMapper);
	}


	public Map<String,Object> findByDeviceCode(DwSyCash dwSyCash){
		if(dwSyCash.getDeviceCode() == null){
			return null;
		}
		Map<String,Object> data = (Map<String,Object>)redisTemplate.opsForValue().get("deviceCode_"+dwSyCash.getDeviceCode());
		if(data==null) {
			data = dwSyCashMapper.findByDeviceCode(dwSyCash);
			if(data == null) {
				redisTemplate.opsForValue().set("deviceCode_" + dwSyCash.getDeviceCode(), data);
			}
		}
		return  data;
	}

	public ResultMsg insertData(HttpServletRequest request, DwSyCash dwSyCash){
		if(StringUtil.isEmpty(dwSyCash.getDeviceCode())){
			return new ResultMsg("301","设备编码不能为空");
		}
		if(StringUtil.isEmpty(dwSyCash.getShopId())){
			return new ResultMsg("302","所属门店不能为空");
		}
		List<DwSyCash> cash = dwSyCashMapper.findList(dwSyCash);
		if(cash.size()>0){
			boolean have = false;
			for (int i = 0; i < cash.size(); i++) {
				if(cash.get(i).getAuditState()!=3){
					have = true;
				}

			}
			if(have)
				return new ResultMsg("305","设备已经存在");
		}


		dwSyCashMapper.add(dwSyCash);
		redisTemplate.delete("deviceCode_"+dwSyCash.getDeviceCode());
		return new ResultMsg(ResultCode.Success);
	}



	public Map<String,Object>  getSyCashNo(DwSyCash dwSyCash){
		Map<String,Object> data = dwSyCashMapper.getSyCashNo(dwSyCash);
		return data;
	}

	public ResultMsg auditData(DwSyCash dwSyCash){

		dwSyCashMapper.auditData(dwSyCash);
		Map<String,Object> data = dwSyCashMapper.findByDeviceCode(dwSyCash);
		redisTemplate.opsForValue().set("deviceCode_"+dwSyCash.getDeviceCode(), data);

		return new ResultMsg(ResultCode.Success);
	}

	/**
	 * 添加或修改
	 *
	 * @param dwSyCash
	 * @return
	 */
	public int addOrUp(DwSyCash dwSyCash) {

		if (StringUtils.isEmpty(dwSyCash.getCode())) {
			//插入唯一code
			dwSyCash.setCode(UUID.randomUUID().toString().replace("-", ""));
			int i = dwSyCashMapper.add(dwSyCash);
			Map<String,Object> data = dwSyCashMapper.findByDeviceCode(dwSyCash);
			redisTemplate.opsForValue().set("deviceCode_"+dwSyCash.getDeviceCode(), data);
			return i;
		} else {
			dwSyCash.setUpdate_time(new Date());
			int up = dwSyCashMapper.up(dwSyCash);
			Map<String,Object> data = dwSyCashMapper.findByDeviceCode(dwSyCash);
			redisTemplate.opsForValue().set("deviceCode_"+dwSyCash.getDeviceCode(), data);
			return up;
		}

	}


	public Long delete(String codes) {
		Set<String> collect = Arrays.stream(codes.split(",")).collect(Collectors.toSet());

			DwSyCash dwSyCash=new DwSyCash();
			dwSyCash.setCode(codes);
			DwSyCash data = (DwSyCash)dwSyCashMapper.findOne(dwSyCash);
			redisTemplate.delete("deviceCode_"+data.getDeviceCode());


		return dwSyCashMapper.deleteByIdSet(collect);
	}

}
