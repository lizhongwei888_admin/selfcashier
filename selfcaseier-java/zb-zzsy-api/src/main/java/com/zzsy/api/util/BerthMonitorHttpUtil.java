package com.zzsy.api.util;

import java.util.HashMap;

import com.zzsy.api.config.UserAuthConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.pagehelper.util.StringUtil;
import com.lujie.common.constant.HttpContentType;
import com.lujie.common.util.HttpRequest;

public class BerthMonitorHttpUtil {
	private final static Logger log = LoggerFactory.getLogger(BerthMonitorHttpUtil.class);
	
	public static void send(final String suburl,final String jsonStr){
		
		Runnable runnable = new Runnable() {
            @Override
            public void run() {
            	 try {
            		String controlDeviceUrl= UserAuthConfig.ConfigMap.getStr("host");
        			if(StringUtil.isEmpty(controlDeviceUrl)){
        				log.info("没有配置項目地址host");
        				return;
        			}
        			controlDeviceUrl=controlDeviceUrl+":8994"+suburl;
        			
        			log.info("开始调用泊位监控接口:controlDeviceUrl="+controlDeviceUrl+",jsonStr="+jsonStr);
        			
            		HashMap<String,String> reqHeaderMap=new HashMap<>();
            		reqHeaderMap.put("Content-Type",HttpContentType.ApplicationJSON);
            		reqHeaderMap.put("token", UserAuthConfig.Systoken);
            		
            		String result=HttpRequest.sendPost(reqHeaderMap, controlDeviceUrl, jsonStr);
        			
            		log.info("调用泊位监控接口結果:,"+result);
        		} catch (Exception e) {
        			log.info("调用泊位监控接口失败:"+e.getMessage());
        		}
        	}
        };
        new Thread(runnable).start();  
		
		
		
	}
	 
}
