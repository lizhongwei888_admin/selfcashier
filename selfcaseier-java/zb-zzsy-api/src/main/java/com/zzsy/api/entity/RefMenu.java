package com.zzsy.api.entity;

import com.lujie.common.entity.BasicEntity;

import java.io.Serializable;
import java.util.List;

/*
菜单管理
 */
public class RefMenu extends BasicEntity implements Serializable {
    private static final long serialVersionUID = -301062307193761449L;

    //唯一标识
    private String code;
    //菜单名称
    private String menuName;
    //菜单描述
    private String menuDesc;
    //菜单类型：1目录；2页面；3按钮
    private Integer menuType;
    //上级菜单ID
    private String menuPid;

    //菜单路径
    private String menuPath;
    //权限标识
    private String authority;
    //菜单种类：1.管理端；2.自助收银端
    private Integer menuClass;
    //排序
    private Integer sort;
    //图标
    private String icon;
    //状态：true启用；false禁用 默认开启
    private String state;

    //状态：true显示；false隐藏 默认显示
    private String isHidden;

    //存放下一个等级的数据
    private List<RefMenu> nextLevelData;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getMenuDesc() {
        return menuDesc;
    }

    public void setMenuDesc(String menuDesc) {
        this.menuDesc = menuDesc;
    }

    public Integer getMenuType() {
        return menuType;
    }

    public void setMenuType(Integer menuType) {
        this.menuType = menuType;
    }



    public String getMenuPath() {
        return menuPath;
    }

    public void setMenuPath(String menuPath) {
        this.menuPath = menuPath;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public Integer getMenuClass() {
        return menuClass;
    }

    public void setMenuClass(Integer menuClass) {
        this.menuClass = menuClass;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getMenuPid() {
        return menuPid;
    }

    public void setMenuPid(String menuPid) {
        this.menuPid = menuPid;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getIsHidden() {
        return isHidden;
    }

    public void setIsHidden(String isHidden) {
        this.isHidden = isHidden;
    }

    public List<RefMenu> getNextLevelData() {
        return nextLevelData;
    }

    public void setNextLevelData(List<RefMenu> nextLevelData) {
        this.nextLevelData = nextLevelData;
    }
}
