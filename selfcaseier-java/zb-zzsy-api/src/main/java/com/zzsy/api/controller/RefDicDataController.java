package com.zzsy.api.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.util.JsonHelper;
import com.zzsy.api.entity.RefCity;
import com.zzsy.api.entity.RefDicData;
import com.zzsy.api.entity.RefUser;
import com.zzsy.api.service.RefDicDataService;
import com.zzsy.api.util.RedisRefUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * 字典数据管理
 */
@Controller
@RequestMapping(value = "/refDicData")
public class RefDicDataController {

    @Autowired
    private RefDicDataService refDicDataService;
    @Autowired
    private RedisTemplate redisTemplate;


    @RequestMapping(value = "/addRefDicData", method = RequestMethod.POST)
    @ResponseBody
    public String addRefDicData(HttpServletRequest request, @RequestBody RefDicData refDicData) {
        RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
        refDicData.setCreateby(loginRefUser.getCode());
        refDicData.setCreate_time(new Date());
        refDicData.setUpdateby(loginRefUser.getCode());
        refDicData.setUpdate_time(new Date());
        if (refDicDataService.addOrUp(refDicData) <= 0) {
            return JsonHelper.toJson(new ResultMsg("300", "添加失败"));
        }
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success, refDicData));

    }

    @RequestMapping(value = "/upRefDicData", method = RequestMethod.POST)
    @ResponseBody
    public String upRefDicData(HttpServletRequest request, @RequestBody RefDicData refDicData) {
        if (StringUtils.isEmpty(refDicData.getCode())) {
            return JsonHelper.toJson(new ResultMsg("300", "code不能为空"));
        }
        RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
        refDicData.setUpdateby(loginRefUser.getCode());
        refDicData.setUpdate_time(new Date());
        if (refDicDataService.addOrUp(refDicData) <= 0) {
            return JsonHelper.toJson(new ResultMsg("300", "修改失败"));
        }
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success, refDicData));

    }

    /**
     * 删除
     *
     * @param object
     * @return
     */
    @RequestMapping(value = "/delRefDicData", method = RequestMethod.POST)
    @ResponseBody
    public String delRefDicData(@RequestBody JSONObject object) {
        String codes = object.getString("codes");
        if (StringUtils.isEmpty(codes)) {
            return JsonHelper.toJson(new ResultMsg("300", "code不能为空"));
        }

        refDicDataService.delete(codes);
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success));
    }

    /**
     * 后端分页查询集合
     *
     * @param object
     * @return
     */
    @RequestMapping(value = "/listRefDicData", method = RequestMethod.POST)
    @ResponseBody
    public String listRefDicData(@RequestBody JSONObject object) {
        RefDicData refDicData = JSONObject.parseObject(JSON.toJSONString(object.getJSONObject("params")), RefDicData.class);
        if (object.getInteger("offset") != null && object.getInteger("limit") != null) {
            refDicData.setPageNum(object.getInteger("offset"));
            refDicData.setPageSize(object.getInteger("limit"));
        }
        JSONObject sort = object.getJSONObject("sort");
        if (sort != null) {
            String prop = sort.getString("prop");
            String order = sort.getString("order");
            refDicData.setProp(prop);
            refDicData.setOrder(order);
        }
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success, refDicDataService.findPageList(refDicData), refDicDataService.countTotal(refDicData)));
    }

}
