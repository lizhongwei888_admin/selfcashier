package com.zzsy.api.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lujie.common.util.JsonHelper;
import com.zzsy.api.entity.ModuleRole;
import com.zzsy.api.entity.ModuleRoleRelative;
import com.zzsy.api.service.ModuleRoleRelativeService;
import com.zzsy.api.service.ModuleRoleService;

/**
 * 模块角色关系控制器
 * @author hupei
 *
 */
@Controller
@RequestMapping(value = "/modulerolerelative")
public class ModuleRoleRelativeController  {
	
	@Autowired
	private ModuleRoleService moduleRoleService;
	@Autowired
	private ModuleRoleRelativeService moduleRoleRelativeService;
	
	/**
	 * 角色添加模块
	 */
	@RequestMapping(value = "/roleAddModule", method = RequestMethod.POST)
	@ResponseBody
	public String roleAddModule(@RequestBody ModuleRoleRelative moduleRoleRelative,HttpServletRequest request){
		return JsonHelper.toJson(moduleRoleRelativeService.roleAddModule(moduleRoleRelative,request));
	}
	/**
	 * 角色删除模块
	 * @param roleid
	 */
	@RequestMapping(value = "/roleDeleteModule", method = RequestMethod.POST)
	@ResponseBody
	public String roleDeleteModule(@RequestBody ModuleRoleRelative moduleRoleRelative,HttpServletRequest request){
		return JsonHelper.toJson(moduleRoleRelativeService.roleDeleteModule(moduleRoleRelative,request));
	}
	
	
	@RequestMapping(value = "/roleAddModuleData", method = RequestMethod.POST)
	@ResponseBody
	public String roleAddModuleData(@RequestBody ModuleRole moduleRole,HttpServletRequest request){
		return JsonHelper.toJson(moduleRoleRelativeService.roleAddModuleData(moduleRole,request));
	} 
	@RequestMapping(value = "/roleDeleteModuleData", method = RequestMethod.POST)
	@ResponseBody
	public String roleDeleteModuleData(@RequestBody ModuleRole moduleRole,HttpServletRequest request){
		return JsonHelper.toJson(moduleRoleRelativeService.roleDeleteModuleData(moduleRole,request));
	} 
	
	
	 
}
