package com.zzsy.api.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.util.JsonHelper;
import com.zzsy.api.entity.RefApi;
import com.zzsy.api.entity.RefApiItem;
import com.zzsy.api.entity.RefUser;
import com.zzsy.api.service.RefApiItemService;
import com.zzsy.api.util.RedisRefUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * 系统接口配置项管理
 */
@Controller
@RequestMapping(value = "/refApiItem")
public class RefApiItemController {

    @Autowired
    private RefApiItemService refApiItemService;
    @Autowired
    private RedisTemplate redisTemplate;


    /**
     * 添加系统接口配置项
     *
     * @param request
     * @param refApiItem
     * @return
     */
    @RequestMapping(value = "/addRefApiItem", method = RequestMethod.POST)
    @ResponseBody
    public String addRefApiItem(HttpServletRequest request, @RequestBody RefApiItem refApiItem) {
        RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
        refApiItem.setCreateby(loginRefUser.getCode());
        refApiItem.setCreate_time(new Date());
        refApiItem.setUpdateby(loginRefUser.getCode());
        refApiItem.setUpdate_time(new Date());
        if (refApiItemService.addOrUp(refApiItem) <= 0) {
            return JsonHelper.toJson(new ResultMsg("300", "添加失败"));
        }
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success, refApiItem));

    }

    /**
     * 修改系统接口配置项
     *
     * @param request
     * @param refApiItem
     * @return
     */
    @RequestMapping(value = "/upRefApiItem", method = RequestMethod.POST)
    @ResponseBody
    public String upRefApiItem(HttpServletRequest request, @RequestBody RefApiItem refApiItem) {
        RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
        refApiItem.setUpdateby(loginRefUser.getCode());
        refApiItem.setUpdate_time(new Date());
        if (refApiItemService.addOrUp(refApiItem) <= 0) {
            return JsonHelper.toJson(new ResultMsg("300", "修改失败"));
        }
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success, refApiItem));

    }

    /**
     * 删除
     *
     * @param object code逗号分隔
     * @return
     */
    @RequestMapping(value = "/delRefApiItem", method = RequestMethod.POST)
    @ResponseBody
    public String delRefApiItem(@RequestBody JSONObject object) {
        String codes = object.getString("codes");
        if (StringUtils.isEmpty(codes)) {
            return JsonHelper.toJson(new ResultMsg("300", "code不能为空"));
        }

        refApiItemService.delete(codes);
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success));
    }

    /**
     * 后端分页查询集合
     *
     * @param object
     * @return
     */
    @RequestMapping(value = "/listRefApiItem", method = RequestMethod.POST)
    @ResponseBody
    public String listRefApiItem(@RequestBody JSONObject object) {
        RefApiItem refApiItem = JSONObject.parseObject(JSON.toJSONString(object.getJSONObject("params")), RefApiItem.class);
        if (object.getInteger("offset") != null && object.getInteger("limit") != null) {
            refApiItem.setPageNum(object.getInteger("offset"));
            refApiItem.setPageSize(object.getInteger("limit"));
        }
        JSONObject sort = object.getJSONObject("sort");
        if (sort != null) {
            String prop = sort.getString("prop");
            String order = sort.getString("order");
            refApiItem.setProp(prop);
            refApiItem.setOrder(order);
        }
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success, refApiItemService.findPageList(refApiItem), refApiItemService.countTotal(refApiItem)));
    }

}
