package com.zzsy.api.service;

import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.service.BasicService;
import com.zzsy.api.entity.DwOrderShopdetail;
import com.zzsy.api.mapper.DwOrderShopdetailMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

@Service
public class DwOrderShopdetailService extends BasicService {
	public static final Logger log = LoggerFactory.getLogger(DwOrderShopdetailService.class);

	@Autowired
	private RedisTemplate redisTemplate;
	@Autowired
	private DwOrderShopdetailMapper dwOrderShopdetailMapper;
	@Resource(name = "dwOrderShopdetailMapper")
	public void initBaseDao(DwOrderShopdetailMapper dwOrderShopdetailMapper) {
		super.setBaseMapper(dwOrderShopdetailMapper);
	}

	public ResultMsg insertData(HttpServletRequest request, DwOrderShopdetail dwOrderShopdetail){
		String code = UUID.randomUUID().toString();
		dwOrderShopdetail.setCode(code);
		dwOrderShopdetailMapper.insertData(dwOrderShopdetail);
		return new ResultMsg(ResultCode.Success);
	}



}
