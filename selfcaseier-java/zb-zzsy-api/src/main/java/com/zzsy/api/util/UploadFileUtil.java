package com.zzsy.api.util;

import com.lujie.common.constant.ResultMsg;
import com.lujie.common.util.StringUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.net.InetAddress;
import java.util.UUID;

/** * 文件上传 * * @author YangXiuTeng * @date Jun 1, 2015 */
@Component
public class UploadFileUtil {

    /** * 上传文件 * * @param multipartFile 文件对象 * @param dir 上传目录 * @return */
    public static ResultMsg uploadFile(MultipartFile multipartFile, String dir) {

        try {

            if (multipartFile.isEmpty()) {

                return new ResultMsg("302","请选择上传文件");
            }
            // 获取文件的名称
            String originalFilename = multipartFile.getOriginalFilename();
            // 文件后缀 例如：.png
            String fileSuffix = originalFilename.substring(originalFilename.lastIndexOf("."));
            // uuid 生成文件名
            String uuid = String.valueOf(UUID.randomUUID());
            // 根路径，在 resources/static/upload
            String basePath = ResourceUtils.getURL("classpath:").getPath() + "static/upload/" + (StringUtil.isNotEmpty(dir) ? (dir + "/") : "");
            // 新的文件名，使用uuid生成文件名
            String fileName = uuid + fileSuffix;
            // 创建新的文件
            File fileExist = new File(basePath);
            // 文件夹不存在，则新建
            if (!fileExist.exists()) {

                fileExist.mkdirs();
            }
            // 获取文件对象
            File file = new File(basePath, fileName);
            // 完成文件的上传
            multipartFile.transferTo(file);

            // 返回绝对路径
            return new ResultMsg("200", "http://" + InetAddress.getLocalHost().getHostAddress() + ":8990/userbase/upload/" + fileName);
        } catch (Exception e) {

            e.printStackTrace();
        }
        return new ResultMsg("200","上传失败");
    }
}