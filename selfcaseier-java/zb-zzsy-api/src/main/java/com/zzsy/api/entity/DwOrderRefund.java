package com.zzsy.api.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.lujie.common.entity.BasicEntity;

import java.util.Date;
import java.io.Serializable;

/**
 * 退货管理(DwOrderRefund)实体类
 *
 * @author makejava
 * @since 2024-02-05 18:28:05
 */
public class DwOrderRefund extends BasicEntity implements Serializable {
    private static final long serialVersionUID = -93186519352555427L;
    /**
     * 唯一标识
     */
    private String code;
    /**
     * 用户ID
     */
    private String userId;
    /**
     * 第三方用户Code
     */
    private String userCode;
    /**
     * 第三方用户手机号
     */
    private String userPhone;
    /**
     * 第三方用户姓名
     */
    private String userName;
    /**
     * 退货编号
     */
    private String refundNo;
    /**
     * 订单状态：1.待支付；2.已支付；3.用户取消；4.系统取消；5申请退款；6已退款；7.退款失败；11：部分支付；12 取消支付；
     */
    private Integer orderState;
    /**
     * 商品总金额
     */
    private Integer goodTMoney;
    /**
     * 促销优惠总价格
     */
    private Integer promotTMoney;
    /**
     * 订单支付金额
     */
    private Integer actualTMoney;
    /**
     * 销售日期（订单完成时间）
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date saleTime;
    /**
     * 订单关闭时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date cancelTime;
    /**
     * 订单渠道：1自助收银app
     */
    private Integer orderChannel;
    /**
     * 所属门店ID
     */
    private String shopId;
    /**
     * 所属门店名称
     */
    private String shopName;
    /**
     * 收银机ID
     */
    private String cashID;
    /**
     * 商户ID
     */
    private String merchantId;
    /**
     * 推送erp状态：1未推送；2已推送
     */
    private Integer erpState;
    /**
     * 推送erp的请求json内容
     */
    private String requestJsonErp;
    /**
     * 推送erp的返回json内容
     */
    private String returnJsonErp;
    /**
     * 整单促销请求json内容
     */
    private String requestJsonPay;
    /**
     * 整单促销返回json内容
     */
    private String returnJsonPay;
    /**
     * 来源订单ID
     */
    private String orderId;
    /**
     * 退货原因
     */
    private String reason;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRefundNo() {
        return refundNo;
    }

    public void setRefundNo(String refundNo) {
        this.refundNo = refundNo;
    }

    public Integer getOrderState() {
        return orderState;
    }

    public void setOrderState(Integer orderState) {
        this.orderState = orderState;
    }

    public Integer getGoodTMoney() {
        return goodTMoney;
    }

    public void setGoodTMoney(Integer goodTMoney) {
        this.goodTMoney = goodTMoney;
    }

    public Integer getPromotTMoney() {
        return promotTMoney;
    }

    public void setPromotTMoney(Integer promotTMoney) {
        this.promotTMoney = promotTMoney;
    }

    public Integer getActualTMoney() {
        return actualTMoney;
    }

    public void setActualTMoney(Integer actualTMoney) {
        this.actualTMoney = actualTMoney;
    }

    public Date getSaleTime() {
        return saleTime;
    }

    public void setSaleTime(Date saleTime) {
        this.saleTime = saleTime;
    }

    public Date getCancelTime() {
        return cancelTime;
    }

    public void setCancelTime(Date cancelTime) {
        this.cancelTime = cancelTime;
    }

    public Integer getOrderChannel() {
        return orderChannel;
    }

    public void setOrderChannel(Integer orderChannel) {
        this.orderChannel = orderChannel;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getCashID() {
        return cashID;
    }

    public void setCashID(String cashID) {
        this.cashID = cashID;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public Integer getErpState() {
        return erpState;
    }

    public void setErpState(Integer erpState) {
        this.erpState = erpState;
    }

    public String getRequestJsonErp() {
        return requestJsonErp;
    }

    public void setRequestJsonErp(String requestJsonErp) {
        this.requestJsonErp = requestJsonErp;
    }

    public String getReturnJsonErp() {
        return returnJsonErp;
    }

    public void setReturnJsonErp(String returnJsonErp) {
        this.returnJsonErp = returnJsonErp;
    }

    public String getRequestJsonPay() {
        return requestJsonPay;
    }

    public void setRequestJsonPay(String requestJsonPay) {
        this.requestJsonPay = requestJsonPay;
    }

    public String getReturnJsonPay() {
        return returnJsonPay;
    }

    public void setReturnJsonPay(String returnJsonPay) {
        this.returnJsonPay = returnJsonPay;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }


}

