package com.zzsy.api.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.lujie.common.service.BasicService;
import com.lujie.common.util.PageBean;
import com.lujie.common.util.PaginationContext;
import com.zzsy.api.entity.Role;
import com.zzsy.api.entity.RoleMenu;
import com.zzsy.api.entity.UserRole;
import com.zzsy.api.mapper.RoleMapper;
import com.zzsy.api.mapper.UserRoleMapper;

/**
 * 角色接口
 * @author hupei
 *
 */
@Service
public class RoleService extends BasicService {
	
	@Autowired
	private RoleMapper roleMapper;
	@Autowired
	private UserRoleMapper userRoleMapper;
	@Resource(name="roleMapper")
	public void initBaseDao(RoleMapper roleMapper){
		super.setBaseMapper(roleMapper);
	}
	/**
	 * 通过菜单获取拥有该菜单的角色
	 */
	
	public List<Role> findRoleListByMenu(RoleMenu roleMenu){
		return roleMapper.findRoleListByMenu(roleMenu);
	}
	public Integer countRoleListByMenu(RoleMenu roleMenu){
		return roleMapper.countRoleListByMenu(roleMenu);
	}

	/**
	 * 查找全部角色
	 */
	
	public PageBean<Role> findAllRole(String id, String text) {
		PageHelper.startPage(PaginationContext.getPageNum(), PaginationContext.getPageSize());
		Map<String , String> map = new HashMap<String , String>();
		map.put("id", id);
		map.put("text" , text);
        map.put("orderField" , PaginationContext.getOrderField());
        map.put("orderDirection" , PaginationContext.getOrderDirection());
        List<Role> list = this.roleMapper.findAllRole(map);
        //PageInfo<Role> pageInfo=new PageInfo<Role>(list);
        return new PageBean<Role>(list);
	}

	/**
	 * 保存角色
	 */
	
	public void saveOrUpdate(Role role) {
		if (null == role.getId() || role.getId().equals("")){
			role.setId(UUID.randomUUID().toString().replaceAll("-", ""));
			this.roleMapper.insertSelective(role);
		}else{
			this.roleMapper.updateByPrimaryKeySelective(role);
		}
	}

	/**
	 * 删除角色
	 */
	
	public void delRole(String roleid) {
		this.roleMapper.deleteByPrimaryKey(roleid);
	}

	
	public Role findOneRole(String roleid) {
		return this.roleMapper.selectByPrimaryKey(roleid);
	}

	
	public List<Role> findAllRole() {
		Map<String , String> map = new HashMap<String , String>();
        map.put("orderField" ,"text");
        map.put("orderDirection" ,"desc");
		return this.roleMapper.findAllRole(map);
	}

	
	public String findRoleByUid(String uid) {
		return this.userRoleMapper.findRoleByUid(uid);
	}

	
	public void updateRoleUserByUid(String uid, String myrole) {
		this.userRoleMapper.delRoleUserByUid(uid);
		for(String role : myrole.split(",")){
			UserRole record = new UserRole();
			record.setUid(uid);
			record.setRid(role);
			this.userRoleMapper.insertSelective(record);
		}
	}

}
