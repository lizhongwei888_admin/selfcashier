package com.zzsy.api.service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import com.zzsy.api.constant.LogType;
import com.zzsy.api.constant.UserType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.entity.EntityMap;
import com.lujie.common.service.BasicService;
import com.lujie.common.util.StringUtil;
import com.zzsy.api.entity.Log;
import com.zzsy.api.entity.User;
import com.zzsy.api.mapper.UserSettingMapper;
import com.zzsy.api.util.RedisUtil;

@Service
public class UserSettingService extends BasicService{
	private static final Logger log = LoggerFactory.getLogger(UserSettingService.class);
	
	@Autowired
	private LogService logService;
	
	
	@Autowired 
	private RedisTemplate redisTemplate;
	
	@Autowired
	private UserSettingMapper userSettingMapper;
	@Resource(name="userSettingMapper")
	public void initBaseDao(UserSettingMapper userSettingMapper){
		super.setBaseMapper(userSettingMapper);
	}
	
	
	public ResultMsg save(HttpServletRequest request, EntityMap entityMap){
		if(StringUtil.isEmpty(entityMap.getStr("userId"))){
			return new ResultMsg("301","usId不能为空");//user id
		}
		if(StringUtil.isEmpty(entityMap.getStr("alertType"))){
			return new ResultMsg("303","alertType不能为空");//user id
		}
		if(StringUtil.isEmpty(entityMap.getStr("isAlert"))){
			return new ResultMsg("302","isAlert不能为空");//user id
		}
		if(StringUtil.isEmpty(entityMap.getStr("alertTypeValue"))){
			return new ResultMsg("304","alertTypeValue不能为空");//user id
		}
		try{
			
			User admin=RedisUtil.getUser(redisTemplate, request);
			entityMap.put("createUser", admin.getId());
			entityMap.put("modifyUser", admin.getId());
			entityMap.generateDataId();
			userSettingMapper.save(entityMap);
			
			//logService.create(admin, "添加用户设置："+entityMap.toString()); 
			this.logService.create(new Log(admin.getId(), "添加用户设置："+entityMap.toString(), LogType.UserType.getDesc(), UserType.Account.getDesc(), 0, 0) );

			
		}catch(Exception ex){
			ex.printStackTrace();
			log.info("添加用户设置异常:"+StringUtil.getExceptionDesc(ex));
			return new ResultMsg("303","异常");
		}
		
		
		return new ResultMsg(ResultCode.Success);
	}
	
	public EntityMap findById(Integer id){
		EntityMap entityMap=new EntityMap();
		entityMap.put("id", id);
		return (EntityMap)userSettingMapper.findOne(entityMap);
	}
	public EntityMap findByUserId(String userId){
		EntityMap entityMap=new EntityMap();
		entityMap.put("userId", userId);
		return (EntityMap)userSettingMapper.findOne(entityMap);
	}
	public EntityMap findByUserIdAndalertType(String userId,Integer alertType){
		EntityMap entityMap=new EntityMap();
		entityMap.put("userId", userId);
		entityMap.put("alertType", alertType);
		return (EntityMap)userSettingMapper.findOne(entityMap);
	}
	
	public ResultMsg update(HttpServletRequest request, EntityMap entityMap){
		
		String userId=entityMap.getStr("userId");
		if(StringUtil.isEmpty(userId)){
			return new ResultMsg("301","用户设置userId不能为空");//id
		}
		Integer alertType=entityMap.getInt("alertType");
		if(alertType==null){
			return new ResultMsg("303","提醒类型不能为空");//user id
		}
		
		EntityMap dbEntityMap=findByUserIdAndalertType(userId,alertType);
		if(dbEntityMap==null){
			return save(request,  entityMap);
		}
		
		User admin=RedisUtil.getUser(redisTemplate, request);
		entityMap.put("modifyUser", admin.getId());
		
		userSettingMapper.update(entityMap);
		
		String oldEntityMap=dbEntityMap.toString();
		dbEntityMap=findByUserId(userId);
		String newEntityMap=dbEntityMap.toString();
		
		//logService.create(admin, "修改用户设置：原："+oldEntityMap+",改为:"+newEntityMap); 
		this.logService.create(new Log(admin.getId(), "修改用户设置：原："+oldEntityMap+",改为:"+newEntityMap, LogType.UserType.getDesc(), UserType.Account.getDesc(), 1, 0) );

		return new ResultMsg(ResultCode.Success);
	}
	
	
	
}
