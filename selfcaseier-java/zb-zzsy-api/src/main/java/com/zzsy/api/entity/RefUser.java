package com.zzsy.api.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.lujie.common.entity.BasicEntity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * 用户管理
 */
public class RefUser extends BasicEntity implements Serializable {

    private static final long serialVersionUID = -301062307193761449L;

    //唯一标识
    private String code;
    //昵称
    private String nickName;
    //账号
    private String account;
    //密码
    private String password;
    //邮箱
    private String email;
    //电话
    private String phone;
    //描述
    private String userDesc;

    //头像
    private String avatar;
    //到期时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date expireTime;
    //角色ID
    private String roleID;
    //上次登录时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date lastLoginTime;
    //状态：true启用；false禁用
    private String state;

    //存放后台用户的登录token
    private String token;

    /*---------------与数据库无关字段--------*/
    private String role_name;
    private Boolean stateBool;
    private Set<String> roleIDs;
    //登录验证码的key
    private String userKey;
    //登录验证码
    private String codeRefUser;
    //旧密码
    private String oldPassword;
    //新密码
    private String newPassword;
    //确认新密码
    private String confirmNewPassword;
    /*---------------结束--------*/

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUserDesc() {
        return userDesc;
    }

    public void setUserDesc(String userDesc) {
        this.userDesc = userDesc;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Date getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Date expireTime) {
        this.expireTime = expireTime;
    }

    public String getRoleID() {
        return roleID;
    }

    public void setRoleID(String roleID) {
        this.roleID = roleID;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getRole_name() {
        return role_name;
    }

    public void setRole_name(String role_name) {
        this.role_name = role_name;
    }


    public Boolean getStateBool() {
        if ("false".equals(this.state)) {
            this.stateBool = Boolean.FALSE;
        } else {
            this.stateBool = Boolean.TRUE;
        }
        return this.stateBool;
    }

    public void setStateBool(Boolean stateBool) {
        if ("false".equals(this.state)) {
            this.stateBool = Boolean.FALSE;
        } else {
            this.stateBool = Boolean.TRUE;
        }
    }

    public Set<String> getRoleIDs() {
        return roleIDs;
    }

    public void setRoleIDs(Set<String> roleIDs) {
        this.roleIDs = roleIDs;
    }

    public String getUserKey() {
        return userKey;
    }

    public void setUserKey(String userKey) {
        this.userKey = userKey;
    }

    public String getCodeRefUser() {
        return codeRefUser;
    }

    public void setCodeRefUser(String codeRefUser) {
        this.codeRefUser = codeRefUser;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmNewPassword() {
        return confirmNewPassword;
    }

    public void setConfirmNewPassword(String confirmNewPassword) {
        this.confirmNewPassword = confirmNewPassword;
    }
}
