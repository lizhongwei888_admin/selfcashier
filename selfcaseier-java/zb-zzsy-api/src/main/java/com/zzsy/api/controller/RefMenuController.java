package com.zzsy.api.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.util.JsonHelper;
import com.zzsy.api.entity.RefDicType;
import com.zzsy.api.entity.RefLogAdmin;
import com.zzsy.api.entity.RefMenu;
import com.zzsy.api.entity.RefUser;
import com.zzsy.api.service.RefLogAdminService;
import com.zzsy.api.service.RefMenuService;
import com.zzsy.api.util.RedisRefUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 系统菜单控制器
 */
@Controller
@RequestMapping(value = "/refMenu")
public class RefMenuController {

    @Autowired
    private RefMenuService refMenuService;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private RefLogAdminService refLogAdminService;

    @RequestMapping(value = "/addRefMenu", method = RequestMethod.POST)
    @ResponseBody
    public String addRefMenu(HttpServletRequest request, @RequestBody RefMenu refMenu) {
        RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
        refMenu.setCreateby(loginRefUser.getCode());
        refMenu.setCreate_time(new Date());
        refMenu.setUpdateby(loginRefUser.getCode());
        refMenu.setUpdate_time(new Date());
        if (refMenuService.addOrUp(refMenu) <= 0) {
            return JsonHelper.toJson(new ResultMsg("300", "添加失败"));
        }
        this.refLogAdminService.create(new RefLogAdmin(loginRefUser.getCode()
                , "菜单页面", "添加菜单", "addRefMenu", "/refMenu/addRefMenu"
                , refMenu, "ok", loginRefUser.getOrgID()));
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success, refMenu));

    }

    @RequestMapping(value = "/upRefMenu", method = RequestMethod.POST)
    @ResponseBody
    public String upRefMenu(HttpServletRequest request, @RequestBody RefMenu refMenu) {
        RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
        if (StringUtils.isEmpty(refMenu.getCode())) {
            return JsonHelper.toJson(new ResultMsg("300", "code不能为空"));
        }
        refMenu.setUpdateby(loginRefUser.getCode());
        refMenu.setUpdate_time(new Date());
        if (refMenuService.addOrUp(refMenu) <= 0) {
            return JsonHelper.toJson(new ResultMsg("300", "修改失败"));
        }
        this.refLogAdminService.create(new RefLogAdmin(loginRefUser.getCode()
                , "菜单页面", "修改菜单信息", "upRefMenu", "/refMenu/upRefMenu"
                , refMenu, "ok", loginRefUser.getOrgID()));
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success, refMenu));

    }

    /**
     * 删除
     *
     * @param object
     * @return
     */
    @RequestMapping(value = "/delRefMenu", method = RequestMethod.POST)
    @ResponseBody
    public String delRefMenu(HttpServletRequest request, @RequestBody JSONObject object) {
        String codes = object.getString("codes");
        RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
        if (StringUtils.isEmpty(codes)) {
            return JsonHelper.toJson(new ResultMsg("300", "code不能为空"));
        }
        refMenuService.delete(codes);
        this.refLogAdminService.create(new RefLogAdmin(loginRefUser.getCode()
                , "菜单页面", "删除菜单", "delRefMenu", "/refMenu/delRefMenu"
                , codes, "ok", loginRefUser.getOrgID()));
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success));
    }

    /**
     * 后端分页查询集合
     *
     * @param object
     * @return
     */
    @RequestMapping(value = "/listRefMenu", method = RequestMethod.POST)
    @ResponseBody
    public String listRefMenu(@RequestBody JSONObject object) {
        RefMenu refMenu = new RefMenu();
        JSONObject params = object.getJSONObject("params");
        refMenu.setMenuName(params.getString("title"));
        refMenu.setMenuPid(params.getString("pid"));
        refMenu.setMenuType(params.getInteger("lever"));
        if (object.getInteger("offset") != null && object.getInteger("limit") != null) {
            refMenu.setPageNum(object.getInteger("offset"));
            refMenu.setPageSize(object.getInteger("limit"));
        }
        JSONObject sort = object.getJSONObject("sort");
        if (sort != null) {
            String prop = sort.getString("prop");
            String order = sort.getString("order");
            refMenu.setProp(prop);
            refMenu.setOrder(order);
        }
        List<RefMenu> refMenuList = refMenuService.findPageList(refMenu);
        if (CollectionUtils.isEmpty(refMenuList)) {
            return JsonHelper.toJson(new ResultMsg(ResultCode.Success, new ArrayList<>(), 0));
        }
        //菜单进行转换
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success, refMenuService.menuToTreeZhList(refMenuList), refMenuService.countTotal(refMenu)));
    }

    /**
     * 启用/禁用
     *
     * @param object
     * @return
     */
    @RequestMapping(value = "/state", method = RequestMethod.POST)
    @ResponseBody
    public String state(@RequestBody JSONObject object, HttpServletRequest request) {
        String code = object.getString("code");
        if (StringUtils.isEmpty(code)) {
            return JsonHelper.toJson(new ResultMsg("300", "code不能为空"));
        }
        RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);

        RefMenu parmRefMenu = new RefMenu();
        parmRefMenu.setCode(code);
        RefMenu oneByParms = refMenuService.getOneByParms(parmRefMenu);
        if ("false".equals(oneByParms.getState())) {
            parmRefMenu.setState("true");
        } else {
            parmRefMenu.setState("false");
        }
        parmRefMenu.setUpdateby(loginRefUser.getCode());
        if (refMenuService.addOrUp(parmRefMenu) <= 0) {
            return JsonHelper.toJson(new ResultMsg("300", "修改状态失败"));
        }
        this.refLogAdminService.create(new RefLogAdmin(loginRefUser.getCode()
                , "用户管理页面", "修改状态", "state", "/refMenu/state"
                , parmRefMenu, "ok", loginRefUser.getOrgID()));
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success));
    }

}
