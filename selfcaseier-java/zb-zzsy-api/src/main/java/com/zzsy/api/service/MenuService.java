package com.zzsy.api.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zzsy.api.entity.Menu;
import com.zzsy.api.mapper.MenuMapper;

/**
 * 菜单操作接口
 * @author zzm
 *
 */
@Service
public class MenuService  {
	@Autowired
	private MenuMapper menumapper;

	/**
	 * 获取菜单
	 */
	
	public void allChildMenu(List<Menu> sl, Menu menu , String userId) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("userId", userId);
		List<Menu> slist = null;
		if (null == menu.getId()){
			map.put("pid", "-1");
			slist = this.menumapper.allChildMenu(map);
		}else{
			map.put("pid", menu.getId());
			slist = this.menumapper.allChildMenu(map);
		}
		menu.setSmenus(slist);
		if (null != menu.getId() && menu.getPid().equals("-1")){//只加入顶层的menu
			sl.add(menu);
		}
		
		if (null != slist && slist.size() > 0){
			menu.setHaveChild(1);
			for (Menu temp : slist){
				this.allChildMenu(sl, temp , userId);
			}
		}
	}
	
	/**
	 * 递归查询所有菜单
	 * @param sl
	 * @param menu
	 */
	
	public void allMenu(List<Menu> sl , Menu menu){
		Map<String, Object> map = new HashMap<String, Object>();
		List<Menu> slist = null;
		if (null == menu.getId()){
			map.put("pid", "-1");
			slist = this.menumapper.allMenu(map);
		}else{
			map.put("pid", menu.getId());
			slist = this.menumapper.allMenu(map);
		}
		menu.setSmenus(slist);
		if (null != menu.getId() && menu.getPid().equals("-1")){//只加入顶层的menu
			sl.add(menu);
		}
		if (null != slist && slist.size() > 0){
			for (Menu temp : slist){
				this.allMenu(sl , temp);
			}
		}		
	}

	/**
	 * 递归获取角色所拥有的菜单
	 */
	
	public void menufromrole(List<Menu> sl, Map<String, Object> map , Menu menu) {
		List<Menu> slist = null;
		map.put("id", menu.getId());
		if (null == map.get("id")){
			map.put("pid", "-1");
			slist = this.menumapper.menuFromRole(map);
		}else{
			map.put("pid", menu.getId());
			slist = this.menumapper.menuFromRole(map);
		}
		
		menu.setSmenus(slist);
		if (null != menu.getId() && menu.getPid().equals("-1")){//只加入顶层的menu
			sl.add(menu);
		}
		if (null != slist && slist.size() > 0){
			for (Menu temp : slist){
				this.menufromrole(sl, map, temp);
			}
		}
	}
	
	public void setParentNodeUnChecked(List<Menu> menuList){
		if(menuList==null || menuList.size()==0) return ;
		for(Menu menu:menuList){
			if(menu.getSmenus()!=null && menu.getSmenus().size()>0){
				if(menu.isChecked()){
					menu.setOpen(true);
					menu.setChecked(false);
				}
				setParentNodeUnChecked(menu.getSmenus());
			}
		}
	}

	/**
	 * 保存
	 */
	
	public void saveOrUpdate(Menu menu) {
		if (null != menu.getId() && !menu.getId().equals("")){
			this.menumapper.updateByPrimaryKeySelective(menu);
		}else{
			menu.setId(UUID.randomUUID().toString().replaceAll("-", ""));
			this.menumapper.insertSelective(menu);
		}
	}

	/**
	 * 删除
	 */
	
	public void delMenu(String id) {
		this.menumapper.deleteByPrimaryKey(id);
	}

	
	public Menu findOneMenu(String id) {
		// TODO Auto-generated method stub
		return this.menumapper.selectByPrimaryKey(id);
	}
}
