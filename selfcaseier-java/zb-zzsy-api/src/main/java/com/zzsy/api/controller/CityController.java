package com.zzsy.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.util.JsonHelper;
import com.zzsy.api.entity.City;
import com.zzsy.api.mapper.CityMapper;

/**
 * 城市控制器
 * @author hupei
 *
 */
@Controller
@RequestMapping(value = "/modulerolerelative/city")
public class CityController  {
	
	@Autowired
	private CityMapper cityMapper;
	@RequestMapping(value={"/list"})
	@ResponseBody
	public String list(@RequestBody  City city){
		List<City> list=cityMapper.findPageList(city);
		Long totalCount=cityMapper.countTotal(city);
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success,list,totalCount));
	}
}
