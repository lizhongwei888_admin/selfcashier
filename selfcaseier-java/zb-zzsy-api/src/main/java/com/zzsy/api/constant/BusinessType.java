package com.zzsy.api.constant;

/**
 * 申请类型
 * @author sl
 *
 */
public enum BusinessType  {

	LaborOrEquipment(1, "人工或者设备")
	;
	
	private Integer code;

	
	private String desc;

	BusinessType(Integer code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public Integer getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}
	
	public static String getDesc(Integer code) {
		
		BusinessType[] logTypes = values();
		for(BusinessType logType : logTypes) {
			if(code.equals(logType.getCode())) {
				return logType.getDesc();
			}
		}
		return "";
	}

}
