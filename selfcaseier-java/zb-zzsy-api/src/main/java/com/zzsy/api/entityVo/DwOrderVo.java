package com.zzsy.api.entityVo;

import com.zzsy.api.entity.DwOrder;
import com.zzsy.api.entity.DwOrderPaywaydetail;
import com.zzsy.api.entity.DwOrderShopdetail;

import java.util.List;

public class DwOrderVo {

    private DwOrder dwOrder;
    private List<DwOrderShopdetail> dwOrderShopdetails;

    private List<DwOrderPaywaydetail> dwOrderPaywaydetail;


    public DwOrder getDwOrder() {
        return dwOrder;
    }

    public void setDwOrder(DwOrder dwOrder) {
        this.dwOrder = dwOrder;
    }
    public List<DwOrderShopdetail> getDwOrderShopdetails() {
        return dwOrderShopdetails;
    }

    public void setDwOrderShopdetails(List<DwOrderShopdetail> dwOrderShopdetails) {
        this.dwOrderShopdetails = dwOrderShopdetails;
    }


    public List<DwOrderPaywaydetail> getDwOrderPaywaydetail() {
        return dwOrderPaywaydetail;
    }

    public void setDwOrderPaywaydetail(List<DwOrderPaywaydetail> dwOrderPaywaydetail) {
        this.dwOrderPaywaydetail = dwOrderPaywaydetail;
    }

}
