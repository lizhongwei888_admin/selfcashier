package com.zzsy.api.constant;

/**
 * 集卡调度-指令描述,指令名
 */
public enum InstructionNameEnum  {

	INTERNAL_EMPTY( "701" , "内集卡放空箱"),
	INTERNAL_FULL( 702+"" , "内集卡拖重箱"),
	INTERNAL_PULL_EMPTY( 705+"" , "内集卡拖空箱"),
	EXTERNAL_EMPTY( 703+"" , "外集卡拖空箱"),
	EXTERNAL_FULL( 704+"" , "外集卡拖重箱") ;
	
	private String code;

	private String desc;

	InstructionNameEnum(String code , String desc) {
		this.code = code;
		this.desc = desc;
	}

	public String getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}
	
	public static String getDesc(String code) {
		
		InstructionNameEnum[] logTypes = values();
		for(InstructionNameEnum logType : logTypes) {
			if(code.equals(logType.getCode())) {
				return logType.getDesc();
			}
		}
		return "";
	}

}
