package com.zzsy.api.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.util.JsonHelper;
import com.lujie.common.util.MD5Util;
import com.zzsy.api.entity.RefLogAdmin;
import com.zzsy.api.entity.RefUser;
import com.zzsy.api.service.RefLogAdminService;
import com.zzsy.api.service.RefUserService;
import com.zzsy.api.util.RedisRefUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.sql.Ref;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * 系统-后台用户控制器
 */
@Controller
@RequestMapping(value = "/refUser")
public class RefUserController {

    @Autowired
    private RefUserService refUserService;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private RefLogAdminService refLogAdminService;

    /**
     * 添加用户
     *
     * @param request
     * @param refUser
     * @return
     */
    @RequestMapping(value = "/addRefUser", method = RequestMethod.POST)
    @ResponseBody
    public String addRefUser(HttpServletRequest request, @RequestBody RefUser refUser) {
        if (!StringUtils.isEmpty(refUser.getCode())) {
            return JsonHelper.toJson(new ResultMsg("300", "添加失败"));
        }
        RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);

        if (refUser.getAccount().length() < 5 || refUser.getAccount().length() > 32) {
            return JsonHelper.toJson(new ResultMsg("310", "用户名长度范围5-32字符"));
        }
        if (refUser.getPassword().length() < 6 || refUser.getPassword().length() > 16) {
            return JsonHelper.toJson(new ResultMsg("340", "密码长度6-16范围内！"));
        }
        if (StringUtils.isEmpty(refUser.getOrgID())) {
            return JsonHelper.toJson(new ResultMsg("341", "orgID不能为空"));
        }
        RefUser parmsRefUser = new RefUser();
        parmsRefUser.setAccount(refUser.getAccount());
        parmsRefUser.setOrgID(refUser.getOrgID());
        RefUser dbRefUser = refUserService.getOneByParms(parmsRefUser);
        //查询用户
        if (dbRefUser != null) {
            return JsonHelper.toJson(new ResultMsg("342", "系统已存在该用户"));
        }

        refUser.setPassword(MD5Util.encrypt(refUser.getPassword()));

        refUser.setCreateby(loginRefUser.getCode());
        refUser.setCreate_time(new Date());
        refUser.setUpdateby(loginRefUser.getCode());
        refUser.setUpdate_time(new Date());

        if (refUserService.addOrUp(refUser) <= 0) {
            return JsonHelper.toJson(new ResultMsg("300", "添加失败"));
        }

        this.refLogAdminService.create(new RefLogAdmin(loginRefUser.getCode()
                , "用户页面", "添加用户", "addRefUser", "/refUser/addRefUser"
                , refUser, "ok", loginRefUser.getOrgID()));

        return JsonHelper.toJson(new ResultMsg(ResultCode.Success, refUser));
    }


    /**
     * 修改用户信息
     *
     * @param request
     * @param refUser
     * @return
     */
    @RequestMapping(value = "/upRefUser", method = RequestMethod.POST)
    @ResponseBody
    public String upRefUser(HttpServletRequest request, @RequestBody RefUser refUser) {
        if (StringUtils.isEmpty(refUser.getCode())) {
            return JsonHelper.toJson(new ResultMsg("300", "修改失败"));
        }
        RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
        //获得要修改的用户信息
        RefUser parmsRefUser = new RefUser();
        parmsRefUser.setCode(refUser.getCode());
        RefUser oneByParms = refUserService.getOneByParms(parmsRefUser);
        if (oneByParms == null) {
            return JsonHelper.toJson(new ResultMsg("340", "用户记录不存在!"));
        }
        if (!StringUtils.isEmpty(refUser.getAccount())) {
            if (refUser.getAccount().length() < 5 || refUser.getAccount().length() > 32) {
                return JsonHelper.toJson(new ResultMsg("310", "用户名长度范围5-32字符"));
            }
            //判断当前用户名是否不一致
            if (!oneByParms.getAccount().equals(refUser.getAccount())) {
                //判断是否账号冲突
                RefUser parmsRefUserOne = new RefUser();
                parmsRefUserOne.setAccount(refUser.getAccount());
                parmsRefUserOne.setOrgID(refUser.getOrgID());
                RefUser dbRefUser = refUserService.getOneByParms(parmsRefUser);
                //查询用户
                if (dbRefUser != null) {
                    return JsonHelper.toJson(new ResultMsg("342", "系统已存在该用户"));
                }
            }
        }
        //判断是否修改密码
        if (!StringUtils.isEmpty(refUser.getNewPassword())
                && !StringUtils.isEmpty(refUser.getOldPassword()) && !StringUtils.isEmpty(refUser.getConfirmNewPassword())) {
            if (refUser.getNewPassword().length() < 6 || refUser.getNewPassword().length() > 16) {
                return JsonHelper.toJson(new ResultMsg("340", "新密码长度6-16范围内！"));
            }
            if (refUser.getConfirmNewPassword().length() < 6 || refUser.getConfirmNewPassword().length() > 16) {
                return JsonHelper.toJson(new ResultMsg("340", "新密码长度6-16范围内！"));
            }
            if (refUser.getOldPassword().length() < 6 || refUser.getOldPassword().length() > 16) {
                return JsonHelper.toJson(new ResultMsg("340", "旧密码长度6-16范围内！"));
            }
            if (!refUser.getConfirmNewPassword().equals(refUser.getNewPassword())) {
                return JsonHelper.toJson(new ResultMsg("300", "新密码与确认密码不一致!"));
            }
            if (refUser.getOldPassword().equals(refUser.getNewPassword())) {
                return JsonHelper.toJson(new ResultMsg("300", "新旧密码值相同!"));
            }
            if (!oneByParms.getPassword().equals(MD5Util.encrypt(refUser.getOldPassword()))) {
                return JsonHelper.toJson(new ResultMsg("300", "旧密码值错误!"));
            }
            refUser.setPassword(MD5Util.encrypt(refUser.getNewPassword()));
        }

        refUser.setUpdateby(loginRefUser.getCode());
        refUser.setUpdate_time(new Date());


        if (refUserService.addOrUp(refUser) <= 0) {
            return JsonHelper.toJson(new ResultMsg("300", "修改失败"));
        }

        this.refLogAdminService.create(new RefLogAdmin(loginRefUser.getCode()
                , "用户页面", "修改用户信息", "upRefUser", "/refUser/upRefUser"
                , refUser, "ok", loginRefUser.getOrgID()));
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success, refUser));
    }


    /**
     * 对用户进行删除
     *
     * @param object
     * @return
     */
    @RequestMapping(value = "/delRefUser", method = RequestMethod.POST)
    @ResponseBody
    public String delRefUser(@RequestBody JSONObject object, HttpServletRequest request) {
        String codes = object.getString("codes");
        if (StringUtils.isEmpty(codes)) {
            return JsonHelper.toJson(new ResultMsg("300", "code不能为空"));
        }
        RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
        //判断是否删除的用户有自己
        Optional<String> result = Arrays.stream(codes.split(",")).filter(s -> s.equals(loginRefUser.getCode()))
                .findAny();
        // 找到任意一个符合条件的元素
        if (result.isPresent()) {
            return JsonHelper.toJson(new ResultMsg("300", "不能删除自己"));
        }
        //判断是否有admin用户
        List<RefUser> listByCodes = refUserService.getListByCodes(codes);
        if (CollectionUtils.isEmpty(listByCodes)) {
            return JsonHelper.toJson(new ResultMsg("300", "无用户信息"));
        }
        Optional<RefUser> resultOne = listByCodes.stream().filter(s -> s.getAccount().equals("admin")).findAny();
        if (resultOne.isPresent()) {
            return JsonHelper.toJson(new ResultMsg("300", "admin用户无法删除"));
        }
        refUserService.delete(codes);
        this.refLogAdminService.create(new RefLogAdmin(loginRefUser.getCode()
                , "用户页面", "删除", "delRefUser", "/refUser/delRefUser"
                , codes, "ok", loginRefUser.getOrgID()));
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success));
    }

    /**
     * 查询用户列表(分页)
     *
     * @param object
     * @return
     */
    @RequestMapping(value = "/listRefUser", method = RequestMethod.POST)
    @ResponseBody
    public String listRefUser(@RequestBody JSONObject object) {
        RefUser refUser = JSONObject.parseObject(JSON.toJSONString(object.getJSONObject("params")), RefUser.class);
        refUser.setPageNum(object.getInteger("offset"));
        refUser.setPageSize(object.getInteger("limit"));
        JSONObject sort = object.getJSONObject("sort");
        String prop = sort.getString("prop");
        String order = sort.getString("order");
        refUser.setProp(prop);
        refUser.setOrder(order);

        return JsonHelper.toJson(new ResultMsg(ResultCode.Success, refUserService.findPageList(refUser), refUserService.countTotal(refUser)));
    }

    /**
     * 启用/禁用
     *
     * @param object
     * @return
     */
    @RequestMapping(value = "/state", method = RequestMethod.POST)
    @ResponseBody
    public String state(@RequestBody JSONObject object, HttpServletRequest request) {
        String code = object.getString("code");
        if (StringUtils.isEmpty(code)) {
            return JsonHelper.toJson(new ResultMsg("300", "code不能为空"));
        }
        RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
        RefUser parmRefUser = new RefUser();
        parmRefUser.setCode(code);
        RefUser oneByParms = refUserService.getOneByParms(parmRefUser);
        if ("false".equals(oneByParms.getState())) {
            parmRefUser.setState("true");
        } else {
            parmRefUser.setState("false");
        }
        parmRefUser.setUpdateby(loginRefUser.getCode());
        if (refUserService.addOrUp(parmRefUser) <= 0) {
            return JsonHelper.toJson(new ResultMsg("300", "修改状态失败"));
        }
        this.refLogAdminService.create(new RefLogAdmin(loginRefUser.getCode()
                , "用户管理页面", "修改状态", "state", "/refUser/state"
                , parmRefUser, "ok", loginRefUser.getOrgID()));
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success));
    }

    /**
     * 重置密码为123456
     *
     * @param object
     * @return
     */
    @RequestMapping(value = "/reset", method = RequestMethod.POST)
    @ResponseBody
    public String reset(@RequestBody JSONObject object, HttpServletRequest request) {
        String code = object.getString("code");
        if (StringUtils.isEmpty(code)) {
            return JsonHelper.toJson(new ResultMsg("300", "code不能为空"));
        }
        RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
        RefUser parmRefUser = new RefUser();
        parmRefUser.setCode(code);
        //重置密码123456
        parmRefUser.setPassword(MD5Util.encrypt("123456"));
        parmRefUser.setUpdateby(loginRefUser.getCode());
        if (refUserService.addOrUp(parmRefUser) <= 0) {
            return JsonHelper.toJson(new ResultMsg("300", "重置失败"));
        }
        this.refLogAdminService.create(new RefLogAdmin(loginRefUser.getCode()
                , "用户管理页面", "重置密码", "reset", "/refUser/reset"
                , parmRefUser, "ok", loginRefUser.getOrgID()));
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success));
    }

    /**
     * 获得当前登录人的信息
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/userInfo", method = RequestMethod.POST)
    @ResponseBody
    public String userInfo(HttpServletRequest request) {
        RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
        RefUser parmRefUser = new RefUser();
        parmRefUser.setCode(loginRefUser.getCode());
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success, refUserService.getOneByParms(parmRefUser)));
    }

}
