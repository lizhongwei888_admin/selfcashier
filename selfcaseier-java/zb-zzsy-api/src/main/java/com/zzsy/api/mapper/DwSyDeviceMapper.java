package com.zzsy.api.mapper;

import com.lujie.common.dao.BasicMapper;
import com.zzsy.api.entity.DwSyDevice;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DwSyDeviceMapper extends BasicMapper{

    DwSyDevice findByDeviceCode(@Param("p") DwSyDevice d);

    void insertData(@Param("p") DwSyDevice d);
}