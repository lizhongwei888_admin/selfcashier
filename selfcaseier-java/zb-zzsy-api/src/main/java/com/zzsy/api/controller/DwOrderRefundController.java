package com.zzsy.api.controller;

import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.util.JsonHelper;
import com.zzsy.api.entity.DwOrder;
import com.zzsy.api.entity.DwOrderPaywaydetail;
import com.zzsy.api.entity.DwOrderRefund;
import com.zzsy.api.entity.RefUser;
import com.zzsy.api.service.DwOrderPaywaydetailService;
import com.zzsy.api.service.DwOrderRefundService;
import com.zzsy.api.service.DwOrderService;
import com.zzsy.api.service.RefLogAdminService;
import com.zzsy.api.util.RedisRefUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
@RequestMapping(value = "/dwOrderRefund")
public class DwOrderRefundController {

	@Autowired
	private DwOrderRefundService dwOrderRefundService;

	@RequestMapping(value={"/refund"})
	@ResponseBody
	public ResultMsg refund(HttpServletRequest request, @RequestBody Map<String,Object> data){
		if(!data.containsKey("code")){
			return new ResultMsg("302","请传入订单编号");
		}
		return dwOrderRefundService.insertData(request,data);
	}

	@RequestMapping(value={"/getList"})
	@ResponseBody
	public String getList(@RequestBody DwOrderRefund data){
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success,dwOrderRefundService.findPageList(data),dwOrderRefundService.countTotal(data)));
	}
}
