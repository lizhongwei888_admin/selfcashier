package com.zzsy.api.entity;

import com.lujie.common.entity.BasicEntity;

public class DwSyBags extends BasicEntity {
    private String code;
    private String shopId;

    private String merchantID;

    private String  bagsCode;
    private String  bagsName;



    private int  price;
    private String  sort;
    private String state;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getBagsCode() {
        return bagsCode;
    }

    public void setBagsCode(String bagsCode) {
        this.bagsCode = bagsCode;
    }

    public String getBagsName() {
        return bagsName;
    }

    public void setBagsName(String bagsName) {
        this.bagsName = bagsName;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getMerchantID() {
        return merchantID;
    }

    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }
}
