package com.zzsy.api.job;

import com.zzsy.api.service.SseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * 系统启动后，需要自动运行的程序
 *  
 */
@Component
public class SysStartRunUserAuthApplication implements ApplicationRunner {
	private final static Logger log = LoggerFactory.getLogger(SysStartRunUserAuthApplication.class);
	
	@Autowired
	private SseService sseService;

	private volatile boolean flag = true;
	
	private Long popTime = 30000L;
	
	@Override
    public void run(ApplicationArguments args) throws Exception {
		String startparam="sys start param:";
		String[] sourceArgs = args.getSourceArgs();
        for (String arg : sourceArgs) {
        	startparam+=arg + ",";
        }
        
        log.info(startparam+",系統启动完成");

        startSseSerive();
	}
	
	public void startSseSerive() {
		log.info("SSE服务  ready");

		Runnable r = new Runnable() {
			@Override
			public void run() {
				try {
					sendSse();
				} catch (Exception e) {
					// TODO 自动生成的 catch 块
					e.printStackTrace();
				}
			}
		};
		new Thread(r).start();
		log.info("SSE服务  start");
	}
	

	public void sendSse() throws Exception {

	}

	
	
	
}