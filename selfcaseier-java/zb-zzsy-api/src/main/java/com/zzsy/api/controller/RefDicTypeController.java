package com.zzsy.api.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.util.JsonHelper;
import com.zzsy.api.entity.RefDicData;
import com.zzsy.api.entity.RefDicType;
import com.zzsy.api.entity.RefUser;
import com.zzsy.api.service.RefDicDataService;
import com.zzsy.api.service.RefDicTypeService;
import com.zzsy.api.util.RedisRefUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 字典类型管理
 */
@Controller
@RequestMapping(value = "/refDicType")
public class RefDicTypeController {

    @Autowired
    private RefDicTypeService refDicTypeService;
    @Autowired
    private RefDicDataService refDicDataService;
    @Autowired
    private RedisTemplate redisTemplate;


    @RequestMapping(value = "/addRefDicType", method = RequestMethod.POST)
    @ResponseBody
    public String addRefDicType(HttpServletRequest request, @RequestBody RefDicType refDicType) {
        RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
        refDicType.setCreateby(loginRefUser.getCode());
        refDicType.setCreate_time(new Date());
        refDicType.setUpdateby(loginRefUser.getCode());
        refDicType.setUpdate_time(new Date());
        if (refDicTypeService.addOrUp(refDicType) <= 0) {
            return JsonHelper.toJson(new ResultMsg("300", "添加失败"));
        }
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success, refDicType));

    }

    @RequestMapping(value = "/upRefDicType", method = RequestMethod.POST)
    @ResponseBody
    public String upRefDicType(HttpServletRequest request, @RequestBody RefDicType refDicType) {
        if (StringUtils.isEmpty(refDicType.getCode())) {
            return JsonHelper.toJson(new ResultMsg("300", "code不能为空"));
        }
        RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
        refDicType.setUpdateby(loginRefUser.getCode());
        refDicType.setUpdate_time(new Date());
        if (refDicTypeService.addOrUp(refDicType) <= 0) {
            return JsonHelper.toJson(new ResultMsg("300", "修改失败"));
        }
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success, refDicType));

    }

    /**
     * 删除
     *
     * @param object
     * @return
     */
    @RequestMapping(value = "/delRefDicType", method = RequestMethod.POST)
    @ResponseBody
    public String delRefDicType(@RequestBody JSONObject object) {
        String codes = object.getString("codes");
        if (StringUtils.isEmpty(codes)) {
            return JsonHelper.toJson(new ResultMsg("300", "code不能为空"));
        }
        //判断下是否有下一级数据 有不能删
        Set<String> collect = Arrays.stream(codes.split(",")).collect(Collectors.toSet());
        RefDicData refDicData = new RefDicData();
        refDicData.setCodeSet(collect);
        if (refDicDataService.countTotal(refDicData) > 0L) {
            return JsonHelper.toJson(new ResultMsg("300", "存在下一级数据，无法删除"));
        }
        refDicTypeService.delete(codes);
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success));
    }

    /**
     * 后端分页查询集合
     *
     * @param object
     * @return
     */
    @RequestMapping(value = "/listRefDicType", method = RequestMethod.POST)
    @ResponseBody
    public String listRefDicType(@RequestBody JSONObject object) {
        RefDicType refDicType = JSONObject.parseObject(JSON.toJSONString(object.getJSONObject("params")), RefDicType.class);
        if (object.getInteger("offset") != null && object.getInteger("limit") != null) {
            refDicType.setPageNum(object.getInteger("offset"));
            refDicType.setPageSize(object.getInteger("limit"));
        }
        JSONObject sort = object.getJSONObject("sort");
        if (sort != null) {
            String prop = sort.getString("prop");
            String order = sort.getString("order");
            refDicType.setProp(prop);
            refDicType.setOrder(order);
        }
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success, refDicTypeService.findPageList(refDicType), refDicTypeService.countTotal(refDicType)));
    }

    /**
     * 获得一条记录
     *
     * @param refDicType
     * @return
     */
    @RequestMapping(value = "/findOneRefDicType", method = RequestMethod.POST)
    @ResponseBody
    public String findOneRefDicType(@RequestBody RefDicType refDicType) {
        if (StringUtils.isEmpty(refDicType.getCode())) {
            return JsonHelper.toJson(new ResultMsg("300", "code参数不能为空"));
        }
        RefDicType oneRefDicType = refDicTypeService.findOneRefDicType(refDicType);
        if (oneRefDicType == null) {
            return JsonHelper.toJson(new ResultMsg("300", "未查到记录"));
        }

        return JsonHelper.toJson(new ResultMsg(ResultCode.Success, oneRefDicType));
    }
}
