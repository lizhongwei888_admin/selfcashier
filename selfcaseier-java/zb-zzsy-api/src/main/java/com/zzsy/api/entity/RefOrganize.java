package com.zzsy.api.entity;

import com.lujie.common.entity.BasicEntity;

import java.io.Serializable;

/**
 * 组织管理
 */
public class RefOrganize extends BasicEntity implements Serializable {

    private static final long serialVersionUID = -301062307193761449L;

    //唯一标识
    private Long code;
    //机构名称
    private String orgName;
    //上级机构ID
    private Long orgPid;
    //电话
    private String phone;
    //地址
    private String orgUrl;
    //备注
    private String remark;
    //二级域名
    private String domain;
    //集团账号：true是；false否
    private String isGroup;
    //组织图片
    private String orgImage;
    //状态：true启用；false禁用
    private String state;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public Long getOrgPid() {
        return orgPid;
    }

    public void setOrgPid(Long orgPid) {
        this.orgPid = orgPid;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getOrgUrl() {
        return orgUrl;
    }

    public void setOrgUrl(String orgUrl) {
        this.orgUrl = orgUrl;
    }

    @Override
    public String getRemark() {
        return remark;
    }

    @Override
    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getIsGroup() {
        return isGroup;
    }

    public void setIsGroup(String isGroup) {
        this.isGroup = isGroup;
    }

    public String getOrgImage() {
        return orgImage;
    }

    public void setOrgImage(String orgImage) {
        this.orgImage = orgImage;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
