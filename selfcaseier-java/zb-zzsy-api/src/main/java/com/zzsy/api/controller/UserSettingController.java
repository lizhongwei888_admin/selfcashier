package com.zzsy.api.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lujie.common.entity.EntityMap;
import com.lujie.common.util.JsonHelper;
import com.lujie.common.util.StringUtil;
import com.zzsy.api.service.UserSettingService;

/**
 * 角色控制器
 * @author hupei
 *
 */
@Controller
@RequestMapping(value = "/usersetting")
public class UserSettingController  {
	
	@Autowired
	private UserSettingService userSettingService;
	
	@RequestMapping(value={"/findAll"})
	@ResponseBody
	public String findAll(@RequestBody EntityMap entityMap){
		if(StringUtil.isEmpty(entityMap.getStr("userId"))){
			JsonHelper.toJsonResultMsg("301","usId不能为空");//user id
		}
		return JsonHelper.toJsonResultMsg(userSettingService.findList(entityMap));
	}
	
	@RequestMapping(value={"/list"})
	@ResponseBody
	public String list(@RequestBody EntityMap entityMap){
		entityMap.setRowIndex();
		List list=userSettingService.findPageList(entityMap);
		Long totalCount=userSettingService.countTotal(entityMap);
		return JsonHelper.toJsonResultMsg(list,totalCount);
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ResponseBody
	public String add(HttpServletRequest request,@RequestBody EntityMap entityMap){
		return JsonHelper.toJson(userSettingService.save(request, entityMap));
	}
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public String update(HttpServletRequest request,@RequestBody EntityMap entityMap){
		return JsonHelper.toJson(userSettingService.update(request, entityMap));
	}
	@RequestMapping(value = "/detail", method = RequestMethod.POST)
	@ResponseBody
	public String detail(HttpServletRequest request,@RequestBody EntityMap entityMap){
		return JsonHelper.toJsonResultMsg(userSettingService.findOne(entityMap));
	}
	
	
}
