package com.zzsy.api.service;

import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.service.BasicService;
import com.lujie.common.util.DateUtil;
import com.lujie.common.util.JsonHelper;
import com.lujie.common.util.StringUtil;
import com.zzsy.api.entity.*;
import com.zzsy.api.mapper.DwOrderMapper;
import com.zzsy.api.mapper.DwOrderPaywaydetailMapper;
import com.zzsy.api.mapper.DwOrderRefundMapper;
import com.zzsy.api.mapper.DwOrderShopdetailMapper;
import com.zzsy.api.util.RedisRefUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Service
public class DwOrderRefundService extends BasicService {
	public static final Logger log = LoggerFactory.getLogger(DwOrderRefundService.class);

	@Autowired
	private DwOrderRefundMapper dwOrderRefundMapper;

	@Autowired
	private DwOrderMapper dwOrderMapper;

	@Autowired
	private DwOrderShopdetailMapper dwOrderShopdetailMapper;

	@Autowired
	DwOrderRefundDetailService dwOrderRefundDetailService;

	@Autowired
	DwOrderPaywaydetailMapper dwOrderPaywaydetailMapper;

	@Autowired
	private RefLogAdminService refLogAdminService;


	@Autowired
	private InterFaceService interFaceService;

	@Autowired
	private RedisTemplate redisTemplate;

	@Resource(name = "dwOrderRefundMapper")
	public void initBaseDao(DwOrderRefundMapper dwOrderRefundMapper) {
		super.setBaseMapper(dwOrderRefundMapper);
	}

	public ResultMsg insertData(HttpServletRequest request,Map<String,Object> mapOrder){

		RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
		String orderCode = mapOrder.get("code").toString();
		DwOrder order = new DwOrder();
		order.setCode(orderCode);
		order = (DwOrder)dwOrderMapper.findOne(order);

		if(order == null){
			return new ResultMsg("302","订单不存在");
		}

		String code = UUID.randomUUID().toString();
		DwOrderRefund dwOrderRefund = new DwOrderRefund();
		dwOrderRefund.setCode(code);
		dwOrderRefund.setOrderId(orderCode);
		dwOrderRefund.setUserId(order.getUserId());
		dwOrderRefund.setUserCode(order.getUserCode());
		dwOrderRefund.setUserPhone(order.getUserPhone());
		dwOrderRefund.setUserName(order.getUserName());
		dwOrderRefund.setRefundNo(new Date().getTime()+"");
		dwOrderRefund.setOrderState(1);
		dwOrderRefund.setGoodTMoney(order.getGoodTMoney());
		dwOrderRefund.setPromotTMoney(order.getPromotTMoney());
		dwOrderRefund.setActualTMoney(order.getActualTMoney());
		dwOrderRefund.setSaleTime(order.getSaleTime());
		dwOrderRefund.setShopId(order.getShopId());
		dwOrderRefund.setShopName(order.getShopName());
		dwOrderRefund.setCashID(order.getCashID());
		dwOrderRefund.setMerchantId(order.getMerchantId());
		dwOrderRefund.setErpState(1);
		dwOrderRefund.setReason(mapOrder.containsKey("reason")?StringUtil.getStr(mapOrder.get("reason")):"");
		dwOrderRefund.setCreate_time(DateUtil.getNowTime());
		dwOrderRefund.setCreateby(loginRefUser.getCode());
		dwOrderRefund.setOrgID(order.getOrgID());
		dwOrderRefundMapper.insertData(dwOrderRefund);
		//订单详情退款
		List<DwOrderShopdetail> dwOrderShopdetailList = dwOrderShopdetailMapper.findListByOderId(orderCode);
		for (DwOrderShopdetail dwOrderShopdetail:dwOrderShopdetailList) {
			DwOrderRefunddetail dwOrderRefunddetail = new DwOrderRefunddetail() ;
			dwOrderRefunddetail.setCode(UUID.randomUUID().toString());
			dwOrderRefunddetail.setRefundId(code);
			dwOrderRefunddetail.setGoodId(dwOrderShopdetail.getGoodId());
			dwOrderRefunddetail.setGoodCode(dwOrderShopdetail.getGoodCode());
			dwOrderRefunddetail.setGoodName(dwOrderShopdetail.getGoodName());
			dwOrderRefunddetail.setGoodSpecs(dwOrderShopdetail.getGoodSpecs());
			dwOrderRefunddetail.setGoodMoney(dwOrderShopdetail.getGoodMoney());
			dwOrderRefunddetail.setPromotMoney(dwOrderShopdetail.getPromotMoney());
			dwOrderRefunddetail.setActualMoney(dwOrderShopdetail.getActualMoney());
			dwOrderRefunddetail.setGoodState(2);
			dwOrderRefunddetail.setRefundNum(dwOrderShopdetail.getGoodNum());
			dwOrderRefunddetail.setOrderDetailId(dwOrderShopdetail.getCode());
			dwOrderRefunddetail.setOrgID(dwOrderRefund.getOrgID());
			dwOrderRefunddetail.setCreate_time(DateUtil.getNowTime());
			dwOrderRefunddetail.setCreateby(loginRefUser.getCode());
			dwOrderRefundDetailService.insertData(dwOrderRefunddetail);
		}
		Integer isReturnSucc = 6;
		//订单支付详情退款
		List<DwOrderPaywaydetail> dwOrderPaywaydetails = dwOrderPaywaydetailMapper.findListByOderId(orderCode,"1");
		for (DwOrderPaywaydetail dwOrderPaywaydetail:dwOrderPaywaydetails) {

			Map<String,Object> map = new HashMap<>();
			map.put("orgID",dwOrderRefund.getOrgID());
			//缺少参数
			//待真机测试时补上
			Map<String,Object> dataMap = new HashMap<>();
			dataMap.put("storeCode",order.getShopCode());
			dataMap.put("transDate",DateUtil.formatDate(new Date(),"yyyyMMddHHmmss"));
			dataMap.put("posId",order.getCashCode());
			dataMap.put("listNo",UUID.randomUUID().toString().replaceAll("-",""));
			dataMap.put("cashier","0031");
			dataMap.put("orderNo",dwOrderPaywaydetail.getPayNo());
			dataMap.put("refundNo",dwOrderPaywaydetail.getPayNo());
			dataMap.put("transAmt",dwOrderPaywaydetail.getPayActualMoney());
			map.put("data",dataMap);
			String str = interFaceService.orderRefund(map);

			try{
				if(!str.equals("")) {
					Map<String, Object> res = JsonHelper.toObject(str, Map.class);


					if(StringUtil.getStr(res.get("retCode")).equals("00") && StringUtil.getStr(res.get("resultCode")).equals("SUCCESS")){
						//退款成功
						dwOrderPaywaydetail.setIsReturnSucc(1);
					}else{
						//退款失败
						dwOrderPaywaydetail.setIsReturnSucc(2);
						isReturnSucc = 7;
					}
					dwOrderPaywaydetail.setReturnJsonPay(str);
					dwOrderPaywaydetail.setRequestJsonPay(StringUtil.getStr(res.get("requestJsonPay")));
				}
			}catch (Exception e){

			}
			dwOrderPaywaydetail.setCode(UUID.randomUUID().toString().replaceAll("-",""));
			dwOrderPaywaydetail.setType("2");
			dwOrderPaywaydetail.setOrderId(code);
			dwOrderPaywaydetail.setCreate_time(DateUtil.getNowTime());
			dwOrderPaywaydetail.setCreateby(loginRefUser.getCode());
			dwOrderPaywaydetailMapper.insertData(dwOrderPaywaydetail);

		}
		//修改订单状态
		dwOrderMapper.refundOrderState(isReturnSucc,orderCode);

		//退款成功修改退货单状态
		if(isReturnSucc == 6)
			dwOrderRefundMapper.updateState(code,"2");

		//修改商品详情退款数量
		for (DwOrderShopdetail dwOrderShopdetail:dwOrderShopdetailList) {
			dwOrderShopdetail.setUpdateby(loginRefUser.getCode());
			dwOrderShopdetailMapper.updateRefund(dwOrderShopdetail);
		}

		return new ResultMsg(ResultCode.Success,code);
	}

}
