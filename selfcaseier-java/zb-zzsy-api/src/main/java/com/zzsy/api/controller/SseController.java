package com.zzsy.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import com.zzsy.api.service.SseService;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/sse")
public class SseController {

	@Autowired
	private SseService sseService;
	
	 /**
     * 创建SSE连接
     *
     * @return
     */
    @GetMapping(path = "/connect/{userKey}")
    public SseEmitter sse(@PathVariable("userKey")String userKey,HttpServletRequest request ) {
       
        return  sseService.connect(userKey);
        
      
    }

}
