package com.zzsy.api.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.lujie.common.dao.BasicMapper;
import com.zzsy.api.entity.Menu;
import com.zzsy.api.entity.User;
@Repository
public interface MenuMapper extends BasicMapper{
	
	int deleteByPrimaryKey(String id);

    int insertSelective(Menu record);

    Menu selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Menu record);
    
    List<Menu> allChildMenu(Map<String, Object> map);
    List<Menu> allMenu(Map<String, Object> map);
    
    List<Menu> menuFromRole(Map<String, Object> map);
    
    List<Menu> findByUser(@Param("p") User user);
    
}