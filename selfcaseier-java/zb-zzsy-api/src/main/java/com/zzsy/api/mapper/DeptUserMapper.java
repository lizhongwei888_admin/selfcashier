package com.zzsy.api.mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.lujie.common.dao.BasicMapper;
import com.lujie.common.util.SerializableList;
import com.zzsy.api.entity.DeptUser;

public interface DeptUserMapper extends BasicMapper{
    int deleteByPrimaryKey(Integer id);

    int insertSelective(DeptUser record);

    DeptUser selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(DeptUser record);
    
    List<DeptUser> selectFromUidDid(Map<String, Object> map);
    
    SerializableList<DeptUser> selectByUid_sync(Map<String, Object> map);

	List<DeptUser> findAll();

	List<DeptUser> findDeptUserByUid(String userid);
	
	List<HashMap> findDirectDeptByUid(String userId);

}