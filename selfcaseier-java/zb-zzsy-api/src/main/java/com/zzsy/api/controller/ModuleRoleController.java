package com.zzsy.api.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.util.JsonHelper;
import com.zzsy.api.constant.KeysManager;
import com.zzsy.api.entity.ModuleRole;
import com.zzsy.api.entity.ModuleRoleRelative;
import com.zzsy.api.entity.ModuleType;
import com.zzsy.api.entity.User;
import com.zzsy.api.mapper.ModuleTypeMapper;
import com.zzsy.api.service.ModuleRoleRelativeService;
import com.zzsy.api.service.ModuleRoleService;
import com.zzsy.api.util.RedisUtil;

/**
 * 模块角色控制器
 * @author hupei
 *
 */
@Controller
@RequestMapping(value = "/modulerole")
public class ModuleRoleController  {
	@Autowired 
	private RedisTemplate redisTemplate;
	
	@Autowired
	private ModuleRoleService moduleRoleService;
	@Autowired
	private ModuleRoleRelativeService moduleRoleRelativeService;
	
	@Autowired
	private ModuleTypeMapper moduleTypeMapper;
	
	@RequestMapping(value={"/findAll"})
	@ResponseBody
	public String findAll(HttpServletRequest request,@RequestBody ModuleRole moduleRole){
		User loginUser = RedisUtil.getUser(redisTemplate, request);
		if(!KeysManager.isAdmin(loginUser.getUsername())){
			moduleRole.setUserId(loginUser.getId());
		}	
		List<ModuleRole> list=moduleRoleService.findList(moduleRole);
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success,list,Long.parseLong(list.size()+"")));
	}
	@RequestMapping(value={"/findAllModule"})
	@ResponseBody
	public String findAllModule(@RequestBody ModuleType moduleType){
		List<ModuleType> list=moduleTypeMapper.findList(moduleType);
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success,list,Long.parseLong(list.size()+"")));
	}
	/**
	 * 角色首页
	 * @return
	 */
	@RequestMapping(value={"/list"})
	@ResponseBody
	public String list(@RequestBody ModuleRole moduleRole){
		List<ModuleRole> list=moduleRoleService.findPageList(moduleRole);
		Long totalCount=moduleRoleService.countTotal(moduleRole);
		setModulelist(list);
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success,list,totalCount));
	}
	private void setModulelist(List<ModuleRole> list){
		if(list==null || list.size()==0) return;
		
		for(ModuleRole dbModuleRole:list){
			ModuleRoleRelative moduleRoleRelative=new ModuleRoleRelative();
			moduleRoleRelative.setModuleRoleId(dbModuleRole.getId());
			List<ModuleRoleRelative> moduleTypeList=moduleRoleRelativeService.findModules(moduleRoleRelative);
			setModulePath(moduleTypeList);
			dbModuleRole.setModules(moduleTypeList);
		}
	}
	private void setModulePath(List<ModuleRoleRelative> moduleTypeList){
		if(moduleTypeList==null || moduleTypeList.size()==0) return;
		
		for(ModuleRoleRelative moduleRoleRelative:moduleTypeList){
			moduleRoleRelative.setModuleRoleRelativePath("/modulerolerelative"+moduleRoleRelative.getModuleRoleRelativePath()+"/list");
		}
	}
	
	@RequestMapping(value={"/getModulesByRole"})
	@ResponseBody
	public String getModulesByRole(@RequestBody ModuleRole moduleRole){
		ModuleRoleRelative moduleRoleRelative=new ModuleRoleRelative();
		moduleRoleRelative.setModuleRoleId(moduleRole.getId());
		List<ModuleRoleRelative> moduleTypeList=moduleRoleRelativeService.findModules(moduleRoleRelative);
		Long totalCount=moduleTypeList==null?0L:Long.parseLong(moduleTypeList.size()+"");
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success,moduleTypeList,totalCount));
	}
	
	/**
	 * 批量保存角色
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ResponseBody
	public String add(@RequestBody ModuleRole moduleRole,HttpServletRequest request){
		return JsonHelper.toJson(moduleRoleService.save(moduleRole,request));
	}
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public String update(@RequestBody ModuleRole moduleRole,HttpServletRequest request){
		return JsonHelper.toJson(moduleRoleService.update(moduleRole,request));
	}
	
	/**
	 * 删除角色
	 * @param roleid
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public String remove(@RequestBody ModuleRole moduleRole,HttpServletRequest request){
		return JsonHelper.toJson(moduleRoleService.delete(moduleRole,request));
	}
	
	 
}
