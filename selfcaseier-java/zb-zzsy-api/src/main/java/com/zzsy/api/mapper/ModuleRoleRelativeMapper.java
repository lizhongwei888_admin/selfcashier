package com.zzsy.api.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.lujie.common.dao.BasicMapper;
import com.zzsy.api.entity.ModuleRoleRelative;
@Repository
public interface ModuleRoleRelativeMapper extends BasicMapper{
	List<ModuleRoleRelative> findModules(@Param("p")ModuleRoleRelative moduleRoleRelative);
	Integer updateByModuleRole(@Param("p")ModuleRoleRelative moduleRoleRelative);
}