package com.zzsy.api.entity;

import com.alibaba.fastjson.JSONArray;
import com.lujie.common.entity.BasicEntity;
import org.apache.commons.collections.CollectionUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * 第三方接口日志
 */
public class RefLogInterface extends BasicEntity implements Serializable {
    private static final long serialVersionUID = -301062307193761449L;

    //唯一标识
    private String code;
    //业务类型（会员查询、商品查询、销售促销、下单erp、支付
    private String type;
    //操作内容
    private String content;
    //请求方式
    private String payType;
    //方法名称
    private String method;
    //请求url地址
    private String url;
    //主机IP地址
    private String ip;
    //操作地点
    private String site;

    //请求参数
    private String requestParm;
    //返回结果
    private String returnParm;
    //状态：true启用；false禁用 默认开启
    private String state;
    /*---------------与数据库无关字段--------*/
    //时间搜索的范围数组
    private JSONArray timeSearch;
    private String startTime;
    private String endTime;
    /*---------------结束--------*/

    public RefLogInterface() {
    }

    public RefLogInterface(String createby, String type, String method, String url,
                           String requestParm, String returnParm,String site, String orgID) {
        this.type = type;
        this.content = type;
        this.payType = "post";
        this.method = method;
        this.url = url;
        this.site = site;
        this.requestParm = requestParm;
        this.returnParm = returnParm;
        this.state = "true";
        this.createby = createby;
        this.updateby = createby;
        Date nowDate = new Date();
        this.create_time = nowDate;
        this.update_time = nowDate;
        this.orgID = orgID;
    }

    public RefLogInterface(String createby, String type, String content, String payType, String method, String url, String ip,
                           String site, String requestParm, String returnParm, String orgID) {
        this.type = type;
        this.content = content;
        this.payType = payType;
        this.method = method;
        this.url = url;
        this.ip = ip;
        this.site = site;
        this.requestParm = requestParm;
        this.returnParm = returnParm;
        this.state = "true";
        this.createby = createby;
        this.updateby = createby;
        Date nowDate = new Date();
        this.create_time = nowDate;
        this.update_time = nowDate;
        this.orgID = orgID;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getRequestParm() {
        return requestParm;
    }

    public void setRequestParm(String requestParm) {
        this.requestParm = requestParm;
    }

    public String getReturnParm() {
        return returnParm;
    }

    public void setReturnParm(String returnParm) {
        this.returnParm = returnParm;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public JSONArray getTimeSearch() {
        return timeSearch;
    }

    public void setTimeSearch(JSONArray timeSearch) {
        this.timeSearch = timeSearch;
    }

    public String getStartTime() {
        if (CollectionUtils.isNotEmpty(this.timeSearch)) {
            return this.timeSearch.getString(0);
        }
        return "";
    }

    public void setStartTime(String startTime) {
        if (CollectionUtils.isNotEmpty(this.timeSearch)) {
            this.startTime = this.timeSearch.getString(0);
        }
    }

    public String getEndTime() {
        if (CollectionUtils.isNotEmpty(this.timeSearch)) {
            return this.timeSearch.getString(1);
        }
        return "";
    }

    public void setEndTime(String endTime) {
        if (CollectionUtils.isNotEmpty(this.timeSearch)) {
            this.endTime = this.timeSearch.getString(0);
        }
    }
}
