package com.zzsy.api.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.lujie.common.entity.BasicEntity;

import net.sf.json.JSONObject;

public class User extends BasicEntity implements Serializable {
	private static final long serialVersionUID = 3798144702687083181L;

	private String id;

	private String username;

	private String password;

	private String realname;

	private String code;

	private Integer isactive;

	private Date entrytime;

	private String s_entrytime;

	private String leader;

	private String email;

	private String info;

	private String telphone;

	private String mobilephone;

	private String weixin;

	private String qq;

	private Short sex;

	private Short age;

	private Date logintime;

	private String s_logintime;

	private Date outtime;

	private String s_outtime;

	private String usertype;//1登录了些的用户，2接口类型的用户

	private Date inserttime;

	private String s_inserttime;

	private Short isvalid;

	private Date lasttime;

	private String s_lasttime;

	private Short version;

	private String usersystemtype;

	private String reservearg1;

	private String reservearg2;

	private String reservearg3;// xufeng 使用 用作识别是否微信端操作

	private String unitName;

	private String unit;
	
	private String positionId;
	
	private String positionName;
	
	private boolean checked;

	private String usertypename;
	
	private String userSite;
	
	public String getUserSite() {
	    return userSite;
	}

	public void setUserSite(String userSite) {
	    this.userSite = userSite;
	}
	
	public String getUnit() {
	    return unit;
	}

	public void setUnit(String unit) {
	    this.unit = unit;
	}

	public String getPositionId() {
	    return positionId;
	}

	public void setPositionId(String positionId) {
	    this.positionId = positionId;
	}

	public String getPositionName() {
	    return positionName;
	}

	public void setPositionName(String positionName) {
	    this.positionName = positionName;
	}


	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	private List<DeptUser> deptUsers;
	
	private String roleIds;
	/** 角色的菜单包含的所有后台接口地址*/
	private String roleMenuBackendUrls;
	
	private List<UserModuleRole> userModuleRoles;
	
	private String moduleRoleIds;
	
	private String deptId;
	
	public String dataId;
	
	private Integer deptUserId;
	
	private String userKey;
	
	private String token;
	
	private Integer loginType;
	
	private String belongAreaType;
	
	private String belongAreaDesc;
	
	private String dataIds;
	
	private List<Menu> menuList;
	
	private JSONObject userdevice;
	
	private String wechatNo;
	private String wechatId;
	private String wechatNick;
	public String getUsertypename() {
		return usertypename;
	}

	public void setUsertypename(String usertypename) {
		this.usertypename = usertypename;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id == null ? null : id.trim();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username == null ? null : username.trim();
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password == null ? null : password.trim();
	}

	public String getRealname() {
		return realname;
	}

	public void setRealname(String realname) {
		this.realname = realname == null ? null : realname.trim();
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code == null ? null : code.trim();
	}

	public Integer getIsactive() {
		return isactive;
	}

	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	public Date getEntrytime() {
		return entrytime;
	}

	public void setEntrytime(Date entrytime) {
		this.entrytime = entrytime;
	}

	public String getLeader() {
		return leader;
	}

	public void setLeader(String leader) {
		this.leader = leader == null ? null : leader.trim();
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email == null ? null : email.trim();
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info == null ? null : info.trim();
	}

	public String getTelphone() {
		return telphone;
	}

	public void setTelphone(String telphone) {
		this.telphone = telphone == null ? null : telphone.trim();
	}

	public String getMobilephone() {
		return mobilephone;
	}

	public void setMobilephone(String mobilephone) {
		this.mobilephone = mobilephone == null ? null : mobilephone.trim();
	}

	public String getWeixin() {
		return weixin;
	}

	public void setWeixin(String weixin) {
		this.weixin = weixin == null ? null : weixin.trim();
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq == null ? null : qq.trim();
	}

	public Short getSex() {
		return sex;
	}

	public void setSex(Short sex) {
		this.sex = sex;
	}

	public Short getAge() {
		return age;
	}

	public void setAge(Short age) {
		this.age = age;
	}
	
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	public Date getLogintime() {
		return logintime;
	}

	public void setLogintime(Date logintime) {
		this.logintime = logintime;
	}

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	public Date getOuttime() {
		return outtime;
	}

	public void setOuttime(Date outtime) {
		this.outtime = outtime;
	}

	public String getUsertype() {
		return usertype;
	}

	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	public Date getInserttime() {
		return inserttime;
	}

	public void setInserttime(Date inserttime) {
		this.inserttime = inserttime;
	}

	public Short getIsvalid() {
		return isvalid;
	}

	public void setIsvalid(Short isvalid) {
		this.isvalid = isvalid;
	}

	public Date getLasttime() {
		return lasttime;
	}

	public void setLasttime(Date lasttime) {
		this.lasttime = lasttime;
	}

	public Short getVersion() {
		return version;
	}

	public void setVersion(Short version) {
		this.version = version;
	}

	public String getUsersystemtype() {
		return usersystemtype;
	}

	public void setUsersystemtype(String usersystemtype) {
		this.usersystemtype = usersystemtype;
	}

	public String getReservearg1() {
		return reservearg1;
	}

	public void setReservearg1(String reservearg1) {
		this.reservearg1 = reservearg1 == null ? null : reservearg1.trim();
	}

	public String getReservearg2() {
		return reservearg2;
	}

	public void setReservearg2(String reservearg2) {
		this.reservearg2 = reservearg2 == null ? null : reservearg2.trim();
	}

	public String getReservearg3() {
		return reservearg3;
	}

	public void setReservearg3(String reservearg3) {
		this.reservearg3 = reservearg3 == null ? null : reservearg3.trim();
	}

	public String getS_entrytime() {
		return s_entrytime;
	}

	public void setS_entrytime(String s_entrytime) {
		this.s_entrytime = s_entrytime;
	}

	public String getS_logintime() {
		return s_logintime;
	}

	public void setS_logintime(String s_logintime) {
		this.s_logintime = s_logintime;
	}

	public String getS_outtime() {
		return s_outtime;
	}

	public void setS_outtime(String s_outtime) {
		this.s_outtime = s_outtime;
	}

	public String getS_inserttime() {
		return s_inserttime;
	}

	public void setS_inserttime(String s_inserttime) {
		this.s_inserttime = s_inserttime;
	}

	public String getS_lasttime() {
		return s_lasttime;
	}

	public void setS_lasttime(String s_lasttime) {
		this.s_lasttime = s_lasttime;
	}

	public String getUserKey() {
		return userKey;
	}

	public void setUserKey(String userKey) {
		this.userKey = userKey;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public List<DeptUser> getDeptUsers() {
		return deptUsers;
	}

	public void setDeptUsers(List<DeptUser> deptUsers) {
		this.deptUsers = deptUsers;
	}

	public String getRoleIds() {
		return roleIds;
	}

	public void setRoleIds(String roleIds) {
		this.roleIds = roleIds;
	}

	public List<UserModuleRole> getUserModuleRoles() {
		return userModuleRoles;
	}

	public void setUserModuleRoles(List<UserModuleRole> userModuleRoles) {
		this.userModuleRoles = userModuleRoles;
	}

	public String getDeptId() {
		return deptId;
	}

	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}

	public Integer getLoginType() {
		return loginType;
	}

	public void setLoginType(Integer loginType) {
		this.loginType = loginType;
	}

	public Integer getDeptUserId() {
		return deptUserId;
	}

	public void setDeptUserId(Integer deptUserId) {
		this.deptUserId = deptUserId;
	}

	public String getModuleRoleIds() {
		return moduleRoleIds;
	}

	public void setModuleRoleIds(String moduleRoleIds) {
		this.moduleRoleIds = moduleRoleIds;
	}

	public String getDataIds() {
		return dataIds;
	}

	public void setDataIds(String dataIds) {
		this.dataIds = dataIds;
	}

	public String getRoleMenuBackendUrls() {
		return roleMenuBackendUrls;
	}

	public void setRoleMenuBackendUrls(String roleMenuBackendUrls) {
		this.roleMenuBackendUrls = roleMenuBackendUrls;
	}

	public String getBelongAreaType() {
		return belongAreaType;
	}

	public void setBelongAreaType(String belongAreaType) {
		this.belongAreaType = belongAreaType;
	}

	public String getBelongAreaDesc() {
		return belongAreaDesc;
	}

	public void setBelongAreaDesc(String belongAreaDesc) {
		this.belongAreaDesc = belongAreaDesc;
	}

	public String getDataId() {
		return dataId;
	}

	public void setDataId(String dataId) {
		this.dataId = dataId;
	}

	public List<Menu> getMenuList() {
		return menuList;
	}

	public void setMenuList(List<Menu> menuList) {
		this.menuList = menuList;
	}

	public JSONObject getUserdevice() {
		return userdevice;
	}

	public void setUserdevice(JSONObject userdevice) {
		this.userdevice = userdevice;
	}

	public String getWechatNo() {
		return wechatNo;
	}

	public void setWechatNo(String wechatNo) {
		this.wechatNo = wechatNo;
	}

	public String getWechatId() {
		return wechatId;
	}

	public void setWechatId(String wechatId) {
		this.wechatId = wechatId;
	}

	public String getWechatNick() {
		return wechatNick;
	}

	public void setWechatNick(String wechatNick) {
		this.wechatNick = wechatNick;
	}

	
	
	
	

	
	
	

}