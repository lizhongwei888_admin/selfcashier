package com.zzsy.api.interceptor;

import com.lujie.common.constant.HttpContentType;
import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.util.JsonHelper;
import com.lujie.common.util.MD5Util;
import com.lujie.common.util.StringUtil;
import com.lujie.common.util.StringUtils;
import com.zzsy.api.config.UserAuthConfig;
import com.zzsy.api.entity.RefUser;
import com.zzsy.api.service.InterfaceAccountService;
import com.zzsy.api.service.ModuleRoleRelativeService;
import com.zzsy.api.service.UserService;
import com.zzsy.api.util.JedisTool;
import com.zzsy.api.util.RedisRefUtil;
import com.zzsy.api.constant.KeysManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


/**
 * @author zzm 后台管理登录拦截器
 *
 */
public class SessionInterceptor extends HandlerInterceptorAdapter{

	private static final Logger log = LoggerFactory.getLogger(SessionInterceptor.class);

	@Autowired 
	private RedisTemplate redisTemplate;
	@Autowired
	private JedisTool jedisTool;
	
	@Autowired
	private ModuleRoleRelativeService moduleRoleRelativeService;
	
	@Autowired
	private InterfaceAccountService interfaceAccountService;
	@Autowired
	private UserService userService;
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		//String userToken="b8b59ff19fdb409a8a18246bff7923f1:77367ecedbed49fa975d93012657bf42";
		//RedisUtil.deleteMiddleLike(redisTemplate,RedisUtil.LoginUserToken+userToken);
		//RedisUtil.deleteMiddleLike(redisTemplate,RedisUtil.LoginUserToken);
		//RedisUtil.deleteMiddleLike(redisTemplate, com.lujie.userauth.constant.KeysManager.User_Auth);

		String urlPath = request.getServletPath();
		log.info("---请求url:"+urlPath);
		log.info("请求头信息："+HttpContentType.getRequestHeaderMap(request));
		
		
		String requestBody=HttpContentType.getParamStr(request);
		log.info("业务请求参数:"+requestBody);
		
		/*if(ConfigProperties.isAppUser(request)){//小程序
			ResultMsg msg= userService.checkWeChatSmallProgram(request);
			if(!msg.getCode().equals(ResultCode.Success.getCode().toString())){
				printMsg(response,msg);
				return false;
			}
		}*/

		//不需要登录、地址拦截
		if (notFilter(urlPath)) {
			return super.preHandle(request, response, handler);
		}


		//String token=RedisUtil.getToken(request);
		String token = RedisRefUtil.getToken(request);
		if (StringUtil.isEmpty(token)) {
			printMsg(response, new ResultMsg("0", "token不能为空"));
			return false;
		}

		//User user=RedisUtil.getUserByToken(redisTemplate, request);
		RefUser user = RedisRefUtil.getRefUserByToken(redisTemplate, request);
		if (user == null) {
			/*user=userService.findByToken(token);//通过配置用户token,免登录
			if(user==null){
				printMsg(response,new ResultMsg("0","token无效"));
				return false;
			}else{
				RedisUtil.saveUserByToken(redisTemplate, user);
			}*/
			printMsg(response, new ResultMsg("0", "token无效"));
			return false;
		}
		
		//对重复请求进行处理
		ResultMsg lockFormMsg = stopSameForm(request, user.getCode(), urlPath, requestBody);
		if(!lockFormMsg.getCode().equals(ResultCode.Success.getCode().toString())){
			printMsg(response, lockFormMsg);
			return false;
		}
		/*ResultMsg resultMsg=moduleRoleRelativeService.getUserAuth(request,user);//判断接口是否有权限访问模块
		if(!resultMsg.getCode().equals(ResultCode.Success.getCode().toString())){
			printMsg(response,resultMsg);
			removeSameFormLock(user.getId(),urlPath,requestBody); 
			return false;
		}*/

		//RedisUtil.updateLoginStatus(redisTemplate, request);//更新用户session时间
		RedisRefUtil.updateLoginStatus(redisTemplate, request);//更新用户session时间
		return super.preHandle(request, response, handler);
	}
	
	/**
	 * 防止重复提交
	 * @param urlPath
	 * @param requestBody
	 */
	private ResultMsg stopSameForm(HttpServletRequest request,
			String user,
			String urlPath,String requestBody){
		try{
			
			urlPath=urlPath.toLowerCase();
			if(!(urlPath.contains("/save")
					|| urlPath.contains("/add")
					|| urlPath.contains("/create")
					|| urlPath.contains("/new")
					|| urlPath.contains("/update")
					|| urlPath.contains("/interFace")
					|| urlPath.contains("/dw")
					|| (urlPath.contains("/up") && !urlPath.contains("/upload"))
					)){
				return new ResultMsg(ResultCode.Success);
			}
			String ip=StringUtils.getIpAddr(request);
			
			String lockKey= KeysManager.User_Auth+"FormLock"+user+""+urlPath+MD5Util.encrypt(requestBody);
			String lockIPKey=lockKey+ip;
			
			String lockIP=jedisTool.get(lockIPKey);
			if(lockIP!=null && !lockIP.equals("null")){
				return new ResultMsg("730","高频访问接口，请稍后再进行操作！");
            }
			Integer expireSecond=Integer.parseInt(UserAuthConfig.AccessLockTime.toString())/1000;
			
			if(jedisTool.getLock(lockKey, System.currentTimeMillis()+"")==0){
				String lockTime=jedisTool.get(lockKey);
				Long accessMinintervalTime=UserAuthConfig.AccessMinintervalTime;//每次访问接口间隔最短时间
				if (lockTime!=null && System.currentTimeMillis() < Long.parseLong(lockTime)+accessMinintervalTime) {
					log.info("ip:"+ip+","+urlPath);
					jedisTool.resetLock(lockKey, System.currentTimeMillis()+"");
					
					jedisTool.getLock(lockIPKey, System.currentTimeMillis()+"");
					//String bbb=jedisTool.get(lockIPKey);
					
					jedisTool.expire(lockIPKey, expireSecond);//60秒过期
					return new ResultMsg("720","高频访问接口，请稍后再进行操作！");
	            }
				
				
				if (lockTime!=null && System.currentTimeMillis() < Long.parseLong(lockTime)+UserAuthConfig.AccessLockTime) {
					return new ResultMsg("700","重复相同的请求，请稍后再进行操作！");
	            }else{
	            	jedisTool.unLock(lockKey);
	            	return new ResultMsg(ResultCode.Success);
	            }
			}
			jedisTool.expire(lockKey, expireSecond);//自动解锁时间
		}catch(Exception lockException){
			log.info("urlPath:"+urlPath+", requestBody:"+ requestBody);
			log.info(lockException.getMessage());
			return new ResultMsg("710","服务器表单数据锁异常");
		}
		return new ResultMsg(ResultCode.Success); 
	}
	private void removeSameFormLock(String user,String urlPath,String requestBody){
		try{
			
			urlPath=urlPath.toLowerCase();
			if(!(urlPath.contains("/save")
					|| urlPath.contains("/add")
					|| urlPath.contains("/create")
					|| urlPath.contains("/new")
					|| urlPath.contains("/update")
					|| urlPath.contains("/interFace")
					|| urlPath.contains("/dw")
					|| (urlPath.contains("/up") && !urlPath.contains("/upload"))
					)){
				return;
			}
			String lockKey= KeysManager.User_Auth+"FormLock"+user+MD5Util.encrypt(requestBody);
			
			jedisTool.unLock(lockKey);
		}catch(Exception lockException){
			log.info("urlPath:"+urlPath+", requestBody:"+ requestBody);
			log.info(lockException.getMessage()); 
		} 
	}
	
	private Boolean notFilter(String urlPath){
		String[] notFilter = new String[] 
				{ "favicon.ico",
						"/verifycode/getUserKey",
						"/verifycode/image",
						"/verifycode/getCode",
						"/admin/sendSmsCode",
						"/admin/checkmobile",
						"/admin/changepwdfromforget",
						"/admin/login",
						"/app/login",
						"/upload/file",
						"/interFace",
						"/dw",
						"/refAdmin/login"
				};
        
		Boolean notFilterFlag = false;
		for (String url : notFilter) {
			if ((urlPath.contains(url))) {
				notFilterFlag = true;
				break;
			}
		}
		return notFilterFlag;
	}
	private void printMsg(HttpServletResponse response,ResultMsg msg) throws IOException{
		response.setHeader("Content-type", "application/json; charset=utf-8");
		response.setContentType("application/json; charset=utf-8");
		PrintWriter out = response.getWriter();
    	String errormsg=JsonHelper.toJson(msg);
    	out.println(errormsg);
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		super.postHandle(request, response, handler, modelAndView);
		try {
			/*User user=getLoginUser(request);
			if(user!=null){
				String urlPath = request.getServletPath();
				String requestBody=HttpContentType.getParamStr(request);
				removeSameFormLock(user.getId(),urlPath,requestBody);
			}*/

			RefUser refUserByToken = RedisRefUtil.getRefUserByToken(redisTemplate, request);
			if (refUserByToken != null) {
				String urlPath = request.getServletPath();
				String requestBody = HttpContentType.getParamStr(request);
				removeSameFormLock(refUserByToken.getCode(), urlPath, requestBody);
			}
		}catch(Exception ex){
			ex.printStackTrace();
			log.info("postHandle:"+ex.getMessage());
			
		}
		
		
		
	}
	
	
	/*private User getLoginUser(HttpServletRequest request){
		User user=null;
		if(ConfigProperties.isInterfaceUser(request)){//如果是接口用户请求，则判断是否是有效的接口请求
        	user=interfaceAccountService.getInterfaceUser(request);
			
		}else {
			user=RedisUtil.getUserByToken(redisTemplate, request);
			if(user==null){
				ResultMsg msg=userService.checkNoPicLoginUse(request);
				if(msg.getCode().equals("0")){
					return null;
				}
				
				if(!msg.getCode().equals(ResultCode.Success.getCode().toString())){
					return null;
				}
				user=(User)msg.getData();
	        }  
		}
		return user;
	}*/

}
