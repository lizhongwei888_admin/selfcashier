package com.zzsy.api.mapper;

import com.lujie.common.dao.BasicMapper;
import com.zzsy.api.entity.RefApi;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RefApiMapper extends BasicMapper {

    List<RefApi> findAll(@Param("p")RefApi refApi );

    RefApi findOne(@Param("p")RefApi refApi);
}
