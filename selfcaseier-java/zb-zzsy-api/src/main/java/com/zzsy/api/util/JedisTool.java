package com.zzsy.api.util;


import java.util.List;

import javax.annotation.PostConstruct;

import com.zzsy.api.config.RedisConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.lujie.common.util.CollectionUtil;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

@Component
public class JedisTool {
 
    
    private static JedisPool jedisPool;
 
    @Autowired
    private RedisConfig redisConfig;
 
    /**
     * JedisPool 无法通过@Autowired注入，可能由于是方法bean的原因，此处可以先注入RedisConfig，
     * 然后通过@PostConstruct初始化的时候将factory直接赋给jedisPool
     */
    @PostConstruct
    public void init() {
        jedisPool = redisConfig.redisPoolFactory();
    }
 
    public static String get(String key) {
        Jedis jedis = jedisPool.getResource();//获取一个jedis实例
        String value = jedis.get(key);
        jedis.close();
        return value;
    }
    public static List<String> lrange(String key,long start,long stop) {
        Jedis jedis = jedisPool.getResource();//获取一个jedis实例
        List<String> list = jedis.lrange(key, start, stop);
        jedis.close();
        return list;
    }
    
    public static Long setIfNotExist(String key, String value) {
        Jedis jedis = null; 
 
        jedis = jedisPool.getResource();//获取一个jedis实例
        Long result=jedis.setnx(key,value);
        
        jedis.close(); 
        return result;
    }
    public static Long getLock(String key, String value) throws Exception {
        Jedis jedis = null; 
 
        jedis = jedisPool.getResource();//获取一个jedis实例
        Long result=jedis.setnx(key,value);
        jedis.close(); 
        return result;
    }
    public static Long resetLock(String key, String value) throws Exception {
        Jedis jedis = null;  
        jedis = jedisPool.getResource();
        jedis.del(key);
        Long result=jedis.setnx(key,value);
        jedis.close(); 
        return result;
    }
    public static Long unLock(String key) {
        Jedis jedis = jedisPool.getResource();//获取一个jedis实例
        Long result=jedis.del(key);
        jedis.close(); 
        return result;
    }
    public static Long expire(String key,int seconds){
    	Jedis jedis = jedisPool.getResource();//获取一个jedis实例
        Long result=jedis.expire(key, seconds);
        jedis.close();
        return result;
    }
    public static List<String> getQueueList(String key,Integer start,Integer stop) {
        Jedis jedis = jedisPool.getResource();
        List<String> result=jedis.lrange(key, start, stop);
        jedis.close();
        return result;
    }
    public static long removeDataFormQueue(String key,Integer count,String value) {
        Jedis jedis = jedisPool.getResource();
        long result=jedis.lrem(key, count, value);
        jedis.close();
        return result;
    }
    
    public static void removeAllDataFormQueue(String key,String value) {
       
    	List<String> datalist=JedisTool.getQueueList(key, 0, -1);
		if(CollectionUtil.isEmpty(datalist)) {
			return;
		}
		Jedis jedis = jedisPool.getResource();
       
		for(String data:datalist){
			if(value.equals(data)){
				long result=jedis.lrem(key, 1, value);
			}
		}
		jedis.close();
    }
    
    public static long appendToQueueEnd(String key,String value) {
        Jedis jedis = jedisPool.getResource();//获取一个jedis实例
        long result=jedis.rpush(key, value);
        jedis.close();
        return result;
    }
}
