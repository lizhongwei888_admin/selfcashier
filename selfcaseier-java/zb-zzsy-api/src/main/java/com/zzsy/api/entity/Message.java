package com.zzsy.api.entity;


public class Message {
	private int msgKey;
	private String userKey;

	public String getUserKey() {
		return userKey;
	}

	public void setUserKey(String userKey) {
		this.userKey = userKey;
	}

	public int getMsgKey() {
		return msgKey;
	}

	public void setMsgKey(int msgKey) {
		this.msgKey = msgKey;
	}


}
