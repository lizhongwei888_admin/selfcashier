package com.zzsy.api.entity;

import java.io.Serializable;

import com.lujie.common.entity.BaseEntity;

public class Log extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 3163315610662611965L;

	private String id;

	/**
	 * 用户id
	 */
	private String userId;

	/**
	 * 内容
	 */
	private String mark;
	/**
	 * IP地址
	 */
	private String ipAddress;
	/**
	 * 类型
	 */
	private String type;
	/**
	 * 子类型
	 */
	private String subType;
	/**
	 * 操作类型 0:增 1:改 2:删
	 */
	private Integer operType;
	/**
	 * 操作状态 0:成功，1:失败
	 */
	private Integer operStatus;

	public Log() {
	}

	/**
	 * 
	 * @param userId
	 * @param mark
	 * @param type
	 * @param subType
	 * @param operType
	 * @param operStatus
	 */
	public Log(String userId, String mark, String type, String subType, Integer operType, Integer operStatus) {
		this.userId = userId; //用户id
		this.mark = mark;     //日志内容
		//this.ipAddress = ipAddress;
		this.type = type;     //日志类型
		this.subType = subType; //日志子类型
		this.operType = operType; //操作类型 0:新增 1:修改 2:删除 3:查询 4:导入 5:导出
		this.operStatus = operStatus; //操作状态 0:成功，1:失败
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId == null ? null : userId.trim();
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	public Integer getOperType() {
		return operType;
	}

	public void setOperType(Integer operType) {
		this.operType = operType;
	}

	public Integer getOperStatus() {
		return operStatus;
	}

	public void setOperStatus(Integer operStatus) {
		this.operStatus = operStatus;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id == null ? null : id.trim();
	}

	public String getMark() {
		return mark;
	}

	public void setMark(String mark) {
		this.mark = mark;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}


}