package com.zzsy.api.entity;

public class Community extends ModuleRoleRelative{

	private static final long serialVersionUID = -2787624493067620084L;

	private Integer communityID;
	private Integer streetID;
	private String streetName;
	private String communityName;
	private String communityAddress;
	private Integer communityResidenceNo;
	private Integer communityOperationSiteNo;
	private Integer communityType;
	private String communitySequenceNumber;
	private Integer isValid;
	private String bdlongitude;
	private String bdlatitude;
	private String gpslongitude;
	private String gpslatitude;
	private String manualLongitude;
	private String manualLatutide;
	private String reserveArg1;
	private String reserveArg2;
	private String initDate;
	private String shortName;
	private String operationSiteNeighborhoodCommitteeName;
	
	private String communityTypeStr;
	/**
	 * @return the initDate
	 */
	public String getInitDate() {
		return initDate;
	}
	/**
	 * @param initDate the initDate to set
	 */
	public void setInitDate(String initDate) {
		this.initDate = initDate;
	}
	/**
	 * @return the communityID
	 */
	public Integer getCommunityID() {
		return communityID;
	}
	/**
	 * @param communityID the communityID to set
	 */
	public void setCommunityID(Integer communityID) {
		this.communityID = communityID;
	} 
	
	
	public Integer getStreetID() {
		return streetID;
	}
	public void setStreetID(Integer streetID) {
		this.streetID = streetID;
	}
	/**
	 * @return the communityName
	 */
	public String getCommunityName() {
		return communityName;
	}
	/**
	 * @param communityName the communityName to set
	 */
	public void setCommunityName(String communityName) {
		this.communityName = communityName;
	}
	/**
	 * @return the communityAddress
	 */
	public String getCommunityAddress() {
		return communityAddress;
	}
	/**
	 * @param communityAddress the communityAddress to set
	 */
	public void setCommunityAddress(String communityAddress) {
		this.communityAddress = communityAddress;
	}
	/**
	 * @return the communityResidenceNo
	 */
	public Integer getCommunityResidenceNo() {
		return communityResidenceNo;
	}
	/**
	 * @param communityResidenceNo the communityResidenceNo to set
	 */
	public void setCommunityResidenceNo(Integer communityResidenceNo) {
		this.communityResidenceNo = communityResidenceNo;
	}
	/**
	 * @return the communityOperationSiteNo
	 */
	public Integer getCommunityOperationSiteNo() {
		return communityOperationSiteNo;
	}
	/**
	 * @param communityOperationSiteNo the communityOperationSiteNo to set
	 */
	public void setCommunityOperationSiteNo(Integer communityOperationSiteNo) {
		this.communityOperationSiteNo = communityOperationSiteNo;
	}
	/**
	 * @return the communityType
	 */
	public Integer getCommunityType() {
		return communityType;
	}
	/**
	 * @param communityType the communityType to set
	 */
	public void setCommunityType(Integer communityType) {
		this.communityType = communityType;
	}
	/**
	 * @return the communitySequenceNumber
	 */
	public String getCommunitySequenceNumber() {
		return communitySequenceNumber;
	}
	/**
	 * @param communitySequenceNumber the communitySequenceNumber to set
	 */
	public void setCommunitySequenceNumber(String communitySequenceNumber) {
		this.communitySequenceNumber = communitySequenceNumber;
	}
	/**
	 * @return the isValid
	 */
	public Integer getIsValid() {
		return isValid;
	}
	/**
	 * @param isValid the isValid to set
	 */
	public void setIsValid(Integer isValid) {
		this.isValid = isValid;
	}
	/**
	 * @return the bdlongitude
	 */
	public String getBdlongitude() {
		return bdlongitude;
	}
	/**
	 * @param bdlongitude the bdlongitude to set
	 */
	public void setBdlongitude(String bdlongitude) {
		this.bdlongitude = bdlongitude;
	}
	/**
	 * @return the bdlatitude
	 */
	public String getBdlatitude() {
		return bdlatitude;
	}
	/**
	 * @param bdlatitude the bdlatitude to set
	 */
	public void setBdlatitude(String bdlatitude) {
		this.bdlatitude = bdlatitude;
	}
	/**
	 * @return the gpslongitude
	 */
	public String getGpslongitude() {
		return gpslongitude;
	}
	/**
	 * @param gpslongitude the gpslongitude to set
	 */
	public void setGpslongitude(String gpslongitude) {
		this.gpslongitude = gpslongitude;
	}
	/**
	 * @return the gpslatitude
	 */
	public String getGpslatitude() {
		return gpslatitude;
	}
	/**
	 * @param gpslatitude the gpslatitude to set
	 */
	public void setGpslatitude(String gpslatitude) {
		this.gpslatitude = gpslatitude;
	}
	/**
	 * @return the manualLongitude
	 */
	public String getManualLongitude() {
		return manualLongitude;
	}
	/**
	 * @param manualLongitude the manualLongitude to set
	 */
	public void setManualLongitude(String manualLongitude) {
		this.manualLongitude = manualLongitude;
	}
	/**
	 * @return the manualLatutide
	 */
	public String getManualLatutide() {
		return manualLatutide;
	}
	/**
	 * @param manualLatutide the manualLatutide to set
	 */
	public void setManualLatutide(String manualLatutide) {
		this.manualLatutide = manualLatutide;
	}
	/**
	 * @return the reserveArg1
	 */
	public String getReserveArg1() {
		return reserveArg1;
	}
	/**
	 * @param reserveArg1 the reserveArg1 to set
	 */
	public void setReserveArg1(String reserveArg1) {
		this.reserveArg1 = reserveArg1;
	}
	/**
	 * @return the reserveArg2
	 */
	public String getReserveArg2() {
		return reserveArg2;
	}
	/**
	 * @param reserveArg2 the reserveArg2 to set
	 */
	public void setReserveArg2(String reserveArg2) {
		this.reserveArg2 = reserveArg2;
	}
	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	public String getOperationSiteNeighborhoodCommitteeName() {
		return operationSiteNeighborhoodCommitteeName;
	}
	public void setOperationSiteNeighborhoodCommitteeName(String operationSiteNeighborhoodCommitteeName) {
		this.operationSiteNeighborhoodCommitteeName = operationSiteNeighborhoodCommitteeName;
	}
	public String getCommunityTypeStr() {
		return communityTypeStr;
	}
	public void setCommunityTypeStr(String communityTypeStr) {
		this.communityTypeStr = communityTypeStr;
	}
	public String getStreetName() {
		return streetName;
	}
	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}
	
}