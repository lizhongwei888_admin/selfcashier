package com.zzsy.api.constant;

/**
 * 申请类型
 * @author sl
 *
 */
public enum BasicsType  {

	Site(1, "站点管理"),
	District(2, "行政区管理"),
	SystematicCode(3, "系统码管理"),
	Announcement(4, "公告管理"),
	Park(5, "泊位管理"),
	Device(6, "设备管理"),
	InternalVehicle(7, "内集卡管理"),
	Box(8, "集装箱管理"),
	externalVehicle(9, "外运车管理")
	;
	
	private Integer code;

	
	private String desc;

	BasicsType(Integer code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public Integer getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}
	
	public static String getDesc(Integer code) {
		
		BasicsType[] logTypes = values();
		for(BasicsType logType : logTypes) {
			if(code.equals(logType.getCode())) {
				return logType.getDesc();
			}
		}
		return "";
	}

}
