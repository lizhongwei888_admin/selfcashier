package com.zzsy.api.util;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;

import javax.imageio.ImageIO;
	 
	public class CompressImage {
	 
		private static double minscale=0.3;
		private static int minwidth = 800; 
		private static int minheight = 600;
		private static long minsize=1024*1024l;
		
		
		
		public static void main(String[] args) {
			//compressToPNG();
			compressToJPG();
			
			
		}
		
		public static String getFormat(String fileName){
			return fileName.substring(fileName.lastIndexOf(".")+1);
		}
		
		public static void compressToJPG(){
			try{
				String source1="C:\\Users\\sl\\Desktop\\work\\微信图片_20210602105303.jpg";
				String dest1="C:\\Users\\sl\\Desktop\\work\\微信图片_20210602105303_dest.jpg";
				InputStream in=new FileInputStream(new File(source1));
				OutputStream out=new FileOutputStream(new File(dest1));
				//scaleImage(source1, dest1, 0.3,"jpg");
				compressImage(in, out, 0.3,"jpg");
				
				
				
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		public static void compressToPNG(){
			try{
				String source1="C:\\Users\\sl\\Desktop\\work\\QQ截图20220513160932.png";
				String dest1="C:\\Users\\sl\\Desktop\\work\\QQ截图20220513160932_dest.png";
				
				File sourceFile=new File("C:\\Users\\sl\\Desktop\\work\\QQ截图20220721151043.jpg");
				BufferedImage bufferedImage=ImageIO.read(sourceFile);
				int width=bufferedImage.getWidth();
				int height=bufferedImage.getHeight();
				System.out.println(width+","+height);
				scaleImage(source1, 
					      dest1, 0.5,"png");
				
				
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}

		  
		  
		  
		  /*** 
		   * 按指定的比例缩放图片 
		   * 
		   * @param sourceImagePath 
		   *      源地址 
		   * @param destinationPath 
		   *      改变大小后图片的地址 
		   * @param scale 
		   *      缩放比例，如1.2 
		   */
		  public static void scaleImage(String sourceImagePath, 
		      String destinationPath, double scale,String format) { 
		  
		    File file = new File(sourceImagePath); 
		    BufferedImage bufferedImage; 
		    try { 
		      bufferedImage = ImageIO.read(file); 
		      int width = bufferedImage.getWidth(); 
		      int height = bufferedImage.getHeight(); 
		  
		      width = parseDoubleToInt(width * scale); 
		      height = parseDoubleToInt(height * scale); 
		  
		      Image image = bufferedImage.getScaledInstance(width, height, 
		          Image.SCALE_SMOOTH); 
		      BufferedImage outputImage = new BufferedImage(width, height, 
		          BufferedImage.TYPE_INT_RGB); 
		      Graphics graphics = outputImage.getGraphics(); 
		      graphics.drawImage(image, 0, 0, null); 
		      graphics.dispose(); 
		  
		      ImageIO.write(outputImage, format, new File(destinationPath)); 
		    } catch (IOException e) { 
		      System.out.println("scaleImage方法压缩图片时出错了"); 
		      e.printStackTrace(); 
		    } 
		  
		  } 
		  /**
		   * 压缩图片
		   * @param in 上传文件的字节流
		   * @param out 输出压缩文件的字节流
		   * @param scale 压缩比例
		   * @param format 文件格式 支持jpg pnj等常见文件格式
		 * @throws IOException 
		   */
		  public static boolean compressImage(InputStream in, 
				     OutputStream out, double scale,String format) throws IOException { 
				  
				  BufferedImage bufferedImage; 
				    
				    bufferedImage = ImageIO.read(in); 
				    int width = bufferedImage.getWidth(); 
				      int height = bufferedImage.getHeight(); 
				  
				      width = parseDoubleToInt(width * scale); 
				      height = parseDoubleToInt(height * scale); 
				      
				      if(width<minwidth || height<minheight){
				    	  return false;
				      }
				  
				      Image image = bufferedImage.getScaledInstance(width, height, 
				          Image.SCALE_SMOOTH); 
				      BufferedImage outputImage = new BufferedImage(width, height, 
				          BufferedImage.TYPE_INT_RGB); 
				      Graphics graphics = outputImage.getGraphics(); 
				      graphics.drawImage(image, 0, 0, null); 
				      graphics.dispose(); 
				  
				      ImageIO.write(outputImage, format, out); 
				      
				    
				    return true;
				  } 
		  
		  
		  
		  
		  /*** 
		   * 将图片缩放到指定的高度或者宽度 
		   * @param sourceImagePath 图片源地址 
		   * @param destinationPath 压缩完图片的地址 
		   * @param width 缩放后的宽度 
		   * @param height 缩放后的高度 
		   * @param auto 是否自动保持图片的原高宽比例 
		   * @param format 图图片格式 例如 jpg 
		   * image.SCALE_SMOOTH //平滑优先
			 image.SCALE_FAST//速度优先
			 image.SCALE_AREA_AVERAGING //区域均值
			 image.SCALE_REPLICATE //像素复制型缩放
			 image.SCALE_DEFAULT //默认缩放模式
		   */
		  public static boolean scaleImageWithParams(String sourceImagePath, 
		      String destinationPath, int width, int height, boolean auto,String format) { 
			  File file =null;
		    try { 
		     file = new File(sourceImagePath); 
		    BufferedImage bufferedImage = null; 
		    bufferedImage = ImageIO.read(file); 
		      if (auto) { 
		        ArrayList<Integer> paramsArrayList = getAutoWidthAndHeight(bufferedImage,width,height); 
		        width = paramsArrayList.get(0); 
		        height = paramsArrayList.get(1); 
		        System.out.println("自动调整比例，width="+width+" height="+height); 
		      } 
		      
		    Image image = bufferedImage.getScaledInstance(width, height, 
		        Image.SCALE_REPLICATE); 
		    BufferedImage outputImage = new BufferedImage(width, height, 
		        BufferedImage.SCALE_REPLICATE); 
		    Graphics graphics = outputImage.getGraphics(); 
		    graphics.drawImage(image, 0, 0, null); 
		    graphics.dispose(); 
		    ImageIO.write(outputImage, format, new File(destinationPath)); 
		    } catch (Exception e) { 
		      System.out.println("scaleImageWithParams方法压缩图片时出错了"); 
		      e.printStackTrace(); 
		      return false; 
		    }finally {
		    	if (file!=null&&file.exists()) {
		    		 file.delete();
				}
		    }
			return true; 
		      
		      
		  } 
		  
		  /** 
		   * 将double类型的数据转换为int，四舍五入原则 
		   * 
		   * @param sourceDouble 
		   * @return 
		   */
		  private static int parseDoubleToInt(double sourceDouble) { 
		    int result = 0; 
		    result = (int) sourceDouble; 
		    return result; 
		  } 
		    
		  /*** 
		   * 
		   * @param bufferedImage 要缩放的图片对象 
		   * @param width_scale 要缩放到的宽度 
		   * @param height_scale 要缩放到的高度 
		   * @return 一个集合，第一个元素为宽度，第二个元素为高度 
		   */
		  private static ArrayList<Integer> getAutoWidthAndHeight(BufferedImage bufferedImage,int width_scale,int height_scale){ 
		    ArrayList<Integer> arrayList = new ArrayList<Integer>(); 
		    int width = bufferedImage.getWidth(); 
		    int height = bufferedImage.getHeight(); 
		    double scale_w =getDot2Decimal( width_scale,width); 
		      
		    System.out.println("getAutoWidthAndHeight width="+width + "scale_w="+scale_w); 
		    double scale_h = getDot2Decimal(height_scale,height); 
		    if (scale_w<scale_h) { 
		      arrayList.add(parseDoubleToInt(scale_w*width)); 
		      arrayList.add(parseDoubleToInt(scale_w*height)); 
		    } 
		    else { 
		      arrayList.add(parseDoubleToInt(scale_h*width)); 
		      arrayList.add(parseDoubleToInt(scale_h*height)); 
		    } 
		    return arrayList; 
		      
		  } 
		    
		    
		  /*** 
		   * 返回两个数a/b的小数点后三位的表示 
		   * @param a 
		   * @param b 
		   * @return 
		   */
		  public static double getDot2Decimal(int a,int b){ 
		      
		    BigDecimal bigDecimal_1 = new BigDecimal(a); 
		    BigDecimal bigDecimal_2 = new BigDecimal(b); 
		    BigDecimal bigDecimal_result = bigDecimal_1.divide(bigDecimal_2,new MathContext(4)); 
		    Double double1 = new Double(bigDecimal_result.toString()); 
		    System.out.println("相除后的double为："+double1); 
		    return double1; 
		  } 
		  
	 
	}
