package com.zzsy.api.mapper;

import java.util.List;
import java.util.Map;

import com.lujie.common.dao.BaseMapper;
import com.zzsy.api.entity.Log;

public interface LogMapper extends BaseMapper{
    int deleteByPrimaryKey(String id);

    int insertSelective(Log record);

    Log selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Log record);
    
    List<Log> findAllLog(Map<String, String> map);
}