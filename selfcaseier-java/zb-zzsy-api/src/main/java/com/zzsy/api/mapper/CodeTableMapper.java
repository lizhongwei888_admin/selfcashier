package com.zzsy.api.mapper;

import java.util.List;

import com.lujie.common.dao.BasicMapper;
import com.zzsy.api.entity.CodeTable;
import org.springframework.stereotype.Repository;

@Repository
public interface CodeTableMapper extends BasicMapper{
	public List<CodeTable> getAllType();
	public List<CodeTable>getGarbageTypeBySite();
}