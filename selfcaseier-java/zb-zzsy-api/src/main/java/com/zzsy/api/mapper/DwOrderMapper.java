package com.zzsy.api.mapper;

import com.lujie.common.dao.BasicMapper;
import com.zzsy.api.entity.DwOrder;
import com.zzsy.api.entityVo.DwOrderVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface DwOrderMapper extends BasicMapper{
    void insertData(@Param("p") DwOrder d);

    void completeOrder(String orderCode,String requestJsonErp,String returnJsonErp,String saleId);

    void refundOrderState(Integer orderState,String orderCode);


    void uploadOrder(@Param("p") DwOrder d);

    List<DwOrder> findTop10(@Param("p") DwOrder d);


    DwOrder findLastOrder(@Param("p") DwOrder d);
}