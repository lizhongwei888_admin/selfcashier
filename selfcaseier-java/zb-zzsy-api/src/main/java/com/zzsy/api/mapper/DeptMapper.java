package com.zzsy.api.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.lujie.common.dao.BasicMapper;
import com.zzsy.api.entity.Dept;

public interface DeptMapper extends BasicMapper{ 

	List<Dept> deptTreeAuth(@Param("p")Dept record);
	
	
	int deleteByPrimaryKey(String id);

    int insertSelective(Dept record);

    Dept selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Dept record);
    
    List<Dept> deptTree(String pid);

	List<Dept> allChildDept(Map<String, Object> map);

	List<Dept> deptFromUser(Map<String, Object> map);

	List<Dept> allDeptFromUser(Map<String, Object> map);
	
	List<Dept> findAllDeptByIsValid();
}