package com.zzsy.api.config;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.RedeliveryPolicy;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;

@Configuration
@PropertySource("classpath:application-activemq.properties")
public class ActiveMQPiontToPointConfig {
	
	
	@Bean("SmsQueue")//将userInfoQueue的Bean注册到spring容器
	public ActiveMQQueue registUserInfoQueueBean() {
		return new ActiveMQQueue("sms-queue");//将userInfo-queue的消息队列注册到activemq服务器
	}
	
	
	@Bean
	public RedeliveryPolicy redeliveryPolicy() {
		RedeliveryPolicy redeliveryPolicy = new RedeliveryPolicy();
		redeliveryPolicy.setUseExponentialBackOff(true);// 是否在每次失败重发是，增长等待时间
		redeliveryPolicy.setMaximumRedeliveries(10);// 重发次数
		redeliveryPolicy.setInitialRedeliveryDelay(1);// 重发时间间隔
		redeliveryPolicy.setBackOffMultiplier(2);// 第一次失败后重发前等待500毫秒，第二次500*2，依次递增
		redeliveryPolicy.setUseCollisionAvoidance(false);// 是否避免消息碰撞
		return redeliveryPolicy;
	}
	@Value("${spring.activemq.brokerurl}")
    private String brokerUrl;
	@Value("${spring.activemq.user}")
    private String user;
	@Value("${spring.activemq.password}")
    private String password;

	@Bean
	public PooledConnectionFactory 
	pooledConnectionFactory(
			RedeliveryPolicy redeliveryPolicy) {
		ActiveMQConnectionFactory factory = 
				new ActiveMQConnectionFactory(user, password, brokerUrl);
		factory.setRedeliveryPolicy(redeliveryPolicy);
		factory.setTrustAllPackages(true);
		PooledConnectionFactory pooledConnectionFactory = new PooledConnectionFactory(factory);
		pooledConnectionFactory.setMaxConnections(50);
		pooledConnectionFactory.setMaximumActiveSessionPerConnection(300);
		pooledConnectionFactory.setTimeBetweenExpirationCheckMillis(1000);
		return pooledConnectionFactory;
	}

	@Bean
	public JmsTemplate jmsTemplate(PooledConnectionFactory pooledConnectionFactory) {
		JmsTemplate jmsTemplate = new JmsTemplate();
		jmsTemplate.setDeliveryMode(2);// 设置持久化，1 非持久， 2 持久化
		jmsTemplate.setConnectionFactory(pooledConnectionFactory);
		return jmsTemplate;
	}
	
	
	@Bean("jmsListener")
	public DefaultJmsListenerContainerFactory listener(PooledConnectionFactory factory) {
		DefaultJmsListenerContainerFactory listener = new DefaultJmsListenerContainerFactory();
		listener.setConnectionFactory(factory);
		listener.setConcurrency("8");// 设置连接数
		listener.setRecoveryInterval(1000L);// 重连间隔时间
		return listener;
	}
}
