package com.zzsy.api.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.lujie.common.dao.BasicMapper;
import com.zzsy.api.entity.User;
@Repository
public interface UserMapper extends BasicMapper{
	int updateByPrimaryKeySelective(User user);
	int insertSelective(User user);
	int updatePwdByMobile(User record);
	List<User> findListByDept(@Param("p")User user);
	Integer findListCountByDept(@Param("p")User user);
	
	
}