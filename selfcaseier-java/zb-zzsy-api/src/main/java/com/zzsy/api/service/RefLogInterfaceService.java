package com.zzsy.api.service;

import com.lujie.common.service.BaseService;
import com.zzsy.api.entity.RefLogApp;
import com.zzsy.api.entity.RefLogInterface;
import com.zzsy.api.mapper.RefLogInterfaceMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.util.List;
import java.util.UUID;

@Service
public class RefLogInterfaceService extends BaseService {

    private static final Logger log = LoggerFactory.getLogger(RefLogInterfaceService.class);

    @Autowired
    private RefLogInterfaceMapper refLogInterfaceMapper;

    //插入日志
    public void create(RefLogInterface refLogInterface) {
        try {
            refLogInterface.setCode(UUID.randomUUID().toString().replaceAll("-", ""));
            InetAddress ip4 = Inet4Address.getLocalHost();
            refLogInterface.setIp(ip4.getHostAddress());
            refLogInterfaceMapper.add(refLogInterface);
        } catch (Exception e) {
            log.error("RefLogInterfaceService-create异常", e);
        }
    }

    public List<RefLogInterface> findPageList(RefLogInterface refLogInterface) {
        return refLogInterfaceMapper.findPageList(refLogInterface);
    }

    public Long countTotal(RefLogInterface refLogInterface) {
        return refLogInterfaceMapper.countTotal(refLogInterface);
    }

    public List<RefLogInterface> findList(RefLogInterface refLogInterface) {
        return refLogInterfaceMapper.findList(refLogInterface);
    }

}
