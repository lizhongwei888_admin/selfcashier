package com.zzsy.api.mapper;

import com.lujie.common.dao.BasicMapper;
import com.zzsy.api.entity.RefStorage;
import com.zzsy.api.entity.RefUser;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface RefStorageMapper extends BasicMapper {


    RefStorage getOneByParms(@Param("p") RefStorage refStorage);

}
