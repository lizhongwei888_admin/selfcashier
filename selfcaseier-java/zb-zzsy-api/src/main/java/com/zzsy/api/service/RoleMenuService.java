package com.zzsy.api.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.zzsy.api.constant.LogType;
import com.zzsy.api.constant.UserType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.util.StringUtil;
import com.zzsy.api.entity.Log;
import com.zzsy.api.entity.Role;
import com.zzsy.api.entity.RoleMenu;
import com.zzsy.api.entity.User;
import com.zzsy.api.mapper.RoleMenuMapper;
import com.zzsy.api.util.RedisUtil;

@Service
public class RoleMenuService   {
	@Autowired
	private RoleMenuMapper roleMenumapper;
	@Autowired
	private LogService logService;
	@Autowired 
	private RedisTemplate redisTemplate;
	
	
	public ResultMsg batchSaveRoleMenu(Role role , HttpServletRequest request){
		if(role==null || role.getRoleMenuList()==null || role.getRoleMenuList().size()==0){
			return  new ResultMsg("310","请求参数不能为空");
		}
		for(RoleMenu roleMenu:role.getRoleMenuList()){
			if(roleMenu.getRid()==null || roleMenu.getMid()==null){
				return  new ResultMsg("320","请求参数不能为空");
			}
		}
		saveRoleMenu(role);
		
		User loginuser = RedisUtil.getUser(redisTemplate, request);
		//this.logService.create(loginuser, "添加角色菜单");
		this.logService.create(new Log(loginuser.getId(), "添加角色菜单", LogType.UserType.getDesc(), UserType.Menu.getDesc(), 0, 0) );

		return new ResultMsg(ResultCode.Success);
		
	}
	
	
	
	private Integer findIndexByMenu(List<RoleMenu> allRoleMenuList,String mid){
		for(Integer index=0;index<allRoleMenuList.size();index++){
			RoleMenu dbRoleMenu=allRoleMenuList.get(index);
			if(dbRoleMenu.getMid().equals(mid)){
				return index;
			}
		}
		return -1;
	}
	private Integer findIndexByRole(List<RoleMenu> allRoleMenuList,String rid){
		for(Integer index=0;index<allRoleMenuList.size();index++){
			RoleMenu dbRoleMenu=allRoleMenuList.get(index);
			if(dbRoleMenu.getRid().equals(rid)){
				return index;
			}
		}
		return -1;
	}
	private List<RoleMenu> getDeleteRoleMenu(List<RoleMenu> allRoleMenuList,List<Integer> existMenuIndexList){
		List<RoleMenu> deleteMenuList=new ArrayList<>();
		for(Integer index=0;index<allRoleMenuList.size();index++){
			if(!existMenuIndexList.contains(index)){
				deleteMenuList.add(allRoleMenuList.get(index));
			}
		}
		return deleteMenuList;
	}
	/**
	 * //找到这个角色的所有菜单
		//匹配找到用户已经存在的菜单
		//删除角色菜单
		//新增角色菜单
		
	 * @param role
	 * @return
	 */
	private ResultMsg saveRoleMenu(Role role){
		List<RoleMenu> userSelectMenus=role.getRoleMenuList();
		
		RoleMenu roleMenu=new RoleMenu();
		if(StringUtil.isEmpty(role.getFlag())){//空值,默认从角色添加角色菜单关系
			roleMenu.setRid(userSelectMenus.get(0).getRid());
		}else{//2，从角色添加角色菜单关系
			roleMenu.setMid(userSelectMenus.get(0).getMid());
		}
		
		List<RoleMenu> allRoleMenuList=roleMenumapper.findList(roleMenu);
		
		List<Integer> existMenuIndexList=new ArrayList<>();
		List<RoleMenu> newRoleMenuList=new ArrayList<>();
		for(RoleMenu userSelectMenu:userSelectMenus){
			Integer existIndex=findIndexByMenu(allRoleMenuList,userSelectMenu.getMid());
			
			if(StringUtil.isEmpty(role.getFlag())){//空值,默认从角色添加角色菜单关系
				existIndex=findIndexByMenu(allRoleMenuList,userSelectMenu.getMid());
			}else{//2，从角色添加角色菜单关系
				existIndex=findIndexByRole(allRoleMenuList,userSelectMenu.getMid());
			}
			
			if(existIndex!=-1){
				existMenuIndexList.add(existIndex);//已经存在的菜单，不添加
			}else{
				newRoleMenuList.add(userSelectMenu);
			} 
		}
		 
		List<RoleMenu> deleteRoleMenuList=getDeleteRoleMenu(allRoleMenuList,existMenuIndexList);
		if(!deleteRoleMenuList.isEmpty()){
			for(RoleMenu deleteRoleMenu:deleteRoleMenuList){
				roleMenumapper.deleteByPrimaryKey(deleteRoleMenu.getId());
			}	
		}
		if(!newRoleMenuList.isEmpty()){
			for(RoleMenu newRoleMenu:newRoleMenuList){
				roleMenumapper.insertSelective(newRoleMenu);
			}	
		}
		
		return new ResultMsg(ResultCode.Success);
	}
	public ResultMsg batchDeleteRoleMenu(Role role , HttpServletRequest request){
		if(role==null || role.getRoleMenuList()==null || role.getRoleMenuList().size()==0){
			return  new ResultMsg("310","请求参数不能为空");
		}
		for(RoleMenu roleMenu:role.getRoleMenuList()){
			if(roleMenu.getId()==null){
				return  new ResultMsg("320","请求参数不能为空");
			}
		}
		for(RoleMenu roleMenu:role.getRoleMenuList()){
			roleMenumapper.deleteByPrimaryKey(roleMenu.getId());
		}
		User loginuser = RedisUtil.getUser(redisTemplate, request);
		//this.logService.create(loginuser, "删除角色菜单");
		this.logService.create(new Log(loginuser.getId(), "删除角色菜单", LogType.UserType.getDesc(), UserType.Menu.getDesc(), 2, 0) );

		return new ResultMsg(ResultCode.Success);
		
	}

	
	public List<RoleMenu> findMenuByRid(Map<String, String> map) {
		return this.roleMenumapper.selectByRidMid(map);
	}

	
	
}
