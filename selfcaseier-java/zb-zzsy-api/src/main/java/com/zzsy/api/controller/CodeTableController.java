package com.zzsy.api.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.util.JsonHelper;
import com.lujie.common.util.StringUtil;
import com.zzsy.api.constant.KeysManager;
import com.zzsy.api.entity.CodeTable;
import com.zzsy.api.service.CodeTableService;


/**
 * 行政区管理
 * @author zzm
 */
@RestController
@RequestMapping("/codetable")
public class CodeTableController {
	private static final Logger logger = LoggerFactory.getLogger(CodeTableController.class);
	@Autowired
	private CodeTableService codeTableService;
	@Autowired 
	private RedisTemplate redisTemplate;
	
	@RequestMapping(value="/findAll")
	@ResponseBody
	public String findAll(@RequestBody CodeTable codeTable){
		String codeTypeKey="";
		if(!StringUtil.isEmpty(codeTable.getCodeType())){
			codeTypeKey=codeTable.getCodeType();
		}
		String parentId="";
		if(codeTable.getParentId()!=null){
			parentId=codeTable.getParentId().toString();
		}
		String createTimeRangeStr="";
		if(StringUtil.isNotEmpty(codeTable.getCreateTimeStart())){
			createTimeRangeStr+=codeTable.getCreateTimeStart();
		}
		if(StringUtil.isNotEmpty(codeTable.getCreateTimeEnd())){
			createTimeRangeStr+=codeTable.getCreateTimeEnd();
		}
		
		//RedisUtil.deleteMiddleLike(redisTemplate, KeysManager.Muck_E3UForm_CodeTable);
		List<CodeTable> codeTables = (List<CodeTable>)redisTemplate.opsForValue().get(KeysManager.User_Auth+KeysManager.CodeTbale+codeTypeKey+parentId+createTimeRangeStr);
		if(codeTables==null){
			codeTables=codeTableService.findList(codeTable);
			if(codeTables!=null)
				redisTemplate.opsForValue().set(KeysManager.User_Auth+KeysManager.CodeTbale+codeTypeKey+parentId+createTimeRangeStr, codeTables);
		}
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success,codeTables,codeTables!=null?codeTables.size():0L));
	}
	
	@RequestMapping(value="/getAllType")
	@ResponseBody
	public String getAllType(@RequestBody CodeTable codeTable){
		List<CodeTable> codeTables = codeTableService.getAllType();
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success,codeTables,codeTables!=null?codeTables.size():0L));
	}
	
	@RequestMapping(value="/getGarbageTypeBySite")
	@ResponseBody
	public String getGarbageTypeBySite(){
		List<CodeTable> codeTables = codeTableService.getGarbageTypeBySite();
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success,codeTables));
	}
	
	
	@RequestMapping(path={"/list"})
	@ResponseBody
	public String list(@RequestBody CodeTable codeTable) {
		List list=codeTableService.findPageList(codeTable);
		Long totalCount=codeTableService.countTotal(codeTable);
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success,list,totalCount));
	}
	@RequestMapping(path={"/detail"})
	@ResponseBody
	public String detail(@RequestBody CodeTable codeTable) {
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success,
				codeTableService.findOne(codeTable)));
	}
	
	@RequestMapping(path={"/save"})
	@ResponseBody
	public String save(HttpServletRequest request,@RequestBody CodeTable codeTable) {
		return JsonHelper.toJson(codeTableService.save(request,codeTable));
	}
	@RequestMapping(path={"/update"})
	@ResponseBody
	public String update(HttpServletRequest request,@RequestBody CodeTable codeTable) {
		return JsonHelper.toJson(codeTableService.update(request,codeTable));
	}
	@RequestMapping(path={"/delete"})
	@ResponseBody
	public String delete(HttpServletRequest request,@RequestBody CodeTable codeTable) {
		return JsonHelper.toJson(codeTableService.delete(request,codeTable));
	}
	
}
