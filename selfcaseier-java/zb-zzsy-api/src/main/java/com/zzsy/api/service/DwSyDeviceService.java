package com.zzsy.api.service;

import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.service.BasicService;
import com.lujie.common.util.StringUtil;
import com.zzsy.api.entity.DwSyDevice;
import com.zzsy.api.entity.User;
import com.zzsy.api.mapper.DwSyDeviceMapper;
import com.zzsy.api.util.RedisUtil;
import com.zzsy.api.constant.KeysManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Service
public class DwSyDeviceService extends BasicService {
	public static final Logger log = LoggerFactory.getLogger(DwSyDeviceService.class);

	@Autowired
	private RedisTemplate redisTemplate;
	@Autowired
	private DwSyDeviceMapper dwSyDeviceMapper;

	@Resource(name = "dwSyDeviceMapper")
	public void initBaseDao(DwSyDeviceMapper dwSyDeviceMapper) {
		super.setBaseMapper(dwSyDeviceMapper);
	}



	public DwSyDevice findByDeviceCode(DwSyDevice dwSyDevice){
		DwSyDevice device= dwSyDeviceMapper.findByDeviceCode(dwSyDevice);
		return  device;
	}


	public ResultMsg save(HttpServletRequest request, DwSyDevice dwSyDevice){
		if(StringUtil.isEmpty(dwSyDevice.getDeviceCode())){
			return new ResultMsg("301","设备编码不能为空");
		}
		if(StringUtil.isEmpty(dwSyDevice.getShopId())){
			return new ResultMsg("302","所属门店不能为空");
		}

		if(findByDeviceCode(dwSyDevice)!=null){
			return new ResultMsg("305","设备已存在的关联已经存在");
		}
		dwSyDeviceMapper.save(dwSyDevice);

		RedisUtil.deleteMiddleLike(redisTemplate, KeysManager.User_Auth+KeysManager.CodeTbale);

		User admin=RedisUtil.getUser(redisTemplate, request);

		//logService.create(admin, "添加codetable");
		//this.logService.create(new Log(admin.getId(), "添加codetable", LogType.Basics.getDesc(), BasicsType.SystematicCode.getDesc(), 0, 0) );



		return new ResultMsg(ResultCode.Success);
	}

}
