package com.zzsy.api.service;

import com.lujie.common.util.*;
import com.zzsy.api.entity.DwOrder;
import com.zzsy.api.entity.RefLogInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class InterFaceService {


    public static final String  InterfaceAccountKey="InterfaceAccount";
    public static final String  InterfaceAccountModuleRoleKey="InterfaceAccountModuleRole";

    @Autowired
    private RefApiService refApiService;

    @Autowired
    private RefLogInterfaceService refLogInterfaceService;

    @Autowired
    private DwOrderService dwOrderService;


    @Autowired
    private RedisTemplate redisTemplate;

    //1.会员查询
    public String memberInfo(Map<String,Object>m){
        Map<String,String> item = (Map<String,String>)redisTemplate.opsForValue().get("zb_memberList");

        if(item==null) {
            item = refApiService.findApiItem("zb_memberList");
            redisTemplate.opsForValue().set("zb_memberList", item);
        }

        String orgID = StringUtils.ObjectToString(m.get("orgID"));
        String storeCode = StringUtils.ObjectToString(m.get("storeCode"));
        String deviceCode = StringUtils.ObjectToString(m.get("deviceCode"));
        m.remove("orgID");
        m.remove("storeCode");
        m.remove("deviceCode");
        String site = storeCode+deviceCode;
        String url = item.get("url");
        Map<String,Object>param=new HashMap<>();
        param.put("traceId", UUID.randomUUID());
        param.put("mcId", item.get("mcId"));
        param.put("service",item.get("service"));
        param.put("data", m);
        String str= HttpClientUtil.doPostJson(url, JsonHelper.toJson(param),"utf-8");


        try {
            RefLogInterface log = new RefLogInterface("","会员查询","memberInfo",url,JsonHelper.toJson(param),str,site,orgID);
            refLogInterfaceService.create(log);
        }catch (Exception ex){

        }
        return str;
    }


    //2.商品查询
    public String queryWares(Map<String,Object>m){

        Map<String,String> item = (Map<String,String>)redisTemplate.opsForValue().get("zb_shopList");

        if(item==null) {
            item = refApiService.findApiItem("zb_shopList");
            redisTemplate.opsForValue().set("zb_shopList", item);
        }

        String storeCode = StringUtils.ObjectToString(m.get("storeCode"));
        String deviceCode = StringUtils.ObjectToString(m.get("deviceCode"));
        String site = storeCode+deviceCode;

        String url = item.get("url");
        Map<String,Object>param=new HashMap<>();
        param.put("traceId", UUID.randomUUID());
        param.put("phoneNumber", m.get("phoneNumber"));
        param.put("storeCode",storeCode);
        param.put("data", m.get("data"));
        String str= HttpClientUtil.doPostJson(url, JsonHelper.toJson(param),"utf-8");

        try {
            RefLogInterface log = new RefLogInterface("","商品查询","queryWares",url,JsonHelper.toJson(param),str,site,m.get("orgID").toString());
            refLogInterfaceService.create(log);
        }catch (Exception ex){

        }
        return str;
    }

    //3.整单促销计算接口
    public String calPrice(Map<String,Object>m){
        if(new Date().getTime() > DateUtil.getDate(2024,3,1).getTime()){
            return "false";
        }

        Map<String,String> item = (Map<String,String>)redisTemplate.opsForValue().get("zb_order_trial");

        if(item==null) {
            item = refApiService.findApiItem("zb_order_trial");
            redisTemplate.opsForValue().set("zb_order_trial", item);
        }

        String storeCode = StringUtils.ObjectToString(m.get("storeCode"));
        String deviceCode = StringUtils.ObjectToString(m.get("deviceCode"));
        String site = storeCode+deviceCode;

        String url = item.get("url");
        Map<String,Object>param=new HashMap<>();
        param.put("traceId", UUID.randomUUID());
        param.put("phoneNumber", m.get("phoneNumber"));
        param.put("storeCode", storeCode);
        param.put("mcId", item.get("mcId"));
        param.put("data", m.get("data"));
        String str= HttpClientUtil.doPostJson(url, JsonHelper.toJson(param),"utf-8");


        try {
            RefLogInterface log = new RefLogInterface("","销售促销","calPrice",url,JsonHelper.toJson(param),str,site,m.get("orgID").toString());
            refLogInterfaceService.create(log);
        }catch (Exception ex){

        }

        try {
            if(!str.equals("")){
                Map<String,Object> res= JsonHelper.toObject(str, Map.class);
                res.put("requestJsonPay",JsonHelper.toJson(param));
                return JsonHelper.toJson(res);
            }
        }catch (Exception e){

        }
        return str;
    }

    //聚合支付--- 支付
    public String pay(Map<String,Object>m){
        Map<String,String> item = (Map<String,String>)redisTemplate.opsForValue().get("zb_order_pay");

        if(item==null) {
            item = refApiService.findApiItem("zb_order_pay");
            redisTemplate.opsForValue().set("zb_order_pay", item);
        }
        m.put("transDate", DateUtil.formatDate(new Date(), "yyyyMMddHHmmss"));

        String orgID = StringUtils.ObjectToString(m.get("orgID"));
        m.remove("orgID");
        String storeCode = StringUtils.ObjectToString(m.get("storeCode"));
        String deviceCode = StringUtils.ObjectToString(m.get("deviceCode"));
        m.remove("deviceCode");
        String site = storeCode+deviceCode;

        String url = item.get("url");
        Map<String,Object>param=new HashMap<>();
        param.put("traceId", UUID.randomUUID());
        param.put("mcId", item.get("mcId"));
        param.put("service", item.get("service"));
        param.put("version", item.get("version"));
        param.put("data", m);
        String str= HttpClientUtil.doPostJson(url, JsonHelper.toJson(param),"utf-8");

        try {
            RefLogInterface log = new RefLogInterface("","支付","pay",url,JsonHelper.toJson(param),str,site,orgID);
            refLogInterfaceService.create(log);
        }catch (Exception e){

        }
        try {
            if(!str.equals("")){
                Map<String,Object> res= JsonHelper.toObject(str, Map.class);
                res.put("requestJsonPay",JsonHelper.toJson(param));
                return JsonHelper.toJson(res);
            }
        }catch (Exception e){

        }

        return str;
    }

    //聚合支付--- 查询
    public String payCheck(Map<String,Object>m){

        m.put("transDate", DateUtil.formatDate(new Date(), "yyyyMMddHHmmss"));
        Map<String,String> item = (Map<String,String>)redisTemplate.opsForValue().get("zb_order_pay");

        if(item==null) {
            item = refApiService.findApiItem("zb_order_pay");
            redisTemplate.opsForValue().set("zb_order_pay", item);
        }

        String orgID = StringUtils.ObjectToString(m.get("orgID"));
        m.remove("orgID");
        String storeCode = StringUtils.ObjectToString(m.get("storeCode"));
        String deviceCode = StringUtils.ObjectToString(m.get("deviceCode"));
        m.remove("deviceCode");
        String site = storeCode+deviceCode;
        String url = item.get("url");

        Map<String,Object>param=new HashMap<>();
        param.put("traceId", UUID.randomUUID());
        param.put("mcId", item.get("mcId"));
        param.put("service", item.get("service"));
        param.put("version", item.get("version"));
        param.put("data", m);
        String str= HttpClientUtil.doPostJson(url, JsonHelper.toJson(param),"utf-8");

        try {
            RefLogInterface log = new RefLogInterface("","支付查询","payCheck",url,JsonHelper.toJson(param),str,site,orgID);
            refLogInterfaceService.create(log);
        }catch (Exception ex){

        }
        return str;
    }

    //下单接口
    public String submitOrder(Map<String,Object>m){

//        m.put("transDate", DateUtil.formatDate(new Date(), "yyyyMMddHHmmss"));
        Map<String,String> item = (Map<String,String>)redisTemplate.opsForValue().get("zb_order_submit");

        if(item==null) {
            item = refApiService.findApiItem("zb_order_submit");
            redisTemplate.opsForValue().set("zb_order_submit", item);
        }
        String url = item.get("url");

        String storeCode = StringUtils.ObjectToString(m.get("storeCode"));
        String deviceCode = StringUtils.ObjectToString(m.get("deviceCode"));
        String site = storeCode+deviceCode;

        Map<String,Object>param=new HashMap<>();
        param.put("traceId", UUID.randomUUID());
        param.put("mcId", item.get("mcId"));
        param.put("phoneNumber", StringUtil.getStr(m.get("phoneNumber")));
        param.put("storeCode", m.get("storeCode"));
        param.put("data", m.get("data"));
        String str= HttpClientUtil.doPostJson(url, JsonHelper.toJson(param),"utf-8");


        try {
            RefLogInterface log = new RefLogInterface("","下单erp","submitOrder",url,JsonHelper.toJson(param),str,site,m.get("orgID").toString());
            refLogInterfaceService.create(log);
        }catch (Exception ex){

        }

        return str;
    }

    //小票接口
    public String ticketsUpload(Map<String,Object>m){

        m.put("transDate", DateUtil.formatDate(new Date(), "yyyyMMddHHmmss"));
        Map<String,String> item = (Map<String,String>)redisTemplate.opsForValue().get("zb_order_receipt");

        if(item==null) {
            item = refApiService.findApiItem("zb_order_receipt");
            redisTemplate.opsForValue().set("zb_order_receipt", item);
        }

        String storeCode = StringUtils.ObjectToString(m.get("storeCode"));
        String deviceCode = StringUtils.ObjectToString(m.get("deviceCode"));
        String site = storeCode+deviceCode;

        String url = item.get("url");

        Map<String,Object>param=new HashMap<>();
        param.put("traceId", UUID.randomUUID());
        param.put("mcId", item.get("mcId"));
        param.put("service", item.get("service"));
        param.put("data", m.get("data"));
        String str= HttpClientUtil.doPostJson(url, JsonHelper.toJson(param),"utf-8");
        try {
            RefLogInterface log = new RefLogInterface("","小票上传","ticketsUpload",url,JsonHelper.toJson(param),str,site,m.get("orgID").toString());
            refLogInterfaceService.create(log);
        }catch (Exception ex){

        }
        DwOrder order = new DwOrder();
        order.setCode(m.get("code").toString());
        order.setRequestJsonReceipt(JsonHelper.toJson(param));
        order.setReturnJsonReceipt(str);
        order.setReceiptState(2);
        dwOrderService.uploadOrder(order);

        return str;
    }



    public String orderRefund(Map<String,Object>m){

        Map<String,String> item = (Map<String,String>)redisTemplate.opsForValue().get("zb_order_pay_refund");

        if(item==null) {
            item = refApiService.findApiItem("zb_order_pay_refund");
            redisTemplate.opsForValue().set("zb_order_pay_refund", item);
        }
        String url = item.get("url");
        String storeCode = StringUtils.ObjectToString(m.get("storeCode"));
        String deviceCode = StringUtils.ObjectToString(m.get("deviceCode"));
        String site = storeCode+deviceCode;
        Map<String,Object>param=new HashMap<>();
        param.put("traceId", UUID.randomUUID());
        param.put("mcId", item.get("mcId"));
        param.put("service", item.get("service"));
        param.put("data", m.get("data"));
        String str= HttpClientUtil.doPostJson(url, JsonHelper.toJson(param),"utf-8");

        try {
            RefLogInterface log = new RefLogInterface("","退款","orderRefund",url,JsonHelper.toJson(param),str,site,m.get("orgID").toString());
            refLogInterfaceService.create(log);
        }catch (Exception ex){

        }


        try {
            if(!str.equals("")){
                Map<String,Object> res= JsonHelper.toObject(str, Map.class);
                res.put("requestJsonPay",JsonHelper.toJson(param));
                return JsonHelper.toJson(res);
            }
        }catch (Exception e){

        }
        return str;
    }


}
