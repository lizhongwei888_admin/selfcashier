package com.zzsy.api.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.util.JsonHelper;
import com.zzsy.api.entity.RefLogAdmin;
import com.zzsy.api.entity.RefLogApp;
import com.zzsy.api.entity.RefUser;
import com.zzsy.api.excel.RefLogExcel;
import com.zzsy.api.service.RefLogAdminService;
import com.zzsy.api.service.RefLogAppService;
import com.zzsy.api.util.RedisRefUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

/**
 * 系统-收银操作日志控制器
 */
@Controller
@RequestMapping(value = "/refLogApp")
public class RefLogAppController {
    private static final Logger log = LoggerFactory.getLogger(RefLogAppController.class);

    @Autowired
    private RefLogAppService refLogAppService;
    @Autowired
    private RefLogAdminService refLogAdminService;
    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 后端分页查询集合
     *
     * @param object
     * @return
     */
    @RequestMapping(value = "/listRefLogApp", method = RequestMethod.POST)
    @ResponseBody
    public String listRefLogApp(@RequestBody JSONObject object) {
        RefLogApp refLogApp = JSONObject.parseObject(JSON.toJSONString(object.getJSONObject("params")), RefLogApp.class);
        if (object.getInteger("offset") != null && object.getInteger("limit") != null) {
            refLogApp.setPageNum(object.getInteger("offset"));
            refLogApp.setPageSize(object.getInteger("limit"));
        }
        JSONObject sort = object.getJSONObject("sort");
        if (sort != null) {
            String prop = sort.getString("prop");
            String order = sort.getString("order");
            refLogApp.setProp(prop);
            refLogApp.setOrder(order);
        }
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success, refLogAppService.findPageList(refLogApp), refLogAppService.countTotal(refLogApp)));
    }

    @PostMapping(value = "/listRefLogAppExport")
    @ResponseBody
    public String listRefLogAppExport(@RequestBody JSONObject object, HttpServletRequest request, HttpServletResponse response) {
        RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
        RefLogApp refLogApp = JSONObject.parseObject(JSON.toJSONString(object.getJSONObject("params")), RefLogApp.class);
        JSONObject sort = object.getJSONObject("sort");
        if (sort != null) {
            String prop = sort.getString("prop");
            String order = sort.getString("order");
            refLogApp.setProp(prop);
            refLogApp.setOrder(order);
        }
        // 查询列表
        List<RefLogApp> result = refLogAppService.findList(refLogApp);
        // 类型转换
        List<RefLogExcel> list = JSONArray.parseArray(JSON.toJSONString(result), RefLogExcel.class);
        try {
            // 设置响应头信息
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.setCharacterEncoding("utf-8");
            String fileName = URLEncoder.encode("收银操作日志信息表", "UTF-8").replaceAll("\\+", "%20");
            response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + "_" + System.currentTimeMillis() + ".xlsx");
            // 使用EasyExcel进行数据导出
            EasyExcel.write(response.getOutputStream(), RefLogExcel.class).sheet("导出数据").doWrite(list);

            this.refLogAdminService.create(new RefLogAdmin(loginRefUser.getCode()
                    , "收银操作管理界面", "导出", "listRefLogAppExport", "/refLogApp/listRefLogAppExport"
                    , JSON.toJSONString(result), "ok", loginRefUser.getOrgID()));
            return JsonHelper.toJson(new ResultMsg(ResultCode.Success));
        } catch (IOException e) {
            log.error("listRefLogAppExport:", e);
            return JsonHelper.toJson(new ResultMsg("300", "导出异常"));
        }
    }

}
