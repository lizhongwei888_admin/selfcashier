package com.zzsy.api.controller;

import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.util.JsonHelper;
import com.zzsy.api.entity.DwOrder;
import com.zzsy.api.entity.DwOrderPaywaydetail;
import com.zzsy.api.service.DwOrderPaywaydetailService;
import com.zzsy.api.service.DwOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
@RequestMapping(value = "/dwOrder")
public class DwOrderController {

	@Autowired
	private DwOrderService dwOrderService;

	@Autowired
	private DwOrderPaywaydetailService dwOrderPaywaydetailService;

	@RequestMapping(value={"/insertData"})
	@ResponseBody
	public ResultMsg insertData(HttpServletRequest request, @RequestBody Map<String,Object> data){

		return dwOrderService.insertData(request,data);
	}

	//插入订单支付详情
	@RequestMapping(value={"/insertPayData"})
	@ResponseBody
	public String insertPayData(HttpServletRequest request, @RequestBody DwOrderPaywaydetail dwOrderPaywaydetails){

		return JsonHelper.toJson(new ResultMsg(ResultCode.Success,dwOrderPaywaydetailService.insertData(request,dwOrderPaywaydetails)));
	}

	//完成订单
	@RequestMapping(value={"/completeOrder"})
	@ResponseBody
	public String completeOrder(HttpServletRequest request, @RequestBody  Map<String,Object> data){
		return JsonHelper.toJson(dwOrderService.completeOrder(request,data));
	}

	@RequestMapping(value={"/findTop10"})
	@ResponseBody
	public String findTop10(@RequestBody DwOrder data){
		return JsonHelper.toJson(dwOrderService.findTop10(data));
	}

	@RequestMapping(value={"/getOrderList"})
	@ResponseBody
	public String getOrderList(@RequestBody DwOrder data){
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success,dwOrderService.findPageList(data),dwOrderService.countTotal(data)));
	}

	@RequestMapping(value={"/lastOrder"})
	@ResponseBody
	public String lastOrder(@RequestBody DwOrder data){
		return JsonHelper.toJson(dwOrderService.findLastOrder(data));
	}
}
