package com.zzsy.api.constant;

/**
 * 申请类型
 * @author sl
 *
 */
public enum Module  {

	Presentiment(1, "预感知"),
	SchedulingAndControl(2, "调度与管控"),
	Park(3, "派位管理"),
	Process(4, "作业履历"),
	FENCE(6, "电子围栏"),
	EXTERNAL_VEHICLE_FULL(7 ,"外集卡拖重箱") ,
	EXTERNAL_VEHICLE_EMPTY(8 , "外集卡拖空箱" ),
	INTERNAL_VEHICLE_FULL(9 , "内集卡拖重箱") ,
	INTERNAL_VEHICLE_EMPTY(10 , "内集卡拖空箱")
	;
	
	private Integer code;

	
	private String desc;

	Module(Integer code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public Integer getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}
	
	public static String getDesc(Integer code) {
		
		Module[] logTypes = values();
		for(Module logType : logTypes) {
			if(code.equals(logType.getCode())) {
				return logType.getDesc();
			}
		}
		return "";
	}

}
