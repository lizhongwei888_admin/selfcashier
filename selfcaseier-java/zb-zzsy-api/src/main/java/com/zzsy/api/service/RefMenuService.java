package com.zzsy.api.service;

import com.lujie.common.service.BaseService;
import com.zzsy.api.entity.RefMenu;
import com.zzsy.api.entity.RefRole;
import com.zzsy.api.entity.TreeZH;
import com.zzsy.api.mapper.RefMenuMapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class RefMenuService extends BaseService {

    @Autowired
    private RefMenuMapper refMenuMapper;

    /**
     * 添加或修改
     *
     * @param refMenu
     * @return
     */
    public int addOrUp(RefMenu refMenu) {
        if (StringUtils.isEmpty(refMenu.getCode())) {
            //插入唯一code
            refMenu.setCode(UUID.randomUUID().toString().replace("-", ""));
            return refMenuMapper.add(refMenu);
        } else {
            refMenu.setUpdate_time(new Date());
            int up = refMenuMapper.up(refMenu);
            return up;
        }

    }

    public void delete(String codes) {
        Set<String> collect = Arrays.stream(codes.split(",")).collect(Collectors.toSet());
        refMenuMapper.deleteByIdSet(collect);
    }

    public List<RefMenu> findPageList(RefMenu refMenu) {
        return refMenuMapper.findPageList(refMenu);
    }

    public Long countTotal(RefMenu refMenu) {
        return refMenuMapper.countTotal(refMenu);
    }

    //通过codes获得菜单
    public List<RefMenu> getListByCodes(String codes) {
        if (StringUtils.isEmpty(codes)) {
            return null;
        }
        Set<String> collect = Arrays.stream(codes.split(",")).collect(Collectors.toSet());
        List<RefMenu> listByCodes = refMenuMapper.getListByCodes(collect);
        if (CollectionUtils.isEmpty(listByCodes)) {
            return null;
        }
        return listByCodes;
        //return getListByCodes(listByCodes);
    }

//    //将菜单进行分级展示
//    public List<RefMenu> getListByCodes(List<RefMenu> refMenuList) {
//        if (CollectionUtils.isEmpty(refMenuList)) {
//            return null;
//        }
//        Map<Integer, List<RefMenu>> collect = refMenuList.stream()
//                .collect(Collectors.groupingBy(RefMenu::getMenuLenver));
//        List<RefMenu> refMenus = collect.get(1);
//        if (CollectionUtils.isEmpty(refMenus)) {
//            //无一级目录信息
//            return null;
//        }
//        List<RefMenu> refMenus1 = collect.get(2);
//        if (CollectionUtils.isEmpty(refMenus1)) {
//            //无二级级目录信息
//            return refMenus;
//        }
//        //一级二级目录进行拼接
//        assembleRefMenuData(refMenus, refMenus1);
//        List<RefMenu> refMenus2 = collect.get(3);
//        if (CollectionUtils.isEmpty(refMenus2)) {
//            //无三级级目录信息
//            return refMenus;
//        }
//        //二级三级级目录进行拼接
//        assembleRefMenuData(refMenus1, refMenus2);
//        return refMenus;
//    }

    private void assembleRefMenuData(List<RefMenu> refMenuListOne, List<RefMenu> refMenuListTwo) {
        if (CollectionUtils.isEmpty(refMenuListOne) || CollectionUtils.isEmpty(refMenuListTwo)) {
            return;
        }
        Map<String, List<RefMenu>> collect = refMenuListTwo.stream()
                .collect(Collectors.groupingBy(RefMenu::getMenuPid));
        for (RefMenu refMenu : refMenuListOne) {
            if (!collect.containsKey(refMenu.getCode())) {
                continue;
            }
            //赋值
            refMenu.setNextLevelData(collect.get(refMenu.getCode()));
        }
    }

    public List<TreeZH> menuToTreeZhList(List<RefMenu> refMenuList) {
        if (CollectionUtils.isEmpty(refMenuList)) {
            return null;
        }
        List<TreeZH> treeZHList = new ArrayList<TreeZH>();
        for (RefMenu refMenu : refMenuList) {
            TreeZH treeZH = menuToTreeZhOne(refMenu);
            if (treeZH != null) {
                treeZHList.add(treeZH);
            }
        }
        return treeZHList;
    }

    public TreeZH menuToTreeZhOne(RefMenu one) {
        if (one == null) {
            return null;
        }
        TreeZH treeZHNew = new TreeZH();
        treeZHNew.setOrderNum(one.getSort());
        treeZHNew.setPid(one.getMenuPid());
        treeZHNew.setId(one.getCode());
        treeZHNew.setAuthority(one.getAuthority());
        treeZHNew.setIcon(one.getIcon());
        treeZHNew.setCreatedTime(one.getCreate_time());
        treeZHNew.setLever(one.getMenuType());
        treeZHNew.setMenu_desc(one.getMenuDesc());
        if ("false".equals(one.getIsHidden())) {
            treeZHNew.setIsHidden(Boolean.FALSE);
        } else {
            treeZHNew.setIsHidden(Boolean.TRUE);
        }

        treeZHNew.setPath(one.getMenuPath());
        if ("false".equals(one.getState())) {
            treeZHNew.setState(Boolean.FALSE);
        } else {
            treeZHNew.setState(Boolean.TRUE);
        }
        treeZHNew.setTitle(one.getMenuName());
        treeZHNew.setUpdatedTime(one.getUpdate_time());

        treeZHNew.setMenuClass(one.getMenuClass());
        treeZHNew.setMenuClass(one.getMenuClass());

        return treeZHNew;
    }

    public RefMenu getOneByParms(RefMenu refMenu) {
        return (RefMenu) refMenuMapper.findOneByParams(refMenu);
    }

    public List<RefMenu> findListByParams(RefMenu refMenu) {
        return refMenuMapper.findListByParams(refMenu);
    }

}
