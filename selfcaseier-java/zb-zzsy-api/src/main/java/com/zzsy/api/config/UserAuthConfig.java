package com.zzsy.api.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import com.lujie.common.constant.ConfigProperties;
import com.lujie.common.entity.EntityMap;
import com.lujie.common.util.StringUtil;

@Configuration
@PropertySource("classpath:application-userauth.properties")
public class UserAuthConfig {
	
	public static Long AccessMinintervalTime=100L;//毫秒
	public static Long AccessLockTime=30000L;//毫秒
	public static String BgPicHost;
	public static String BgPicStoragePath;
	public static String UploadImageWebPath;
	
	@Value("${com.lujie.userbase.pic.host}")
	public void setBgPicHost(String picHost) {
		BgPicHost = picHost;
	}

	
	@Value("${com.lujie.userbase.pic.storagePath}")
	public void setBgPicStoragePath(String storagePath) {
		BgPicStoragePath = storagePath;
	} 

	
	@Value("${com.lujie.userbase.pic.uploadImageWebPath}")
	public void setUploadImageWebPath(String uploadImageWebPath) {
		UploadImageWebPath = uploadImageWebPath;
	} 

	@Value("${com.lujie.mmftransferstation.app.AppKey}")
	public void setAppKey(String appKey) {
		ConfigProperties.AppKey=appKey;
	}
	
	@Value("${com.lujie.mmftransferstation.app.AppSecret}")
	public void setAppSecret(String appSecret) {
		ConfigProperties.AppSecret=appSecret;
	}
	

	public static  String  TOKEN ;
	@Value("${com.lujie.mmftransferstation.lujie.token}")
	public void setToken(String token) {
		TOKEN = token ;
	}

	public static  String  DATA_URL ;
	@Value("${com.lujie.mmftransferstation.data.url}")
	public void setDataUrl(String dataUrl) {
		DATA_URL = dataUrl ;
	}
	
	public static  String  Systoken;
	@Value("${com.lujie.userbase.systoken}")
	public void setSystoken(String systoken) {
		Systoken = systoken ;
	}
	
	public static String getSysUserId() {
		if(StringUtil.isNotEmpty(Systoken)){
			String[] tokeninfo=Systoken.split(":");
			return tokeninfo[0];
		}
		return null;
	}
	
	public static EntityMap ConfigMap=new EntityMap();
	@Value("${com.lujie.userbase.icomet.url}")
	public void setIcometUrl(String icometUrl) {
		if(icometUrl==null) return;
		ConfigMap.put("IcometUrl", icometUrl.trim());
	}
	
	public static  String  HostUrl;
	@Value("${com.lujie.userbase.host.url}")
	public void setHostUrl(String hostUrl) {
		HostUrl = hostUrl ;
	}
	
	public static  String  JobUrl;
	@Value("${com.lujie.userbase.job.url}")
	public void setJobUrl(String jobUrl) {
		JobUrl = jobUrl ;
	}
	
	
	@Value("${com.lujie.userbase.host}")
	public void setHost(String host) {
		ConfigMap.put("host", host);
	}
	
	@Value("${com.lujie.userbase.service.host}")
	public void setServiceHost(String serviceHost) {
		ConfigMap.put("ServiceHost", serviceHost);
	}
}
