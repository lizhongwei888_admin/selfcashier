package com.zzsy.api.service;

import com.lujie.common.service.BaseService;
import com.zzsy.api.entity.RefApi;
import com.zzsy.api.entity.RefApiItem;
import com.zzsy.api.mapper.RefApiItemMapper;
import com.zzsy.api.mapper.RefApiMapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class RefApiService extends BaseService {

    @Autowired
    private RefApiMapper refApiMapper;
    @Autowired
    private RefApiItemMapper refApiItemMapper;
    @Autowired
    private RedisTemplate redisTemplate;


    public List<RefApi> findAll(RefApi refApi) {
        return refApiMapper.findAll(refApi);
    }

    public RefApi findOne(RefApi refApi) {
        RefApi one = refApiMapper.findOne(refApi);
        if (one == null) {
            return null;
        }
        if (one.getNeedSubset().equals(0)) {
            return one;
        }
        RefApiItem refApiItem = new RefApiItem();
        refApiItem.setApiid(one.getCode());
        List<RefApiItem> listByApiId = refApiItemMapper.findListByApiId(refApiItem);
        if (CollectionUtils.isNotEmpty(listByApiId)) {
            if (one.getNeedSubset().equals(1)) {
                one.setListSubset(listByApiId);
            }
            if (one.getNeedSubset().equals(2)) {
                Map<String, String> collect = listByApiId.stream().collect(Collectors.toMap(RefApiItem::getItemkey, RefApiItem::getItemvalue));
                one.setMapSubset(collect);
            }
        }
        return one;
    }

    public Map<String, String> findApiItem(String apiCode) {
        RefApi refApi = new RefApi();
        refApi.setApicode(apiCode);
        RefApi api = refApiMapper.findOne(refApi);
        if(api==null){
            return null;
        }
        RefApiItem item = new RefApiItem();
        item.setApiid(api.getCode());
        List<RefApiItem> list = refApiItemMapper.findAll(item);
        Map<String, String> m = new HashMap<>();
        for (int i = 0; i < list.size(); i++) {
            RefApiItem item1 = list.get(i);
            m.put(item1.getItemkey(), item1.getItemvalue());
        }
        return m;
    }


    /**
     * 添加或修改
     *
     * @param refApi
     * @return
     */
    public int addOrUp(RefApi refApi) {
        if (StringUtils.isEmpty(refApi.getCode())) {
            //插入唯一code
            refApi.setCode(UUID.randomUUID().toString().replace("-", ""));
            return refApiMapper.add(refApi);
        } else {
            refApi.setUpdate_time(new Date());
            int up = refApiMapper.up(refApi);
            return up;
        }

    }

    public void delete(String codes) {
        Set<String> collect = Arrays.stream(codes.split(",")).collect(Collectors.toSet());
        refApiMapper.deleteByIdSet(collect);
    }

    public List<RefApi> findPageList(RefApi refApi) {
        return refApiMapper.findPageList(refApi);
    }

    public Long countTotal(RefApi refApi) {
        return refApiMapper.countTotal(refApi);
    }
}
