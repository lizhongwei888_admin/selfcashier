package com.zzsy.api.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
 
@Configuration
public class RedisConfig extends CachingConfigurerSupport {
 
    
	@Value("${spring.redis.host}")
	private String host;
 
	@Value("${spring.redis.port}")
	private int port;
 
    @Value("${spring.redis.password}")
	private String password;
 
    @Value("${spring.redis.timeout}")
	private Integer timeout;

	@Value("${spring.redis.databaseId}")
	private Integer databaseId;

	@Value("${spring.redis.namespace}")
	private String namespace;
    
    @Value("${spring.redis.pool.maxTotal}")
    private int maxTotal;
    
    @Value("${spring.redis.pool.maxIdle}")
    private int maxIdle;
    
    @Value("${spring.redis.pool.minIdle}")
    private int minIdle;
    
    @Value("${spring.redis.pool.maxWaitMillis}")
    private long maxWaitMillis;

 
    public JedisPool redisPoolFactory() {
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        jedisPoolConfig.setMaxTotal(maxTotal);
        jedisPoolConfig.setMaxIdle(maxIdle);
        jedisPoolConfig.setMinIdle(minIdle);
        JedisPool jedisPool = new JedisPool(jedisPoolConfig, host, port, timeout, password);
        return jedisPool;
    }
}
