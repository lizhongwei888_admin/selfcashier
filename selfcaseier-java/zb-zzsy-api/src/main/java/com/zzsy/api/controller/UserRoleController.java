package com.zzsy.api.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.util.JsonHelper;
import com.zzsy.api.entity.Role;
import com.zzsy.api.entity.UserRole;
import com.zzsy.api.service.UserRoleService;

/**
 * 角色控制器
 * @author hupei
 *
 */
@Controller
@RequestMapping(value = "/userrole")
public class UserRoleController  {
	
	@Autowired
	private UserRoleService userRoleService;

	@RequestMapping(value={"/list"})
	@ResponseBody
	public String list(@RequestBody UserRole userRole){
		
		List<UserRole> roleList=userRoleService.findPageList(userRole);
		Long totalCount=userRoleService.countTotal(userRole);
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success,roleList,totalCount));
	}
	
	@RequestMapping(value = "/batchSaveUserRole", method = RequestMethod.POST)
	@ResponseBody
	public String batchSaveUserRole(@RequestBody Role role , HttpServletRequest request){
		return JsonHelper.toJson(userRoleService.batchSaveUserRole(role, request));
	}
	@RequestMapping(value = "/batchDeleteUserRole", method = RequestMethod.POST)
	@ResponseBody
	public String batchDeleteUserRole(@RequestBody Role role , HttpServletRequest request){
		return JsonHelper.toJson(userRoleService.batchDeleteUserRole(role, request));
	}
}
