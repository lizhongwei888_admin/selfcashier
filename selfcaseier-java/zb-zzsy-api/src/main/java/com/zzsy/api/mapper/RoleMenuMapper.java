package com.zzsy.api.mapper;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.lujie.common.dao.BasicMapper;
import com.zzsy.api.entity.RoleMenu;
@Repository
public interface RoleMenuMapper extends BasicMapper{
	int deleteByPrimaryKey(Integer id);

    int insertSelective(RoleMenu record);

    RoleMenu selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(RoleMenu record);
    
    List<RoleMenu> selectByRidMid(Map<String, String> map);
    
}