package com.zzsy.api.controller;

import javax.servlet.http.HttpServletRequest;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.util.JsonHelper;
import com.zzsy.api.entity.RefLogAdmin;
import com.zzsy.api.entity.RefStorage;
import com.zzsy.api.entity.RefUser;
import com.zzsy.api.service.RefLogAdminService;
import com.zzsy.api.service.RefStorageService;
import com.zzsy.api.util.RedisRefUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zzsy.api.service.UploadService;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@Controller
@RequestMapping(value = "/upload")
public class UploadController {

	@Autowired
	private UploadService uploadService;
	@Autowired
	private RedisTemplate redisTemplate;
	@Autowired
	private RefStorageService refStorageService;
	@Autowired
	private RefLogAdminService refLogAdminService;

	/**
	 * 仅上传文件
	 *
	 * @param request
	 * @return 返回文件路径
	 */
	@RequestMapping(value = {"/file"})
	@ResponseBody
	public String upload(HttpServletRequest request) {
		//TODO 参数控制本地还是obs服务器
		return uploadService.uploadFileOBS(request, 1);
	}


	/**
	 * 上传文件且记录存入存储表
	 *
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/uploadRefStorage", method = RequestMethod.POST)
	@ResponseBody
	public String addRefStorage(HttpServletRequest request) {
		RefStorage refStorage = new RefStorage();
		//TODO 参数控制本地还是obs服务器
//		String s = uploadService.uploadFile(request, 1);
		String s = uploadService.uploadFileOBS(request, 0);
		ResultMsg resultMsg = JsonHelper.toObject(s, ResultMsg.class);
		if (!resultMsg.getCode().equals("200")) {
			return JsonHelper.toJson(new ResultMsg("300", "添加失败"));
		}
		Map<String, String> data = JSONObject.parseObject(JSON.toJSONString(resultMsg.getData()), Map.class);
		String picUrl = data.get("picUrl");

		refStorage.setFileName(data.get("fileName"));//文件名称
		//如果文件没有后缀名,加一个
		Integer typeIndexName = refStorage.getFileName().lastIndexOf(".");
		if (typeIndexName == -1) {
			refStorage.setFileName(data.get("fileName") + "." + data.get("fileType"));//文件名称
		}
		refStorage.setFileType(data.get("fileType"));//文件后缀
		refStorage.setFileSize(new BigDecimal(data.get("fileSize")));//文件大小
		refStorage.setFileUrl(picUrl);//文件地址
		refStorage.setRemark("无");
		RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
		refStorage.setCreateby(loginRefUser.getCode());
		refStorage.setCreate_time(new Date());
		refStorage.setUpdateby(loginRefUser.getCode());
		refStorage.setUpdate_time(new Date());
		refStorage.setOrgID(loginRefUser.getOrgID());
		if (refStorageService.addOrUp(refStorage) <= 0) {
			return JsonHelper.toJson(new ResultMsg("300", "添加失败"));
		}

		this.refLogAdminService.create(new RefLogAdmin(loginRefUser.getCode()
				, "存储管理", "上传文件", "uploadRefStorage", "/upload/uploadRefStorage"
				, data, refStorage, loginRefUser.getOrgID()));

		return JsonHelper.toJson(new ResultMsg(ResultCode.Success, refStorage));

	}


}
