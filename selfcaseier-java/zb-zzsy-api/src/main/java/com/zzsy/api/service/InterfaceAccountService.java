package com.zzsy.api.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.zzsy.api.constant.KeysManager;
import com.zzsy.api.constant.LoginType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import com.lujie.common.constant.ConfigProperties;
import com.lujie.common.service.BasicService;
import com.zzsy.api.entity.User;
import com.zzsy.api.entity.UserModuleRole;

@Service
public class InterfaceAccountService  extends BasicService  {
	
	public static final String  InterfaceAccountKey="InterfaceAccount";
	public static final String  InterfaceAccountModuleRoleKey="InterfaceAccountModuleRole";
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private UserModuleRoleService userModuleRoleService;
	
	@Autowired 
	private RedisTemplate redisTemplate;
	
	public User getInterfaceUser(HttpServletRequest request){
		String account=request.getHeader("account");
		//RedisUtil.deleteMiddleLike(redisTemplate, KeysManager.User_Auth+InterfaceAccountModuleRoleKey);
		User user=(User)redisTemplate.opsForValue().get(KeysManager.User_Auth+InterfaceAccountModuleRoleKey+"_"+account);
		if(user==null){
			user=userService.findByUserName(account);
			if(user!=null && user.getLoginType()!=null && user.getLoginType().equals(LoginType.InterfaceUser.getCode())){
				List<UserModuleRole> userModuleRoles=userModuleRoleService.findByUserId(user.getId());
				user.setUserModuleRoles(userModuleRoles);
				user.setModuleRoleIds(userModuleRoleService.getModuleRoleIds(userModuleRoles));
				
				redisTemplate.opsForValue().set(KeysManager.User_Auth+InterfaceAccountModuleRoleKey+"_"+user.getUsername(), user);
			}
		}
		if(user!=null){
			ConfigProperties.AccountMap.put(user.getUsername(), user.getPassword());
		}
		return user;
	}
	
	
	 
}
