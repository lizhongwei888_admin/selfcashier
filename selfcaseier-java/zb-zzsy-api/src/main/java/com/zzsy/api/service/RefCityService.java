package com.zzsy.api.service;

import com.lujie.common.service.BaseService;
import com.zzsy.api.entity.RefCity;
import com.zzsy.api.mapper.RefCityMapper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class RefCityService extends BaseService {

    @Autowired
    private RefCityMapper refCityMapper;

    /**
     * 添加或修改
     *
     * @param refCity
     * @return
     */
    public int addOrUp(RefCity refCity) {
        if (StringUtils.isEmpty(refCity.getCode())) {
            //插入唯一code
            refCity.setCode(UUID.randomUUID().toString().replace("-", ""));
            return refCityMapper.add(refCity);
        } else {
            refCity.setUpdate_time(new Date());
            int up = refCityMapper.up(refCity);
            return up;
        }

    }

    public void delete(String codes) {
        Set<String> collect = Arrays.stream(codes.split(",")).collect(Collectors.toSet());
        refCityMapper.deleteByIdSet(collect);
    }

    public List<RefCity> findPageList(RefCity refCity) {
        return refCityMapper.findPageList(refCity);
    }

    public Long countTotal(RefCity refCity) {
        return refCityMapper.countTotal(refCity);
    }


    /**
     * 返回前端省市区三级架构
     *
     * @return
     */
    public List<RefCity> findAllRefCity() {
        List<RefCity> allRefCity = refCityMapper.findAllRefCity();
        if (CollectionUtils.isEmpty(allRefCity)) {
            return new ArrayList<>();
        }
        Map<Integer, List<RefCity>> collect = allRefCity.stream()
                .collect(Collectors.groupingBy(RefCity::getCityType));
        //取出省市区对应的集合 城市类型：1省；2市；3区
        List<RefCity> refCitysProvincial = collect.get(1);
        if (CollectionUtils.isEmpty(refCitysProvincial)) {
            //无省一级数据
            return new ArrayList<>();
        }
        List<RefCity> refCitys = collect.get(2);
        List<RefCity> refCitiDistrict = collect.get(3);

        assembleCityData(refCitys, refCitiDistrict);
        assembleCityData(refCitysProvincial, refCitys);

        return refCitysProvincial;
    }

    private void assembleCityData(List<RefCity> cityListOne, List<RefCity> cityListTwo) {
        if (CollectionUtils.isEmpty(cityListOne) || CollectionUtils.isEmpty(cityListTwo)) {
            return;
        }
        Map<String, List<RefCity>> collect = cityListTwo.stream()
                .collect(Collectors.groupingBy(RefCity::getCityPid));
        for (RefCity refCity : cityListOne) {
            if (!collect.containsKey(refCity.getCode())) {
                continue;
            }
            refCity.setNextLevelData(collect.get(refCity.getCode()));
        }
    }

}
