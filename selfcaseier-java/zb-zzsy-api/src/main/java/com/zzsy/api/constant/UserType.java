package com.zzsy.api.constant;

/**
 * 申请类型
 * @author sl
 *
 */
public enum UserType  {

	Account(1, "账户管理"),
	Menu(2, "菜单管理"),
	Organization(3, "组织管理"),
	Role(4, "角色管理"),
	Department(5, "部门管理")
	;
	
	private Integer code;

	
	private String desc;

	UserType(Integer code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public Integer getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}
	
	public static String getDesc(Integer code) {
		
		UserType[] logTypes = values();
		for(UserType logType : logTypes) {
			if(code.equals(logType.getCode())) {
				return logType.getDesc();
			}
		}
		return "";
	}

}
