package com.zzsy.api.constant;

/**
 * 申请类型
 * @author sl
 *
 */
public enum OperType  {
	AddType(0, "新增"),
	ModifyType(1, "修改"),
	DeleteType(2, "删除"),
	SelectType(3, "查询"),
	ImportType(4, "导入"),
	ExportType(5, "导出")
	;
	
	private Integer code;

	
	private String desc;

	OperType(Integer code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public Integer getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}
	
	public static String getDesc(Integer code) {
		
		OperType[] logTypes = values();
		for(OperType logType : logTypes) {
			if(code.equals(logType.getCode())) {
				return logType.getDesc();
			}
		}
		return "";
	}

}
