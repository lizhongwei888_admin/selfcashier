package com.zzsy.api.controller;

import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.util.JsonHelper;
import com.zzsy.api.entity.RefRole;
import com.zzsy.api.entity.RefUser;
import com.zzsy.api.service.RefRoleService;
import com.zzsy.api.service.RefUserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * 用户登录退出
 */
@Controller
@RequestMapping(value = "/refAdmin")
public class RefAdminController {

    @Autowired
    private RefUserService refUserService;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private RefRoleService refRoleService;

    /**
     * 登陆
     *
     * @param refUser
     * @return
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public String login(@RequestBody RefUser refUser) {
        return JsonHelper.toJson(refUserService.login(refUser));
    }

    /**
     * 注销
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/logout")
    @ResponseBody
    public String logout(HttpServletRequest request) {
        return JsonHelper.toJson(refUserService.logout(request));
    }


    /**
     * 获取用户的角色
     *
     * @param refRole
     * @return
     */
    @PostMapping(value = "/getRefUserRole")
    @ResponseBody
    public String refUserRole(@RequestBody RefRole refRole) {
        if (StringUtils.isEmpty(refRole.getCode())) {
            return JsonHelper.toJson(new ResultMsg("300", "code参数为空!"));
        }
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success, refRoleService.getOneByCode(refRole.getCode())));
    }

}
