package com.zzsy.api.service;

import com.lujie.common.service.BaseService;
import com.zzsy.api.entity.RefLogAdmin;
import com.zzsy.api.entity.RefLogApp;
import com.zzsy.api.mapper.RefLogAppMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.util.List;
import java.util.UUID;

@Service
public class RefLogAppService extends BaseService {

    private static final Logger log = LoggerFactory.getLogger(RefLogAppService.class);

    @Autowired
    private RefLogAppMapper refLogAppMapper;

    //插入日志
    public void create(RefLogApp refLogApp) {
        try {
            refLogApp.setCode(UUID.randomUUID().toString().replaceAll("-", ""));
            InetAddress ip4 = Inet4Address.getLocalHost();
            refLogApp.setIp(ip4.getHostAddress());
            refLogAppMapper.add(refLogApp);
        } catch (Exception e) {
            log.error("RefLogAppService-create异常", e);
        }
    }

    public List<RefLogApp> findPageList(RefLogApp refLogApp) {
        return refLogAppMapper.findPageList(refLogApp);
    }

    public Long countTotal(RefLogApp refLogApp) {
        return refLogAppMapper.countTotal(refLogApp);
    }

    public List<RefLogApp> findList(RefLogApp refLogApp) {
        return refLogAppMapper.findList(refLogApp);
    }
}
