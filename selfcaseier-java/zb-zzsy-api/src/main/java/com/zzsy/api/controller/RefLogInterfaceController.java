package com.zzsy.api.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.util.ExcelReaderUtil;
import com.lujie.common.util.JsonHelper;
import com.zzsy.api.entity.RefLogAdmin;
import com.zzsy.api.entity.RefLogApp;
import com.zzsy.api.entity.RefLogInterface;
import com.zzsy.api.entity.RefUser;
import com.zzsy.api.excel.RefLogInterfaceExcel;
import com.zzsy.api.service.RefLogAdminService;
import com.zzsy.api.service.RefLogAppService;
import com.zzsy.api.service.RefLogInterfaceService;
import com.zzsy.api.service.RefUserService;
import com.zzsy.api.util.RedisRefUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

/**
 * 系统-第三方接口日志控制器
 */
@Controller
@RequestMapping(value = "/refLogInterface")
public class RefLogInterfaceController {
    private static final Logger log = LoggerFactory.getLogger(RefLogInterfaceController.class);

    @Autowired
    private RefLogInterfaceService refLogInterfaceService;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private RefLogAdminService refLogAdminService;

    /**
     * 后端分页查询集合
     *
     * @param object
     * @return
     */
    @RequestMapping(value = "/listRefLogInterface", method = RequestMethod.POST)
    @ResponseBody
    public String listRefLogInterface(@RequestBody JSONObject object) {
        RefLogInterface refLogInterface = JSONObject.parseObject(JSON.toJSONString(object.getJSONObject("params")), RefLogInterface.class);
        if (object.getInteger("offset") != null && object.getInteger("limit") != null) {
            refLogInterface.setPageNum(object.getInteger("offset"));
            refLogInterface.setPageSize(object.getInteger("limit"));
        }
        JSONObject sort = object.getJSONObject("sort");
        if (sort != null) {
            String prop = sort.getString("prop");
            String order = sort.getString("order");
            refLogInterface.setProp(prop);
            refLogInterface.setOrder(order);
        }
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success, refLogInterfaceService.findPageList(refLogInterface), refLogInterfaceService.countTotal(refLogInterface)));
    }

    @PostMapping(value = "/listRefLogInterfaceExport")
    @ResponseBody
    public String listRefLogInterfaceExport(@RequestBody JSONObject object, HttpServletRequest request, HttpServletResponse response) {
        RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
        RefLogInterface refLogInterface = JSONObject.parseObject(JSON.toJSONString(object.getJSONObject("params")), RefLogInterface.class);
        JSONObject sort = object.getJSONObject("sort");
        if (sort != null) {
            String prop = sort.getString("prop");
            String order = sort.getString("order");
            refLogInterface.setProp(prop);
            refLogInterface.setOrder(order);
        }
        // 查询列表
        List<RefLogInterface> result = refLogInterfaceService.findList(refLogInterface);
        // 类型转换
        List<RefLogInterfaceExcel> list = JSONArray.parseArray(JSON.toJSONString(result), RefLogInterfaceExcel.class);
        try {
            // 设置响应头信息
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.setCharacterEncoding("utf-8");
            String fileName = URLEncoder.encode("第三方接口日志信息表", "UTF-8").replaceAll("\\+", "%20");
            response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + "_" + System.currentTimeMillis() + ".xlsx");
            // 使用EasyExcel进行数据导出
            EasyExcel.write(response.getOutputStream(), RefLogInterfaceExcel.class).sheet("导出数据").doWrite(list);

            this.refLogAdminService.create(new RefLogAdmin(loginRefUser.getCode()
                    , "三方日志管理界面", "导出", "listRefLogInterfaceExport", "/refLogInterface/listRefLogInterfaceExport"
                    , JSON.toJSONString(result), "ok", loginRefUser.getOrgID()));
            return JsonHelper.toJson(new ResultMsg(ResultCode.Success));
        } catch (IOException e) {
            log.error("listRefLogInterfaceExport异常:", e);
            return JsonHelper.toJson(new ResultMsg("300", "导出异常"));
        }
    }

}
