package com.zzsy.api.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.util.JsonHelper;
import com.zzsy.api.entity.UserModuleRole;
import com.zzsy.api.service.UserModuleRoleService;

/**
 * 用户-模块角色控制器
 * @author hupei
 *
 */
@Controller
@RequestMapping(value = "/usermodulerole")
public class UserModuleRoleController  {
	
	@Autowired
	private UserModuleRoleService userModuleRoleService;
	
	@RequestMapping(value={"/findAll"})
	@ResponseBody
	public String findAll(@RequestBody UserModuleRole userModuleRole){
		List<UserModuleRole> list=userModuleRoleService.findList(userModuleRole);
		Long totalCount=Long.parseLong(list.size()+"");
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success,list,totalCount));
	}
	/**
	 * 用户模块角色
	 * @return
	 */
	@RequestMapping(value={"/list"})
	@ResponseBody
	public String list(@RequestBody UserModuleRole userModuleRole){
		List<UserModuleRole> list=userModuleRoleService.findList(userModuleRole);
		Long totalCount=userModuleRoleService.countTotal(userModuleRole);
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success,list,totalCount));
	}
	
	/**
	 * 添加用户模块角色
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ResponseBody
	public String add(@RequestBody UserModuleRole userModuleRole,HttpServletRequest request){
		return JsonHelper.toJson(userModuleRoleService.save(userModuleRole,request));
	} 
	
	/**
	 * 删除用户模块角色
	 * @param roleid
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public String remove(@RequestBody UserModuleRole userModuleRole,HttpServletRequest request){
		return JsonHelper.toJson(userModuleRoleService.delete(userModuleRole,request));
	}
	
	 
}
