package com.zzsy.api.entity;

import java.io.Serializable;
import java.util.List;

import com.lujie.common.entity.BasicEntity;
/**
 * 菜单的角色
 * @author sl
 *
 */
public class Role extends BasicEntity  implements Serializable{
	private static final long serialVersionUID = -385429157197792921L;

	private String id;

    private String text;
    private String flag;
    
    private List<RoleMenu> roleMenuList;
    private List<UserRole> userRoleList;
    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text == null ? null : text.trim();
    }

	public List<RoleMenu> getRoleMenuList() {
		return roleMenuList;
	}

	public void setRoleMenuList(List<RoleMenu> roleMenuList) {
		this.roleMenuList = roleMenuList;
	}

	public List<UserRole> getUserRoleList() {
		return userRoleList;
	}

	public void setUserRoleList(List<UserRole> userRoleList) {
		this.userRoleList = userRoleList;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}
	
	
    
    
}