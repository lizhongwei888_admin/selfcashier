package com.zzsy.api.service;

import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.service.BasicService;
import com.zzsy.api.entity.DwOrderRefunddetail;
import com.zzsy.api.entity.DwOrderShopdetail;
import com.zzsy.api.mapper.DwOrderRefundDetailMapper;
import com.zzsy.api.mapper.DwOrderRefundMapper;
import com.zzsy.api.mapper.DwOrderShopdetailMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

@Service
public class DwOrderRefundDetailService extends BasicService {
	public static final Logger log = LoggerFactory.getLogger(DwOrderRefundDetailService.class);
	@Autowired
	private DwOrderRefundDetailMapper dwOrderRefundDetailMapper;
	@Resource(name = "dwOrderRefundDetailMapper")
	public void initBaseDao(DwOrderRefundDetailMapper dwOrderRefundDetailMapper) {
		super.setBaseMapper(dwOrderRefundDetailMapper);
	}

	public ResultMsg insertData(DwOrderRefunddetail dwOrderRefunddetail){
		dwOrderRefundDetailMapper.insertData(dwOrderRefunddetail);
		return new ResultMsg(ResultCode.Success);
	}


}
