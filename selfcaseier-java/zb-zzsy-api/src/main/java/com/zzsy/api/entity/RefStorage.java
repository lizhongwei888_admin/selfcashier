package com.zzsy.api.entity;

import com.lujie.common.entity.BasicEntity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;

/**
 * 存储管理
 */
public class RefStorage extends BasicEntity implements Serializable {

    private static final long serialVersionUID = -301062307193761449L;

    //唯一标识
    private String code;
    //文件名称
    private String fileName;
    //文件后缀
    private String fileType;
    //文件大小
    private BigDecimal fileSize;
    //地址
    private String fileUrl;
    //备注
    private String remark;

    private String username;

    private Set<String> fileTypeSet;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public BigDecimal getFileSize() {
        return fileSize;
    }

    public void setFileSize(BigDecimal fileSize) {
        this.fileSize = fileSize;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    @Override
    public String getRemark() {
        return remark;
    }

    @Override
    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Set<String> getFileTypeSet() {
        return fileTypeSet;
    }

    public void setFileTypeSet(Set<String> fileTypeSet) {
        this.fileTypeSet = fileTypeSet;
    }
}
