package com.zzsy.api.controller;

import com.alibaba.fastjson.JSONObject;
import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.entity.EntityMap;
import com.lujie.common.util.JsonHelper;
import com.zzsy.api.entity.*;
import com.zzsy.api.entityVo.DwSyCashVo;
import com.zzsy.api.service.DwSyCashService;
import com.zzsy.api.service.RefLogAdminService;
import com.zzsy.api.util.RedisRefUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

@Controller
@RequestMapping(value = "/dwSyCash")
public class DwSyCashController {

	@Autowired
	private DwSyCashService dwSyCashService;

	@Autowired
	private RefLogAdminService refLogAdminService;

	@Autowired
	private RedisTemplate redisTemplate;

	@RequestMapping(value={"/getCashList"})
	@ResponseBody
	public String getCashList(@RequestBody DwSyCashVo data){
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success,dwSyCashService.findPageList(data),dwSyCashService.countTotal(data)));
	}

	@RequestMapping(value={"/findByDeviceCode"})
	@ResponseBody
	public String findByDeviceCode(HttpServletRequest request, @RequestBody DwSyCash dwSyCash){

		Map<String,Object> dw =dwSyCashService.findByDeviceCode(dwSyCash);
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success,dw));
	}

	@RequestMapping(value={"/insertData"})
	@ResponseBody
	public String insertData(HttpServletRequest request, @RequestBody DwSyCash dwSyCash){
		dwSyCash.setCode(UUID.randomUUID().toString());
		dwSyCash.setState("true");
		dwSyCash.setCreate_time(new Date());
		//收银机编号
		String no= dwSyCashService.getSyCashNo(dwSyCash).get("code").toString();
		dwSyCash.setCashCode1(dwSyCash.getCashCode()+no);
		return JsonHelper.toJson(dwSyCashService.insertData(request,dwSyCash));


	}

	@RequestMapping(value={"/auditData"})
	@ResponseBody
	public String auditData(HttpServletRequest request, @RequestBody DwSyCash dwSyCash){
		dwSyCash.setAuditTime(new Date());
		RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
		dwSyCash.setAuditer(loginRefUser.getCode());
		return JsonHelper.toJson(dwSyCashService.auditData(dwSyCash));
	}


	@RequestMapping(value = "/updateState", method = RequestMethod.POST)
	@ResponseBody
	public String updateState(@RequestBody DwSyCash dwSyCash, HttpServletRequest request) {
		String code = dwSyCash.getCode();
		if (StringUtils.isEmpty(code)) {
			return JsonHelper.toJson(new ResultMsg("300", "code不能为空"));
		}
		RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
		dwSyCash = (DwSyCash) dwSyCashService.findOne(dwSyCash);
		if ("false".equals(dwSyCash.getState())) {
			dwSyCash.setState("true");
		} else {
			dwSyCash.setState("false");
		}
		dwSyCash.setUpdateby(loginRefUser.getCode());
		if (dwSyCashService.addOrUp(dwSyCash) <= 0) {
			return JsonHelper.toJson(new ResultMsg("300", "修改状态失败"));
		}
		this.refLogAdminService.create(new RefLogAdmin(loginRefUser.getCode()
				, "收银机管理页面", "修改状态", "state", "/dwSyCash/updateState"
				, dwSyCash, "ok", loginRefUser.getOrgID()));
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success));
	}

	/**
	 * 删除
	 *
	 * @param object
	 * @return
	 */
	@RequestMapping(value = "/delCash", method = RequestMethod.POST)
	@ResponseBody
	public String delCash(HttpServletRequest request, @RequestBody JSONObject object) {

		String codes = object.getString("codes");
		if (StringUtils.isEmpty(codes)) {
			return JsonHelper.toJson(new ResultMsg("300", "code不能为空"));
		}
		RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);

		if (dwSyCashService.delete(codes) <= 0) {
			return JsonHelper.toJson(new ResultMsg("300", "删除失败"));
		}

		this.refLogAdminService.create(new RefLogAdmin(loginRefUser.getCode()
				, "收银机管理页面", "删除收银机", "delCash", "/dwSyCash/delCash"
				, codes, "ok", loginRefUser.getOrgID()));
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success));
	}



}
