package com.zzsy.api.constant;

/**
 * 申请类型
 * @author sl
 *
 */
public enum LogType  {

	UserType(1, "用户类型"),
	Basics(2, "基础数据"),
	Business(3, "业务数据"),
	ThirdAPI(4, "第三方接口"),
	Module(5, "模块交互")
	;
	
	private Integer code;

	
	private String desc;

	LogType(Integer code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public Integer getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}
	
	public static String getDesc(Integer code) {
		
		LogType[] logTypes = values();
		for(LogType logType : logTypes) {
			if(code.equals(logType.getCode())) {
				return logType.getDesc();
			}
		}
		return "";
	}

}
