package com.zzsy.api.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import com.zzsy.api.constant.BasicsType;
import com.zzsy.api.constant.KeysManager;
import com.zzsy.api.constant.LogType;
import com.zzsy.api.entity.CodeTable;
import com.zzsy.api.entity.Log;
import com.zzsy.api.entity.User;
import com.zzsy.api.mapper.CodeTableMapper;
import com.zzsy.api.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.entity.EntityMap;
import com.lujie.common.service.BasicService;
import com.lujie.common.util.CollectionUtil;
import com.lujie.common.util.StringUtil;

@Service
public class CodeTableService extends BasicService{
	
	@Autowired
	private LogService logService;
	
	
	@Autowired 
	private RedisTemplate redisTemplate;
	
	@Autowired
	private CodeTableMapper codeTableMapper;
	@Resource(name="codeTableMapper")
	public void initBaseDao(CodeTableMapper codeTableMapper){
		super.setBaseMapper(codeTableMapper);
	}
	
	public List<CodeTable> getAllType(){
		return codeTableMapper.getAllType();
	}
	
	public ResultMsg save(HttpServletRequest request, CodeTable codeTable){
		if(StringUtil.isEmpty(codeTable.getCodeType())){
			return new ResultMsg("301","CodeType不能为空");
		}
		if(StringUtil.isEmpty(codeTable.getCodeDesc())){
			return new ResultMsg("302","CodeDesc不能为空");
		}
		if(StringUtil.isEmpty(codeTable.getCodeKey())){
			return new ResultMsg("303","CodeKey不能为空");
		}
		if(StringUtil.isEmpty(codeTable.getCodeName())){
			return new ResultMsg("304","CodeName不能为空");
		}
		if(findByTypeKey(codeTable)!=null){
			return new ResultMsg("305","CodeType、CodeKey的关联已经存在");
		}
		codeTableMapper.save(codeTable);
		RedisUtil.deleteMiddleLike(redisTemplate, KeysManager.User_Auth+KeysManager.CodeTbale);
		
		User admin=RedisUtil.getUser(redisTemplate, request);
		
		//logService.create(admin, "添加codetable"); 
		this.logService.create(new Log(admin.getId(), "添加codetable", LogType.Basics.getDesc(), BasicsType.SystematicCode.getDesc(), 0, 0) );

		
		
		return new ResultMsg(ResultCode.Success);
	}
	public CodeTable findByTypeKey(String codeType,String codeKey){
		CodeTable tempCodeTable=new CodeTable();
		tempCodeTable.setCodeType(codeType);
		tempCodeTable.setCodeKey(codeKey);
		return (CodeTable)codeTableMapper.findOne(tempCodeTable);
	}
	
	public CodeTable findByTypeAndCodeName(String codeType,String codeName){
		CodeTable tempCodeTable=new CodeTable();
		tempCodeTable.setCodeType(codeType);
		tempCodeTable.setCodeName(codeName);
		return (CodeTable)codeTableMapper.findOne(tempCodeTable);
	}
	public List<CodeTable> findByType(String codeType){
		CodeTable tempCodeTable=new CodeTable();
		tempCodeTable.setCodeType(codeType);
		return codeTableMapper.findList(tempCodeTable);
	}
	
	public List<CodeTable> getGarbageTypeBySite(){
		return codeTableMapper.getGarbageTypeBySite();
	}
	
	
	public CodeTable findByTypeKey(CodeTable codeTable){
		CodeTable tempCodeTable=new CodeTable();
		tempCodeTable.setCodeType(codeTable.getCodeType());
		tempCodeTable.setCodeKey(codeTable.getCodeKey());
		return (CodeTable)codeTableMapper.findOne(tempCodeTable);
	}
	
	public ResultMsg update(HttpServletRequest request, CodeTable codeTable){
		if(StringUtil.isEmpty(codeTable.getCodeType())){
			return new ResultMsg("301","CodeType不能为空");
		}
		if(StringUtil.isEmpty(codeTable.getCodeKey())){
			return new ResultMsg("302","CodeKey不能为空");
		}
		if(findByTypeKey(codeTable)==null){
			return new ResultMsg("303","没有记录要更新");
		}
		codeTableMapper.update(codeTable);
		RedisUtil.deleteMiddleLike(redisTemplate, KeysManager.User_Auth+KeysManager.CodeTbale);
		
		User admin=RedisUtil.getUser(redisTemplate, request);
		
		//logService.create(admin, "更新codetable");
		this.logService.create(new Log(admin.getId(), "更新codetable", LogType.Basics.getDesc(), BasicsType.SystematicCode.getDesc(), 1, 0) );

		
		return new ResultMsg(ResultCode.Success);
	}
	public ResultMsg delete(HttpServletRequest request, CodeTable codeTable){
		if(StringUtil.isEmpty(codeTable.getCodeType())){
			return new ResultMsg("301","CodeType不能为空");
		}
		if(StringUtil.isEmpty(codeTable.getCodeKey())){
			return new ResultMsg("302","CodeKey不能为空");
		}
		if(findByTypeKey(codeTable)==null){
			return new ResultMsg("303","没有记录要删除");
		}
		codeTableMapper.delete(codeTable);
		RedisUtil.deleteMiddleLike(redisTemplate, KeysManager.User_Auth+KeysManager.CodeTbale);
		
		User admin=RedisUtil.getUser(redisTemplate, request);
		
		//logService.create(admin, "删除codetable");
		this.logService.create(new Log(admin.getId(), "删除codetable", LogType.Basics.getDesc(), BasicsType.SystematicCode.getDesc(), 2, 0) );

		
		
		return new ResultMsg(ResultCode.Success);
	}
	
	public Map<String,String> getColumnFunction(String columnComment){
		Map<String,String> columnFunMap=new HashMap<String,String>();
		if(StringUtil.isEmpty(columnComment)) return columnFunMap;
		
		String[] columnCommentArr=columnComment.split(":");
		columnFunMap.put("columnRemark", columnCommentArr[0]);
		
		if(columnCommentArr.length>1){
			for(int index=1;index<columnCommentArr.length;index++){
				if(columnCommentArr[index].equalsIgnoreCase("export")){
					columnFunMap.put("export", columnCommentArr[0]);
				}else if(columnCommentArr[index].contains("relativeselect(")){
					String functionStr=columnCommentArr[index].replace("relativeselect(", "").replace(")", "");
					columnFunMap.put("relativeSelect", functionStr);
				}else if(columnCommentArr[index].contains("unique(")){
					String functionStr=columnCommentArr[index].replace("unique(", "").replace(")", "");
					columnFunMap.put("unique", functionStr);
				}
			}
		}
		return columnFunMap;
	}
	
	public EntityMap getColumnsInfo(List<EntityMap> dbColumnList){
		EntityMap columnsInfo=new EntityMap();
		for(EntityMap dbEntityMap:dbColumnList){
			String dbColumnName=dbEntityMap.getStr("COLUMN_NAME");
			columnsInfo.put(dbColumnName, dbEntityMap);
		}
		return columnsInfo;
	}
	
	public List<EntityMap> getColumnInfoWithRelativeTable(String tableName){
		EntityMap entityMap=new EntityMap();
		entityMap.put("tableName", tableName);
		List<EntityMap> entityMapList=codeTableMapper.getColumnInfo(entityMap);
		
		
		
		if(CollectionUtil.isNotEmpty(entityMapList)){//关联表信息
			Integer relativeTableIndex=1;
			for(EntityMap dbEntityMap:entityMapList){
				String dbColumnName=dbEntityMap.getStr("COLUMN_NAME");
				
				String COLUMN_COMMENT=dbEntityMap.getStr("COLUMN_COMMENT");
				if(StringUtil.isEmpty(COLUMN_COMMENT)) continue;
				
				Map<String,String> columnFunctionMap=getColumnFunction(COLUMN_COMMENT);
				dbEntityMap.put("columnRemark", columnFunctionMap.get("columnRemark"));
				if(columnFunctionMap.size()==1) continue;
				
				String relativeSelect=columnFunctionMap.get("relativeSelect");
				dbEntityMap.put("export", columnFunctionMap.get("export"));
				dbEntityMap.put("relativeselect", relativeSelect);
				dbEntityMap.put("unique", columnFunctionMap.get("unique"));
				
				if(StringUtil.isEmpty(relativeSelect)) continue;
				String[] relateselectArr=relativeSelect.split(",");
				String relativetable=relateselectArr[0].trim();
				String relativecolumnid=relateselectArr[1].trim();
				String[] relativeselectcolumns=relateselectArr;
				
				String relativetableShortName="t"+relativeTableIndex;
				String[] relativetableArr=relativetable.split(" ");
				if(relativetableArr.length>1){
					relativetable=relativetableArr[0];
					relativetableShortName=relativetableArr[1];
				}
				
				dbEntityMap.put("relativeTableName", relativetable);
				dbEntityMap.put("relativeTableShortName", relativetableShortName);
				dbEntityMap.put("relativeColumnId", relativecolumnid);
				dbEntityMap.put("relativeSelectColumns", relativeselectcolumns);
				
				
				EntityMap relativeentityMap=new EntityMap();
				relativeentityMap.put("tableName", relativetable);
				List<EntityMap> relativeTableColumnList=codeTableMapper.getColumnInfo(relativeentityMap);
				EntityMap relativeTableColumnMap=getColumnsInfo(relativeTableColumnList);
				
				dbEntityMap.put("relativeTableColumnList", relativeTableColumnList);
				dbEntityMap.put("relativeTableColumnMap", relativeTableColumnMap);
				
				for(EntityMap relativeColumn:relativeTableColumnList){
					String dbRelativeColumn=relativeColumn.getStr("COLUMN_NAME");
					dbEntityMap.put(relativetableShortName+"."+dbRelativeColumn,relativeColumn);
				}
				relativeTableIndex++;
			}
		}
		return entityMapList;
	}
	
	
}
