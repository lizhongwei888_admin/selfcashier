package com.zzsy.api.entity;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.lujie.common.entity.BasicEntity;
import org.apache.commons.collections.CollectionUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 角色管理
 */
public class RefRole extends BasicEntity implements Serializable {

    private static final long serialVersionUID = -301062307193761449L;

    //唯一标识
    private String code;
    //角色类型：1.管理端；2.自助收银端
    private Integer roleType;
    //角色名称
    private String roleName;
    //角色描述
    private String roleDesc;
    //权限内容（菜单权限、按钮权限）
    private String menus;
    //数据权限内容（选择商户；选择门店）
    private String datas;

    //状态：true启用；false禁用
    private String state;

    /*---------------与数据库无关字段--------*/
    private List<RefMenu> refMenuList;
    private List<TreeZH> treeMenus = new ArrayList<TreeZH>();
    private Boolean stateBool;
    //时间搜索的范围数组
    private JSONArray timeSearch;
    private String startTime;
    private String endTime;
    /*---------------结束--------*/


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getRoleType() {
        return roleType;
    }

    public void setRoleType(Integer roleType) {
        this.roleType = roleType;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleDesc() {
        return roleDesc;
    }

    public void setRoleDesc(String roleDesc) {
        this.roleDesc = roleDesc;
    }

    public String getMenus() {
        return menus;
    }

    public void setMenus(String menus) {
        this.menus = menus;
    }

    public String getDatas() {
        return datas;
    }

    public void setDatas(String datas) {
        this.datas = datas;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public List<RefMenu> getRefMenuList() {
        return refMenuList;
    }

    public void setRefMenuList(List<RefMenu> refMenuList) {
        this.refMenuList = refMenuList;
    }

    public List<TreeZH> getTreeMenus() {
        return treeMenus;
    }

    public void setTreeMenus(List<TreeZH> treeMenus) {
        this.treeMenus = treeMenus;
    }

    public Boolean getStateBool() {
        if ("false".equals(this.state)) {
            this.stateBool = Boolean.FALSE;
        } else {
            this.stateBool = Boolean.TRUE;
        }
        return this.stateBool;
    }

    public void setStateBool(Boolean stateBool) {
        if ("false".equals(this.state)) {
            this.stateBool = Boolean.FALSE;
        } else {
            this.stateBool = Boolean.TRUE;
        }
    }

    public JSONArray getTimeSearch() {
        return timeSearch;
    }

    public void setTimeSearch(JSONArray timeSearch) {
        this.timeSearch = timeSearch;
    }

    public String getStartTime() {
        if (CollectionUtils.isNotEmpty(this.timeSearch)) {
            return this.timeSearch.getString(0);
        }
        return "";
    }

    public void setStartTime(String startTime) {
        if (CollectionUtils.isNotEmpty(this.timeSearch)) {
            this.startTime = this.timeSearch.getString(0);
        }
    }

    public String getEndTime() {
        if (CollectionUtils.isNotEmpty(this.timeSearch)) {
            return this.timeSearch.getString(1);
        }
        return "";
    }

    public void setEndTime(String endTime) {
        if (CollectionUtils.isNotEmpty(this.timeSearch)) {
            this.endTime = this.timeSearch.getString(0);
        }
    }
}
