package com.zzsy.api.util;

import java.util.HashMap;

import com.zzsy.api.config.UserAuthConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lujie.common.constant.HttpContentType;

import com.lujie.common.util.HttpRequest;

public class IcometUtil {
	private final static Logger log = LoggerFactory.getLogger(IcometUtil.class);
	
	public final static String ParkRealtimes="ParkRealtimes";
	public final static String AlarmPassStationInfo="AlarmPassStationInfo";
	public final static String PassStationInfo="PassStationInfo";
	
	public static void send(final String chnalename ,final  String content){
		
		Runnable runnable = new Runnable() {
            @Override
            public void run() {
            	 try {
            		 HashMap<String, String> map = new HashMap<String, String>();  
                 	map.put("cname", chnalename);  
                 	map.put("content", content);
                    
        			String icometUrl= UserAuthConfig.ConfigMap.getStr("IcometUrl");
        			log.info("开始向icomet推送数据:icometUrl="+icometUrl);
        			
            		HashMap<String,String> reqHeaderMap=new HashMap<>();
            		reqHeaderMap.put("Content-Type",HttpContentType.ApplicationForm);
            		
            		String result=HttpRequest.sendPost(reqHeaderMap, icometUrl, map);
        			
            		log.info("向icomet推送数据結果:chnalename,"+"chnalename:"+chnalename+","+content+",result:"+result);
        		} catch (Exception e) {
        			log.info("向页面推送数据失败:"+e.getMessage());
        		}
        	}
        };
        new Thread(runnable).start();  
		
		
		
	}
}
