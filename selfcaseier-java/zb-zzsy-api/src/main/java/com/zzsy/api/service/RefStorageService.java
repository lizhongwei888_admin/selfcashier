package com.zzsy.api.service;

import com.lujie.common.service.BaseService;
import com.zzsy.api.entity.RefRole;
import com.zzsy.api.entity.RefStorage;
import com.zzsy.api.mapper.RefStorageMapper;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class RefStorageService extends BaseService {

    @Autowired
    private RefStorageMapper refStorageMapper;

    /**
     * 添加或修改
     *
     * @param refStorage
     * @return
     */
    public int addOrUp(RefStorage refStorage) {
        if (StringUtils.isEmpty(refStorage.getCode())) {
            //插入唯一code
            refStorage.setCode(UUID.randomUUID().toString().replace("-", ""));
            return refStorageMapper.add(refStorage);
        } else {
            refStorage.setUpdate_time(new Date());
            int up = refStorageMapper.up(refStorage);
            return up;
        }

    }

    public void delete(String codes) {
        Set<String> collect = Arrays.stream(codes.split(",")).collect(Collectors.toSet());
        refStorageMapper.deleteByIdSet(collect);
    }

    public List<RefStorage> findPageList(RefStorage refStorage) {
        return refStorageMapper.findPageList(refStorage);
    }

    public Long countTotal(RefStorage refStorage) {
        return refStorageMapper.countTotal(refStorage);
    }

    public RefStorage getOneByParms(RefStorage refStorage) {
        return refStorageMapper.getOneByParms(refStorage);
    }


}
