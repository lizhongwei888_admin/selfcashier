package com.zzsy.api.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.util.JsonHelper;
import com.lujie.common.util.XslUtils;
import com.zzsy.api.entity.Log;
import com.zzsy.api.service.LogService;

/**
 * 日志控制器
 * @author hupei
 *
 */
@RestController
@RequestMapping(value = "/log")
public class LogController {
	private static final Logger logger = LoggerFactory.getLogger(LogController.class);
	
	@Autowired
	private LogService logService;
	
	/**
	 * 日志首页
	 * @param tabid
	 * @param stime
	 * @param etime
	 * @param map
	 * @return
	 */
	@RequestMapping(path={"/list"})
	@ResponseBody
	public String list(@RequestBody Log log) {
		List<Log> list=logService.findPageListByProperty(log);
		Long totalCount=logService.countTotalByProperty(log);
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success,list,totalCount));
	}
	
	@RequestMapping(path={"/detail"})
	@ResponseBody
	public String detail(@RequestBody Log log) {
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success,
				logService.findOneByProperty(log)));
	}
	
	@RequestMapping(value = "/export")
	public void exportLog_Excel(
			String createUserName,String createTimeStart,String createTimeEnd,String operType,String subType,
			String type, HttpServletResponse response)  throws Exception {
		Log log = new Log();
		log.setCreateUserName(createUserName);
		log.setCreateTimeStart(createTimeStart);
		log.setCreateTimeEnd(createTimeEnd);
		if (null != operType && !operType.isEmpty()) {
			log.setOperType(Integer.parseInt(operType));
		}
		log.setSubType(subType);
		log.setType(type);
		log.setRowIndex(0);
		log.setPageSize(10000000);

		List<Log> list=logService.findPageListByProperty(log);
		List<Object[]>  dataList =logService.getExportColumnList(list);
		String title = "系统日志";
		String[] rowsName = new String[]{"序号","日志类型","日志子类型","操作人","内容","操作类型","执行状态","操作时间","ip地址","备注"};
		XslUtils ex = new XslUtils(response,title, rowsName, dataList);
		ex.export();
	}
}
