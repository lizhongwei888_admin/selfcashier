package com.zzsy.api.entity;

import java.io.Serializable;

import com.lujie.common.entity.BasicEntity;

public class ModuleRoleRelative extends BasicEntity  implements Serializable{
	private static final long serialVersionUID = -385429157197792921L;

	private Integer id;
	private Integer moduleRoleRelativeId;
	private Integer moduleId;
	private Integer moduleRoleId;
	private String dataId; 
	private Boolean allowList; 
	private Boolean allowFindAll;
	private Boolean allowDetail;
	private Boolean allowAdd;
	private Boolean allowUpdate;
	private Boolean allowDelete;
	
	private String moduleName;
	private String moduleCode;
	private String moduleRoleName;
	private String moduleRoleRelativePath;
	
	private String moduleRoleIds;
	private String dataIds;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getModuleId() {
		return moduleId;
	}
	public void setModuleId(Integer moduleId) {
		this.moduleId = moduleId;
	}
	public Integer getModuleRoleId() {
		return moduleRoleId;
	}
	public void setModuleRoleId(Integer moduleRoleId) {
		this.moduleRoleId = moduleRoleId;
	}
	public String getDataId() {
		return dataId;
	}
	public void setDataId(String dataId) {
		this.dataId = dataId;
	} 
	public Boolean getAllowAdd() {
		return allowAdd;
	}
	public void setAllowAdd(Boolean allowAdd) {
		this.allowAdd = allowAdd;
	}
	
	public Boolean getAllowUpdate() {
		return allowUpdate;
	}
	public void setAllowUpdate(Boolean allowUpdate) {
		this.allowUpdate = allowUpdate;
	}
	public Boolean getAllowDelete() {
		return allowDelete;
	}
	public void setAllowDelete(Boolean allowDelete) {
		this.allowDelete = allowDelete;
	}
	public Boolean getAllowList() {
		return allowList;
	}
	public void setAllowList(Boolean allowList) {
		this.allowList = allowList;
	}
	public Boolean getAllowFindAll() {
		return allowFindAll;
	}
	public void setAllowFindAll(Boolean allowFindAll) {
		this.allowFindAll = allowFindAll;
	}
	public Boolean getAllowDetail() {
		return allowDetail;
	}
	public void setAllowDetail(Boolean allowDetail) {
		this.allowDetail = allowDetail;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public String getModuleRoleName() {
		return moduleRoleName;
	}
	public void setModuleRoleName(String moduleRoleName) {
		this.moduleRoleName = moduleRoleName;
	}
	public Integer getModuleRoleRelativeId() {
		return moduleRoleRelativeId;
	}
	public void setModuleRoleRelativeId(Integer moduleRoleRelativeId) {
		this.moduleRoleRelativeId = moduleRoleRelativeId;
	}
	public String getModuleRoleRelativePath() {
		return moduleRoleRelativePath;
	}
	public void setModuleRoleRelativePath(String moduleRoleRelativePath) {
		this.moduleRoleRelativePath = moduleRoleRelativePath;
	}
	public String getModuleCode() {
		return moduleCode;
	}
	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}
	public String getModuleRoleIds() {
		return moduleRoleIds;
	}
	public void setModuleRoleIds(String moduleRoleIds) {
		this.moduleRoleIds = moduleRoleIds;
	}
	public String getDataIds() {
		return dataIds;
	}
	public void setDataIds(String dataIds) {
		this.dataIds = dataIds;
	} 
	
	
}