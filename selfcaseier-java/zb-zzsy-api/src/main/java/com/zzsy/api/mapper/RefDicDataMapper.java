package com.zzsy.api.mapper;

import com.lujie.common.dao.BasicMapper;
import com.zzsy.api.entity.RefDicData;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RefDicDataMapper extends BasicMapper {

    List<RefDicData> findListByTypeId(@Param("p") RefDicData refDicData);


}
