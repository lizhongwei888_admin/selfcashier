package com.zzsy.api.entity;

import com.lujie.common.entity.BasicEntity;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class RefDicType extends BasicEntity implements Serializable {
    private static final long serialVersionUID = -301062307193761449L;

    private String code;
    //字典类型名称
    private String dicType;
    //父级id
    private String pid;
    //备注
    private String remark;
    //状态：true启用；false禁用
    private String state;

    /****非数据库字段*********/
    //是否需要子集 0是需要 1是list集合 2 是map集合
    private Integer needSubset = 0;

    private List<RefDicData> listSubset;

    private Map<String, String> mapSubset;

    /****结束*********/

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDicType() {
        return dicType;
    }

    public void setDicType(String dicType) {
        this.dicType = dicType;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    @Override
    public String getRemark() {
        return remark;
    }

    @Override
    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Integer getNeedSubset() {
        return needSubset;
    }

    public void setNeedSubset(Integer needSubset) {
        this.needSubset = needSubset;
    }

    public List<RefDicData> getListSubset() {
        return listSubset;
    }

    public void setListSubset(List<RefDicData> listSubset) {
        this.listSubset = listSubset;
    }

    public Map<String, String> getMapSubset() {
        return mapSubset;
    }

    public void setMapSubset(Map<String, String> mapSubset) {
        this.mapSubset = mapSubset;
    }
}
