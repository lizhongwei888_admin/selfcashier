package com.zzsy.api.service;

import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.service.BaseService;
import com.lujie.common.util.MD5Util;
import com.lujie.common.util.StringUtil;
import com.zzsy.api.entity.RefLogAdmin;
import com.zzsy.api.entity.RefUser;
import com.zzsy.api.mapper.RefUserMapper;
import com.zzsy.api.util.RedisRefUtil;
import com.zzsy.api.util.RedisUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class RefUserService extends BaseService {

    private static final Logger log = LoggerFactory.getLogger(RefUserService.class);

    @Autowired
    private RefUserMapper refUserMapper;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private RefLogAdminService refLogAdminService;

    @Autowired
    SseService sseService;


    /**
     * 添加或者修改
     *
     * @param refUser
     * @return
     */
    public int addOrUp(RefUser refUser) {
        if (StringUtils.isEmpty(refUser.getCode())) {
            //插入唯一code
            refUser.setCode(UUID.randomUUID().toString().replace("-", ""));
            return refUserMapper.add(refUser);
        } else {
            refUser.setUpdate_time(new Date());
            int up = refUserMapper.up(refUser);
            if (up > 0) {
                //更新redis缓存
                savePermanentRefUser(refUser.getCode());
            }
            return up;
        }

    }

    private void savePermanentRefUser(String code) {
        if (StringUtils.isEmpty(code)) {
            return;
        }
        RefUser parmsRefUser = new RefUser();
        parmsRefUser.setCode(code);
        RefUser oneByParms = refUserMapper.getOneByParms(parmsRefUser);
        RedisRefUtil.savePermanentRefUser(redisTemplate, oneByParms);
    }


    public void delete(String codes) {
        Set<String> collect = Arrays.stream(codes.split(",")).collect(Collectors.toSet());
        refUserMapper.deleteByIdSet(collect);
    }

    public List<RefUser> findPageList(RefUser refUser) {
        return refUserMapper.findPageList(refUser);
    }

    public Long countTotal(RefUser refUser) {
        return refUserMapper.countTotal(refUser);
    }


    public ResultMsg login(RefUser refUser) {
        Integer loginType = 0;
        if (!StringUtil.isEmpty(refUser.getAccount())) {
            loginType = 1;
        }
        if (loginType == 0) {
            return new ResultMsg("301", "账号不能为空");
        }

        if (StringUtil.isEmpty(refUser.getPassword())) {
            return new ResultMsg("302", "密码不能为空");
        }
        //TODO 登录验证码校验
        /*if (StringUtil.isEmpty(refUser.getCodeRefUser())) {
            return new ResultMsg("303", "验证码codeRefUser不能为空");
        }
        if (StringUtil.isEmpty(refUser.getUserKey())) {
            return new ResultMsg("3011", "验证码userKey不能为空");
        }

        Object verifyCodeObj = RedisUtil.getVerifyCode(redisTemplate, refUser.getUserKey());
        if (verifyCodeObj == null) {
            return new ResultMsg("304", "验证码不存在");
        }
        if (!verifyCodeObj.toString().equals(refUser.getCodeRefUser())) {
            return new ResultMsg("305", "验证码错误");
        }*/

        RefUser parmsRefUser = new RefUser();
        parmsRefUser.setAccount(refUser.getAccount());
        if (!StringUtils.isEmpty(refUser.getOrgID()) && !refUser.getAccount().equals("admin")) {
            parmsRefUser.setOrgID(refUser.getOrgID());
        }
        RefUser dbRefUser = refUserMapper.getOneByParms(parmsRefUser);
        //查询登录用户
        if (dbRefUser == null) {
            return new ResultMsg("306", "系统不存在这个用户");
        }
        String encryptPassword = MD5Util.encrypt(refUser.getPassword());
        if (!dbRefUser.getPassword().equals(encryptPassword)) {
            return new ResultMsg("308", "密码有误");
        }
        //判断是否激活
        if ("false".equals(dbRefUser.getState())) {
            return new ResultMsg("308", "系统不存在这个用户");
        }
        //判断是否过期
        if (dbRefUser.getExpireTime()!=null) {
            if (dbRefUser.getExpireTime().before(new Date())) {
                return new ResultMsg("308", "用户已失效");
            }
        }


        // 同个账号登录多台设备，最后一次登录生效，前面的失效
        if (!dbRefUser.getAccount().contains("admin")) {
            // 断开sse连接
            try {
                sseService.removeUid(dbRefUser.getCode());
            } catch (Exception e) {
                log.error("login异常", e);
            }
            // 删除这个userId下所有的TOKEN
            RedisRefUtil.removeTokenByUserCode(redisTemplate, dbRefUser.getCode());
        }

        dbRefUser.setUpdateby(dbRefUser.getCode());
        dbRefUser.setUpdate_time(new Date());
        Date lastLoginTime = dbRefUser.getLastLoginTime();
        //设置新的登录时间
        dbRefUser.setLastLoginTime(new Date());
        Integer sessionMinuteTime = 7 * 24 * 60;
        RedisRefUtil.genarateTokenAndSaveRefUser(redisTemplate, dbRefUser, sessionMinuteTime);
        refUserMapper.up(dbRefUser);


        this.refLogAdminService.create(new RefLogAdmin(dbRefUser.getCode()
                , "登录页面", "登录", "login", "/refadmin/login"
                , refUser, dbRefUser, dbRefUser.getOrgID()));
        dbRefUser.setPassword(null);// 密码保密，不能传给前台
        dbRefUser.setLastLoginTime(lastLoginTime);
        return new ResultMsg(ResultCode.Success, dbRefUser);
    }

    public ResultMsg logout(HttpServletRequest request) {
        RefUser refUser = RedisRefUtil.getRefUser(redisTemplate, request);
        RedisRefUtil.removeToken(redisTemplate, request);
        this.refLogAdminService.create(new RefLogAdmin(refUser.getCode()
                , "管理页面", "退出登录", "logout", "/refadmin/logout"
                , refUser, refUser, refUser.getOrgID()));

        return new ResultMsg(ResultCode.Success);
    }

    public RefUser getOneByParms(RefUser refUser) {
        return refUserMapper.getOneByParms(refUser);
    }

    public RefUser getOneByRoleCodes(String roleCodes) {
        Set<String> collect = Arrays.stream(roleCodes.split(",")).collect(Collectors.toSet());
        return refUserMapper.getOneByRoleCodes(collect);
    }

    public List<RefUser> getListByCodes(String codes) {
        Set<String> collect = Arrays.stream(codes.split(",")).collect(Collectors.toSet());
        return refUserMapper.getListByCodes(collect);
    }

}
