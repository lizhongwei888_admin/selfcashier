package com.zzsy.api.entity;

import com.lujie.common.entity.BasicEntity;

import java.io.Serializable;
import java.util.Date;

/**
 * 订单管理-支付明细(DwOrderPaywaydetail)实体类
 *
 * @author makejava
 * @since 2024-02-05 18:27:04
 */
public class DwOrderPaywaydetail extends BasicEntity implements Serializable {
    private static final long serialVersionUID = -71431545187320208L;
    /**
     * 唯一标识
     */
    private String code;
    /**
     * 来源类型：1订单；2退货单
     */
    private String type;
    /**
     * 来源单据ID（所属订单表ID/所属退货表ID)
     */
    private String orderId;
    /**
     * 支付方式ID
     */
    private String payWayId;
    /**
     * 支付类型
     */
    private String payType;
    /**
     * 支付金额
     */
    private String payActualMoney;
    /**
     * 支付编号
     */
    private String payNo;
    /**
     * 支付时间
     */
    private Date payTime;
    /**
     * 第三方支付方式编号
     */
    private String payCode;
    /**
     * 支付请求json内容
     */
    private String requestJsonPay;
    /**
     * 支付返回json内容
     */
    private String returnJsonPay;



    /**
     * 是否退款成功
     */
    private Integer isReturnSucc;

    public Integer getIsReturnSucc() {
        return isReturnSucc;
    }

    public void setIsReturnSucc(Integer isReturnSucc) {
        this.isReturnSucc = isReturnSucc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPayWayId() {
        return payWayId;
    }

    public void setPayWayId(String payWayId) {
        this.payWayId = payWayId;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getPayActualMoney() {
        return payActualMoney;
    }

    public void setPayActualMoney(String payActualMoney) {
        this.payActualMoney = payActualMoney;
    }

    public String getPayNo() {
        return payNo;
    }

    public void setPayNo(String payNo) {
        this.payNo = payNo;
    }

    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }

    public String getPayCode() {
        return payCode;
    }

    public void setPayCode(String payCode) {
        this.payCode = payCode;
    }

    public String getRequestJsonPay() {
        return requestJsonPay;
    }

    public void setRequestJsonPay(String requestJsonPay) {
        this.requestJsonPay = requestJsonPay;
    }

    public String getReturnJsonPay() {
        return returnJsonPay;
    }

    public void setReturnJsonPay(String returnJsonPay) {
        this.returnJsonPay = returnJsonPay;
    }


}

