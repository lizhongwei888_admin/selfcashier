package com.zzsy.api.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class HttpRequest {
    /**
     * 向指定URL发送GET方法的请求
     * 
     * @param url
     *            发送请求的URL
     * @param param
     *            请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
     * @return URL 所代表远程资源的响应结果
     * @throws Exception 
     */
    public static String sendGet(String url, String param) throws Exception {
        String result = "";
        BufferedReader in = null;
        String urlNameString = url;
        if(param!=null && !param.equals("")){
        	urlNameString+="?" + param;
        }
        URL realUrl = new URL(urlNameString);
        // 打开和URL之间的连接
        URLConnection connection = realUrl.openConnection();
        // 设置通用的请求属性
        connection.setRequestProperty("accept", "*/*");
        connection.setRequestProperty("connection", "Keep-Alive");
        connection.setRequestProperty("user-agent",
                "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
        // 建立实际的连接
        connection.connect();
        // 获取所有响应头字段
        Map<String, List<String>> map = connection.getHeaderFields();
        // 遍历所有的响应头字段
        for (String key : map.keySet()) {
            System.out.println(key + "--->" + map.get(key));
        }
        // 定义 BufferedReader输入流来读取URL的响应
        in = new BufferedReader(new InputStreamReader(
                connection.getInputStream()));
        String line;
        while ((line = in.readLine()) != null) {
            result += line;
        }
        // 使用finally块来关闭输入流
        if (in != null) {
            in.close();
        }
        
        return result;
    }
    public static String downLoadFile(String url, String savepath) throws Exception {
        String result = "";
        String urlNameString = url;

        URL realUrl = new URL(urlNameString);

        URLConnection connection = realUrl.openConnection();

        connection.setRequestProperty("connection", "Keep-Alive");
        connection.setRequestProperty("user-agent", 
          "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");

        connection.connect();

        Map map = connection.getHeaderFields();
        String str1;
        for (Iterator localIterator = map.keySet().iterator(); localIterator.hasNext(); str1 = (String)localIterator.next());
        InputStream in = connection.getInputStream();
        FileOutputStream fos = new FileOutputStream(new File(savepath));
        byte[] b = new byte[1024];
        int actionLen;
        while ((actionLen = in.read(b, 0, b.length)) != -1)
        {
          fos.write(b, 0, actionLen);
        }
        in.close();
        fos.close();

        return result;
      }
    public static String sendGet(String url, String param, String responsecharset)
    {
      String result = "";
      BufferedReader in = null;
      try {
        String urlNameString = url;

        if ((param != null) && (param.length() > 0)) {
          urlNameString = url + "?" + param;
        }
        URL realUrl = new URL(urlNameString);

        URLConnection connection = realUrl.openConnection();
        connection.setConnectTimeout(100000);
        connection.setReadTimeout(100000);
        connection.setRequestProperty("accept", "*/*");
        connection.setRequestProperty("connection", "Keep-Alive");
        connection.setRequestProperty("user-agent", 
          "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");

        connection.connect();

        Map map = connection.getHeaderFields();
        String str1;
        for (Iterator localIterator = map.keySet().iterator(); localIterator.hasNext(); str1 = (String)localIterator.next());
        in = new BufferedReader(
          new InputStreamReader(connection.getInputStream(), responsecharset));
        String line;
        while ((line = in.readLine()) != null)
        {
          result = result + line;
        }
      } catch (Exception e) {
        System.out.println("发送GET请求出现异常！" + e);
        e.printStackTrace();
        try
        {
          if (in != null)
            in.close();
        }
        catch (Exception e2) {
          e2.printStackTrace();
        }
      }
      finally
      {
        try
        {
          if (in != null)
            in.close();
        }
        catch (Exception e2) {
          e2.printStackTrace();
        }
      }
      return result;
    }
    public static String sendPost(HashMap<String,String> requestHeaderMap,String url, String param) throws Exception {
        PrintWriter out = null;
         BufferedReader in = null;
         String result = "";
         
         URL realUrl = new URL(url);
         // 打开和URL之间的连接
         URLConnection conn = realUrl.openConnection();
         // 设置通用的请求属性
         conn.setRequestProperty("accept", "*/*");
         conn.setRequestProperty("connection", "Keep-Alive");
         conn.setRequestProperty("user-agent",
                 "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
         
         if(requestHeaderMap!=null && !requestHeaderMap.isEmpty()){
        	 Iterator<String> reqheadKeys=requestHeaderMap.keySet().iterator();
             while(reqheadKeys.hasNext()){
            	 String key=reqheadKeys.next();
            	 String value=requestHeaderMap.get(key);
            	 conn.setRequestProperty(key, value);
             }	 
         }
         
         
         // 发送POST请求必须设置如下两行
         conn.setDoOutput(true);
         conn.setDoInput(true);
         // 获取URLConnection对象对应的输出流
         out = new PrintWriter(conn.getOutputStream());
         // 发送请求参数
         out.print(param);
         // flush输出流的缓冲
         out.flush();
         // 定义BufferedReader输入流来读取URL的响应
         in = new BufferedReader(
                 new InputStreamReader(conn.getInputStream()));
         String line;
         while ((line = in.readLine()) != null) {
             result += line;
         } 
         if(out!=null){
             out.close();
         }
         if(in!=null){
             in.close();
         }
         return result;
     }

    /**
     * 向指定 URL 发送POST方法的请求
     * 
     * @param url
     *            发送请求的 URL
     * @param param
     *            请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
     * @return 所代表远程资源的响应结果
     */
    public static String sendPost(String url, String param) throws Exception {
       PrintWriter out = null;
        BufferedReader in = null;
        String result = "";
        
        URL realUrl = new URL(url);
        // 打开和URL之间的连接
        URLConnection conn = realUrl.openConnection();
        // 设置通用的请求属性
        conn.setRequestProperty("accept", "*/*");
        conn.setRequestProperty("connection", "Keep-Alive");
        conn.setRequestProperty("user-agent",
                "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
        // 发送POST请求必须设置如下两行
        conn.setDoOutput(true);
        conn.setDoInput(true);
        // 获取URLConnection对象对应的输出流
        out = new PrintWriter(conn.getOutputStream());
        // 发送请求参数
        out.print(param);
        // flush输出流的缓冲
        out.flush();
        // 定义BufferedReader输入流来读取URL的响应
        in = new BufferedReader(
                new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = in.readLine()) != null) {
            result += line;
        } 
        if(out!=null){
            out.close();
        }
        if(in!=null){
            in.close();
        }
        return result;
    }  
    public static String sendPost(String url, Map param) throws Exception {
    	StringBuffer sb=new StringBuffer();
    	Iterator iterator=param.entrySet().iterator();
    	while(iterator.hasNext()){
    		Map.Entry<String, String>  entry=(Map.Entry<String, String> )iterator.next();
    		sb.append(entry.getKey()+"="+entry.getValue()+"&");
    	}
    	
    	return sendPost(url,sb.deleteCharAt(sb.length()-1).toString());
    }
    
     
    public static void test2(String page) throws Exception {
    	/*
    	Map<String,String> map=new HashMap<String,String>();
    	map.put("city_code",DesHelper.encrypt_str("wuhan", ZZWLUtil.hotel_list_key));
    	map.put("opt", "100");
    	
    	System.out.println(HttpRequest.sendPost("http://192.168.100.187:8080/hotel/hotel_list.do", map));
    	*/ 
    	String[] hotels=new String[] {};
    	Map<String,String> map=new HashMap<String,String>();//MT-398355 MT-321399 MT-061144 MT-061144 MT-321399   MT-322463  MT-004668 //MT-4326190 MT-336836
    	
    	map.put("check_in_date",check_in_date);
    	map.put("check_out_date",check_out_date);
    	map.put("adult_count",adult_count);
    	map.put("children_count",children_count);
    	map.put("children_age_list",children_age_list);
    	
    	
    	map.put("room_count",room_count);
    	
    	
    	//map.put("is_free_cancel","1");
    	map.put("page_index",page);
    	map.put("page_size","5"); 
    	long start=System.currentTimeMillis();
    	//String ret=HttpRequest.sendPost("http://192.168.100.187:8080/hotel/service.do", map);
    	//System.out.println(ret);
    	//System.out.println(HttpRequest.sendPost("http://192.168.100.31:18080/hotel/service.do", map));
    	//System.out.println(HttpRequest.sendPost("http://192.168.100.187:8080/hotel/service.do", map));
    	//System.out.println();
    	if(isLocalTest) {
    		System.out.println(HttpRequest.sendPost("http://192.168.100.187:8080/hotel/service.do", map));
        }else {
        	System.out.println(HttpRequest.sendPost("http://61.183.72.78:8090/hotel/service.do", map));
        }
    	
    	System.out.println(System.currentTimeMillis()-start);
    }
    
    static String rate="6_cfbc52fd2a11c4f35f53b7a5627f3579_16:200110508:200609266:P";
    static String price="2133.60";//
    static String check_in_date="2017-11-05";
	static String check_out_date="2017-11-07";
	static String adult_count="2";
	static String children_count="1";
    static String children_age_list="7";
    static String room_count="2";
    static String children_age="7";
    static String rooms_adult_name_list="张三,李四";
    static int test_i=1;
    static Boolean isLocalTest=true;
    
    static String order_id="20171009145151";
    
    
        
}