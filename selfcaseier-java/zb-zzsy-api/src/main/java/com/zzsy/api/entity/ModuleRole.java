package com.zzsy.api.entity;

import java.io.Serializable;
import java.util.List;

import com.lujie.common.entity.BasicEntity;

public class ModuleRole extends BasicEntity  implements Serializable{
	private static final long serialVersionUID = -385429157197792921L;

	private Integer id;

    private String name;
    
    private String userId;
    
    private List<ModuleRoleRelative> modules;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ModuleRoleRelative> getModules() {
		return modules;
	}

	public void setModules(List<ModuleRoleRelative> modules) {
		this.modules = modules;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	
	

	 
	
    
}