package com.zzsy.api.service;

import com.lujie.common.service.BaseService;
import com.zzsy.api.entity.RefDicData;
import com.zzsy.api.mapper.RefDicDataMapper;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class RefDicDataService extends BaseService {

    @Autowired
    private RefDicDataMapper refDicDataMapper;

    /**
     * 添加或修改
     *
     * @param refDicData
     * @return
     */
    public int addOrUp(RefDicData refDicData) {
        if (StringUtils.isEmpty(refDicData.getCode())) {
            //插入唯一code
            refDicData.setCode(UUID.randomUUID().toString().replace("-", ""));
            return refDicDataMapper.add(refDicData);
        } else {
            refDicData.setUpdate_time(new Date());
            int up = refDicDataMapper.up(refDicData);
            return up;
        }

    }

    public void delete(String codes) {
        Set<String> collect = Arrays.stream(codes.split(",")).collect(Collectors.toSet());
        refDicDataMapper.deleteByIdSet(collect);
    }

    public List<RefDicData> findPageList(RefDicData refDicData) {
        return refDicDataMapper.findPageList(refDicData);
    }

    public Long countTotal(RefDicData refDicData) {
        return refDicDataMapper.countTotal(refDicData);
    }


}
