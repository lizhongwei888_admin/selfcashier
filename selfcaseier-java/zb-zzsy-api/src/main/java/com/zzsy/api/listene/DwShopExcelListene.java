package com.zzsy.api.listene;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.metadata.CellData;
import com.alibaba.excel.read.listener.ReadListener;
import com.alibaba.fastjson.JSON;
import com.zzsy.api.entity.DwShop;
import com.zzsy.api.entity.RefUser;
import com.zzsy.api.excel.DwShopExcel;
import com.zzsy.api.mapper.DwShopMapper;
import com.zzsy.api.service.DwShopService;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.BeanUtilsBean2;
import org.apache.commons.collections4.ListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;

import java.util.*;

public class DwShopExcelListene implements ReadListener<DwShopExcel> {


    public static final Logger log = LoggerFactory.getLogger(DwShopExcelListene.class);
    /**
     * 假设这个是一个DAO，当然有业务逻辑这个也可以是一个service。当然如果不用存储这个对象没用。
     */
    private DwShopMapper dwShopMapper;

    private RefUser loginRefUser;

    public DwShopExcelListene() {

    }

    /**
     * 如果使用了spring,请使用这个构造方法。每次创建Listener的时候需要把spring管理的类传进来
     *
     * @param dwShopMapper
     */
    public DwShopExcelListene(DwShopMapper dwShopMapper, RefUser loginRefUser) {
        log.info("进入DwShopExcelListene赋值");
        this.dwShopMapper = dwShopMapper;
        this.loginRefUser = loginRefUser;
    }

    /**
     * 这个每一条数据解析都会来调用
     *
     * @param data    one row value. Is is same as {@link AnalysisContext#readRowHolder()}
     * @param context
     */
    @Override
    public void invoke(DwShopExcel data, AnalysisContext context) {
        log.info("进入DwShopExcelListene_invoke:{}", JSON.toJSONString(data));
        DwShop dwShop = new DwShop();
        //插入唯一code
        dwShop.setCode(UUID.randomUUID().toString().replace("-", ""));
        dwShop.setCreateby(loginRefUser.getCode());
        dwShop.setCreate_time(new Date());
        dwShop.setUpdateby(loginRefUser.getCode());
        dwShop.setUpdate_time(new Date());
        dwShop.setOrgID(loginRefUser.getOrgID());
        BeanUtils.copyProperties(data, dwShop);
        dwShopMapper.add(dwShop);

    }

    /**
     * 所有数据解析完成了 都会来调用
     *
     * @param context
     */
    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        // 这里也要保存数据，确保最后遗留的数据也存储到数据库
        log.info("进入DwShopExcelListene_doAfterAllAnalysed:{}", context.readSheetHolder().getApproximateTotalRowNumber());

    }

    @Override
    public void onException(Exception e, AnalysisContext analysisContext) throws Exception {
        log.info("进入DwShopExcelListene_onException:{}", analysisContext.readSheetHolder().getApproximateTotalRowNumber());
    }

    @Override
    public void invokeHead(Map<Integer, CellData> map, AnalysisContext analysisContext) {
        log.info("进入DwShopExcelListene_invokeHead:{}", analysisContext.readSheetHolder().getApproximateTotalRowNumber());
    }

    @Override
    public boolean hasNext(AnalysisContext analysisContext) {
        return true;
    }
}
