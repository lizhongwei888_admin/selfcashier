package com.zzsy.api.entity;

import com.lujie.common.entity.BasicEntity;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

public class RefCity extends BasicEntity implements Serializable {
    private static final long serialVersionUID = -301062307193761449L;

    private String code;
    //城市编码
    private String cityCode;
    //城市名称
    private String cityName;
    //城市类型：1省；2市；3区
    private Integer cityType;
    //地市父级Id
    private String cityPid;

    //存放下一个等级的数据
    private List<RefCity> nextLevelData;

    //父级code集合
    private Set<String> codeSet;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public Integer getCityType() {
        return cityType;
    }

    public void setCityType(Integer cityType) {
        this.cityType = cityType;
    }

    public String getCityPid() {
        return cityPid;
    }

    public void setCityPid(String cityPid) {
        this.cityPid = cityPid;
    }

    public List<RefCity> getNextLevelData() {
        return nextLevelData;
    }

    public void setNextLevelData(List<RefCity> nextLevelData) {
        this.nextLevelData = nextLevelData;
    }

    public Set<String> getCodeSet() {
        return codeSet;
    }

    public void setCodeSet(Set<String> codeSet) {
        this.codeSet = codeSet;
    }
}
