package com.zzsy.api.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

import com.alibaba.fastjson.JSON;
import com.zzsy.api.constant.LogType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.util.DateUtil;
import com.lujie.common.util.JsonHelper;
import com.lujie.common.util.StringUtil;
import com.lujie.common.util.StringUtils;
import com.zzsy.api.config.UserAuthConfig;
import com.zzsy.api.entity.Log;
import com.zzsy.api.util.CompressImage;
import com.obs.services.ObsClient;
import com.obs.services.exception.ObsException;
import com.obs.services.model.PutObjectRequest;
import com.obs.services.model.PutObjectResult;

@Service
public class UploadService {
	private final static Logger log = LoggerFactory.getLogger(UploadService.class);
	
	/**
	 * iam auth 
	 * User Name  ,  Access Key Id  ,  Secret Access Key
	 " LJ-lujiem" , ZCOBEOFMWOELEQDVLELD , 1DhUAx0Y2a0BWdxP6ofalDMHtvUnSkjsGliSyncs
	 * @param request
	 * @param isBasicData
	 * @return
	 */
	public String uploadFileOBS(HttpServletRequest request,Integer isBasicData){

	  String bucketName = "zzsy2024";
	  
	  if(StringUtils.isEmptyStr(bucketName)) {
			return JsonHelper.toJson(new ResultMsg("305","桶名为空")); 
		}

		Map<String , String > resMap = new HashMap<>() ;
		
		Map<String,MultipartFile> files=((MultipartHttpServletRequest)request).getFileMap();
		if(files.isEmpty()){
			return JsonHelper.toJson(new ResultMsg("301","上传为空")); 
		}
		
		Iterator iterator=files.entrySet().iterator();
		
		if(iterator.hasNext()) {
			Map.Entry<String, MultipartFile> obj = (Map.Entry<String, MultipartFile>) iterator.next();
			MultipartFile file = obj.getValue();

			if (file == null) {
				return JsonHelper.toJson(new ResultMsg("301", "上传为空:" + obj.getKey()));
			}
			resMap.put("fileName", file.getOriginalFilename());
			resMap.put("fileType", getFileTypeNew(file.getContentType(), file.getOriginalFilename()));
			resMap.put("fileSize", String.valueOf(file.getSize()));

			log.info("file name:" + file.getOriginalFilename() + ",file size:" + file.getSize());

			if (file.getSize() > 20 * 1024 * 1024) {
				log.info("file size 超过20M,file name:" + file.getOriginalFilename() + ",file size:" + file.getSize());
				return JsonHelper.toJson(new ResultMsg("303", "上传大小不能超过20M"));
			}

			String fileUrl = "";
			
			try {
				
				Long currentTime=System.currentTimeMillis();
				
				String saveDir=DateUtil.formatDate(new Date(), "yyyyMMdd");

				InputStream in = file.getInputStream();

				String fileUploadName = saveDir + "/" + currentTime + "_" + file.getOriginalFilename();

				fileUrl = this.upload(bucketName, fileUploadName, in);

				if (in != null) {
					in.close();
				}
				if (StringUtils.isEmptyStr(fileUrl)) {
					return JsonHelper.toJson(new ResultMsg("300", "上传失败"));
				}

				resMap.put("picUrl", fileUrl);
				return JsonHelper.toJson(new ResultMsg(ResultCode.Success, resMap));
			} catch (Exception ex) {
				log.info("上传图片异常:"+StringUtil.getExceptionDesc(ex));
				return JsonHelper.toJson(new ResultMsg("302","上传异常")); 
			}
		}
		return JsonHelper.toJson(new ResultMsg("316","上传失败"));
		
	}
	
	 public String upload( String bucketName , String fileName , InputStream inputStream ) {
	        // 您可以通过环境变量获取访问密钥AK/SK，也可以使用其他外部引入方式传入。如果使用硬编码可能会存在泄露风险。
	        // 您可以登录访问管理控制台获取访问密钥AK/SK
	        String ak = "K2TFPUTCC9KGXBD4YALL";
	        String sk = "vd9t41kiNhl5gGPKkR35fLX9rLjFskOOcYt7ryHe" ;
	        // 【可选】如果使用临时AK/SK和SecurityToken访问OBS，同样建议您尽量避免使用硬编码，以降低信息泄露风险。
	        // 您可以通过环境变量获取访问密钥AK/SK/SecurityToken，也可以使用其他外部引入方式传入。
	        // String securityToken = System.getenv("SECURITY_TOKEN");
	        // endpoint填写桶所在的endpoint,
	      //  https://xupu-mmf-pic.obs.cn-east-2.myhuaweicloud.com:443/3a81a369ee8f279df608fdb014c617f1.jpeg?AccessKeyId=ZCOBEOFMWOELEQDVLELD&Expires=1703402372&Signature=prUtQ1sDfa836qkPh10ZI3t30O4%3D   
		//	此处以华北-北京四为例，其他地区请按实际情况填写。
	        String endPoint = "https://obs.cn-north-4.myhuaweicloud.com" ;
	        // 您可以通过环境变量获取endPoint，也可以使用其他外部引入方式传入。
	        //String endPoint = System.getenv("ENDPOINT");
	        
	        // 创建ObsClient实例
	        // 使用永久AK/SK初始化客户端
	        ObsClient obsClient = new ObsClient(ak, sk,endPoint);
	        
	        PutObjectResult  putObjectResult = null ;
	        String fileUrl = null ;
	        try {
				// 文件上传
				// localfile为待上传的本地文件路径，需要指定到具体的文件名
				//   obsClient.putObject(bucketName , "picName", new File(filePath));
				// localfile2 为待上传的本地文件路径，需要指定到具体的文件名
				PutObjectRequest request = new PutObjectRequest();
				request.setBucketName(bucketName);
				request.setObjectKey(fileName);
				request.setInput(inputStream);

				putObjectResult = obsClient.putObject(request);

				log.info("putObject successfully" + putObjectResult);

				fileUrl = putObjectResult.getObjectUrl();

			} catch (ObsException e) {
	           log.info("putObject failed");
	            // 请求失败,打印http状态码
	           log.info("HTTP Code:" + e.getResponseCode());
	            // 请求失败,打印服务端错误码
	           log.info("Error Code:" + e.getErrorCode());
	            // 请求失败,打印详细错误信息
	           log.info("Error Message:" + e.getErrorMessage());
	            // 请求失败,打印请求id
	           log.info("Request ID:" + e.getErrorRequestId());
				log.info("Host ID:" + e.getErrorHostId());
				log.info("putObject failed:" + StringUtil.getExceptionDesc(e));
	        } catch (Exception e) {
				log.info("putObject failed:" + StringUtil.getExceptionDesc(e));

	        }
			return fileUrl ;
	    }
	
	
	public String uploadFile(HttpServletRequest request,Integer isBasicData){
		
		Map<String , String > resMap = new HashMap<>() ;
		resMap.put("picHost" , UserAuthConfig.BgPicHost);
		
		Map<String,MultipartFile> files=((MultipartHttpServletRequest)request).getFileMap();
		if(files.isEmpty()){
			return JsonHelper.toJson(new ResultMsg("301","上传为空")); 
	    }
		
	    Iterator iterator=files.entrySet().iterator();
	    if(iterator.hasNext()) {
			Map.Entry<String, MultipartFile> obj = (Map.Entry<String, MultipartFile>) iterator.next();
			MultipartFile file = obj.getValue();
			if (file == null) {
				return JsonHelper.toJson(new ResultMsg("301", "上传为空:" + obj.getKey()));
			}
			resMap.put("fileName", file.getOriginalFilename());
			resMap.put("fileType", getFileTypeNew(file.getContentType(), file.getOriginalFilename()));
			resMap.put("fileSize", String.valueOf(file.getSize()));

			log.info("file name:" + file.getOriginalFilename() + ",file size:" + file.getSize());

			if (file.getSize() > 50 * 1024 * 1024) {
				log.info("file size 超过100M,file name:" + file.getOriginalFilename() + ",file size:" + file.getSize());
				return JsonHelper.toJson(new ResultMsg("303", "上传大小不能超过10M"));
			}

			try {

				Long currentTime = System.currentTimeMillis();
				String saveDir = DateUtil.formatDate(new Date(), "yyyy-MM");
				/*if (isBasicData != null && isBasicData.equals(1)) {
					saveDir = "basic";
				}*/
				//获得项目的根路径上一级路径
				String projectPath = System.getProperty("user.dir");
				File fileProjectPath = new File(projectPath);
				String parentPath = fileProjectPath.getParent();

				InputStream in = file.getInputStream();
				String outfilepath = parentPath + "\\" + UserAuthConfig.UploadImageWebPath + "\\" + saveDir;
				String fileWebPathName = parentPath + "\\" + UserAuthConfig.UploadImageWebPath + "\\" + saveDir + "\\" + currentTime + "_" + file.getOriginalFilename();

				File outfilepathDir = new File(outfilepath);
				if (!outfilepathDir.exists()) {
					outfilepathDir.mkdirs();
				}

				String fileName = outfilepath + "/" + currentTime + "_" + file.getOriginalFilename();
				FileOutputStream out = new FileOutputStream(fileName);


				String fileType = getFileTypeNew(file.getContentType(), file.getOriginalFilename());
	    		if(!fileType.equals("") 
	    				&& pictypes.contains(fileType) 
	    				&& file.getSize()>1024*1024){//文件需要压缩
	    			try{
	    				Boolean isCompressFile=CompressImage.compressImage(file.getInputStream(), out, 0.3, CompressImage.getFormat(file.getOriginalFilename()));
		    			if(isCompressFile){
		    				resMap.put("picUrl", fileWebPathName );
		    				return JsonHelper.toJson(new ResultMsg(ResultCode.Success,resMap));
		    			}
	    			}catch(Exception ex){
	    				ex.printStackTrace();
	    				log.info("压缩图片异常:"+StringUtil.getExceptionDesc(ex));
	    			}
	    		}
	    		
	    		byte [] bs = new byte[1024];
	            int len = 0;
	            while((len=in.read(bs))!=-1){
	            	out.write(bs,0,len);
	            }
	            if(in!=null){
	            	in.close();
	            }
	            if(out!=null){
	            	out.close();
	            }
				resMap.put("picUrl" , fileWebPathName );
	            return JsonHelper.toJson(new ResultMsg(ResultCode.Success,resMap));
	        } catch (Exception ex) {
	        	log.info("上传图片异常:"+StringUtil.getExceptionDesc(ex));
				return JsonHelper.toJson(new ResultMsg("302","上传异常")); 
	        }
	    }
	    return JsonHelper.toJson(new ResultMsg("316","上传失败"));

	}

	public static List<String> pictypes = Arrays.asList(new String[]{"jpg", "jpeg", "png", "bmp", "gif"});
	
	/*public static String getFileType(String fileName){
		Integer typeIndex=fileName.lastIndexOf(".");
		if(typeIndex==-1) return "";
		
		return fileName.substring(typeIndex+1).toLowerCase();
	}*/

	public static List<String> fileTypeList = Arrays.asList(new String[]{"mp3", "mp4", "wav", "flac", "aac", "avi", "mov", "mkv", "jpg", "jpeg", "png", "bmp", "gif", "webp", "pdf", "txt", "doc", "docx", "xls", "csv", "xlsx", "zip", "rar", "7z"});

	public static String getFileTypeNew(String fileType, String fileName) {
		Integer typeIndex = fileType.lastIndexOf("/");
		if (typeIndex == -1) {
			return "";
		}
		String one = fileType.substring(typeIndex + 1).toLowerCase();
		if (fileTypeList.contains(one)) {
			return one;
		}

		Integer typeIndexName = fileName.lastIndexOf(".");
		if (typeIndexName == -1) {
			return "";
		}
		return fileName.substring(typeIndexName + 1).toLowerCase();

	}
}
