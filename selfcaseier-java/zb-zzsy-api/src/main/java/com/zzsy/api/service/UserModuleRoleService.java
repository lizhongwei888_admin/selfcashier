package com.zzsy.api.service;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import com.zzsy.api.constant.LogType;
import com.zzsy.api.constant.UserType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import com.github.pagehelper.util.StringUtil;
import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.service.BasicService;
import com.zzsy.api.entity.Log;
import com.zzsy.api.entity.User;
import com.zzsy.api.entity.UserModuleRole;
import com.zzsy.api.mapper.UserModuleRoleMapper;
import com.zzsy.api.util.RedisUtil;

@Service
public class UserModuleRoleService extends BasicService{

	@Autowired
	private LogService logservice;
	
	@Autowired 
	private RedisTemplate redisTemplate;
	
	@Autowired
	private UserModuleRoleMapper userModuleRoleMapper;
	@Resource(name="userModuleRoleMapper")
	public void initBaseDao(UserModuleRoleMapper userModuleRoleMapper){
		super.setBaseMapper(userModuleRoleMapper);
	} 
	
	public List<UserModuleRole> findByUserId(String userId){
		UserModuleRole userModuleRole=new UserModuleRole();
		userModuleRole.setUserId(userId);
		return userModuleRoleMapper.findList(userModuleRole);
	} 
	public String getModuleRoleIds(List<UserModuleRole> userModuleRoles){
		StringBuilder sb=new StringBuilder();
		if(!userModuleRoles.isEmpty()){
			for(UserModuleRole umr:userModuleRoles){
				sb.append(umr.getModuleRoleId()+",");
			}
			sb.delete(sb.length()-1, sb.length());
		}
		return sb.toString();
	}
	public ResultMsg save(UserModuleRole userModuleRole,HttpServletRequest request){
		if(StringUtil.isEmpty(userModuleRole.getUserId())){
			return new ResultMsg("310","用户id不能为空");
		}
		if(userModuleRole.getModuleRoleId()==null){
			return new ResultMsg("320","模块角色id不能为空");
		}
		UserModuleRole dbUserModuleRole=(UserModuleRole)userModuleRoleMapper.findOne(userModuleRole);
		if(dbUserModuleRole!=null){
			return new ResultMsg("321","已经添加了这个角色");
		}
		
		User loginUser = RedisUtil.getUser(redisTemplate, request);
		
		userModuleRole.setCreateUserId(loginUser.getId());
		userModuleRoleMapper.save(userModuleRole);
		
		//logservice.create(loginUser, "添加用户模块角色");
		this.logservice.create(new Log(loginUser.getId(), "添加用户模块角色", LogType.UserType.getDesc(), UserType.Role.getDesc(), 0, 0) );

		
		return  new ResultMsg(ResultCode.Success);
	}
	 
	public ResultMsg delete(UserModuleRole userModuleRole,HttpServletRequest request){
		if(userModuleRole.getId()==null){
			return new ResultMsg("310","用户模块角色id不能为空");
		}
		User loginUser = RedisUtil.getUser(redisTemplate, request);
		userModuleRole.setIsValid(0);
		userModuleRole.setModifyTime(new Date());
		userModuleRole.setModifyUserId(loginUser.getId());
		userModuleRoleMapper.update(userModuleRole);
		
		//this.logservice.create(loginUser, "删除用户模块角色");
		this.logservice.create(new Log(loginUser.getId(), "删除用户模块角色", LogType.UserType.getDesc(), UserType.Role.getDesc(), 2, 0) );

		
		return  new ResultMsg(ResultCode.Success);
	}
}
