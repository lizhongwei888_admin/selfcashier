package com.zzsy.api.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.util.JsonHelper;
import com.zzsy.api.entity.RefApi;
import com.zzsy.api.entity.RefApiItem;
import com.zzsy.api.entity.RefUser;
import com.zzsy.api.service.RefApiItemService;
import com.zzsy.api.service.RefApiService;
import com.zzsy.api.util.RedisRefUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 系统接口定义控制器
 */
@Controller
@RequestMapping(value = "/refApi")
public class RefApiController {

    @Autowired
    private RefApiService refApiService;
    @Autowired
    private RefApiItemService refApiItemService;

    @Autowired
    private RedisTemplate redisTemplate;

    @RequestMapping(value = "/findAll", method = RequestMethod.POST)
    @ResponseBody
    public String findAll(HttpServletRequest request, @RequestBody RefApi refApi) {

        return JsonHelper.toJson(new ResultMsg(ResultCode.Success, refApiService.findAll(refApi)));
    }

    @RequestMapping(value = "/findOne", method = RequestMethod.POST)
    @ResponseBody
    public String findOne(HttpServletRequest request, @RequestBody RefApi refApi) {

        return JsonHelper.toJson(new ResultMsg(ResultCode.Success, refApiService.findOne(refApi)));
    }


    /**
     * 添加系统接口定义
     *
     * @param request
     * @param refApi
     * @return
     */
    @RequestMapping(value = "/addRefApi", method = RequestMethod.POST)
    @ResponseBody
    public String addRefApi(HttpServletRequest request, @RequestBody RefApi refApi) {
        RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
        refApi.setCreateby(loginRefUser.getCode());
        refApi.setCreate_time(new Date());
        refApi.setUpdateby(loginRefUser.getCode());
        refApi.setUpdate_time(new Date());
        if (refApiService.addOrUp(refApi) <= 0) {
            return JsonHelper.toJson(new ResultMsg("300", "添加失败"));
        }
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success, refApi));

    }

    /**
     * 修改系统接口定义
     *
     * @param request
     * @param refApi
     * @return
     */
    @RequestMapping(value = "/upRefApi", method = RequestMethod.POST)
    @ResponseBody
    public String upRefApi(HttpServletRequest request, @RequestBody RefApi refApi) {
        RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
        refApi.setUpdateby(loginRefUser.getCode());
        refApi.setUpdate_time(new Date());
        if (refApiService.addOrUp(refApi) <= 0) {
            return JsonHelper.toJson(new ResultMsg("300", "修改失败"));
        }
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success, refApi));

    }

    /**
     * 删除
     *
     * @param codes 多个code逗号相隔
     * @return
     */
    @RequestMapping(value = "/delRefApi", method = RequestMethod.POST)
    @ResponseBody
    public String delRefApi(@RequestBody JSONObject object) {
        String codes = object.getString("codes");
        if (StringUtils.isEmpty(codes)) {
            return JsonHelper.toJson(new ResultMsg("300", "code不能为空"));
        }
        //判断下是否有下一级数据 有不能删
        Set<String> collect = Arrays.stream(codes.split(",")).collect(Collectors.toSet());
        RefApiItem refApiItem = new RefApiItem();
        refApiItem.setCodeSet(collect);
        if (refApiItemService.countTotal(refApiItem) > 0L) {
            return JsonHelper.toJson(new ResultMsg("300", "存在下一级数据，无法删除"));
        }
        refApiService.delete(codes);
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success));
    }

    /**
     * 后端分页查询集合
     *
     * @param object
     * @return
     */
    @RequestMapping(value = "/listRefApi", method = RequestMethod.POST)
    @ResponseBody
    public String listRefApi(@RequestBody JSONObject object) {
        RefApi refApi = JSONObject.parseObject(JSON.toJSONString(object.getJSONObject("params")), RefApi.class);
        if (object.getInteger("offset") != null && object.getInteger("limit") != null) {
            refApi.setPageNum(object.getInteger("offset"));
            refApi.setPageSize(object.getInteger("limit"));
        }
        JSONObject sort = object.getJSONObject("sort");
        if (sort != null) {
            String prop = sort.getString("prop");
            String order = sort.getString("order");
            refApi.setProp(prop);
            refApi.setOrder(order);
        }

        return JsonHelper.toJson(new ResultMsg(ResultCode.Success, refApiService.findPageList(refApi), refApiService.countTotal(refApi)));
    }

}
