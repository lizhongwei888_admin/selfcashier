package com.zzsy.api.mapper;

import com.lujie.common.dao.BasicMapper;
import com.zzsy.api.entity.DwMerchant;
import com.zzsy.api.entity.RefMenu;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface DwMerchantMapper extends BasicMapper {

    DwMerchant getInfoByMerchantCode(String merchantCode);

    List<DwMerchant> findAllByParam(@Param("p") DwMerchant merchant);

    int deleteByCodes(String code);

    List<DwMerchant> getListByDwMerchantCodes(@Param("p") Set<String> dwMerchantcodeSet);

}