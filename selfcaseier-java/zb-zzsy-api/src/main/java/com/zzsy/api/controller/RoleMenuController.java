package com.zzsy.api.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lujie.common.util.JsonHelper;
import com.zzsy.api.entity.Role;
import com.zzsy.api.entity.RoleMenu;
import com.zzsy.api.mapper.RoleMenuMapper;
import com.zzsy.api.service.RoleMenuService;

/**
 * 角色菜单
 * @author hupei
 *
 */
@Controller
@RequestMapping(value = "/rolemenu")
public class RoleMenuController  {
	
	@Autowired
	private RoleMenuService roleMenuService;
	@Autowired
	private RoleMenuMapper roleMenumapper;
	
	/**
	 * 保存角色菜单关系
	 * @param menus
	 * @param roleid
	 * @return
	 */
	@RequestMapping(value = "/batchSaveRoleMenu", method = RequestMethod.POST)
	@ResponseBody
	public String batchSaveRoleMenu(@RequestBody Role role , HttpServletRequest request){
		return JsonHelper.toJson(roleMenuService.batchSaveRoleMenu(role, request));
	}
	@RequestMapping(value = "/batchDeleteRoleMenu", method = RequestMethod.POST)
	@ResponseBody
	public String batchDeleteRoleMenu(@RequestBody Role role , HttpServletRequest request){
		return JsonHelper.toJson(roleMenuService.batchDeleteRoleMenu(role, request));
	}
	@RequestMapping(value = "/findAll", method = RequestMethod.POST)
	@ResponseBody
	public String findAll(@RequestBody RoleMenu roleMenu , HttpServletRequest request){
		return JsonHelper.toJsonResultMsg(roleMenumapper.findList(roleMenu));
	}
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	@ResponseBody
	public String list(@RequestBody RoleMenu roleMenu , HttpServletRequest request){
		List list=roleMenumapper.findPageList(roleMenu);
		Long totalCount=roleMenumapper.countTotal(roleMenu);
		
		return JsonHelper.toJsonResultMsg(list,totalCount);
	}
	
	
	
}
