package com.zzsy.api.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.lujie.common.entity.BasicEntity;

import java.util.Date;

public class DwOrder extends BasicEntity {
    private String code;
    private String userId;
    private String userCode;
    private String userPhone;
    private String userName;
    private String orderNo;
    private Integer orderState;
    private Integer goodTMoney;
    private Integer promotTMoney;
    private Integer actualTMoney;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date saleTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date cancelTime;
    private Integer orderChannel;
    private String shopId;
    private String shopName;
    private String cashID;
    private String cashCode;
    private String merchantCode;
    private String shopCode;
    private String merchantId;
    private String deviceCode;
    private Integer erpState;
    private String requestJsonErp;
    private String returnJsonErp;
    private String requestJsonPay;
    private String returnJsonPay;

    private String dsfOrderID;
    private int receiptState;
    private String requestJsonReceipt;

    public String getDsfOrderID() {
        return dsfOrderID;
    }

    public void setDsfOrderID(String dsfOrderID) {
        this.dsfOrderID = dsfOrderID;
    }

    private String returnJsonReceipt;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Integer getOrderState() {
        return orderState;
    }

    public void setOrderState(Integer orderState) {
        this.orderState = orderState;
    }

    public Integer getGoodTMoney() {
        return goodTMoney;
    }

    public void setGoodTMoney(Integer goodTMoney) {
        this.goodTMoney = goodTMoney;
    }

    public Integer getPromotTMoney() {
        return promotTMoney;
    }

    public void setPromotTMoney(Integer promotTMoney) {
        this.promotTMoney = promotTMoney;
    }

    public Integer getActualTMoney() {
        return actualTMoney;
    }

    public void setActualTMoney(Integer actualTMoney) {
        this.actualTMoney = actualTMoney;
    }

    public Date getSaleTime() {
        return saleTime;
    }

    public void setSaleTime(Date saleTime) {
        this.saleTime = saleTime;
    }

    public Date getCancelTime() {
        return cancelTime;
    }

    public void setCancelTime(Date cancelTime) {
        this.cancelTime = cancelTime;
    }

    public Integer getOrderChannel() {
        return orderChannel;
    }

    public void setOrderChannel(Integer orderChannel) {
        this.orderChannel = orderChannel;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getCashID() {
        return cashID;
    }

    public void setCashID(String cashID) {
        this.cashID = cashID;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public Integer getErpState() {
        return erpState;
    }

    public void setErpState(Integer erpState) {
        this.erpState = erpState;
    }

    public String getRequestJsonErp() {
        return requestJsonErp;
    }

    public void setRequestJsonErp(String requestJsonErp) {
        this.requestJsonErp = requestJsonErp;
    }

    public String getReturnJsonErp() {
        return returnJsonErp;
    }

    public void setReturnJsonErp(String returnJsonErp) {
        this.returnJsonErp = returnJsonErp;
    }

    public String getRequestJsonPay() {
        return requestJsonPay;
    }

    public void setRequestJsonPay(String requestJsonPay) {
        this.requestJsonPay = requestJsonPay;
    }

    public String getReturnJsonPay() {
        return returnJsonPay;
    }

    public void setReturnJsonPay(String returnJsonPay) {
        this.returnJsonPay = returnJsonPay;
    }

    public String getCashCode() {
        return cashCode;
    }

    public void setCashCode(String cashCode) {
        this.cashCode = cashCode;
    }

    public String getMerchantCode() {
        return merchantCode;
    }

    public void setMerchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public String getDeviceCode() {
        return deviceCode;
    }

    public void setDeviceCode(String deviceCode) {
        this.deviceCode = deviceCode;
    }



    public int getReceiptState() {
        return receiptState;
    }

    public void setReceiptState(int receiptState) {
        this.receiptState = receiptState;
    }

    public String getRequestJsonReceipt() {
        return requestJsonReceipt;
    }

    public void setRequestJsonReceipt(String requestJsonReceipt) {
        this.requestJsonReceipt = requestJsonReceipt;
    }

    public String getReturnJsonReceipt() {
        return returnJsonReceipt;
    }

    public void setReturnJsonReceipt(String returnJsonReceipt) {
        this.returnJsonReceipt = returnJsonReceipt;
    }
}
