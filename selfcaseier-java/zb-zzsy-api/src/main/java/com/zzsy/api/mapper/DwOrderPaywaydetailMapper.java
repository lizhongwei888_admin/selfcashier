package com.zzsy.api.mapper;

import com.lujie.common.dao.BasicMapper;
import com.zzsy.api.entity.DwOrderPaywaydetail;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DwOrderPaywaydetailMapper extends BasicMapper{
    void insertData(@Param("p") DwOrderPaywaydetail d);

    List<DwOrderPaywaydetail> findListByOderId(String orderId,String type);

}