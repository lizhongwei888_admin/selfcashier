package com.zzsy.api.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.util.JsonHelper;
import com.zzsy.api.entity.RefLogAdmin;
import com.zzsy.api.entity.RefRole;
import com.zzsy.api.entity.RefStorage;
import com.zzsy.api.entity.RefUser;
import com.zzsy.api.service.RefLogAdminService;
import com.zzsy.api.service.RefStorageService;
import com.zzsy.api.service.RefUserService;
import com.zzsy.api.service.UploadService;
import com.zzsy.api.util.FileDownUtil;
import com.zzsy.api.util.RedisRefUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 文件上传管理
 */
@Controller
@RequestMapping(value = "/refStorage")
public class RefStorageController {

    private static final Logger log = LoggerFactory.getLogger(RefStorageController.class);

    @Autowired
    private RefStorageService refStorageService;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private RefLogAdminService refLogAdminService;


    /*@RequestMapping(value = "/upRefStorage", method = RequestMethod.POST)
    @ResponseBody
    public String upRefStorage(HttpServletRequest request, @RequestBody RefStorage refStorage) {
        if (refStorageService.addOrUp(refStorage) <= 0) {
            return JsonHelper.toJson(new ResultMsg("300", "修改失败"));
        }
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success, refStorage));

    }*/

    /**
     * 删除
     *
     * @param object
     * @return
     */
    @RequestMapping(value = "/delRefStorage", method = RequestMethod.POST)
    @ResponseBody
    public String delRefStorage(@RequestBody JSONObject object, HttpServletRequest request) {
        String codes = object.getString("codes");
        if (StringUtils.isEmpty(codes)) {
            return JsonHelper.toJson(new ResultMsg("300", "code不能为空"));
        }
        RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
        refStorageService.delete(codes);

        this.refLogAdminService.create(new RefLogAdmin(loginRefUser.getCode()
                , "存储页面", "删除", "delRefStorage", "/refStorage/delRefStorage"
                , codes, "ok", loginRefUser.getOrgID()));
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success));
    }

    /**
     * 后端分页查询集合
     *
     * @param object
     * @return
     */
    @RequestMapping(value = "/listRefStorage", method = RequestMethod.POST)
    @ResponseBody
    public String listRefStorage(@RequestBody JSONObject object) {
        RefStorage refStorage = JSONObject.parseObject(JSON.toJSONString(object.getJSONObject("params")), RefStorage.class);
        if (object.getInteger("offset") != null && object.getInteger("limit") != null) {
            refStorage.setPageNum(object.getInteger("offset"));
            refStorage.setPageSize(object.getInteger("limit"));
        }
        JSONObject sort = object.getJSONObject("sort");
        if (sort != null) {
            String prop = sort.getString("prop");
            String order = sort.getString("order");
            refStorage.setProp(prop);
            refStorage.setOrder(order);
        }

        //对文件类型进行判断
        if (!StringUtils.isEmpty(refStorage.getFileType())) {
            Set<String> fileTypeSet = null;
            if ("mp4".equals(refStorage.getFileType())) {
                String mp4Str = "mp3,mp4,wav,flac,aac,avi,mov,mkv";
                fileTypeSet = Arrays.stream(mp4Str.split(",")).collect(Collectors.toSet());

            }
            if ("img".equals(refStorage.getFileType())) {
                String imgStr = "jpg,jpeg,png,bmp,gif,webp";
                fileTypeSet = Arrays.stream(imgStr.split(",")).collect(Collectors.toSet());
            }
            if ("file".equals(refStorage.getFileType())) {
                String fileStr = "pdf,txt,doc,docx,xls,csv,xlsx,zip,rar,7z";
                fileTypeSet = Arrays.stream(fileStr.split(",")).collect(Collectors.toSet());
            }
            refStorage.setFileTypeSet(fileTypeSet);

        }

        return JsonHelper.toJson(new ResultMsg(ResultCode.Success, refStorageService.findPageList(refStorage), refStorageService.countTotal(refStorage)));
    }

    @GetMapping(value = "/down")
    public String down(String code, HttpServletRequest request, HttpServletResponse response) {
        if (StringUtils.isEmpty(code)) {
            return JsonHelper.toJson(new ResultMsg("300", "code不能为空"));
        }
        RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
        RefStorage refStorage = new RefStorage();
        refStorage.setCode(code);
        RefStorage oneByParms = refStorageService.getOneByParms(refStorage);
        if (oneByParms == null) {
            return JsonHelper.toJson(new ResultMsg("300", "请选择有效数据"));
        }
        //设置文件ContentType类型
        String contentType = FileDownUtil.getContentType(oneByParms.getFileType());
        if (StringUtils.isEmpty(contentType)) {
            return JsonHelper.toJson(new ResultMsg("300", "下载类型错误失败"));
        }
        response.setContentType(contentType);
        if (!FileDownUtil.downUrlFile(oneByParms.getFileUrl(), oneByParms.getFileName(), response)) {
            return JsonHelper.toJson(new ResultMsg("300", "下载失败"));
        }

        this.refLogAdminService.create(new RefLogAdmin(loginRefUser.getCode()
                , "存储", "下载", "down", "/refStorage/down"
                , oneByParms, "ok", loginRefUser.getOrgID()));
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success));
    }

}
