package com.zzsy.api.mapper;

import com.lujie.common.dao.BasicMapper;
import com.zzsy.api.entity.RefUser;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface RefUserMapper extends BasicMapper {

    RefUser getOneByParms(@Param("p") RefUser refUser);

    /**
     * 通过角色id获得用户信息
     *
     * @param idSet
     * @return
     */
    RefUser getOneByRoleCodes(@Param("p") Set<String> idSet);

    /**
     * 通过用户code集合获取用户信息集合
     *
     * @param codeSet
     * @return
     */
    List<RefUser> getListByCodes(@Param("p") Set<String> codeSet);
}
