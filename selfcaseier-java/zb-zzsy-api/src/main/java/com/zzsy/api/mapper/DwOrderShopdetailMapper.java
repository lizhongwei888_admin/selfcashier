package com.zzsy.api.mapper;

import com.lujie.common.dao.BasicMapper;
import com.zzsy.api.entity.DwOrderRefunddetail;
import com.zzsy.api.entity.DwOrderShopdetail;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DwOrderShopdetailMapper extends BasicMapper{
    void insertData(@Param("p") DwOrderShopdetail d);

    List<DwOrderShopdetail> findListByOderId(String orderId);

    void updateRefund(@Param("p") DwOrderShopdetail d);

}