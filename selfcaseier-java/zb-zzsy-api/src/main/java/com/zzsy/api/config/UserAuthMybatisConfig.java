package com.zzsy.api.config;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@PropertySource("classpath:application-datasource-userauth.properties")
@MapperScan(basePackages={ "com.zzsy.api.mapper"},
sqlSessionFactoryRef="sqlSessionFactory")
@EnableTransactionManagement(proxyTargetClass=true)
public class UserAuthMybatisConfig {
    
    @Value("${mybatis.userauth.configLocation}")
    private String configLocation;
    
    @Value("${mybatis.userauth.typeAliasesPackage}")
    private String typeAliasesPackage;
    
    @Value("${mybatis.userauth.mapperLocations}")
    private String mapperLocations;
    
    //@Primary
    @Bean(name="transactionManager")
    public PlatformTransactionManager transactionManager(@Qualifier("dataSource")DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }
    
    //@Primary
    @Bean(name="sqlSessionFactory")
    public SqlSessionFactory sqlSessionFactory(@Qualifier("dataSource")DataSource dataSource) throws Exception {
        SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
        bean.setDataSource(dataSource);
        bean.setConfigLocation(new PathMatchingResourcePatternResolver().getResource(configLocation));
        bean.setTypeAliasesPackage(typeAliasesPackage);
        System.out.println("----------------------"+mapperLocations);
        bean.setMapperLocations(getMapperLocations(mapperLocations));
        return bean.getObject();
    }
    
    public Resource[] getMapperLocations(String mapperLocations) {
    	String[] mapperLocationArr=mapperLocations.split(",");
    	if(mapperLocationArr==null || mapperLocationArr.length==0){
    		System.out.println("没有配置mapper location");
    	}
    	ResourcePatternResolver resourceResolver = new PathMatchingResourcePatternResolver();
    	List<Resource> resources = new ArrayList();
    	
    	for(String mapperLocation:mapperLocationArr){
    		try {
    	    	Resource[] mappers = resourceResolver.getResources(mapperLocation);
    	    	resources.addAll(Arrays.asList(mappers));
    	    } catch (IOException e) {
    	    	e.printStackTrace();
    	    }
    	}
    	
    	return resources.toArray(new Resource[resources.size()]);
    }
    

    //@Primary
    @Bean(name="dataSource")
    @ConfigurationProperties(prefix="spring.datasource.lujie.userauth")
    public DataSource dataSource() {
        DataSource ds = DataSourceBuilder.create().build();
        return ds;
    }

}
