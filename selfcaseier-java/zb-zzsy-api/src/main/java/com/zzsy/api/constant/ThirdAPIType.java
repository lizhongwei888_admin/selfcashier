package com.zzsy.api.constant;

/**
 * 申请类型
 * @author sl
 *
 */
public enum ThirdAPIType  {

	Generate(1, "生成"),
	EastChina(2, "华东"),
	Ametrum(3, "美讯"),
	LuJie(4, "陆杰")
	;
	
	private Integer code;

	
	private String desc;

	ThirdAPIType(Integer code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public Integer getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}
	
	public static String getDesc(Integer code) {
		
		ThirdAPIType[] logTypes = values();
		for(ThirdAPIType logType : logTypes) {
			if(code.equals(logType.getCode())) {
				return logType.getDesc();
			}
		}
		return "";
	}

}
