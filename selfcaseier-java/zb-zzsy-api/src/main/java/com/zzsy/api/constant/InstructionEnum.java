package com.zzsy.api.constant;

/**
 * 内集卡调度-指令描述
 */
public enum InstructionEnum  {

	START( 0 , "开始") ,
	COMPLETE(1 , "完成") ,
	EXECUTING (3 , "执行中") ;
	
	private Integer code;

	private String desc;

	InstructionEnum(Integer code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public Integer getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}
	
	public static String getDesc(Integer code) {
		
		InstructionEnum[] logTypes = values();
		for(InstructionEnum logType : logTypes) {
			if(code.equals(logType.getCode())) {
				return logType.getDesc();
			}
		}
		return "";
	}

}
