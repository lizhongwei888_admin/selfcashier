package com.zzsy.api.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.util.JsonHelper;
import com.zzsy.api.entity.RefMenu;
import com.zzsy.api.entity.RefOrganize;
import com.zzsy.api.entity.RefUser;
import com.zzsy.api.service.RefOrganizeService;
import com.zzsy.api.util.RedisRefUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * 系统-组织管理控制器
 */
@Controller
@RequestMapping(value = "/refOrganize")
public class RefOrganizeController {

    @Autowired
    private RefOrganizeService refOrganizeService;
    @Autowired
    private RedisTemplate redisTemplate;

    @RequestMapping(value = "/addRefOrganize", method = RequestMethod.POST)
    @ResponseBody
    public String addRefOrganize(HttpServletRequest request, @RequestBody RefOrganize refOrganize) {
        RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
        refOrganize.setCreateby(loginRefUser.getCode());
        refOrganize.setCreate_time(new Date());
        refOrganize.setUpdateby(loginRefUser.getCode());
        refOrganize.setUpdate_time(new Date());
        if (refOrganizeService.addOrUp(refOrganize) <= 0) {
            return JsonHelper.toJson(new ResultMsg("300", "添加失败"));
        }
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success, refOrganize));

    }

    @RequestMapping(value = "/upRefOrganize", method = RequestMethod.POST)
    @ResponseBody
    public String upRefOrganize(HttpServletRequest request, @RequestBody RefOrganize refOrganize) {
        if (refOrganize.getCode() == null) {
            return JsonHelper.toJson(new ResultMsg("300", "code不能为空"));
        }
        RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
        refOrganize.setUpdateby(loginRefUser.getCode());
        refOrganize.setUpdate_time(new Date());
        if (refOrganizeService.addOrUp(refOrganize) <= 0) {
            return JsonHelper.toJson(new ResultMsg("300", "修改失败"));
        }
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success, refOrganize));

    }

    /**
     * 删除
     *
     * @param object
     * @return
     */
    @RequestMapping(value = "/delRefOrganize", method = RequestMethod.POST)
    @ResponseBody
    public String delRefOrganize(@RequestBody JSONObject object) {
        String codes = object.getString("codes");
        if (StringUtils.isEmpty(codes)) {
            return JsonHelper.toJson(new ResultMsg("300", "code不能为空"));
        }
        refOrganizeService.delete(codes);
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success));
    }

    /**
     * 后端分页查询集合
     *
     * @param object
     * @return
     */
    @RequestMapping(value = "/listRefOrganize", method = RequestMethod.POST)
    @ResponseBody
    public String listRefOrganize(@RequestBody JSONObject object) {
        RefOrganize refOrganize = JSONObject.parseObject(JSON.toJSONString(object.getJSONObject("params")), RefOrganize.class);
        if (object.getInteger("offset") != null && object.getInteger("limit") != null) {
            refOrganize.setPageNum(object.getInteger("offset"));
            refOrganize.setPageSize(object.getInteger("limit"));
        }
        JSONObject sort = object.getJSONObject("sort");
        if (sort != null) {
            String prop = sort.getString("prop");
            String order = sort.getString("order");
            refOrganize.setProp(prop);
            refOrganize.setOrder(order);
        }
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success, refOrganizeService.findPageList(refOrganize), refOrganizeService.countTotal(refOrganize)));
    }

}
