package com.zzsy.api.entity;

import com.lujie.common.entity.BasicEntity;

import java.io.Serializable;
import java.util.Set;

public class RefDicData extends BasicEntity implements Serializable {
    private static final long serialVersionUID = -301062307193761449L;

    private String code;

    //字典标签
    private String dicDateName;
    //字典键值
    private String dicDateKey;
    //所属字典类型
    private String dicTypeId;
    //样式属性
    private String styleCss;
    //字典排序
    private Integer sort;
    //是否默认：1是；2否
    private Integer isDefault;
    //备注
    private String remark;
    //状态：true启用；false禁用
    private String state;

    //父级code集合
    private Set<String> codeSet;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDicDateName() {
        return dicDateName;
    }

    public void setDicDateName(String dicDateName) {
        this.dicDateName = dicDateName;
    }

    public String getDicDateKey() {
        return dicDateKey;
    }

    public void setDicDateKey(String dicDateKey) {
        this.dicDateKey = dicDateKey;
    }

    public String getDicTypeId() {
        return dicTypeId;
    }

    public void setDicTypeId(String dicTypeId) {
        this.dicTypeId = dicTypeId;
    }

    public String getStyleCss() {
        return styleCss;
    }

    public void setStyleCss(String styleCss) {
        this.styleCss = styleCss;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Integer isDefault) {
        this.isDefault = isDefault;
    }

    @Override
    public String getRemark() {
        return remark;
    }

    @Override
    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Set<String> getCodeSet() {
        return codeSet;
    }

    public void setCodeSet(Set<String> codeSet) {
        this.codeSet = codeSet;
    }
}
