package com.zzsy.api.util;

import com.zzsy.api.constant.KeysManager;
import com.zzsy.api.entity.RefUser;
import com.zzsy.api.entity.User;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.redis.core.RedisTemplate;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class RedisRefUtil {
    public static final String LoginUserToken = KeysManager.Ref_User_Auth + "Login_Ref_User_Token_";

    private static final Integer LoginUserSessionTime = 7 * 24 * 60;

    /**
     * 根据后台登录token获得用户信息
     * RefUser
     *
     * @param redisTemplate
     * @param request
     * @return
     */
    public static RefUser getRefUser(RedisTemplate redisTemplate, HttpServletRequest request) {
        String token = getToken(request);
        if (StringUtils.isEmpty(token)) {
            //若无token，则需要登录
            return null;
        }
        String tokenKey = LoginUserToken + token;
        return (RefUser) redisTemplate.opsForValue().get(tokenKey);
    }

    /**
     * 获得后台登录用户的token
     *
     * @param request
     * @return
     */
    public static String getToken(HttpServletRequest request) {
        String token = request.getHeader("token");
        if (StringUtils.isEmpty(token)) {
            token = request.getParameter("token");
        }
        if (StringUtils.isEmpty(token)) {
            return null;
        }

        return token;
    }

    public static String genarateTokenAndSaveRefUser(RedisTemplate redisTemplate, RefUser refUser, Integer loginUserSessionTime) {
        String userid = refUser.getCode();
        String token = userid + ":" + UUID.randomUUID().toString().replace("-", "");//token生成策略
        refUser.setToken(token);
        String tokenKey = LoginUserToken + token;
        redisTemplate.opsForValue().set(tokenKey, refUser);
        redisTemplate.expire(tokenKey, loginUserSessionTime, TimeUnit.MINUTES);
        return token;
    }

    public static void removeToken(RedisTemplate redisTemplate, HttpServletRequest request) {
        String token = getToken(request);
        String tokenKey = LoginUserToken + token;
        redisTemplate.delete(tokenKey);
    }

    public static RefUser getRefUserByToken(RedisTemplate redisTemplate, HttpServletRequest request) {
        String token = getToken(request);
        String tokenKey = LoginUserToken + token;
        return (RefUser) redisTemplate.opsForValue().get(tokenKey);
    }

    public static void updateLoginStatus(RedisTemplate redisTemplate, HttpServletRequest request) {
        String token = getToken(request);
        String tokenKey = LoginUserToken + token;
        Object userObj = redisTemplate.opsForValue().get(tokenKey);
        redisTemplate.opsForValue().set(tokenKey, userObj);
        redisTemplate.expire(tokenKey, LoginUserSessionTime, TimeUnit.MINUTES);
    }

    public static void removeTokenByUserCode(RedisTemplate redisTemplate, String userCode) {
        String token = userCode + ":";
        String tokenKey = LoginUserToken + token;
        RedisUtil.deleteStartLike(redisTemplate, tokenKey);

    }

    public static void savePermanentRefUser(RedisTemplate redisTemplate, RefUser refUser) {
        if (refUser == null || refUser.getToken() == null) {
            return;
        }
        String tokenKey = LoginUserToken + refUser.getToken();
        redisTemplate.opsForValue().set(tokenKey, refUser);
    }
}
