package com.zzsy.api.entity;

import java.util.Date;

public class City extends ModuleRoleRelative{

	private static final long serialVersionUID = -2787624493067620084L;
	private String cityid;

    private String cityname;

    private String areacode;

    private String info;

    private Short isvalid;

    private Date inserttime;

    private Date lasttime;

    private String reservearg1;

    private String reservearg2;
    
    private Integer version;
    private Integer orderNum;
    

    public String getCityid() {
        return cityid;
    }

    public void setCityid(String cityid) {
        this.cityid = cityid == null ? null : cityid.trim();
    }

    public String getCityname() {
        return cityname;
    }

    public void setCityname(String cityname) {
        this.cityname = cityname == null ? null : cityname.trim();
    }

    public String getAreacode() {
        return areacode;
    }

    public void setAreacode(String areacode) {
        this.areacode = areacode == null ? null : areacode.trim();
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info == null ? null : info.trim();
    }

    public Short getIsvalid() {
        return isvalid;
    }

    public void setIsvalid(Short isvalid) {
        this.isvalid = isvalid;
    }

    public Date getInserttime() {
        return inserttime;
    }

    public void setInserttime(Date inserttime) {
        this.inserttime = inserttime;
    }

    public Date getLasttime() {
        return lasttime;
    }

    public void setLasttime(Date lasttime) {
        this.lasttime = lasttime;
    }

    public String getReservearg1() {
        return reservearg1;
    }

    public void setReservearg1(String reservearg1) {
        this.reservearg1 = reservearg1 == null ? null : reservearg1.trim();
    }

    public String getReservearg2() {
        return reservearg2;
    }

    public void setReservearg2(String reservearg2) {
        this.reservearg2 = reservearg2 == null ? null : reservearg2.trim();
    }

    
	
	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public Integer getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(Integer orderNum) {
		this.orderNum = orderNum;
	}
	
	
	
}