package com.zzsy.api.controller;

import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.util.JsonHelper;
import com.zzsy.api.entity.DwSyBags;
import com.zzsy.api.service.DwSyBagsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping(value = "/dwSyBags")
public class DwSyBagsController {

	@Autowired
	private DwSyBagsService dwSyBagsService;

	@RequestMapping(value={"/findList"})
	@ResponseBody
	public String findList(HttpServletRequest request, @RequestBody DwSyBags dwSyBags){
		return JsonHelper.toJson(dwSyBagsService.findByMerchantId(dwSyBags));
	}

	@RequestMapping(value={"/insertData"})
	@ResponseBody
	public String insertData(HttpServletRequest request, @RequestBody DwSyBags dwSyBags){
		return JsonHelper.toJson(dwSyBagsService.insertData(request,dwSyBags));
	}
	@RequestMapping(value={"/findByMerchantId"})
	@ResponseBody
	public String findByShopId(HttpServletRequest request, @RequestBody DwSyBags dwSyBags){
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success,dwSyBagsService.findByMerchantId(dwSyBags)));
	}

}
