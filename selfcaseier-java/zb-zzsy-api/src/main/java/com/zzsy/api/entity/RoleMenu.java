package com.zzsy.api.entity;

import java.io.Serializable;

import com.lujie.common.entity.BasicEntity;

public class RoleMenu extends BasicEntity  implements Serializable{
	private static final long serialVersionUID = 1252619653370243673L;

	private Integer id;

    private String mid;

    private String rid;
    
    private String menuName;
    private String roleName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid == null ? null : mid.trim();
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid == null ? null : rid.trim();
    }

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
    
    
}