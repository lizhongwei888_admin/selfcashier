package com.zzsy.api.mapper;

import com.lujie.common.dao.BasicMapper;
import com.zzsy.api.entity.RefDicType;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface RefDicTypeMapper extends BasicMapper {

    RefDicType findOneRefDicType(@Param("p") RefDicType refDicType);


}
