package com.zzsy.api.entity;

import com.lujie.common.entity.BasicEntity;

import java.io.Serializable;
import java.util.Set;

/**
 * 系统配置-系统接口定义-配置项(RefApiItem)实体类
 *
 * @author makejava
 * @since 2024-02-04 20:21:46
 */
public class RefApiItem  extends BasicEntity implements Serializable {
    private static final long serialVersionUID = -99170583923013023L;
    /**
     * 唯一标识
     */
    private String code;
    /**
     * 所属系统接口ID
     */
    private String apiid;
    /**
     * 配置项名称
     */
    private String itemname;
    /**
     * 配置项参数
     */
    private String itemkey;
    /**
     * 配置项参数值
     */
    private String itemvalue;
    /**
     * 配置项说明
     */
    private String remark;
    /**
     * 状态：true启用；false禁用
     */
    private String state;

    //父级code集合
    private Set<String> codeSet;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getApiid() {
        return apiid;
    }

    public void setApiid(String apiid) {
        this.apiid = apiid;
    }

    public String getItemname() {
        return itemname;
    }

    public void setItemname(String itemname) {
        this.itemname = itemname;
    }

    public String getItemkey() {
        return itemkey;
    }

    public void setItemkey(String itemkey) {
        this.itemkey = itemkey;
    }

    public String getItemvalue() {
        return itemvalue;
    }

    public void setItemvalue(String itemvalue) {
        this.itemvalue = itemvalue;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Set<String> getCodeSet() {
        return codeSet;
    }

    public void setCodeSet(Set<String> codeSet) {
        this.codeSet = codeSet;
    }
}

