package com.zzsy.api.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.util.JsonHelper;
import com.zzsy.api.entity.RefApiItem;
import com.zzsy.api.entity.RefCity;
import com.zzsy.api.entity.RefUser;
import com.zzsy.api.service.RefCityService;
import com.zzsy.api.util.RedisRefUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 地市管理
 */
@Controller
@RequestMapping(value = "/refCity")
public class RefCityController {

    @Autowired
    private RefCityService refCityService;
    @Autowired
    private RedisTemplate redisTemplate;


    /**
     * @param request
     * @param refCity
     * @return
     */
    @RequestMapping(value = "/addRefCity", method = RequestMethod.POST)
    @ResponseBody
    public String addRefCity(HttpServletRequest request, @RequestBody RefCity refCity) {
        RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
        refCity.setCreateby(loginRefUser.getCode());
        refCity.setCreate_time(new Date());
        refCity.setUpdateby(loginRefUser.getCode());
        refCity.setUpdate_time(new Date());
        if (refCityService.addOrUp(refCity) <= 0) {
            return JsonHelper.toJson(new ResultMsg("300", "添加失败"));
        }
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success, refCity));

    }

    @RequestMapping(value = "/upRefCity", method = RequestMethod.POST)
    @ResponseBody
    public String upRefCity(HttpServletRequest request, @RequestBody RefCity refCity) {
        if (StringUtils.isEmpty(refCity.getCode())) {
            return JsonHelper.toJson(new ResultMsg("300", "code不能为空"));
        }
        RefUser loginRefUser = RedisRefUtil.getRefUser(redisTemplate, request);
        refCity.setUpdateby(loginRefUser.getCode());
        refCity.setUpdate_time(new Date());
        if (refCityService.addOrUp(refCity) <= 0) {
            return JsonHelper.toJson(new ResultMsg("300", "修改失败"));
        }
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success, refCity));

    }

    /**
     * 删除
     *
     * @param object
     * @return
     */
    @RequestMapping(value = "/delRefCity", method = RequestMethod.POST)
    @ResponseBody
    public String delRefCity(@RequestBody JSONObject object) {
        String codes = object.getString("codes");
        if (StringUtils.isEmpty(codes)) {
            return JsonHelper.toJson(new ResultMsg("300", "code不能为空"));
        }
        //判断下是否有下一级数据 有不能删
        Set<String> collect = Arrays.stream(codes.split(",")).collect(Collectors.toSet());
        RefCity refCity = new RefCity();
        if (refCityService.countTotal(refCity) > 0L) {
            return JsonHelper.toJson(new ResultMsg("300", "存在下一级数据，无法删除"));
        }
        refCityService.delete(codes);
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success));
    }

    /**
     * 后端分页查询集合
     *
     * @param object
     * @return
     */
    @RequestMapping(value = "/listRefCity", method = RequestMethod.POST)
    @ResponseBody
    public String listRefCity(@RequestBody JSONObject object) {
        RefCity refCity = JSONObject.parseObject(JSON.toJSONString(object.getJSONObject("params")), RefCity.class);
        if (object.getInteger("offset") != null && object.getInteger("limit") != null) {
            refCity.setPageNum(object.getInteger("offset"));
            refCity.setPageSize(object.getInteger("limit"));
        }
        JSONObject sort = object.getJSONObject("sort");
        if (sort != null) {
            String prop = sort.getString("prop");
            String order = sort.getString("order");
            refCity.setProp(prop);
            refCity.setOrder(order);
        }
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success, refCityService.findPageList(refCity), refCityService.countTotal(refCity)));
    }

    /**
     * 查询省市区数据三级架构返回
     *
     * @return
     */
    @RequestMapping(value = "/findAllRefCity", method = RequestMethod.POST)
    @ResponseBody
    public String findAllRefCity() {
        return JsonHelper.toJson(new ResultMsg(ResultCode.Success, refCityService.findAllRefCity()));
    }

}
