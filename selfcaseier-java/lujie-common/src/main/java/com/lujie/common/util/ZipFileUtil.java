package com.lujie.common.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class ZipFileUtil
{
	public static void main(String[] args){
		genZiporUnzip(args);
		
	}
	
	public static void genZiporUnzip(String[] args){
		if(args==null || args.length==0){
			System.out.println("请求参数");
			System.out.println("zip sourcefolder destfile");
			System.out.println("unzip sourcezipfile destfolder");
			
			return;
		}
		if(args.length<3){
			System.out.println("请求参数有误");
			return;
		}
		for(String arg:args){
			System.out.println(arg);
		}
		try{
			if(args[0].toLowerCase().equals("zip")){
				compressFile(args[1],args[2]);	
			}else{
				ZipUncompress(args[1],args[2]);	
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	public void getFileList(ArrayList<File> allFileList,File file){
		if(file==null) return;
		
		if(file.isFile()){
			allFileList.add(file);
			System.out.println(file.getPath());
			return;
		}
		
		File[] subFiles=file.listFiles();
		if(subFiles!=null){
			for(File subFile:subFiles){
				getFileList(allFileList,subFile);
			}	
		}
		System.out.println(file.getPath());
		allFileList.add(file);
	}
	
	public Boolean deleteFile(String fileName,Integer deleteType){
		ArrayList<File> allFileList=new ArrayList<>();
		File file=new File(fileName);
		getFileList(allFileList,file);
		if(!allFileList.isEmpty()){
			if(deleteType.equals(1)){
				allFileList.remove(allFileList.size()-1);
			}
			if(!allFileList.isEmpty()){
				for(File deletefile:allFileList){
					if(!deletefile.delete()){
						System.out.println("删除文件失败:"+deletefile.getPath());
						return false;
					}
				}	
			}
			
		}
		
		System.out.println("删除文件完成");
		return true;
	}
	
	public static void test() {
		try{
			
			String comunitymodePath="d:/zzm/test/comunitymode";
			File comunitymodeFile=new File(comunitymodePath);
			for(File streetFile:comunitymodeFile.listFiles()){
				for(File comunityFile:streetFile.listFiles()){
					comunityFile.delete();
				}
			}
			
			
			
			/*
			FileOutputStream fos=new FileOutputStream("d:/zzm/test/test1/a.zip");
			ZipOutputStream zos=new ZipOutputStream(fos);
			File sourceFile=new File("d:/zzm/test/community");
			compressFile(zos,sourceFile,sourceFile.getName());
			zos.close();
			fos.close();*/
			System.out.println("ok");
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	public static void compressFile(String source,String dest) throws Exception{
		FileOutputStream fos=new FileOutputStream(dest);
		ZipOutputStream zos=new ZipOutputStream(fos);
		File sourceFile=new File(source);
		compressFile(zos,sourceFile,sourceFile.getName());
		zos.close();
		fos.close();
		
	}
	
	public static void compressFile(ZipOutputStream zos,File sourceFile,String name) throws Exception{
		
		System.out.println("sourceFile:"+sourceFile.getPath());
		System.out.println("name:"+name);
		
		byte[] buf=new byte[1024];
		if(sourceFile.isFile()){
			zos.putNextEntry(new ZipEntry(name));
			
			int len;
			FileInputStream fis=new FileInputStream(sourceFile);
			while((len=fis.read(buf))!=-1){
				zos.write(buf,0,len);
			}
			zos.closeEntry();
			fis.close();
		}else{
			File[] listFiles=sourceFile.listFiles();
			if(listFiles==null || listFiles.length==0){
				zos.putNextEntry(new ZipEntry(name+"/"));
				zos.closeEntry();
			}else{
				for(File file:listFiles){
					compressFile(zos,file,name+"/"+file.getName());
				}
			}
		}
		
		
		
	}
	public static void ZipUncompress(String inputFile,String destDirPath) throws Exception {
        File srcFile = new File(inputFile);//获取当前压缩文件
        // 判断源文件是否存在
        if (!srcFile.exists()) {
            throw new Exception(srcFile.getPath() + "所指文件不存在");
        }
        //开始解压
        //构建解压输入流
        ZipInputStream zIn = new ZipInputStream(new FileInputStream(srcFile));
        ZipEntry entry = null;
        File file = null;
        while ((entry = zIn.getNextEntry()) != null) {
            if (!entry.isDirectory()) {
                file = new File(destDirPath, entry.getName());
                if (!file.exists()) {
                    new File(file.getParent()).mkdirs();//创建此文件的上级目录
                }
                OutputStream out = new FileOutputStream(file);
                BufferedOutputStream bos = new BufferedOutputStream(out);
                int len = -1;
                byte[] buf = new byte[1024];
                while ((len = zIn.read(buf)) != -1) {
                    bos.write(buf, 0, len);
                }
                // 关流顺序，先打开的后关闭
                bos.close();
                out.close();
            }
        }
    }
}