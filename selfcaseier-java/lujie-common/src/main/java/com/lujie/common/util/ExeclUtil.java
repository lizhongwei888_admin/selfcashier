package com.lujie.common.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

public class ExeclUtil {
	
	
	/**
	 * 页面导出excel
	 * @param pageList
	 * @param headTitle
	 * @param field
	 * @param type
	 * @param format
	 * @param fileName
	 */
	public static void exportExcelPage(List pageList,String []headTitle,String []fieldName,String []type,String []format,String fileName,HttpServletResponse response)
	   {
		File tempfile=null;
		try{
        //声明一个工作薄
        HSSFWorkbook workbook=new HSSFWorkbook();
        //生成一个表格
        HSSFSheet sheet=workbook.createSheet("表格1");
        sheet.setDefaultColumnWidth((short)19);
        //生成一个行
        HSSFRow row=sheet.createRow(0);
        //创建excel的头
        HSSFFont font=workbook.createFont();
        font.setColor(HSSFColor.BLACK.index);
        font.setFontHeightInPoints((short)12);
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        for(short i=0;i<headTitle.length;i++){
             //声明一个头单元格
            HSSFCell cell=row.createCell(i);
             //设置单元格的字符值
            cell.setCellValue(new HSSFRichTextString(headTitle[i]));
            //生成一个头样式
            HSSFCellStyle style=workbook.createCellStyle();
            style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            style.setBorderRight(HSSFCellStyle.BORDER_THIN);
            style.setBorderTop(HSSFCellStyle.BORDER_THIN);
            style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            //生成一个头字体
            style.setFont(font);
            cell.setCellStyle(style);
            
        }
        List list=pageList;
        HSSFFont cellfont=workbook.createFont();
        cellfont.setColor(HSSFColor.BLACK.index);
        cellfont.setFontHeightInPoints((short)11);
        HSSFCellStyle cellstyle=workbook.createCellStyle();
      //设置单元格风格
        cellstyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        cellstyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        cellstyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        cellstyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        
        
        for(int m=0;m<list.size();m++){
          Object object=list.get(m);
          HSSFRow rowobj=sheet.createRow(m+1);
          //填充单元格
          for(int i=0;i<fieldName.length;i++){
            //声明一个单元格
        	  HSSFCell cell=rowobj.createCell(i);
            //获得对象中的字段值
            String fieldValue=getObjectValueByField(object,fieldName[i],type[i],format[i]);
            //设置单元格的字符值
            cell.setCellValue(new HSSFRichTextString(fieldValue));
            
            //生成一个样式           
            if(type[i]!=null){
            	if(type[i].equals("string")){
            		cellstyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);	
            	}
            	if(type[i].equals("long")||type[i].equals("int")){
            		cellstyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);	
            	}
            	if(type[i].equals("date")){
                cellstyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            	}
            	if(type[i].equals("double")){
                    cellstyle.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
                }
            	/*if(type[i].equals("constant")){
            		cellstyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);	
            	}*/
            }
            cellstyle.setWrapText(false);
            //生成一个字体
            cellstyle.setFont(cellfont);
            cell.setCellStyle(cellstyle);
          }
        }
        //创建临时文件
        tempfile = File.createTempFile(fileName, ".xls");
        FileOutputStream fout=new FileOutputStream(tempfile);
        //输出到文件
        workbook.write(fout);
        fout.flush();
        fout.close();
        //下载文件
        FileInputStream inputstream=new FileInputStream(tempfile);
        downloadFile(inputstream,fileName,response);
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		finally{
			//下载结束后，删除临时文件
			if(tempfile!=null){
				if(tempfile.exists()){
					tempfile.delete();
				}
				
			}
			
			
			
		}
    }
	
	/**
	 * 下载文件
	 * @param is
	 * @param fileName
	 * @param response
	 * @throws Exception
	 */
	public static void downloadFile(InputStream is, String fileName,HttpServletResponse response) throws Exception {
		OutputStream out = null; 
		try {
			 response.reset();    
	         response.addHeader("Content-Disposition", "attachment;filename=" + 
	            		URLEncoder.encode(fileName, "UTF-8"));
                //得到向客户端输出二进制数据的对象 
	            out = new BufferedOutputStream(response.getOutputStream()); 
                //设置返回的文件类型
				response.setContentType("application/octet-stream"); 
			    int maxBufferSize=5*1024*1024;
			 	byte[] bytes = new byte[maxBufferSize];
	            int c;
	            long result = 0;
	            while ((c = is.read(bytes)) != -1) {
	            	out.write(bytes, 0, c);
					result = result + c;
				}
	            
	            response.addHeader("Content-Length", "" + result);
	            out.flush();
			
			
		} finally {
			try
        	{
        		if (is != null)
        		{
        			is.close();
        		}
        		
        		if (out != null)
        		{
        			out.close();
        		}
        	}
        	catch(Exception e)
        	{
        		throw e;
        	}
		}
	}
	/**
	 * 通过反射得到对象中的字段值
	 * @param object
	 * @param filed
	 * @return
	 */
	private static String getObjectValueByField(Object object,String fieldName,String type,String format) throws Exception{
		String returnValue="";
		//如果list中的对象是Map类型
		if(object instanceof Map){
			Map map=(Map)object;
			returnValue=getMapValueByFieldName(map,fieldName,type,format);
		}
		//如果list中的对象是普通对象
		else{
			returnValue=getObjectValueByFieldName(object,fieldName,type,format);
		}
		return returnValue;
	}
	
	/**
	 * 通过map对象字段名，获得map里对应的值
	 * @param map
	 * @param fieldName
	 * @param type
	 * @param format
	 * @return
	 * @throws Exception
	 */
	private static String getMapValueByFieldName(Map map,String fieldName,String type,String format) throws Exception{
		String returnValue="";
		Object obj=(Object)map.get(fieldName);
	    if(obj!=null){
		    //对象数据值格式化
			returnValue=formatObjectValue(obj,type,format);
	    }
		return returnValue;
	}
	
	/**
	 * 对象数据值格式化
	 * @param obj
	 * @param type
	 * @param format
	 * @return
	 * @throws Exception
	 */
	private static String formatObjectValue(Object obj,String type,String format) throws Exception{
		String returnValue=null;
		if(type!=null&&!type.equals("")){
			if(type.equals("string")){
				returnValue=obj.toString();
			}
			else if(type.equals("date")){
				Date date=(Date)obj;
				SimpleDateFormat formatter=new SimpleDateFormat(format);
				returnValue=formatter.format(date);
			}
            else if(type.equals("long")||type.equals("int")){
            	if(obj instanceof java.lang.Integer){
            		Integer intObj=(Integer)obj;
            		returnValue=intObj.toString();
            	}
				if(obj instanceof java.lang.Long){
					Long longObj=(Long)obj;
					returnValue=longObj.toString();
				}
				if(obj instanceof java.math.BigDecimal){
					BigDecimal bigObj=(BigDecimal)obj;
					returnValue=bigObj.toString();
				}
			 }
             else if(type.equals("double")){
	            if(obj instanceof java.lang.Double){
	            	Double doubleObj=(Double)obj;
	            	DecimalFormat decimalformat = new DecimalFormat(format);
					String doublstring = decimalformat.format(doubleObj);
	            	returnValue=doublstring;
	            }
	            if(obj instanceof java.math.BigDecimal){
	            	BigDecimal bigObj=(BigDecimal)obj;
	            	DecimalFormat decimalformat = new DecimalFormat(format);
					String bigstring = decimalformat.format(bigObj);
					returnValue=bigstring;
	           }
               }
              /*else if(type.equals("constant")){
            	if(obj instanceof java.lang.Long){
					Long longObj=(Long)obj;
					String constantValue=longObj.toString();
					returnValue=Constant.getValueNameByKey.getValueNameByKey(format, constantValue);
				}
				if(obj instanceof java.math.BigDecimal){
					BigDecimal bigObj=(BigDecimal)obj;
					String constantValue=bigObj.toString();
					returnValue=Constant.getValueNameByKey.getValueNameByKey(format, constantValue);
				}
              }*/
		}
		return returnValue;
	}
	
	/**
	 * 通过普通对象字段名，获得对象里对应的值
	 * @param object
	 * @param fieldName
	 * @param type
	 * @param format
	 * @return
	 */
	private static String getObjectValueByFieldName(Object object,String fieldName,String type,String format) throws Exception{
		String returnValue="";
		Object returnObject=object;
		String fieldNameArray[]=fieldName.split("[.]");
		for(int i=0;i<fieldNameArray.length;i++){
		Class classobj=returnObject.getClass();
		//通过字段名，获得对象的get方法名
		String methodName=getMethodName(fieldNameArray[i]);
		if(methodName!=null&&!methodName.equals("")){
		Method  method=classobj.getDeclaredMethod(methodName,new Class[]{});
		returnObject=method.invoke(returnObject,new Object[]{});
		}
		}
		if(returnObject!=null){
		    //对象数据值格式化
			returnValue=formatObjectValue(returnObject,type,format);
	    }
		return returnValue;
	}
	
	/**
	 * 通过字段名，获得对象的get方法名
	 * @param fieldName
	 * @return
	 */
	private static String getMethodName(String fieldName){
		String methodName=null;
		if(fieldName!=null&&!fieldName.equals("")){
			//获得属性名的第一个字母
			String firstLetter=fieldName.substring(0,1);
			String backName="";
			if(fieldName.length()>1){
			    backName=fieldName.substring(1);
			}
			else{
				backName="";	
			}
            //组成要调用的方法
			methodName="get"+firstLetter.toUpperCase()+backName;
		}	
		return  methodName;
	}

}
