package com.lujie.common.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class DateUtil {
	//
	/**  年-月-日(yyyy-MM-dd)*/
	public final static String PATTERN_SIMPLE_DATE = "yyyy-MM-dd";

	/** 年-月-日 时:分:秒(yyyy-MM-dd HH:mm:ss)*/
	public final static String PATTERN_SIMPLE_DATETIME = "yyyy-MM-dd HH:mm:ss";

	/** 年-月-日 时:分:秒.毫秒(yyyy-MM-dd HH:mm:ss.SSS)*/
	public final static String PATTERN_SIMPLE_DATETIME_FULL = "yyyy-MM-dd HH:mm:ss.SSS";
	
	public final static String DATE_PART_YEAR = "yyyy";
	public final static String DATE_PART_MONTH = "MM";
	public final static String DATE_PART_DATE = "dd";
	public final static String DATE_PART_HOUR = "HH";
	public final static String DATE_PART_MIN = "mm";
	public final static String DATE_PART_SECOND = "ss";
	public final static String DATE_PART_MILLISECOND = "SSS";
	public final static String DATE_PART_DAY_OF_WEEK = "dw";
	
	public final static String Date_Morning = "Morning";
	public final static String Date_Afternoon = "Afternoon";
	public final static String Date_Night = "Night"; 
	
	
	/**
	 * 获取当前时间
	 * @param millis
	 * 		毫秒
	 * @return
	 */
	public static Date getTime(Long millis){
		if(millis==null){
			return null;
		}
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(millis);
		return cal.getTime();
	}
	/**
	 * 获取当前时间(年月日)
	 * @return
	 */
	public static Date getNowDate(){
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}
	/**
	 * <pre>
	 * 
	 * @param year
	 * @param month
	 * 		月份从1开始，1=1月， 12=12月
	 * @param date
	 * @return
	 * </pre>
	 */
	public static Date getDate(int year, int month, int date){
		Calendar cal = Calendar.getInstance();
		cal.set(year, month-1, date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}
	/**
	 * <pre>
	 * @param year
	 * @param month
	 * 		月份从1开始，1=1月， 12=12月
	 * @param date
	 * @param hour
	 * 		24小时
	 * @param min
	 * @param sec
	 * @return
	 * </pre>
	 */
	public static Date getTime(int year, int month, int date, int hour, int min, int sec){
		Calendar cal = Calendar.getInstance();
		cal.set(year, month-1, date, hour, min, sec);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	public static Integer getDatePart(Date date, String part){
		if(date==null || StringUtil.isBlank(part)){
			return null;
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		Integer ret = null;
		switch (part) {
		case DATE_PART_YEAR:
			ret = cal.get(Calendar.YEAR);
			break;
		case DATE_PART_MONTH:
			ret = cal.get(Calendar.MONTH)+1;
			break;
		case DATE_PART_DATE:
			ret = cal.get(Calendar.DATE);
			break;
		case DATE_PART_HOUR:
			ret = cal.get(Calendar.HOUR_OF_DAY);
			break;
		case DATE_PART_MIN:
			ret = cal.get(Calendar.MINUTE);
			break;
		case DATE_PART_SECOND:
			ret = cal.get(Calendar.SECOND);
			break;
		case DATE_PART_DAY_OF_WEEK:
			ret = cal.get(Calendar.DAY_OF_WEEK);
			ret = ret - 1;
			ret = ret.equals(0) ? 7 : ret;
			break;
		default:
			break;
		}
		return ret;
	}
	
	public static Date getNowTime(){
		return Calendar.getInstance().getTime();
	}
	
	public static String formatDate(Date date, String pattern){
		if(date==null){
			return null;
		}
		DateFormat df = new SimpleDateFormat(pattern);
		return df.format(date);
	}
	/**
	 * 根据某个日期，返回这个日期到现在所包含的年月份
	 * @param date
	 * @param pattern
	 * @return
	 */
	public static List<String> getHistoryYearMonth(Date date, String pattern){
		List<String> yearmonthList=new ArrayList<>();
		
		if(date==null){
			return yearmonthList;
		}
		Date currentDate=new Date();
		if(date.getTime()>currentDate.getTime()){
			return yearmonthList;
		}
		
		String currentYearMonth=formatDate(currentDate,pattern);
		
		String tempyearmonth=formatDate(date,pattern);
		while(!tempyearmonth.equals(currentYearMonth)){
			yearmonthList.add(tempyearmonth);
			
			date=dateAdd(DateUtil.DATE_PART_MONTH, date, 1);
			tempyearmonth=formatDate(date,pattern);
		}
		
		return yearmonthList;
	}
	
	public static String getNotNullDateStr(Date date, String pattern){
		if(date==null){
			return "";
		}
		DateFormat df = new SimpleDateFormat(pattern);
		return df.format(date);
	}
	
	public static Date parserDate(String dateStr){
		if(StringUtil.isBlank(dateStr)){
			return null;
		}
		Integer len = dateStr.length();
		if(len != PATTERN_SIMPLE_DATE.length() && len!=PATTERN_SIMPLE_DATETIME.length() && len!=PATTERN_SIMPLE_DATETIME_FULL.length()){
			throw new RuntimeException("source[" + dateStr
					+ "] is wrong date format, can't parse to Date! right format should like [" + PATTERN_SIMPLE_DATE
					+ "/" + PATTERN_SIMPLE_DATETIME + "/" + PATTERN_SIMPLE_DATETIME_FULL + "]");
		}
		String pattern = StringUtil.EMPTY;
		pattern = (len == PATTERN_SIMPLE_DATE.length() && StringUtil.isBlank(pattern)) ? PATTERN_SIMPLE_DATE : pattern;
		pattern = (len == PATTERN_SIMPLE_DATETIME.length() && StringUtil.isBlank(pattern)) ? PATTERN_SIMPLE_DATETIME : pattern;
		pattern = (len == PATTERN_SIMPLE_DATETIME_FULL.length() && StringUtil.isBlank(pattern)) ? PATTERN_SIMPLE_DATETIME_FULL : pattern;
		return parserDate(dateStr, pattern);
	}
	
	public static Date parserDate(String dateStr, String pattern){
		if(StringUtil.isAnyBlank(dateStr, pattern)){
			return null;
		}
		DateFormat df = new SimpleDateFormat(pattern);
		try {
			Date date = df.parse(dateStr);
			return date;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public static String formatDate(String dateStr, String fromPattern,String toPattern){
		Date date=parserDate(dateStr,fromPattern);
		if(date==null){
			return null;
		}
		return formatDate(date,toPattern);
	}
	
	/**
	 * 计算时间差 date1-date2
	 * @param date1
	 * @param date2
	 * @param datePart
	 * @return
	 */
	public static Integer dateDiff(Date date1, Date date2, String datePart){
		if(date1==null || date2==null){
			throw new RuntimeException("getInterval param error, arg date1 or date2 is null.");
		}
		if(datePart==null){
			throw new RuntimeException("getInterval param error, datePart is null.");
		}
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date1);
		cal1.set(Calendar.MILLISECOND, 0);
		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(date2);
		cal2.set(Calendar.MILLISECOND, 0);
		
		Integer factor = 1;
		switch (datePart) {
			case DATE_PART_YEAR:
				return cal1.get(Calendar.YEAR) - cal2.get(Calendar.YEAR);
			case DATE_PART_MONTH:
				Integer year1 = cal1.get(Calendar.YEAR);
				Integer year2 = cal2.get(Calendar.YEAR);
				Integer month1 = cal1.get(Calendar.MONTH);
				Integer month2 = cal2.get(Calendar.MONTH);
				if(year1.compareTo(year2)>0){
					return (year1-year2)*12+(month1 - month2);
				}else if(year1.compareTo(year2)==0){
					return month1 - month2;
				}else{
					return -dateDiff(date2, date1, DATE_PART_MONTH);
				}
			case DATE_PART_DATE:
				factor = factor * 24 * 60 * 60 * 1000;
				break;
			case DATE_PART_HOUR:
				factor = factor * 60 * 60 * 1000;
				break;
			case DATE_PART_MIN:
				factor = factor * 60 * 1000;
				break;
			case DATE_PART_SECOND:
				factor = factor * 1000;
				break;
			case DATE_PART_MILLISECOND:
				break;
			default:
				throw new RuntimeException("getInterval param error, datePart["+datePart+"] not supported.");
		}
		Long interval = (cal1.getTimeInMillis() - cal2.getTimeInMillis())/factor;
		return interval.intValue();
	}
	/**
	 * <pre>
	 * the value 0 if the argument t1 is equal to t2; 
	 * a value less than 0 if t1 is before the t2; 
	 * and a value greater than 0 if this t1 is after the t2.
	 * <i>
	 * =0 t1 equals t2
	 * <0 t1 before t2
	 * >0 t1 after t2
	 * </i>
	 * @param t1
	 * @param t2
	 * @return
	 * @Author: simple
	 * @CreateDate：2017年5月6日
	 * </pre>
	 */
	public static Integer compare(Date t1, Date t2){
		if(t1==null || t2==null){
			throw new RuntimeException("two compare date object neither can be null.");
		}
		return t1.compareTo(t2);
	}
	
	/**
	 * 获取第i天得日期
	 * @param date
	 * @param i
	 */
	public static Date getDateAfterIDay(Date date, int i) {
		return dateAdd(DATE_PART_DATE, date, i);
	}
	public static Date dateAdd(String part, String dateTimeStr, Integer value){
		Date date=DateUtil.parserDate(dateTimeStr);
		return dateAdd( part,  date,  value);
	}
	
	public static Date dateAdd(String part, Date date, Integer value){
		if(StringUtil.isBlank(part) || date == null || value == null){
			throw new RuntimeException("dateAdd param error, part:"+part+", date:"+date+", value:"+value);
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		Integer interval = null;
		switch (part) {
		case DATE_PART_YEAR:
			interval = Calendar.YEAR;
			break;
		case DATE_PART_MONTH:
			interval = Calendar.MONTH;
			break;
		case DATE_PART_DATE:
			interval = Calendar.DAY_OF_MONTH;
			break;
		case DATE_PART_HOUR:
			interval = Calendar.HOUR_OF_DAY;
			break;
		case DATE_PART_MIN:
			interval = Calendar.MINUTE;
			break;
		case DATE_PART_SECOND:
			interval = Calendar.SECOND;
			break;
		case DATE_PART_MILLISECOND:
			interval = Calendar.MILLISECOND;
			break;
		default:
			throw new RuntimeException("dateAdd param error, part:"+part);
		}
		cal.add(interval, value);
		return cal.getTime();
	}
	
	public static Date getDateBeginTime(Integer year, Integer month, Integer date){
		if(year==null || year<1 || month==null || month<1 || month>12 || date==null || date<1 || date>31){
			throw new RuntimeException("getDateBeginTime[year:"+year+", month:"+month+", date:"+date+"] error");
		}
		Calendar cal = Calendar.getInstance();
		cal.set(year, month-1, date, 0, 0, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}
	
	public static Date getDateBeginTime(Date date){
		if(date==null){
			throw new RuntimeException("getDateBeginTime[date:"+date+"] error");
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}
	
	public static Date getDateEndTime(Integer year, Integer month, Integer date){
		if(year==null || year<1 || month==null || month<1 || month>12 || date==null || date<1 || date>31){
			throw new RuntimeException("getDateEndTime[year:"+year+", month:"+month+", date:"+date+"] error");
		}
		Calendar cal = Calendar.getInstance();
		cal.set(year, month-1, date, 23, 59, 59);
		cal.set(Calendar.MILLISECOND, 999);
		return cal.getTime();
	}
	
	public static Date getDateEndTime(Date date){
		if(date==null){
			throw new RuntimeException("getDateEndTime[date:"+date+"] error");
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MILLISECOND, 999);
		return cal.getTime();
	}
	
	
	
	public static String getMonthBeginTimeStr(Date date){
		if(date==null ){
			return null;
		}
		
		return formatDate(date, "yyyy-MM")+"-01 00:00:00";
	}
	public static String getYearBeginTimeStr(Date date){
		if(date==null ){
			return null;
		}
		return formatDate(date, "yyyy")+"-01-01 00:00:00";
	}
	
	public static Date getMonthBeginTime(Integer year, Integer month){
		if(year==null || year<1 || month==null || month<1 || month>12){
			throw new RuntimeException("getFirstDate[year:"+year+", month:"+month+"] error");
		}
		Calendar cal = Calendar.getInstance();
		cal.set(year, month-1, 1, 0, 0, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}
	
	public static Date getMonthEndTime(Integer year, Integer month){
		Date date = getMonthBeginTime(year, month);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, 1);
		cal.add(Calendar.DATE, -1);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MILLISECOND, 999);
		return cal.getTime();
	}
	
	public static int getDaysOfMonth(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
	}	
	public static Date getMonthEndTime(Date date){
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.DATE, getDaysOfMonth(date));
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MILLISECOND, 999);
		return cal.getTime();
	}
	public static String getMonthEndTimeStr(Date date,String pattern){
		return formatDate(getMonthEndTime(date), pattern);
	}
	public static Date getMonthStartTime(Date date){
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.DATE, 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}
	public static String getMonthStartTimeStr(Date date,String format){
		return formatDate(getMonthStartTime(date),format);
	}
	
	public static Integer getDateField(Date date, Integer field){
		if(date==null)return null;
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.get(field);
	}
	public static Date getReportEndTime(Date date){
		if(date==null){
			throw new RuntimeException("getReportEndTime[date:"+date+"] error");
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		if(cal.get(Calendar.HOUR_OF_DAY)>=5){//取后一天
			cal.add(Calendar.DAY_OF_MONTH, 1);
		}
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
	
		cal.set(Calendar.HOUR_OF_DAY, 5);
		return cal.getTime();
	}
	public static Date getReportBeginTime(Date date){//获取当日5点时间
		if(date==null){
			throw new RuntimeException("getDateBeginTime[date:"+date+"] error");
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		if(cal.get(Calendar.HOUR_OF_DAY)<5){//取前一天的時間
			cal.add(Calendar.DAY_OF_MONTH, -1);
		}
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		cal.set(Calendar.HOUR_OF_DAY, 5);
		return cal.getTime();
	}
	
	public static String[] getStartTimeAndEndTime(String timeRange){
		String startTime=null;
		String endTime=null;
		
		if(timeRange!=null && !timeRange.isEmpty()){//2021-07-26 00:00:00 - 2021-08-26 00:59:59
			int startTimeIndex=timeRange.indexOf(":")+6;
			startTime=timeRange.substring(0,startTimeIndex).trim();
			timeRange=timeRange.substring(startTimeIndex+1).trim();
			endTime=timeRange.substring(timeRange.indexOf(" ")).trim();
		
			return new String[]{startTime,endTime};
		}
		return null;
	}
	
	public static String getDateStartTimeStr(Date date){
		return formatDate(date, PATTERN_SIMPLE_DATE+" 00:00:00");
	}
	public static Date getDateStartTime(Date date){
		String datestr=formatDate(date, PATTERN_SIMPLE_DATE+" 00:00:00");
		return parserDate(datestr);
	}
	public static String getDateEndTimeStr(Date date){
		return formatDate(date, PATTERN_SIMPLE_DATE+" 23:59:59");
	}
	/**
	 * 这个时间是否是最近N个月内的时间
	 * @param timestamp
	 * @param lately
	 * @return
	 */
	public static Boolean isLatelyMonthOfTime(Long timestamp,Integer lately){
		Date date=new Date(timestamp);
		date=DateUtil.dateAdd(DateUtil.DATE_PART_MONTH, date, lately);
		return date.after(new Date());
	}
	public static Date getCurrentMonthFirstDay(){//获取当日5点时间
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}
	public static void main(String[] args){
		System.out.println(formatDate(parserDate("20230215095512","yyyyMMddHHmmss"), "yyyy-MM-dd HH:mm:ss"));
	}
	
	public static void testGetHistoryYearMonth(){
		Date date=DateUtil.parserDate("2022-01-09 00:00:00");
		
		System.out.println(getHistoryYearMonth(date, "yyyyMM"));
		
	}
	
	public static void test1(){
		System.out.println(getMonthStartTime(new Date()));
		
		/*Date date=new Date();
		String str=getMonthBeginTimeStr(date);
		System.out.println(str);
		
		str=getYearBeginTimeStr(new Date());
		System.out.println(str);
		System.out.println(str);
		
		//System.out.println(getWeek("2022-08-16 00:00:00"));
		
		//System.out.println(getSpecifiedDayBefore("2022-08-16 00:00:00",5));
		
		System.out.println(getWeekStartDate("2022-08-21"));;
		System.out.println(getWeekEndDate("2022-08-21"));;
		*/
	}
	
	public static String GetMonth(String month){
		switch(month){
		
			case "一月":return "01";
			case "二月":return "02";
			case "三月":return "03";
			case "四月":return "04";
			case "五月":return "05";
			case "六月":return "06";
			case "七月":return "07";
			case "八月":return "08";
			case "九月":return "09";
			case "十月":return "10";
			case "十一月":return "11";
			case "二十二月":return "12";
		}
		return "";
	}
	
	public static String getWeekStartDate(String date){
		Integer weekIndex=getWeek(date);
		Integer days=weekIndex-1;
		if(days==0){
			return date;
		}
		return formatDate(dateAdd(DateUtil.DATE_PART_DATE, date, -1*days), DateUtil.PATTERN_SIMPLE_DATE);
	}
	public static String getWeekEndDate(String date){
		Integer weekIndex=getWeek(date);
		Integer days=weekIndex-7;
		if(days==0){
			return date;
		}
		return formatDate(dateAdd(DateUtil.DATE_PART_DATE, date, -1*days), DateUtil.PATTERN_SIMPLE_DATE);
	}
	
	public static String getDate(String dateTime){
		return formatDate(parserDate(dateTime), DateUtil.PATTERN_SIMPLE_DATE);
	}
	
	public static String getDate(Date date){
		return formatDate(date, DateUtil.PATTERN_SIMPLE_DATE);
	}
	public static String getDateTimeStr(Date date){
		return formatDate(date, DateUtil.PATTERN_SIMPLE_DATETIME);
	}
	
	public static Date getDateStartTimeByDateStr(String dateStr){
		return parserDate(dateStr+" 00:00:00",DateUtil.PATTERN_SIMPLE_DATETIME);
	}
	public static Date getDateEndTimeByDateStr(String dateStr){
		return parserDate(dateStr+" 23:59:59",DateUtil.PATTERN_SIMPLE_DATETIME);
	}
	public static Date getDateByDateTimeStr(String dateTime){
		return parserDate(dateTime,DateUtil.PATTERN_SIMPLE_DATETIME);
	}
	public static String getDateStrByDateTimeStr(String dateTime){
		return dateTime.trim().split(" ")[0];
	}
	
	public static int getWeek(String daystring){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		int week_index = 0 ;
		try {
			Date date = formatter.parse(daystring);
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			week_index = cal.get(Calendar.DAY_OF_WEEK) - 1;
			if(week_index<0){
				week_index = 0;
			} 
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return (week_index==0?7:week_index);
	}
	
	public static String getWechatFormatTime(Date date){
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH)+1;
		int day = cal.get(Calendar.DATE);
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int minute = cal.get(Calendar.MINUTE);
		int second = cal.get(Calendar.SECOND);
		
		
		StringBuilder sb=new StringBuilder();
		sb.append(year+"年");
		sb.append(month+"月");
		sb.append(day+"日 ");
		if(hour<10){
			sb.append("0");
		}
		sb.append(hour+":");
		if(minute<10){
			sb.append("0");
		}
		sb.append(minute+":");
		if(second<10){
			sb.append("0");
		}
		sb.append(second);
		
		return sb.toString();
	}
	
	
}
