package com.lujie.common.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import com.lujie.common.entity.EntityMap;

/**
 * 
 * @author sl
 *
 */

@Repository
public interface BaseMapper <T>  {

	public List<T> findAll();
	
	/**
	 * 根据对象属性查找对象
	 * @param item
	 * @return
	 */
	public T findByItem(@Param("item") T  item);
	/**
	 * 根据对象属性查找对象集合
	 * @param item
	 * @return
	 */
	public List<T> findListByItem(@Param("item") T  item);
	
	/**
	 * 根据多个id批量查询记录
	 * @param ids
	 * @return
	 */
	public List<T> findByIds(@Param("ids")String ids);
	public T findById(@Param("id")String id);
	List<T> findByIdList(@Param("idList") List<Long> idList);
	
	
	/**
	 * 批量保存记录
	 * @param checkInList
	 */
	public void batchSave(@Param("list")List<T> list);
	public void batchSaveVarTableData(@Param("tableName")String tableName,@Param("list")List list);
	public void saveVarTableData(@Param("tableName")String tableName,@Param("p")Object obj);
	
	public void save(@Param("p") T p);
	/**
	 * 批量更新数据
	 * @param checkInList
	 */
	public void batchUpdate(@Param("list")List<T> list);
	
	public Integer update(@Param("p") T  p);
	
	
	/**
	 * 查找返回分页信息
	 * pageIndex,pageSize
	 * @param paramMap
	 * @return
	 */
	public List<T> findPageList(@Param("p")HashMap<String,Object> paramMap);
	
	
	/**
	 * 根据对象属性，查找对象列表
	 * @param property
	 * @return
	 */
	public List<T> findPageListByProperty(@Param("p") T property);
	
	
	/**
	 * 根据条件，统计记录调试
	 * @param paramMap
	 * @return
	 */
	public Long countTotal(@Param("param")HashMap<String,Object> paramMap);
	
	/**
	 * 根据对象属性，计算对象总数
	 * @param property
	 * @return
	 */
	public Long countTotalByProperty(@Param("p") T property);
	
	public Integer deleteByIds(@Param("ids")String ids);
	public Integer delete(@Param("p")Object obj);
	public Integer batchDelete(@Param("list")List list);
	
	
	/**
	 * 根据用户id和数据类型，找到拥有权限的数据
	 * @param userid
	 * @param datatype
	 * @return
	 */
	public List<T> findAllDataItem(
			@Param("userid")String userid,
			@Param("datatype")String datatype);
	
	/**
	 * 根据用户id和数据类型或者名字，找到拥有权限的数据
	 * @param userid
	 * @param datatype
	 * @param name
	 * @return
	 */
	public List<T> findAllDataItemByName(
			@Param("userid")String userid,
			@Param("datatype")String datatype,
			@Param("name")String name);
	
	/**
	 * 根据用户id和数据类型或者ids，找到拥有权限的数据
	 * @param userid
	 * @param datatype
	 * @param ids
	 * @return
	 */
	public List<T> findAllDataItemByIds(
			@Param("userid")String userid,
			@Param("datatype")String datatype,
			@Param("name")String ids);
	/**
	 * 根据对象属性，查找所有记录，并且如果对象属性都为空，则返回所有记录
	 * @param property
	 * @return
	 */
	public List<T> findListByProperty(@Param("p") T property);
	/**
	 * 获取最大id,根据条件
	 * @param 
	 * @return
	 */
	public Integer findMaxIdByProperty(@Param("p") T property);
	/**
	 * 根据条件删除
	 * @param property
	 */
	public void deleteByProperty(@Param("p") T property);
	/**
	 * 根据属性查找一条记录
	 * @param property
	 * @return
	 */
	public T findOneByProperty(@Param("p") T property);
	
	public T sumByProperty(@Param("p") T property);
	

	public List findPageList(@Param("p")Object object);
	public Object findOne(@Param("p")Object object);
	public Object findEarliestOne(@Param("p")Object object);
	public EntityMap findPreOne(@Param("p")Object  object);
	public Long countTotal(@Param("p")Object object);
	public List findList(@Param("p")Object object);
	public HashMap countTotalMap(@Param("p")Object object);
	public Object findMaxOne(@Param("p")Object object);
	public Object sum(@Param("p")Object object);//汇总
	public List sumList(@Param("p")Object object);//汇总
	public Long sumListCount(@Param("p")Object object);//汇总
	public List<EntityMap> export(@Param("p")EntityMap param);
    
	public int existTable(@Param("p")String tableName);
	@Select("select count(*)  from information_schema.TABLES where  LCASE(table_name)=#{tableName} ")
    public Integer existTableCount(@Param("tableName")String tableName);
	
	
	@Select("select count(1)  from ${tableName} ")
    public Integer getTableRowCount(@Param("tableName")String tableName);
	
	
	@Update("drop table if exists  ${tableName} ")
	public void dropTable(@Param("tableName")String tableName);
	
	/**
	 * 获取表所有记录
	 * @param tableName
	 * @return
	 */
	public List<HashMap<String,Object>> findTableData(@Param("p")String tableName);
	/**
	 * 返回表的所有列名
	 * @param tableName
	 * @return
	 */
	public List<String> findTableColumnNames(@Param("p")String tableName);
	
	/**
     * 创建新表
     * @param tableName
     * @return
     */
	public int createNewTable(@Param("p")String tableName);
    /**
     * 向指定表中添加数据
     * @param tableName
     * @param deviveDatas
     * @return
     */
	public int batchSaveByTable(@Param("p")String tableName,
			@Param("list")List datas);
	public List<EntityMap> getColumnInfo(@Param("p")Object p);
	
	public List<EntityMap> getIndexColumn(@Param("p")Object p);
	public EntityMap getTableInfo(@Param("p")Object p);
	
	public List<EntityMap> findEntityMapPageList(@Param("p")Object p);
	public Integer countTotalEntityMap(@Param("p")Object p);
	public EntityMap findOneEntityMap(@Param("p")Object p);
	public List<EntityMap> findAllEntityMap(@Param("p")Object p);
	public EntityMap sumEntityMap(@Param("p")Object p);
	
	public void saveEntityMap(@Param("p") EntityMap p);
	public Integer updateEntityMap(@Param("p")Object p);
	public Integer deleteEntityMap(@Param("p")Object p);

	
	@Update(" ${sql} ")
	public void executeSql(@Param("sql") String sql);
	
	@Select(" ${sql} ")
	public HashMap findOneBySql(@Param("sql") String sql);
	
	@Select(" ${sql} ")
	public List<EntityMap> findListBySql(@Param("sql") String sql);
	
	@Select("select * from information_schema.columns where table_schema = #{dbName} and TABLE_NAME=#{tableName} order by ORDINAL_POSITION asc")
	public List<EntityMap> getDBTableColumnInfo(@Param("dbName")String dbName,@Param("tableName")String tableName);
	
	@Select("select * from information_schema.columns where TABLE_NAME=#{tableName} order by ORDINAL_POSITION asc")
	public List<EntityMap> getTableColumnInfo(@Param("tableName")String tableName);

}
