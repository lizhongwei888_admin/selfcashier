package com.lujie.common.util;

import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

public class StringUtils {

	/** 判断是否为数字的正则表达式 */
	private final static String IS_NUMBER = "^-?([1-9]\\d*\\.\\d+|0\\.\\d*[1-9]\\d*|0\\.0+|0|\\d+)$";
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//
	public static String toUpCase(String str) {
		if (str != null) {
			str = str.toUpperCase();
		} else {
			str = "";
		}
		return str;
	}
	
	/*按特定的编码格式获取长度*/  
    public static int getWordCountCode(String str, String code) throws UnsupportedEncodingException{  
        if(code.equals("")){
        	return str.getBytes("GBK").length;
        }else{
        	return str.getBytes(code).length;
        }
    }
	public static Boolean isNumeric(String str){
		try{
			Integer.parseInt(str);
			return true;
		}catch(Exception ex){
			
		}
		return false;
	}

	public static String ObjectToString(Object o) {
		if (null != o) {
			return String.valueOf(o);
		}
		return "";
	}

	public static int ObjectToInt(Object o) {
		if (null != o) {
			return Integer.parseInt(String.valueOf(o));
		}
		return 999;
	}

	public static String toLowCase(String str) {
		if (str != null) {
			str = str.toLowerCase();
		} else {
			str = "";
		}
		return str;
	}

	/**
	 * 将一个字符串进行指定的字符集转换
	 * 
	 * @param st
	 *          要转换字符集的字符串
	 * @param original
	 *          字符串原始的字符集,null表示不指定原有字符集
	 * @param toCharset
	 *          要转换的字符集
	 * @return 转换失败返回NULL
	 */
	public static String convertCharSet(String st, String original,
			String toCharset) {
		if (st == null || toCharset == null)
			return "";

		String strResult;
		try {
			if (original == null) {
				strResult = new String(st.toString().getBytes(), toCharset);
			} else {
				strResult = new String(st.toString().getBytes(original), toCharset);
			}
		} catch (Exception e) {
			return null;
		}

		return strResult;
	}

	/**
	 * 从ISO8859_1转换到GBK
	 * 
	 * @param str
	 *          需要转换字符集的字符串
	 * @return 转换后的字符串
	 */
	public static String iso2gbk(String str) {
		String value = "";
		if (str == null || str.length() == 0) {
			return "";
		}

		try {
			value = new String(str.getBytes("ISO8859_1"), "GBK");
		} catch (Exception e) {
			return null;
		}
		return value;
	}

	/**
	 * 从GBK转换到ISO8859_1
	 * 
	 * @param str
	 *          需要转换字符集的字符串
	 * @return 转换后的字符串
	 */
	public static String gbk2iso(String str) {
		String value = "";
		if (str == null || str.length() == 0) {
			return "";
		}
		try {
			value = new String(str.getBytes("GBK"), "ISO8859_1");
		} catch (Exception e) {
			return null;
		}
		return value;
	}

	/**
	 * 处理为NULL的字符串，返回""
	 * 
	 * @param str
	 *          字符串
	 * @return 字符串
	 */
	public static String nullStr(String str) {
		if (str == null || str.length() == 0) {
			return "";
		} else {
			return str;
		}
	}

	/**
	 * 将字符串转换成16进制的字节数组
	 * 
	 * @param inHex
	 *          字符串
	 * @return 字节数组
	 */
	public static byte[] fromString(String inHex) {
		int len = inHex.length();
		int pos = 0;
		byte buffer[] = new byte[((len + 1) / 2)];
		if ((len % 2) == 1) {
			buffer[0] = (byte) asciiToHex(inHex.charAt(0));
			pos = 1;
			len--;
		}

		for (int ptr = pos; len > 0; len -= 2)
			buffer[pos++] = (byte) ((asciiToHex(inHex.charAt(ptr++)) << 4) | (asciiToHex(inHex
					.charAt(ptr++))));
		return buffer;
	}

	/**
	 * 将16进制的字节数组转换成字符串
	 * 
	 * @param buffer
	 *          字节数组
	 * @return 字符串
	 */
	public static final String toString(byte buffer[]) {
		StringBuffer returnBuffer = new StringBuffer();
		for (int pos = 0, len = buffer.length; pos < len; pos++)
			returnBuffer.append(hexToAscii((buffer[pos] >>> 4) & 0x0F)).append(
					hexToAscii(buffer[pos] & 0x0F));
		return returnBuffer.toString();
	}

	/**
	 * 从字符串中得到整数型的值
	 * 
	 * @param str
	 *          字符串
	 * @param defaultValue
	 *          缺省值
	 * @return 整数值
	 */
	public static int getInt(String str, int defaultValue) {
		if (str == null) {
			return defaultValue;
		}

		int intValue = defaultValue;
		try {
			intValue = Integer.parseInt(str);
		} catch (Exception e) {
			intValue = defaultValue;
		}
		return intValue;
	}

	/**
	 * 从字符串中得到Long型的值
	 * 
	 * @param str
	 *          字符串
	 * @param defaultValue
	 *          缺省值
	 * @return 长整数
	 */
	public static long getLong(String str, int defaultValue) {
		if (str == null) {
			return defaultValue;
		}

		long longValue = defaultValue;
		try {
			longValue = Long.parseLong(str);
		} catch (Exception e) {
			longValue = defaultValue;
		}
		return longValue;
	}

	private static final int asciiToHex(char c) {
		if ((c >= 'a') && (c <= 'f'))
			return (c - 'a' + 10);
		if ((c >= 'A') && (c <= 'F'))
			return (c - 'A' + 10);
		if ((c >= '0') && (c <= '9'))
			return (c - '0');
		throw new Error("ascii to hex failed");
	}

	private static char hexToAscii(int h) {
		if ((h >= 10) && (h <= 15))
			return (char) ('A' + (h - 10));
		if ((h >= 0) && (h <= 9))
			return (char) ('0' + h);
		throw new Error("hex to ascii failed");
	}

	/**
	 * 将字符串用分隔字符拆成字符串数组
	 * 
	 * @param str
	 *          原字符串
	 * @param delim
	 *          分隔字符集
	 * @return 字符串数组
	 */
	public static String[] split(String str, String delim) {
		StringTokenizer strtok = new StringTokenizer(str, delim);
		String[] result = new String[strtok.countTokens()];
		for (int i = 0; i < result.length; i++) {
			result[i] = strtok.nextToken();
		}
		return result;
	}

	/**
	 * Utf8URL解码
	 * 
	 * @param text
	 * @return
	 */
	public static String Utf8URLdecode(String text) {
		String result = "";
		int p = 0;

		if (text != null && text.length() > 0) {
			text = text.toLowerCase();
			p = text.indexOf("%e");
			if (p == -1)
				return text;

			while (p != -1) {
				result += text.substring(0, p);
				text = text.substring(p, text.length());
				if (text == "" || text.length() < 9)
					return result;

				result += CodeToWord(text.substring(0, 9));
				text = text.substring(9, text.length());
				p = text.indexOf("%e");
			}

		}
		return result + text;
	}

	/**
	 * utf8URL编码转字符
	 * 
	 * @param text
	 * @return
	 */
	private static String CodeToWord(String text) {
		String result;

		if (Utf8codeCheck(text)) {
			byte[] code = new byte[3];
			code[0] = (byte) (Integer.parseInt(text.substring(1, 3), 16) - 256);
			code[1] = (byte) (Integer.parseInt(text.substring(4, 6), 16) - 256);
			code[2] = (byte) (Integer.parseInt(text.substring(7, 9), 16) - 256);
			try {
				result = new String(code, "UTF-8");
			} catch (UnsupportedEncodingException ex) {
				result = null;
			}
		} else {
			result = text;
		}

		return result;
	}

	/**
	 * 编码是否有效
	 * 
	 * @param text
	 * @return
	 */
	private static boolean Utf8codeCheck(String text) {
		String sign = "";
		if (text.startsWith("%e"))
			for (int i = 0, p = 0; p != -1; i++) {
				p = text.indexOf("%", p);
				if (p != -1)
					p++;
				sign += p;
			}
		return sign.equals("147-1");
	}

	/**
	 * 是否Utf8Url编码
	 * 
	 * @param text
	 * @return
	 */
	public static boolean isUtf8Url(String text) {
		text = text.toLowerCase();
		int p = text.indexOf("%");
		if (p != -1 && text.length() - p > 9) {
			text = text.substring(p, p + 9);
		}
		return Utf8codeCheck(text);
	}

	public static String replaceString(String str, String key, String toKey) {

		if (key == null || key.equals("")) {
			return str;
		} else {
			StringBuffer sbuf = new StringBuffer();
			int keyLen = key.length();
			int intPos = -1;
			int pos = 0;

			while ((intPos = str.indexOf(key, pos)) != -1) {
				sbuf.append(str.substring(pos, intPos));
				sbuf.append(toKey);
				pos = intPos + keyLen;
			}
			sbuf.append(str.substring(pos));
			return sbuf.toString();
		}
	}

	/**
	 * 将内容转换成html可显示的内容
	 * 
	 * @param strOriginal
	 *          需转换字符串
	 * @return 转换后的字符串
	 */
	public static String replaceExclusivestr(String strOriginal) {
		StringBuffer sbuf = new StringBuffer();

		StringTokenizer st = new StringTokenizer(strOriginal, "\n");
		if (st.hasMoreTokens()) {
			String str = st.nextToken();
			sbuf.append(str.trim()).append("\n");
		}

		while (st.hasMoreTokens()) {
			String str = st.nextToken();
			sbuf.append("<BR>").append(str.trim()).append("\n");
		}

		return sbuf.toString();
	}

	/**
	 * 将显示内容的回车转换成<br>
	 * 
	 * @param strOriginal
	 *          需转换字符串
	 * @return 转换后的字符串
	 */
	public static String replaceReturn(String strOriginal) {
		StringBuffer sbuf = new StringBuffer();

		StringTokenizer st = new StringTokenizer(strOriginal, "\n");
		if (st.hasMoreTokens()) {
			String str = st.nextToken();
			sbuf.append(str.trim()).append("\n");
		}

		while (st.hasMoreTokens()) {
			String str = st.nextToken();
			sbuf.append("<BR>").append(str.trim()).append("\n");
		}

		return sbuf.toString();
	}

	/**
	 * 检查所给参数是否有字符(不包括空格回车等).
	 * 
	 * @return 如有字符返回<code>true</code>, 否则返回<code>false</code>.
	 */
	public static boolean hasText(String value) {
		if (value != null && !"".equals(value.trim())) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 判断是否为数字. 包括正负, 整数, 浮点.
	 * 
	 * @param value
	 *          要被验证的字符串.
	 * @return 如果为数字符返回<code>true</code>, 否则返回<code>false</code>.
	 */
	public static boolean isNumber(String value) {
		if (hasText(value)) {
			return value.matches(IS_NUMBER);
		} else {
			return false;
		}
	}

	/**
	 * 判断是否为正整数数值.
	 * 
	 * @param value
	 *          要被验证的字符串.
	 * @return 如果为数字符返回<code>true</code>, 否则返回<code>false</code>.
	 */
	public static boolean isIntNumber(String value) {
		// ^[1-9]+\\d*$
		if (hasText(value)) {
			Pattern p = Pattern.compile("^[1-9]+[0-9]*$");
			Matcher m = p.matcher(value);
			if (m.find()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 处理文件名称特殊字符由半角转换成全角
	 * 
	 * @param String
	 *          要处理的字符串
	 * @return 处理后的字符串
	 */
	public static String charHalfReplaceFull(String fileName) {

		String tempName = fileName;
		if ("".equals(tempName) || tempName == null) {
			tempName = "";
		} else {
			if (tempName.indexOf("\\") != -1) {
				tempName = tempName.replace("\\", "＼");
			}
			if (tempName.indexOf("/") != -1) {
				tempName = tempName.replace("/", "／");
			}
			if (tempName.indexOf(":") != -1) {
				tempName = tempName.replace(":", "：");
			}
			if (tempName.indexOf("*") != -1) {
				tempName = tempName.replace("*", "＊");
			}
			if (tempName.indexOf("?") != -1) {
				tempName = tempName.replace("?", "？");
			}
			if (tempName.indexOf("!") != -1) {
				tempName = tempName.replace("!", "！");
			}
			if (tempName.indexOf("@") != -1) {
				tempName = tempName.replace("@", "＠");
			}
			if (tempName.indexOf("%") != -1) {
				tempName = tempName.replace("%", "％");
			}
			if (tempName.indexOf("&") != -1) {
				tempName = tempName.replace("&", "＆");
			}
			if (tempName.indexOf("(") != -1) {
				tempName = tempName.replace("(", "（");
			}
			if (tempName.indexOf(")") != -1) {
				tempName = tempName.replace(")", "）");
			}
			if (tempName.indexOf("\"") != -1) {
				tempName = tempName.replace("\"", "＂");
			}
			if (tempName.indexOf("<") != -1) {
				tempName = tempName.replace("<", "＜");
			}
			if (tempName.indexOf(">") != -1) {
				tempName = tempName.replace(">", "＞");
			}
			if (tempName.indexOf("|") != -1) {
				tempName = tempName.replace("|", "｜");
			}
			if (tempName.indexOf("\n") != -1) {
				tempName = tempName.replace("\n", "\\n");
			}
			if (tempName.indexOf("\r") != -1) {
				tempName = tempName.replace("\r", "\\r");
			}
			if (tempName.indexOf("~") != -1) {
				tempName = tempName.replace("~", "～");
			}
			if (tempName.indexOf(";") != -1) {
				tempName = tempName.replace(";", "；");
			}
			if (tempName.indexOf("[") != -1) {
				tempName = tempName.replace("[", "【");
			}
			if (tempName.indexOf("]") != -1) {
				tempName = tempName.replace("]", "】");
			}
			if (tempName.indexOf("{") != -1) {
				tempName = tempName.replace("{", "｛");
			}
			if (tempName.indexOf("}") != -1) {
				tempName = tempName.replace("}", "｝");
			}
			if (tempName.indexOf("\'") != -1) {
				tempName = tempName.replace("\'", "’");
			}
			if (tempName.indexOf("`") != -1) {
				tempName = tempName.replace("`", "·");
			}

			if (tempName.indexOf("+") != -1) {
				tempName = tempName.replace("+", "＋");
			}

		}
		return tempName;
	}

	/**
	 * 处理文件名称特殊字符由半角转换成全角
	 * 
	 * @param String
	 *          要处理的字符串
	 * @return 处理后的字符串
	 */
	public static String charHalfReplaceISO(String fileName) {

		String tempName = fileName;
		if ("".equals(tempName) || tempName == null) {
			tempName = "";
		} else {

			if (tempName.indexOf("\r\n") != -1) {
				tempName = tempName.replaceAll("\r\n", "");
			}
			if (tempName.indexOf("\r") != -1) {
				tempName = tempName.replaceAll("\r", "");
			}
			if (tempName.indexOf("\n") != -1) {
				tempName = tempName.replaceAll("\n", "");
			}
			if (tempName.indexOf("\t") != -1) {
				tempName = tempName.replaceAll("\t", "");
			}
			if (tempName.indexOf("'") != -1) {
				tempName = tempName.replaceAll("'", "’");
			}
			if (tempName.indexOf("\"") != -1) {
				tempName = tempName.replaceAll("\"", "＂");
			}
			if (tempName.indexOf("%") != -1) {
				tempName = tempName.replaceAll("%", "％");
			}
			if (tempName.indexOf("<") != -1) {
				tempName = tempName.replaceAll("<", "＜");
			}
			if (tempName.indexOf(">") != -1) {
				tempName = tempName.replaceAll(">", "＞");
			}
		}
		return tempName;
	}

	/**
	 * 分割字符并且去结果除值为 null 或"" 的项
	 * 
	 * @param str
	 *          需要分割的字符串
	 * @param regx
	 *          分隔符
	 * @return 分割后的数组 (不包含为""或null的数组项)
	 */
	public static String[] splitAndEliminateEmptyItem(String str, String regx) {
		String[] result = new String[] {};
		if (null != str && !"".equals(str)) {
			String[] splited = str.split(regx);
			List<String> strList = new ArrayList<String>();
			for (int i = 0; i < splited.length; i++) {
				if (null != splited[i] && !"".equals(splited[i])) {
					strList.add(splited[i]);
				}
				result = new String[strList.size()];
				strList.toArray(result);
			}
		}
		return result;
	}

	/**
	 * 分割字符并且去结果除值为 null 或"" 的项
	 * 
	 * @param str
	 *          需要分割的字符串
	 * @param regx
	 *          分隔符
	 * @return 分割后的数组 (不包含为""或null的数组项)
	 */
	public static List<String> splitToListAndEliminateEmptyItem(String str,
			String regx) {
		List<String> strList = new ArrayList<String>();
		if (null != str && !"".equals(str)) {
			String[] splited = str.split(regx);
			for (int i = 0; i < splited.length; i++) {
				if (null != splited[i] && !"".equals(splited[i])) {
					strList.add(splited[i]);
				}

			}
		}
		return strList;
	}

	/**
	 * 判断是否为空字符串
	 * 
	 * @param str
	 *          需要判断的字符串
	 * @return 返回判断的结果如果为空字符串则返回 true,反之返回false
	 */
	public static boolean isEmptyStr(String str) {
		if (null != str && !"".equals(str.trim())) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * 判断是否为整数
	 * 
	 * @param str
	 *          需要判断的字符串
	 * @return 返回判断的结果如果为整数串则返回 true,反之返回false
	 */
	public static boolean isInteger(String str) {
		Pattern pattern = Pattern.compile("^-{0,1}[0-9]*$");
		Matcher isInteger = pattern.matcher(str);
		if (!isInteger.matches()) {
			return false;
		}
		return true;
	}

	/**
	 * 根据对传入的对象返回他的字符串形式
	 * 
	 * @param obj
	 *          要转换的对象
	 * @return 此对象的字符形式
	 */
	public static String nullToEmptyStr(Object obj) {
		if (null != obj && obj instanceof String) {
			return (String) obj;
		}
		if (null != obj) {
			return obj.toString();
		}
		return "";

	}

	/**
	 * 判断数组中是否包含输入的字符
	 * 
	 * @param strArray
	 *          字符串数组
	 * @param str
	 *          字符串
	 * @return 代表是否包含的boolean
	 */
	public static boolean isContains(String[] strArray, String str) {
		if (null == strArray || 0 == strArray.length) {
			throw new IllegalArgumentException("错误的参数!");
		}
		List<String> strList = Arrays.asList(strArray);
		if (strList.contains(str)) {
			return true;
		}
		return false;
	}

	/**
	 * 生成唯一主键ID
	 * 
	 * @return
	 */
	public static String getUUID() {
		String s = UUID.randomUUID().toString();
		return s.substring(0, 8) + s.substring(9, 13) + s.substring(14, 18)
				+ s.substring(19, 23) + s.substring(24);
	}

	/**
	 * 获取系统时间HH:mm:ss
	 * 
	 * @return
	 */
	public static String getCurrentTime() {
		SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
		Calendar current = Calendar.getInstance(Locale.CHINA);
		current.setTimeInMillis(new Date().getTime());

		return formatter.format(current.getTime());
	}

	/**
	 * 获取系统时间yyyy-MM-dd
	 * 
	 * @return
	 */
	public static String getCurrentDay() {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Calendar current = Calendar.getInstance(Locale.CHINA);
		current.setTimeInMillis(new Date().getTime());

		return formatter.format(current.getTime());
	}

	/**
	 * getCurrentDay:(获取系统时间yyyy-MM-dd). <br/>
	 * TODO(这里描述这个方法适用条件 – 可选).<br/>
	 * 
	 * @author Dev-shilei
	 * @param month 为负数表示前一个月
	 * @param day   为负数表示前一天,反之...
	 * @return
	 * @since JDK 1.6
	 */
	public static String getCurrentDay(int month, int day) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Calendar current = Calendar.getInstance(Locale.CHINA);
		current.setTimeInMillis(new Date().getTime());
		current.add(Calendar.DATE, day); // 得到前一天
		current.add(Calendar.MONTH, month); // 得到前一个月
		return formatter.format(current.getTime());
	}

	/**
	 * 获取系统时间yyyy-MM-dd HH:mm:ss
	 * 
	 * @return
	 */
	public static String getCurrentDate() {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar current = Calendar.getInstance(Locale.CHINA);
		current.setTimeInMillis(new Date().getTime());

		return formatter.format(current.getTime());
	}

	public static String getUpDate(int y,int m,int d,int h,int mm,int s){
		Calendar c = Calendar.getInstance();//可以对每个时间域单独修改
		int year = c.get(Calendar.YEAR)+y;
		int month = c.get(Calendar.MONTH)+m;
		int date = c.get(Calendar.DATE)+d;
		int hourOfDay = c.get(Calendar.HOUR_OF_DAY)+h;
		int minute = c.get(Calendar.MINUTE)+mm;
		int second = c.get(Calendar.SECOND)+s;

		c.set(year, month, date, hourOfDay, minute, second);
		return dateFormat.format(c.getTime()); 
	}
	/**
	 * 获取系统时间yyyy-MM-dd HH:mm:ss eg:new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
	 * 
	 * @return
	 */
	public static String getCurrentDate(SimpleDateFormat formatter) {
		Calendar current = Calendar.getInstance(Locale.CHINA);
		current.setTimeInMillis(new Date().getTime());

		return formatter.format(current.getTime());
	}

	/**
	 * 获取系统时间yyyy-MM-dd HH:mm:ss eg:new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
	 * 
	 * @return
	 */
	public static String setCurrentDate(String format, long times) {
		Calendar current = Calendar.getInstance(Locale.CHINA);
		current.setTimeInMillis(times);
		if(null == format || format.length()<=0)
			 format = "yyyy-MM-dd HH:mm:ss";
		return new SimpleDateFormat(format).format(current.getTime());
	}
	
	public static String parseMsgDateFormat(String date) {
		// /SimpleDateFormat formatter = new
		// SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		// 140813141516
		if (date.length() == 12) {
			date = new StringBuilder()
					.append(Calendar.getInstance().get(Calendar.YEAR)).append("-")
					.append(date.substring(2, 4)).append("-")
					.append(date.substring(4, 6)).append(" ")
					.append(date.substring(6, 8)).append(":")
					.append(date.substring(8, 10)).append(":")
					.append(date.substring(10, 12)).toString();
		} else if (date.length() == 14) {
			date = new StringBuilder(date.substring(0, 4)).append("-")
					.append(date.substring(4, 6)).append("-")
					.append(date.substring(6, 8)).append(" ")
					.append(date.substring(8, 10)).append(":")
					.append(date.substring(10, 12)).append(":")
					.append(date.substring(12, 14)).toString();
		}else if(date.length() == 8){
			date = new StringBuilder(date.substring(0, 4)).append("-")
			.append(date.substring(4, 6)).append("-")
			.append(date.substring(6, 8)).toString();
		}
		return date;
	}

	public static Date stringToDate(String dateStr, String formatStr) {
		if (dateStr == null || dateStr.length() == 0 )
			return null;
		if(formatStr == null || formatStr.trim().length() <= 0)
			formatStr = "yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat dd = new SimpleDateFormat(formatStr);
		Date date = null;
		try {
			if (dateStr.length() <= 10) {
				dateStr += " 00:00:00";
			}
			date = dd.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	public static Date stringToDay(String dateStr, String formatStr) {
		if (dateStr == null || dateStr.length() == 0 )
			return null;
		if(formatStr == null || formatStr.trim().length() <= 0)
			formatStr = "yyyy-MM-dd";
		SimpleDateFormat dd = new SimpleDateFormat(formatStr);
		Date date = null;
		try {
			date = dd.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}
	
	public static Date dateFormatDate(Date date, String formatStr) {
		if (date == null || formatStr == null)
			return null;
		SimpleDateFormat dd = new SimpleDateFormat(formatStr);
		try {
			String dateStr = dd.format(date);
			if (dateStr.length() <= 10) {
				dateStr += "00:00:00";
			}
			date = dd.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	public static String formatDate(Date date, String formatStr) {
		if (date == null)
			return null;
		if (formatStr == null || formatStr.trim().length() <= 0) {
			formatStr = "yyyy-MM-dd HH:mm:ss";
		}
		SimpleDateFormat dd = new SimpleDateFormat(formatStr);
		try {
			return dd.format(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 获取系统当前IP
	 */
	public static String getCurrentHostIp() {
		try {
			return String.valueOf(InetAddress.getLocalHost());
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * 获取客户端IP
	 * 
	 * @param request
	 * @return
	 */
	public static String getIpAddr(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		if(ip == null) ip="";
		
		return ip;
	}
	/**
	 * 将多个逗号分隔的字符串转换成用单引号拼接并用逗号分割的字符串，用于sql语句
	 * @param words
	 * @return
	 */
	public static String wordsToSqlWords(String words){
		String[] wordsArr=words.split(",");
		StringBuffer wordsSB=new StringBuffer();
		for(String word:wordsArr){
			wordsSB.append("'"+word+"',");
		}
		wordsSB.delete(wordsSB.length()-1, wordsSB.length());
		return wordsSB.toString();
	}
	public static String wordsToSqlWords(List<String> wordList){
		StringBuffer wordsSB=new StringBuffer();
		for(String word:wordList){
			wordsSB.append("'"+word+"',");
		}
		wordsSB.delete(wordsSB.length()-1, wordsSB.length());
		return wordsSB.toString();
	}

	public static void main(String[] args) throws ParseException {
		System.out.println(wordsToSqlWords("abc,bcd"));

	}
	
}
