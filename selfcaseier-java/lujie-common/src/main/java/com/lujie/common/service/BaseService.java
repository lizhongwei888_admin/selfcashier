package com.lujie.common.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.poi.ss.formula.functions.T;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.dao.BaseMapper;
import com.lujie.common.entity.BaseEntity;
import com.lujie.common.entity.EntityMap;
import com.lujie.common.util.ClassObjectUtil;
import com.lujie.common.util.CollectionUtil;
import com.lujie.common.util.DateUtil;
import com.lujie.common.util.JsonHelper;
import com.lujie.common.util.MD5Util;
import com.lujie.common.util.ReflectUtil;
import com.lujie.common.util.SQLUtil;
import com.lujie.common.util.StringUtil;
import com.lujie.common.util.StringUtils;
import com.lujie.common.util.TcpProtocolUtil;
import com.lujie.common.util.XslUtils;

public class BaseService   {
	private static final Logger log = LoggerFactory.getLogger(BaseService.class);
	
	public String serviceDesc;
	public String tableName;
	public String dbName;
	public List<EntityMap> columnInfoList;
	
	private static final String selectColumnSql="selectColumnSql";
	private static final String fromTableSql="fromTableSql";
	private static final String tableConditionSql="tableConditionSql";
	private static final String orderBySql="orderBySql";
	private static final String pageListSql="pageListSql";
	private static final String selectCountSql="selectCountSql";
	
	
	private BaseMapper baseMapper;
	
	
	
	public BaseMapper getBaseMapper() {
		return baseMapper;
	}

	public void setBaseMapper(BaseMapper baseMapper) {
		this.baseMapper = baseMapper;
	}

	@SuppressWarnings("unchecked")
	public List findAll(){
		return baseMapper.findAll();
	}
	
	/**
	 * 根据对象属性查找对象
	 * @param item
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Object findByItem(Object  item){
		return baseMapper.findByItem(item);
	}
	/**
	 * 根据对象属性查找对象集合
	 * @param item
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List findListByItem( Object  item){
		return baseMapper.findListByItem(item);
	}
	
	/**
	 * 根据多个id批量查询记录
	 * @param ids
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<T> findByIds(String ids){
		return baseMapper.findByIds(ids);
	}
	@SuppressWarnings("unchecked")
	public Object findById(String id){
		return baseMapper.findById(id);
	}
	
	public Object findByEntityMapId(Integer id){
		EntityMap tempEntityMap=new EntityMap();
		tempEntityMap.put("id", id);
		return baseMapper.findOne(tempEntityMap);
	}
	public EntityMap findByEntityMapId(String tableName,String idName,Integer idValue){
		EntityMap tempEntityMap=new EntityMap();
		tempEntityMap.put(this.selectColumnSql, "select * ");
		tempEntityMap.put(this.fromTableSql, "  from "+tableName);
		tempEntityMap.put(this.tableConditionSql, " where "+idName+"="+idValue);
		return (EntityMap)baseMapper.findOneEntityMap(tempEntityMap);
	}
	
	
	public ResultMsg updateEntityMap(
			HttpServletRequest request,EntityMap entityMap,
			String tableName,
			List<EntityMap> dbColumnList,
			String username,String userId) throws Exception {
		Integer id=entityMap.getInt("id");
		if(id==null){
			return new ResultMsg("610","id不能为空");
		}
		
		EntityMap dbEntityMap=(EntityMap)findByEntityMapId(id);
		if(dbEntityMap==null){
			return new ResultMsg("620","没有记录");
		}
		
		ResultMsg convertResultMsg=queryConditionCheck(entityMap,dbColumnList);
		if(!convertResultMsg.getCode().equals(ResultCode.Success.getCode().toString())){
			return convertResultMsg;
		}
		dbEntityMap.updateEntityMap(entityMap);
		String dataId=dbEntityMap.generateDataId();
		
		EntityMap columnInfoMap=getColumnsInfo(dbColumnList);
		EntityMap dbEntityMap2=findEntityMapByDataId(tableName,dataId,columnInfoMap);
		
		if(dbEntityMap2!=null && !dbEntityMap2.getInt("id").equals(id)){
			return new ResultMsg("303","已经存在相同记录，不能更新");
		}
		
		dbEntityMap.putCommonUpdateData(userId, username);
		
		baseMapper.update(dbEntityMap);
		
		return new ResultMsg(ResultCode.Success);
	}
	
	public EntityMap getColumnsInfo(List<EntityMap> dbColumnList){
		EntityMap columnsInfo=new EntityMap();
		for(EntityMap dbEntityMap:dbColumnList){
			String dbColumnName=dbEntityMap.getStr("COLUMN_NAME");
			columnsInfo.put(dbColumnName, dbEntityMap);
		}
		return columnsInfo;
	}
	
	
	public EntityMap getAllTableColumnsInfo(List<EntityMap> dbColumnList){
		EntityMap columnsInfo=new EntityMap();
		for(EntityMap dbEntityMap:dbColumnList){
			String dbColumnName=dbEntityMap.getStr("COLUMN_NAME");
			columnsInfo.put(dbColumnName, dbEntityMap);
			
			EntityMap relativeTableColumnMap=dbEntityMap.getEntityMap("relativeTableColumnMap");
			if(relativeTableColumnMap!=null){
				String relativeTableShortName=dbEntityMap.getStr("relativeTableShortName");
				for(Object key:relativeTableColumnMap.keySet()){
					columnsInfo.put(relativeTableShortName+"."+key.toString(), relativeTableColumnMap.getEntityMap(key.toString()));
				}
			}
		}
		return columnsInfo;
	}
	
	public ResultMsg updateEntityMap(EntityMap param,String username,String userId) throws Exception {
		List<EntityMap> dbColumnList=getColumnInfo(tableName);
		return updateEntityMap(param,tableName,dbColumnList,username,userId);
	}
	
	public ResultMsg updateEntityMap(
			EntityMap param,
			String tableName,
			List<EntityMap> dbColumnList,
			String username,String userId) throws Exception {
		
		String idName=getEntityIdName(dbColumnList);
		if(idName==null){
			return new ResultMsg("602","没有定义id,不能更新");
		}
		Integer id=param.getInt(idName);
		if(id==null){
			return new ResultMsg("610","id不能为空");
		}
		
		EntityMap dbEntityMap=findByEntityMapId(tableName,idName,id);
		if(dbEntityMap==null){
			return new ResultMsg("620","没有这个id的记录,不能更新");
		}
		EntityMap columnInfoMap=getColumnsInfo(dbColumnList);
		
		ResultMsg checkColumnMsg=checkColumn(tableName,param,dbColumnList,columnInfoMap,id.toString());
		if(!checkColumnMsg.getCode().equals(ResultCode.Success.getCode().toString())){
			return checkColumnMsg;
		}
		
		if(columnInfoMap.get("isValid")!=null){
			param.put("isValid", 1);
		}
		if(columnInfoMap.get("dataId")!=null){
			String dataId=param.generateDataIdV2();//这里生成了dataId值
			EntityMap dbEntityMap2=findEntityMapByDataId(tableName,dataId,columnInfoMap);
			if(dbEntityMap2!=null && !dbEntityMap2.getInt(idName).equals(id)){
				return new ResultMsg("510","不能更新,已经存在相同数据");
			}
		}
		
		ResultMsg updateEntitySql=getUpdateEntitySql(tableName,columnInfoMap,param,idName,userId,username);
		if(!updateEntitySql.getCode().equals(ResultCode.Success.getCode().toString())){
			return updateEntitySql;
		}
		EntityMap sqlEntityMap=new EntityMap();
		sqlEntityMap.put("updateSql", updateEntitySql.getData());
		log.info("sql:"+sqlEntityMap.toString());
		
		
		baseMapper.updateEntityMap(sqlEntityMap);
		
		return new ResultMsg(ResultCode.Success);
	}
	
	public ResultMsg deleteEntityMap(EntityMap param,String username,String userId) throws Exception {
		return deleteEntityMap(param,tableName, username, userId);
	}
	
	public String getEntityIdName(List<EntityMap> columnInfoList){
		for(EntityMap dbEntityMap:columnInfoList){
			String dbColumnName=dbEntityMap.getStr("COLUMN_NAME");
			String COLUMN_KEY=dbEntityMap.getStr("COLUMN_KEY");
			String EXTRA=dbEntityMap.getStr("EXTRA");
			if(COLUMN_KEY!=null && COLUMN_KEY.equals("PRI") && EXTRA!=null && EXTRA.equals("auto_increment")) {
				return dbColumnName;
			}
		}
		return null;
	}
	
	public String getEntityIdName(EntityMap columnInfoMap){
		for(Object dbColumnName:columnInfoMap.keySet()){
			EntityMap dbEntityMap=columnInfoMap.getEntityMap(dbColumnName.toString());
			String COLUMN_KEY=dbEntityMap.getStr("COLUMN_KEY");
			String EXTRA=dbEntityMap.getStr("EXTRA");
			if(COLUMN_KEY!=null && COLUMN_KEY.equals("PRI") && EXTRA!=null && EXTRA.equals("auto_increment")) {
				return dbColumnName.toString();
			}
		}
		return null;
	}
	
	public ResultMsg deleteEntityMap(
			EntityMap param,
			String tableName,
			String username,String userId) throws Exception {
		EntityMap columnInfoMap=getColumnInfoMap(tableName);
		String idName=getEntityIdName(columnInfoMap);
		if(idName==null){
			return new ResultMsg("601","没有定义id,不能删除");
		}
		Integer id=param.getInt(idName);
		if(id==null){
			return new ResultMsg("610","id不能为空");
		}
		
		EntityMap dbEntityMap=findByEntityMapId(tableName,idName,id);
		if(dbEntityMap==null){
			return new ResultMsg("620","没有记录");
		}
		
		String nowTime=DateUtil.getDateTimeStr(new Date());
		StringBuilder deleteSql=new StringBuilder();
		deleteSql.append("update "+tableName+" set  isValid=0,");
		
		
		if(columnInfoMap.get("modifyTime")!=null){
			deleteSql.append("modifyTime='"+nowTime+"',");
		}
		if(columnInfoMap.get("modifyUser")!=null){
			deleteSql.append("modifyUser='"+userId+"',");
		}
		if(columnInfoMap.get("modifyUserName")!=null){
			deleteSql.append("modifyUserName='"+username+"',");
		}
		if(columnInfoMap.get("dataId")!=null){
			deleteSql.append("dataId='"+System.currentTimeMillis()+"',");
		}
		deleteSql.delete(deleteSql.length()-1, deleteSql.length());
		
		deleteSql.append(" where "+idName+"="+id);
		
		
		if(columnInfoMap.get("isValid")==null){//直接删除记录
			String deleteSqlStr="delete from "+tableName+" where "+idName+"="+id;
			EntityMap sqlEntityMap=new EntityMap();
			sqlEntityMap.put("deleteSql", deleteSqlStr);
			baseMapper.deleteEntityMap(sqlEntityMap);
			log.info("直接删除记录:"+deleteSql);
			return new ResultMsg(ResultCode.Success);
		}
		
		
		EntityMap sqlEntityMap=new EntityMap();
		sqlEntityMap.put("deleteSql", deleteSql.toString());
		baseMapper.deleteEntityMap(sqlEntityMap);
		
		return new ResultMsg(ResultCode.Success);
	}
	
	public ResultMsg deleteEntityMap(
			HttpServletRequest request,EntityMap entityMap,
			String username,String userId) throws Exception {
		Integer id=entityMap.getInt("id");
		if(id==null){
			return new ResultMsg("610","id不能为空");
		}
		
		EntityMap dbEntityMap=(EntityMap)findByEntityMapId(id);
		if(dbEntityMap==null){
			return new ResultMsg("620","没有记录");
		}
		entityMap.put("id", id);
		entityMap.put("isValid", 0);
		entityMap.putCommonUpdateData(userId, username);
		
		baseMapper.update(entityMap);
		
		return new ResultMsg(ResultCode.Success);
	}
	
	public EntityMap findEntityMapByDataId(String tableName,String dataId,EntityMap columnInfoMap){
		String isValidSql="";
		if(columnInfoMap.get("isValid")!=null){
			isValidSql=" and isValid=1";
		}
		String orderbySql=null;
		if(columnInfoMap.get("modifyTime")!=null){
			orderbySql=" order by modifyTime desc";
		}
		
		
		EntityMap tempEntityMap=new EntityMap();
		tempEntityMap.put(this.selectColumnSql, "select * ");
		tempEntityMap.put(this.fromTableSql, " from "+tableName);
		tempEntityMap.put(this.tableConditionSql,  " where dataId='"+dataId+"' "+isValidSql);
		tempEntityMap.put(this.orderBySql, orderbySql);
		return (EntityMap)baseMapper.findOneEntityMap(tempEntityMap);
	}
	public ResultMsg checkUniqueEntityMap(String tableName,EntityMap param,List<String> columnList){
		
		if(CollectionUtil.isNotEmpty(columnList)){
			for(String columnName:columnList){
				if(columnName.equalsIgnoreCase("dataId")) continue;
				
				EntityMap tempEntityMap=new EntityMap();
				tempEntityMap.put(this.selectCountSql, "select count(1) from "+tableName);
				tempEntityMap.put(this.fromTableSql, " from "+tableName);
				
				Boolean isStringType=param.isStringType(columnName);
				if(isStringType==null){
					return new ResultMsg("7010","唯一条件检查："+columnName+",数据不能为空");
				}
				String columnValue=param.getStr(columnName);
				if(isStringType){
					tempEntityMap.put(this.tableConditionSql, " where isValid=1 and "+columnName+"='"+columnValue+"' ");
				}else{
					tempEntityMap.put(tableConditionSql, " where isValid=1 and "+columnName+"="+columnValue);
				}
				Integer totalCount=baseMapper.countTotalEntityMap(tempEntityMap);
				if(totalCount>0){
					return new ResultMsg("7020","唯一条件检查：字段名:"+columnName+",数据值:"+columnValue+"已经存在");
				}
			}
		}
		return new ResultMsg(ResultCode.Success);
	}
	
	
	/**
	 * 批量保存记录
	 * @param checkInList
	 */
	@SuppressWarnings("unchecked")
	public void batchSave(List list){
		baseMapper.batchSave(list);
	}
	public void save(Object p){
		baseMapper.save(p);
	}
	public void add(Object p){
		baseMapper.save(p);
	}
	public ResultMsg saveEntityMap(
			HttpServletRequest request,EntityMap entityMap,
			String tableName,
			List<EntityMap> dbColumnList,
			String username,String userId) throws Exception {
		EntityMap columnInfoMap=getColumnsInfo(dbColumnList);
		
		ResultMsg checkColumnMsg=checkColumn(tableName,entityMap,dbColumnList,columnInfoMap,"add");
		if(!checkColumnMsg.getCode().equals(ResultCode.Success.getCode().toString())){
			return checkColumnMsg;
		}
		String dataId=entityMap.generateDataId();
		
		EntityMap dbEntityMap=findEntityMapByDataId(tableName,dataId,columnInfoMap);
		if(dbEntityMap!=null){
			return new ResultMsg("510","已经存在相同数据");
		}
		
		entityMap.putCommonNewData(userId, username);
		
		baseMapper.save(entityMap);
		
		return new ResultMsg(ResultCode.Success,entityMap);
	}
	
	public ResultMsg saveEntityMap(HttpServletRequest request,
			EntityMap paramMap,
			String username,String userId) throws Exception {
		List<EntityMap> dbColumnList=getColumnInfo(tableName);
		return saveEntityMap(request,paramMap,dbColumnList,tableName,username,userId);
	}
	
	/**
	 * 添加数据
	 * 1、检查必填项
	 * 2、数据内容检查，防止sql注入
	 * 3、检查数据类型,类型，长度
	 * 4、检查数据唯一性，防止重复数据请求和插入重复数据
	 * 5、保存数据,生成sql,保存数据
	 * @param request
	 * @param paramMap
	 * @param tableName
	 * @param username
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	
	public ResultMsg saveEntityMap(HttpServletRequest request,
			EntityMap paramMap,List<EntityMap> dbColumnList,
			String tableName,
			String username,String userId) throws Exception {
		EntityMap columnInfoMap=getColumnsInfo(dbColumnList);
		
		ResultMsg checkColumnMsg=checkColumn(tableName,paramMap,dbColumnList,columnInfoMap,"add");
		if(!checkColumnMsg.getCode().equals(ResultCode.Success.getCode().toString())){
			return checkColumnMsg;
		}
		
		EntityMap resultMap=(EntityMap)checkColumnMsg.getData();
		StringBuilder columnNames=(StringBuilder)resultMap.get("columnNames");
		StringBuilder columnValues=(StringBuilder)resultMap.get("columnValues");
		String idName=resultMap.getStr("idName");
		
		if(idName==null){
			return new ResultMsg("622","没有定义自增主键,请先定义");
		}
		
		if(columnNames.length()==0){
			return new ResultMsg("623","请求保存"+paramMap+"数据无效,不保存");
		}
		
		//共用字段赋值
		String nowTime=DateUtil.getDateTimeStr(new Date());
		
		if(columnInfoMap.get("isValid")!=null){
			columnNames.append("isValid,");
			columnValues.append("1,");
		}
		if(columnInfoMap.get("dataId")!=null){//dataId，业务数据唯一标识，作用，1、可以作为一条记录的唯一标识，2、通过dataId,可以判断一条记录的某些字段值是否被非正常流程修改
			String dataId=paramMap.generateDataIdV2();
			
			EntityMap dbEntityMap=findEntityMapByDataId(tableName,dataId,columnInfoMap);
			if(dbEntityMap!=null){
				return new ResultMsg("510","已经存在相同数据");
			}
			columnNames.append("dataId,");
			columnValues.append("'"+dataId+"',");
		}
		
		if(columnInfoMap.get("createTime")!=null){
			columnNames.append("createTime,");
			columnValues.append("'"+nowTime+"',");
		}
		
		if(columnInfoMap.get("createUser")!=null){
			columnNames.append("createUser,");
			columnValues.append("'"+userId+"',");
		}
		
		if(columnInfoMap.get("createUserId")!=null){
			columnNames.append("createUserId,");
			columnValues.append("'"+userId+"',");
		}
		
		if(columnInfoMap.get("createUserName")!=null){
			columnNames.append("createUserName,");
			columnValues.append("'"+username+"',");
		}
		
		if(columnInfoMap.get("modifyTime")!=null){
			columnNames.append("modifyTime,");
			columnValues.append("'"+nowTime+"',");
		}
		
		if(columnInfoMap.get("modifyUser")!=null){
			columnNames.append("modifyUser,");
			columnValues.append("'"+userId+"',");
		}
		
		if(columnInfoMap.get("modifyUserId")!=null){
			columnNames.append("modifyUserId,");
			columnValues.append("'"+userId+"',");
		}
		
		if(columnInfoMap.get("modifyUserName")!=null){
			columnNames.append("modifyUserName,");
			columnValues.append("'"+username+"',");
		}
		
		
		columnNames=columnNames.delete(columnNames.length()-1, columnNames.length());
		columnValues=columnValues.delete(columnValues.length()-1, columnValues.length());
		
		StringBuilder insertSQL=new StringBuilder();
		insertSQL.append(" insert into "+tableName+" (");
		insertSQL.append(columnNames+" ) values( ");
		insertSQL.append(columnValues+" )");
		
		String insertSQLStr=insertSQL.toString();
		log.info("insert sql:"+insertSQLStr);
		
		EntityMap entity=new EntityMap();
		entity.put(idName, null);
		entity.put("insertSql", insertSQLStr);
		log.info("sql:"+entity.toString());
		
		baseMapper.saveEntityMap(entity);
		
		entity.remove("insertSql");
		return new ResultMsg(ResultCode.Success,entity);
	}
	
	
	
	
	
	
	public ResultMsg findEntityMapPageList(
			HttpServletRequest request,EntityMap entityMap,
			List<EntityMap> dbColumnList){
		
		ResultMsg convertResultMsg=queryConditionCheck(entityMap,dbColumnList);
		if(!convertResultMsg.getCode().equals(ResultCode.Success.getCode().toString())){
			return convertResultMsg;
		}
		
		entityMap.setRowIndex();
		List list=findPageList(entityMap);
		Long totalCount=countTotal(entityMap);
		return new ResultMsg(ResultCode.Success,list,totalCount);
	}
	
	/**
	 * 获取实体分页列表
	 * 
	 * 1、检查请求参数数据类型和转换
	 * 2、生成条件sql 根据请求参数字段,等值查询、数值或者时间范围查询(Start,End)、包含查询(InValues)、模糊查询(Like)
	 * 3、生成主表列sql和在字段备注中配置的关联表字段
	 * 4、生成主表排序sql modifyTime desc
	 * 5、生成分页sql ,获取查询结果
	 * 
	 * 查询条件示例：
	 * 1、等值查询(=)，请求参数名和表字段名保持一致，例如请求参数和值为age和10,将查询age=10的记录
	 * 2、范围查询(>=,<=)，开始参数名为表字段名+Start,结束参数名为表字段名+End,例如， ageStart=10,ageEnd=20,将查询age>=10 and age<=20的记录
	 * 3、包含查询(in),参数名为表字段名+InValues，例如1，ageInValues=1,2,3,将查询 age in(1,2,3),例如2，stateInValues=a,b,c,将查询 state in('a','b','c')
	 * 4、模糊查询(like),参数名为表字段名+Like，例如，nameLike=三,将查询 name like '%三%' 的记录
	 * 
	 * 结果排序
	 * 1、默认按修改时间倒序
	 * 2、前台自定义排序，t0.字段名 asc/desc,t1.字段名 asc/desc....
	 * 3、后台对应的mapper文件自定义排序
	 *  
			orderBySql=" order by "+orderBySql;
		}
	 * 
	 * 返回结果中的列
	 * 1、时间类型的字段，返回yyyy-MM-dd HH:mm:ss格式，返回名字和数据库字段名字一样，例如 createTime,2022-10-20 10:33:33
	 * 2、值描述的字段，表字段名+Desc,例如 valid值为1对应的描述为有效，valid的描述字段就为validDesc，validDesc的指为有效。注意这种关系配置在表字段备注中
	 * 
	 * 表的别名定义
	 * 主表别名为t0,
	 * 关联表别名为定义表字段的順序，
	 * 例如定义表字段顺序为(name,age,position(关联表codetable),worktype(关联表codetable)),
	 * 则,第一个关联表为codetable 表别名为t1,第二个关联表为codetable 表别名为t2
	 * 
	 * @param entityMap
	 * @param dbColumnList
	 * @return
	 */
	public ResultMsg findEntityMapPageList(
			String tableName,
			List<EntityMap> columnInfoList,
			EntityMap param){
		
		
		if(param.getInt("pageNum")==null){
			param.put("pageNum", 1);
		}
		if(param.getInt("pageSize")==null){
			param.put("pageSize", 10);
		}
		
		EntityMap columnInfoMap=getAllTableColumnsInfo(columnInfoList);
		ResultMsg getAllTableColumnConditionMsg=getAllTableColumnCondition(columnInfoMap, param);
		if(!getAllTableColumnConditionMsg.getCode().equals(ResultCode.Success.getCode().toString())){
			return getAllTableColumnConditionMsg;
		}
		
		String selectColumnSql="select "+getAllTableSelectColumnSql(columnInfoList);
		String fromTableSql=getFromTableSql(tableName,columnInfoList);
		String tableCondition=getAllTableColumnConditionMsg.getData().toString();
		String orderBySql=param.getStr("orderBy");
		if(StringUtil.isEmpty(orderBySql)){
			if(columnInfoMap.get("modifyTime")!=null){
				orderBySql=" order by t0."+"modifyTime desc ";
			}
		}else{
			orderBySql=" order by "+orderBySql;
		}
		EntityMap sqlEntityMap=new EntityMap();
		sqlEntityMap.put("selectColumnSql", selectColumnSql);
		sqlEntityMap.put("fromTableSql", fromTableSql);
		sqlEntityMap.put("tableConditionSql", tableCondition);
		sqlEntityMap.put("orderBySql", orderBySql);
		sqlEntityMap.put("pageListSql", " limit "+param.getRowIndex()+","+param.getPageSize());
		log.info("sql:"+sqlEntityMap.toString());
		
		List<EntityMap> list=baseMapper.findEntityMapPageList(sqlEntityMap);
		
		EntityMap countsqlEntityMap=new EntityMap();
		countsqlEntityMap.put("selectCountSql", "select count(1) ");
		countsqlEntityMap.put("fromTableSql", fromTableSql);
		countsqlEntityMap.put("tableConditionSql", tableCondition);
		Integer totalCount=baseMapper.countTotalEntityMap(countsqlEntityMap);
		
		return new ResultMsg(ResultCode.Success,list,totalCount);
	}
	
	public ResultMsg findEntityMapPageList(EntityMap param){
		columnInfoList=getColumnInfoWithRelativeTable(tableName);
		return findEntityMapPageList(tableName,columnInfoList,param);
	}
	
	public List<EntityMap> findEntityMapPageListNoMsg(EntityMap param){
		columnInfoList=getColumnInfoWithRelativeTable(tableName);
		
		ResultMsg msg=findEntityMapPageList(tableName,columnInfoList,param);
		if(msg.getCode().equals(ResultCode.Success.getCode().toString())){
			return (List<EntityMap>)msg.getData();
		}
		return null;
	}
	
	/**
	 * 获取所有实体
	 * 
	 * 1、请求参数类型检查和转换
	 * 2、非法关键字检查
	 * 3、生成条件sql，等值查询、模糊查询、范围查询、多值查询
	 * 4、生成查询的列
	 * 5、生成排序sql
	 * @param entityMap
	 * @param dbColumnList
	 * @return
	 */
	public ResultMsg findAllEntityMap(
			String tableName,
			List<EntityMap> columnInfoList,
			EntityMap param){
		
		EntityMap columnInfoMap=getAllTableColumnsInfo(columnInfoList);
		ResultMsg getAllTableColumnConditionMsg=getAllTableColumnCondition(columnInfoMap, param);
		if(!getAllTableColumnConditionMsg.getCode().equals(ResultCode.Success.getCode().toString())){
			return getAllTableColumnConditionMsg;
		}
		
		String selectColumnSql="select "+getAllTableSelectColumnSql(columnInfoList);
		String fromTableSql=getFromTableSql(tableName,columnInfoList);
		String tableCondition=getAllTableColumnConditionMsg.getData().toString();
		String orderBySql=param.getStr("orderBy");
		if(StringUtil.isEmpty(orderBySql)){
			if(columnInfoMap.get("modifyTime")!=null){
				orderBySql=" order by t0."+"modifyTime desc ";
			}
		}else{
			orderBySql=" order by "+orderBySql;
		}
		EntityMap sqlEntityMap=new EntityMap();
		sqlEntityMap.put("selectColumnSql", selectColumnSql);
		sqlEntityMap.put("fromTableSql", fromTableSql);
		sqlEntityMap.put("tableConditionSql", tableCondition);
		sqlEntityMap.put("orderBySql", orderBySql);
		log.info("sql:"+sqlEntityMap.toString());
		List<EntityMap> list=baseMapper.findAllEntityMap(sqlEntityMap);
		
		return new ResultMsg(ResultCode.Success,list);
	}
	
	
	
	public ResultMsg findAllEntityMap(EntityMap param){
		columnInfoList=getColumnInfoWithRelativeTable(tableName);
		return findAllEntityMap(tableName,columnInfoList,param);
	}
	
	/**
	 * 获取一个实体
	 * 
	 * 1、请求参数类型检查和转换
	 * 2、非法关键字检查
	 * 3、生成条件sql，等值查询、模糊查询、范围查询、多值查询
	 * 4、生成查询的列
	 * 5、生成排序sql
	 * @param entityMap
	 * @param dbColumnList
	 * @return
	 */
	public ResultMsg findOneEntityMap(
			String tableName,
			List<EntityMap> columnInfoList,
			EntityMap param){
		
		EntityMap columnInfoMap=getAllTableColumnsInfo(columnInfoList);
		ResultMsg getAllTableColumnConditionMsg=getAllTableColumnCondition(columnInfoMap, param);
		if(!getAllTableColumnConditionMsg.getCode().equals(ResultCode.Success.getCode().toString())){
			return getAllTableColumnConditionMsg;
		}
		
		String selectColumnSql="select "+getAllTableSelectColumnSql(columnInfoList);
		String fromTableSql=getFromTableSql(tableName,columnInfoList);
		String tableCondition=getAllTableColumnConditionMsg.getData().toString();
		String orderBySql=param.getStr("orderBy");
		if(StringUtil.isEmpty(orderBySql)){
			if(columnInfoMap.get("modifyTime")!=null){
				orderBySql=" order by t0."+"modifyTime desc ";
			}
		}else{
			orderBySql=" order by "+orderBySql;
		}
		EntityMap sqlEntityMap=new EntityMap();
		sqlEntityMap.put("selectColumnSql", selectColumnSql);
		sqlEntityMap.put("fromTableSql", fromTableSql);
		sqlEntityMap.put("tableConditionSql", tableCondition);
		sqlEntityMap.put("orderBySql", orderBySql);
		log.info("sql:"+sqlEntityMap.toString());
		
		EntityMap dbEntityMap=baseMapper.findOneEntityMap(sqlEntityMap);
		
		return new ResultMsg(ResultCode.Success,dbEntityMap);
	}
	
	
	public List<EntityMap> getColumnInfo(String tableName){
		if(dbName!=null){
			return baseMapper.getDBTableColumnInfo(dbName, tableName);
		}
		
		EntityMap relativeentityMap=new EntityMap();
		relativeentityMap.put("tableName", tableName);
		List<EntityMap> columnInfoList=baseMapper.getColumnInfo(relativeentityMap);
		
		return columnInfoList;
	}
	
	public EntityMap getColumnInfoMap(String tableName){
		List<EntityMap> columnInfoList=getColumnInfo(tableName);
		
		EntityMap columnInfoMap=new EntityMap();
		if(CollectionUtil.isNotEmpty(columnInfoList)){
			for(EntityMap dbEntityMap:columnInfoList){
				String dbColumnName=dbEntityMap.getStr("COLUMN_NAME");
				columnInfoMap.put(dbColumnName, dbEntityMap);
			}
		}
		return columnInfoMap;
	}
	
	
	public List<EntityMap> getColumnInfoWithRelativeTable(String tableName){
		List<EntityMap> entityMapList=getColumnInfo(tableName);
		
		if(CollectionUtil.isNotEmpty(entityMapList)){//关联表信息
			Integer relativeTableIndex=1;
			for(EntityMap dbEntityMap:entityMapList){
				String dbColumnName=dbEntityMap.getStr("COLUMN_NAME");
				
				String COLUMN_COMMENT=dbEntityMap.getStr("COLUMN_COMMENT");
				if(StringUtil.isEmpty(COLUMN_COMMENT)) {
					dbEntityMap.put("columnRemark", dbColumnName);
					continue;
				}
				
				Map<String,String> columnFunctionMap=getColumnFunction(COLUMN_COMMENT);
				dbEntityMap.put("columnRemark", columnFunctionMap.get("columnRemark"));
				if(columnFunctionMap.size()==1) continue;
				
				String relativeSelect=columnFunctionMap.get("relativeSelect");
				dbEntityMap.put("export", columnFunctionMap.get("export"));
				dbEntityMap.put("relativeselect", relativeSelect);
				dbEntityMap.put("unique", columnFunctionMap.get("unique"));
				dbEntityMap.put("columnValueDesc", columnFunctionMap.get("desc"));
				
				if(StringUtil.isEmpty(relativeSelect)) continue;
				String[] relateselectArr=relativeSelect.split(",");
				String relativetable=relateselectArr[0].trim();
				String relativecolumnid=relateselectArr[1].trim();
				String[] relativeselectcolumns=relateselectArr;
				
				String relativetableShortName="t"+relativeTableIndex;
				String[] relativetableArr=relativetable.split(" ");
				if(relativetableArr.length>1){
					relativetable=relativetableArr[0];
					relativetableShortName=relativetableArr[1];
				}
				
				dbEntityMap.put("relativeTableName", relativetable);
				dbEntityMap.put("relativeTableShortName", relativetableShortName);
				dbEntityMap.put("relativeColumnId", relativecolumnid);
				dbEntityMap.put("relativeSelectColumns", relativeselectcolumns);
				
				
				List<EntityMap> relativeTableColumnList=getColumnInfo(relativetable);
				EntityMap relativeTableColumnMap=getColumnsInfo(relativeTableColumnList);
				
				dbEntityMap.put("relativeTableColumnList", relativeTableColumnList);
				dbEntityMap.put("relativeTableColumnMap", relativeTableColumnMap);
				
				for(EntityMap relativeColumn:relativeTableColumnList){
					String dbRelativeColumn=relativeColumn.getStr("COLUMN_NAME");
					dbEntityMap.put(relativetableShortName+"."+dbRelativeColumn,relativeColumn);
				}
				relativeTableIndex++;
			}
		}
		return entityMapList;
	}
	
	
	public EntityMap findOneEntityMap(
			EntityMap param){
		columnInfoList=getColumnInfoWithRelativeTable(tableName);
		ResultMsg resultMsg=findOneEntityMap(this.tableName,columnInfoList,param);
		if(resultMsg.equals(ResultCode.Success)){
			return (EntityMap)resultMsg.getData();
		}
		return null;
	}
	
	public ResultMsg findOneEntity(EntityMap param){
		columnInfoList=getColumnInfoWithRelativeTable(tableName);
		return findOneEntityMap(this.tableName,columnInfoList,param);
	}
	
	
	
	public ResultMsg sumEntityMap(
			String tableName,
			List<EntityMap> columnInfoList,
			EntityMap param){
		
		EntityMap columnInfoMap=getAllTableColumnsInfo(columnInfoList);
		ResultMsg getAllTableColumnConditionMsg=getAllTableColumnCondition(columnInfoMap, param);
		if(!getAllTableColumnConditionMsg.getCode().equals(ResultCode.Success.getCode().toString())){
			return getAllTableColumnConditionMsg;
		}
		
		String selectColumnSql="select "+getSumColumnSql(columnInfoList);
		String fromTableSql=getFromTableSql(tableName,columnInfoList);
		String tableCondition=getAllTableColumnConditionMsg.getData().toString();
		
		EntityMap sqlEntityMap=new EntityMap();
		sqlEntityMap.put("selectColumnSql", selectColumnSql);
		sqlEntityMap.put("fromTableSql", fromTableSql);
		sqlEntityMap.put("tableConditionSql", tableCondition);
		log.info("sql:"+sqlEntityMap);
		EntityMap dbEntityMap=baseMapper.findOneEntityMap(sqlEntityMap);
		
		return new ResultMsg(ResultCode.Success,dbEntityMap);
	}
	
	public ResultMsg sumEntityMap(EntityMap param){
		columnInfoList=getColumnInfoWithRelativeTable(tableName);
		return sumEntityMap(tableName,columnInfoList,param);
	}
	
	public List<EntityMap> batchFindEntityMapPageList(EntityMap entityMap){
		entityMap.setRowIndex();
		List<EntityMap> list=baseMapper.findPageList(entityMap); 
		List<EntityMap> allList=new ArrayList<>();
		
		while(!CollectionUtil.isEmpty(list)){
			allList.addAll(list);
			entityMap.pageNumAddOne();
			list=baseMapper.findPageList(entityMap); 
		}
		return allList;
	} 
	
	/**
	 * 批量更新数据
	 * @param checkInList
	 */
	public void batchUpdate(List list){
		baseMapper.batchUpdate(list);
	}
	@SuppressWarnings("unchecked")
	public Integer update(Object  p){
		return baseMapper.update(p);
	}
	
	public Object update(HttpServletRequest request,Object  p){
		return baseMapper.update(p);
	}
	
	public Object delete(HttpServletRequest request,Object  p){
		return baseMapper.delete(p);
	}
	
	
	
	
	/**
	 * 查找返回分页信息
	 * pageIndex,pageSize
	 * @param paramMap
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<T> findPageList(HashMap<String,Object> paramMap){
		return baseMapper.findPageList(paramMap);
	}
	
	
	/**
	 * 根据对象属性，查找对象列表
	 * @param property
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List findPageListByProperty(Object property){
		return baseMapper.findPageListByProperty(property);
	}
	
	
	public ResultMsg inValidSql(String sql){
		if(StringUtil.isEmpty(sql)){
			return new ResultMsg(ResultCode.Success);
		}
		
		if(sql.contains("'")){
			return new ResultMsg("6010","内容不能包含特殊字符'");
		}
		
//		String keywords = "',and,exec,insert,select,delete,update,count,*,chr,mid,master,truncate,char,declare,or,+";
//		sql=sql.toLowerCase();
//		
//		String keyarr[] =keywords.split(",");
//		for(String key:keyarr){
//			if(sql.contains(key)){
//				return new ResultMsg("6010","内容不能包含特殊字符"+key);
//			}
//		}
		return new ResultMsg(ResultCode.Success);
	}
	
	
	public String getAllTableConditionSql(EntityMap columnInfoMap,EntityMap param){
		String mainTableShortName="t0.";
		
		StringBuilder allTableConditionSql=new StringBuilder();
		
		
		String conditionSqlHeader=" where "+mainTableShortName+"isValid=1 ";
		allTableConditionSql.append(conditionSqlHeader);
		
		for(Object paramNameObj:param.keySet()){
			if(paramNameObj==null) continue;
			String paramName=paramNameObj.toString();
			Object paramValue=param.get(paramName);
			if(paramValue==null) continue;
			
			EntityMap columnInfo=columnInfoMap.getEntityMap(paramName);
			String startTimeColumnName=null;
			String endTimeColumnName=null;
			String likeColumnName=null;
			
			
			if(columnInfo==null){//时间Start,时间End,Like
				startTimeColumnName=paramName.replace("Start", "");
				if(startTimeColumnName.length()==paramName.length()-5){
					columnInfo=columnInfoMap.getEntityMap(startTimeColumnName);
				}else{
					endTimeColumnName=paramName+"End";
					if(endTimeColumnName.length()==paramName.length()-3){
						columnInfo=columnInfoMap.getEntityMap(endTimeColumnName);
					}else{
						likeColumnName=paramName+"Like";
						if(likeColumnName.length()==paramName.length()-4){
							columnInfo=columnInfoMap.getEntityMap(likeColumnName);
						}
					}
				}
			}
			
			if(columnInfo!=null){
				String DATA_TYPE=columnInfo.getStr("DATA_TYPE");
				
				String condditionColumnName=mainTableShortName+paramName;
				if(paramName.contains(".")){
					condditionColumnName=paramName;
				}
				if(startTimeColumnName!=null){
					allTableConditionSql.append(" and "+condditionColumnName+" >='"+paramValue+"' ");
				}else if(endTimeColumnName!=null){
					allTableConditionSql.append(" and "+condditionColumnName+" <='"+paramValue+"' ");
				}else if(likeColumnName!=null){
					allTableConditionSql.append(" and "+condditionColumnName+" like '%"+paramValue+"%' ");
				}else if(isStringType(DATA_TYPE)){
					allTableConditionSql.append(" and "+condditionColumnName+" ='"+paramValue+"' ");
				}else{
					allTableConditionSql.append(" and "+condditionColumnName+" ="+paramValue+" ");
				}
			}
		}
		
		
		return allTableConditionSql.toString();
	}
	
	public ResultMsg getUpdateEntitySql(String tableName,EntityMap dbColumnInfo,EntityMap param,
			String idName,String userId,String username){
		StringBuilder updateSql=new StringBuilder();
		
		String updateSqlHeader="update "+tableName+" set ";
		updateSql.append(updateSqlHeader);
		
		List<String> strTypeList=Arrays.asList(new String[]{"varchar","datetime","date","text","mediumtext","longtext"});
		//要修改的字段
		for(Object columnNameObj:param.keySet()){
			if(columnNameObj==null) continue;
			String dbColumnName=columnNameObj.toString();
			Object columnValue=param.get(dbColumnName);
			if(columnValue==null) continue;
			
			EntityMap columnInfo=dbColumnInfo.getEntityMap(dbColumnName);
			if(columnInfo==null) continue;
			
			String DATA_TYPE=columnInfo.getStr("DATA_TYPE");
			String COLUMN_TYPE=columnInfo.getStr("COLUMN_TYPE");
			String COLUMN_COMMENT=columnInfo.getStr("COLUMN_COMMENT");
			if(StringUtil.isEmpty(COLUMN_COMMENT)){
				COLUMN_COMMENT=dbColumnName;
			}else{
				String[] comments=COLUMN_COMMENT.trim().split(":");
				COLUMN_COMMENT=comments[0];
			}
			
			if(strTypeList.contains(DATA_TYPE)){
				updateSql.append(dbColumnName+"='"+param.getStr(dbColumnName)+"',");
			}else{
				updateSql.append(dbColumnName+"="+columnValue+",");
			}
		}
		
		if(updateSql.length()==updateSqlHeader.length()){
			return new ResultMsg("7010","请求修改数据的参数为空");
		}
		
		
		String nowTime=DateUtil.getDateTimeStr(new Date());
		
		if(dbColumnInfo.get("modifyTime")!=null){
			updateSql.append("modifyTime='"+nowTime+"',");
		}
		if(dbColumnInfo.get("modifyUser")!=null){
			updateSql.append("modifyUser='"+userId+"',");
		}
		if(dbColumnInfo.get("modifyUserId")!=null){
			updateSql.append("modifyUserId='"+userId+"',");
		}
		if(dbColumnInfo.get("modifyUserName")!=null){
			updateSql.append("modifyUserName='"+username+"',");
		}
		updateSql.delete(updateSql.length()-1, updateSql.length());
		updateSql.append(" where "+idName+"="+param.getStr(idName));
		
		return new ResultMsg(ResultCode.Success,updateSql.toString());
	}
	
	public Map<String,String> getColumnFunction(String columnComment){
		Map<String,String> columnFunMap=new HashMap<String,String>();
		if(StringUtil.isEmpty(columnComment)) return columnFunMap;
		
		String[] columnCommentArr=columnComment.split(":");
		columnFunMap.put("columnRemark", columnCommentArr[0]);
		
		if(columnCommentArr.length>1){
			for(int index=1;index<columnCommentArr.length;index++){
				if(columnCommentArr[index].equalsIgnoreCase("export")){
					columnFunMap.put("export", columnCommentArr[0]);
				}else if(columnCommentArr[index].contains("relativeselect(")){
					String functionStr=columnCommentArr[index].replace("relativeselect(", "").replace(")", "");
					columnFunMap.put("relativeSelect", functionStr);
				}else if(columnCommentArr[index].contains("unique(")){
					String functionStr=columnCommentArr[index].replace("unique(", "").replace(")", "");
					columnFunMap.put("unique", functionStr);
				}else if(columnCommentArr[index].contains("desc(")){//如果字段是类型值，接口查询这个字段，将返回这个字段值和描述
					String functionStr=columnCommentArr[index].replace("desc(", "").replace(")", "");
					columnFunMap.put("desc", functionStr);
				}
			}
		}
		return columnFunMap;
	}
	
	public List<String> getUniqueColumnList(String columnComment){
		if(StringUtil.isEmpty(columnComment)) return null;
		String[] columnCommentArr=columnComment.split(":");
		if(columnCommentArr.length==1) return null;
		
		for(int index=1;index<columnCommentArr.length;index++){
			if(columnCommentArr[index].contains("unique(")){
				String uniqueColumns=columnCommentArr[index].replace("unique(", "").replace(")", "").trim();
				List<String> uniqueColumnList=new ArrayList<>();
				if(StringUtil.isEmpty(uniqueColumns)) return uniqueColumnList;
				
				uniqueColumnList.addAll(Arrays.asList(uniqueColumns.split(",")));
				return uniqueColumnList;
			}
		}
		
		return null;
	}
	
	public String getAllTableSelectColumnSql(List<EntityMap> columnInfoList){
		StringBuilder mainColumnsSql=new StringBuilder();
		String shortTableName="t0.";
		for(EntityMap dbEntityMap:columnInfoList){
			String dbColumnName=dbEntityMap.getStr("COLUMN_NAME");
			String DATA_TYPE=dbEntityMap.getStr("DATA_TYPE");
			String COLUMN_COMMENT=dbEntityMap.getStr("COLUMN_COMMENT");
			
			
			if(DATA_TYPE.equalsIgnoreCase("datetime")){
				mainColumnsSql.append("ifnull(DATE_FORMAT("+shortTableName+dbColumnName+",'%Y-%m-%d %H:%i:%s'),'') "+dbColumnName+",");
			}else if(DATA_TYPE.equalsIgnoreCase("date")){
				mainColumnsSql.append("ifnull(DATE_FORMAT("+shortTableName+dbColumnName+",'%Y-%m-%d'),'') "+dbColumnName+",");
			}else{
				mainColumnsSql.append("ifnull("+shortTableName+dbColumnName+",'') "+dbColumnName+",");	
			}
			
			String columnValueDesc=dbEntityMap.getStr("columnValueDesc");
			if(StringUtil.isNotEmpty(columnValueDesc)){
				try{
					String tableColumnName=shortTableName+dbColumnName;
					
					Boolean isColumnInt=isNumberType(DATA_TYPE);
					
					columnValueDesc=columnValueDesc.trim();
					String[] columnValueDescArr=columnValueDesc.split(",");
					
					StringBuilder tableColumnNameDescs=new StringBuilder();
					tableColumnNameDescs.append("(case");
					
					if(columnValueDescArr!=null){
						for(String tempcolumnValueDesc:columnValueDescArr){
							tempcolumnValueDesc=tempcolumnValueDesc.trim();
							String[] tempcolumnValueItemArr=tempcolumnValueDesc.split("=");
							if(tempcolumnValueItemArr!=null && tempcolumnValueItemArr.length>1){
								String keyValue=tempcolumnValueItemArr[0];
								String keyValueDesc=tempcolumnValueItemArr[1].replace("'", "");
								keyValueDesc="'"+keyValueDesc+"'";
								
								if(!isColumnInt){
									keyValue="'"+keyValue+"'";
								}
								tableColumnNameDescs.append(" when "+tableColumnName+"="+keyValue+" then "+keyValueDesc);
							}
						}
					}
					if(tableColumnNameDescs.length()>5){
						tableColumnNameDescs.append(" else '' end) "+dbColumnName+"Desc,");
						mainColumnsSql.append(tableColumnNameDescs);
					}
					
				}catch(Exception ex1){
					log.info(StringUtil.getExceptionDesc(ex1));
				}
			}
			
			
			
			String relativeTableShortName=dbEntityMap.getStr("relativeTableShortName");
			if(!StringUtil.isEmpty(relativeTableShortName)){
				try{
					String[] relativeSelectColumns=(String[])dbEntityMap.get("relativeSelectColumns");
					if(relativeSelectColumns!=null && relativeSelectColumns.length>2){
						for(int relativeSelectColumnIndex=2;relativeSelectColumnIndex<relativeSelectColumns.length;relativeSelectColumnIndex++){
							String relativeSelectColumn=relativeSelectColumns[relativeSelectColumnIndex].trim();
							String[] relativeSelectColumnArr=relativeSelectColumn.split(" ");
							String relativeSelectColumnName=relativeSelectColumn;
							String relativeSelectColumnShorrName=relativeSelectColumn;
							
							if(relativeSelectColumnArr.length==2){
								relativeSelectColumnName=relativeSelectColumnArr[0].trim();
								relativeSelectColumnShorrName=relativeSelectColumnArr[1].trim();
							}
							EntityMap relativeEntityMap=dbEntityMap.getEntityMap(relativeTableShortName+"."+relativeSelectColumnName);
							if(relativeEntityMap!=null){
								String relativeEntityMapDATA_TYPE=dbEntityMap.getStr("DATA_TYPE");
								
								if(relativeEntityMapDATA_TYPE.equalsIgnoreCase("datetime")){
									mainColumnsSql.append("ifnull(DATE_FORMAT("+relativeTableShortName+"."+relativeSelectColumnName+",'%Y-%m-%d %H:%i:%s'),'') "+relativeSelectColumnShorrName+",");
								}else if(relativeEntityMapDATA_TYPE.equalsIgnoreCase("date")){
									mainColumnsSql.append("ifnull(DATE_FORMAT("+relativeTableShortName+"."+relativeSelectColumnName+",'%Y-%m-%d'),'') "+relativeSelectColumnShorrName+",");
								}else{
									mainColumnsSql.append("ifnull("+relativeTableShortName+"."+relativeSelectColumnName+",'') "+relativeSelectColumnShorrName+",");
								}
							}
						}
					}
				}catch(Exception ex2){
					log.info(StringUtil.getExceptionDesc(ex2));
				}
				
			}
			
		}
		
		mainColumnsSql=mainColumnsSql.delete(mainColumnsSql.length()-1, mainColumnsSql.length());
		return mainColumnsSql.toString();
	}
	
	public String getUpperFirstColumnName(String columnName){
		return columnName.substring(0,1).toUpperCase()+columnName.substring(1);
	}
	
	public String getSumColumnSql(List<EntityMap> columnInfoList){
		StringBuilder mainColumnsSql=new StringBuilder();
		String shortTableName="t0.";
		for(EntityMap dbEntityMap:columnInfoList){
			String dbColumnName=dbEntityMap.getStr("COLUMN_NAME");
			String DATA_TYPE=dbEntityMap.getStr("DATA_TYPE");
			
			String sumName="total"+getUpperFirstColumnName(dbColumnName);
			
			if(isNumberType(DATA_TYPE)){
				mainColumnsSql.append("ifnull(sum("+shortTableName+dbColumnName+"),0) "+sumName+",");
				
			}
			
		}
		mainColumnsSql.append(" count(1) totalCount ");
		return mainColumnsSql.toString();
	}
	
	
	public String getFromTableSql(String tableName,List<EntityMap> columnInfoList){
		String shortTableName="t0.";
		
		StringBuilder fromTableSql=new StringBuilder();
		fromTableSql.append(" from "+tableName+" t0 ");
		
		
		for(EntityMap dbEntityMap:columnInfoList){
			String dbColumnName=dbEntityMap.getStr("COLUMN_NAME");
			String relativeTableName=dbEntityMap.getStr("relativeTableName");
			
			if(StringUtil.isNotEmpty(relativeTableName)){
				String relativeTableShortName=dbEntityMap.getStr("relativeTableShortName");
				String relativeColumnId=dbEntityMap.getStr("relativeColumnId");
				String isValid=dbEntityMap.getStr(relativeTableShortName+".isValid");
				
				fromTableSql.append(" left join "+relativeTableName+" "+relativeTableShortName);
				fromTableSql.append(" on "+shortTableName+dbColumnName+"="+relativeTableShortName+"."+relativeColumnId+" ");
				if(StringUtil.isNotEmpty(isValid)){
					fromTableSql.append(" and "+relativeTableShortName+".isValid=1 ");
				}
			}
		}
		
		return fromTableSql.toString();
	}
	
	/**
	 * 1、默认导出所有字段的值
	 * 2、备注配置export后，
	 * 2.1、没有配置关联表字段和描述值，则导出这个字段的值
	 * 2.2 配置了desc,则导出这个字段描述而不导出这个字段
	 * 2.3、有配置关联表字段，则，不导出这个字段的值，导出关联表字段的值，如果需要导出这个字段值，需要在备注中配置关联表的这个字段
	 * 
	 * 注意，配置了值描述，则只导出描述，没有配置值描述，配置了关联表，则导出关联表字段
	 * 
	 * 、要导出的列的sql
	 * @param shortTableName
	 * @param columnInfoList
	 * @return
	 */
	public Map<String,Object> getExportColumn(List<EntityMap> columnInfoList){
		String shortTableName="t0.";
		
		StringBuilder exportColumnsSql=new StringBuilder();
		StringBuilder customExportColumnsSql=new StringBuilder();
		
		StringBuilder dbColumns=new StringBuilder();//查询返回字段名
		StringBuilder dbCustomolumns=new StringBuilder();//自定义查询返回字段名
		
		List<String> columnNameList=new ArrayList<>();
		columnNameList.add("序号");
		List<String> customcolumnNameList=new ArrayList<>();
		customcolumnNameList.add("序号");
		
		List<String> donotExportColumnList=Arrays.asList(new String[]{"dataid","isvalid","createuser","modifyuser"});
		
		Boolean defaultDexport=true;//mysql某个表中，所有字段都备注没有标识export关键字，则默认导出这个表所有字段，否则，导出这个表中字段备注带有export的字段
		
		for(EntityMap dbEntityMap:columnInfoList){
			String dbColumnName=dbEntityMap.getStr("COLUMN_NAME");
			String DATA_TYPE=dbEntityMap.getStr("DATA_TYPE");
			String columnRemark=dbEntityMap.getStr("columnRemark");
			
			if(donotExportColumnList.contains(dbColumnName.toLowerCase())){
				continue;
			}
			
			String export=dbEntityMap.getStr("export");
			if(StringUtil.isNotEmpty(export)){
				defaultDexport=false;
				break;
			}else{
				if(DATA_TYPE.equalsIgnoreCase("datetime")){
					exportColumnsSql.append("ifnull(DATE_FORMAT("+shortTableName+dbColumnName+",'%Y-%m-%d %H:%i:%s'),'') "+dbColumnName+",");
				}else if(DATA_TYPE.equalsIgnoreCase("date")){
					exportColumnsSql.append("ifnull(DATE_FORMAT("+shortTableName+dbColumnName+",'%Y-%m-%d'),'') "+dbColumnName+",");
				}else{
					exportColumnsSql.append("ifnull("+shortTableName+dbColumnName+",'') "+dbColumnName+",");	
				}
				dbColumns.append(dbColumnName+",");
				columnNameList.add(columnRemark);
				
			}
		}
		
		if(!defaultDexport){
			for(EntityMap dbEntityMap:columnInfoList){
				String dbColumnName=dbEntityMap.getStr("COLUMN_NAME");
				String DATA_TYPE=dbEntityMap.getStr("DATA_TYPE");
				String columnRemark=dbEntityMap.getStr("columnRemark");
				
				if(donotExportColumnList.contains(dbColumnName.toLowerCase())){
					continue;
				}
				
				String export=dbEntityMap.getStr("export");
				if(StringUtil.isEmpty(export)){
					continue;
				}
				
				String columnValueDesc=dbEntityMap.getStr("columnValueDesc");
				if(StringUtil.isNotEmpty(columnValueDesc)){
					String tableColumnName=shortTableName+dbColumnName;
					
					Boolean isColumnInt=isNumberType(DATA_TYPE);
					
					columnValueDesc=columnValueDesc.trim();
					String[] columnValueDescArr=columnValueDesc.split(",");
					
					StringBuilder tableColumnNameDescs=new StringBuilder();
					tableColumnNameDescs.append("(case");
					
					if(columnValueDescArr!=null){
						for(String tempcolumnValueDesc:columnValueDescArr){
							tempcolumnValueDesc=tempcolumnValueDesc.trim();
							String[] tempcolumnValueItemArr=tempcolumnValueDesc.split("=");
							if(tempcolumnValueItemArr!=null && tempcolumnValueItemArr.length>1){
								String keyValue=tempcolumnValueItemArr[0];
								String keyValueDesc=tempcolumnValueItemArr[1].replace("'", "");
								keyValueDesc="'"+keyValueDesc+"'";
								
								if(!isColumnInt){
									keyValue="'"+keyValue+"'";
								}
								tableColumnNameDescs.append(" when "+tableColumnName+"="+keyValue+" then "+keyValueDesc);
							}
						}
					}
					if(tableColumnNameDescs.length()>5){
						String resultColumnName=dbColumnName+"Desc";
						tableColumnNameDescs.append(" else '' end) "+resultColumnName+",");
						customExportColumnsSql.append(tableColumnNameDescs);	
						dbCustomolumns.append(resultColumnName+",");
						customcolumnNameList.add(columnRemark);
						continue;
					}
				}
				
				String relativeTableShortName=dbEntityMap.getStr("relativeTableShortName");
				if(!StringUtil.isEmpty(relativeTableShortName)){
					String[] relativeSelectColumns=(String[])dbEntityMap.get("relativeSelectColumns");
					if(relativeSelectColumns!=null && relativeSelectColumns.length>2){
						for(int relativeSelectColumnIndex=2;relativeSelectColumnIndex<relativeSelectColumns.length;relativeSelectColumnIndex++){
							String relativeSelectColumn=relativeSelectColumns[relativeSelectColumnIndex].trim();
							String[] relativeSelectColumnArr=relativeSelectColumn.split(" ");
							String relativeSelectColumnName=relativeSelectColumn;
							String relativeSelectColumnAlisName=relativeSelectColumn;
							
							if(relativeSelectColumnArr.length==2){
								relativeSelectColumnName=relativeSelectColumnArr[0].trim();
								relativeSelectColumnAlisName=relativeSelectColumnArr[1].trim();
							}
							EntityMap relativeEntityMap=dbEntityMap.getEntityMap(relativeTableShortName+"."+relativeSelectColumnName);
							if(relativeEntityMap!=null){
								String relativeEntityMapDATA_TYPE=dbEntityMap.getStr("DATA_TYPE");
								
								if(relativeEntityMapDATA_TYPE.equalsIgnoreCase("datetime")){
									customExportColumnsSql.append("ifnull(DATE_FORMAT("+relativeTableShortName+"."+relativeSelectColumnName+",'%Y-%m-%d %H:%i:%s'),'') "+relativeSelectColumnAlisName+",");
								}else if(relativeEntityMapDATA_TYPE.equalsIgnoreCase("date")){
									customExportColumnsSql.append("ifnull(DATE_FORMAT("+relativeTableShortName+"."+relativeSelectColumnName+",'%Y-%m-%d'),'') "+relativeSelectColumnAlisName+",");
								}else{
									customExportColumnsSql.append("ifnull("+relativeTableShortName+"."+relativeSelectColumnName+",'') "+relativeSelectColumnAlisName+",");
								}
								dbCustomolumns.append(relativeSelectColumnAlisName+",");
								
								String relativeCOLUMN_COMMENT=relativeEntityMap.getStr("COLUMN_COMMENT");
								String relativeColumnRemark=relativeSelectColumnName;
								if(!StringUtil.isEmpty(relativeCOLUMN_COMMENT)){
									relativeColumnRemark=relativeCOLUMN_COMMENT.trim().split(":")[0];
								}
								customcolumnNameList.add(relativeColumnRemark);
							}
						}
						continue;
					}
				}
				
				
				if(DATA_TYPE.equalsIgnoreCase("datetime")){
					customExportColumnsSql.append("ifnull(DATE_FORMAT("+shortTableName+dbColumnName+",'%Y-%m-%d %H:%i:%s'),'') "+dbColumnName+",");
				}else if(DATA_TYPE.equalsIgnoreCase("date")){
					customExportColumnsSql.append("ifnull(DATE_FORMAT("+shortTableName+dbColumnName+",'%Y-%m-%d'),'') "+dbColumnName+",");
				}else{
					customExportColumnsSql.append("ifnull("+shortTableName+dbColumnName+",'') "+dbColumnName+",");	
				}
				dbCustomolumns.append(dbColumnName+",");
				customcolumnNameList.add(columnRemark);
			}
		}
		
		if(!defaultDexport){
			exportColumnsSql=customExportColumnsSql;
			dbColumns=dbCustomolumns;
			columnNameList=customcolumnNameList;
		}
		
		String[] columnDescArr=new String[columnNameList.size()];
		for(Integer index=0;index<columnDescArr.length;index++){
			columnDescArr[index]=columnNameList.get(index);
		}
		
		exportColumnsSql=exportColumnsSql.delete(exportColumnsSql.length()-1, exportColumnsSql.length());
		dbColumns=dbColumns.delete(dbColumns.length()-1, dbColumns.length());
		
		Map<String,Object> result=new HashMap<>();
		result.put("columnDescArr", columnDescArr);
		result.put("exportColumnsSql", exportColumnsSql);
		result.put("dbColumns", dbColumns);
		return result;
	}
	
	
	public String getExportColumnsSql(String shortTableName,String[] exportColumnsArr,EntityMap columnInfo){
		StringBuilder exportColumnsSql=new StringBuilder();
		
		if(shortTableName==null) {
			shortTableName="";
		}else{
			shortTableName=shortTableName+".";
		}
		
		for(String exportColumn:exportColumnsArr){
			EntityMap dbEntityMap=columnInfo.getEntityMap(exportColumn);
			String DATA_TYPE=dbEntityMap.getStr("DATA_TYPE");
			
			if(DATA_TYPE.equalsIgnoreCase("datetime")){
				exportColumnsSql.append("DATE_FORMAT("+shortTableName+exportColumn+",'%Y-%m-%d %H:%i:%s') "+exportColumn+",");
			}else if(DATA_TYPE.equalsIgnoreCase("date")){
				exportColumnsSql.append("DATE_FORMAT("+shortTableName+exportColumn+",'%Y-%m-%d') "+exportColumn+",");
			}else{
				exportColumnsSql.append(shortTableName+exportColumn+",");	
			}
		}
		exportColumnsSql=exportColumnsSql.delete(exportColumnsSql.length()-1, exportColumnsSql.length());
		return exportColumnsSql.toString();
	}
	
	public void exportEntity(HttpServletResponse response,EntityMap param) throws Exception  {
		List<EntityMap> columnInfoList=getColumnInfoWithRelativeTable(tableName);
		 exportEntity(response,serviceDesc,tableName,columnInfoList,param);
	}
	
	/**
	 * 导出实体
	 * 
	 * 1、请求参数检查
	 * 2、生成条件sql 
	 * 3、生成导出列sql 
	 * 
	 *  
	 *  蓝牙称重-垃圾类别

备注字段说明：
1、备注为空，代表字段描述为字段名
2、备注不为空，备注的意义，
2.1、没有用:符号分隔，代表字段描述，
2.2、有用:符号分隔，
2.2.1 分隔后的第一个代表字段描述
2.2.2 分隔后的其他关键字的不同意义
2.2.2.1 export,代表要导出的字段，(在导出时候使用)，如果表中所有字段备注都没有export标识，则导出时候，将导出所有表字段，否则导出备注带有export标识的字段
2.2.2.2 relativeselect,代表要关联查询的字段，格式relativeselect(表名,关联id,查询的字段1,查询的字段2...)
2.2.2.3 unique,字段的数据内容唯一约束，格式unique(column1,column2...),如果括号内容没有字段，则代表本字段数据内容唯一，如果有一些字段，代表本字段和这些字段联合的数据需要唯一

设备内部id:relativeselect(bluetoothbalance_device,deviceInnerId,deviceName,deviceUuid):unique(userId)

	 *  
	 *  
	 *  
	 * @param entityMap
	 * @param dbColumnList
	 * @return
	 */
	public void exportEntity(
			HttpServletResponse response,String title,
			String tableName,
			List<EntityMap> columnInfoList,
			EntityMap param) throws Exception  {
		
		EntityMap columnInfoMap=getAllTableColumnsInfo(columnInfoList);
		ResultMsg getAllTableColumnConditionMsg=getAllTableColumnCondition(columnInfoMap, param);
		if(!getAllTableColumnConditionMsg.getCode().equals(ResultCode.Success.getCode().toString())){
			JsonHelper.outputMsg(response, getAllTableColumnConditionMsg);
			return;
		}
		
		Map<String,Object> exportColumnMap=getExportColumn(columnInfoList);
		String[] columnDescArr=(String[])exportColumnMap.get("columnDescArr");
		String exportColumnsSql=exportColumnMap.get("exportColumnsSql").toString();
		String[] dbColumnNames=exportColumnMap.get("dbColumns").toString().split(",");
		
		String selectColumnSql="select "+exportColumnsSql;
		String fromTableSql=getFromTableSql(tableName,columnInfoList);
		String tableCondition=getAllTableColumnConditionMsg.getData().toString();
		String orderBySql=param.getStr("orderBy");
		if(StringUtil.isEmpty(orderBySql)){
			if(columnInfoMap.get("modifyTime")!=null){
				orderBySql=" order by t0."+"modifyTime desc ";
			}
		}else{
			orderBySql=" order by "+orderBySql;
		}
		
		EntityMap sqlEntityMap=new EntityMap();
		sqlEntityMap.put("selectColumnSql", selectColumnSql);
		sqlEntityMap.put("fromTableSql", fromTableSql);
		sqlEntityMap.put("tableConditionSql", tableCondition);
		sqlEntityMap.put("orderBySql", orderBySql);
		
		Integer index=1;
		List<Object[]> dataList=new ArrayList<>();
		param.put("pageNum", 1);
		param.put("pageSize", 10);
		sqlEntityMap.put("pageListSql", " limit "+param.getRowIndex()+","+param.getPageSize());
		log.info("sql:"+sqlEntityMap.toString());
		
		List<EntityMap> list=baseMapper.findEntityMapPageList(sqlEntityMap);
		
		while(CollectionUtil.isNotEmpty(list)){
			
			for(EntityMap dbEntityMap:list){
				ArrayList line=new ArrayList();
				line.add(index++); 
				
				for(Integer columnIndex=0;columnIndex<dbColumnNames.length;columnIndex++){
					line.add(dbEntityMap.getNotNullStr(dbColumnNames[columnIndex]));
				}
				dataList.add(line.toArray());
			}
			param.pageNumAddOne();
			sqlEntityMap.put("pageListSql", " limit "+param.getRowIndex()+","+param.getPageSize());
			list=baseMapper.findEntityMapPageList(sqlEntityMap);
		}
		
		XslUtils ex = new XslUtils(response,title, columnDescArr, dataList);  
		ex.export();
	}
	/**
	 * 导出自定义数据
	 * @param request
	 * @param response
	 * @param title
	 * @param rowsNames
	 * @param dbColumnNames
	 * @throws Exception
	 */
	public void exportCustom(HttpServletRequest request,HttpServletResponse response,
			String title,String[] rowsNames,String[] dbColumnNames) throws Exception  {
		
		EntityMap param=ClassObjectUtil.getEntityMap(request.getParameterMap());
		List<EntityMap> allList=batchFindEntityMapPageList(param);
		if(CollectionUtil.isEmpty(allList)){
			JsonHelper.outputMsg(response, new ResultMsg("301","没有记录"));
			return;
		}
		List<Object[]> dataList=new ArrayList<>();
		for(int i=1;i<=allList.size();i++){
			EntityMap entityMap=allList.get(i-1);
			ArrayList list=new ArrayList();
			list.add(i);
			for(String columnName:dbColumnNames){
				list.add(entityMap.getNotNullStr(columnName));
			}
			dataList.add(list.toArray());
		}
		XslUtils ex = new XslUtils(response,title, rowsNames, dataList);  
		ex.export();
	}
	
	
	
	public List<Object[]> export(EntityMap param,EntityMap columnInfo,String exportColumns,String shortTableName){
		List<Object[]> dataList=new ArrayList<>();
		
		String[] exportColumnsArr=exportColumns.split(",");
		
		String exportColumnsSql=getExportColumnsSql( shortTableName, exportColumnsArr, columnInfo);
		param.put("exportColumnsSql", exportColumnsSql);
		log.info("exportparam:"+param);
		
		Integer index=1;
		param.pageNumAddOne();
		List<EntityMap> dbEntityMapList=baseMapper.export(param);
		while(CollectionUtil.isNotEmpty(dbEntityMapList)){
			
			for(EntityMap dbEntityMap:dbEntityMapList){
				ArrayList list=new ArrayList();
				list.add(index++); 
				
				for(Integer columnIndex=0;columnIndex<exportColumnsArr.length;columnIndex++){
					list.add(dbEntityMap.getNotNullStr(exportColumnsArr[columnIndex]));
				}
				dataList.add(list.toArray());
			}
			param.pageNumAddOne();
			dbEntityMapList=baseMapper.export(param);
		}
		return dataList;
	}
	
	
	/**
	 * 根据条件，统计记录调试
	 * @param paramMap
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Long countTotal(HashMap<String,Object> paramMap){
		return baseMapper.countTotal(paramMap);
	}
	
	public static Integer BatchSize=50;
	@SuppressWarnings("unchecked")
	public List batchFindPageListByProperty(Object property){
		
		BaseEntity baseEntity=(BaseEntity)property;
		baseEntity.setPageNum(1);
		baseEntity.setPageSize(BatchSize);
		
		List allList=new ArrayList();
		List list=baseMapper.findPageListByProperty(property);
		while(list!=null && list.size()>0){
			allList.addAll(list);
			
			baseEntity.setPageNum(baseEntity.getPageNum()+1);
			list=baseMapper.findPageListByProperty(property);
		} 
		return allList;
	}
	@SuppressWarnings("unchecked")
	public List batchFindPageList(Object property){
		
		BaseEntity baseEntity=(BaseEntity)property;
		baseEntity.setPageNum(1);
		baseEntity.setPageSize(BatchSize);
		
		List allList=new ArrayList();
		List list=baseMapper.findPageList(property);
		while(list!=null && list.size()>0){
			allList.addAll(list);
			
			baseEntity.setPageNum(baseEntity.getPageNum()+1);
			list=baseMapper.findPageList(property);
		} 
		return allList;
	}
	
	/**
	 * 根据对象属性，计算对象总数
	 * @param property
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Long countTotalByProperty( Object property){
		return baseMapper.countTotalByProperty(property);
	}
	@SuppressWarnings("unchecked")
	public HashMap countTotalMap( Object property){
		return baseMapper.countTotalMap(property);
	}
	@SuppressWarnings("unchecked")
	public Integer deleteByIds(String ids){
		return baseMapper.deleteByIds(ids);
	}
	
	@SuppressWarnings("unchecked")
	public Integer delete(Object obj){
		return baseMapper.delete(obj);
	}
	
	
	
	public List findAllDataItem(
			String userid,String datatype){
		return baseMapper.findAllDataItem(userid, datatype);
	}
	
	public List findAllDataItemByName(
			String userid,String datatype ,String name){
		return baseMapper.findAllDataItemByName(userid, datatype,name);
	}
	public List findAllDataItemByIds(
			String userid,String datatype ,String ids){
		return baseMapper.findAllDataItemByIds(userid, datatype,ids);
	}
	@SuppressWarnings("unchecked")
	public List findListByProperty(Object property){
		return baseMapper.findListByProperty(property);
	}
	public Object findOneByProperty(Object property){
		return baseMapper.findOneByProperty(property);
	}
	
	
	public List findPageList(Object object) {
		if(object!=null && object instanceof EntityMap){
			EntityMap entityMap=(EntityMap)object;
			if(entityMap.get("rowIndex")==null){
				entityMap.setRowIndex();
			}
		}
		return baseMapper.findPageList(object); 
	}
	public Object findOne(Object property){
		return baseMapper.findOne(property);
	}
	public List findList(Object object) {
		return baseMapper.findList(object); 
	}

	public Long countTotal(Object object) {
		return baseMapper.countTotal(object); 
	}
	/**
	 * 根据数据id名字+缓存,将权限数据值设置到结果列表中,
	 * 作用：前端根据权限数据，是否展示功能按钮
	 * @param list
	 * @param map
	 * @param dataIdName
	 */
	public void updateListByMap(List list,Map map,String dataIdName) {
		if(list==null || list.isEmpty()) return;
		if(map==null || map.isEmpty()) return;
		
		for(Object obj:list){
			Object dataIdValue=ReflectUtil.getFieldValue(obj, dataIdName);
			BaseEntity entity=(BaseEntity)obj;
			entity.setModuleRoleRelative(map.get(dataIdValue)); 
		}
		
	}
	public void updateListMapByMap(List list,Map map,String dataIdName) {
		if(list==null || list.isEmpty()) return;
		if(map==null || map.isEmpty()) return;
		
		for(Object obj:list){
			Map entity=(Map)obj;
			Object dataIdValue=entity.get(dataIdName)+"";
			entity.put("moduleRoleRelative", map.get(dataIdValue)); 
		}
		
	}
	
	public Object findMaxOne(Object object){
		return baseMapper.findMaxOne(object);
	}
	public Object sum(Object object){
		return baseMapper.sum(object);
	}
	public List sumList(Object object){
		return baseMapper.sumList(object);
	}
	public Long sumListCount(Object object){
		return baseMapper.sumListCount(object);
	}
	
	public Map<String,EntityMap> toColumnEntityMap(List<EntityMap> entityMapList){
		Map<String,EntityMap> columnEntityMap=new HashMap<>();
		if(CollectionUtil.isEmpty(entityMapList)) return columnEntityMap;
		
		for(EntityMap entityMap:entityMapList){
			columnEntityMap.put(entityMap.getStr("COLUMN_NAME"), entityMap);
		}
		return columnEntityMap;
	}
	
	public ResultMsg checkNeedColumn(EntityMap paramMap,List<EntityMap> entityMapList){
		if(CollectionUtil.isEmpty(entityMapList)) return new ResultMsg("3000","没有字段信息");
		Map<String,List<String>>  columnInfoMap=new HashMap<>();
		columnInfoMap.put("uniqueColumnList", new ArrayList<String>());
		for(EntityMap dbEntityMap:entityMapList){
			String dbColumnName=dbEntityMap.getStr("COLUMN_NAME");
			String IS_NULLABLE=dbEntityMap.getStr("IS_NULLABLE");
			String COLUMN_KEY=dbEntityMap.getStr("COLUMN_KEY");
			String EXTRA=dbEntityMap.getStr("EXTRA");
			
			//检查必填字段
			if(IS_NULLABLE!=null && IS_NULLABLE.toUpperCase().equals("NO")){
				if(COLUMN_KEY!=null && COLUMN_KEY.equals("PRI") && EXTRA!=null && EXTRA.equals("auto_increment")) continue;
				
				Object COLUMN_NAMEValue=paramMap.get(dbColumnName);
				if(COLUMN_NAMEValue==null){
					String COLUMN_COMMENT=dbEntityMap.getStr("COLUMN_COMMENT");
					if(StringUtil.isEmpty(COLUMN_COMMENT)){
						return new ResultMsg("3010",dbColumnName+"不能为空");
					}else{
						String[] comments=COLUMN_COMMENT.trim().split(":");
						return new ResultMsg("3011",comments[0]+"不能为空");
					}
				}
				if(COLUMN_KEY.equalsIgnoreCase("UNI") && !dbColumnName.equalsIgnoreCase("dataId")){
					columnInfoMap.get("uniqueColumnList").add(dbColumnName);	
				}
			}
		}
		return new ResultMsg(ResultCode.Success,columnInfoMap);
	}
	
	public Map<String,List<EntityMap>> getIndexColumnMap(String tableName){
		Map<String,List<EntityMap>> indexColumnMap=new HashMap<>();
		
		EntityMap temp=new EntityMap();
		temp.put("tableName", tableName);
		List<EntityMap> dbEntityMapList=baseMapper.getIndexColumn(temp);
		if(CollectionUtil.isEmpty(dbEntityMapList)){
			return indexColumnMap;
		}
		for(EntityMap indexColumn:dbEntityMapList){
			String Column_name=indexColumn.getStr("Column_name");
			String Key_name=indexColumn.getStr("Key_name");
			List<EntityMap> indexColumnList=indexColumnMap.get(Column_name);
			if(indexColumnList==null){
				indexColumnList=new ArrayList<>();
				indexColumnList.add(indexColumn);
				indexColumnMap.put(Column_name, indexColumnList);
			}else{
				indexColumnList.add(indexColumn);
			}
		}
		return indexColumnMap;
	}
	
	public Boolean checkColumnUnique(Map<String,List<EntityMap>> indexColumnMap,String columnName){
		List<EntityMap> indexColumnList=indexColumnMap.get(columnName);
		if(CollectionUtil.isEmpty(indexColumnList)) return false;
		
		for(EntityMap indexColumn:indexColumnList){
			if(indexColumn.getInt("Non_unique").equals(0)){
				return true;
			}
		}
		return false;
	}
	
	public ResultMsg checkUniqueColumn(EntityMap param,String tableName,String columnName,String COLUMN_COMMENT,String operateType,
			EntityMap columnInfoMap,String idName){
		List<String> uniqueColumnList=getUniqueColumnList(COLUMN_COMMENT);//多个字段联合唯一
		if(uniqueColumnList==null) {
			return new ResultMsg(ResultCode.Success);
		}
		
		uniqueColumnList.add(columnName);
		
		StringBuffer uniqueSql=new StringBuffer();
		
		if(columnInfoMap.get("isValid")!=null){
			uniqueSql.append(" where isValid=1 ");
		}else{
			uniqueSql.append(" where 1=1 ");
		}
		
		if(idName==null){
			return new ResultMsg("603","没有定义自增标识,唯一值检查失败");
		}
		
		
		StringBuffer uniqueColumnNames=new StringBuffer();
		StringBuffer uniqueColumnValues=new StringBuffer();
		uniqueColumnNames.append("[");
		uniqueColumnValues.append("[");
		for(String uniqueColumn:uniqueColumnList){
			EntityMap columnInfo=columnInfoMap.getEntityMap(uniqueColumn);
			if(columnInfo==null){
				return new ResultMsg("6200","唯一字段配置有误,表中没有"+uniqueColumn+"这个字段");
			}
			String uniqueCOLUMN_COMMENT=columnInfo.getStr("COLUMN_COMMENT");
			if(!StringUtil.isEmpty(uniqueCOLUMN_COMMENT)){
				uniqueColumnNames.append(uniqueCOLUMN_COMMENT.split(":")[0]+",");
			}else{
				uniqueColumnNames.append(uniqueColumn+",");
			}
			String uniqueColumn_DATA_TYPE=columnInfo.getStr("DATA_TYPE");
			
			String uniqueColumnValue=param.getStr(uniqueColumn);
			if(StringUtil.isEmpty(uniqueColumnValue)){
				uniqueSql.append(" and "+uniqueColumn+" is null ");
				uniqueColumnValues.append(",");
			}else{
				if(isStringType(uniqueColumn_DATA_TYPE)){
					uniqueSql.append(" and "+uniqueColumn+"='"+uniqueColumnValue+"' ");
				}else{
					uniqueSql.append(" and "+uniqueColumn+"="+uniqueColumnValue+" ");
				}
				uniqueColumnValues.append(uniqueColumnValue+",");
			}
		}
		uniqueColumnNames.delete(uniqueColumnNames.length()-1, uniqueColumnNames.length());
		uniqueColumnValues.delete(uniqueColumnValues.length()-1, uniqueColumnValues.length());
		uniqueColumnNames.append("]");
		uniqueColumnValues.append("]");
		
		
		EntityMap tempEntityMap=new EntityMap();
		tempEntityMap.put(this.selectColumnSql, "select "+idName+" ");
		tempEntityMap.put(this.fromTableSql, " from  "+tableName);
		tempEntityMap.put(this.tableConditionSql, uniqueSql.toString());
		
		EntityMap dbEntityMap2=baseMapper.findOneEntityMap(tempEntityMap);
		if(dbEntityMap2!=null){
			if(operateType.equalsIgnoreCase("add")){
				return new ResultMsg("7020","字段:"+uniqueColumnNames.toString()+",数据值:"+uniqueColumnValues.toString()+"不能重复");
			}else if(!dbEntityMap2.getStr(idName).equals(operateType)){
				return new ResultMsg("7020","字段:"+uniqueColumnNames.toString()+",数据值:"+uniqueColumnValues.toString()+"不能重复"+",和记录id:"+dbEntityMap2.getStr("id")+"的数据重复");
			}
		}
		return new ResultMsg(ResultCode.Success);
	}
	
	/**
	 * 检查字段(必填项、数据类型转换、sql注入检查、唯一值检查)
	 * @param tableName
	 * @param paramMap
	 * @param entityMapList
	 * @param operateType
	 * @return
	 */
	public ResultMsg checkColumn(String tableName,EntityMap paramMap,
			List<EntityMap> entityMapList,EntityMap columnInfoMap,
			String operateType){
		if(CollectionUtil.isEmpty(entityMapList)) return new ResultMsg("3000","没有字段信息");
		
		List<String> notCheckColumnListe=Arrays.asList(new String[]{"id","dataId"});
		String idName=getEntityIdName(columnInfoMap);
		StringBuilder columnNames=new StringBuilder();
		StringBuilder columnValues=new StringBuilder();
		
		
		for(EntityMap dbEntityMap:entityMapList){
			String dbColumnName=dbEntityMap.getStr("COLUMN_NAME");
			String IS_NULLABLE=dbEntityMap.getStr("IS_NULLABLE");
			String COLUMN_KEY=dbEntityMap.getStr("COLUMN_KEY");
			String EXTRA=dbEntityMap.getStr("EXTRA");
			String DATA_TYPE=dbEntityMap.getStr("DATA_TYPE");
			String COLUMN_TYPE=dbEntityMap.getStr("COLUMN_TYPE");
			String COLUMN_COMMENT=dbEntityMap.getStr("COLUMN_COMMENT");
			Object ColumnValue=paramMap.get(dbColumnName);
			String ColumnNameDesc=dbColumnName;
			String[] FunctionExpress=null;//：分隔，1、下标0备注，2、其他功能：根据关键字判断,export:unique():relativeselect()
			
			if(!StringUtil.isEmpty(COLUMN_COMMENT)){
				FunctionExpress=COLUMN_COMMENT.trim().split(":");
				ColumnNameDesc=FunctionExpress[0];
			}
			
			//检查必填字段
			if(operateType.equals("add")){
				if(IS_NULLABLE!=null && IS_NULLABLE.toUpperCase().equals("NO")){
					if(COLUMN_KEY!=null && COLUMN_KEY.equals("PRI") && EXTRA!=null && EXTRA.equals("auto_increment")) continue;
					if(ColumnValue==null){
						return new ResultMsg("3010",ColumnNameDesc+"不能为空");
					}
				}	
			}
			
			
			if(ColumnValue!=null){
				//数据类型转换
				ResultMsg columnTypeConvertMsg=columnTypeConvert(paramMap,dbColumnName,ColumnNameDesc,DATA_TYPE,COLUMN_TYPE,ColumnValue);
				if(!columnTypeConvertMsg.getCode().equals(ResultCode.Success.getCode().toString())){
					return columnTypeConvertMsg;
				}
				//检查sql注入
				/*ResultMsg invalidSqlMsg=inValidSql(ColumnValue.toString());
				if(!invalidSqlMsg.getCode().equals(ResultCode.Success.getCode().toString())){
					return invalidSqlMsg;
				}*/
				
				if(isStringType(DATA_TYPE)){
					String columnStrValue=ColumnValue.toString().replace("'", "''");
					columnValues.append("'"+columnStrValue+"',");
				}else{
					columnValues.append(ColumnValue.toString()+",");
				}
				columnNames.append(dbColumnName+",");
				
			}
			
			//检查字段唯一值
			if(notCheckColumnListe.contains(dbColumnName.toLowerCase())) continue;
			ResultMsg checkUniqueColumnMsg=checkUniqueColumn(paramMap,tableName, dbColumnName, COLUMN_COMMENT, operateType,columnInfoMap,idName);
			if(!checkUniqueColumnMsg.getCode().equals(ResultCode.Success.getCode().toString())){
				return checkUniqueColumnMsg;
			}
		}
		
		EntityMap resultMap=new EntityMap();
		resultMap.put("columnNames", columnNames);
		resultMap.put("columnValues", columnValues);
		resultMap.put("idName", idName);
		return new ResultMsg(ResultCode.Success,resultMap);
	}
	
	/**
	 * 类型转换
	 * @param paramMap
	 * @param dbColumnName
	 * @param ColumnNameDesc
	 * @param DATA_TYPE
	 * @param COLUMN_TYPE
	 * @param ColumnValue
	 * @return
	 */
	public ResultMsg columnTypeConvert(EntityMap paramMap,String dbColumnName,String ColumnNameDesc,String DATA_TYPE,String COLUMN_TYPE,Object ColumnValue){
		
		try{
			if(DATA_TYPE.equals("int")){
				paramMap.put(dbColumnName, Integer.parseInt(ColumnValue.toString()));	
			}else if(DATA_TYPE.equals("tinyint")){//-128dao127,0-255
				Integer data=Integer.parseInt(ColumnValue.toString());
				if(data>127){
					return new ResultMsg("4002",ColumnNameDesc+"数据值超过限制:值在127内");
				}
				paramMap.put(dbColumnName, data);	
			}else if(DATA_TYPE.equals("smallint")){// 32767,0-65536
				Integer data=Integer.parseInt(ColumnValue.toString());
				if(data>32767){
					return new ResultMsg("4002",ColumnNameDesc+"数据值超过限制:值在32767内");
				}
				paramMap.put(dbColumnName, data);	
			}else if(DATA_TYPE.equals("integer")){
				paramMap.put(dbColumnName, Integer.parseInt(ColumnValue.toString()));	
			}else if(DATA_TYPE.equals("bigint")){
				paramMap.put(dbColumnName, Long.parseLong(ColumnValue.toString()));	
			}else if(DATA_TYPE.equals("decimal")){
				String[] datatypeLen=COLUMN_TYPE.replace("decimal", "").replace("(", "").replace(")", "").split(",");
				double dataValue=paramMap.getDouble(dbColumnName,Integer.parseInt(datatypeLen[1]));
				paramMap.put("dbColumnName", dataValue);
			}else if(DATA_TYPE.equals("double")){
				String[] datatypeLen=COLUMN_TYPE.replace("decimal", "").replace("(", "").replace(")", "").split(",");
				double dataValue=paramMap.getDouble(dbColumnName,Integer.parseInt(datatypeLen[1]));
				paramMap.put("dbColumnName", dataValue);
			}else if(DATA_TYPE.equals("datetime")){
				String dateStr=paramMap.getDateStrFromStr(dbColumnName, "yyyy-MM-dd HH:mm:ss");
				if(dateStr==null){
					return new ResultMsg("4002",ColumnNameDesc+"数据类型转换异常:"+ColumnValue+"->"+COLUMN_TYPE);
				}
				paramMap.put(dbColumnName, dateStr);
			}else if(DATA_TYPE.equals("date")){
				String dateStr=paramMap.getDateStrFromStr(dbColumnName, "yyyy-MM-dd");
				if(dateStr==null){
					return new ResultMsg("4003",ColumnNameDesc+"数据类型转换异常:"+ColumnValue+"->"+COLUMN_TYPE);
				}
				paramMap.put(dbColumnName, dateStr);
			}else if(DATA_TYPE.equals("bit")){
				Integer data=Integer.parseInt(ColumnValue.toString());
				if(data.equals("0")){
					paramMap.put(dbColumnName, 0);
				}else{
					paramMap.put(dbColumnName, 1);
				}
			}else if(DATA_TYPE.equals("varchar")){
				String datatypeLen=COLUMN_TYPE.replace("varchar", "").replace("(", "").replace(")", "");
				String ColumnValueStr=ColumnValue.toString().trim();
				if(ColumnValueStr.length()>Integer.parseInt(datatypeLen)){
					return new ResultMsg("4004",ColumnNameDesc+"数据长度超过限制:长度在"+datatypeLen+"个字符内");
				}
				paramMap.put(dbColumnName, ColumnValueStr);
			}else if(DATA_TYPE.equals("text")){
				paramMap.put(dbColumnName, ColumnValue.toString());
			}else if(DATA_TYPE.equals("mediumtext")){
				paramMap.put(dbColumnName, ColumnValue.toString());
			}else if(DATA_TYPE.equals("longtext")){
				paramMap.put(dbColumnName, ColumnValue.toString());
			}
			String columnStrValue=ColumnValue.toString();
			if(columnStrValue.contains("'")){
				paramMap.put(dbColumnName, columnStrValue.replace("'", "''"));
			}
		}catch(Exception ex){
			return new ResultMsg("4001",ColumnNameDesc+"数据类型转换异常:"+ColumnValue+"->"+COLUMN_TYPE);
		}
		return new ResultMsg(ResultCode.Success);
	}
	
	
	public ResultMsg queryConditionCheck(EntityMap paramMap,
			List<EntityMap> entityMapList){
		if(CollectionUtil.isEmpty(entityMapList)) return new ResultMsg("3000","没有字段信息");
		
		for(EntityMap dbEntityMap:entityMapList){
			String dbColumnName=dbEntityMap.getStr("COLUMN_NAME");
			String IS_NULLABLE=dbEntityMap.getStr("IS_NULLABLE");
			String COLUMN_KEY=dbEntityMap.getStr("COLUMN_KEY");
			String EXTRA=dbEntityMap.getStr("EXTRA");
			String DATA_TYPE=dbEntityMap.getStr("DATA_TYPE");
			String COLUMN_TYPE=dbEntityMap.getStr("COLUMN_TYPE");
			String COLUMN_COMMENT=dbEntityMap.getStr("COLUMN_COMMENT");
			Object ColumnValue=paramMap.get(dbColumnName);
			String ColumnNameDesc=dbColumnName;
			String[] FunctionExpress=null;//：分隔，1、下标0备注，2、其他功能：根据关键字判断,export:unique():relativeselect()
			
			if(!StringUtil.isEmpty(COLUMN_COMMENT)){
				FunctionExpress=COLUMN_COMMENT.trim().split(":");
				ColumnNameDesc=FunctionExpress[0];
			}
			if(ColumnValue!=null){
				ResultMsg columnTypeConvertMsg=columnTypeConvert(paramMap,dbColumnName,ColumnNameDesc,DATA_TYPE,COLUMN_TYPE,ColumnValue);
				if(!columnTypeConvertMsg.getCode().equals(ResultCode.Success.getCode().toString())){
					return columnTypeConvertMsg;
				}
				//检查sql注入
				ResultMsg invalidSqlMsg=inValidSql(ColumnValue.toString());
				if(!invalidSqlMsg.getCode().equals(ResultCode.Success.getCode().toString())){
					return invalidSqlMsg;
				}
			}
			
		}
		return new ResultMsg(ResultCode.Success);
	}
	
	public ResultMsg getAllTableColumnCondition(EntityMap columnInfoMap,EntityMap param){
		
		String mainTableShortName="t0.";
		
		StringBuilder allTableConditionSql=new StringBuilder();
		
		String conditionSqlHeader=null;
		EntityMap isValidColumn=columnInfoMap.getEntityMap("isValid");
		if(isValidColumn==null){
			conditionSqlHeader=" where 1=1 ";
		}else{
			conditionSqlHeader=" where "+mainTableShortName+"isValid=1 ";
		}
		
		allTableConditionSql.append(conditionSqlHeader);
		List paramKeys=Arrays.asList(param.keySet().toArray());
		
		for(Object paramNameObj:paramKeys){
			if(paramNameObj==null) continue;
			String paramName=paramNameObj.toString();
			
			if(paramName.equals("pageNum")) continue;
			if(paramName.equals("pageSize")) continue;
			
			Object paramValue=param.get(paramName);
			if(paramValue==null) continue;
			
			EntityMap columnInfo=columnInfoMap.getEntityMap(paramName);
			String startColumnName=null;//范围查询，开始
			String endColumnName=null;//范围查询，结束
			String likeColumnName=null;//模糊
			String inValuesColumnName=null;//多个值
			
			if(columnInfo==null){//时间或者数字Start,时间或者数字End,Like,InValues
				startColumnName=StringUtil.removeKeyWordAtEnd(paramName,"Start");
				if(startColumnName.length()<paramName.length()){
					columnInfo=columnInfoMap.getEntityMap(startColumnName);
				}else{
					startColumnName=null;
					endColumnName=StringUtil.removeKeyWordAtEnd(paramName,"End");
					if(endColumnName.length()<paramName.length()){
						columnInfo=columnInfoMap.getEntityMap(endColumnName);
					}else{
						endColumnName=null;
						likeColumnName=StringUtil.removeKeyWordAtEnd(paramName,"Like");
						if(likeColumnName.length()<paramName.length()){
							columnInfo=columnInfoMap.getEntityMap(likeColumnName);
						}else{
							likeColumnName=null;
							inValuesColumnName=StringUtil.removeKeyWordAtEnd(paramName,"InValues");
							if(inValuesColumnName.length()<paramName.length()){
								columnInfo=columnInfoMap.getEntityMap(inValuesColumnName);
							}else{
								inValuesColumnName=null;
							}
						}
					}
				}
			}
			if(columnInfo==null) continue;
			
			String DATA_TYPE=columnInfo.getStr("DATA_TYPE");
			String COLUMN_TYPE=columnInfo.getStr("COLUMN_TYPE");
			String ColumnNameDesc=columnInfo.getStr("columnRemark");
			
			if(inValuesColumnName==null){
				ResultMsg columnTypeConvertMsg=columnTypeConvert(param,paramName,ColumnNameDesc,DATA_TYPE,COLUMN_TYPE,paramValue);
				if(!columnTypeConvertMsg.getCode().equals(ResultCode.Success.getCode().toString())){
					return columnTypeConvertMsg;
				}
			}
			
			
			String paramStrValue=paramValue.toString();
			if(paramStrValue.contains("'")){
				paramValue=paramStrValue.replace("'", "''");
			}
			//检查sql注入
			/*ResultMsg invalidSqlMsg=inValidSql(paramValue.toString());
			if(!invalidSqlMsg.getCode().equals(ResultCode.Success.getCode().toString())){
				return invalidSqlMsg;
			}*/
			
			String condditionColumnName=mainTableShortName+paramName;
			if(paramName.contains(".")){
				condditionColumnName=paramName;
			}
			if(startColumnName!=null){
				if(!startColumnName.contains(".")){
					startColumnName=mainTableShortName+startColumnName;
				}
				allTableConditionSql.append(" and "+startColumnName+" >='"+paramValue+"' ");
			}else if(endColumnName!=null){
				if(!endColumnName.contains(".")){
					endColumnName=mainTableShortName+endColumnName;
				}
				allTableConditionSql.append(" and "+endColumnName+" <='"+paramValue+"' ");
			}else if(likeColumnName!=null){
				if(!likeColumnName.contains(".")){
					likeColumnName=mainTableShortName+likeColumnName;
				}
				allTableConditionSql.append(" and "+likeColumnName+" like '%"+paramValue+"%' ");
			}else if(inValuesColumnName!=null){
				if(!inValuesColumnName.contains(".")){
					inValuesColumnName=mainTableShortName+inValuesColumnName;
				}
				if(isStringType(DATA_TYPE)){
					allTableConditionSql.append(" and "+inValuesColumnName+" in("+StringUtils.wordsToSqlWords(paramValue.toString())+") ");
				}else{
					allTableConditionSql.append(" and "+inValuesColumnName+" in("+paramValue+") ");
				}
			}else if(isStringType(DATA_TYPE)){
				allTableConditionSql.append(" and "+condditionColumnName+" ='"+paramValue+"' ");
			}else{
				allTableConditionSql.append(" and "+condditionColumnName+" ="+paramValue+" ");
			}
		}
		return new ResultMsg(ResultCode.Success,allTableConditionSql.toString());
	}
	
	public Boolean isStringType(String DATA_TYPE){
		List<String> StringTypeList=Arrays.asList(new String[]{"datetime","date","varchar","text","mediumtext","longtext"});
		return StringTypeList.contains(DATA_TYPE);
	}
	
	public Boolean isNumberType(String DATA_TYPE){
		List<String> NumberTypeList=Arrays.asList(new String[]{
				"int","tinyint","smallint","integer","bigint","longtext",
				"decimal","double"});
		return NumberTypeList.contains(DATA_TYPE);
	}
	
	public ResultMsg convertColumnType(EntityMap paramMap,String dbColumnName,String DATA_TYPE,String COLUMN_TYPE,Object COLUMN_NAMEValue,String COLUMN_COMMENT){
		try{
			if(DATA_TYPE.equals("int")){
				paramMap.put(dbColumnName, Integer.parseInt(COLUMN_NAMEValue.toString()));	
			}else if(DATA_TYPE.equals("tinyint")){
				paramMap.put(dbColumnName, Integer.parseInt(COLUMN_NAMEValue.toString()));	
			}else if(DATA_TYPE.equals("smallint")){
				paramMap.put(dbColumnName, Integer.parseInt(COLUMN_NAMEValue.toString()));	
			}else if(DATA_TYPE.equals("integer")){
				paramMap.put(dbColumnName, Integer.parseInt(COLUMN_NAMEValue.toString()));	
			}else if(DATA_TYPE.equals("bigint")){
				paramMap.put(dbColumnName, Long.parseLong(COLUMN_NAMEValue.toString()));	
			}else if(DATA_TYPE.equals("decimal")){
				String[] datatypeLen=COLUMN_TYPE.replace("decimal", "").replace("(", "").replace(")", "").split(",");
				double dataValue=paramMap.getDouble(dbColumnName,Integer.parseInt(datatypeLen[1]));
				paramMap.put("dbColumnName", dataValue);
			}else if(DATA_TYPE.equals("double")){
				String[] datatypeLen=COLUMN_TYPE.replace("decimal", "").replace("(", "").replace(")", "").split(",");
				double dataValue=paramMap.getDouble(dbColumnName,Integer.parseInt(datatypeLen[1]));
				paramMap.put("dbColumnName", dataValue);
			}else if(DATA_TYPE.equals("datetime")){
				String dateStr=paramMap.getDateStrFromStr(dbColumnName, "yyyy-MM-dd HH:mm:ss");
				if(dateStr==null){
					return new ResultMsg("4002",COLUMN_COMMENT+"数据类型转换异常:"+COLUMN_NAMEValue+"->"+COLUMN_TYPE);
				}
				paramMap.put(dbColumnName, dateStr);
			}else if(DATA_TYPE.equals("date")){
				String dateStr=paramMap.getDateStrFromStr(dbColumnName, "yyyy-MM-dd");
				if(dateStr==null){
					return new ResultMsg("4003",COLUMN_COMMENT+"数据类型转换异常:"+COLUMN_NAMEValue+"->"+COLUMN_TYPE);
				}
				paramMap.put(dbColumnName, dateStr);
			}else if(DATA_TYPE.equals("bit")){
				Integer data=Integer.parseInt(COLUMN_NAMEValue.toString());
				if(data.equals("0")){
					paramMap.put(dbColumnName, 0);
				}else{
					paramMap.put(dbColumnName, 1);
				}
			}else if(DATA_TYPE.equals("varchar")){
				String datatypeLen=COLUMN_TYPE.replace("varchar", "").replace("(", "").replace(")", "");
				String COLUMN_NAMEValueStr=COLUMN_NAMEValue.toString().trim();
				if(COLUMN_NAMEValueStr.length()>Integer.parseInt(datatypeLen)){
					return new ResultMsg("4004",COLUMN_COMMENT+"数据长度超过限制:长度在"+datatypeLen+"内");
				}
				paramMap.put(dbColumnName, COLUMN_NAMEValueStr);
			}else if(DATA_TYPE.equals("text")){
				paramMap.put(dbColumnName, COLUMN_NAMEValue.toString());
			}else if(DATA_TYPE.equals("mediumtext")){
				paramMap.put(dbColumnName, COLUMN_NAMEValue.toString());
			}else if(DATA_TYPE.equals("longtext")){
				paramMap.put(dbColumnName, COLUMN_NAMEValue.toString());
			}
		}catch(Exception ex){
			return new ResultMsg("4001",COLUMN_COMMENT+"数据类型转换异常:"+COLUMN_NAMEValue+"->"+COLUMN_TYPE);
		}
		return new ResultMsg(ResultCode.Success);
	}
	
	
	public Map<Integer,Integer> getNotSplitColumnIndexsMap(Integer needSplitInfo,EntityMap configMap){
		Map<Integer,Integer> notSplitColumnIndexsMap=new HashMap<>();
		
		String notSplitColumnIndexs=configMap.getStr("notSplitColumnIndexs");
		
		if(needSplitInfo!=null && needSplitInfo==1){
			if(StringUtil.isNotEmpty(notSplitColumnIndexs)){
				String[] notSplitColumnIndexsArr=notSplitColumnIndexs.split(",");
				if(notSplitColumnIndexsArr!=null){
					for(String notSplitColumnIndexStr:notSplitColumnIndexsArr){
						try{
							Integer notSplitColumnIndex=Integer.parseInt(notSplitColumnIndexStr);
							notSplitColumnIndexsMap.put(notSplitColumnIndex,notSplitColumnIndex);	
						}catch(Exception ex){
							log.info("跳过无效的notSplitColumnIndex");;
						}
						
					}
				}
			}
		}
		
		
		return notSplitColumnIndexsMap;
	}
	
	public static void main(String[] args){
		System.out.println("a/b".split("/").length);
	}
	
	public List<EntityMap> getColumnValueMap(EntityMap configMap,List<EntityMap> sourceTableColumnMapList,
			Map<String,String> businessDataMap,Integer parseColumnStartIndex,
			String dataId){
		List<EntityMap> columnValueList=new ArrayList<>();
		if(sourceTableColumnMapList==null || sourceTableColumnMapList.size()<parseColumnStartIndex) {
			log.info("column个数有误");
			return null;
		}
		
		Integer needSplitInfo=configMap.getInt("needSplitInfo");
		
		
		if(needSplitInfo==null || needSplitInfo == 0){
			EntityMap columnValueMap=new EntityMap();
			Integer key=1;
			
			for(Integer i=parseColumnStartIndex-1;i<sourceTableColumnMapList.size();i++){
				EntityMap columnMap=sourceTableColumnMapList.get(i);
				String dbColumnName=columnMap.getStr("COLUMN_NAME");
				String DATA_TYPE=columnMap.getStr("DATA_TYPE");
				String columnValue=businessDataMap.get(""+key);
				
				if(DATA_TYPE.equalsIgnoreCase("datetime") && StringUtil.isNotEmpty(columnValue)){
					columnValue=DateUtil.formatDate(columnValue, "yyyyMMddHHmmss","yyyy-MM-dd HH:mm:ss");
				}
				
				
				columnValueMap.put(dbColumnName, columnValue);
				key++;
			}
			
			columnValueMap.put("createTime", DateUtil.formatDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
			columnValueMap.put("modifyTime", DateUtil.formatDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
			columnValueMap.put("dataId", dataId);
			columnValueList.add(columnValueMap);
			return columnValueList;
		}
		
		
		if(needSplitInfo==1){
			
			Map<Integer,Integer> notSplitColumnIndexsMap=getNotSplitColumnIndexsMap(needSplitInfo,configMap);
			
			
			String splitByChar=configMap.getStr("splitByChar");
			if(StringUtil.isEmpty(splitByChar)){
				return null;	
			}
			
			
			Integer splitNum=0;
			for(Integer i=1;i<businessDataMap.size();i++){
				String splitcolumnValue=businessDataMap.get(""+i);
				if(StringUtil.isNotEmpty(splitcolumnValue) && splitcolumnValue.contains("/")){
					splitNum=splitcolumnValue.split("/").length;
					break;
				}
			}
			
			if(splitNum<=0){
				return null;
			}
			for(Integer rowIndex=0;rowIndex<splitNum;rowIndex++){
				EntityMap columnValueMap=new EntityMap();
				Integer key=1;
				
				for(Integer i=parseColumnStartIndex-1;i<sourceTableColumnMapList.size();i++){
					EntityMap columnMap=sourceTableColumnMapList.get(i);
					String dbColumnName=columnMap.getStr("COLUMN_NAME");
					String DATA_TYPE=columnMap.getStr("DATA_TYPE");
					String columnValue=businessDataMap.get(""+key);
					
										
					
					if(notSplitColumnIndexsMap.get(i+1)==null 
							&&StringUtil.isNotEmpty(columnValue)){
						try{
							String[] columnValueArr=columnValue.split(splitByChar);
							columnValue=columnValueArr[rowIndex];	
						}catch(Exception ex){
							ex.printStackTrace();
						}
					}
					
					if(DATA_TYPE.equalsIgnoreCase("datetime") && StringUtil.isNotEmpty(columnValue)){
						columnValue=DateUtil.formatDate(columnValue, "yyyyMMddHHmmss","yyyy-MM-dd HH:mm:ss");
					}
					
					columnValueMap.put(dbColumnName, columnValue);
					key++;
				}
				columnValueMap.put("createTime", DateUtil.formatDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
				columnValueMap.put("modifyTime", DateUtil.formatDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
				columnValueMap.put("dataId", dataId);
				columnValueMap.put("subDataId", MD5Util.encrypt(columnValueMap.toString()));
				
				columnValueList.add(columnValueMap);
				
			}
		}
		
		
		return columnValueList;
	}
	
	private void saveParseDeviceData(String tableName,List<EntityMap> saveSqlList){
		if(CollectionUtils.isNotEmpty(saveSqlList)){
			for(EntityMap columnValueMap:saveSqlList){
				String insertSQL=SQLUtil.generateInsertSQL(tableName, columnValueMap);
				if(StringUtil.isNotEmpty(insertSQL)){
					try{
						baseMapper.executeSql(insertSQL);
					}catch(Exception ex){
						ex.printStackTrace();
						log.info("通用生成历史数据-执行批量保存时候异常:"+StringUtil.getExceptionDesc(ex));
						log.info("通用生成历史数据-执行批量保存转单个保存开始");
						
					}
				}
			}
		}
	}
	
	public void parseDeviceData(EntityMap configMap,EntityMap deviceDataMap){
		try{
			
			String businessData=deviceDataMap.getStr("businessData");
			if(StringUtil.isEmpty(businessData)){
				log.info("businessData为空");
				return;
			}
			
			Map<String,String> businessDataMap=TcpProtocolUtil.getBusinessDataMap(businessData);
			if(businessDataMap==null){
				log.info("没有有效的businessData");
				return;
			}
			
			String tableName=configMap.getStr("parseSavetoTableName");
			if(StringUtil.isEmpty(tableName)){
				log.info("没有配置解析表");
				return;
			}
			if(baseMapper.existTableCount(tableName)<=0){//是否存在源表
				log.info("系统不存在表:"+tableName);
				return;
			}
			
			Integer parseColumnStartIndex=configMap.getInt("parseColumnStartIndex");
			if(parseColumnStartIndex==null || parseColumnStartIndex<1){
				log.info("解析字段的开始位置无效parseColumnStartIndex");
				return;
			}
			List<EntityMap> sourceTableColumnMapList=baseMapper.getTableColumnInfo(tableName);
			
			String dataId=deviceDataMap.getStr("dataId");
			List<EntityMap> columnValueMapList=getColumnValueMap(configMap,sourceTableColumnMapList,businessDataMap,parseColumnStartIndex,dataId);
			
			
			saveParseDeviceData(tableName,columnValueMapList);
			
		}catch(Exception ex){
			ex.printStackTrace();
			log.info("解析业务数据异常:"+StringUtil.getExceptionDesc(ex));
		}
		
	}
	
	
	
	
	 public static String lowerFirstCapse(String str) {
	        char[] chars = str.toCharArray();
	        chars[0] += 32;
	        return String.valueOf(chars);
	}
}
