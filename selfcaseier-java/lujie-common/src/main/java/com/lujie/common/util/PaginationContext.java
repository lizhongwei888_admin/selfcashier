package com.lujie.common.util;

/**
 * 分页工具类
 * @author hupei
 *
 */
public class PaginationContext {
    private static ThreadLocal<Integer> pageNum = new ThreadLocal<Integer>();    // 保存第几页
    private static ThreadLocal<Integer> pageSize = new ThreadLocal<Integer>();    // 保存每页记录条数
    private static ThreadLocal<String> orderField = new ThreadLocal<String>();    // 排序关键字
    private static ThreadLocal<String> orderDirection = new ThreadLocal<String>();    // 升序或者降序

    /*
     * pageNum ：get、set、remove
     */
    public static int getPageNum() {
        Integer pn = pageNum.get();
        if (pn == null) {
            return 0;
        }
        return pn;
    }

    public static void setPageNum(int pageNumValue) {
        pageNum.set(pageNumValue);
    }

    public static void removePageNum() {
        pageNum.remove();
    }

    /*
     * pageSize ：get、set、remove
     */
    public static int getPageSize() {
        Integer ps = pageSize.get();
        if (ps == null) {
            return 0;
        }
        return ps;
    }

    public static void setPageSize(int pageSizeValue) {
        pageSize.set(pageSizeValue);
    }

    public static void removePageSize() {
        pageSize.remove();
    }
    
    /*
     * orderField ：get、set、remove
     */
    public static String getOrderField() {
        String ps = orderField.get();
        if (ps == null) {
            return "id";
        }
        return ps;
    }

    public static void setOrderField(String orderFieldValue) {
    	orderField.set(orderFieldValue);
    }

    public static void removeOrderField() {
    	orderField.remove();
    }
    
    /*
     * orderDirection ：get、set、remove
     */
    public static String getOrderDirection() {
        String ps = orderDirection.get();
        if (ps == null) {
            return "asc";
        }
        return ps;
    }

    public static void setOrderDirection(String orderDirectionValue) {
    	orderDirection.set(orderDirectionValue);
    }

    public static void removeOrderDirection() {
    	orderDirection.remove();
    }
}