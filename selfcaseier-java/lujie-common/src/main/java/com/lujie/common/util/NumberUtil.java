package com.lujie.common.util;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <pre>
 * 数字处理
 * </pre>
 */
public class NumberUtil {
	private final static String[] CN_UNIT_CAPITAL = {"拾","佰","仟","萬","亿"};
	
//	private final static String[] UNIT_CHINESE_NORMAL = {"十","百","千","万","亿"};
	
	private final static String[] CN_NUMBER_CAPITAL = {"零","壹","贰","叁","肆","伍","陆","柒","捌","玖"};
	
//	private final static String[] CN_NUMBER_NORMAL = {"零","一","二","三","四","五","六","七","八","九","十"};
	
	private final static String CN_FULL = "整";
	
	private static final String CN_NEGATIVE = "负";
	
	public static String NumberToCapital(Integer number){
		return null;
	}
	
	public static String NumberToCapital(Long number){
		return null;
	}
	
	public static String NumberToCapital(Double number){
		return null;
	}
	
	public static String NumberToCapital(BigDecimal number){
		return null;
	}
	
	//目前的移动号段：139、138、137、136、135、134、147、150、151、152、157、158、159、172、178、182、183、184、187、188、198.
		//联通号段：130、131、132、140、145、146、155、156、166、167、185、186、145、175、176
		//电信号段：133、149、153、177、173、180、181、189、191、199
		
	public static boolean isMobile(String mobile) {
        String regex = "^((13[0-9])|(14[5,7,9])|(15([0-3]|[5-9]))|(16[5,6])|(17[0-8])|(18[0-9])|(19[1、5、8、9]))\\d{8}$";
        Pattern p = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(mobile);
        return m.matches();
    }
	public static Double getDoubleNotNull(Double data) {
        if(data==null) return 0.0;
        return data;
    }
	public static void main(String[] args){
		//System.out.println(urlMatch("/upload/test"));
		System.out.println(containStr("23张三44"));
	}
	public static boolean urlMatch(String url) {
        String regex = "/upload/.*";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(url);
        return m.matches();
    }
	
	public static boolean containStr(String str) {
        String regex = "^(.*张三.*)$";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(str);
        return m.matches();
    }
	
	
}

