package com.lujie.common.util;

import java.util.Date;

import org.jsoup.internal.StringUtil;
import org.springframework.core.convert.converter.Converter;

public class DateTimeConverter implements Converter<String, Date>{

    /* (non-Javadoc)
     * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
     */
    @Override
    public Date convert(String source) {
        if(StringUtil.isBlank(source)) {
            return null;
        }
        return DateUtil.parserDate(source);
    }

}
