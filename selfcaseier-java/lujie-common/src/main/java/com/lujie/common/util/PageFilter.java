package com.lujie.common.util;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 分页过滤器
 * @author hupei
 *
 */
public class PageFilter implements Filter {
	private static final Logger log = LoggerFactory.getLogger(PageFilter.class);
	public PageFilter() {}

    public void destroy() {}

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest httpRequest = (HttpServletRequest) request;

        PaginationContext.setPageNum(getPageNum(httpRequest));
        PaginationContext.setPageSize(getPageSize(httpRequest));
        PaginationContext.setOrderField(getOrderField(httpRequest));
        PaginationContext.setOrderDirection(getOrderDirection(httpRequest));

        try {
            chain.doFilter(request, response);
        }catch(Exception e){
        	log.error("发生异常:"+e);
        }
        // 使用完Threadlocal，将其删除。使用finally确保一定将其删除
        finally {
            PaginationContext.removePageNum();
            PaginationContext.removePageSize();
        }
    }

    /**
     * 获得pager.offset参数的值
     * 
     * @param request
     * @return
     */
    protected int getPageNum(HttpServletRequest request) {
        int pageNum = 1;
        try {
            String pageNums = request.getParameter("pageNum");
            if (pageNums != null && StringUtils.isNumeric(pageNums)) {
                pageNum = Integer.parseInt(pageNums);
            }else {
            	pageNums = request.getParameter("page");
            	if (pageNums != null && StringUtils.isNumeric(pageNums)) {
                    pageNum = Integer.parseInt(pageNums);
                }
            }
        } catch (NumberFormatException e) {
            log.error("类型转换错误："+e);
        }
        return pageNum;
    }

    /**
     * 设置默认每页大小
     * 
     * @return
     */
    protected int getPageSize(HttpServletRequest request) {
        int pageSize = 15;    // 默认每页10条记录
        try {
            String pageSizes = request.getParameter("pageSize");
            if (pageSizes != null && StringUtils.isNumeric(pageSizes)) {
                pageSize = Integer.parseInt(pageSizes);
            }else{
            	pageSizes = request.getParameter("limit");
            	if (pageSizes != null && StringUtils.isNumeric(pageSizes)) {
                    pageSize = Integer.parseInt(pageSizes);
                }
            }
        } catch (NumberFormatException e) {
        	log.error("类型转换错误："+e);
        }
        return pageSize;
    }
    
    /**
     * 设置排序关键字
     * 
     * @return
     */
    protected String getOrderField(HttpServletRequest request) {
        String orderField = "id";    // 默认id
        String orderFields = request.getParameter("orderField");
        if (orderFields != null) {
        	orderField = orderFields;
        }
        return orderField;
    }
    
    /**
     * 设置升序或者降序
     * 
     * @return
     */
    protected String getOrderDirection(HttpServletRequest request) {
        String orderDirection = "desc";    // 默认asc
        String orderDirections = request.getParameter("orderDirection");
        if (orderDirections != null) {
        	orderDirection = orderDirections;
        }
        return orderDirection;
    }

    /**
     * @see Filter#init(FilterConfig)
     */
    public void init(FilterConfig fConfig) throws ServletException {}

}
