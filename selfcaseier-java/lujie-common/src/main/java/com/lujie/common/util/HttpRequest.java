package com.lujie.common.util;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HttpRequest {
	private static final Logger log = LoggerFactory.getLogger(HttpRequest.class);

	/**
     * 向指定URL发送GET方法的请求
     * 
     * @param url
     *            发送请求的URL
     * @param param
     *            请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
     * @return URL 所代表远程资源的响应结果
     * @throws Exception 
     */
    public static String sendGet(String url, String param) throws Exception {
        String result = "";
        BufferedReader in = null;
        String urlNameString = url;
        if(param!=null && !param.equals("")){
        	urlNameString+="?" + param;
        }
        URL realUrl = new URL(urlNameString);
        // 打开和URL之间的连接
        URLConnection connection = realUrl.openConnection();
        // 设置通用的请求属性
        connection.setRequestProperty("accept", "*/*");
        connection.setRequestProperty("connection", "Keep-Alive");
        connection.setRequestProperty("user-agent",
                "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
        // 建立实际的连接
        connection.connect();
        // 获取所有响应头字段
        Map<String, List<String>> map = connection.getHeaderFields();
        // 遍历所有的响应头字段
        for (String key : map.keySet()) {
            System.out.println(key + "--->" + map.get(key));
        }
        // 定义 BufferedReader输入流来读取URL的响应
        in = new BufferedReader(new InputStreamReader(
                connection.getInputStream()));
        String line;
        while ((line = in.readLine()) != null) {
            result += line;
        }
        // 使用finally块来关闭输入流
        if (in != null) {
            in.close();
        }
        
        return result;
    }
    
    public static String sendGet(HashMap<String,String> requestHeaderMap,String url, String param) throws Exception {
        String result = "";
        BufferedReader in = null;
        String urlNameString = url;
        if(param!=null && !param.equals("")){
        	urlNameString+="?" + param;
        }
        URL realUrl = new URL(urlNameString);
        // 打开和URL之间的连接
        URLConnection connection = realUrl.openConnection();
        // 设置通用的请求属性
        connection.setRequestProperty("accept", "*/*");
        connection.setRequestProperty("connection", "Keep-Alive");
        connection.setRequestProperty("user-agent",
                "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
        
        if(requestHeaderMap!=null && !requestHeaderMap.isEmpty()){
       	 Iterator<String> reqheadKeys=requestHeaderMap.keySet().iterator();
            while(reqheadKeys.hasNext()){
           	 String key=reqheadKeys.next();
           	 String value=requestHeaderMap.get(key);
           	connection.setRequestProperty(key, value);
            }	 
        }
        
        // 建立实际的连接
        connection.connect();
        // 获取所有响应头字段
        Map<String, List<String>> map = connection.getHeaderFields();
        // 遍历所有的响应头字段
        for (String key : map.keySet()) {
            System.out.println(key + "--->" + map.get(key));
        }
        // 定义 BufferedReader输入流来读取URL的响应
        in = new BufferedReader(new InputStreamReader(
                connection.getInputStream(),"utf-8"));
        String line;
        while ((line = in.readLine()) != null) {
            result += line;
        }
        // 使用finally块来关闭输入流
        if (in != null) {
            in.close();
        }
        
        return result;
    }
    
    public static String sendGetV2(HashMap<String,String> requestHeaderMap,String url, String param) throws Exception {
        String result = "";
        BufferedReader in = null;
        String urlNameString = url;
        if(param!=null && !param.equals("")){
        	urlNameString+="?" + param;
        }
        URL realUrl = new URL(urlNameString);
        // 打开和URL之间的连接
        URLConnection connection = realUrl.openConnection();
        // 设置通用的请求属性
        connection.setRequestProperty("accept", "*/*");
        connection.setRequestProperty("connection", "Keep-Alive");
        connection.setRequestProperty("user-agent",
                "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
        
        if(requestHeaderMap!=null && !requestHeaderMap.isEmpty()){
       	 Iterator<String> reqheadKeys=requestHeaderMap.keySet().iterator();
            while(reqheadKeys.hasNext()){
           	 String key=reqheadKeys.next();
           	 String value=requestHeaderMap.get(key);
           	connection.setRequestProperty(key, value);
            }	 
        }
        
        // 建立实际的连接
        connection.connect();
        // 获取所有响应头字段
        Map<String, List<String>> map = connection.getHeaderFields();
        // 遍历所有的响应头字段
        //System.out.println("响应头信息:");
        /*for (String key : map.keySet()) {
            System.out.println(key + "--->" + map.get(key));
        }*/
        String responseCharset="utf-8";
        Object responseContentType=map.get("Content-Type");
        if(responseContentType!=null){
        	try{
        		String[] responseContentTypes=responseContentType.toString().split(";");
            	if(responseContentTypes!=null && responseContentTypes.length>0){
            		for(String type:responseContentTypes){
            			if(type.trim().toLowerCase().contains("charset")){
            				responseCharset=type.trim().split("=")[1].replace("]", "");
            				break;
            			}
            		}
            	}
        	}catch(Exception ex2){
        		ex2.printStackTrace();
        	}
        }
        
        // 定义 BufferedReader输入流来读取URL的响应
        /*
         InputStream is=connection.getInputStream();
        
        FileOutputStream fos = new FileOutputStream(new File("d:/zzm/testurl/exchangerate.txt"));
        byte[] b = new byte[1024];
        int actionLen;
        while ((actionLen = is.read(b, 0, b.length)) != -1)
        {
          fos.write(b, 0, actionLen);
        }
        is.close();
        fos.close();
        */
        in = new BufferedReader(new InputStreamReader(
                connection.getInputStream(),responseCharset));
        
        String line;
        while ((line = in.readLine()) != null) {
            result += line;
        }
        // 使用finally块来关闭输入流
        if (in != null) {
            in.close();
        }
        
        return result;
    }
    
    
    
    public static void sendGetReturnResetConvertStream(HashMap<String,String> requestHeaderMap,String url, String param,HttpServletResponse response) throws Exception {
        BufferedReader in = null;
        String urlNameString = url;
        if(param!=null && !param.equals("")){
        	urlNameString+="?" + param;
        }
        URL realUrl = new URL(urlNameString);
        URLConnection connection = realUrl.openConnection();
        // 设置通用的请求属性
        connection.setRequestProperty("accept", "*/*");
        connection.setRequestProperty("connection", "Keep-Alive");
        connection.setRequestProperty("user-agent",
                "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
        
        if(requestHeaderMap!=null && !requestHeaderMap.isEmpty()){
       	 Iterator<String> reqheadKeys=requestHeaderMap.keySet().iterator();
            while(reqheadKeys.hasNext()){
           	 String key=reqheadKeys.next();
           	 String value=requestHeaderMap.get(key);
           	connection.setRequestProperty(key, value);
            }	 
        }
        
        // 建立实际的连接
        connection.connect();
        // 获取所有响应头字段
        
       
        response.reset();//注意需要重置resonse
        
        Map<String, List<String>> map = connection.getHeaderFields();
        
        // 遍历所有的响应头字段
        
        for (String key : map.keySet()) {
        	if(key==null 
        			|| key.equalsIgnoreCase("null") 
        			|| key.equalsIgnoreCase("Transfer-Encoding")) continue;
        	
        	List<String> values=map.get(key);
            //log.info(key + "--->" + values);
            
            if(values!=null && !values.isEmpty()){
            	response.setHeader(key, values.get(0));
            }else{
            	response.setHeader(key, null);
            }
        }
        //System.out.println(connection.getContentEncoding());
        //System.out.println(connection.getContentType());
        
        
        //response.setHeader("Content-disposition", map.get("Content-disposition").get(0));
        //response.setContentType("application/vnd_ms-excel");
        //response.setCharacterEncoding("UTF-8");
        response.setContentType(connection.getContentType());
        
        //response.setHeader("Date", new Date().toString());
		//response.setContentType(connection.getContentType());
		//if(connection.getContentEncoding()!=null){
        //	response.setCharacterEncoding(connection.getContentEncoding());
        //}
        
        ServletOutputStream os=response.getOutputStream();
        // 定义 BufferedReader输入流来读取URL的响应
        InputStream is=connection.getInputStream();
        int len = 0;
		byte[] b = new byte[1024];
		while((len = is.read(b))!=-1) {
			os.write(b, 0, len);
		}
        // 使用finally块来关闭输入流
        if (in != null) {
            in.close();
        }
         
    }
    
    public static String downLoadFile(String url, String savepath) throws Exception {
        String result = "";
        String urlNameString = url;

        URL realUrl = new URL(urlNameString);

        URLConnection connection = realUrl.openConnection();

        connection.setRequestProperty("connection", "Keep-Alive");
        connection.setRequestProperty("user-agent", 
          "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");

        connection.connect();

        Map map = connection.getHeaderFields();
        String str1;
        for (Iterator localIterator = map.keySet().iterator(); localIterator.hasNext(); str1 = (String)localIterator.next());
        InputStream in = connection.getInputStream();
        FileOutputStream fos = new FileOutputStream(new File(savepath));
        byte[] b = new byte[1024];
        int actionLen;
        while ((actionLen = in.read(b, 0, b.length)) != -1)
        {
          fos.write(b, 0, actionLen);
        }
        in.close();
        fos.close();

        return result;
      }
    public static String sendGet(String url, String param, String responsecharset)
    {
      String result = "";
      BufferedReader in = null;
      try {
        String urlNameString = url;

        if ((param != null) && (param.length() > 0)) {
          urlNameString = url + "?" + param;
        }
        URL realUrl = new URL(urlNameString);

        URLConnection connection = realUrl.openConnection();
        connection.setConnectTimeout(100000);
        connection.setReadTimeout(100000);
        connection.setRequestProperty("accept", "*/*");
        connection.setRequestProperty("connection", "Keep-Alive");
        connection.setRequestProperty("user-agent", 
          "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");

        connection.connect();

        Map map = connection.getHeaderFields();
        String str1;
        for (Iterator localIterator = map.keySet().iterator(); localIterator.hasNext(); str1 = (String)localIterator.next());
        in = new BufferedReader(
          new InputStreamReader(connection.getInputStream(), responsecharset));
        String line;
        while ((line = in.readLine()) != null)
        {
          result = result + line;
        }
      } catch (Exception e) {
        System.out.println("发送GET请求出现异常！" + e);
        e.printStackTrace();
        try
        {
          if (in != null)
            in.close();
        }
        catch (Exception e2) {
          e2.printStackTrace();
        }
      }
      finally
      {
        try
        {
          if (in != null)
            in.close();
        }
        catch (Exception e2) {
          e2.printStackTrace();
        }
      }
      return result;
    }
    public static String sendPost(HashMap<String,String> requestHeaderMap,String url, String param) throws Exception {
        PrintWriter out = null;
         BufferedReader in = null;
         String result = "";
         
         URL realUrl = new URL(url);
         // 打开和URL之间的连接
         URLConnection conn = realUrl.openConnection();
         // 设置通用的请求属性
         conn.setRequestProperty("accept", "*/*");
         conn.setRequestProperty("connection", "Keep-Alive");
         conn.setRequestProperty("user-agent",
                 "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
         
         if(requestHeaderMap!=null && !requestHeaderMap.isEmpty()){
        	 Iterator<String> reqheadKeys=requestHeaderMap.keySet().iterator();
             while(reqheadKeys.hasNext()){
            	 String key=reqheadKeys.next();
            	 String value=requestHeaderMap.get(key);
            	 conn.setRequestProperty(key, value);
             }	 
         }
         
         
         // 发送POST请求必须设置如下两行
         conn.setDoOutput(true);
         conn.setDoInput(true);
         // 获取URLConnection对象对应的输出流
         out = new PrintWriter(conn.getOutputStream());
         // 发送请求参数
         out.print(param);
         // flush输出流的缓冲
         out.flush();
         // 定义BufferedReader输入流来读取URL的响应
         in = new BufferedReader(
                 new InputStreamReader(conn.getInputStream(),"utf-8"));
         String line;
         while ((line = in.readLine()) != null) {
             result += line;
         } 
         if(out!=null){
             out.close();
         }
         if(in!=null){
             in.close();
         }
         return result;
     }
    
    public static String sendPost(HashMap<String,String> requestHeaderMap,String url, HashMap<String,String> paramMap) throws Exception {
        PrintWriter out = null;
         BufferedReader in = null;
         String result = "";
         
         URL realUrl = new URL(url);
         // 打开和URL之间的连接
         URLConnection conn = realUrl.openConnection();
         // 设置通用的请求属性
         conn.setRequestProperty("accept", "*/*");
         conn.setRequestProperty("connection", "Keep-Alive");
         conn.setRequestProperty("user-agent",
                 "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
         
         if(requestHeaderMap!=null && !requestHeaderMap.isEmpty()){
        	 Iterator<String> reqheadKeys=requestHeaderMap.keySet().iterator();
             while(reqheadKeys.hasNext()){
            	 String key=reqheadKeys.next();
            	 String value=requestHeaderMap.get(key);
            	 conn.setRequestProperty(key, value);
             }	 
         }
         StringBuffer paramSB=new StringBuffer();
         if(paramMap!=null && !paramMap.isEmpty()){
        	 Iterator<String> paramMapKeys=paramMap.keySet().iterator();
             while(paramMapKeys.hasNext()){
            	 String key=paramMapKeys.next();
            	 String value=paramMap.get(key);
            	 paramSB.append(key+"="+value+"&");
             }
             paramSB=paramSB.delete(paramSB.length()-1, paramSB.length());
         }
         
         // 发送POST请求必须设置如下两行
         conn.setDoOutput(true);
         conn.setDoInput(true);
         // 获取URLConnection对象对应的输出流
         out = new PrintWriter(conn.getOutputStream());
         // 发送请求参数
         out.print(paramSB.toString());
         // flush输出流的缓冲
         out.flush();
         // 定义BufferedReader输入流来读取URL的响应
         in = new BufferedReader(
                 new InputStreamReader(conn.getInputStream(),"utf-8"));
         String line;
         while ((line = in.readLine()) != null) {
             result += line;
         } 
         if(out!=null){
             out.close();
         }
         if(in!=null){
             in.close();
         }
         return result;
     }
    
    public static void main(String[] args){
    	try{
    		HashMap<String, InputStream> files=new HashMap<String, InputStream>();
    		InputStream in1=new FileInputStream("F:/download/a.jpg");
    		InputStream in2=new FileInputStream("F:/download/b.jpg");
    		InputStream in3=new FileInputStream("F:/download/c.jpg");
    		InputStream in4=new FileInputStream("F:/download/d.jpg");
    		    		
    		files.put("a.jpg", in1);
    		files.put("b.jpg", in2);
    		files.put("c.jpg", in3);
    		files.put("d.jpg", in4);
    		
    		
    		//String result=uploadFile("http://121.41.94.104:9080/fastdfs/upload/multifile",files);	
    		//String result=uploadFile("http://121.41.94.104:9080/fastdfs/upload/multifile",files);	
    		String result=uploadFile("http://121.41.94.104:9080/fastdfs/upload/singlefile",files);	
    		
    		//String result=uploadFile("http://localhost:9080/fastdfs/upload/singlefile",files);	
    		System.out.println(result);
    	}catch(Exception ex){
    		ex.printStackTrace();
    	}
    	
    }
    
    
    
    public static String uploadFile(String u, HashMap<String, InputStream> files)   throws Exception {
        String result = "";
         
        try {
    		String BOUNDARY = "---------7d4a6d158c9"; // 定义数据分隔线
    		URL url = new URL(u);
    		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    		// 发送POST请求必须设置如下两行
    		conn.setDoOutput(true);
    		conn.setDoInput(true);
    		conn.setUseCaches(false);
    		conn.setRequestMethod("POST");
    		conn.setRequestProperty("connection", "Keep-Alive");
    		conn.setRequestProperty("user-agent",
    				"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)");
    		conn.setRequestProperty("Charsert", "UTF-8");
    		conn.setRequestProperty("Content-Type",
    				"multipart/form-data; boundary=" + BOUNDARY);

    		OutputStream out = new DataOutputStream(conn.getOutputStream());
    		byte[] end_data = ("\r\n--" + BOUNDARY + "--\r\n").getBytes();// 定义最后数据分隔线
    		Iterator iter = files.entrySet().iterator();
    		int i=0;
    		while (iter.hasNext()) {
    			i++;
    			Map.Entry entry = (Map.Entry) iter.next();
    			String key = (String) entry.getKey();
    			InputStream val = (InputStream) entry.getValue();
    			String fname = key;
    			File file = new File(fname);
    			StringBuilder sb = new StringBuilder();
    			sb.append("--");
    			sb.append(BOUNDARY);
    			sb.append("\r\n");
    			sb.append("Content-Disposition: form-data;name=\"file" + i
    					+ "\";filename=\"" + key + "\"\r\n");
    			sb.append("Content-Type:application/octet-stream\r\n\r\n");

    			byte[] data = sb.toString().getBytes();
    			out.write(data);
    			DataInputStream in = new DataInputStream(val);
    			int bytes = 0;
    			byte[] bufferOut = new byte[1024];
    			while ((bytes = in.read(bufferOut)) != -1) {
    				out.write(bufferOut, 0, bytes);
    			}
    			out.write("\r\n".getBytes()); // 多个文件时，二个文件之间加入这个
    			in.close();
    		}
    		out.write(end_data);
    		out.flush();
    		out.close();

    		// 定义BufferedReader输入流来读取URL的响应
    		BufferedReader reader = new BufferedReader(new InputStreamReader(
    				conn.getInputStream(), "UTF-8"));
    		String line = null;
    		while ((line = reader.readLine()) != null) {
    			System.out.println(line);
    			result+=line;
    		}

    	} catch (Exception e) {
    		System.out.println("发送POST请求出现异常！" + e);
    		e.printStackTrace();
    	}
        
        
        
        
         return result;
     }
    
    public static String sendPostReturnFile(HashMap<String,String> requestHeaderMap,String url, String param,String filepath,String charset) throws Exception {
        PrintWriter out = null;
         BufferedReader in = null;
         String result = "";
         
         URL realUrl = new URL(url);
         // 打开和URL之间的连接
         URLConnection conn = realUrl.openConnection();
         // 设置通用的请求属性
         conn.setRequestProperty("accept", "*/*");
         conn.setRequestProperty("connection", "Keep-Alive");
         conn.setRequestProperty("user-agent",
                 "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
         
         if(requestHeaderMap!=null && !requestHeaderMap.isEmpty()){
        	 Iterator<String> reqheadKeys=requestHeaderMap.keySet().iterator();
             while(reqheadKeys.hasNext()){
            	 String key=reqheadKeys.next();
            	 String value=requestHeaderMap.get(key);
            	 conn.setRequestProperty(key, value);
             }	 
         }
         
         
         // 发送POST请求必须设置如下两行
         conn.setDoOutput(true);
         conn.setDoInput(true);
         // 获取URLConnection对象对应的输出流
         out = new PrintWriter(conn.getOutputStream());
         // 发送请求参数
         out.print(param);
         // flush输出流的缓冲
         out.flush();
         // 定义BufferedReader输入流来读取URL的响应
         in = new BufferedReader(
                 new InputStreamReader(conn.getInputStream(),charset));
         String line;
         
         while ((line = in.readLine()) != null) {
             result += line;
         } 
         if(out!=null){
             out.close();
         }
         if(in!=null){
             in.close();
         }
         return result;
     }

    /**
     * 向指定 URL 发送POST方法的请求
     * 
     * @param url
     *            发送请求的 URL
     * @param param
     *            请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
     * @return 所代表远程资源的响应结果
     */
    public static String sendPost(String url, String param) throws Exception {
       PrintWriter out = null;
        BufferedReader in = null;
        String result = "";
        
        URL realUrl = new URL(url);
        // 打开和URL之间的连接
        URLConnection conn = realUrl.openConnection();
        // 设置通用的请求属性
        conn.setRequestProperty("accept", "*/*");
        conn.setRequestProperty("connection", "Keep-Alive");
        conn.setRequestProperty("user-agent",
                "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
        // 发送POST请求必须设置如下两行
        conn.setDoOutput(true);
        conn.setDoInput(true);
        // 获取URLConnection对象对应的输出流
        out = new PrintWriter(conn.getOutputStream());
        // 发送请求参数
        out.print(param);
        // flush输出流的缓冲
        out.flush();
        // 定义BufferedReader输入流来读取URL的响应
        in = new BufferedReader(
                new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = in.readLine()) != null) {
            result += line;
        } 
        if(out!=null){
            out.close();
        }
        if(in!=null){
            in.close();
        }
        return result;
    }  
    public static String sendPost(String url, Map param) throws Exception {
    	StringBuffer sb=new StringBuffer();
    	Iterator iterator=param.entrySet().iterator();
    	while(iterator.hasNext()){
    		Map.Entry<String, String>  entry=(Map.Entry<String, String> )iterator.next();
    		sb.append(entry.getKey()+"="+entry.getValue()+"&");
    	}
    	
    	return sendPost(url,sb.deleteCharAt(sb.length()-1).toString());
    }
    
     
    public static void test2(String page) throws Exception {
    	/*
    	Map<String,String> map=new HashMap<String,String>();
    	map.put("city_code",DesHelper.encrypt_str("wuhan", ZZWLUtil.hotel_list_key));
    	map.put("opt", "100");
    	
    	System.out.println(HttpRequest.sendPost("http://192.168.100.187:8080/hotel/hotel_list.do", map));
    	*/ 
    	String[] hotels=new String[] {};
    	Map<String,String> map=new HashMap<String,String>();//MT-398355 MT-321399 MT-061144 MT-061144 MT-321399   MT-322463  MT-004668 //MT-4326190 MT-336836
    	
    	map.put("check_in_date",check_in_date);
    	map.put("check_out_date",check_out_date);
    	map.put("adult_count",adult_count);
    	map.put("children_count",children_count);
    	map.put("children_age_list",children_age_list);
    	
    	
    	map.put("room_count",room_count);
    	
    	
    	//map.put("is_free_cancel","1");
    	map.put("page_index",page);
    	map.put("page_size","5"); 
    	long start=System.currentTimeMillis();
    	//String ret=HttpRequest.sendPost("http://192.168.100.187:8080/hotel/service.do", map);
    	//System.out.println(ret);
    	//System.out.println(HttpRequest.sendPost("http://192.168.100.31:18080/hotel/service.do", map));
    	//System.out.println(HttpRequest.sendPost("http://192.168.100.187:8080/hotel/service.do", map));
    	//System.out.println();
    	if(isLocalTest) {
    		System.out.println(HttpRequest.sendPost("http://192.168.100.187:8080/hotel/service.do", map));
        }else {
        	System.out.println(HttpRequest.sendPost("http://61.183.72.78:8090/hotel/service.do", map));
        }
    	
    	System.out.println(System.currentTimeMillis()-start);
    }
    
    static String rate="6_cfbc52fd2a11c4f35f53b7a5627f3579_16:200110508:200609266:P";
    static String price="2133.60";//
    static String check_in_date="2017-11-05";
	static String check_out_date="2017-11-07";
	static String adult_count="2";
	static String children_count="1";
    static String children_age_list="7";
    static String room_count="2";
    static String children_age="7";
    static String rooms_adult_name_list="张三,李四";
    static int test_i=1;
    static Boolean isLocalTest=true;
    
    static String order_id="20171009145151";
    
    
        
}