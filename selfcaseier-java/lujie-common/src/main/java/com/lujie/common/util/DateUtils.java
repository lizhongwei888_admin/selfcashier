/*
 *  Copyright 2014-2015 snakerflow.com
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *     http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */
package com.lujie.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Minutes;
import org.joda.time.Seconds;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;


/**
 * 时间工具
 * @author hupei
 */
public class DateUtils {
	private static final String DATE_FORMAT_DEFAULT = "yyyy-MM-dd HH:mm:ss";
	private static final String DATE_FORMAT_YMD = "yyyy-MM-dd";
	private static final String DATE_FORMAT_YMDHMS = "yyyyMMddHHmmss";
	private static final String DATE_FORMAT_YMDHMSS="yyyyMMddHHmmssSSS";
	
	public static String getCurrentTime() {
		return new DateTime().toString(DATE_FORMAT_DEFAULT);
	}
	
	public static String getCurrentDay() {
		return new DateTime().toString(DATE_FORMAT_YMD);
	}
	
	public static String getDefaultAllTime(){
		return new DateTime().toString(DATE_FORMAT_YMDHMS);
	}
	public static String getDeteTime(){
		return new DateTime().toString(DATE_FORMAT_YMDHMSS);
	}
	public static String getDateStr(Date date,String format){
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}
	
	/**
	 * 得到多少天以前的时间
	 * @param days
	 * @return
	 */
	public static String getMinusDay(int days , String format){
		return new DateTime().minusDays(days).toString(format);
	}
	
	/**
	 * 得到多少天以前的时间
	 * @param days
	 * @return
	 */
	public static String getMinusDay(int days){
		return new DateTime().minusDays(days).toString(DATE_FORMAT_YMD);
	}
	

	/**
	 * 得到多少天之前的时间
	 * @param time
	 * @param days
	 * @return
	 */
	public static String getMinusDay(DateTime time  , int days){
		return time.minusDays(days).toString(DATE_FORMAT_YMD);
	}
	
	/**
	 * 得到多少周以后的时间
	 * @param days
	 * @return
	 */
	public static String getPlusWeeks(Date date,int weeks){
		return new DateTime(date.getTime()).plusWeeks(weeks).toString(DATE_FORMAT_YMD);
	}
	
	/**
	 * 得到多少月以前的时间
	 * @param days
	 * @return
	 */
	public static String getMinusMonths(int months){
		return new DateTime().minusMonths(months).toString(DATE_FORMAT_YMD);
	}
	
	/**
	 * 得到指定日期多少月以前的时间
	 * @param days
	 * @return
	 */
	public static String getMinusMonths(Date date,int months){
		return new DateTime(date.getTime()).minusMonths(months).toString(DATE_FORMAT_YMD);
	}
	
	/**
	 * 得到指定日期多少秒以前的时间
	 * @param days
	 * @return
	 */
	public static String getMinusSeconds(Date date,int seconds){
		return new DateTime(date.getTime()).minusSeconds(seconds).toString(DATE_FORMAT_DEFAULT);
	}
	
	/**
	 * 得到指定日期多少分以前的时间
	 * @param days
	 * @return
	 */
	public static String getMinusMinutes(Date date,int minutes){
		return new DateTime(date.getTime()).minusMinutes(minutes).toString(DATE_FORMAT_DEFAULT);
	}
	
	/**
	 * 得到指定日期多少月以后的时间
	 * @param days
	 * @return
	 */
	public static String getPlusMonths(Date date,int months){
		return new DateTime(date.getTime()).plusMonths(months).toString(DATE_FORMAT_YMD);
	}
	

	/**
	 * 得到多少月之前的时间
	 * @param time
	 * @param days
	 * @return
	 */
	public static String getMinusMonths(DateTime time  , int months){
		return time.minusMonths(months).toString(DATE_FORMAT_YMD);
	}
	
	/**
	 * 得到多少月之前的时间的月首时间
	 * @param time
	 * @param days
	 * @return
	 */
	public static String getMinusMonthsFirst(DateTime time  , int months){
		String ss = time.minusMonths(months).toString(DATE_FORMAT_YMD);
		return ss.substring(0, ss.length()-2)+"01";
	}
	/**
	 * 得到多少月之前的时间的月尾时间
	 * @param time
	 * @param days
	 * @return
	 */
	public static String getMinusMonthsEnd(DateTime time  , int months){
		String ss = time.minusMonths(months).toString(DATE_FORMAT_YMD);
		return ss.substring(0, ss.length()-2)+time.minusMonths(months).dayOfMonth().getMaximumValue();
	}
	
	/**
	 * 得到多少年以前的时间
	 * @param days
	 * @return
	 */
	public static String getMinusYears(int years){
		return new DateTime().minusYears(years).toString(DATE_FORMAT_YMD);
	}
	
	/**
	 * 得到现在的小时数
	 * @return
	 */
	public static int getHour(){
		return new DateTime().getHourOfDay();
	}
	
	/**
	 * 得到现在是本月第几天
	 * @return
	 */
	public static int getDay(){
		return new DateTime().getDayOfMonth();
	}

	/**
	 * 得到多少年之前的时间
	 * @param time
	 * @param days
	 * @return
	 */
	public static String getMinusYears(String time  , int years){
		DateTimeFormatter format = DateTimeFormat.forPattern(DATE_FORMAT_YMD);
		DateTime dateTime1 = DateTime.parse(time, format);
		return dateTime1.minusYears(years).toString(DATE_FORMAT_YMD);
	}	
	
	/**
	 * 得到多少天之后的时间
	 * @param days
	 * @return
	 */
	public static String getPlusDay(int days){
		return new DateTime().plusDays(days).toString(DATE_FORMAT_YMD);
	}
	
	/**
	 * 得到指定日期多少天之后的时间
	 * @param days
	 * @return
	 */
	public static String getPlusDay(Date date,int days){
		return new DateTime(date.getTime()).plusDays(days).toString(DATE_FORMAT_YMD);
	}
	
	/**
	 * 得到多少天之后的时间
	 * @param time
	 * @param days
	 * @return
	 */
	public static String getPlusDay(DateTime time , int days){
		return time.plusDays(days).toString(DATE_FORMAT_YMD);
	}
	
	/**
	 * 获取本月开头的日期
	 * @return
	 */
	public static String startOfMonths(){
		int days = new DateTime().getDayOfMonth() - 1;
		return DateUtils.getMinusDay(days);
	}
	
	/**
	 * 比较2个时间之间的秒差 >0 表示end>start   <0表示end<start
	 * @param start
	 * @param end
	 * @return
	 */
	public static int getBetweenMinus(String start , String end){
		DateTimeFormatter format = DateTimeFormat.forPattern(DATE_FORMAT_DEFAULT);
		DateTime dateTime1 = DateTime.parse(start, format);
		DateTime dateTime2 = DateTime.parse(end, format);
		return Seconds.secondsBetween(dateTime1, dateTime2).getSeconds();
	}
	/**
	 * 比较2个时间之间的分钟差 >0 表示end>start   <0表示end<start
	 * @param start
	 * @param end
	 * @return
	 */
	public static int getBetweenMinutes(String start , String end){
		DateTimeFormatter format = DateTimeFormat.forPattern(DATE_FORMAT_DEFAULT);
		DateTime dateTime1 = DateTime.parse(start, format);
		DateTime dateTime2 = DateTime.parse(end, format);
		return Minutes.minutesBetween(dateTime1,dateTime2).getMinutes();
	}
	
	/**
	 * 比较2个时间之间的天数差 >0 表示end>start   <0表示end<start
	 * @param start
	 * @param end
	 * @return
	 */
	public static int getBetweenDays(String start , String end){
		DateTimeFormatter format = DateTimeFormat.forPattern(DATE_FORMAT_YMD);
		DateTime dateTime1 = DateTime.parse(start, format);
		DateTime dateTime2 = DateTime.parse(end, format);
		return Days.daysBetween(dateTime1, dateTime2).getDays();
	}
	
	/**
	 * 等分时间段
	 * @param stime
	 * @param etime
	 * @return
	 * @throws ParseException 
	 */
	public static List<String> shutTime(String stime , String etime) throws ParseException{
		SimpleDateFormat sdf  = new SimpleDateFormat("yyyy-MM-dd");
		List<String> ad = new ArrayList<String>(); 
		if (stime.equals("")&&!etime.equals("")){
			Date ddate = sdf.parse(etime);
			Date temp = ddate;
			ad.add(sdf.format(ddate));
			Calendar calendar = new GregorianCalendar();
			for (int i=0 ; i < 30 ; i++){
				Date date = temp;
				calendar.setTime(date);
				calendar.add(calendar.DATE,-1);	
				date = temp = calendar.getTime();
				ad.add(sdf.format(date));
			}
			Collections.reverse(ad);
		}else if(!stime.equals("")&&etime.equals("")){
			Date sdate = sdf.parse(stime);
			Date temp = sdate;
			ad.add(sdf.format(sdate));
			Calendar calendar = new GregorianCalendar();
			Date now = sdf.parse(sdf.format(new Date()));
			long betweenDays = (long)((now.getTime() - sdate.getTime()) / (1000 * 60 * 60 *24) + 0.5);
			if (betweenDays <= 30){
				for (int i=0 ; i < betweenDays ; i++){
					Date date = temp;
					calendar.setTime(date);
					calendar.add(calendar.DATE,1);	
					date = temp = calendar.getTime();
					ad.add(sdf.format(date));
				}				
			}else{
				double j = (double)betweenDays;
				long days = Math.round(j/30);
				for (int i=0 ; i < 29 ; i++){
					Date date = temp;
					calendar.setTime(date);
					calendar.add(calendar.DATE,(int)days);	
					date = temp = calendar.getTime();
					ad.add(sdf.format(date));
				}
				ad.add(sdf.format(now));
			}
		}else if(!stime.equals("")&&!etime.equals("")){
			Date sdate = sdf.parse(stime);
			Date temp = sdate;
			ad.add(sdf.format(sdate));
			Calendar calendar = new GregorianCalendar();
			Date ddate = sdf.parse(etime);
			long betweenDays = (long)((ddate.getTime() - sdate.getTime()) / (1000 * 60 * 60 *24) + 0.5);
			if (betweenDays <= 30){
				for (int i=0 ; i < betweenDays ; i++){
					Date date = temp;
					calendar.setTime(date);
					calendar.add(calendar.DATE,1);	
					date = temp = calendar.getTime();
					ad.add(sdf.format(date));
				}				
			}else{
				double j = (double)betweenDays;
				long days = Math.round(j/30);
				for (int i=0 ; i < 29 ; i++){
					Date date = temp;
					calendar.setTime(date);
					calendar.add(calendar.DATE,(int)days);	
					date = temp = calendar.getTime();
					ad.add(sdf.format(date));
				}
				ad.add(sdf.format(ddate));
			}			
		}else if(stime.equals("")&&etime.equals("")){
			Date ddate = new Date();
			Date temp = ddate;
			ad.add(sdf.format(ddate));
			Calendar calendar = new GregorianCalendar();
			for (int i=0 ; i < 30 ; i++){
				Date date = temp;
				calendar.setTime(date);
				calendar.add(calendar.DATE,-1);	
				date = temp = calendar.getTime();
				ad.add(sdf.format(date));
			}
			Collections.reverse(ad);
		}
		return ad;
	} 	
	
	public static void main(String [] args) throws ParseException{
//       System.out.println(DateUtils.getMinusMonthsFirst(new DateTime() , 2));
//       System.out.println(DateUtils.getBetweenDays(DateUtils.getMinusMonthsFirst(new DateTime() , 2) , DateUtils.getCurrentDay()));
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
		System.out.println(DateUtils.getMinusDay(10 , DATE_FORMAT_YMDHMS));
	}
}
