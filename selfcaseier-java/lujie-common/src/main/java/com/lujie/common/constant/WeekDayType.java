package com.lujie.common.constant;

/**
 * @author zzm
 *
 */
public enum WeekDayType  {

	Monday(1, "星期一"),
	Tuesday(2, "星期二"),
	Wednesday(3, "星期三"),
	Thursday(4, "星期四"),
	Friday(5, "星期五"),
	Saturday(6, "星期六"),
	Sunday(7, "星期日")
	
	
	
	;
	
	private Integer code;

	
	private String desc;

	WeekDayType(Integer code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public Integer getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

}
