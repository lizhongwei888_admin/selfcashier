package com.lujie.common.util;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import com.fasterxml.jackson.core.type.TypeReference;

/**
 * @author user
 * @param <T>
 *
 */
public abstract class JsonTypeReference<T> {
	
	private TypeReference<T> typeReference;

	protected final Type type;
	
	public JsonTypeReference() {
		super();
        Type superClass = getClass().getGenericSuperclass();
        if (superClass instanceof Class<?>) { // sanity check, should never happen
            throw new IllegalArgumentException("Internal error: TypeReference constructed without actual type information");
        }
        type = ((ParameterizedType) superClass).getActualTypeArguments()[0];
        
		this.typeReference = new TypeReference<T>() {
			@Override
			public Type getType() {
				return type;
			}
			@Override
			public int compareTo(TypeReference<T> o) {
				return 0;
			}
		};
	}
	
	public TypeReference<T> getTypeReference(){
		return this.typeReference;
	}

}
