package com.lujie.common.util;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

public class WechatUtil {


	
	/**
	 * https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=ACCESS_TOKEN
	
	 * 调用获取accesstoken接口报错
	 * {"errcode":41002,"errmsg":"appid missing"}
	 * 原因是请求参数有误，参数拼接错误
	 *{"access_token":"60_bMGliZ9brF_Gvz7V__iY69UCZlg2SvzgEr0Fd-OU6Vbas_8ZDigOkU_Z7QexLpLMpPt9rssmavL-fvZuWc-bD-E9aSzijOr4yTN6V1ARkJJ9o5sML_Dap5O-yMybOcQUOgfN2_WC6RWLkIIUKWRjAEAXJE","expires_in":7200}

	 * @return
	 */
	public static String getWechatServiceNotifyAccessToken()throws Exception {
		//https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/access-token/auth.getAccessToken.html
		String requestUrl="https://api.weixin.qq.com/cgi-bin/token";
		String parm="grant_type=client_credential&appid=wxf4ad74cf1b249a18&secret=fb349405d803ab6dea606ccb6d3d59b7";
		//{"access_token":"60_bMGliZ9brF_Gvz7V__iY69UCZlg2SvzgEr0Fd-OU6Vbas_8ZDigOkU_Z7QexLpLMpPt9rssmavL-fvZuWc-bD-E9aSzijOr4yTN6V1ARkJJ9o5sML_Dap5O-yMybOcQUOgfN2_WC6RWLkIIUKWRjAEAXJE","expires_in":7200}

			
		String accessTokenResult= HttpRequest.sendGet(requestUrl, parm);
		System.out.println(accessTokenResult);
		
        return accessTokenResult;
	}
	
	public static String getWechatServiceNotifyAccessToken(String appid,String secret)throws Exception {
		//https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/access-token/auth.getAccessToken.html
		String requestUrl="https://api.weixin.qq.com/cgi-bin/token";
		String parm="grant_type=client_credential&appid="+appid+"&secret="+secret;
		//{"access_token":"60_bMGliZ9brF_Gvz7V__iY69UCZlg2SvzgEr0Fd-OU6Vbas_8ZDigOkU_Z7QexLpLMpPt9rssmavL-fvZuWc-bD-E9aSzijOr4yTN6V1ARkJJ9o5sML_Dap5O-yMybOcQUOgfN2_WC6RWLkIIUKWRjAEAXJE","expires_in":7200}

			
		String accessTokenResult= HttpRequest.sendGet(requestUrl, parm);
		System.out.println(accessTokenResult);
		
        return accessTokenResult;
	}
	
	
	
	public String getOpenId(HttpServletRequest request,String code)throws Exception {
		String requestUrl = "https://api.weixin.qq.com/sns/jscode2session";
        String parm="appid=wxf4ad74cf1b249a18&secret=fb349405d803ab6dea606ccb6d3d59b7&js_code="+code+"&grant_type=authorization_code";
        String getOpenIdResult= HttpRequest.sendGet(requestUrl, parm);
		return getOpenIdResult;
		
	}
	
	public static  HashMap<String,String> getRequestHeadMap(){
		HashMap<String,String> reqHeaderMap=new HashMap<>();
		reqHeaderMap.put("Content-Type","application/json;charset=utf-8");
		return reqHeaderMap;
	}
	
	public static void main(String[] args){
		try{
			String accessToken=getWechatServiceNotifyAccessToken();
			//System.out.println(accessToken);
			//String accessToken="60_l932SXDmFhLqQsjd0p85TDnWQJ_iX96R1zVLcCQ48ywsMJp1Ouds6FMxzMhsXXXlpIebQG0rAjjbLjtR527TY9kAl7tsskgQ_GACrwJkI2JMeQ-vbA_41DtgR58aImSu7p6Jn8-xnfXyXP4wJXLaAFANBP";
			//String accessToken="60_bMGliZ9brF_Gvz7V__iY69UCZlg2SvzgEr0Fd-OU6Vbas_8ZDigOkU_Z7QexLpLMpPt9rssmavL-fvZuWc-bD-E9aSzijOr4yTN6V1ARkJJ9o5sML_Dap5O-yMybOcQUOgfN2_WC6RWLkIIUKWRjAEAXJE";
			//{"access_token":"60_l932SXDmFhLqQsjd0p85TDnWQJ_iX96R1zVLcCQ48ywsMJp1Ouds6FMxzMhsXXXlpIebQG0rAjjbLjtR527TY9kAl7tsskgQ_GACrwJkI2JMeQ-vbA_41DtgR58aImSu7p6Jn8-xnfXyXP4wJXLaAFANBP","expires_in":7200}

			JSONObject jsonObject=JSONObject.fromObject(accessToken);
			accessToken=jsonObject.opt("access_token").toString();
			
			send("ojxbC5ZkAjNoiqz1KXs-DBv1nutA", accessToken);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		
	}
	/**
	 * https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/subscribe-message/subscribeMessage.send.html
	 
	 */
	public static String send(String openId,String accessToken) throws Exception{
		String requestUrl="https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token="+accessToken;
		HashMap<String,String> requestHeadMap=getRequestHeadMap();
		HashMap dataMap=new HashMap();
		
		dataMap.put("touser", openId);
		dataMap.put("template_id", "xy6rUf0BUXPge61aBvR1LXkEHX6Gt0x0aE6CcHAD-30");
		dataMap.put("page", "pages/usercenter/index?test1=a&test2=b");
		//pages/usercenter/index
		
		//?notice_type=alert&alertBigType=1&vehicleLicenseNum=***&alertTime=
		
		
		dataMap.put("miniprogram_state", "developer");//注意改成正式版，跳转小程序类型：developer为开发版；trial为体验版；formal为正式版；默认为正式版

		HashMap businessDataMap=new HashMap();
		
		HashMap name1ValueMap=new HashMap();
		name1ValueMap.put("value", "名字");
		businessDataMap.put("name1", name1ValueMap); 
		
		HashMap phone2ValueMap=new HashMap();
		phone2ValueMap.put("value", "13011112222");
		businessDataMap.put("phone_number2", phone2ValueMap);
		
		HashMap time3ValueMap=new HashMap();
		time3ValueMap.put("value", "2022年8月27日 00:00:00");
		businessDataMap.put("time3",time3ValueMap);
		
		
		
		dataMap.put("data", businessDataMap);
		
		
		
		/*
		 * 
		{"errcode":0,"errmsg":"ok","msgid":2546860234519248896}
		42001
	          */
		
		
		String param=JsonHelper.toJson(dataMap);
		System.out.println(param);
		
		String result=HttpRequest.sendPost(requestHeadMap,requestUrl, param);
		System.out.println(result);
		return result;
	}
	
	/**
	 * 
	 微信小程序推送消息
	 https://blog.csdn.net/u013733643/article/details/125309103
	 微信小程序推荐使用“订阅消息”功能，推送消息
	开通小程序“订阅消息”功能、配置订阅消息“模板”
	：微信小程序管理平台
			开通小程序“订阅消息”功能
			配置订阅消息“模板”
				订阅消息模板分为“一次性订阅”和“长期订阅”两种模板。
				一次性订阅模板只能授权一次发送一次。
				长期订阅模板可以授权一次，一直发送，没有次数限制。
				长期订阅模板和小程序配置的类目有关，有的类目下没有长期订阅模板。目前长期性订阅消息仅向政务民生、医疗、交通、金融、教育等线下公共服务开放。
				如果没有找到符合需求的订阅模板，可以自定义创建新的订阅模板
	
	用户授权“订阅消息”
		小程序调用wx.login，获取code，小程序调用服务器端接口(传code)，获取用户openid,
		登录成功后，服务器保存用户openid
	推送订阅消息
		服务器端获取小程序全局唯一后台接口调用凭据access_token
		服务器端推送业务数据内容给微信客户端
	 * 
	 * 
	 * 
	 */
	
}
