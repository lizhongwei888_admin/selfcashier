package com.lujie.common.util.qqmail;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
/**
 * 使用qq邮箱发送邮件
 * @author Administrator
 *
 */
public class QQEmailServer {

	public static void main(String[] args) {
		try{
			QQEmailServer.sendEmail("lujiewuhan@qq.com","yoqobuivstrjbejh","582786499@qq.com","测试发送邮件2","测试内容2");	
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
	}
	public  static void sendEmail(String from,String password,String to,String title,String content) throws AddressException, MessagingException{
		  Properties properties = new Properties();
        properties.put("mail.transport.protocol", "smtp");// 连接协议
        properties.put("mail.smtp.host", "smtp.qq.com");// 主机名
        properties.put("mail.smtp.port", 465);// 端口号
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.ssl.enable", "true");// 设置是否使用ssl安全连接 ---一般都使用
        properties.put("mail.debug", "true");// 设置是否显示debug信息 true 会在控制台显示相关信息
        // 得到回话对象
        Session session = Session.getInstance(properties);
        // 获取邮件对象
        Message message = new MimeMessage(session);
        // 设置发件人邮箱地址
        message.setFrom(new InternetAddress(from));
        // 设置收件人邮箱地址 
        message.setRecipients(Message.RecipientType.TO, new InternetAddress[]{new InternetAddress(to)});
        //message.setRecipient(Message.RecipientType.TO, new InternetAddress("xxx@qq.com"));//一个收件人
        // 设置邮件标题
        message.setSubject(title);
        // 设置邮件内容
        
         message.setText(content);
        // 得到邮差对象
        Transport transport = session.getTransport();
        // 连接自己的邮箱账户
        //transport.connect("lujiewuhan@qq.com", "chhcwjfqsnfsebge");// 密码为QQ邮箱开通的stmp服务后得到的客户端授权码,输入自己的即可
        transport.connect(from, password);// 密码为QQ邮箱开通的stmp服务后得到的客户端授权码,输入自己的即可
        
        
        // 发送邮件
        transport.sendMessage(message, message.getAllRecipients());
        transport.close();
}

}
