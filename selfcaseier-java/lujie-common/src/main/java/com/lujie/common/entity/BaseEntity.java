package com.lujie.common.entity;

import java.util.Date;

import com.lujie.common.util.JsonHelper;
import com.lujie.common.util.MD5Util;

public abstract class BaseEntity {

	protected Integer pageNum=1;//页面的第几页
	protected Integer pageSize=10;//页面的每页记录条数
	protected Integer rowIndex=0;//表的记录的位置
	protected Integer count=0;//记录条数
	protected Integer sum=0;//求和中枢
	protected String path;//层级
	
	protected Integer isValid; 
	
	protected Date createTime;
	protected Date modifyTime;
	
	private String createDateStr;
	protected String createTimeStr;
	protected String modifyTimeStr;
	
	protected String createTimeStart;
	protected String createTimeEnd;
	protected String createUser;
	protected String createUserName;
	
	
	protected String modifyTimeStart;
	protected String modifyTimeEnd;
	
	protected String modifyUser;
	protected String modifyUserName;
	
	protected String remark;
	protected String dataIds;
	protected String dataId;
	private Object moduleRoleRelative;
	protected String associatedColumn;
	
	protected String orderBy;
	
	protected boolean equals(String child, String other) {

		return objEquals(child, other);
	}

	protected int hashCode(String id) {

		return objHashCode(id);
	}

	protected boolean equals(Long child, Long other) {

		return objEquals(child, other);
	}

	protected int hashCode(Long id) {

		return objHashCode(id);
	}

	protected boolean equals(Integer child, Integer other) {

		return objEquals(child, other);
	}

	protected int hashCode(Integer id) {

		return objHashCode(id);
	}

	protected boolean objEquals(Object child, Object other) {

		if (child == other)
			return true;
		if ((child == null) || (other == null))
			return false;
		return child.equals(other);
	}

	protected int objHashCode(Object id) {

		if (id == null)
			return super.hashCode();
		return id.hashCode();
	}

	public Integer getPageNum() {
		return pageNum;
	}

	public void setPageNum(Integer pageNum) {
		this.pageNum = pageNum;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getRowIndex() {
		if(pageNum!=null && pageSize!=null){
			rowIndex=(pageNum-1)*pageSize;
		}
		return rowIndex;
	}

	public void setRowIndex(Integer rowIndex) {
		this.rowIndex = rowIndex;
	}

	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreateTimeStart() {
		return createTimeStart;
	}

	public void setCreateTimeStart(String createTimeStart) {
		this.createTimeStart = createTimeStart;
	}

	public String getCreateTimeEnd() {
		return createTimeEnd;
	}

	public void setCreateTimeEnd(String createTimeEnd) {
		this.createTimeEnd = createTimeEnd;
	}

	public String getModifyUser() {
		return modifyUser;
	}

	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getIsValid() {
		return isValid;
	}

	public void setIsValid(Integer isValid) {
		this.isValid = isValid;
	}

	public String getCreateTimeStr() {
		return createTimeStr;
	}

	public void setCreateTimeStr(String createTimeStr) {
		this.createTimeStr = createTimeStr;
	}

	public String getModifyTimeStr() {
		return modifyTimeStr;
	}

	public void setModifyTimeStr(String modifyTimeStr) {
		this.modifyTimeStr = modifyTimeStr;
	}

	public String getModifyUserName() {
		return modifyUserName;
	}

	public void setModifyUserName(String modifyUserName) {
		this.modifyUserName = modifyUserName;
	}

	public String getModifyTimeStart() {
		return modifyTimeStart;
	}

	public void setModifyTimeStart(String modifyTimeStart) {
		this.modifyTimeStart = modifyTimeStart;
	}

	public String getModifyTimeEnd() {
		return modifyTimeEnd;
	}

	public void setModifyTimeEnd(String modifyTimeEnd) {
		this.modifyTimeEnd = modifyTimeEnd;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Integer getSum() {
		return sum;
	}

	public void setSum(Integer sum) {
		this.sum = sum;
	}

	
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getCreateDateStr() {
		return createDateStr;
	}

	public void setCreateDateStr(String createDateStr) {
		this.createDateStr = createDateStr;
	}

	public String getDataIds() {
		return dataIds;
	}

	public void setDataIds(String dataIds) {
		this.dataIds = dataIds;
	}

	public Object getModuleRoleRelative() {
		return moduleRoleRelative;
	}

	public void setModuleRoleRelative(Object moduleRoleRelative) {
		this.moduleRoleRelative = moduleRoleRelative;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getCreateUserName() {
		return createUserName;
	}

	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}

	public String getAssociatedColumn() {
		return associatedColumn;
	}

	public void setAssociatedColumn(String associatedColumn) {
		this.associatedColumn = associatedColumn;
	}

	public String getDataId() {
		return dataId;
	}

	public void setDataId(String dataId) {
		this.dataId = dataId;
	}

	public String genarateDataId() {
		//String str=JsonHelper.toJson(this);
		//System.out.println(str);
		dataId=MD5Util.encrypt(JsonHelper.toJson(this));
		return dataId;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}
	
	public String toString(){
		return JsonHelper.toJson(this);
	}
 
	
	
}
