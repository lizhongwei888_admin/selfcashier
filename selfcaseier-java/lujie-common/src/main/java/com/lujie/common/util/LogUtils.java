package com.lujie.common.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;

import org.apache.commons.lang.time.DateFormatUtils;

public class LogUtils {
	
	/**
	 *	 打印堆栈信息
	 * @param throwable
	 * @return
	 */
	public static void printStackTrace(Throwable e){
	String currentDate=	DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss.SSS");
	   StringWriter sw = new StringWriter();
	   try (PrintWriter pw = new PrintWriter(sw)) {
	      e.printStackTrace(pw);
	       System.out.print(currentDate+": "+sw.toString());
	   }
	}
	
	/**
	 * 	获取堆栈信息
	 * @param throwable
	 * @return
	 */
	public static String getStackTrace(Throwable throwable){
		String currentDate=	DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss.SSS");
		StringWriter sw = new StringWriter();
		try (PrintWriter pw = new PrintWriter(sw)) {
			throwable.printStackTrace(pw);
		}
		return sw.toString();
	}
}
