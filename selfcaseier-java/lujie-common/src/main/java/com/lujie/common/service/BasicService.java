package com.lujie.common.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.poi.ss.formula.functions.T;

import com.lujie.common.dao.BasicMapper;
import com.lujie.common.entity.BasicEntity;
import com.lujie.common.entity.EntityMap;

public class BasicService   {

	private BasicMapper basicMapper;
	
	
	
	public BasicMapper getBaseMapper() {
		return basicMapper;
	}

	public void setBaseMapper(BasicMapper baseMapper) {
		this.basicMapper = baseMapper;
	}

	@SuppressWarnings("unchecked")
	public List findAll(){
		return basicMapper.findAll();
	}
	
	
	
	/**
	 * 根据多个id批量查询记录
	 * @param ids
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<T> findByIds(String ids){
		return basicMapper.findByIds(ids);
	}
	@SuppressWarnings("unchecked")
	public Object findById(String id){
		return basicMapper.findById(id);
	}
	
	
	/**
	 * 批量保存记录
	 * @param checkInList
	 */
	@SuppressWarnings("unchecked")
	public void batchSave(List list){
		basicMapper.batchSave(list);
	}
	public void save(Object p){
		basicMapper.save(p);
	}
	
	
	/**
	 * 批量更新数据
	 * @param checkInList
	 */
	public void batchUpdate(List list){
		basicMapper.batchUpdate(list);
	}
	@SuppressWarnings("unchecked")
	public Integer update(Object  p){
		return basicMapper.update(p);
	} 
	
	
	/**
	 * 根据对象属性，查找对象列表
	 * @param property
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List findPageList(Object property){
		return basicMapper.findPageList(property);
	}
	 
	
	/**
	 * 根据对象属性，计算对象总数
	 * @param property
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Long countTotal( Object property){
		return basicMapper.countTotal(property);
	}
	
	public static Integer BatchSize=2;
	@SuppressWarnings("unchecked")
	public List batchFindPageListByProperty(Object property){
		
		BasicEntity basicEntity=(BasicEntity)property;
		basicEntity.setPageNum(1);
		basicEntity.setPageSize(BatchSize);
		
		List allList=new ArrayList();
		List list=basicMapper.findPageList(property);
		while(list!=null && list.size()>0){
			allList.addAll(list);
			
			basicEntity.setPageNum(basicEntity.getPageNum()+1);
			list=basicMapper.findPageList(property);
		} 
		return allList;
	}
	
	@SuppressWarnings("unchecked")
	public Integer deleteByIds(String ids){
		return basicMapper.deleteByIds(ids);
	}
	
	@SuppressWarnings("unchecked")
	public List findList(Object property){
		return basicMapper.findList(property);
	}
	public Object findOne(Object property){
		return basicMapper.findOne(property);
	}
	
	public List<EntityMap> getColumnInfo(String tableName){
		EntityMap entityMap=new EntityMap();
		entityMap.put("tableName", tableName);
		return basicMapper.getColumnInfo(entityMap);
	}
	
}
