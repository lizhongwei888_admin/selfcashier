
package com.lujie.common.util;


/**
 * ClassName:GraphicsUtils <br/>
 * 图形工具
 */
public class GraphicsUtils {

	/**
	  * 判断一点是否在多边形内
	  *
	  * @param target
	  * @param points
	  * @return
	  */
	 public static boolean isPointInPolygon(LatLonPoint target, LatLonPoint[] points) {
	     int iSum, iCount, iIndex;
	     double dLon1 = 0, dLon2 = 0, dLat1 = 0, dLat2 = 0, dLon;
	     if (points.length < 3) {
	         return false;
	     }
	     iSum = 0;
	     iCount = points.length;
	     for (iIndex = 0; iIndex<iCount;iIndex++) {
	         if (iIndex == iCount - 1) {
	             dLon1 = points[iIndex].getLongitude();
	             dLat1 = points[iIndex].getLatitude();
	             dLon2 = points[0].getLongitude();
	             dLat2 = points[0].getLatitude();
	         } else {
	             dLon1 = points[iIndex].getLongitude();
	             dLat1 = points[iIndex].getLatitude();
	             dLon2 = points[iIndex + 1].getLongitude();
	             dLat2 = points[iIndex + 1].getLatitude();
	         }

	         double ALat = target.getLatitude();
	         double ALon = target.getLongitude();
	         // 以下语句判断A点是否在边的两端点的水平平行线之间，在则可能有交点，开始判断交点是否在左射线上
	         if (((ALat >= dLat1) && (ALat < dLat2)) || ((ALat >= dLat2) && (ALat < dLat1))) {
	             if (Math.abs(dLat1 - dLat2) > 0) {
	                 //得到 A点向左射线与边的交点的x坐标：
	                 dLon = dLon1 - ((dLon1 - dLon2) * (dLat1 - ALat) ) / (dLat1 - dLat2);
	                 // 如果交点在A点左侧（说明是做射线与 边的交点），则射线与边的全部交点数加一：
	                 if (dLon < ALon) {
	                     iSum++;
	                 }
	             }
	         }
	     }
	   //  System.out.println("iSum="+iSum);
	     return (iSum % 2) != 0;
	 }

	 public static boolean  ptInPolygon (LatLonPoint p, LatLonPoint[] ptPolygon, int nCount){
		 int nCross = 0;
		 for (int i = 0; i < nCount; i++){
			 LatLonPoint p1 = ptPolygon[i];
			 LatLonPoint p2 = ptPolygon[(i + 1) % nCount];

			 // 求解 y=p.y 与 p1p2 的交点
			 if ( p1.getLatitude() == p2.getLatitude() ) // p1p2 与 y=p0.y平行
				 continue;
			 if ( p.getLatitude() < getminValue(p1.getLatitude(), p2.getLatitude()) ) // 交点在p1p2延长线上
				 continue;
			 if ( p.getLatitude() >= getmaxValue(p1.getLatitude(), p2.getLatitude()) ) // 交点在p1p2延长线上
				 continue;
			 // 求交点的 X 坐标 --------------------------------------------------------------
			 double x = (double)(p.getLatitude() - p1.getLatitude()) * (double)(p2.getLongitude() - p1.getLongitude()) / (double)(p2.getLatitude() - p1.getLatitude()) + p1.getLongitude();
			 if ( x > p.getLongitude())
				 nCross++; // 只统计单边交点
			 }
		 	// 单边交点为偶数，点在多边形之外 ---
		 System.out.println(nCross);
		 	return (nCross % 2 == 1);
	 } 
	 

	private static double getminValue(double latitude, double latitude2) {
		if(latitude<latitude2){
			return latitude;
		}
		return latitude2;
	}

	private static double getmaxValue(double latitude, double latitude2) {
		if(latitude>latitude2){
			return latitude;
		}
		return latitude2;
	}
	
	public static void main(String[] args) {
		 double weight = 1580.0;
		 double height = 950.0;
		 LatLonPoint[] points = {
			new  LatLonPoint(652/weight,0/height),
			new  LatLonPoint(686/weight,69/height),
			new  LatLonPoint(666/weight,243/height),
			new  LatLonPoint(617/weight,306/height),
			new  LatLonPoint(571/weight,356/height),
			new  LatLonPoint(539/weight,480/height),
			new  LatLonPoint(434/weight,633/height),
			new  LatLonPoint(350/weight,747/height),
			new  LatLonPoint(349/weight,930/height),
			new  LatLonPoint(1286/weight,933/height),
			new  LatLonPoint(1286/weight,811/height),
			
			new  LatLonPoint(1276/weight,685/height),
			new  LatLonPoint(1182/weight,607/height),
			new  LatLonPoint(1137/weight,519/height),
			new  LatLonPoint(1077/weight,404/height),
			new  LatLonPoint(1032/weight,321/height),
			new  LatLonPoint(984/weight,248/height),
			new  LatLonPoint(938/weight,189/height),
			new  LatLonPoint(938/weight,0/height)
			
		 };
		/* double x=0.662;
		 double y= 0.7792;
		 double z=0.7666;
		 double w =0.8444;*/
		 double x=0.6698;
		 double y=0.7005;
		 double z=0.2517;
		 double w=0.2701;
		 for(LatLonPoint p :points){
			 System.out.println(p);
		 }
		 
		 System.out.println(x+(y-x)/2);
		 System.out.println(z+(w-z)/2);
		 System.out.println(new GraphicsUtils().isPointInPolygon(new LatLonPoint(x+(y-x)/2,z+(w-z)/2),points));
	 }
}
