package com.lujie.common.constant;

import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.lujie.common.util.RequestWrapper;
import com.lujie.common.util.StringUtil;

import net.sf.json.JSONObject;

public class HttpContentType {
	
	/**资源文件*/
	public static final String TextPlain="text/plain";
	/**网页*/
	public static final String TextHtml="text/html";
	/** 表单*/
	public static final String ApplicationForm="application/x-www-form-urlencoded";
	/**上传文件*/
	public static final String MultipartFormData="multipart/form-data";
	/**二进制流*/
	public static final String ApplicationOctetStream="application/octet-stream";
	/**json*/
	public static final String ApplicationJSON="application/json";
	
	public static Boolean isJsonContentTypeRequest(HttpServletRequest request){
		String contentType=request.getContentType();
		if(contentType!=null && contentType.toUpperCase().contains(ApplicationJSON.toUpperCase())){
			return true;
		}
		return false;
	}
	public static Boolean isFormContentTypeRequest(HttpServletRequest request){
		String contentType=request.getContentType();
		if(contentType!=null && contentType.toUpperCase().contains(ApplicationForm.toUpperCase())){
			return true;
		}
		return false;
	}
	
	public static String getRequestBodyStr(byte[] allbytes){
		byte[] buffer = new byte[1024];
        StringBuilder sb=new StringBuilder();
        int index=0;
    	while((index+buffer.length)<allbytes.length){
    		sb.append(new String(allbytes, index, buffer.length));
    		index+=buffer.length;
    	} 
    	sb.append(new String(allbytes, index, allbytes.length-index));
    	return sb.toString();
	}
	
	public static String formatRequest(String requestBody){
		if(requestBody==null) return null;
		
		JSONObject jsonObject=JSONObject.fromObject(requestBody);
		
		JSONObject formatJsonObject=JSONObject.fromObject(requestBody);
		
		for(Object key:jsonObject.keySet()){
			Object value=jsonObject.get(key);
			if(value!=null && value instanceof java.lang.String){
				value=value.toString().trim();
			}
			formatJsonObject.put(key, value);
		}
		
		return formatJsonObject.toString();
	}
	
	public static Map getRequestHeaderMap(HttpServletRequest request){
		Map headerMap=new HashMap();
		Enumeration enumeration=request.getHeaderNames();
		
		while(enumeration.hasMoreElements()){
			String name=enumeration.nextElement().toString();
			headerMap.put(name, request.getHeader(name));
		}
		return headerMap;
	}
	
	public static String jsonParamToformParam(String jsonParam) {
		
		if(StringUtil.isEmpty(jsonParam)){
			return "";
		}
		
		JSONObject jsonObject=JSONObject.fromObject(jsonParam);
		StringBuilder formParams=new StringBuilder();
		Set set=jsonObject.keySet();
		if(set.size()>0){
			for(Object key:set){
				Object objvalue=jsonObject.get(key);
				if(objvalue==null){
					objvalue="";
				}
				formParams.append(key+"="+objvalue+"&");
			}
			formParams.delete(formParams.length()-1, formParams.length());
			
		}
		return formParams.toString();
	}
	
	public static String getParamStr(HttpServletRequest request) throws Exception{
		StringBuffer sbParam=new StringBuffer();
		
		String contentType=request.getContentType();
		if(contentType!=null){
			if(contentType.toUpperCase().contains(MultipartFormData.toUpperCase())
					||contentType.toUpperCase().contains(ApplicationOctetStream.toUpperCase())	){
				//不处理上传文件、二进制文件请求
				return null;
			}
			if(contentType.toUpperCase().contains(ApplicationJSON.toUpperCase())){
				
				//MyHttpServletRequestWrapper  requestWrapper =new MyHttpServletRequestWrapper(request);
				//String requestBody=getRequestBodyStr(requestWrapper.getBody());
				
				RequestWrapper requestWrapper=new RequestWrapper(request);
				String requestBody=requestWrapper.getBodyString();
				requestBody=formatRequest(requestBody);
				
				/*
				StringBuilder sb=new StringBuilder();
				InputStream is = request.getInputStream();
				byte[] buffer = new byte[1024];
				int size = 0;
				while ((size = is.read(buffer)) > 0) {
				    sb.append(new String(buffer, 0, size));
				}
				return sb.toString();*/
				
				return requestBody;
			}
		}
		String reqMethod=request.getMethod();
		if(reqMethod.toUpperCase().equals("GET")){
			RequestWrapper requestWrapper=new RequestWrapper(request);
			
			Map paramMap=requestWrapper.getParameterMap();
			Iterator iterator=paramMap.entrySet().iterator();
			while(iterator.hasNext()){
				Map.Entry<String,Object> entry=(Map.Entry<String,Object>)iterator.next();
				Object val=entry.getValue();
				String[] value=(String[])val;
				String encodeParamVal=URLEncoder.encode(value[0].trim(),"utf-8");
				sbParam.append(entry.getKey()+"="+encodeParamVal+"&");
				
			}
		}else{
			RequestWrapper requestWrapper=new RequestWrapper(request);
			String requestBody=requestWrapper.getBodyString().trim();
			sbParam.append(requestBody);
			/*
			String id=request.getParameter("id");
			Map paramMap=request.getParameterMap();
			Iterator iterator=paramMap.entrySet().iterator();
			while(iterator.hasNext()){
				Map.Entry<String,Object> entry=(Map.Entry<String,Object>)iterator.next();
				Object val=entry.getValue();
				String[] value=(String[])val;
				//sbParam.append(entry.getKey()+"="+value[0]+"&");
				
				String encodeParamVal=URLEncoder.encode(value[0].trim(),"utf-8");
				sbParam.append(entry.getKey()+"="+encodeParamVal+"&");
			}*/
		}
		
		return sbParam.toString();
	}
	
	
	
}
