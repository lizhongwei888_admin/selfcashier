package com.lujie.common.util;

/**
 * ClassName:LatLonPoint <br/>
 * Function: TODO <br/>
 */
public class LatLonPoint {

	public LatLonPoint(double longitude, double latitude) {
		super();
		this.longitude = longitude;
		this.latitude = latitude;
	}

	private double longitude;
	private double latitude;

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	@Override
	public String toString() {
		return "LatLonPoint [longitude=" + longitude + ", latitude=" + latitude
				+ "]";
	}
	
	
}
