package com.lujie.common.util;

import java.net.URI;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.Set;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;

public class HttpClientUtil {
	
	static Logger logger = LoggerFactory.getLogger(HttpClientUtil.class);
	
	public static String doGet(String url , Map<String ,String > param) {
		CloseableHttpClient httpClient = HttpClients.createDefault();
		String resultString ="";
		CloseableHttpResponse response = null ;
		try {
			URIBuilder builder = new URIBuilder(url);
			if(param !=null) {
				for(String key : param.keySet()) {
					builder.addParameter(key, param.get(key));
				}
			}
			URI uri = builder.build() ;
			HttpGet httpGet = new HttpGet(uri);
			RequestConfig requestConfig =  RequestConfig.custom()
					.setConnectTimeout(11000).build();
			httpGet.setConfig(requestConfig);
			response = httpClient.execute(httpGet);
			if(response.getStatusLine().getStatusCode()==200) {
				resultString = EntityUtils.toString(response.getEntity(),"UTF-8");
			}
		}catch(Exception e) {
			logger.error("http get 请求异常：", e);
			LogUtils.printStackTrace(e);
		}finally {
			try {
				if(response!=null) {
					response.close();
				}
				httpClient.close();
			}catch (Exception e) {
				logger.error("http get 请求异常：", e);
				// TODO: handle exception
				LogUtils.printStackTrace(e);
			}
		}
		return resultString;
	}
	
	public static String doGet(String url) {
		logger.debug("get url :{}" ,url );
		return doGet(url, null);
	}
	
	/**
	 * application/json request
	 * @param url
	 * @param jsonstr
	 * @param charset
	 * @return
	 */
	public static String doPostJson(String url, String jsonstr, String charset) {
		CloseableHttpClient httpClient = null;
		HttpPost httpPost = null;
		String result = null;
		CloseableHttpResponse response=null;
		try {
			SslUtils.ignoreSsl();

			httpClient = new SSLClient();
			httpPost = new HttpPost(url);
			RequestConfig requestConfig =  RequestConfig.custom()
					.setConnectTimeout(11000).build();
			httpPost.setConfig(requestConfig);
			httpPost.addHeader("Content-Type", "application/json; charset=utf-8");
			httpPost.setHeader("Accept", "application/json");
			StringEntity se = new StringEntity(jsonstr, Charset.forName("UTF-8"));
			se.setContentType("text/json");
			se.setContentEncoding(new BasicHeader("Content-Type",
					"application/json"));
			httpPost.setEntity(se);

			response = httpClient.execute(httpPost);
			if (response != null) {
				HttpEntity resEntity = response.getEntity();
				if (resEntity != null) {
					result = EntityUtils.toString(resEntity , charset);
				}
			}
			
		} catch (Exception e) {
			logger.error("http json 请求异常：", e);
			LogUtils.printStackTrace(e);
		}finally{
			try {
				if(response!=null) {
					response.close();
				}
				httpClient.close();
			}catch (Exception e) {
				logger.error("http json 请求异常：", e);
				// TODO: handle exception
				LogUtils.printStackTrace(e);
			}
		}
		return result;
	}
	
	
	/**
	 * application/json request
	 * @param url
	 * @param jsonstr
	 * @param charset
	 * @return
	 */
	public static String doPostJson(String url, String jsonstr, String charset,Map<String, String> header) {
		CloseableHttpClient httpClient = null;
		HttpPost httpPost = null;
		String result = null;
		CloseableHttpResponse response=null;
		try {
			httpClient = HttpClients.createDefault(); 
			httpPost = new HttpPost(url);
			RequestConfig requestConfig =  RequestConfig.custom()
					.setConnectTimeout(11000).build();
			httpPost.setConfig(requestConfig);
			httpPost.addHeader("Content-Type", "application/json; charset=utf-8");
			if(header != null) {
				Set<String> keySet = header.keySet();  
		        for(String key : keySet) {  
		        	httpPost.addHeader(key,header.get(key));
		        }  
		          
			}
			httpPost.setHeader("Accept", "application/json");
			StringEntity se = new StringEntity(jsonstr, Charset.forName("UTF-8"));
			se.setContentType("text/json");
			se.setContentEncoding(new BasicHeader("Content-Type",
					"application/json"));
			httpPost.setEntity(se);
			response = httpClient.execute(httpPost);
			if (response != null) {
				HttpEntity resEntity = response.getEntity();
				if (resEntity != null) {
					result = EntityUtils.toString(resEntity , charset);
				}
			}
			
		} catch (Exception e) {
			logger.error("http json 请求异常：", e);
			LogUtils.printStackTrace(e);
		}finally{
			try {
				if(response!=null) {
					response.close();
				}
				httpClient.close();
			}catch (Exception e) {
				logger.error("http json 请求异常：", e);
				// TODO: handle exception
				LogUtils.printStackTrace(e);
			}
		}
		return result;
	}
	
	
}
