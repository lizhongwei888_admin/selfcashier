package com.lujie.common.util;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;


/**
 * 
 * @author user
 *
 */
public class XmlHelper {
	

	public static <T> String toXMLWithJAXB(T obj){
		Marshaller marshaller = null;
		try {
			JAXBContext context = JAXBContext.newInstance(obj.getClass());
			marshaller = context.createMarshaller();
//			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		} catch (JAXBException e) {
			throw new RuntimeException(e);
		}
        try (StringWriter sw = new StringWriter();){
			marshaller.marshal(obj,sw);
			return sw.toString();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T toObjectWithJAXB(String xml, Class<T> clazz) {
		Unmarshaller unmarshaller = null;
		try {
			JAXBContext context = JAXBContext.newInstance(clazz);
			unmarshaller = context.createUnmarshaller();
		} catch (JAXBException e) {
			throw new RuntimeException(e);
		}
		try (InputStream is = new ByteArrayInputStream(xml.getBytes("UTF-8"))) {
			T obj = (T)unmarshaller.unmarshal(is);
			return obj;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
