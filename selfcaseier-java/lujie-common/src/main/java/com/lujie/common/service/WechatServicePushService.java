package com.lujie.common.service;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lujie.common.entity.EntityMap;
import com.lujie.common.util.HttpRequest;
import com.lujie.common.util.JsonHelper;
import com.lujie.common.util.StringUtil;
import com.lujie.common.util.WechatUtil;


public class WechatServicePushService {
	private static final Logger log = LoggerFactory.getLogger(WechatServicePushService.class);
	
	public HashMap<String,String> getWechatServiceParamValue(HashMap<String,String> businessDataMap){
		HashMap wechatServiceParamValue=new HashMap();
		for(Object key:businessDataMap.keySet()){
			Object value=businessDataMap.get(key);
			if(value==null) value="";
			
			HashMap paraValueMap=new HashMap();
			paraValueMap.put("value", value);
			wechatServiceParamValue.put(key, paraValueMap);
		}
		return wechatServiceParamValue;
	}
	
	
	public String getWechatServiceNotifyAccessToken(String appid,String secret){
		String accessToken=null;
		try {
			accessToken=WechatUtil.getWechatServiceNotifyAccessToken(appid,secret);
		} catch (Exception ex) {
			log.info("获取accessToken异常,停止推送:"+StringUtil.getExceptionDesc(ex));
			return null;
		}
		log.info("获取accessToken:"+accessToken);
		accessToken=EntityMap.GetEntityMapByJsonStr(accessToken).getStr("access_token");
		
		if(StringUtil.isEmpty(accessToken)){
			return null;
		}
		return accessToken;
	}
	/**
	 * 
	 * @param appid 小程序appid  wx0d4011bd0bdf84bc
	 * @param secret 小程序密钥   65fb95458c1f782b02287a3ae6ae86bc
	 * @param openId 用户openId   xxxxxxxxxxxx
	 * @param templateId 模板id  18TMbA46ijhCNibg_XbosQnvuCAZFSJWU96GWBDWL1A
	 * @param businessDataMap 模板参数  键值对形式
	 * @param smallProgramUrl 跳转到小程序页面地址可以包含参数 例如:/test?token=1111&bgOrderId=2
	 */
	public void push(String appid,String secret,
			String openId,
			String templateId,HashMap<String,String> businessDataMap,
			String smallProgramUrl){
		
		String accessToken=getWechatServiceNotifyAccessToken( appid, secret);
		if(StringUtil.isEmpty(accessToken)){
			log.info("获取accessToken失败，停止推送");
			return ;
		}
		
		try {
			String requestUrl="https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token="+accessToken;
			HashMap<String,String> requestHeadMap=WechatUtil.getRequestHeadMap();
			HashMap dataMap=new HashMap();
			
			dataMap.put("touser", openId);
			dataMap.put("template_id", templateId);
			dataMap.put("page", smallProgramUrl);
			
			dataMap.put("miniprogram_state", "formal");//注意改成正式版，跳转小程序类型：developer为开发版；trial为体验版；formal为正式版；默认为正式版

			businessDataMap=getWechatServiceParamValue(businessDataMap);
			dataMap.put("data", businessDataMap);
			
			String param=JsonHelper.toJson(dataMap);
			log.info("推送参数内容:"+param);
			
			String result=HttpRequest.sendPost(requestHeadMap,requestUrl, param);
			log.info("调用微信服务通知返回结果:"+result);
			
		} catch (Exception ex) {
			log.info("微信服务通知推送失败:"+StringUtil.getExceptionDesc(ex));
		}
        
	}
	
	public static String push(String template_id,HashMap businessDataMap,String openId,String accessToken,String smallProgramUrl) throws Exception{
		String requestUrl="https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token="+accessToken;
		HashMap<String,String> requestHeadMap=WechatUtil.getRequestHeadMap();
		HashMap dataMap=new HashMap();
		
		dataMap.put("touser", openId);
		dataMap.put("template_id", template_id);
		dataMap.put("page", smallProgramUrl);
		
		dataMap.put("miniprogram_state", "formal");//注意改成正式版，跳转小程序类型：developer为开发版；trial为体验版；formal为正式版；默认为正式版

		
		dataMap.put("data", businessDataMap);
		
		String param=JsonHelper.toJson(dataMap);
		log.info("推送参数内容:"+param);
		
		String result=HttpRequest.sendPost(requestHeadMap,requestUrl, param);
		log.info("调用微信服务通知返回结果:"+result);
		return result;
	}
	
	public static void main(String[] args){
		
		HashMap<String,String> businessDataMap=new HashMap<>();
		businessDataMap.put("mobile", "13011112222");
		businessDataMap.put("address", "usa");
		
		
		new WechatServicePushService().push("wx0d4011bd0bdf84bc","65fb95458c1f782b02287a3ae6ae86bc",
				"xxxxxxxxxxxx",
				"18TMbA46ijhCNibg_XbosQnvuCAZFSJWU96GWBDWL1A",businessDataMap,
				"/test?token=1111&bgOrderId=2");
		
	}
	
	
	
}
