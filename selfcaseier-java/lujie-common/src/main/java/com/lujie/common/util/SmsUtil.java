package com.lujie.common.util;
import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;

/**
 * 短信发送工具类
 * @author hupei
 *
 */
public class SmsUtil {
  private static String send_message_url;
  private static String sms_number_url;
  private static String account;
  private static String token;
  private static String send_message_state;
	public static String getSend_message_url() {
		return send_message_url;
	}
	public static void setSend_message_url(String send_message_url) {
		SmsUtil.send_message_url = send_message_url;
	}
	public static String getSms_number_url() {
		return sms_number_url;
	}
	public static void setSms_number_url(String sms_number_url) {
		SmsUtil.sms_number_url = sms_number_url;
	}
	public static String getSend_message_state() {
		return send_message_state;
	}
	public static void setSend_message_state(String send_message_state) {
		SmsUtil.send_message_state = send_message_state;
	}
	public static String getAccount() {
		return account;
	}
	public static void setAccount(String account) {
		SmsUtil.account = account;
	}
	public static String getToken() {
		return token;
	}
	public static void setToken(String token) {
		SmsUtil.token = token;
	}
	/**
	 * 发送短信
	 * @param params
	 * @return
	 * @throws Exception 
	 */
	public JSONObject sendSms(String mobiles , String content) throws Exception{
		Map<String, Object> params = new HashMap<String , Object>();
		params.put("account", account);
		params.put("token" , token);
		params.put("mobile" , mobiles);
		params.put("content", "【陆杰科技】"+content);
		params.put("type" , 0);
		return HttpUtilClient.jsonPost(send_message_url, params, "");
	}
	
	/**
	 * 查询短信量
	 * @param params
	 * @return
	 * @throws Exception 
	 */
	public JSONObject selectSmsnums() throws Exception{
		Map<String, Object> params = new HashMap<String , Object>();
		params.put("account", account);
		params.put("token" , token);
		params.put("type" , 0);
		return HttpUtilClient.jsonPost(sms_number_url, params, "");
	}

	/**
	 * 查询短信状态
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public JSONObject selectSmsstate(String smsIds) throws Exception{
		Map<String, Object> params = new HashMap<String , Object>();
		params.put("smsId", smsIds);
		params.put("account", account);
		params.put("token" , token);
		params.put("type" , 0);
		return HttpUtilClient.jsonPost(send_message_state, params, "");
	}
}
