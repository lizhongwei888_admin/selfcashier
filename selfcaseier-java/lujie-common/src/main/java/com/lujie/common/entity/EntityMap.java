package com.lujie.common.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.lujie.common.util.DateUtil;
import com.lujie.common.util.MD5Util;
import com.lujie.common.util.StringUtil;

import net.sf.json.JSONObject;


public class EntityMap extends TreeMap{

	protected Integer pageNum=1;//页面的第几页
	protected Integer pageSize=10;//页面的每页记录条数
	protected Integer rowIndex=0;//表的记录的位置
	protected String dataId;
	
	public EntityMap(){}
	public String getStr(String key){
		Object objValue=get(key);
		if(objValue!=null) return objValue.toString();
		
		return null;
	}
	public String getNotNullStr(String key){
		Object objValue=get(key);
		if(objValue!=null) return objValue.toString();
		
		return "";
	}
	public Integer getInt(String key){
		Object objValue=get(key);
		if(objValue!=null) return Integer.parseInt(objValue.toString());
		
		return null;
	}
	public Long getLong(String key){
		Object objValue=get(key);
		if(objValue!=null) return Long.parseLong(objValue.toString());
		
		return null;
	}
	
	public Double getDouble(String key){
		Object objValue=get(key);
		if(objValue!=null) return Double.parseDouble(objValue.toString());
		
		return null;
	}
	public Boolean getBoolean(String key){
		Object objValue=get(key);
		if(objValue!=null) return Boolean.parseBoolean(objValue.toString());
		
		return null;
	}
	
	public Boolean isStringType(String key){
		Object objValue=get(key);
		if(objValue==null) return null;
		
		if(objValue instanceof String){
			return true;
		}
		return false;
	}
	
	public BigDecimal getBigDecimal(String key){
		Object objValue=get(key);
		if(objValue==null) return null;
		
		BigDecimal dataBigDecimal=new BigDecimal(objValue.toString());
		dataBigDecimal=dataBigDecimal.setScale(2, BigDecimal.ROUND_HALF_DOWN);
		return dataBigDecimal;
	}
	public Double getDouble(String key,Integer num){
		Object objValue=get(key);
		if(objValue==null) return null;
		
		BigDecimal dataBigDecimal=new BigDecimal(objValue.toString());
		dataBigDecimal=dataBigDecimal.setScale(num, BigDecimal.ROUND_HALF_DOWN);
		return dataBigDecimal.doubleValue();
	}
	public void putDouble(String key,Object objValue,Integer num){
		if(objValue==null) return;
		
		BigDecimal dataBigDecimal=new BigDecimal(objValue.toString());
		dataBigDecimal=dataBigDecimal.setScale(num, BigDecimal.ROUND_HALF_DOWN);
		put(key,dataBigDecimal.doubleValue());
	}
	
	public Date getDate(String key,String pattern){
		Object objValue=get(key);
		if(objValue==null) return null;
		
		Date date=null;
		try{
			date= DateUtil.parserDate(objValue.toString(), pattern);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return date;
	}
	public String getDateStrFromDate(String key,String pattern){
		Object objValue=get(key);
		if(objValue==null) return null;
		
		Date date=null;
		String dateStr=null;
		try{
			date=(Date)objValue;
			dateStr= DateUtil.formatDate(date, pattern);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return dateStr;
	}
	
	public String getDateStrFromStr(String key,String pattern){
		Object objValue=get(key);
		if(objValue==null) return null;
		
		Date date=null;
		String dateStr=null;
		try{
			date=DateUtil.parserDate(objValue.toString());
			dateStr= DateUtil.formatDate(date, pattern);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return dateStr;
	}
	
	/**
	 * 生成数据id
	 * @return
	 */
	public String generateDataId(){
		StringBuilder datas=new StringBuilder();
		Set set=keySet();
		
		List<String> notDataIds=Arrays.asList(new String[]{
				"id","dataId","isValid","remark",
				"pageNum","pageSize",
				"createTime","createTimeStr","createUser","createUserName",
				"modifyTime","modifyTimeStr","modifyUser","createUserName"});
		
		
		if(set==null || set.isEmpty()) return null;
		for(Object key:set){
			if(key==null) continue;
			if(notDataIds.contains(key.toString())) continue;
			
			datas.append(get(key));
		}
		dataId=MD5Util.encrypt(datas.toString());
		put("dataId",dataId);
		return dataId;
	}
	
	public String generateDataIdV2(){
		StringBuilder datas=new StringBuilder();
		Set set=keySet();
		
		List<String> notDataIds=Arrays.asList(new String[]{
				"id","dataId","pageNum","pageSize",
				"createTime","createTimeStr","createUser","createUserName",
				"modifyTime","modifyTimeStr","modifyUser","createUserName"});
		
		
		if(set==null || set.isEmpty()) return null;
		for(Object key:set){
			if(key==null) continue;
			if(notDataIds.contains(key.toString())) continue;
			
			datas.append(key+","+get(key));
		}
		dataId=MD5Util.encrypt(datas.toString());
		put("dataId",dataId);
		return dataId;
	}
	
	/**
	 * 生成数据id,但是不包含一些字段
	 * @param excludeColumnList
	 * @return
	 */
	public String generateDataId(List<String> excludeColumnList){
		StringBuilder datas=new StringBuilder();
		Set set=keySet();
		if(set==null || set.isEmpty()) return null;
		for(Object key:set){
			if(key==null) continue;
			if(key.toString().equals("pageNum") || key.toString().equals("pageSize")) continue;
			
			if(excludeColumnList.contains(key)) continue;
			
			datas.append(get(key));
		}
		dataId=MD5Util.encrypt(datas.toString());
		put("dataId",dataId);
		return dataId;
	}
	
	public String generateSignParam(){
		StringBuilder signparams=new StringBuilder();
		Set set=keySet();
		if(set==null || set.isEmpty()) return null;
		for(Object key:set){
			if(key==null) continue;
			
			signparams.append(key+"="+get(key)+"&");
		}
		signparams.delete(signparams.length()-1, signparams.length());
		return signparams.toString();
	}
	
	public static void main(String[] args) {
		EntityMap map=new EntityMap();
		map.put("a", 1);
		map.put("b", 2);
		String dataId=map.generateDataId(Arrays.asList("c"));
		System.out.println(map);
		dataId=map.generateDataId();
		System.out.println(map);
		
		
		
	}
	/**
	 * 获取分页的数据在数据库表中中的行记录位置
	 * @return
	 */
	public Integer getRowIndex() {
		pageNum=getInt("pageNum");
		pageSize=getInt("pageSize");
		if(pageNum==null){
			pageNum=1;
		}
		if(pageSize==null){
			pageSize=10;
		}
		rowIndex=(pageNum-1)*pageSize;
		return rowIndex;
	}
	
	public void setRowIndex(){
		rowIndex=getRowIndex();
		put("rowIndex",rowIndex);
		
		pageNum=getInt("pageNum");
		pageSize=getInt("pageSize");
		if(pageNum==null){
			pageNum=1;
		}
		if(pageSize==null){
			pageSize=10;
		}
		put("pageNum",pageNum);
		put("pageSize",pageSize);
		
	}
	
	public void put(String key, Object value) {
		super.put(key, value);
		
		if(key!=null && key.equals("pageSize")){
			if(get("rowIndex")==null){
				setRowIndex();
			}
		}
    }
	
	
	public Integer getPageNum() {
		pageNum=getInt("pageNum");
		return pageNum;
	}
	public Integer getPageSize() {
		pageSize=getInt("pageSize");
		return pageSize;
	}
	public void setPageNum(Integer pageNum) {
		put("pageNum",pageNum);
	}
	public void pageNumAddOne() {
		Integer pageNum=getInt("pageNum");
		if(pageNum==null){
			pageNum=0;
		}
		pageNum++;
		put("pageNum",pageNum);
		setRowIndex();
	}
	public void setPageSize(Integer pageSize) {
		put("pageSize",pageSize);
	}
	
	public List getPageList(List list){
		Integer startIndex=getRowIndex();
		Integer endIndex=startIndex+getPageSize();
		
		List resultList=new ArrayList();
		if(list==null || list.size()==0) return resultList;
		
		if(endIndex>list.size()){
			endIndex=list.size();
		}
		if(startIndex<=endIndex){
			resultList=list.subList(startIndex, endIndex);
		}
		return resultList;
	}
	
	public void jsonToEntity(JSONObject json){
		if(json==null) return ;
		
		for(Object key:json.keySet()){
			put(key,json.get(key));
		}
	}
	public JSONObject toJson(){
		
		JSONObject json=new JSONObject();
		
		for(Object key:this.keySet()){
			json.put(key, get(key));
		}
		return json;
	}
	
	public static EntityMap GetEntityMapByJsonStr(String jsonStr){
		EntityMap entityMap=new EntityMap();
		if(StringUtil.isEmpty(jsonStr)) return entityMap;
		
		JSONObject jsonObject=JSONObject.fromObject(jsonStr);
		entityMap.jsonToEntity(jsonObject);
		return entityMap;
	}
	public EntityMap getEntityMap(String key){
		return (EntityMap)get(key);
	}
	
	public static EntityMap toEntityMap(Map map){
		EntityMap entityMap=new EntityMap();
		if(map==null || map.isEmpty()) return entityMap;
		
		for(Object objKey:map.keySet()){
			entityMap.put(objKey, map.get(objKey));
		}
		return entityMap;
	}
	public static EntityMap toEntityMap(JSONObject json){
		EntityMap entityMap=new EntityMap();
		if(json==null || json.isEmpty()) return entityMap;
		
		for(Object objKey:json.keySet()){
			entityMap.put(objKey, json.get(objKey));
		}
		return entityMap;
	}
	/**
	 * 将每条记录的某一列的值作为map的key,这条记录作为map的value
	 * @param list
	 * @param keyName
	 * @return
	 */
	public static EntityMap toEntityMap(List<EntityMap> list,String keyName){
		EntityMap entityMap=new EntityMap();
		if(list==null || list.isEmpty()) return entityMap;
		
		for(EntityMap map:list){
			entityMap.put(map.get(keyName),map);
		}
		return entityMap;
	}
	
	public void putCommonNewData(String userId,String userName){
		generateDataId();
		put("createTime", new Date());
		put("isValid", 1);
		put("createUser", userId);
		put("createUserName", userName);
		put("modifyTime", new Date());
		put("modifyUser", userId);
		put("modifyUserName", userName);
	}
	public void putCommonUpdateData(String userId,String userName){
		put("modifyTime", new Date());
		put("modifyUser", userId);
		put("modifyUserName", userName);
	}
	
	public void updateEntityMap(EntityMap newEntityMap){
		for(Object key:newEntityMap.keySet()){
			put(key.toString(),newEntityMap.get(key));
		}
	}
	
	public static EntityMap formatEntityMapKey(EntityMap map){
		EntityMap formatMap=new EntityMap();
		
		for(Object key:map.keySet()){
			formatMap.put(key.toString().toUpperCase(),map.get(key));
		}
		
		return formatMap;
	}
	
}



