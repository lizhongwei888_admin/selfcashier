package com.lujie.common.exception;


import java.sql.SQLTransientConnectionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.lujie.common.constant.ResultMsg;
import com.lujie.common.util.JsonHelper;
import com.lujie.common.util.StringUtil;

@RestControllerAdvice
public class BaseException {
	private static final Logger log = LoggerFactory.getLogger(BaseException.class);
	
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public String ErrorHandler(Exception e) {
        String errormsg="";
        if(e instanceof HttpRequestMethodNotSupportedException){
        	errormsg=JsonHelper.toJson(new ResultMsg("0","请求方式有误！"));
        }else if(e instanceof CannotGetJdbcConnectionException){
        	errormsg=JsonHelper.toJson(new ResultMsg("510","连接数据库异常！"));
        }else if(e instanceof SQLTransientConnectionException){
        	errormsg=JsonHelper.toJson(new ResultMsg("511","连接数据库异常！"));
        }else if(e instanceof DuplicateKeyException){
    		errormsg=JsonHelper.toJson(new ResultMsg("510","拦截重复保存！"));
    		return errormsg;
    	}else{
        	String msg=StringUtil.getExceptionDesc(e);
        	log.info(msg);
        	errormsg=JsonHelper.toJson(new ResultMsg("500","异常！"+StringUtil.getLimitLenStr(msg, 300, "...")));
        }
        return errormsg;
    }
}