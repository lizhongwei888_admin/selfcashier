package com.lujie.common.util;
 
import java.lang.reflect.Field;
import java.lang.reflect.Method;
public class Constant
{
	/**
	 * 根据Key得到对应的name名
	 * 
	 * @author mingfang
	 * 
	 */
	public static class getValueNameByKey
	{

		public static String getValueNameByKey(String constantName, String key)
		{

			constantName = Constant.class.getName() + "$" + constantName;
			String name = "";
			try
			{
				Class constantClass = Class.forName(constantName);
				Object[] args = new Object[1];
				Field[] field = constantClass.getDeclaredFields();
				Method method = null;
				if (field.length > 0)
				{
					// 确认第一个变量的值的类型即可
					String typeName = field[0].getType().getName();
					if (typeName.equals("long") || typeName.equals("int"))
					{
						if(key==null||key.trim().length()==0){
							args[0]=-1l;
						}else{
							args[0] = Long.valueOf(key);
						}
						method = constantClass.getMethod("getName", long.class);
					}
					else if (typeName.equals("char"))
					{
						args[0] = key;
						method = constantClass.getMethod("getName", char.class);
					}
					else
					{
						args[0] = key;
						method = constantClass.getMethod("getName", String.class);
					}
				}

				name = (String) method.invoke(constantClass, args);
			}
			catch (ClassNotFoundException e)
			{
				return null;
			}			
			catch (Exception e)
			{
				e.printStackTrace();
			}
			return name;
		}
	}
	

}