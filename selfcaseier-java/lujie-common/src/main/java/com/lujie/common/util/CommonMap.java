package com.lujie.common.util;

import java.util.HashMap;

public class CommonMap extends HashMap {

	public CommonMap(){
		put("pageNum", 1);
		put("rowIndex", 0);
		put("pageSize", 10);
	}
	public String getStr(String key){
		Object objValue=get(key);
		if(objValue!=null) return objValue.toString();
		
		return null;
	}
	public Integer getInt(String key){
		Object objValue=get(key);
		if(objValue!=null) return Integer.parseInt(objValue.toString());
		
		return null;
	}
	public Double getDouble(String key){
		Object objValue=get(key);
		if(objValue!=null) return Double.parseDouble(objValue.toString());
		
		return null;
	}
	
	public static void main(String[] args) {
		CommonMap commonMap=new CommonMap();
		System.out.println(commonMap);
	}

}
