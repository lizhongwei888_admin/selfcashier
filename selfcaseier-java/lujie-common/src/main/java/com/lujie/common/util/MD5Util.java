package com.lujie.common.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


/**
 * <pre>
 *
 * @author: simple
 * @date: 2016年8月27日 下午6:56:14
 * </pre>
 */
public class MD5Util {

	public static String encrypt(String source){
		byte[] bytes = digest(source);
		StringBuffer sb = new StringBuffer();
		for (byte b : bytes) {
			sb.append(String.format("%02x", b&0xff));
		}
		return sb.toString();
	}
	
	public static byte[] digest(String source){
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(source.getBytes("UTF-8"));
			return md.digest();
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static void main(String[] args) {
		String account="A";
		String secret="ed1f42cb243441f6829529634085393b";
		String timestamp=System.currentTimeMillis()+"";
		System.out.println(timestamp);
		String sign=MD5Util.encrypt(account+secret+timestamp);
		System.out.println(sign);
	} 
	
}

