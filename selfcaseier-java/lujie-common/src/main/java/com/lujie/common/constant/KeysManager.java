package com.lujie.common.constant;

public class KeysManager {
	
	/** 微服务地址前缀标记*/
	public static final String MicroServicePrefix="/micro/";
	public static final String VerifyCode="VerifyCode_";
	public static final String Platform="Platform_";
	public static final String CodeTbale="CodeTbale_";
	
	
	/**
	 * 系统业务标记key
	 */
	public static final String Admin="admin";
	public static final String AdminRoleId="a594ce962faa404babe4c8ce2ccaf117";//超级管理员角色
	public static final String USysType="USysType"	;
	public static final String UDataType="UDataType"	;
	public static final String SEX="SEX";
	
	public static final String City="CITY";
	public static final String District="DISTRICT";
	public static final String Street="STREET";
	public static final String community="COMMUNITY";
	public static final String OperationSite="OPERATIONSITE";
	
	
	/**
	 * 缓存key
	 */
	public static final String CacheKey_Platform_Basetable_USysType="Platform_Basetable_USysType";
	public static final String CacheKey_Platform_Basetable_UDataType="Platform_Basetable_UDataType";
	public static final String CacheKey_Platform_Basetable_SEX="Platform_Basetable_SEX"; 	
	
	
	
	public static final String CacheKey_Platform_AllDepts="Platform_AllDepts";
	public static final String CacheKey_Platform_UserDepts="Platform_UserDepts_";
	
	public static final String CacheKey_Platform_AllRole="CacheKey_Platform_AllRole";
	
	public static final String CacheKey_Platform_Site_All="CacheKey_Platform_Site_All";
	public static final String CacheKey_Platform_Street_All="CacheKey_Platform_Street_All";
	public static final String CacheKey_Platform_District_All="CacheKey_Platform_District_All";
	public static final String CacheKey_Platform_City_All="CacheKey_Platform_City_All";
	public static final String CacheKey_Platform_City_District="CacheKey_Platform_City_District_";
	public static final String CacheKey_Platform_District_Street="CacheKey_Platform_District_Street_";
	public static final String CacheKey_Platform_Community_All="CacheKey_Platform_Community_All";
	public static final String CacheKey_Platform_CommunityRemark_All="CacheKey_Platform_CommunityRemark_All";
	public static final String CacheKey_Platform_Operationsite_All="CacheKey_Platform_Operationsite_All";
	public static final String CacheKey_Platform_Vehicle_All="CacheKey_Platform_Vehicle_All";
	public static final String CacheKey_Platform_VehicleFleet_All="CacheKey_Platform_VehicleFleet_All";
	public static final String CacheKey_Platform_VehicleFleetCompany_All="CacheKey_Platform_VehicleFleetCompany_All";
	
	public static final String Platform_City="_City_";
	public static final String Platform_District="_District_";
	public static final String Platform_Street="_Street_";
	public static final String Platform_Community="_Community_";
	public static final String Platform_CommunityRemark="_CommunityRemark_";
	public static final String Platform_Operationsite="_Operationsite_";
	public static final String Platform_Vehicle="_Vehicle_";
	public static final String Platform_VehicleFleet="_VehicleFleet_";
	public static final String Platform_VehicleFleetCompany="_VehicleFleetCompany_";
	
	
	
	
	
	
	
	
	public static final String CacheKey_Platform_Menu_User="CacheKey_Platform_Menu_User"; 
	public static final String CacheKey_Platform_Menu_All="CacheKey_Platform_Menu_All"; 
	public static final String CacheKey_Platform_Menu="CacheKey_Platform_Menu_"; 
	
	
	
	
	
	
}
