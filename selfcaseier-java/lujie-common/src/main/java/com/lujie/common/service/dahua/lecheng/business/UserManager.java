package com.lujie.common.service.dahua.lecheng.business;

import java.util.HashMap;

import com.alibaba.fastjson.JSONObject;

public interface UserManager {

    JSONObject accessToken(HashMap<String, Object> paramMap);

}
