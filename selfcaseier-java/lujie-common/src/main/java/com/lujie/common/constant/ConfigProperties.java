package com.lujie.common.constant;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.github.pagehelper.util.StringUtil;
import com.lujie.common.entity.EntityMap;
import com.lujie.common.util.MD5Util;

public class ConfigProperties {

	public static EntityMap ConfigMap=new EntityMap();
	
	/**
	 * 未登录的接口访问需要带token
	 */
	public static String SysToken;
	/**
	 * app客户端访问需要的key
	 */
	public static String AppKey;
	/**
	 * 密钥
	 */
	public static String AppSecret;
	
	public static HashMap<String,String> AccountMap=new HashMap<>();//外部接口用户、密钥
	public static HashMap<String,String> AccountUrlPathMap=new HashMap<>();//外部接口用户、可以访问的接口
	
	
	
	public static boolean isValidToken(String token){
		if(token!=null && token.length()>0 
				&& SysToken.equals(token)){
			return true;
		}
		return false;
	}
	public static String getSysToken(HttpServletRequest request) { 
		String token=getToken(request);
		if(token!=null&&!token.isEmpty()){
			String[] tokenarr=token.split(":");
			if(tokenarr==null || tokenarr.length<2) return null;
			return tokenarr[1];
		}
		return token;
	}
	public static String getTokenUserId(HttpServletRequest request) { 
		String token=getToken(request);
		if(token!=null&&!token.isEmpty()){
			return token.split(":")[0];
		}
		return token;
	}
	public static String getToken(HttpServletRequest request) { 
		String token=request.getHeader("token");
		if(token==null||token.isEmpty()){
			token=request.getParameter("token");
		}
		return token;
	}
	
	public static boolean isValidToken(HttpServletRequest request){
		String token=getSysToken(request);
		if(token!=null && token.length()>0 
				&& SysToken.equals(token)){
			return true;
		}
		return false;
	}
	
	
	public static ResultMsg CheckAppRequest(HttpServletRequest request){
		String userAppKey=request.getHeader("appKey");
		String userSign=request.getHeader("sign");
		String timestamp=request.getHeader("timestamp");
		
		if(userAppKey==null||userAppKey.isEmpty()){
			return new ResultMsg("301","appKey不能为空！");
		}else if(userSign==null||userSign.isEmpty()){
			return new ResultMsg("302","sign不能为空！");
		}else if(timestamp==null||timestamp.isEmpty()){
			return new ResultMsg("303","timestamp不能为空！");
		} 
		if(AppSecret!=null && AppSecret.length()>0){
			String sign=MD5Util.encrypt(userAppKey+AppSecret+timestamp);
			if(sign.equals(userSign)){
				return new ResultMsg(ResultCode.Success);
			}else{
				return new ResultMsg("304","sign有误");
			}
		}
		return new ResultMsg("305","其他错误！");
	}
	public static String getSign(String appKey,String appSecret,long timestamp){
		String sign=MD5Util.encrypt(appKey+appSecret+timestamp);
		return sign;
	}
	public static String getWechatSmallProgramSign(String appKey ,String timestamp,String token){
		String sign=MD5Util.encrypt(appKey+AppSecret+token+timestamp);
		return sign;
	}
	
	
	public static void setAccountSecret(String accounts){
		if(accounts==null || accounts.trim().length()==0) return;
		
		String[] accountArr=accounts.split(";");
		if(accountArr==null || accountArr.length==0)return;
		
		AccountMap.clear();
		for(String account:accountArr){
			String[] accarr=account.split(",");
			if(accarr==null||accarr.length==0) continue;
			
			AccountMap.put(accarr[0], accarr[1]);
		}
		
	}
	public static Boolean isSysToken(HttpServletRequest request){
		String token=request.getHeader("token");
		if(StringUtil.isEmpty(token)){
			return false;
		}
		
		return true;
	}
	
	public static Boolean isInterfaceUser(HttpServletRequest request){
		String account=request.getHeader("account");
		if(StringUtil.isEmpty(account)){
			return false;
		}
		return true;
	}
	public static Boolean isAppUser(HttpServletRequest request){
		String appKey=request.getHeader("appKey");
		if(StringUtil.isEmpty(appKey)){
			return false;
		}
		return true;
	}
	public static Boolean isWechatSmallProgram(HttpServletRequest request){
		if(request==null) return false;
		String wechatSmallProgram=request.getHeader("wechatSmallProgram");
		if(!StringUtil.isEmpty(wechatSmallProgram) && wechatSmallProgram.equals("1")){
			return true;
		}
		return false;
	}
	
	
	public static String getInterfaceUser(HttpServletRequest request){
		String account=request.getHeader("account"); 
		return account;
	}
	public static ResultMsg AllowAccountAccessPath(HttpServletRequest request){
		String account=request.getHeader("account");
		String urlPath = request.getServletPath();
		
		if(StringUtil.isEmpty(account)){
			return new ResultMsg("401","账号信息为空!");
		}
		if(AccountUrlPathMap==null || AccountUrlPathMap.isEmpty()){
			return new ResultMsg("402","没有为账号配置访问接口!");
		}

		String urlPaths=AccountUrlPathMap.get(account);
		if(StringUtil.isEmpty(urlPaths)){
			return new ResultMsg("403","没有为该账号配置访问接口!");
		}
		if(!urlPaths.toLowerCase().contains(urlPath.toLowerCase())){
			return new ResultMsg("404","不能访问这个接口!");
		}
		return new ResultMsg(ResultCode.Success);
	}
	
	
	public static ResultMsg CheckRequestAccount(HttpServletRequest request){
		String account=request.getHeader("account");
		String userSign=request.getHeader("sign");
		String timestamp=request.getHeader("timestamp");
		
		if(StringUtil.isEmpty(account)){
			return new ResultMsg("301","account不能为空！");
		}
		if(StringUtil.isEmpty(userSign)){
			return new ResultMsg("302","sign不能为空！");
		}
		if(StringUtil.isEmpty(timestamp)){
			return new ResultMsg("303","timestamp不能为空！");
		}
		
		String secret=AccountMap.get(account);
		if(StringUtil.isEmpty(secret)){
			return new ResultMsg("304","不存在这个账号！");
		}
		//System.out.println("account+secret+timestamp="+account+secret+timestamp);
		String sign=MD5Util.encrypt(account+secret+timestamp);
		//System.out.println("userSign:"+userSign);
		System.out.println("    sign:"+sign);
		
		if(!sign.equals(userSign)){
			return new ResultMsg("305","sign有误！");
		}
		return new ResultMsg(ResultCode.Success);
		//return AllowAccountAccessPath(request);
	}
	
	
	public static void main(String[] args){
		String appKey="A5";
		String secret="ed1f42cb243441f6829529634085393b";
		String timestamp=System.currentTimeMillis()+"";
		String sign=MD5Util.encrypt(appKey+secret+timestamp);
		System.out.println(sign);
	}
	
	
}
