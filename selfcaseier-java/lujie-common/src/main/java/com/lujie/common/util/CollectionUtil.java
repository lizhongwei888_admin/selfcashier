package com.lujie.common.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.lujie.common.entity.EntityMap;

public class CollectionUtil{
	

	public static void main(String[] args){
		System.out.println(isEmpty(null));
		System.out.println(isEmpty(new HashSet()));
		System.out.println(isEmpty(new HashMap()));
		System.out.println(isEmpty(new ArrayList()));
		
		
	}
	public static Boolean isEmpty(Object obj){
		if(obj==null) return true;
		
		if(obj instanceof java.util.List){
			List list=(List)obj;
			return list.isEmpty();
		}else if(obj instanceof java.util.Map){
			Map map=(Map)obj;
			return map.isEmpty();
		}else if(obj instanceof java.util.Set){
			Set set=(Set)obj;
			return set.isEmpty();
		}
		
		return true;
	}
	
	public static Boolean isNotEmpty(Object obj){
		if(obj==null) return false;
		
		if(obj instanceof java.util.List){
			List list=(List)obj;
			return !list.isEmpty();
		}else if(obj instanceof java.util.Map){
			Map map=(Map)obj;
			return !map.isEmpty();
		}else if(obj instanceof java.util.Set){
			Set set=(Set)obj;
			return !set.isEmpty();
		}
		
		return true;
	}
	
	public static Map listMapToMap(List<EntityMap> list,String mapKey){
		if(list==null) return null;
		Map resultMap=new HashMap();
		for(EntityMap map:list){
			resultMap.put(map.get(mapKey), map);
		}
		return resultMap;
	}
	
}