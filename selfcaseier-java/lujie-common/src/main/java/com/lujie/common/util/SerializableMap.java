package com.lujie.common.util;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.HashMap;
import java.util.Map;

public class SerializableMap<K, V> extends HashMap<K, V> implements Externalizable {

	public SerializableMap(){
		super();
	}
	
	public SerializableMap(Map<? extends K, ? extends V> m){
		super(m);
	}
	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeInt(size());
        if (size() > 0) {
            for(Map.Entry<K,V> e : entrySet()) {
            	out.writeObject(e.getKey());
            	out.writeObject(e.getValue());
            }
        }
	}
	
    public void addEntry(K key, V entry) {
    	this.put(key, entry);
    }

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		 int mappings = in.readInt();
		 for (int i = 0; i < mappings; i++) {
	            K key = (K) in.readObject();
	            V value = (V) in.readObject();
	            addEntry(key, value);
	     }
	}

}
