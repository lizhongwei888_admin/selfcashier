package com.lujie.common.dao;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.lujie.common.entity.BaseEntity;

import javafx.scene.control.Pagination;

public interface AbstractDao<E extends BaseEntity> {

	public Serializable save(E obj);

	public E findById(Serializable id);

	public Collection<E> findByIdList(Serializable[] IDs);

	public List<E> queryById(Serializable id);

	public Pagination pageById(Serializable id);

	public E findByProperty(final String propertyName, final Object value);

	public boolean isPropertyExist(String propertyName, Object value);

	public void saveOrUpdate(E obj);

	public void saveOrUpdateAll(Collection<E> entities);

	public Integer update(E obj);

	public void delete(E obj);

	public void deleteAll(Serializable[] IDs);
	
	public List<?> pageList(String hql, int pageNumber, int pageSize);

	public void deleteAll(Collection<E> collection);

	public int deleteAll();

	public List<E> getAll();

	public List<E> getAllInOrder(String orderHql);
	
	public Pagination getAll(int pageNumber, int pageSize);

	public Pagination getAll(String hql, Object[] values, int pageNumber,
			int pageSize);

	public Pagination pageQuery(Map<String, Object> values, int pageNumber,
			int pageSize);

	public int querySize(String hql, Object[] values);

	public int querySize(String hql, Map<String, Object> values);

	public int getTotalCount();

	public List<E> queryListByProperty(String propertyName, Object value);

	public void updateNameById(String value, Integer id);

	public List<E> queryTop(String sql, int topSize);

	public List<E> getTotalSum(String nameSql);
	
	public List<Object[]> getTotalSum(String nameSql,int pagesize,int pageNo);

	
	public List<String> executeSqlQuery(String sql);
	
	public List<E> findByCondition(Map<String,Object> condition);
	
	public List findPageListByProperty(@Param("p") Object property);
	
	public Long countTotalByProperty(@Param("p") Object property);
	
}
