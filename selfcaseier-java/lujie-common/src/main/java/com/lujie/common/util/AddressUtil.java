package com.lujie.common.util;

import java.util.HashMap;

import com.lujie.common.constant.HttpContentType;

import net.sf.json.JSONObject;

public class AddressUtil {

	
	static String tail_url="http://api.map.baidu.com/geocoder/v2/?callback=renderReverse&location=";
	static String body_url ="31.196974891550088,121.44948661421991";
	static String end_url="&output=json&pois=0&ak=D2b4558ebed15e52558c6a766c35ee73";
	public static String getBDAddress(String bdLongitude,String bdLatitude){
		if(bdLongitude==null || bdLatitude==null) return null;
		
		body_url = bdLatitude + "," + bdLongitude;
		try {
			 
			 HashMap<String,String> reqHeaderMap=new HashMap<>();
				
				reqHeaderMap.put("Content-Type",HttpContentType.ApplicationForm+";charset=utf-8");
				
			 String url=tail_url+body_url+end_url;
			 String jsonp = HttpRequest.sendGet(url, "");

			 if(jsonp!="" && jsonp.indexOf("(")>0 && jsonp.indexOf(")")>0){
				 String json = jsonp.substring(jsonp.indexOf("(")+1, jsonp.lastIndexOf(")"));
				 JSONObject jsonObject=JSONObject.fromObject(json);
				 Object first = jsonObject.get("result");
				 JSONObject second = JSONObject.fromObject(first);
				 String addressComponent =  second.getString("formatted_address");
				 return addressComponent;
				 
			 }
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static void main(String[] args){
		String address=getBDAddress("121.93447787255842","30.920500885899614");
		System.out.println(address);
	}
	
}
