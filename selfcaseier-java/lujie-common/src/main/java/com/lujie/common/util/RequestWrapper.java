package com.lujie.common.util;


import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import lombok.extern.slf4j.Slf4j;

/**
 * @author 01
 * @program wrapper-demo
 * @description 包装HttpServletRequest，目的是让其输入流可重复读
 * @create 2018-12-24 20:48
 * @since 1.0
 **/
@Slf4j
public class RequestWrapper extends HttpServletRequestWrapper {
    /**
     * 存储body数据的容器
     */
    private final byte[] body;
    private Map<String,String> requestParamMap=new HashMap<>();

    public RequestWrapper(HttpServletRequest request) throws IOException {
        super(request);

        // 将body数据存储起来
        String bodyStr = getBodyString(request);
        body = bodyStr.getBytes(Charset.defaultCharset());
    }

    /**
     * 获取请求Body
     *
     * @param request request
     * @return String
     */
    public String getBodyString(final ServletRequest request) {
        try {
            return inputStream2String(request.getInputStream());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 获取请求Body
     *
     * @return String
     */
    public String getBodyString() {
        final InputStream inputStream = new ByteArrayInputStream(body);

        return inputStream2String(inputStream);
    }
    public Map<String,String> getRequestParamMap() {
        if(requestParamMap!=null && requestParamMap.size()>0) return requestParamMap;
        
    	final InputStream inputStream = new ByteArrayInputStream(body);

        String requestParamStr = inputStream2String(inputStream);
        if(requestParamStr!=null && !body.equals("")){
        	String[] requestParamArr=requestParamStr.split("&");
        	if(requestParamArr==null) {
        		return requestParamMap;
        	}
        	
        	for(String requestParam:requestParamArr){
        		String[] keyValue=requestParam.split("=");
        		if(keyValue!=null&&keyValue.length>=2){
        			requestParamMap.put(keyValue[0],keyValue[1]);
        		}
        	}
        }
        return requestParamMap;
    }
    public String getRequestParamValue(String key) {
    	requestParamMap=getRequestParamMap();
    	if(requestParamMap!=null && requestParamMap.size()>0) {
    		return requestParamMap.get(key);
        }
        return null;
    }
    public Integer getRequestParamValueInteger(String key) {
    	requestParamMap=getRequestParamMap();
    	if(requestParamMap!=null && requestParamMap.size()>0) {
    		String value=requestParamMap.get(key);
    		if(value!=null){
    			return Integer.parseInt(value);
    		}
        }
        return null;
    }

    /**
     * 将inputStream里的数据读取出来并转换成字符串
     *
     * @param inputStream inputStream
     * @return String
     */
    private String inputStream2String(InputStream inputStream) {
        StringBuilder sb = new StringBuilder();
        BufferedReader reader = null;

        try {
            reader = new BufferedReader(new InputStreamReader(inputStream, Charset.defaultCharset()));
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();
    }

    @Override
    public BufferedReader getReader() throws IOException {
        return new BufferedReader(new InputStreamReader(getInputStream()));
    }

    @Override
    public ServletInputStream getInputStream() throws IOException {

        final ByteArrayInputStream inputStream = new ByteArrayInputStream(body);

        return new ServletInputStream() {
            @Override
            public int read() throws IOException {
                return inputStream.read();
            }

            @Override
            public boolean isFinished() {
                return false;
            }

            @Override
            public boolean isReady() {
                return false;
            }

            @Override
            public void setReadListener(ReadListener readListener) {
            }
        };
    }
}
