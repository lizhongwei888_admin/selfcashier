package com.lujie.common.constant;

/**
 * 返回处理结果
 * @author sl
 *
 */
public class LayuiPageResultMsg extends ResultMsg{

	private Long count;
	 
	public   LayuiPageResultMsg(String code,String msg,Object data,Long count){
		super(code,msg,data);
		this.count=count;
	}
	public   LayuiPageResultMsg(ResultCode result,Object data,Long count){
		super(result.getCode().toString(),result.getDesc(),data);
		this.count=count;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}
	
	 
}
