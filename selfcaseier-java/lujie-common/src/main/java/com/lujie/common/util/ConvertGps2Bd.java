package com.lujie.common.util;
 
public class ConvertGps2Bd {

	// WGS-84与gcj-02坐标转换
    private static final double pi = 3.14159265358979324;

    //
    // Krasovsky 1940
    //
    // a = 6378245.0, 1/f = 298.3
    // b = a * (1 - f)
    // ee = (a^2 - b^2) / a^2;
    private static final double a = 6378245.0;
    private static final double ee = 0.00669342162296594323;

    // World Geodetic System ==> Mars Geodetic System
    // WGS-84转gcj-02
    public static String transform(double wgLon,double wgLat) //参数顺序 纬度lat, 经度lng
    {
    	double mgLat,mgLon;
        if (outOfChina(wgLat, wgLon))
        {
            mgLat = wgLat;
            mgLon = wgLon;
            return Bd_encrypt(mgLat,mgLon);
        }
        double dLat = transformLat(wgLon - 105.0, wgLat - 35.0);
        double dLon = transformLon(wgLon - 105.0, wgLat - 35.0);
        
        dLat=(dLat * 180.0) / ((a * (1 - ee)) / ((1 - ee * Math.sin(wgLat / 180.0 * pi) * Math.sin(wgLat / 180.0 * pi)) * Math.sqrt( 1 - ee * Math.sin(wgLat / 180.0 * pi) * Math.sin(wgLat / 180.0 * pi))) * pi);
        dLon=(dLon * 180.0) / (a / Math.sqrt( 1 - ee * Math.sin(wgLat / 180.0 * pi) * Math.sin(wgLat / 180.0 * pi)) * Math.cos(wgLat / 180.0 * pi) * pi);
        /* double radLat = wgLat / 180.0 * pi;
        double magic = Math.sin(radLat);
        magic = 1 - ee * magic * magic;
        double sqrtMagic = Math.sqrt(magic);
        dLat = (dLat * 180.0) / ((a * (1 - ee)) / (magic * sqrtMagic) * pi);
        dLon = (dLon * 180.0) / (a / sqrtMagic * Math.cos(radLat) * pi);*/
        mgLat = wgLat + dLat;
        mgLon = wgLon + dLon;
        return Bd_encrypt(mgLat,mgLon);
    }

    static boolean outOfChina(double lat, double lon)
    {
        if (lon < 72.004 || lon > 137.8347)
            return true;
        if (lat < 0.8293 || lat > 55.8271)
            return true;
        return false;
    }

    static double transformLat(double x, double y)
    {
        double ret = -100.0 + 2.0 * x + 3.0 * y + 0.2 * y * y + 0.1 * x * y + 0.2 * Math.sqrt(Math.abs(x));
        ret += (20.0 * Math.sin(6.0 * x * pi) + 20.0 * Math.sin(2.0 * x * pi)) * 2.0 / 3.0;
        ret += (20.0 * Math.sin(y * pi) + 40.0 * Math.sin(y / 3.0 * pi)) * 2.0 / 3.0;
        ret += (160.0 * Math.sin(y / 12.0 * pi) + 320 * Math.sin(y * pi / 30.0)) * 2.0 / 3.0;
        return ret;
    }

    static double transformLon(double x, double y)
    {
        double ret = 300.0 + x + 2.0 * y + 0.1 * x * x + 0.1 * x * y + 0.1 * Math.sqrt(Math.abs(x));
        ret += (20.0 * Math.sin(6.0 * x * pi) + 20.0 * Math.sin(2.0 * x * pi)) * 2.0 / 3.0;
        ret += (20.0 * Math.sin(x * pi) + 40.0 * Math.sin(x / 3.0 * pi)) * 2.0 / 3.0;
        ret += (150.0 * Math.sin(x / 12.0 * pi) + 300.0 * Math.sin(x / 30.0 * pi)) * 2.0 / 3.0;
        return ret;
    }


    //百度与gcj-02坐标转换
    private static  final double x_pi = 3.14159265358979324 * 3000.0 / 180.0 ;        

    // gcj-02到baidu
    public static String Bd_encrypt(double gg_lat, double gg_lon)  
    {  
        double bd_lat; 
        double bd_lon;
       /* double x = gg_lon, y = gg_lat;  
        double z = Math.sqrt(x * x + y * y) + 0.00002 * Math.sin(y * x_pi);  
        double theta = Math.atan2(y, x) + 0.000003 * Math.cos(x * x_pi);  
        bd_lon = z * Math.cos(theta) + 0.0065;  
        bd_lat = z * Math.sin(theta) + 0.006;  */
        bd_lat=(Math.sqrt(gg_lon * gg_lon + gg_lat * gg_lat) + 0.00002 * Math.sin(gg_lat * (3.14159265358979324 * 3000.0 / 180.0)))*Math.sin(Math.atan2(gg_lat,gg_lon) + 0.000003 * Math.cos(gg_lon * (3.14159265358979324 * 3000.0 / 180.0))) + 0.006;
        bd_lon=(Math.sqrt(gg_lon * gg_lon + gg_lat * gg_lat) + 0.00002 * Math.sin(gg_lat * (3.14159265358979324 * 3000.0 / 180.0)))*Math.cos(Math.atan2(gg_lat, gg_lon) + 0.000003 * Math.cos(gg_lon * (3.14159265358979324 * 3000.0 / 180.0))) + 0.0065;
        return String.valueOf(bd_lon)+','+String.valueOf(bd_lat);
    }  
    
    // baidu到gcj-02
    public static String Bd_decrypt(double bd_lat, double bd_lon)  
    {  
        double gg_lat;
        double gg_lon;
        double x = bd_lon - 0.0065, y = bd_lat - 0.006;  
        double z = Math.sqrt(x * x + y * y) - 0.00002 * Math.sin(y * x_pi);  
        double theta = Math.atan2(y, x) - 0.000003 * Math.cos(x * x_pi);  
        gg_lon = z * Math.cos(theta);  
        gg_lat = z * Math.sin(theta); 
        return String.valueOf(gg_lon)+','+String.valueOf(gg_lat);
    }
    
    public static void main(String args[]){
    	/*System.out.println(change("N030132601")+","+change("E121149683"));
    	System.out.println(transform(change("N030132601"),change("E121149683")));
    	System.out.println(transformLat(29.575429778924,114.21892734521));
    	System.out.println(transformLon(29.575429778924,114.21892734521));*/
    	System.out.println("************************");
    	//System.out.println(Bd_encrypt(29.575429778924,114.21892734521));
    	System.out.println(transform(29.575429778924,114.21892734521));
    	
    	System.out.println("a"+transform(31.166956666,121.579048333));
    	System.out.println("b"+transform(31.036345000,121.256855000));
    	System.out.println("c"+transform(31.166961666,121.579063333));
    	System.out.println("d"+transform(31.253276666,121.596930000));
    	//System.out.println("e"+transform(31.002351666,121.190895000));
    	//System.out.println("f"+transform(31.025343333,121.233815000));
    	
    	System.out.println("************************");
    	
    	
    }
    
    public  static double change(String param) {
		String du = param.substring(1, 4);
		String fen = param.substring(4, 6) + "." + param.substring(6);
		return Double.valueOf(du) + (Double.valueOf(fen) / 60);
	}

}
