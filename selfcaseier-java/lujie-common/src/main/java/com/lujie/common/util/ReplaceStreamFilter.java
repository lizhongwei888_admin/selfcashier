package com.lujie.common.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import lombok.extern.slf4j.Slf4j;
 
/**
 * @author 01
 * @program wrapper-demo
 * @description 替换HttpServletRequest
 * @create 2018-12-24 21:04
 * @since 1.0
 **/
@Slf4j
public class ReplaceStreamFilter implements Filter {
    
	public List<String> excludeFilterUrlList=new ArrayList<>();//不进行过滤的地址
	
	@Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("StreamFilter初始化...");
    }
 
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
    	HttpServletRequest httpServletRequest = (HttpServletRequest)request;
    	String urlPath = httpServletRequest.getServletPath();
    	
    	Boolean isExcludeUrl=false;
    	for(String excludeUrl:excludeFilterUrlList){
			//添加导入的过滤/import
			if (urlPath.indexOf(excludeUrl) == 0 || urlPath.contains("/import")) {
				isExcludeUrl = true;
				break;
			}
		}
    	if(isExcludeUrl){
    		chain.doFilter(request, response);
    	}else{
    		ServletRequest requestWrapper = new RequestWrapper((HttpServletRequest) request);
            chain.doFilter(requestWrapper, response);
    	}
    }
 
    @Override
    public void destroy() {
    	System.out.println("StreamFilter销毁...");
    }
}
