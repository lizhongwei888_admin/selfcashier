package com.lujie.common.constant;

import java.util.HashMap;
import java.util.List;

import com.lujie.common.entity.EntityMap;
import com.lujie.common.util.StringUtil;

/**
 * 返回处理结果
 * @author sl
 *
 */
public class ResultMsg {

	private String code;
	private String msg;
	private Object data;
	private Long count;
	
	
	public ResultMsg(){
		ResultCode resultCode=ResultCode.Success;
		this.code=resultCode.getCode().toString();
		this.msg=resultCode.getDesc(); 
	}
	
	public ResultMsg(String code ,String msg){
		this.code=code;
		this.msg=msg;
	}
	public static ResultMsg newResultMsg(String code ,String msg){
		return new ResultMsg( code , msg);
	} 
	public ResultMsg(String code ,String msg,Object data){
		this.code=code;
		this.msg=msg;
		this.data=data;
	}
	public ResultMsg(String code ,String msg,Object data,Object count){
		this.code=code;
		this.msg=msg;
		this.data=data;
		if(count!=null&&!StringUtil.isEmptyJsonStr(count)){
			this.count=Long.parseLong(count.toString());
		}
	}
	public ResultMsg(Object data){
		this.code=ResultCode.Success.getCode().toString();
		this.msg=ResultCode.Success.getDesc();
		this.data=data;
		if(data!=null){
			if(data instanceof java.util.List){
				List list=(List)data;
				this.count=Long.parseLong(list.size()+"");
			}
		}
	}
	public ResultMsg(Object data,Long count){
		this.code=ResultCode.Success.getCode().toString();
		this.msg=ResultCode.Success.getDesc();
		this.data=data;
		this.count=count;
	}
	
	public ResultMsg(Object data,Object sum){
		this.code=ResultCode.Success.getCode().toString();
		this.msg=ResultCode.Success.getDesc();
		HashMap dataMap=new HashMap();
		dataMap.put("data", data);
		dataMap.put("sum", sum);
		this.data=dataMap;
		
		Long totalCount=0L;
		if(sum!=null && sum instanceof EntityMap){
			EntityMap sumEntityMap=(EntityMap)sum;
			totalCount=sumEntityMap.getLong("totalCount");
			if(totalCount==null) totalCount=0L;
		}
		
		this.count=totalCount;
	}
	
	public   ResultMsg(ResultCode resultCode){
		this.code=resultCode.getCode().toString();
		this.msg=resultCode.getDesc(); 
	}
	public   ResultMsg(ResultCode resultCode,Object data){
		this.code=resultCode.getCode().toString();
		this.msg=resultCode.getDesc(); 
		this.data=data;
		if(data!=null){
			if(data instanceof java.util.List){
				List list=(List)data;
				this.count=Long.parseLong(list.size()+"");
			}
		}
	}
	public   ResultMsg(ResultCode resultCode,Object data,Long count){
		this.code=resultCode.getCode().toString();
		this.msg=resultCode.getDesc(); 
		this.data=data;
		this.count=count;
	}
	public   ResultMsg(ResultCode resultCode,Object data,Integer count){
		this.code=resultCode.getCode().toString();
		this.msg=resultCode.getDesc(); 
		this.data=data;
		this.count=Long.parseLong(count.toString());
	}
	public static ResultMsg newResultMsg(ResultCode resultCode){
		return new ResultMsg( resultCode.getCode().toString() , resultCode.getDesc());
	}
	public static ResultMsg newResultMsg(ResultCode resultCode,Object data){
		return new ResultMsg( resultCode.getCode().toString() , resultCode.getDesc(),data);
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public String toString(){
		String resultStr="{\"code\":"+code+",\"msg\":\""+msg+"\",\"data\":"+data+"}";
		return resultStr;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}
	
	
	public Boolean equals(ResultCode resultCode){
		if(getCode().equals(resultCode.getCode().toString())){
			return true;
		}
		return false;
	}
	
	
	
}
