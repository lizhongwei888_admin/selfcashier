package com.lujie.common.util;

import java.util.Random;
import java.util.UUID;
import java.util.regex.Pattern;

public class StringUtil {

	public final static String EMPTY="";
	
	public static String generateUUID() {
		 String uuid = UUID.randomUUID().toString().replace("-", "");
		 return uuid ;
	}
	
	
	public static String lowerFirstCapse(String str) {
        char[] chars = str.toCharArray();
        chars[0] += 32;
        return String.valueOf(chars);
    }
	
	public static String getStrExcludeNull(Object obj){
		if(obj==null) return null;
		String str=obj.toString();
		if(str.toLowerCase().equals("null")) {
			return null;
		}
		return str;
	}
	/**
	 * 获取不包含null字符串，并且trim字符串
	 * @param obj
	 * @return
	 */
	public static String getStrExcludeNullAndTrim(Object obj){
		if(obj==null) return null;
		String str=obj.toString();
		if(str.toLowerCase().equals("null")) {
			return null;
		}
		str=str.trim();
		return str;
	}
	public static String getStrExcludeNullAndRemoveSpace(Object obj){
		if(obj==null) return null;
		String str=obj.toString();
		if(str.toLowerCase().equals("null")) {
			return null;
		}
		str=str.replace(" ", "");
		return str;
	}
	
    
	public static void main(String[] args){
		System.out.println(getLimitLenStr("s22222222222s", 10, "..."));
	}
	
	public static boolean equal(String str1,String str2){
		if(str1==null && str2==null) return true;
		
		if(str1==null && str2!=null) return false;
		
		if(str1!=null && str2==null) return false;
		
		return str1.equals(str2);
	}
	
	public static boolean isEmpty(String str){
		if(str==null || str.trim().length()==0) return true;
		return false;
	}
	
	public static boolean isEmptyObj(Object str){
		if(str==null || isEmpty(str.toString())) return true;
		return false;
	}
	
	public static boolean isNotEmptyObj(Object str){
		if(str!=null && str.toString().trim().length()>0) return true;
		return false;
	}
	
	public static boolean isNotEmpty(String str){
		if(str!=null && str.toString().trim().length()>0) return true;
		return false;
	}
	
	public static boolean isEmptyJsonStr(Object str){
		if(str==null || str.toString().trim().length()==0 
				|| str.toString().trim().toLowerCase().equals("null")) return true;
		return false;
	}
	public static boolean isNotEmptyJsonStr(Object str){
		if(str!=null
				&&str.toString().trim().length()>0 
				&&!str.toString().trim().toLowerCase().equals("null")) return true;
		return false;
	}
	public static String getStr(Object obj){
		if(obj==null) return null;
		if(isEmptyJsonStr(obj.toString())) return null;
		return obj.toString();
	}
	public static String getNotNullStr(Object obj){
		String str=getStr(obj);
		if(str==null) return "";
		return str;
	}
	public static String getNotNullString(Object obj){
		if(obj==null) return "";
		
		if(obj.toString().toLowerCase().equals("null")){
			return "";
		}
		return obj.toString();
	}
	
	
	public static boolean isBlank(String str){
		if(str==null || str.trim().length()==0) return true;
		return false;
	}
	public static boolean isAnyBlank(String str,String... params){
		if(isBlank(str)) return true;
		
		if(params!=null){
			for(String tempstr:params){
				if(isBlank(str)) return true;
			}
		}
		
		return false;
	}
	public static boolean isNum(String str) {
		Pattern pattern = Pattern.compile("[0-9]*");
		return pattern.matcher(str).matches();
	}
	public static String getDigitRandom(Integer len){
		StringBuilder sb=new StringBuilder();
		for(int i=1;i<=len;i++){
			sb.append(new Random().nextInt(10));
		}
		return sb.toString();
	}
	//48～57为0到9十个阿拉伯数字
	//65～90为26个大写英文字母，97～122号为26个小写英文字母
	public static String getRandomStr(Integer len){
		StringBuilder sb=new StringBuilder();
		for(int i=1;i<=len;i++){
			Integer type=new Random().nextInt(3);//0数字，1大写字母，2小写字母
			if(type==0){
				sb.append(new Random().nextInt(10));
			}else if(type==1){
				char bigchar=(char)(new Random().nextInt(26)+65);
				sb.append(bigchar);
			}else if(type==2){
				char smallchar=(char)(new Random().nextInt(26)+97);
				sb.append(smallchar);
			}
		}
		return sb.toString();
	}
	
	public static String getExceptionDesc(Exception ex){
		StringBuilder sb=new StringBuilder();
    	
		Throwable  causeThrowable=ex.getCause();
		String message=ex.getMessage();
		
		sb.append("\r\n\r\n cause:"+causeThrowable+"\r\n\r\n");
		sb.append("message:"+message+"\r\n\r\n");
		sb.append("stack:");
		
		StackTraceElement[] stackTraceElement = ex.getStackTrace();
    	for(StackTraceElement stack:stackTraceElement){
    		sb.append(stack.getFileName());sb.append(",");
    		sb.append(stack.getClassName());sb.append(",");
    		sb.append(stack.getMethodName());sb.append(",");
    		sb.append(stack.getLineNumber());sb.append(",");
    		sb.append("--------\r\n");
    		//stack.toString()
    	}
    	return sb.toString();
	}
	
	public static String getExceptionDesc(Throwable causeThrowable){
		StringBuilder sb=new StringBuilder();
    	
		String message=causeThrowable.getMessage();
		
		sb.append("\r\n\r\n cause:"+causeThrowable+"\r\n\r\n");
		sb.append("message:"+message+"\r\n\r\n");
		sb.append("stack trace:");
		
		StackTraceElement[] stackTraceElement = causeThrowable.getStackTrace();
    	for(StackTraceElement stack:stackTraceElement){
    		sb.append(stack.getFileName());sb.append(",");
    		sb.append(stack.getClassName());sb.append(",");
    		sb.append(stack.getMethodName());sb.append(",");
    		sb.append(stack.getLineNumber());sb.append(",");
    		sb.append("--------\r\n");
    		//stack.toString()
    	}
    	return sb.toString();
	}
	
	public static String getExceptionDescByThrowable(Throwable causeThrowable){
		StringBuilder sb=new StringBuilder();
    	
		String message=causeThrowable.getMessage();
		
		sb.append("\r\n\r\n cause:"+causeThrowable+"\r\n\r\n");
		sb.append("message:"+message+"\r\n\r\n");
		sb.append("stack trace:");
		
		StackTraceElement[] stackTraceElement = causeThrowable.getStackTrace();
    	for(StackTraceElement stack:stackTraceElement){
    		sb.append(stack.getFileName());sb.append(",");
    		sb.append(stack.getClassName());sb.append(",");
    		sb.append(stack.getMethodName());sb.append(",");
    		sb.append(stack.getLineNumber());sb.append(",");
    		sb.append("--------\r\n");
    		//stack.toString()
    	}
    	return sb.toString();
	}
	
	
	public static String removeLastChar(StringBuilder s){
		if(s==null || s.length()<2) return null;
		return s.delete(s.length()-1, s.length()).toString();
	}
	
	public static String getLimitLenStr(String str,Integer maxLen,String str2){
		if(str==null) return "";
		if(str2.length()>maxLen) return str;
		
		Integer contentMaxLen=maxLen-str2.length();
		if(str.length()<contentMaxLen) return str;
		
		return str.substring(0,contentMaxLen-1)+str2;
	}
	
	public static Boolean hasKeyWordAtEnd(String str,String key){
		int lastIndex=str.lastIndexOf(key);
		if(lastIndex==-1) return false;
		if(str.length()!=lastIndex+key.length()) return false;
		
		return true;
	}
	
	public static String removeKeyWordAtEnd(String str,String key){
		int lastIndex=str.lastIndexOf(key);
		if(lastIndex==-1) return str;
		if(str.length()!=lastIndex+key.length()) return str;
		
		return str.substring(0,lastIndex);
	}
	public static String get2LenStr(Integer data){
		if(data>=10 || data<0) return data.toString();
		
		return "0"+data;
	}
	
}
