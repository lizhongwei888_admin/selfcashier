package com.lujie.common.constant;

/**
 * @author zzm
 *
 */
public enum ResultCode  {

	Success(200, "成功"),
	Error(300, "失败") 
	
	;
	
	private Integer code;

	
	private String desc;

	ResultCode(Integer code, String desc) {
		this.code = code;
		this.desc = desc;
	}
	ResultCode(String desc) {
		this.desc = desc;
	}

	public Integer getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

}
