package com.lujie.common.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import net.sf.json.JsonConfig;
import net.sf.json.processors.JsonValueProcessor;

public class DateJsonValueProcessor implements JsonValueProcessor {

	public String format = "yyyy-MM-dd HH:mm:ss";
	
	public DateJsonValueProcessor(){
		super();
	}

	public DateJsonValueProcessor(String format) {
		super();
		this.format=format;
	}

	@Override
	public Object processArrayValue(Object value, JsonConfig jsonConfig) {
		return process(value);
	}

	@Override
	public Object processObjectValue(String key, Object value,
			JsonConfig jsonConfig) {
		return process(value);
	}

	private Object process(Object value) {
		if(value instanceof Date){
			SimpleDateFormat sdf=new SimpleDateFormat(format,Locale.CHINA);
			return sdf.format(value);
		}
		return value==null?"":value.toString();
	}
}
