package com.lujie.common.util;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.lujie.common.constant.ResultMsg;

/**
 * JSON数据转换处理类
 * @author user
 *
 */
public class JsonHelper {
	
	
	private static ObjectMapper objectMapper;
	static{
		objectMapper = new ObjectMapper();
		/**
         * 这个特性决定parser是否将允许使用非双引号属性名字， （这种形式在Javascript中被允许，但是JSON标准说明书中没有）。
         * 注意：由于JSON标准上需要为属性名称使用双引号，所以这也是一个非标准特性，默认是false的。
         * 同样，需要设置JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES为true，打开该特性。
		 */
		objectMapper.configure(Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
		/**
         * 该特性可以允许接受所有引号引起来的字符，使用‘反斜杠\’机制：如果不允许，只有JSON标准说明书中 列出来的字符可以被避开约束。
         * 由于JSON标准说明中要求为所有控制字符使用引号，这是一个非标准的特性，所以默认是关闭的。
         * 注意：一般在设置ALLOW_SINGLE_QUOTES属性时，也设置了ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER属性，
         * 所以，有时候，你会看到不设置ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER为true，但是依然可以正常运行。
         */
		objectMapper.configure(Feature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER, true);
		/**
         * 该特性决定parser是否允许JSON整数以多个0开始(比如，如果000001赋值给json某变量，
         * 如果不设置该属性，则解析成int会抛异常报错：org.codehaus.jackson.JsonParseException: Invalid numeric value: Leading zeroes not allowed
         * 注意：该属性默认是关闭的，如果需要打开，则设置JsonParser.Feature.ALLOW_NUMERIC_LEADING_ZEROS为true。
         */
		objectMapper.configure(Feature.ALLOW_NUMERIC_LEADING_ZEROS, true);
		/**
         * 该特性决定了当遇到未知属性（没有映射到属性，没有任何setter或者任何可以处理它的handler），是否应该抛出一个
         * JsonMappingException异常。这个特性一般式所有其他处理方法对未知属性处理都无效后才被尝试，属性保留未处理状态。
         * 默认情况下，该设置是被打开的。
		 * objectMapper.configure(Feature.FAIL_ON_UNKNOWN_PROPERTIES, true);
         */
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		
		/**
		 * 格式化输出
		 */
		//objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
		
		objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		
		//objectMapper.setSerializationInclusion(Include.NON_NULL);
	}

	/**
	 * json字符转换为任意对象，支持集合类、泛型等任意类型
	 * @param json
	 * 		json字符内容
	 * @param jsonTypeReference
	 * 		
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public static <T> T toObject(String json, JsonTypeReference<T> jsonTypeReference) {
		try {
			return objectMapper.readValue(json, jsonTypeReference.getTypeReference());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * json字符转换为java对象,不支持集合、泛型
	 * @param json
	 * 		json字符内容
	 * @param clazz
	 * 		转换的对象class
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public static <T> T toObject(String json, Class<T> clazz) {
		try {
			return objectMapper.readValue(json, clazz);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * 转换java对象到json字符串
	 * @param obj
	 * @return
	 * @throws JsonProcessingException 
	 */
	public static String toJson(Object obj) {
		try {
			String json = objectMapper.writeValueAsString(obj);
			return json;
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}
	}
	public static String toJsonResultMsg(Object obj) {
		return toJson(new ResultMsg(obj));
	}
	public static String toJsonResultMsg() {
		return toJson(new ResultMsg());
	}
	public static String toJsonResultMsg(Object obj,Long count) {
		return toJson(new ResultMsg(obj,count));
	}
	public static String toJsonResultMsg(Object obj,Object sum) {
		return toJson(new ResultMsg(obj,sum));
	}
	public static String toJsonResultMsg(Object obj,Integer count) {
		return toJson(new ResultMsg(obj,Long.parseLong(count+"")));
	}
	public static String toJsonResultMsg(String code,String msg) {
		return toJson(new ResultMsg(code,msg));
	}
	public static void outputMsg(HttpServletResponse response,String code,String msg) throws IOException{
		outputMsg(response,new ResultMsg(code,msg));
	}
	public static void outputMsg(HttpServletResponse response,ResultMsg msg) throws IOException{
		response.setHeader("Content-type", "application/json; charset=utf-8");
		response.setContentType("application/json; charset=utf-8");
		PrintWriter out = response.getWriter();
    	String errormsg=JsonHelper.toJson(msg);
    	out.println(errormsg);
	}
}
