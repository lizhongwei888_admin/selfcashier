package com.lujie.common.util;

import java.lang.reflect.Field;

public class ReflectUtil {
	
	public static Object getFieldValue(Object target, String variable){
		if(target==null){
			return null;
		}
		try {
			Field field = target.getClass().getDeclaredField(variable);
			field.setAccessible(true);
			return field.get(target);
		}catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	public static void setFieldValue(Object target, String variable, Object value){
		if(target==null){
			return;
		}
		try {
			Field field = target.getClass().getDeclaredField(variable);
			field.setAccessible(true);
			field.set(target, value);
		}catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		
	}
}
