package com.lujie.common.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

/**
 * http协议工具
 * @author hupei
 *
 */
public class HttpUtilClient {
	/**
	 * 简单的提交数据
	 * @param url
	 * @param params
	 * @return
	 */
    public static String simplePost(String url, Map<String, Object> params, String recordName)  throws Exception{  
    	CloseableHttpClient httpclient = HttpClients.createDefault(); 
        String body = null;  
          
//        log.info("create httppost:" + url);  
        HttpPost post = postForm(url, params , recordName);  
          
        body = invoke(httpclient, post);  
          
        httpclient.getConnectionManager().shutdown();  
          
        return body;  
    }
    
    
    /**
     * 提交并获取数据
     * @param url
     * @param params
     * @return
     */
    public static JSONObject jsonPost(String url, Map<String, Object> params, String recordName)  throws Exception{  
    	CloseableHttpClient httpclient = HttpClients.createDefault(); 
    	JSONObject resultJsonObject = null; 
          
//        log.info("create httppost:" + url);  
        HttpPost post = postForm(url, params , recordName);  
          
        resultJsonObject = jsonInvoke(httpclient, post);  
          
        httpclient.getConnectionManager().shutdown();  
          
        return resultJsonObject;  
    }
    
    private static String invoke(CloseableHttpClient httpclient,  
            HttpUriRequest httpost)  throws Exception{  
          
        HttpResponse response = sendRequest(httpclient, httpost);  
        
        String body = "";
        if (response.getStatusLine().getStatusCode() == 200) {
        	body = paseResponse(response);
        }  
          
        return body;  
    }  
    
    private static JSONObject jsonInvoke(CloseableHttpClient httpclient,  
            HttpUriRequest httpost)  throws Exception{  
          
        HttpResponse response = sendRequest(httpclient, httpost);  
        
        JSONObject resultJsonObject = null;
        if (response.getStatusLine().getStatusCode() == 200) {
        	resultJsonObject = jsonPaseResponse(response);
        }  
          
        return resultJsonObject;  
    }
  
    private static String paseResponse(HttpResponse response) throws Exception{  
//        log.info("get response from http server..");  
        HttpEntity entity = response.getEntity();  
          
//        log.info("response status: " + response.getStatusLine());  
        String charset = EntityUtils.getContentCharSet(entity);  
//        log.info(charset);  
          
        String body = null;    
        body = EntityUtils.toString(entity);  
//        log.info(body);   
          
        return body;  
    }  
    
    private static JSONObject jsonPaseResponse(HttpResponse response) throws Exception{  
//        log.info("get response from http server..");  
        HttpEntity entity = response.getEntity();  
          
        JSONObject resultJsonObject = null;
//        log.info("response status: " + response.getStatusLine());  
        if (null != entity){
				BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(entity.getContent(),"UTF-8"), 8 * 1024);
				StringBuilder entityStringBuilder = new StringBuilder();
				String line = null;
				while ((line = bufferedReader.readLine()) != null) {
					entityStringBuilder.append(line);
				}
				// 利用从HttpEntity中得到的String生成JsonObject
				resultJsonObject = JSON.parseObject(entityStringBuilder.toString());
			}      	
          
        return resultJsonObject;  
    }     
  
    private static HttpResponse sendRequest(CloseableHttpClient httpclient,  
            HttpUriRequest httpost) {  
//        log.info("execute post...");  
        HttpResponse response = null;  
          
        try {  
            response = httpclient.execute(httpost);  
        } catch (ClientProtocolException e) {  
            e.printStackTrace();  
        } catch (IOException e) {  
            e.printStackTrace();  
        }  
        return response;  
    }  
  
    private static HttpPost postForm(String url, Map<String, Object> params, String recordName){  
          
        HttpPost httpost = new HttpPost(url);  
        List<NameValuePair> nvps = new ArrayList <NameValuePair>();  
          
        Set<String> keySet = params.keySet();  
        for(String key : keySet) {  
        	if (null != params.get(key)){
        		if (null != recordName && !recordName.equals(""))
        		    nvps.add(new BasicNameValuePair(recordName+"."+key, params.get(key).toString()));
        		else
        			nvps.add(new BasicNameValuePair(key, params.get(key).toString()));
        	}              
        }  
          
        try {  
//            log.info("set utf-8 form entity to httppost");  
            httpost.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));  
        } catch (UnsupportedEncodingException e) {  
            e.printStackTrace();  
        }  
          
        return httpost;  
    }      
}
