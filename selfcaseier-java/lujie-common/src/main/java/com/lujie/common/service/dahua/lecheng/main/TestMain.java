package com.lujie.common.service.dahua.lecheng.main;

import java.util.HashMap;

import com.alibaba.fastjson.JSONObject;
import com.lujie.common.service.dahua.lecheng.util.HttpSend;

public class TestMain {

    public static void main(String[] args) {

    	// 获取管理员token
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        String method = "accessToken";
        JSONObject json = HttpSend.execute(paramsMap, method);
        
        JSONObject jsonResult = json.getJSONObject("result");
        JSONObject jsonData = jsonResult.getJSONObject("data");
        String token = jsonData.getString("accessToken");
        System.out.println(token);
    }

}
