package com.lujie.common.util;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.github.pagehelper.util.StringUtil;
import com.lujie.common.entity.EntityMap;

/**
 * 类对象操作
 * @author zzm
 *
 */
public class ClassObjectUtil { 
	
	
	private Integer id;
	private String name;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	
	public static Object mapToObject(Class clazz,Map<String,String> map) throws Exception{
		if(map==null) return null;
		
		Object obj=clazz.newInstance();
		mapToClassObject(obj,map);
		return obj;
	}
	
	public static Map<String,String> getMap(Map<String,String[]> map) throws Exception{
		if(map==null && map.size()==0) return null;
		
		Map<String,String> resultMap=new HashMap<>();
		for(Map.Entry<String, String[]> entry:map.entrySet()){
			String[] valueArr=entry.getValue();
			String value=null;
			if(valueArr!=null && valueArr.length>0){
				if(!StringUtil.isEmpty(valueArr[0])){
					value=valueArr[0].trim();
					if(value.toLowerCase().equals("null")){
						continue;
					}
				}
			}
			resultMap.put(entry.getKey(),value);
		}
		
		return resultMap;
	}
	
	public static EntityMap getEntityMap(Map<String,String[]> map) throws Exception{
		if(map==null && map.size()==0) return null;
		
		EntityMap resultMap=new EntityMap();
		for(Map.Entry<String, String[]> entry:map.entrySet()){
			String[] valueArr=entry.getValue();
			String value=null;
			if(valueArr!=null && valueArr.length>0){
				if(!StringUtil.isEmpty(valueArr[0])){
					value=valueArr[0].trim();
					if(value.toLowerCase().equals("null")){
						continue;
					}
				}
			}
			resultMap.put(entry.getKey(),value);
		}
		
		return resultMap;
	}
	
	public static void setObjectFromArrValueMap(Object obj,Map<String,String[]> map) throws Exception{
		if(map==null) return ;
		
		List<String> convertEncodeFieldList=Arrays.asList(new String[]{"worksitename","sitename"});
		
		Map<String,String> resultMap=new HashMap<>();
		for(Map.Entry<String, String[]> entry:map.entrySet()){
			String[] valueArr=entry.getValue();
			String value=null;
			if(valueArr!=null && valueArr.length>0){
				if(!StringUtil.isEmpty(valueArr[0])){
					value=valueArr[0].trim();
					if(value.toLowerCase().equals("null")){
						continue;
					}
					
//					if(!StringUtil.isEmpty(entry.getKey()) && !StringUtil.isEmpty(value)){
//						if(convertEncodeFieldList.contains(entry.getKey().toLowerCase())){
//							byte[] decodeBytes = java.util.Base64.getDecoder().decode(value);
//							String decodeStr=new String(decodeBytes);
//							value=decodeStr;
//						}
//					}
					if(!StringUtil.isEmpty(entry.getKey()) && !StringUtil.isEmpty(value)){
						if(convertEncodeFieldList.contains(entry.getKey().toLowerCase())){
							value=URLDecoder.decode(value,"utf-8");
						}
					}
					
					
				}
			}
			resultMap.put(entry.getKey(),value);
		}
		
		 mapToClassObject(obj,resultMap);
	}
	
	/*
	public static String getObjectValue(Object obj){
		
		Method[] methods=obj.getClass().getMethods();
		Method[] declaredMethods=obj.getClass().getDeclaredMethods();
		if(methods==null) return "";
		if(declaredMethods==null) return "";
		
		
		for(Method method:methods){
			if(method==null) continue;
			method.invoke(obj, args)
		}
		
	}*/
	
	public static void mapToClassObject(Object obj,Map<String,String> map) throws Exception{
		
		for(String fieldName:map.keySet()){
			System.out.println(fieldName);
			String value=map.get(fieldName);
			if(StringUtil.isEmpty(value)) continue;
			
			Object objValue=value;
			try{
				Method method=obj.getClass().getMethod(getFieldMethodName(fieldName));
				
				//Field field=obj.getClass().getDeclaredField(key);
				String typeName=method.getReturnType().toString().toLowerCase();
				if(typeName.contains("boolean")){
					objValue=Boolean.parseBoolean(value);
				}else if(typeName.contains("char")){
					
				}else if(typeName.contains("byte")){
					objValue=Byte.parseByte(value);
				}else if(typeName.contains("integer")){
					objValue=Integer.parseInt(value);
				}else if(typeName.contains("short")){
					objValue=Short.parseShort(value);
				}else if(typeName.contains("long")){
					objValue=Long.parseLong(value);
				}else if(typeName.contains("float")){
					objValue=Float.parseFloat(value);
				}else if(typeName.contains("double")){
					objValue=Double.parseDouble(value);
				}else if(typeName.contains("date")){
					objValue=DateUtil.parserDate(value,DateUtil.PATTERN_SIMPLE_DATETIME);
				}
				
				setPropoertyValueByMethod(obj,fieldName,objValue);
				
			}catch(java.lang.NoSuchMethodException ex){
				continue;
			}
		}
	}
	
	public static void main(String[] args){
		ClassObjectUtil cro=new ClassObjectUtil();
		try {
			
			
			
			
			Boolean flag=Boolean.parseBoolean(ClassObjectUtil.booleanToIntStr("true"));
			System.out.println(flag);
		} catch (Exception e) {
			e.printStackTrace();
		}
	} 
	public static String getClassName(Class objclass){
		String name=objclass.getName();
		int index=name.lastIndexOf(".");
		if(index!=-1){
			name=name.substring(index+1);
		}
		return name;
	}
	public static boolean isStringType(Class type){
		String typename=getClassName(type).toUpperCase();
		if(typename.equals("String".toUpperCase())){
			return true;
		}
		return false;
	}
	public static String getFieldMethodName(String fieldname){
		fieldname = fieldname.substring(0, 1).toUpperCase() + fieldname.substring(1);
		return "get"+fieldname; 
	}
	public static Boolean getBooleanValue(String val){
		try{
			return Boolean.valueOf(val);
		}catch(Exception ex){
			try{
				if(Integer.valueOf(val)>0){
					return true;
				}else{
					return false;
				}
			}catch(Exception ex2){}
		}
		return null;
	}
	public static String booleanToIntStr(String intval){
		if(intval.toUpperCase().equals("TRUE")){
			intval="1";
		}else if(intval.toUpperCase().equals("FALSE")){
			intval="0";
		}
		return intval;
	}
	public static String getFieldSetMethodName(String fieldname){
		fieldname = fieldname.substring(0, 1).toUpperCase() + fieldname.substring(1);
		return "set"+fieldname; 
	}
	public static void setFieldValue(Object obj,String fieldname,Object value) throws Exception{
		Method thisclassMethod=obj.getClass().getMethod(getFieldSetMethodName(fieldname), value.getClass());
		thisclassMethod.invoke(obj, value);
		
	}
	
	public static Object getFieldValue(Object Object,String fieldname) throws Exception{
		Method thisclassMethod=Object.getClass().getMethod(getFieldMethodName(fieldname));
		return thisclassMethod.invoke(Object);
	}
	
	public static void setValue(Object Object,Class thisclass,String methodname,Class ... para){
		//Method thisclassMethod=thisclass.getMethod(methodname, para);
		//thisclassMethod.invoke(Object, beans);
		
	}
	
	public static HashMap<String, Object> BeanToMap(Object obj)
			throws IntrospectionException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {

		if (obj == null) {
			return null;
		}
		HashMap<String, Object> map = new HashMap<String, Object>();
		BeanInfo beanInfo = Introspector.getBeanInfo(obj.getClass());
		PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
		for (PropertyDescriptor property : propertyDescriptors) {
			String key = property.getName();

			// 过滤class属性
			if (!key.equals("class")) {
				// 得到property对应的getter方法
				Method getter = property.getReadMethod();
				Object value = getter.invoke(obj);

				map.put(key, value);
			}
		}

		return map;
	}
	public static void setPropoertyValue(Object obj,HashMap map) throws Exception{
		Iterator iterator=map.entrySet().iterator();
		while(iterator.hasNext()){
			Map.Entry<String, Object> entry= (Map.Entry<String, Object>)iterator.next();
			setPropoertyValueByMethod(obj,entry.getKey().toString(),entry.getValue());
		}
	}
	
	public static void setPropoertyValueByMethod(Object obj,String fieldName,Object fieldvalue) throws Exception{
		
		try{
			Method getMethod=obj.getClass().getMethod(getFieldMethodName(fieldName));
			
			fieldName = fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
			String methodName="set"+fieldName;
			Method thisclassMethod=obj.getClass().getMethod(methodName, getMethod.getReturnType());
			thisclassMethod.invoke(obj, fieldvalue);
				 
			}catch(Exception e2){
				 
			}
			
	}
	
	public static void setPropoertyValueByMethod(Object obj,String FieldName,Object fieldvalue,Class fieldType) throws Exception{
		
		try{
			FieldName = FieldName.substring(0, 1).toUpperCase() + FieldName.substring(1);
			String methodName="set"+FieldName;
			Method thisclassMethod=obj.getClass().getMethod(methodName, fieldType);
			thisclassMethod.invoke(obj, fieldvalue);
				 
			}catch(Exception e2){
				 
			}
			
	}
	
	public static void setPropoertyValueByField(Object obj,String FieldName,Object fieldvalue) throws Exception{
		
		try{
			Field field=obj.getClass().getDeclaredField(FieldName);
			field.set(obj, fieldvalue);
			}catch(Exception e2){
				e2.printStackTrace();
			}
			
	}
	
	public static void setPropoertyValue(Object obj,Field field,Object fieldvalue) throws Exception{
		
		try{
			if(field==null || fieldvalue==null){
				return;
			} 
			
			if(field.getType() == java.lang.Double.class){
				fieldvalue=Double.parseDouble(fieldvalue.toString());
			}
			else if(field.getType()==java.lang.Float.class){
				fieldvalue=Float.parseFloat(fieldvalue.toString());
			}
			else if(field.getType() == java.lang.Boolean.class){
				fieldvalue=getBooleanValue(fieldvalue.toString());
			}
			else if(field.getType() == java.lang.Integer.class){
				fieldvalue=Integer.parseInt(ClassObjectUtil.booleanToIntStr(fieldvalue.toString()));
			}
			else if(field.getType() == java.lang.Long.class){
				fieldvalue=Long.parseLong(fieldvalue.toString());
			}
			else if(field.getType() == java.lang.Short.class){
				fieldvalue=Short.parseShort(ClassObjectUtil.booleanToIntStr(fieldvalue.toString()));
			}
			else if(field.getType() == java.lang.Byte.class){
				fieldvalue=Byte.parseByte(ClassObjectUtil.booleanToIntStr(fieldvalue.toString()));
			}
			else if(field.getType() == java.math.BigInteger.class){ 
				fieldvalue=new BigInteger(fieldvalue.toString());
			}
			else if(field.getType() == java.math.BigDecimal.class){
				fieldvalue=new BigDecimal(fieldvalue.toString());
			}
			
			
			String methodName=ClassObjectUtil.getFieldSetMethodName(field.getName());
			Method thisclassMethod=obj.getClass().getMethod(methodName, field.getType());
			thisclassMethod.invoke(obj, fieldvalue);
				 
			}catch(Exception e2){
				setPropoertyValueByField(obj,field.getName(),fieldvalue);
			}
			
		}
	public static void setPropoertyValue(Object obj,String fieldName,Object fieldvalue) throws Exception{
		
		try{
			setPropoertyValue(obj,obj.getClass().getDeclaredField(fieldName),fieldvalue); 
		}catch(Exception e2){
			//e2.printStackTrace();
		}	
			
	}
	/**
	 * 通过字符串，找到field
	 * @param thisclass
	 * @param name
	 * @throws Exception
	 */
	public static Field getFieldByLikeName(Class thisclass,String name) throws Exception{
		
		try{
			Field[] fields=thisclass.getDeclaredFields();
			for(Field field:fields){
				if(field.getName().toUpperCase().equals(name.toUpperCase())){
					return field;
				}
			}
			}catch(Exception e2){
				 
			}
		return null;
			
	}
	public static HashMap<String,String> getFieldColumnNameMap(Class thisclass) throws Exception{
		
		try{
			HashMap<String,String> fieldcolumnNameMap=new HashMap<String,String>();
			Field[] fields=thisclass.getDeclaredFields();
			for(Field field:fields){
				fieldcolumnNameMap.put(field.getName().toUpperCase(), field.getName());
			}
			return fieldcolumnNameMap;
		}catch(Exception e2){
			 
		}
		return null;
			
	}
	public String getDateId(Object obj){
		return MD5Util.encrypt(JsonHelper.toJson(obj));
	}
	public void setDateId(Object obj) throws Exception{
		String dataId=MD5Util.encrypt(JsonHelper.toJson(obj));
		
		String methodName="setDateId";
		Method thisclassMethod=obj.getClass().getMethod(methodName, java.lang.String.class);
		thisclassMethod.invoke(obj, dataId);
	}
}
