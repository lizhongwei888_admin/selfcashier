package com.lujie.common.dao;

import com.lujie.common.entity.EntityMap;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * 
 * @author sl
 *
 */

@Repository
public interface BasicMapper <T>  {

	public List<T> findAll();
	
	/**
	 * 根据多个id批量查询记录
	 * @param ids
	 * @return
	 */
	public List<T> findByIds(@Param("p")String ids);
	public T findById(@Param("p")String id);
	
	
	/**
	 * 批量保存记录
	 * @param checkInList
	 */
	public void batchSave(@Param("p")List<T> list);
	public void save(@Param("p") T p);
	
	
	/**
	 * 批量更新数据
	 * @param checkInList
	 */
	public void batchUpdate(@Param("p")List<T> list);
	
	public Integer update(@Param("p") T  p);
	
	/**
	 * 根据对象属性，查找对象列表
	 * @param property
	 * @return
	 */
	public List<T> findPageList(@Param("p") T property);
	
	/**
	 * 根据对象属性，计算对象总数
	 * @param property
	 * @return
	 */
	public Long countTotal(@Param("p") T property);
	
	/**
	 * 根据对象属性，查找所有记录，并且如果对象属性都为空，则返回所有记录
	 * @param property
	 * @return
	 */
	public List<T> findList(@Param("p") T property);
	/**
	 * 获取最大id,根据条件
	 * @param 
	 * @return
	 */
	public Integer findMaxId(@Param("p") T property);
	
	
	public Integer deleteByIds(@Param("p")String ids);
	
	/**
	 * 根据条件删除
	 * @param property
	 */
	public void delete(@Param("p") T property);

	/**
	 * 根据属性查找一条记录
	 *
	 * @param property
	 * @return
	 */
	public T findOne(@Param("p") T property);

	public T sum(@Param("p") T property);

	public List<EntityMap> getColumnInfo(@Param("p") EntityMap p);

	int add(@Param("p") T property);

	int addBatch(@Param("list") List<T> list);

	int up(@Param("p") T property);

	Long deleteByIdSet(@Param("p") Set<T> idSet);

	T findOneByParams(@Param("p") T property);


}
