package com.lujie.common.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.primitives.Bytes;

import io.netty.buffer.ByteBufUtil;

public class TcpProtocolUtil {

	private static final Logger logger = LogManager.getLogger(TcpProtocolUtil.class);
	
    private static final int crctab16[] = {
            0X0000, 0X1189, 0X2312, 0X329B, 0X4624, 0X57AD, 0X6536, 0X74BF,
            0X8C48, 0X9DC1, 0XAF5A, 0XBED3, 0XCA6C, 0XDBE5, 0XE97E, 0XF8F7,
            0X1081, 0X0108, 0X3393, 0X221A, 0X56A5, 0X472C, 0X75B7, 0X643E,
            0X9CC9, 0X8D40, 0XBFDB, 0XAE52, 0XDAED, 0XCB64, 0XF9FF, 0XE876,
            0X2102, 0X308B, 0X0210, 0X1399, 0X6726, 0X76AF, 0X4434, 0X55BD,
            0XAD4A, 0XBCC3, 0X8E58, 0X9FD1, 0XEB6E, 0XFAE7, 0XC87C, 0XD9F5,
            0X3183, 0X200A, 0X1291, 0X0318, 0X77A7, 0X662E, 0X54B5, 0X453C,
            0XBDCB, 0XAC42, 0X9ED9, 0X8F50, 0XFBEF, 0XEA66, 0XD8FD, 0XC974,
            0X4204, 0X538D, 0X6116, 0X709F, 0X0420, 0X15A9, 0X2732, 0X36BB,
            0XCE4C, 0XDFC5, 0XED5E, 0XFCD7, 0X8868, 0X99E1, 0XAB7A, 0XBAF3,
            0X5285, 0X430C, 0X7197, 0X601E, 0X14A1, 0X0528, 0X37B3, 0X263A,
            0XDECD, 0XCF44, 0XFDDF, 0XEC56, 0X98E9, 0X8960, 0XBBFB, 0XAA72,
            0X6306, 0X728F, 0X4014, 0X519D, 0X2522, 0X34AB, 0X0630, 0X17B9,
            0XEF4E, 0XFEC7, 0XCC5C, 0XDDD5, 0XA96A, 0XB8E3, 0X8A78, 0X9BF1,
            0X7387, 0X620E, 0X5095, 0X411C, 0X35A3, 0X242A, 0X16B1, 0X0738,
            0XFFCF, 0XEE46, 0XDCDD, 0XCD54, 0XB9EB, 0XA862, 0X9AF9, 0X8B70,
            0X8408, 0X9581, 0XA71A, 0XB693, 0XC22C, 0XD3A5, 0XE13E, 0XF0B7,
            0X0840, 0X19C9, 0X2B52, 0X3ADB, 0X4E64, 0X5FED, 0X6D76, 0X7CFF,
            0X9489, 0X8500, 0XB79B, 0XA612, 0XD2AD, 0XC324, 0XF1BF, 0XE036,
            0X18C1, 0X0948, 0X3BD3, 0X2A5A, 0X5EE5, 0X4F6C, 0X7DF7, 0X6C7E,
            0XA50A, 0XB483, 0X8618, 0X9791, 0XE32E, 0XF2A7, 0XC03C, 0XD1B5,
            0X2942, 0X38CB, 0X0A50, 0X1BD9, 0X6F66, 0X7EEF, 0X4C74, 0X5DFD,
            0XB58B, 0XA402, 0X9699, 0X8710, 0XF3AF, 0XE226, 0XD0BD, 0XC134,
            0X39C3, 0X284A, 0X1AD1, 0X0B58, 0X7FE7, 0X6E6E, 0X5CF5, 0X4D7C,
            0XC60C, 0XD785, 0XE51E, 0XF497, 0X8028, 0X91A1, 0XA33A, 0XB2B3,
            0X4A44, 0X5BCD, 0X6956, 0X78DF, 0X0C60, 0X1DE9, 0X2F72, 0X3EFB,
            0XD68D, 0XC704, 0XF59F, 0XE416, 0X90A9, 0X8120, 0XB3BB, 0XA232,
            0X5AC5, 0X4B4C, 0X79D7, 0X685E, 0X1CE1, 0X0D68, 0X3FF3, 0X2E7A,
            0XE70E, 0XF687, 0XC41C, 0XD595, 0XA12A, 0XB0A3, 0X8238, 0X93B1,
            0X6B46, 0X7ACF, 0X4854, 0X59DD, 0X2D62, 0X3CEB, 0X0E70, 0X1FF9,
            0XF78F, 0XE606, 0XD49D, 0XC514, 0XB1AB, 0XA022, 0X92B9, 0X8330,
            0X7BC7, 0X6A4E, 0X58D5, 0X495C, 0X3DE3, 0X2C6A, 0X1EF1, 0X0F78,
    };

    public static short generateDataCRC(List<Byte> crcData) {
        logger.debug("CRC data: {}", ByteBufUtil.hexDump(Bytes.toArray(crcData)));
        int fcs = 0xffff;
        for (int i = 0;i < crcData.size(); i++){
           fcs = (fcs>>8) ^ crctab16[(fcs ^ crcData.get(i)) & 0xff];
        }
        return (short) ~fcs;
    }

    public static short generateDataCRC(byte[] crcData) {
        logger.debug("CRC data: {}", ByteBufUtil.hexDump(crcData));
        int fcs = 0xffff;
        for (int i = 0;i < crcData.length; i++){
            fcs = (fcs>>8) ^ crctab16[(fcs ^ crcData[i]) & 0xff];
        }
        return (short) ~fcs;
    }
    
    public static byte[] shortToBytes(short[] data){
    	byte[] byteValue = new byte[data.length * 2];
    	
    	for (int i = 0; i < data.length; i++) {
    		byteValue[i * 2] = (byte) (data[i] & 0xff);
    		byteValue[i * 2 + 1] = (byte) ((data[i] & 0xff00) >> 8);
    	}
    	return byteValue;
    }
    public static byte[] shortToBytes(short data){
    	byte[] byteValue = new byte[2];
    	
    	byteValue[0] = (byte) ((data & 0xff00) >> 8);
    	byteValue[1] = (byte) (data & 0xff);
		
    	return byteValue;
    }
    public static short bytesToShort(byte[] data){
    	return (short) (((data[0] & 0xff) << 8) | (data[1] & 0xff));
    }
    
    public static short[] byteToShorts(byte[] data) {
    	short[] shortValue = new short[data.length / 2];
    	for (int i = 0; i < shortValue.length; i++) {
    		shortValue[i] = (short) ((data[i * 2] & 0xff) | ((data[i * 2 + 1] & 0xff) << 8));
    	}
    	return shortValue;
    }
    /**
     * 将字节数组转换成10进制
     * @param data
     * @return
     */
    public static Long bytesToDecimalism(byte[] data){
    	
    	Double sum=0.0;
    	Integer powi=0;
    	
    	//    256
    	/*
    	 * CellID 3个字节 移动基站 CellTowerID(CellID)（转换为十进制）
    	 * CellID的值1=byte[0]的10进制值+byte[1]的10进制值+byte[2]的10进制值
    	 * CellID的值2=byte[0]的10进制值*256*256+byte[1]的10进制值*256+byte[2]的10进制值
    	 * CellID的值用哪个
    	 */
    	
    	
    	for(Integer i=data.length-1;i>=0;i--){
    		sum+=data[i]*Math.pow(256.0, powi++);
    	}
    	return sum.longValue();
    }
    /**
     * 获取8位中的一位
     * @param data
     * @param position 从位置1开始，1到8 1是最高位
     * @return
     */
    public static byte getBitInfo(byte data,int position){
    	return (byte)(data >> (8-position) & 0x01);
    } 
    
    public static List<Byte> listAddBytes(List<Byte> list,byte[] bytes){
    	for(byte b:bytes){
    		list.add(b);
    	}
    	return list;
    }
    
    public static void testgetDecimal(){
    	byte[] data=new byte[3];
    	data[0]=0;
    	data[1]=1;
    	data[2]=1;
    	System.out.println(bytesToDecimalism(data));//257
    	
    	
    }
    
    public static Integer byteToInt(byte data){
    	if(data<0) return 256+data;
    	return (int)data;
    }
    
    public static void testbyteToInt(){
    	System.out.println(bytesToDecimalism(new byte[]{(byte)0xe6,(byte)0xe5}));
    }
    
    
    public static void main(String[] args){

    	testGetFullMsg2();
    	
    }
    
    
    public static void testGetFullMsg(){
    	
    	System.out.println(getFullMsgStr("id123", "$aaaa"));
    	System.out.println(clientMsgMap);
    	
    	System.out.println(getFullMsgStr("id123", "1111"));
    	System.out.println(clientMsgMap);
    	
    	
    	System.out.println(getFullMsgStr("id123", "222"));
    	System.out.println(clientMsgMap);
    	
    	System.out.println(getFullMsgStr("id123", "bbb#"));
    	System.out.println(clientMsgMap);
    	
    	System.out.println(getFullMsgStr("id123", "asdf#$cccddde1#$cccddde2#$sdf"));
    	System.out.println(clientMsgMap);
    	
    	System.out.println(getFullMsgStr("id123", "52aa#$cccdd"));
    	System.out.println(clientMsgMap);
    	
    	
    }
    
    public static void printMsgBytes(List<byte[]> fullmsgbytes){
    	System.out.println("printMsgBytes:");
    	if(fullmsgbytes!=null){
    		for(byte[] bytes:fullmsgbytes){
    			for(byte b:bytes){
    				System.out.print(b+",");
    			}
    		}	
    	}
    	
    	System.out.println();
    	
    }
    
    public static String getBytesStr(byte[] bytes){
    	StringBuffer s=new StringBuffer();
    	for(byte b:bytes){
			s.append(b+",");
		}
    	return s.toString();
    	
    }
    
    public static void printTempMsgBytes(HashMap<String,byte[]> clientBytesMsgMap){
    	System.out.println("printTempMsgBytes:");
    	
    	for(String key:clientBytesMsgMap.keySet()){
    		byte[] tempMsgBytes=clientBytesMsgMap.get(key);
    		for(byte b:tempMsgBytes){
				System.out.print(b+",");
			}
    	}
    	System.out.println();
    }
    
    public static void testGetFullMsg2(){
    	
    	//82,-27,-52,-52
    	StartFlagBytes=new byte[]{82,-27};
        EndFlagBytes=new byte[]{-52,-52};
        
        printMsgBytes(getFullMsgByte("id123", new byte[]{82}));
        printTempMsgBytes(clientBytesMsgMap);
        
        printMsgBytes(getFullMsgByte("id123", new byte[]{-27,0,1,2,3,4,5,6,-52}));
        printTempMsgBytes(clientBytesMsgMap);
        
        printMsgBytes(getFullMsgByte("id123", new byte[]{7,8,9,0,-52}));
    	printTempMsgBytes(clientBytesMsgMap);
        
    	printMsgBytes(getFullMsgByte("id123", new byte[]{-52,-52}));
    	printTempMsgBytes(clientBytesMsgMap);
        
    	
    	
    }
    
    
    public static String StartFlagStr="$";
    public static String EndFlagStr="#";
    
    private static HashMap<String,String> clientMsgMap=new HashMap<>();
    /**
     * 获取完整的协议包数据 字符串类型
     * @param channelId
     * @param msg
     * @return
     */
    public static List getFullMsgStr(String channelId,String msg){
    	
    	
    	Integer startIndex=msg.indexOf(StartFlagStr);
		Integer endIndex=msg.indexOf(EndFlagStr);
		
		if(startIndex == 0 && endIndex !=-1 ){// 至少有一个完整的包 $...#
			List msgList=new ArrayList();
			msgList.add(msg.substring(startIndex,endIndex+1));
			
			msg=msg.substring(endIndex+1);
			if(msg.length()>0){
				List msgList2=getFullMsgStr(channelId,msg);
				if(msgList2!=null){
					msgList.addAll(msgList2);
				}
			}
			return msgList;
    	}
		
		if(startIndex !=-1 && endIndex==-1){//只有开始，没有结束 $...
			clientMsgMap.put(channelId, msg);
			return null;
    	} 
		
		if(startIndex ==-1 && endIndex==-1){//只有中间信息，没有开始,没有结束  ...
			String lastMsg=clientMsgMap.remove(channelId);
	    	if(lastMsg==null) return null;
	    	
	    	lastMsg=lastMsg+msg;
			clientMsgMap.put(channelId, lastMsg);
			return null;
    	}
		
		if(startIndex ==-1 && endIndex!=-1){//有尾部信息，没有开始   ....#
			String lastMsg=clientMsgMap.remove(channelId);
	    	if(lastMsg==null) return null;
	    	
	    	msg=lastMsg+msg;//和上次拼接成完整的消息
			return Arrays.asList(new String[]{msg});
    	}
    	
		if(startIndex !=-1 && endIndex!=-1){//有开始，有尾部信息   
			if(startIndex>endIndex){//...#$...
				String lastApartMsg=clientMsgMap.remove(channelId);//获取上次不完整信息
				List msgList=new ArrayList();
				if(lastApartMsg!=null){
					lastApartMsg+=msg.substring(0,endIndex+1);//拼接成完整信息
					msgList.add(lastApartMsg);
				}
				
				msg=msg.substring(startIndex);
				if(msg.length()>0){
					List msgList2=getFullMsgStr(channelId,msg);
							if(msgList2!=null){
								msgList.addAll(msgList2);
							}
				}
				
				return msgList;
			}
		}
		
    	return null;
    }
    
    public static byte[] StartFlagBytes=new byte[]{-82,-27};
    public static byte[] EndFlagBytes=new byte[]{-52,-52};
    private static HashMap<String,byte[]> clientBytesMsgMap=new HashMap<>();
    
    /**
     * 获取子字节数组在源字节数组中的位置
     * @param data
     * @param sub
     * @return
     */
    public static Integer getSubInSourceIndex(byte[] source,byte[] sub){
    	
    	if(source.length==0) return -1;
    	
    	if(source.length<sub.length) return -1;
    	
    	for(int i=0;i<source.length;i++){
    		if((i+sub.length)<=source.length){
    			boolean find=true;
        		for(int j=0;j<sub.length;j++){
        			if(source[i+j]!=sub[j]){
        				find=false;
        				break;
        			}
        		}
    			if(find){
    				return i;
    			}
    		}
    		
    	}
    	
    	
    	return -1;
    }
    
    public static byte[] getSubBytes(byte[] sourceData,Integer startIndex,Integer endIndex){
    	byte[] buffer=new byte[endIndex-startIndex+1];
    	System.arraycopy(sourceData, startIndex, buffer, 0, buffer.length);	
    	return buffer;
    }
    
    public static byte[] mergeSubBytes(byte[] sub1,byte[] sub2){
    	byte[] buffer=new byte[sub1.length+sub2.length];
    	
    	System.arraycopy(sub1, 0, buffer, 0, sub1.length);	
    	System.arraycopy(sub2, 0, buffer, sub1.length, sub2.length);	
    	return buffer;
    }
    
    
    /**
     * 获取完整的协议包数据 字节数组类型
     * @param channelId
     * @param msg
     * @return
     */
    public static List<byte[]> getFullMsgByte(String channelId,byte[] msg){
    	
    	if(msg.length==0) return null;
    	
    	Integer startIndex=getSubInSourceIndex(msg,StartFlagBytes);//82,-27
		Integer endIndex=getSubInSourceIndex(msg,EndFlagBytes);//-52,-52
    	
		if(startIndex == 0 && endIndex !=-1 ){// 至少有一个完整的包 $...#
			List msgList=new ArrayList();
			byte[] buffer=getSubBytes(msg,startIndex,endIndex+EndFlagBytes.length-1);
			msgList.add(buffer);
			
			byte[] newmsg=getSubBytes(msg,endIndex+EndFlagBytes.length,msg.length-1);
			if(newmsg.length>0){
				List msgList2=getFullMsgByte(channelId,newmsg);
				if(msgList2!=null){
					msgList.addAll(msgList2);
				}
			}
			return msgList;
    	}
		
		if(startIndex !=-1 && endIndex==-1){//只有开始，没有结束 $...
			clientBytesMsgMap.put(channelId, msg);
			return null;
    	} 
		
		if(startIndex ==-1 && endIndex==-1){//只有中间信息，没有开始,没有结束  ...
			byte[] lastMsg=clientBytesMsgMap.remove(channelId);
	    	if(lastMsg!=null){
	    		byte[] buffer=mergeSubBytes(lastMsg,msg);
	    		
	    		Integer newstartIndex=getSubInSourceIndex(buffer,StartFlagBytes);//82,-27
				Integer newendIndex=getSubInSourceIndex(buffer,EndFlagBytes);//-52,-52
		    	
				if(newstartIndex!=-1 && newendIndex!=-1){
					List msgList2=getFullMsgByte(channelId,buffer);
					if(msgList2!=null){
						return msgList2;
					}
				}else{
					clientBytesMsgMap.put(channelId, buffer);
				}
				return null;
	    	}else{
	    		clientBytesMsgMap.put(channelId, msg);
				return null;
	    	}
    	}
		
		if(startIndex ==-1 && endIndex!=-1){//有尾部信息，没有开始   ....#
			byte[] lastMsg=clientBytesMsgMap.remove(channelId);
	    	if(lastMsg==null) return null;
	    	
	    	byte[] buffer=mergeSubBytes(lastMsg,msg);//和上次拼接成完整的消息
	    	
	    	List msgList=new ArrayList();
	    	msgList.add(buffer);
			return msgList;
    	}
    	
		if(startIndex !=-1 && endIndex!=-1){//有开始，有尾部信息   
			if(startIndex>endIndex){//...#$...
				byte[] lastApartMsg=clientBytesMsgMap.remove(channelId);;//获取上次不完整信息
		    	List msgList=new ArrayList();
				if(lastApartMsg!=null){
					byte[] buffer0=getSubBytes(msg,0,endIndex+EndFlagBytes.length-1);//abc##$
					byte[] buffer=mergeSubBytes(lastApartMsg,buffer0);//和上次拼接成完整的消息
			    	msgList.add(buffer);
				}
				
				byte[] buffer=getSubBytes(msg,startIndex,msg.length-1);
				
				if(buffer.length>0){
					List msgList2=getFullMsgByte(channelId,buffer);
							if(msgList2!=null){
								msgList.addAll(msgList2);
							}
				}
				
				return msgList;
			}
		}
		
    	return null;
    }
    
    
    /**
     * 处理粘包和被拆分的包，生成有效完整的包
     * 
     * 1、粘包，是多个包合在一起，部分包或者多个包合在一起，
     * 2、被拆分的包，是一个包被拆分成多个不完整的包
     * @param channelId
     * @param msg
     * @return
     */
    public static List getFullMsg(String channelId,String msg){
    	
    	
    	Integer startIndex=msg.indexOf("$");
		Integer endIndex=msg.indexOf("#");
		
		if(startIndex == 0 && endIndex !=-1 ){// 至少有一个完整的包 $...#
			List msgList=new ArrayList();
			msgList.add(msg.substring(startIndex,endIndex+1));
			
			msg=msg.substring(endIndex+1);
			if(msg.length()>0){
				List msgList2=getFullMsg(channelId,msg);
				if(msgList2!=null){
					msgList.addAll(msgList2);
				}
			}
			return msgList;
    	}
		
		if(startIndex !=-1 && endIndex==-1){//只有开始，没有结束 $...
			clientMsgMap.put(channelId, msg);
			return null;
    	} 
		
		if(startIndex ==-1 && endIndex==-1){//只有中间信息，没有开始,没有结束  ...
			String lastMsg=clientMsgMap.remove(channelId);
	    	if(lastMsg==null) return null;
	    	
	    	lastMsg=lastMsg+msg;
			clientMsgMap.put(channelId, lastMsg);
			return null;
    	}
		
		if(startIndex ==-1 && endIndex!=-1){//有尾部信息，没有开始   ....#
			String lastMsg=clientMsgMap.remove(channelId);
	    	if(lastMsg==null) return null;
	    	
	    	msg=lastMsg+msg;//和上次拼接成完整的消息
			return Arrays.asList(new String[]{msg});
    	}
    	
		if(startIndex !=-1 && endIndex!=-1){//有开始，有尾部信息   
			if(startIndex>endIndex){//...#$...
				String lastApartMsg=clientMsgMap.remove(channelId);//获取上次不完整信息
				List msgList=new ArrayList();
				if(lastApartMsg!=null){
					lastApartMsg+=msg.substring(0,endIndex+1);//拼接成完整信息
					msgList.add(lastApartMsg);
				}
				
				msg=msg.substring(startIndex);
				if(msg.length()>0){
					List msgList2=getFullMsg(channelId,msg);
							if(msgList2!=null){
								msgList.addAll(msgList2);
							}
				}
				
				return msgList;
			}
		}
		
    	return null;
    }
    
    public static Map<String,String> getBusinessDataMap(String businessData){
		String[] businessDataArr=businessData.trim().split(",");
    	Map<String,String> businessDataMap=new HashMap<>();
    	for(String data:businessDataArr){
    		String[] arr=data.split(":");
    		if(arr.length>1){
    			businessDataMap.put(arr[0].trim(), arr[1].trim());
    		}else{
    			businessDataMap.put(arr[0].trim(), "");
    		} 
    	}
    	return businessDataMap;
	}
    
    
}
