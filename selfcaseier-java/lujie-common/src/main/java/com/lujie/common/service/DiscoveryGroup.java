package com.lujie.common.service;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lujie.common.util.HttpRequest;
import com.lujie.common.util.StringUtil;

import net.sf.json.JSONObject;

/**
 * 
 * 
 * 
 * 
 * 发现服务提供者
 * 给客户端提供有哪些服服务器可以使用
 * 
 * 实现原理：
 * 集群，多个服务器组成，
 * 集群发起者，第一个加入集群的服务器，其他服务器通过加入发起者来加入集群
 * 
 * 
 * socket客户端使用tcpip协议或者或者web客户端使用http协议与服务器进行通信时候，
 * 
 * 
 * @author Administrator
 *
 */
public class DiscoveryGroup {
	private static final Logger log = LoggerFactory.getLogger(DiscoveryGroup.class);
	
	public static JSONObject LatestServersJson=new JSONObject();
	
	/**
	 * 加入集群，集群返回服务器列表
	 * @param groupheadUrl
	 * @param selfIp
	 * @param selfPort
	 * @param protocol
	 */
	public static void joinToGroup(String groupheadUrl,String selfIp,String selfPort,String protocol) {
		if(StringUtil.isEmpty(groupheadUrl)){
			log.info("沒有配置集群第一个服务器");
			return;
		}
		final String serviceUrl=groupheadUrl+"/serverlist/joinToGroup";
		Runnable runnable = new Runnable() {
            @Override
            public void run() {
            	try{
            		HashMap<String,String> reqHeaderMap=new HashMap<>();
            		reqHeaderMap.put("Content-Type","application/json;charset=utf-8");
            		/*
            		JSONObject json=new JSONObject();
            		json.put("ip", selfIp);
            		json.put("port", selfPort);
            		json.put("protocol", protocol);
            		
            		String result = HttpRequest.sendPost(reqHeaderMap, serviceUrl,  json.toString());
            		log.info("调用加入集群接口返回结果:"+result);
            		
            		JSONObject resultMsg=JSONObject.fromObject(result);
            		String code=resultMsg.getString("code");
            		if(StringUtil.isEmpty(code) || !code.equals("200")){
            			return;
            		}
            		LatestServersJson=resultMsg.getJSONObject("data");
            		*/
            	}catch(Exception ex){
            		ex.printStackTrace();
            		log.info("调用加入集群接口返回异常:"+StringUtil.getExceptionDesc(ex));
            	}
            	
        	}
        };
        new Thread(runnable).start(); 
        log.info("加入集群 start");
		
	}
	
	
	public void doService(){
		
		try{
			Thread.sleep(10000L);
    		
			
			
		}catch(Exception ex){
			ex.printStackTrace();
			log.info(ex.getMessage());
		}
	} 
}
