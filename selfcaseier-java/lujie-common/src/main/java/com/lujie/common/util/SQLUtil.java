package com.lujie.common.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.lujie.common.entity.EntityMap;

public class SQLUtil{
	

	/*
	 * sample create sql:
	 CREATE TABLE ${p} (
		 bdLatitude varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
		  bdLongitude varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
		  modifyTime datetime DEFAULT NULL,
		  PRIMARY KEY (deviceUniqueID,recordTime,vehicleFleetCompanyID,vehicleFleetID,vehicleID),
		  UNIQUE KEY IDX_Group (deviceUniqueID,recordTime) USING BTREE,
		  KEY IDX_QueryTrace (recordTime,vehicleID,recordSpeed) USING BTREE
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='��������������?';
		      
		   
	 * 
	 * */
	public static String generateCreateTableSQL(String sourceTableName,String newTableName,List<EntityMap> columnMapList){
		StringBuffer sCreate=new StringBuffer();
		sCreate.append("create table ");
		sCreate.append(newTableName).append(" (");
		
		for(EntityMap columnMap:columnMapList){
			
			EntityMap formatColumnMap=EntityMap.formatEntityMapKey(columnMap);
			
			String dbColumnName=formatColumnMap.getStr("COLUMN_NAME");
			String COLUMN_TYPE=formatColumnMap.getStr("COLUMN_TYPE");
			String IS_NULLABLE=formatColumnMap.getStr("IS_NULLABLE");//NO  YES
			String EXTRA=formatColumnMap.getStr("EXTRA");//auto_increment
			String COLUMN_KEY=formatColumnMap.getStr("COLUMN_KEY");
			
			
			String charset="";
			if(COLUMN_TYPE.toLowerCase().contains("varchar") 
					||COLUMN_TYPE.toLowerCase().contains("text")){
				charset=" COLLATE utf8_unicode_ci ";
			}
			
			String DEFAULTNULL=" DEFAULT NULL ";
			if(IS_NULLABLE.toUpperCase().equals("NO")){
				DEFAULTNULL=" NOT NULL ";
			}
			
			if(EXTRA==null) {
				EXTRA="";
			}else{
				EXTRA=" "+EXTRA+" ";
			}
			
			sCreate.append(dbColumnName).append(" ")
					.append(COLUMN_TYPE).append(" ")
					.append(charset)
					.append(DEFAULTNULL)
					.append(EXTRA)
					.append(",");
			
			if(COLUMN_KEY!=null && COLUMN_KEY.toUpperCase().equals("PRI")){
				sCreate.append(" PRIMARY KEY ("+dbColumnName+") USING BTREE ,");
			}
		}
		sCreate.delete(sCreate.length()-1, sCreate.length());
		
		sCreate.append(") ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='history table';");
		return sCreate.toString();
	}
	
	public static void main(String[] args){
		EntityMap map=new EntityMap();
		System.out.println(map.keySet());
	}
	
	public static String generateNewColumnsSQL(String tableName,List<EntityMap> sourceTableColumnMapList,List<EntityMap> historyTableColumnMapList){
		EntityMap newColumnMap=getNewColumnList(sourceTableColumnMapList,historyTableColumnMapList);
		if(newColumnMap==null || newColumnMap.keySet().size()==0) return "";
		
		return generateNewColumnsSQL(tableName,newColumnMap);
	}
	
	////alter table  表名  add (字段 字段类型)  [ default  '输入默认值']  [null/not null]  ;
	//ALTER TABLE employee  ADD  spbh varchar(20) NOT NULL Default 0
	public static String generateNewColumnsSQL(String tableName,EntityMap columnMap){
		StringBuffer sCreate=new StringBuffer();
		
		for(Object columnName:columnMap.keySet()){
			EntityMap dbEntityMap=columnMap.getEntityMap(columnName.toString());
			EntityMap formatColumnMap=EntityMap.formatEntityMapKey(dbEntityMap);
			
			String COLUMN_TYPE=formatColumnMap.getStr("COLUMN_TYPE");
			String IS_NULLABLE=formatColumnMap.getStr("IS_NULLABLE");//NO  YES
			String charset="";
			if(COLUMN_TYPE.toLowerCase().contains("varchar") 
					||COLUMN_TYPE.toLowerCase().contains("text")){
				charset=" COLLATE utf8_unicode_ci ";
			}
			
			String DEFAULTNULL=" DEFAULT NULL ";
			if(IS_NULLABLE.toUpperCase().equals("NO")){
				DEFAULTNULL=" NOT NULL ";
			}
			
			sCreate.append("alter table ")
					.append(tableName)
					.append(" add ")
					.append(columnName).append(" ")
					.append(COLUMN_TYPE).append(" ")
					.append(charset)
					.append(DEFAULTNULL)
					.append(";");
			
		}
		
		
		sCreate.delete(sCreate.length()-1, sCreate.length());
		return sCreate.toString();
	}
	
	
	public static EntityMap getNewColumnList(List<EntityMap> sourceTableColumnMapList,List<EntityMap> historyTableColumnMapList){
		EntityMap newColumnMapList=new EntityMap();
		
		EntityMap historyTableColumnMap=EntityMap.toEntityMap(historyTableColumnMapList,"COLUMN_NAME");
		
		for(EntityMap columnMap:sourceTableColumnMapList){
			EntityMap formatColumnMap=EntityMap.formatEntityMapKey(columnMap);
			String dbColumnName=formatColumnMap.getStr("COLUMN_NAME");
			if(!historyTableColumnMap.keySet().contains(dbColumnName)){
				newColumnMapList.put(dbColumnName, columnMap);
			}
		}
		return newColumnMapList;
	}
	
	public Boolean isStringType(String DATA_TYPE){
		List<String> StringTypeList=Arrays.asList(new String[]{"datetime","date","varchar","text","mediumtext","longtext"});
		return StringTypeList.contains(DATA_TYPE);
	}
	
	public static String generateInsertSQL(String tableName,List<EntityMap> columnMapList){
		StringBuffer insertSQL=new StringBuffer();
		
		for(EntityMap columnMap:columnMapList){
			StringBuffer sName=new StringBuffer();
			StringBuffer sValue=new StringBuffer();
			
			
			for(Object columnName:columnMap.keySet()){
				//String COLUMN_TYPE=formatColumnMap.getStr("COLUMN_TYPE");
				Object columnValue=columnMap.get(columnName);
				
				if(columnValue instanceof java.lang.String ){
					columnValue="'"+columnValue+"'";
				}else if(columnValue instanceof java.util.Date){
					try{
						DateUtil.parserDate(columnValue.toString());
						columnValue="'"+columnValue+"'";
					}catch(Exception ex){
						columnValue="null";
					}
				}
				
				sName.append(columnName).append(",");
				sValue.append(columnValue).append(",");
			}
			sName.delete(sName.length()-1, sName.length());
			sValue.delete(sValue.length()-1, sValue.length());
			
			insertSQL.append("insert ignore into ")   //ignore 忽略重复数据
						.append(tableName).append(" (")
						.append(sName).append(") ")
						.append(" values(")
						.append(sValue).append(");");
		}
		return insertSQL.toString();
	}
	public static String generateInsertSQLs(List<String> insertSQLList){
		if(insertSQLList==null || insertSQLList.size()==0) return null;
		
		StringBuffer insertSQLs=new StringBuffer();
		
		for(String insertSQL:insertSQLList){
			insertSQLs.append(insertSQL).append(";");
		}
		return insertSQLs.toString();
	}
	
	
	public static String getTableSelectColumnSql(List<EntityMap> columnInfoList){
		StringBuilder mainColumnsSql=new StringBuilder();
		String shortTableName="t0.";
		for(EntityMap dbEntityMap:columnInfoList){
			String dbColumnName=dbEntityMap.getStr("COLUMN_NAME");
			String DATA_TYPE=dbEntityMap.getStr("DATA_TYPE");
			String COLUMN_COMMENT=dbEntityMap.getStr("COLUMN_COMMENT");
			
			
			if(DATA_TYPE.equalsIgnoreCase("datetime")){
				mainColumnsSql.append("ifnull(DATE_FORMAT("+shortTableName+dbColumnName+",'%Y-%m-%d %H:%i:%s'),'') "+dbColumnName+",");
			}else if(DATA_TYPE.equalsIgnoreCase("date")){
				mainColumnsSql.append("ifnull(DATE_FORMAT("+shortTableName+dbColumnName+",'%Y-%m-%d'),'') "+dbColumnName+",");
			}else{
				mainColumnsSql.append(dbColumnName+",");
			}
		}
		
		mainColumnsSql=mainColumnsSql.delete(mainColumnsSql.length()-1, mainColumnsSql.length());
		return mainColumnsSql.toString();
	}
	
	
	public static List<String> generateInsertSQLList(String tableName,List<EntityMap> columnMapList){
		List<String> insertSQLList=new ArrayList<>();
		
		for(EntityMap columnMap:columnMapList){
			StringBuffer sName=new StringBuffer();
			StringBuffer sValue=new StringBuffer();
			
			
			for(Object columnName:columnMap.keySet()){
				//String COLUMN_TYPE=formatColumnMap.getStr("COLUMN_TYPE");
				Object columnValue=columnMap.get(columnName);
				
				if(columnValue instanceof java.lang.String ){
					columnValue="'"+columnValue+"'";
				}else if(columnValue instanceof java.util.Date){
					try{
						DateUtil.parserDate(columnValue.toString());
						columnValue="'"+columnValue+"'";
					}catch(Exception ex){
						columnValue="null";
					}
				}
				
				sName.append(columnName).append(",");
				sValue.append(columnValue).append(",");
			}
			sName.delete(sName.length()-1, sName.length());
			sValue.delete(sValue.length()-1, sValue.length());
			
			StringBuffer insertSQL=new StringBuffer();
			insertSQL.append("insert ignore into ")   //ignore 忽略重复数据
						.append(tableName).append(" (")
						.append(sName).append(") ")
						.append(" values(")
						.append(sValue).append(")");
			insertSQLList.add(insertSQL.toString());
		}
		return insertSQLList;
	}
	
	public static String generateInsertSQL(String tableName,EntityMap columnMap){
		StringBuffer sName=new StringBuffer();
		StringBuffer sValue=new StringBuffer();
		
		for(Object columnName:columnMap.keySet()){
			//String COLUMN_TYPE=formatColumnMap.getStr("COLUMN_TYPE");
			Object columnValue=columnMap.get(columnName);
			
			if(columnValue instanceof java.lang.String ){
				columnValue="'"+columnValue+"'";
			}else if(columnValue instanceof java.util.Date){
				try{
					DateUtil.parserDate(columnValue.toString());
					columnValue="'"+columnValue+"'";
				}catch(Exception ex){
					columnValue="null";
				}
			}
			
			sName.append(columnName).append(",");
			sValue.append(columnValue).append(",");
		}
		sName.delete(sName.length()-1, sName.length());
		sValue.delete(sValue.length()-1, sValue.length());
		
		StringBuffer insertSQL=new StringBuffer();
		insertSQL.append("insert ignore into ")   //ignore 忽略重复数据
					.append(tableName).append(" (")
					.append(sName).append(") ")
					.append(" values(")
					.append(sValue).append(")");
		return insertSQL.toString();
	}
	
}