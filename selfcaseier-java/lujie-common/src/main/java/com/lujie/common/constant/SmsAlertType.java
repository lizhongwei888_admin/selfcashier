package com.lujie.common.constant;

/**
 * @author zzm
 *
 */
public enum SmsAlertType  {

	VerifyCode(11001, "验证码"),
	SysException(11002, "系统异常"),
	Overload(11003, "超载"),
	OverSpeed(11004, "超速"),
	Notify(11005, "通知")
	
	;
	
	private Integer code;

	
	private String desc;

	SmsAlertType(Integer code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public Integer getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

}
