package com.lujie.common.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lujie.common.constant.ResultMsg;
import com.lujie.common.entity.EntityMap;
import com.lujie.common.service.BaseService;
import com.lujie.common.util.ClassObjectUtil;
import com.lujie.common.util.JsonHelper;
import com.lujie.common.util.MD5Util;
import com.lujie.common.util.StringUtil;

public class BaseController {
	
private static final Logger log = LoggerFactory.getLogger(BaseController.class);
	
	public BaseService baseService;
	
	@RequestMapping(value={"/findAll"})
	@ResponseBody
	public String findAll(HttpServletRequest request,@RequestBody EntityMap entityMap)  {
		return JsonHelper.toJson(baseService.findAllEntityMap(entityMap));
	}
	@RequestMapping(value={"/list"})
	@ResponseBody
	public String list(HttpServletRequest request,@RequestBody EntityMap entityMap)  {
		return JsonHelper.toJson(baseService.findEntityMapPageList(entityMap));
	}
	@RequestMapping(value={"/detail"})
	@ResponseBody
	public String detail(HttpServletRequest request,@RequestBody EntityMap entityMap)  {
		return JsonHelper.toJson(baseService.findOneEntity(entityMap));
	}
	@RequestMapping(value={"/sum"})
	@ResponseBody
	public String sum(HttpServletRequest request,@RequestBody EntityMap entityMap)  {
		return JsonHelper.toJson(baseService.sumEntityMap(entityMap));
	}
	
	
	@RequestMapping(value={"/add"})
	@ResponseBody
	public String add(HttpServletRequest request,@RequestBody EntityMap entityMap)  {
	String lockId=MD5Util.encrypt(entityMap.toString());
		
		ResultMsg msg=null;
		try{
			msg=baseService.saveEntityMap(request,entityMap,
						"","");
			
		}catch(Exception ex){
			ex.printStackTrace();
			log.info(StringUtil.getExceptionDesc(ex));
			if(ex instanceof DuplicateKeyException){
				msg = new ResultMsg("520","不能保存重复数据");
			}else{
				msg = new ResultMsg("530","保存数据异常");
			}
		}
		 
		return JsonHelper.toJson(msg);
	}
	
	
	@RequestMapping(value={"/update"})
	@ResponseBody
	public String update(HttpServletRequest request,@RequestBody EntityMap param)  {
		ResultMsg msg=null;
		try{
			msg=baseService.updateEntityMap(param,"","");
			
		}catch(Exception ex){
			ex.printStackTrace();
			log.info(StringUtil.getExceptionDesc(ex));
			msg = new ResultMsg("630","更新异常");
		}
		
		return JsonHelper.toJson(msg);
	}
	
	@RequestMapping(value={"/delete"})
	@ResponseBody
	public String delete(HttpServletRequest request,@RequestBody EntityMap param)  {
		ResultMsg msg=null;
		try{
			msg=baseService.deleteEntityMap(param,
					"","");
		}catch(Exception ex){
			ex.printStackTrace();
			log.info(StringUtil.getExceptionDesc(ex));
			msg = new ResultMsg("630","删除异常");
		}
		
		return JsonHelper.toJson(msg);
	}
	
	@RequestMapping(value={"/export"})
	@ResponseBody
	public void export(HttpServletRequest request,HttpServletResponse response) throws Exception  {
		EntityMap entityParam=ClassObjectUtil.getEntityMap(request.getParameterMap());
		baseService.exportEntity(response, entityParam);	
	} 
	
	
	

}
