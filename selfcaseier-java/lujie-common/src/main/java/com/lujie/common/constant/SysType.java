package com.lujie.common.constant;

/**
 * @author zzm
 *
 */
public enum SysType  {

	PlatForm(10001, "平台"),
	Vehicle(10002, "车辆监控"),
	Device(10003, "设备微服务"),
	Sms(10004, "短信"),
	DeviceDataSource(10005, "设备数据源微服务"),
	DeviceTrash(10006, "设备垃圾桶微服务"),
	DeviceDeli(10007, "设备得力考勤微服务"),
	DongfeiKaoqin(10008, "东飞考勤微服务"),
	
	
	
	;
	
	private Integer code;

	
	private String desc;

	SysType(Integer code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public Integer getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

}
