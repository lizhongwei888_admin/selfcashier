package com.lujie.common.util;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;

public class FileUtil {
	
	public static void main(String[] args){
		try{
			BufferedReader br=new BufferedReader(new FileReader("samefile20221901.txt"));
			String line=null;
			while((line=br.readLine())!=null){
				if(line==null || line.trim().length()==0) continue;
				
				Integer index=line.lastIndexOf(",");
				if(index==-1) continue;
				
				String filename=line.substring(index+1).trim();
				File file=new File(filename);
				file.delete();
			}
			br.close();
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		
	}
	
	public static byte[] getAllBytes2(File file) throws Exception{
		byte[] allBytes=new byte[(int)file.length()];
		
		
		FileInputStream fis=new FileInputStream(file);
		byte[] buffer=new byte[1024];
		int len=-1;
		int destPos=0;
		while((len=fis.read(buffer))!=-1){
			System.arraycopy(buffer, 0, allBytes, destPos, len);
			destPos+=len;
		}
		fis.close();
		/*
		FileOutputStream fos=new FileOutputStream("f:/test/test2.jpg");
		fos.write(allBytes);
		fos.close();*/
		return allBytes;
	}
	
	public static byte[] getAllBytes(File file) throws Exception{
		byte[] allBytes=new byte[(int)file.length()];
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		FileInputStream fis=new FileInputStream(file);
		byte[] buffer=new byte[1024];
		int len=-1;
		while((len=fis.read(buffer))!=-1){
			baos.write(buffer,0,len);
		}
		fis.close(); 
		allBytes=baos.toByteArray();
		baos.close();
		return allBytes;
	}
	
	
	public static byte[] getAllBytes(String filePath) throws Exception{
		File file=new File(filePath);
		return getAllBytes(file);
	}
	
	public static void bytesToFile(byte[] bytes,String file) throws Exception{
		FileOutputStream fos=new FileOutputStream(file);
		fos.write(bytes);
		fos.close();
	}
	
	public static byte[] getAllBytes(DataInputStream dis) throws IOException{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] bufferbytes=new byte[1024];
		int len=0;
		while((len=dis.read(bufferbytes))!=-1){
			baos.write(bufferbytes,0,len);
		}
		byte[] allBytes=baos.toByteArray();
		baos.close();
		return allBytes;
	}
	public static byte[] getAllBytes(InputStream in) throws IOException{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] bufferbytes=new byte[1024];
		int len=0;
		while((len=in.read(bufferbytes))!=-1){
			baos.write(bufferbytes,0,len);
		}
		byte[] allBytes=baos.toByteArray();
		baos.close();
		return allBytes;
	}
	
	public static void findDirFilesAndSaveNames(String[] args){
		System.out.println("参数：");
		for(String arg :args){
			System.out.println(arg);
		}
		
		
		
	}
}
