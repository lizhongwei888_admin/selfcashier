package com.lujie.common.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.lujie.common.constant.ResultCode;
import com.lujie.common.constant.ResultMsg;
import com.lujie.common.util.DateUtil;
import com.lujie.common.util.JsonHelper;


public class UploadService {
	private final static Logger log = LoggerFactory.getLogger(UploadService.class);
	
	/**
	 * 上传文件
	 * @param request
	 * @param savePath 文件保存路径
	 * @param fileWebPath web访问文件路径
	 * @return
	 */
	public static String uploadFile(HttpServletRequest request,String savePath,String fileWebPath){
		Map<String,MultipartFile> files=((MultipartHttpServletRequest)request).getFileMap();
		if(files.isEmpty()){
			return JsonHelper.toJson(new ResultMsg("301","上传为空")); 
	    }
		String day=DateUtil.formatDate(new Date(), "yyyyMMdd");
		fileWebPath=fileWebPath+"/"+day;
		String fileWebPathName=null;
		
	    Iterator iterator=files.entrySet().iterator();
	    if(iterator.hasNext()){
	    	Map.Entry<String,MultipartFile> obj=(Map.Entry<String,MultipartFile>)iterator.next();
	    	MultipartFile file=obj.getValue();
	    	if(file==null) {
	    		return JsonHelper.toJson(new ResultMsg("301","上传为空:"+obj.getKey())); 
		    }
	    	log.info("file name:"+file.getOriginalFilename()+",file size:"+file.getSize());
			
	    	if(file.getSize()>100*1024*1024){
	    		log.info("file size 超过100M,file name:"+file.getOriginalFilename()+",file size:"+file.getSize());
	    		return JsonHelper.toJson(new ResultMsg("303","上传大小不能超过100M")); 
		    }
	    	try {
	    		
	    		long currenttime=System.currentTimeMillis();
	    		InputStream in=file.getInputStream();
	    		String outfilepath=savePath+"/"+day;
	    		fileWebPathName=fileWebPath+"/"+currenttime+"_"+file.getOriginalFilename();
	    		
	    		File outfilepathDir=new File(outfilepath);
	    		if(!outfilepathDir.exists()){
	    			outfilepathDir.mkdirs();
	    		}
	    		
	    		String fileName=outfilepath+"/"+currenttime+"_"+file.getOriginalFilename();
	    		FileOutputStream out=new FileOutputStream(fileName);
	    		byte [] bs = new byte[1024];
	            int len = 0;
	            while((len=in.read(bs))!=-1){
	            	out.write(bs,0,len);
	            }
	            if(in!=null){
	            	in.close();
	            }
	            if(out!=null){
	            	out.close();
	            }
	            
	        } catch (Exception ex) {
	        	String exceptionMessage=ex.getMessage();
				log.info(exceptionMessage);
				StackTraceElement[] stackTraceElement = ex.getStackTrace();
	        	StringBuilder sb=new StringBuilder();
	        	for(StackTraceElement stack:stackTraceElement){
	        		sb.append(stack.getFileName());sb.append(",");
	        		sb.append(stack.getClassName());sb.append(",");
	        		sb.append(stack.getMethodName());sb.append(",");
	        		sb.append(stack.getLineNumber());sb.append(",");
	        		sb.append("--------\r\n");
	        		//stack.toString()
	        	}
	        	log.info(sb.toString());
				return JsonHelper.toJson(new ResultMsg("302","上传异常")); 
	        }
	    	
	    }
	    
		return JsonHelper.toJson(new ResultMsg(ResultCode.Success,fileWebPathName)); 
    }
	
	public static ResultMsg uploadFile1(HttpServletRequest request,String savePath,String fileWebPath){
		Map<String,MultipartFile> files=((MultipartHttpServletRequest)request).getFileMap();
		if(files.isEmpty()){
			return new ResultMsg("301","上传为空"); 
	    }
		String day=DateUtil.formatDate(new Date(), "yyyyMMdd");
		fileWebPath=fileWebPath+"/"+day;
		String fileWebPathName=null;
		
	    Iterator iterator=files.entrySet().iterator();
	    if(iterator.hasNext()){
	    	Map.Entry<String,MultipartFile> obj=(Map.Entry<String,MultipartFile>)iterator.next();
	    	MultipartFile file=obj.getValue();
	    	if(file==null) {
	    		return new ResultMsg("301","上传为空:"+obj.getKey()); 
		    }
	    	log.info("file name:"+file.getOriginalFilename()+",file size:"+file.getSize());
			
	    	if(file.getSize()>100*1024*1024){
	    		log.info("file size 超过100M,file name:"+file.getOriginalFilename()+",file size:"+file.getSize());
	    		return new ResultMsg("303","上传大小不能超过100M"); 
		    }
	    	try {
	    		
	    		long currenttime=System.currentTimeMillis();
	    		InputStream in=file.getInputStream();
	    		String outfilepath=savePath+"/"+day;
	    		fileWebPathName=fileWebPath+"/"+currenttime+"_"+file.getOriginalFilename();
	    		
	    		File outfilepathDir=new File(outfilepath);
	    		if(!outfilepathDir.exists()){
	    			outfilepathDir.mkdirs();
	    		}
	    		
	    		String fileName=outfilepath+"/"+currenttime+"_"+file.getOriginalFilename();
	    		FileOutputStream out=new FileOutputStream(fileName);
	    		byte [] bs = new byte[1024];
	            int len = 0;
	            while((len=in.read(bs))!=-1){
	            	out.write(bs,0,len);
	            }
	            if(in!=null){
	            	in.close();
	            }
	            if(out!=null){
	            	out.close();
	            }
	            
	        } catch (Exception ex) {
	        	String exceptionMessage=ex.getMessage();
				log.info(exceptionMessage);
				StackTraceElement[] stackTraceElement = ex.getStackTrace();
	        	StringBuilder sb=new StringBuilder();
	        	for(StackTraceElement stack:stackTraceElement){
	        		sb.append(stack.getFileName());sb.append(",");
	        		sb.append(stack.getClassName());sb.append(",");
	        		sb.append(stack.getMethodName());sb.append(",");
	        		sb.append(stack.getLineNumber());sb.append(",");
	        		sb.append("--------\r\n");
	        		//stack.toString()
	        	}
	        	log.info(sb.toString());
				return new ResultMsg("302","上传异常"); 
	        }
	    	
	    }
	    
		return new ResultMsg(ResultCode.Success,fileWebPathName); 
    }
	
}
