package com.lujie.common.service.dahua.lecheng.business.impl;

import java.util.HashMap;

import com.alibaba.fastjson.JSONObject;
import com.lujie.common.service.dahua.lecheng.business.UserManager;
import com.lujie.common.service.dahua.lecheng.util.HttpSend;

public class UserManagerImpl implements UserManager {

    @Override
    public JSONObject accessToken(HashMap<String, Object> paramMap) {
        String method = "accessToken";
        return HttpSend.execute(paramMap, method);
    }

}
