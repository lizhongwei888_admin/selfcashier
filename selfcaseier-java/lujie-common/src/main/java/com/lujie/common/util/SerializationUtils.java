package com.lujie.common.util;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;

import org.nustaq.serialization.FSTConfiguration;


/**
 * 高性能序列化与反序列化工具FST
 * @author hupei
 *
 */
public class SerializationUtils {

	public SerializationUtils() {
	}

	static FSTConfiguration configuration = FSTConfiguration
	// .createDefaultConfiguration();
			.createStructConfiguration(); 

	public static byte[] serialize(Object obj) {
		return configuration.asByteArray(obj);
	}

	public static Object unserialize(byte[] sec) {
		return configuration.asObject(sec);
	}
	
	public static String serializeString(Object obj) throws UnsupportedEncodingException{
		byte[] serialize = SerializationUtils.serialize(obj);
		String result = new String(serialize , "iso-8859-1");
		return result;
	}
	
	public static Object unserializeString(String from) throws UnsupportedEncodingException{
		byte [] testbytes = from.getBytes("iso-8859-1");
		return SerializationUtils.unserialize(testbytes);
	}
	
	public static void main(String[] args) throws UnsupportedEncodingException {
		User bean = new User();
		bean.setUsername("xxxxx");
		bean.setPassword("123456");
		bean.setAge(1000000);
		bean.setMethod("add");
		bean.setTemp(new Date());
		SerializableMap<String , Object> map = new SerializableMap<String , Object>();
		map.put("object", bean);
		map.put("method", "add");
		byte[] serialize = SerializationUtils.serialize(map);
		System.out.println(serialize.length);
		String test = new String(serialize , "iso-8859-1");
		System.out.println(test);
		
		byte [] testbytes = test.getBytes("iso-8859-1");
		System.out.println(testbytes.length);
		SerializableMap<String , Object> u = (SerializableMap<String , Object>) SerializationUtils.unserialize(testbytes);
		System.out.println(u);	
		
	}
}

class User implements Serializable{
	private String username;
	private int age;
	private String password;
	private Date temp;
	private String method;

	
	public Date getTemp() {
		return temp;
	}

	public void setTemp(Date temp) {
		this.temp = temp;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "User [username=" + username + ", age=" + age + ", password=" + password + ", temp=" + temp + ", method="
				+ method + "]";
	}

	
}
