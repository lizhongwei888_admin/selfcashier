import Request from '@/request'

export default {
    actions: {
        // 系统日志列表
        logAdminList({ }, reqData) {
            return Request.post("/refLogAdmin/listRefLogAdmin", reqData)
                .then(res => {
                    return Promise.resolve(res);
                })
                .catch(err => {
                    return Promise.reject(err);
                });
        },
        // 第三方日志列表
        logInterfaceList({ }, reqData) {
            return Request.post("/refLogInterface/listRefLogInterface", reqData)
                .then(res => {
                    return Promise.resolve(res);
                })
                .catch(err => {
                    return Promise.reject(err);
                });
        },
        // 自助收银日志列表
        logAPPList({}, reqData) {
            return Request.post("/refLogApp/listRefLogApp", reqData)
                .then(res => {
                    return Promise.resolve(res);
                })
                .catch(err => {
                    return Promise.reject(err);
                });
        },
        // 第三方日志列表导出
        listRefLogInterfaceExport({}, reqData) {
            return Request({
                url: '/refLogInterface/listRefLogInterfaceExport',
                method: 'post',
                data: reqData,
                responseType: 'blob'
            })
        },
        // 后台日志列表导出
        listRefLogAdminExport({}, reqData) {
            return Request({
                url: '/refLogAdmin/listRefLogAdminExport',
                method: 'post',
                data: reqData,
                responseType: 'blob'
            })
        },
        // 收银日志列表导出
        listRefLogAppExport({}, reqData) {
            return Request({
                url: '/refLogApp/listRefLogAppExport',
                method: 'post',
                data: reqData,
                responseType: 'blob'
            })
        }
    }
}