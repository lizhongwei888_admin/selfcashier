import Request from '@/request';

export default {
    actions: {
        // 商户列表
        getMerList({ commit }, reqData) {
            return Request.post("/dwMerchant/listMerchant", reqData)
                .then(res => {
                    return Promise.resolve(res);
                })
                .catch(err => {
                    return Promise.reject(err);
                });
        },
        // 商户列表（不分页）
        getMerAll({ commit }, reqData) {
            return Request.post("/dwMerchant/findAll", reqData)
                .then(res => {
                    return Promise.resolve(res);
                })
                .catch(err => {
                    return Promise.reject(err);
                });
        },
        // 保存商户信息
        saveMer({ commit }, reqData) {
            let value = reqData.code ? 'updateMerchant' : 'addMerchant'
            return Request.post(`/dwMerchant/${value}`, reqData)
                .then(res => {
                    return Promise.resolve(res);
                })
                .catch(err => {
                    return Promise.reject(err);
                });
        },

        // 修改商户状态
        merState({ }, reqData) {
            return Request.post("/dwMerchant/updateState", reqData)
                .then(res => {
                    return Promise.resolve(res);
                })
                .catch(err => {
                    return Promise.reject(err);
                });
        },

        // 删除商户
        delMerchant({ }, reqData) {
            return Request.post("/dwMerchant/delMerchant", reqData)
                .then(res => {
                    return Promise.resolve(res);
                })
                .catch(err => {
                    return Promise.reject(err);
                });
        },
        // 门店列表
        getShopList({ commit }, reqData) {
            return Request.post("/dwShop/listShop", reqData)
                .then(res => {
                    return Promise.resolve(res);
                })
                .catch(err => {
                    return Promise.reject(err);
                });
        },

        // 保存商户信息
        saveShop({ commit }, reqData) {
            let value = reqData.code ? 'updateShop' : 'addShop'
            return Request.post(`/dwShop/${value}`, reqData)
                .then(res => {
                    return Promise.resolve(res);
                })
                .catch(err => {
                    return Promise.reject(err);
                });
        },

        // 修改商户状态
        shopState({ }, reqData) {
            return Request.post("/dwShop/updateState", reqData)
                .then(res => {
                    return Promise.resolve(res);
                })
                .catch(err => {
                    return Promise.reject(err);
                });
        },

        // 删除商户
        delShop({ }, reqData) {
            return Request.post("/dwShop/delShop", reqData)
                .then(res => {
                    return Promise.resolve(res);
                })
                .catch(err => {
                    return Promise.reject(err);
                });
        }
    }
}