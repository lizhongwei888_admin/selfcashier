import Request from '@/request'

export default {
    actions: {
        // 系统接口-接口项目列表
        apiList({ }, reqData) {
            return Request.post("/refApi/listRefApi", reqData)
                .then(res => {
                    return Promise.resolve(res);
                })
                .catch(err => {
                    return Promise.reject(err);
                });
        },
       // 系统接口-接口项目详情列表
        apiItemList({ }, reqData) {
            return Request.post("/refApiItem/listRefApiItem", reqData)
                .then(res => {
                    return Promise.resolve(res);
                })
                .catch(err => {
                    return Promise.reject(err);
                });
        },

        // 保存接口项目信息
        saveApiItem({ commit }, reqData) {
            let value = reqData.code ? 'upRefApiItem' : 'addRefApiItem'
            return Request.post(`/refApiItem/${value}`, reqData)
                .then(res => {
                    return Promise.resolve(res);
                })
                .catch(err => {
                    return Promise.reject(err);
                });
        },
          // 删除接口项目
          delApiItem({ }, reqData) {
            return Request.post("/refApiItem/delRefApiItem", reqData)
                .then(res => {
                    return Promise.resolve(res);
                })
                .catch(err => {
                    return Promise.reject(err);
                });
        },
        
    }
}