import Request from '@/request'

export default {
    actions: {
        // 菜单列表
        getList({ commit }, reqData) {
            return Request.post("/refMenu/listRefMenu", reqData)
                .then(res => {
                    return Promise.resolve(res);
                })
                .catch(err => {
                    return Promise.reject(err);
                });
        },

        // 菜单保存
        preservation({ commit }, reqData) {
            let value = reqData.id ? 'upRefMenu' : 'addRefMenu'
            return Request.post(`/refMenu/${value}`, reqData)
                .then(res => {
                    return Promise.resolve(res);
                })
                .catch(err => {
                    return Promise.reject(err);
                });
        },

        // 菜单启用
        menusState({ }, reqData) {
            return Request.post("/refMenu/state", reqData)
                .then(res => {
                    return Promise.resolve(res);
                })
                .catch(err => {
                    return Promise.reject(err);
                });
        },

        // 菜单删除
        menusRome({ }, reqData) {
            return Request.post("/refMenu/delRefMenu", reqData)
                .then(res => {
                    return Promise.resolve(res);
                })
                .catch(err => {
                    return Promise.reject(err);
                });
        },
        // 用户列表
        userList({ }, reqData) {
            return Request.post("/refUser/listRefUser", reqData)
                .then(res => {
                    return Promise.resolve(res);
                })
                .catch(err => {
                    return Promise.reject(err);
                });
        },

        // 保存用户
        userSeverForm({ commit }, reqData) {
            let value = reqData.code ? 'upRefUser' : 'addRefUser'
            return Request.post(`/refUser/${value}`, reqData)
                .then(res => {
                    return Promise.resolve(res);
                })
                .catch(err => {
                    return Promise.reject(err);
                });
        },

        // 重置用户密码
        resetPassword({ }, reqData) {
            return Request.post(`/refUser/reset`, reqData)
                .then(res => {
                    return Promise.resolve(res);
                })
                .catch(err => {
                    return Promise.reject(err);
                });
        },

        // 用户删除
        userDelete({ }, reqData) {
            return Request.post("/refUser/delRefUser", reqData)
                .then(res => {
                    return Promise.resolve(res);
                })
                .catch(err => {
                    return Promise.reject(err);
                });
        },

        // 用户启用禁用
        userState({ }, reqData) {
            return Request.post("/refUser/state", reqData)
                .then(res => {
                    return Promise.resolve(res);
                })
                .catch(err => {
                    return Promise.reject(err);
                });
        },

        // 角色列表
        rolesList({ }, reqData) {
            return Request.post("/refRole/listRefRole", reqData)
                .then(res => {
                    return Promise.resolve(res);
                })
                .catch(err => {
                    return Promise.reject(err);
                });
        },

        // 保存角色权限
        modifyJurisdiction({ }, reqData) {
            return Request.post(`/refRole/upRefRoleMenu`, reqData)
                .then(res => {
                    return Promise.resolve(res);
                })
                .catch(err => {
                    return Promise.reject(err);
                });
        },

        // 角色新增修改
        rolesSeve({ }, reqData) {
            let value = reqData.code ? 'upRefRole' : 'addRefRole'
            return Request.post(`/refRole/${value}`, reqData)
                .then(res => {
                    return Promise.resolve(res.data);
                })
                .catch(err => {
                    return Promise.reject(err);
                });
        },

        // 角色删除
        rolesRome({ }, reqData) {
            return Request.post("/refRole/delRefRole", reqData)
                .then(res => {
                    return Promise.resolve(res);
                })
                .catch(err => {
                    return Promise.reject(err);
                });
        },
         // 角色启用禁用
         setEna({ }, reqData) {
            return Request.post("/refRole/state", reqData)
                .then(res => {
                    return Promise.resolve(res);
                })
                .catch(err => {
                    return Promise.reject(err);
                });
        },

        // 文件列表
        fileList({ }, reqData) {
            return Request.post("/refStorage/listRefStorage", reqData)
                .then(res => {
                    return Promise.resolve(res);
                })
                .catch(err => {
                    return Promise.reject(err);
                });
        },
        // 删除文件
        flieRome({ }, reqData) {
            return Request.post("/refStorage/delRefStorage", reqData)
                .then(res => {
                    return Promise.resolve(res);
                })
                .catch(err => {
                    return Promise.reject(err);
                });
        },
        // 文件下载
        fileDownload({ }, code) {

            return Request({
                url: `refStorage/down?code=` + code,
                method: 'get',
                responseType: 'blob'
              })


            // return Request.get(`/refStorage/down?code`+code, {responseType: 'blob' })
            //     .then(res => {
            //         return Promise.resolve(res);
            //     })
            //     .catch(err => {
            //         return Promise.reject(err);
            //     });
        },
    }
}