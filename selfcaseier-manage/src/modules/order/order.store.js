import Request from '@/request';

export default {
    actions: {
        // 订单列表
        getOrderList({ commit }, reqData) {
            return Request.post("/dwOrder/getOrderList", reqData)
                .then(res => {
                    return Promise.resolve(res);
                })
                .catch(err => {
                    return Promise.reject(err);
                });
        },
        // 退款
        refund({ commit }, reqData) {
            return Request.post("/dwOrderRefund/refund", reqData)
                .then(res => {
                    return Promise.resolve(res);
                })
                .catch(err => {
                    return Promise.reject(err);
                });
        },
          // 退款列表
          getRefundList({ commit }, reqData) {
            return Request.post("/dwOrderRefund/getList", reqData)
                .then(res => {
                    return Promise.resolve(res);
                })
                .catch(err => {
                    return Promise.reject(err);
                });
        },
    }
}