import Request from '@/request';

export default {
    actions: {
        // 列表
        getCashList({ commit }, reqData) {
            return Request.post("/dwSyCash/getCashList", reqData)
                .then(res => {
                    return Promise.resolve(res);
                })
                .catch(err => {
                    return Promise.reject(err);
                });
        },
        cashState({ }, reqData) {
            return Request.post("/dwSyCash/updateState", reqData)
                .then(res => {
                    return Promise.resolve(res);
                })
                .catch(err => {
                    return Promise.reject(err);
                });
        },
        auditData({ }, reqData) {
            return Request.post("/dwSyCash/auditData", reqData)
                .then(res => {
                    return Promise.resolve(res);
                })
                .catch(err => {
                    return Promise.reject(err);
                });
        },
        deleteData({ }, reqData) {
            return Request.post("/dwSyCash/delCash", reqData)
                .then(res => {
                    return Promise.resolve(res);
                })
                .catch(err => {
                    return Promise.reject(err);
                });
        },    
        // 退款
        getOrderList({ commit }, reqData) {
            return Request.post("/dwOrder/getOrderList", reqData)
                .then(res => {
                    return Promise.resolve(res);
                })
                .catch(err => {
                    return Promise.reject(err);
                });
        },
    }
}