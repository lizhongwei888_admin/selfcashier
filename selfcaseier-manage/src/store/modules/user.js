import Request from '@/request'
import { BASE64Helper } from '@/utils/CryptUtil.js'
import { getToken, setToken, removeToken, saveUserInfo, loadUserinfo } from '@/utils/ProjectTools.js'


// 默认加载本地用户信息
const state = {
    userInfo: loadUserinfo(),
    token: getToken(),
    valid: false, // 当前路由是否有效
    roles: {}, //角色
}



const mutations = {
    SAVE_AUTHEN_INFO: (state, data) => {
        setToken(data.token)
        saveUserInfo(data)
        state.token = data.token;
        state.userInfo = data
    },
    LOGOUT(state) {
        removeToken();
        state.userInfo = {};
        state.token = '';
        state.roles = [];
        state.valid = false;
    },
    SET_VALID: (state) => {
        state.valid = true;
    },
    USER_ROLES: (state, data) => {
        state.roles = data;
    },
    SET_VALID: (state) => {
        state.valid = true;
    }
};

const actions = {
    // 用户登录
    login({commit, dispatch }, userInfo) {
        const { account, password } = userInfo
        return new Promise((resolve, reject) => {
            Request({
                url: '/refAdmin/login',
                method: 'post',
                data: { account: account.trim(), password: password}
            }).then(async res => {
                // console.log("登录结果:"+JSON.stringify(res.data));
                // 保存状态
                commit('SAVE_AUTHEN_INFO', res.data);
                let menus = await dispatch('getUserRoles', res.data.roleID)
                resolve({ userInfo: res.data, menus})
            }).catch(error => {
                reject(error)
            })
        })
    },
    // 获取用户角色
    getUserRoles({ commit }, id) {
        return new Promise((resolve, reject) => {
            Request({
                url: '/refAdmin/getRefUserRole',
                method: 'post',
                data: {code: id}
            }).then(res => {
                commit("USER_ROLES", res.data);
                resolve(res.data)
            }).catch(error => {
                reject(error)
            })
        })
    },

    // 重置令牌
    resetToken({ commit, dispatch }) {
        console.log("退出")
        return new Promise((resolve, reject) => {
            Request({
                url: '/refAdmin/logout',
                method: 'post',
            }).then(res => {
                commit('LOGOUT')
                resolve(res)
            }).catch(error => {
                reject(error)
            })

        })
    },
    // 当前路由是否有效
    setValid({ commit }) {
        commit('SET_VALID')
    },
    // 获取用户详情
    getUserInfo({ commit }) {
        return new Promise((resolve, reject) => {
            Request({
                url: '/user/userInfo',
                method: 'get',
            }).then(res => {
                resolve(res.data)
            }).catch(error => {
                reject(error)
            })
        })
    },
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}